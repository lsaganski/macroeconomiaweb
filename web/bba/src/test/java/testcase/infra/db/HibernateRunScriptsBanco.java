package testcase.infra.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.hibernate.tool.hbm2ddl.SchemaExport;

/**
 * Exportar apenas script
 * 
 * @author ricardo
 * 
 */
public class HibernateRunScriptsBanco {

	public static void main(String[] args) throws IOException {
		Configuration cfg = new Configuration();
		Properties properties = new Properties();
		properties.load(new FileInputStream("./src/main/resources/datasource-test.properties"));
		cfg.setProperties(properties);
		cfg.setNamingStrategy(new ImprovedNamingStrategy());
		cfg.configure();
		
		String banco = null;
		String dialect = cfg.getProperty("hibernate.dialect");
		if(StringUtils.contains(dialect, "SQLServerDialect")) {
			banco = "mssqlserver";
		} else if(StringUtils.contains(dialect, "MySQLDialect")) {
			banco = "mysql";
		}
		
		if(StringUtils.isNotEmpty(banco)) {
			boolean isMssqlServer = "mssqlserver".equals(banco);
			boolean isMySql = "mysql".equals(banco);
			boolean createScript = true;
			
			String outFile = "./banco/Oficial/"+banco+"/create.sql";
			SchemaExport export = new SchemaExport(cfg);
			export.setDelimiter(";");
			export.setOutputFile(outFile);
			//		export.drop(true, true);
			export.create(createScript, false);
			String s = FileUtils.readFileToString(new File(outFile));
			
			if(isMySql) {
				System.out.println("MySQL, alterando clob para LONGTEXT  ...");
				s = StringUtils.replace(s, "clob", "longtext");
			} else if(isMssqlServer) {
				System.out.println("MS SQL SERVER, alterando clob para text ...");
				s = StringUtils.replace(s, "clob", "nvarchar(max)");
				System.out.println("MS SQL SERVER, alterando blob para image ...");
				s = StringUtils.replace(s, "blob", "image");
			}

			System.out.println("--------------");
			System.out.println(s);
			System.out.println(">> " + outFile);
			FileUtils.writeStringToFile(new File(outFile), s);
		}
	}

}
