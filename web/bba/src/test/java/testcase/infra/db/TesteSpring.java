package testcase.infra.db;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.ComentarioService;
import br.livetouch.livecom.domain.service.MensagemService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.spring.SpringUtil;
import net.livetouch.tiger.ddd.DomainException;

/**
 * Exportar o Banco
 * 
 * @author ricardo
 * 
 */
public class TesteSpring {
	protected static Logger log = Log.getLogger(TesteSpring.class);

	public static void main(String[] args) throws Exception {
		SpringUtil spring = new SpringUtil("spring-test.xml");
		
		testFindLastMensagem(spring);
	}

	private static void testFindLastMensagem(SpringUtil spring) {
		Session session = spring.openSession();
		System.out.println(session);
		MensagemService service = (MensagemService) spring.getBean(MensagemService.class);
		UsuarioService usuarioService = (UsuarioService) spring.getBean(UsuarioService.class);
		
		Usuario u = usuarioService.findByLogin("admin@livetouch.com.br");
		System.out.println(u);
		
		long conversaId = 2386;
		MensagemConversa c = (MensagemConversa) session.get(MensagemConversa.class, conversaId);
		System.out.println(c.getFrom().getLogin()+ " - " + c.getTo().getLogin());
		
		long last = 75703;
		
		
	}

	protected static void testFindUserByLogin(SpringUtil spring) {
		Session session = spring.openSession();
		System.out.println(session);
		UsuarioService service = (UsuarioService) spring.getBean(UsuarioService.class);
		System.out.println("findByLogin");
		Usuario u = service.findByLogin("rlecheta@livetouch.com.br");
		System.out.println(u);
	}

	private static Map<Long, List<Comentario>> findComentariosByPosts(List<Post> posts) {
		SpringUtil s = new SpringUtil("spring-test.xml");
		Session session = s.openSession();
		
		ComentarioService comentarioService = (ComentarioService) s.getBean(ComentarioService.class);
		
		Map<Long,List<Comentario>> mapComentarios = new HashMap<Long, List<Comentario>>();
		
		for (Post post : posts) {
			List<Comentario> comentarios = comentarioService.findByPost(post);
			mapComentarios.put(post.getId(), comentarios);
		}
		s.closeSession();
		return mapComentarios;
	}

	private static void savePosts(List<Post> posts, Map<Long,List<Comentario>> mapComentarios) throws DomainException {
		SpringUtil s = new SpringUtil("spring-test2.xml");
		Session session = s.openSession();
		
		ComentarioService comentarioService = (ComentarioService) s.getBean(ComentarioService.class);
		PostService service = (PostService) s.getBean(PostService.class);
		for (Post post : posts) {
			if(post != null) {
				List<Comentario> comentarios = mapComentarios.get(post.getId());

				Usuario user = post.getUsuario();
				System.out.println(post);
				post.setId(null);
				post.setPostDestaque(null);
				service.saveOrUpdate(user, post);

				if(comentarios != null) {
					for (Comentario c : comentarios) {
						if(c != null) {
							System.out.println(c);
							c.setId(null);
							c.setPost(post);
							comentarioService.saveOrUpdate(c);
						}
					}
				}
			}
		}
		s.closeSession();
		
	}
	
	private static List<Post> getPosts() {
		SpringUtil s = new SpringUtil("spring-test.xml");

		Session session = s.openSession();
		
		PostService service = (PostService) s.getBean(PostService.class);
		
		List<Post> posts = service.findAllByDate(DateUtils.toDate("13/08/2016 00:00:02", "dd/MM/yyyy HH:mm:ss"));
		
//		session.evict(posts);

		s.closeSession();
		
		System.out.println(posts);
		
		
		return posts;
	}
	
}
