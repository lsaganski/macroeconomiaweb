package testcase.infra.spring.scripts;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.service.MensagemService;
import br.livetouch.spring.SpringUtil;

/**
 * Atualiza a base para adicionar o grupo de origem nos grupos do usuario
 * 
 * @author Ricardo Lecheta
 *
 */
public class UpdateHibernatePrimeiraMensagemConversa {
	protected static Logger log = Log.getLogger(UpdateHibernatePrimeiraMensagemConversa.class);

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringUtil spring = SpringUtil.getInstance();
		try {

			MensagemService mensagemService = (MensagemService) spring.getBean(MensagemService.class);
			List<MensagemConversa> list = mensagemService.findAllConversas();
			System.out.println(list.size());
			for (MensagemConversa c : list) {
				if(c.getMensagens().size() > 0) {
					System.out.println("conversa: " + c.getId());
					Mensagem last = new ArrayList<Mensagem>(c.getMensagens()).get(c.getMensagens().size()-1);
					c.setLastMensagem(last);
					mensagemService.saveOrUpdate(c);
					System.out.println("msg " + last.getId());
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			spring.closeSession();
		}
	}

}
