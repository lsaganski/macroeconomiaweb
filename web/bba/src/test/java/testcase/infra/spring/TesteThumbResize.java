package testcase.infra.spring;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;

import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.utils.UploadHelper;
import junit.framework.TestCase;

public class TesteThumbResize extends TestCase {
	
	public void testResize1() throws IOException {
		resize("1.jpg");//1344x672
		resize("2.jpg");//577/1024
		resize("3.jpg");
		resize("4.jpg");
	}
	
	/**
	 * @throws IOException 
	 */
	public void resize(String name) throws IOException {
		String dir = "r:/temp/resize";;
		File f = new File(dir,name);
		
		if(f.exists()) {
			System.out.println(f);
			
			BufferedImage bimg = ImageIO.read(f);
			int w = bimg.getWidth();
			int h = bimg.getHeight();

			System.out.println("img: " + w+"/"+h);

			ThumbVO t = UploadHelper.resizeThumbMural(f.getName(), f, w, h);

			assertNotNull("Thumb nula",t);

			f = t.getFile();
			System.out.println(f);
			assertTrue(f.exists());
			bimg = ImageIO.read(f);
			w = bimg.getWidth();
			h = bimg.getHeight();

			System.out.println("thumb: " + w+"/"+h);
			
			//A - Largura menor que 580px e altura igual a 325px
			boolean a = w < 580 && h == 325;
			boolean b = w == 580 && h < 325;
			
			assertTrue("a:"+a+", b:"+b,a || b);
			
			FileUtils.copyFile(f, new File(dir,f.getName()));
		}
	}

	public void test245() throws Exception {
		File f = new File("C:/Users/RICARD~1/AppData/Local/Temp/img2.jpg");
		
		if(f.exists()) {
			System.out.println(f);
			
			BufferedImage bimg = ImageIO.read(f);
			int w = bimg.getWidth();
			int h = bimg.getHeight();

			System.out.println("img: " + w+"/"+h);
			
			ThumbVO t = UploadHelper.resizeThumbDown(f.getName(), f, w, h, 245);

			f = t.getFile();
			System.out.println(f);
			bimg = ImageIO.read(f);
			w = bimg.getWidth();
			h = bimg.getHeight();

			System.out.println("thumb: " + w+"/"+h);
		}
	}

}
