package testcase.infra.spring;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import br.livetouch.spring.SpringUtil;

public class LivecomSpringUtil extends SpringUtil {
	
	public LivecomSpringUtil(String file) {
		super(file);
	}
	
	public Session bindThread(Session session) {

		if(ctx != null) {
			txManager = (HibernateTransactionManager) ctx.getBean("transactionManager");

			SessionFactory sessionFactory = txManager.getSessionFactory();

			TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));
		}

		return session;
	}

	/**
	 * Remove a Session da Thread
	 */
	public void unbindThread(Session session) {

		if(ctx != null && txManager != null) {
			SessionFactory sessionFactory = txManager.getSessionFactory();

			TransactionSynchronizationManager.unbindResource(sessionFactory);

			SessionFactoryUtils.closeSession(session);
		}
	}
}
