package testcase.infra.spring;


import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Factory para o Spring. Recebe os arquivos XML.
 * 
 * @author ricardo
 *
 */
public class SpringFactory extends ClassPathXmlApplicationContext {

	public SpringFactory(String config){
		super(config);

		registerShutdownHook();
	}

	public SpringFactory(String[] configs){
		super(configs);

		registerShutdownHook();
	}
}
