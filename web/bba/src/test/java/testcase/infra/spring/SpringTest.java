package testcase.infra.spring;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTransactionManager;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.repository.PostRepository;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.CategoriaPostService;
import br.livetouch.livecom.domain.service.ComentarioService;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.FavoritoService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.LikeService;
import br.livetouch.livecom.domain.service.MensagemService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.ParametroService;
import br.livetouch.livecom.domain.service.PerfilService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.Service;
import br.livetouch.livecom.domain.service.TagService;
import br.livetouch.livecom.domain.service.UsuarioService;
import junit.framework.TestCase;
import net.livetouch.extras.util.TimeUtil;

/**
 * Testa se configuração Hibernate/Spring está ok
 * 
 * @author ricardo
 * 
 */
public abstract class SpringTest extends TestCase {
	protected static final Logger log = Log.getLogger(SpringTest.class);

	protected HibernateTransactionManager txManager;
	protected UsuarioService usuarioService;
	protected EmpresaService empresaService;
	protected GrupoService grupoService;
	protected PerfilService perfilService;
	protected PostService postService;
	protected PostRepository postRepository;
	protected ParametroService parametroService;
	protected ComentarioService comentarioService;
	protected MensagemService mensagemService;
	protected ArquivoService arquivoService;
	protected LikeService likeService;
	protected NotificationService notificationService;
	protected FavoritoService favoritoService;
	protected CategoriaPostService categoriaService;
	protected TagService tagService;

	protected LivecomSpringUtil spring;
	protected Session session;

	@SuppressWarnings("static-access")
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		try {
			spring = new LivecomSpringUtil(getSpringFile());

			openSession();

			usuarioService = (UsuarioService) getService(UsuarioService.class);
			empresaService = (EmpresaService) getService(EmpresaService.class);
			grupoService = (GrupoService) getService(GrupoService.class);
			perfilService = (PerfilService) getService(PerfilService.class);
			postService = (PostService) getService(PostService.class);
			postRepository = (PostRepository) getBean(PostRepository.class);
			parametroService = (ParametroService) getService(ParametroService.class);
			comentarioService = (ComentarioService) getService(ComentarioService.class);
			mensagemService = (MensagemService) getService(MensagemService.class);
			arquivoService = (ArquivoService) getService(ArquivoService.class);
			likeService = (LikeService) getService(LikeService.class);
			notificationService = (NotificationService) getService(NotificationService.class);
			favoritoService = (FavoritoService) getService(FavoritoService.class);
			categoriaService = (CategoriaPostService) getService(CategoriaPostService.class);
			tagService = (TagService) getService(TagService.class);

			List<Parametro> parametros = parametroService.findAll();
			ParametrosMap params = ParametrosMap.getInstance();
			params.setParametros(parametros);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw e;
		}
	}
	
	protected void startTime() {
		TimeUtil.start();
	}

	protected void openSession() {
		session = spring.openSession();
	}

	protected String getSpringFile() {
		return "spring-test.xml";
	}

	@SuppressWarnings({ "rawtypes" })
	protected Service getService(Class<?> class1) {
		Service bean = (Service) spring.getBean(class1);
		return bean;
	}
	
	protected Object getBean(Class<?> class1) {
		return spring.getBean(class1);
	}
	
	protected Object getBean(String name) {
		return spring.getBean(name);
	}

	@Override
	protected void runTest() throws Throwable {

		try {
			super.runTest();
		} catch (Throwable e) {

			if (!(e instanceof junit.framework.AssertionFailedError)) {
				log.error(e.getMessage(), e);
			}

			throw e;
		}
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();

		endTime();
		
		closeSession();
	}
	
	public static void main(String[] args) {
		float segundos = 472;
		float minutos = (float) (segundos  / 60);
		System.out.println("Fim Test: " + minutos + " minutos");
	}

	protected void endTime() {
		
		long time = TimeUtil.end();
		float segundos = (float) (time / 1000);
		if(segundos < 60) {
			System.out.println("Fim Test: " + segundos + " segundos");
		} else {
			float minutos = (float) (segundos / 60);
			System.out.println("Fim Test: " + minutos + " minutos");
		}
	}

	protected void closeSession() {
		spring.closeSession();
	}
	protected void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
