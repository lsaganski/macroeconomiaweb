package testcase.infra.spring;

import java.io.File;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory;
import org.dbunit.ext.mssql.MsSqlDataTypeFactory;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.TagService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.spring.SpringUtil;


/**
 * Testa se configuração Hibernate/Spring está ok
 * 
 * @author ricardo
 * 
 */
public abstract class InfraTestCase extends DatabaseTestCase {
	protected static Logger log = Log.getLogger(InfraTestCase.class);
	
	protected boolean DELETA_LOGS_SETUP = true;
	protected boolean DELETA_LOGS_TEARDOWN = true;
	protected boolean DBUNIT = false;

	protected LogService logService;
	protected UsuarioService usuarioService;
	protected TagService tagService;
	protected PostService postService;
	protected ArquivoService arquivoService;
	protected GrupoService grupoService;
	protected Usuario userAdmin;

	private SpringUtil spring;

	@Override
	protected void setUp() throws Exception {
		try {
			spring = SpringUtil.getInstance("spring-test.xml");
			spring.openSession();

			logService 	= (LogService) getBean(LogService.class);
			usuarioService 		= (UsuarioService) getBean(UsuarioService.class);
			grupoService 		= (GrupoService) getBean(GrupoService.class);
			tagService 		= (TagService) getBean(TagService.class);
			postService = (PostService) getBean(PostService.class);
			arquivoService = (ArquivoService) getBean(ArquivoService.class);

			userAdmin = usuarioService.findByLogin("admin",null);

			if (DELETA_LOGS_SETUP) {
				deleteLogs();
			}

		} catch (Exception e) {
			log.error(e.getMessage(),e);
			throw e;
		}

		if (DBUNIT) {
			super.setUp();
		}
	}


	@SuppressWarnings("rawtypes")
	private Object getBean(Class c) {
		Object bean = spring.getBean(c);
		return bean;
	}

	@Override
	protected void runTest() throws Throwable {

		try {
			super.runTest();
		} catch (Throwable e) {

			if (!(e instanceof junit.framework.AssertionFailedError)) {
				log.error(e.getMessage(),e);
			}

			throw e;
		}
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();

		if (DELETA_LOGS_TEARDOWN) {
			deleteLogs();
		}

		spring.closeSession();
	}


	private void deleteLogs() {
		try {
			logService.delete("delete from LogSistema");
			logService.delete("delete from LogTransacao");
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
	}

	protected void refreshSession(){
		spring.refreshSession();
	}


	@Override
	protected IDatabaseConnection getConnection() throws Exception {
		Connection jdbcConnection = spring.getConnection();

		jdbcConnection.setAutoCommit(true);
		DatabaseConnection conn = new DatabaseConnection(jdbcConnection);
		DatabaseConfig config = conn.getConfig();
		if(jdbcConnection.toString().contains("org.hsqldb.jdbc.jdbcConnection")){
			config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new HsqldbDataTypeFactory());
		} else if(jdbcConnection.toString().contains("jdbc:jtds:sqlserver://")) {
			config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MsSqlDataTypeFactory());
		} else {
			config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
		}
		return conn;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected IDataSet getDataSet() throws Exception {
		//push_server-sqlserver.xml
		File file = new File("./src/test/resources/livecom.xml");
		log.debug(">> DunitbTestCase: Populando base com xml: " + file + " - exists: " + file.exists());
		return new FlatXmlDataSet(file);
	}

	/**
	 * @see org.dbunit.DatabaseTestCase#getSetUpOperation()
	 */
	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.CLEAN_INSERT;
	}

	/**
	 * @see org.dbunit.DatabaseTestCase#getTearDownOperation()
	 */
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	protected void assertEquals(byte[] expected, byte[] actual) {
		if (expected == null || actual == null) {
			fail();
		} else if (expected.length != actual.length) {
			fail();
		} else {
			for (int i = 0; i < expected.length; i++) {
				if (expected[i] != actual[i]) {
					fail();
				}
			}
		}
	}

	protected void assertEquals(Date expected, Date actual) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		if (expected == null || actual == null) {
			fail();
		}
		assertEquals(sdf.format(expected), sdf.format(actual));
	}
}
