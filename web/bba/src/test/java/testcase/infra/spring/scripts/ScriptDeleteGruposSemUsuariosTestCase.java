package testcase.infra.spring.scripts;

import java.util.List;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.GrupoFilter;
import testcase.infra.spring.InfraTestCase;

public class ScriptDeleteGruposSemUsuariosTestCase extends InfraTestCase {

	public void testScript() throws Exception {
		
		GrupoFilter filter = new GrupoFilter();
//		filter.nome = nome;
		filter.page = 0;
		filter.maxRows = 50;

		Usuario admin = usuarioService.get(1L);
		List<Grupo> gruposEmpty = grupoService.findAllWithoutUsers(filter, admin);

		for (Grupo grupo : gruposEmpty) {
			System.out.println(grupo.getNome());
			try {
				grupoService.delete(admin, grupo, false);
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
	}
}
