package testcase.date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;

import br.livetouch.livecom.domain.OperacoesCielo;
import br.livetouch.livecom.domain.service.OperacoesCieloService;
import br.livetouch.spring.SpringUtil;
import junit.framework.TestCase;


public class SameWeekDayLastYearTestCase extends TestCase {
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void test() throws ParseException {

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date data = dateFormat.parse("31/01/2016");
		Date anterior = new DateTime(data).minusYears(1).toDate();
		
		int dia = new DateTime(data).getDayOfWeek();
		int diaAnterior = new DateTime(anterior).getDayOfWeek();
		
		int valor = dia - diaAnterior;
		valor = valor < 0 ? valor * -1 : valor;
		
		System.out.println("diferença " + valor);
		
		Date nova = new DateTime(anterior).plusDays(valor).toDate();
		System.out.println("atual " + data);
		System.out.println("passada " + anterior);
		System.out.println("referencia " + nova);
		
		media(nova);
		
	}
	
	public void media(Date data) throws ParseException {

		SpringUtil spring = SpringUtil.getTestInstance();
		OperacoesCieloService transacaoService = (OperacoesCieloService) spring.getBean(OperacoesCieloService.class);
		OperacoesCielo transacao = transacaoService.getByData(data);
		System.out.println(transacao.getData() + " - " + transacao.getVolumetria());
		
		Double atual = 20889193.;
		Double anterior = 22854697.;
		
		Double coeficiente = (atual - anterior) / anterior;
		
		coeficiente = coeficiente * 100;
		
		System.out.printf("%.2f", coeficiente);
		
	}
}
