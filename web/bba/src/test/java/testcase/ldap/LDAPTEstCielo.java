package testcase.ldap;
import br.livetouch.livecom.ldap.LDAPUser;
import br.livetouch.livecom.ldap.LDAPUtil;

public class LDAPTEstCielo {

	public static void main(String[] args) {
		LDAPUtil ldap = new LDAPUtil(null);

		LDAPUser s = ldap.loginByDN("CN=Jaime Peres Servidone Nagase,OU=Users,OU=Alphaville,OU=Sites,DC=visanet,DC=corp","jnagase", "Jimoeds8");
		System.out.println(s.getName());
		System.out.println(s.getNameInNamespace());

		s = ldap.loginByDN("CN=Jaime Peres Servidone Nagase,OU=Users,OU=Alphaville,OU=Sites,DC=visanet,DC=corp","jaime.nagase@cielo.com.br", "Jimoeds8");
		System.out.println(s.getName());
		System.out.println(s.getNameInNamespace());
	}
}
