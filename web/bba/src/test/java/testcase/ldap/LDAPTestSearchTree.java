package testcase.ldap;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class LDAPTestSearchTree {
	static DirContext ldapContext;

	public static void main(String[] args) throws NamingException {
		try {
			System.out.println("LDAP Test");
			
			// base DN
			String searchBase = "dc=livetouchdev,dc=com,dc=br";

			// parametro do usuario admin que vai realizar a autenticação para a busca
			String cn = "cn=admin,dc=livetouchdev,dc=com,dc=br";
			// senha do usuário
			String password = "livetouch2013";

			// faz o primeiro login do admin e depois realiza a busca
			login(cn, password);

			// Create the search controls
			SearchControls searchCtls = new SearchControls();

			// especifica quais atributos vao ser retornados
			String returnedAtts[] = { "sn", "givenName", "mail" };
			searchCtls.setReturningAttributes(returnedAtts);

			// especifica o scopo que vamos buscar todos os usuarios de sub diretorios tbm
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			// email e senha do usuário que vamos buscar na base
			String searchFilter = "(&(mail=rlecheta@livetouch.com.br)(userPassword=rlecheta))";

			// Specify the Base for the search
			print(searchCtls, searchFilter, searchBase);
			
			ldapContext.close();
		} catch (Exception e) {
			System.err.println(" Search error: " + e.getMessage() + " - " + e);
			System.exit(-1);
		}
	}

	private static void login(String cn, String password) throws NamingException {
		Hashtable<String, String> ldapEnv = new Hashtable<String, String>(11);
		ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		ldapEnv.put(Context.PROVIDER_URL, "ldap://livetouchdev.com.br:389");
		ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
		ldapEnv.put(Context.SECURITY_PRINCIPAL,cn);
		ldapEnv.put(Context.SECURITY_CREDENTIALS, password);
		ldapContext = new InitialDirContext(ldapEnv);
	}

	private static void print(SearchControls searchCtls, String searchFilter, String searchBase) throws NamingException {
		// initialize counter to total the results
		int totalResults = 0;
		
		// Search for objects using the filter
		NamingEnumeration<SearchResult> answer = ldapContext.search(searchBase, searchFilter, searchCtls);

		// Loop through the search results
		while (answer.hasMoreElements()) {
			SearchResult sr = (SearchResult) answer.next();

			totalResults++;

			System.out.println(">>>" + sr.getName());
			Attributes attrs = sr.getAttributes();
			System.out.println(">>>>>> " + attrs.get("givenName") + " - MAIL: " + attrs.get("MAIL"));
		}

		System.out.println("Total results: " + totalResults);
	}
}