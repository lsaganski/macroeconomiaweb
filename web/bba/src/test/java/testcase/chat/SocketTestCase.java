package testcase.chat;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.json.JSONException;

import junit.framework.TestCase;

public class SocketTestCase extends TestCase {

	private static final String IP = "127.0.01";

	public void testConnect() throws IOException, JSONException {
		Socket socket = new Socket();
        socket.connect(new InetSocketAddress(IP, 9091), 5000);
        boolean connected = socket.isConnected();
        System.out.println(connected);
        assertTrue(connected);

        StringInputStream in = new StringInputStream(socket.getInputStream());
        StringOutputStream out = new StringOutputStream(socket.getOutputStream());

        String login = "{\"code\":\"login\",\"content\":{\"user\":\"rlecheta@livetouch.com.br\",\"pass\":\"ricardo\"}}";
        out.writeUTF(login);

        // {"code":"loginResponse","content":{"login":"rlecheta@livetouch.com.br","status":"Ok"}}
        String jsonResponse = in.readUTF();
        log(jsonResponse);

        boolean jsonOK = ChatTestHelper.validateJsonOK(jsonResponse);
        assertTrue(jsonOK);

//        String json = "{\"code\":\"ping\",\"content\":{\"user\":\"rlecheta@livetouch.com.br\"}}";
//        out.writeUTF(json);
        
        String json = "{\"code\":\"getMessages\",\"content\":{\"userId\":2524,\"lastReceivedMessageID\":76245}}";
        out.writeUTF(json);

        jsonResponse = in.readUTF();
        log(jsonResponse);

        // Close
        socket.close();
        in.close();
        out.close();
	}

	private void log(String s) {
		System.out.println(s);
	}

}
