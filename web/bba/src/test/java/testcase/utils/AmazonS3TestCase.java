package testcase.utils;

import java.io.IOException;
import java.util.List;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.files.AmazonS3FileManager;
import br.livetouch.livecom.files.FileManager;
import br.livetouch.livecom.files.FileManagerFactory;
import br.livetouch.livecom.files.LivecomFile;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class AmazonS3TestCase extends SpringTest {

	public void testGetFiles() throws DomainException, IOException {
		ParametrosMap map = ParametrosMap.getInstance();
		FileManager file = FileManagerFactory.getFileManager(map);
		try {
			List<LivecomFile> files = file.getFiles();

			for (LivecomFile livecomFile : files) {
				System.out.println(livecomFile.getFileName());
			}
		} finally {
			file.close();
		}
		
	}

	public void testS3() throws IOException {
		ParametrosMap params = ParametrosMap.getInstance();

		String credentials = params.get("file.manager.amazon.credentials");
		System.out.println(credentials);

		AmazonS3FileManager s3 = new AmazonS3FileManager(params);
		LivecomFile file = s3.getFile("nome.txt");
		if (file != null) {
			System.out.println("File OK: " + file.getFileName());
		} else {
			s3.putFile("temp", "nome.txt", "plain/text", "Ricardo".getBytes());
			System.out.println("PUT OK");
		}
	}
}
