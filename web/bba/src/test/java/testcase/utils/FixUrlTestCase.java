package testcase.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.files.AmazonS3FileManager;
import br.livetouch.livecom.utils.FileExtensionUtils;
import testcase.infra.spring.SpringTest;

public class FixUrlTestCase extends SpringTest {

	public void testFixUrl() throws IOException {
		ParametrosMap params = ParametrosMap.getInstance();

		String credentials = params.get("file.manager.amazon.credentials");
		System.out.println(credentials);
		AmazonS3FileManager s3 = new AmazonS3FileManager(params);

		String nome = FileExtensionUtils
				.getFileNameFixed("Eu! estou [fazendo] #isto cérto?.html.jpg");
		nome = br.infra.util.StringUtils.removeAcentos(nome);
		nome = nome.replaceAll("[^\\w{ASCII}^.]", "");

		assertEquals("Eu_estou_fazendo_isto_certo_html.jpg", nome);

		String ext = FileExtensionUtils.getExtensao(nome);

		assertEquals("jpg", ext);

		nome = FileExtensionUtils.getFileNameFixed("02dab01[2].jpg");
		nome = br.infra.util.StringUtils.removeAcentos(nome);
		nome = nome.replaceAll("[^\\w{ASCII}^.]", "");

		// teste foto

		File f = new File("/home/live/Downloads", "02dab01[2].jpg");

		assertTrue(f.exists());

		FileInputStream in = new FileInputStream(f);

		s3.putFile("38023a8d-ba91-4a8f-996f-e25730c0e41f/arquivos", nome,
				"image/jpeg", IOUtils.toByteArray(in));

		String url = s3.getFileUrl(
				"38023a8d-ba91-4a8f-996f-e25730c0e41f/arquivos", nome);

		assertEquals(
				"https://s3-sa-east-1.amazonaws.com/livecom-itau-revista/38023a8d-ba91-4a8f-996f-e25730c0e41f/arquivos/02dab012.jpg",
				url);

		// teste foto

		nome = FileExtensionUtils.getFileNameFixed("fóto!d e tes[es]?.jpg");
		nome = br.infra.util.StringUtils.removeAcentos(nome);
		nome = nome.replaceAll("[^\\w{ASCII}^.]", "");

		f = new File("/home/live/Downloads", "fóto!d e tes[es]?.jpg");

		assertTrue(f.exists());

		in = new FileInputStream(f);

		s3.putFile("38023a8d-ba91-4a8f-996f-e25730c0e41f/arquivos", nome,
				"image/jpeg", IOUtils.toByteArray(in));

		url = s3.getFileUrl("38023a8d-ba91-4a8f-996f-e25730c0e41f/arquivos",
				nome);

		assertEquals(
				"https://s3-sa-east-1.amazonaws.com/livecom-itau-revista/38023a8d-ba91-4a8f-996f-e25730c0e41f/arquivos/fotod_e_teses.jpg",
				url);

	}
}
