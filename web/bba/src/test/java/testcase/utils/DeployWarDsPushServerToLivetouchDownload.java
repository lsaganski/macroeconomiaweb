package testcase.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.IOUtils;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.files.FileManager;
import br.livetouch.livecom.files.FileManagerFactory;
import br.livetouch.livecom.utils.TimeUtils;

public class DeployWarDsPushServerToLivetouchDownload {

	private static String BUCKET = "livetouch-download";
	private static String CREDENTIALS = "credentials_livecom_s3.csv";
	private static File file = new File("./../PushServer/build",
			"push_server-ds.war");

	public static void main(String[] args) throws IOException {

		setupS3();

		String fileName = file.getName();
		TimeUtils.start();
		System.out.println(new Date());
		System.out.println("Fazendo upload: " + fileName + " ...");

		ParametrosMap map = ParametrosMap.getInstance();
		FileManager fm = FileManagerFactory.getFileManager(map);
		try {
			byte[] bytes = IOUtils.toByteArray(new FileInputStream(file));
			String url = fm.putFile(null, fileName, "", bytes);

			System.out.println("Upload OK: " + fileName);
			System.out.println(new Date());
			TimeUtils.end();
			System.out.println(TimeUtils.getTimeMinutosString());
			System.out.println(url);
		} finally {
			fm.close();
		}
		
	}

	private static void setupS3() {
		ParametrosMap params = ParametrosMap.getInstance();

		params.put("file.manager.amazon.server",
				"https://s3-sa-east-1.amazonaws.com");
		params.put("file.manager.amazon.bucket", BUCKET);
		params.put("file.manager.amazon.credentials", CREDENTIALS);
		String bucket = params.get("file.manager.amazon.bucket");
		String credentials = params.get("file.manager.amazon.credentials");
		String server = params.get("file.manager.amazon.server");

		System.out.println(server);
		System.out.println("S3 Bucket: " + bucket);
		System.out.println("S3 credentials: " + credentials);
	}
}
