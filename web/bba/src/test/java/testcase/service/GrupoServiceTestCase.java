package testcase.service;

import java.util.List;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class GrupoServiceTestCase extends SpringTest {

	public void testDeleteAllFromUser() throws DomainException {
		long id = 13;
		Usuario u = usuarioService.get(id);
		assertNotNull(u);
		System.out.println(u.toStringIdDesc());

		List<Grupo> list = grupoService.findAllByUser(u);
		System.out.println("Grupos: " + list.size());

		for (Grupo a : list) {
			System.out.println(a.toStringDesc());
			grupoService.delete(null,a,false);
		}
		
		System.out.println("OK");
	}
	
	public void testDelete() throws DomainException {
		long id = 5315L;

		Arquivo c = arquivoService.get(id);
		assertNotNull(c);
		System.out.println(c.getArquivoRef());
		
		System.out.println(c.getNome());
		System.out.println(c.getUrl());

		arquivoService.delete(c);
		
		c = arquivoService.get(id);

		assertNull(c);
	}
}
