package testcase.service;

import java.util.List;

import br.livetouch.livecom.domain.Likes;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class LikesServiceTestCase extends SpringTest {

	public void testDeleteAll() throws DomainException {
		List<Likes> list = likeService.findAll();
		
		List<Long> ids = Likes.getIds(list);
		System.out.println("Total: " + ids.size());
		
		startTime();

		// 0 segundos
		likeService.delete(ids);
		
		//Fim Test: 24 segundos
//		for (Likes a : list) {
//			likeService.delete(a);
//		}
		
		System.out.println("OK");
		
		list = likeService.findAll();
		System.out.println("Total: " + list.size());
	}
}
