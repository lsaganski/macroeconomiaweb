/**
	 * user[0] = Long id
	 * user[1] = String login
	 * user[2] = Long badge
	 */package testcase.service;

import java.util.ArrayList;
import java.util.List;

import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class ExpedienteTestCase extends SpringTest {
	
	public void testValidaHorario() throws DomainException {
		List<Long> ids = new ArrayList<Long>();
		ids.add(2561L); // nowak
		ids.add(2529L); // augusto
		
		ids = usuarioService.findAllUsuarioIdsDentroHorario(ids);
		
		// nowak
		assertTrue("Qtde errada: " + ids.size(),ids.size() == 2);
		
		assertEquals(String.valueOf(2529L), ids.get(0).toString());
		assertEquals(String.valueOf(2561L), ids.get(1).toString());
	}
}
