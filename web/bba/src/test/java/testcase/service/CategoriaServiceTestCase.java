package testcase.service;

import java.util.List;

import br.livetouch.livecom.domain.CategoriaPost;
import net.livetouch.extras.util.TimeUtil;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class CategoriaServiceTestCase extends SpringTest {

	public void testFindAll() throws DomainException {
		List<CategoriaPost> list = categoriaService.findAll(null);
		for (CategoriaPost c : list) {
			System.out.println(c);
			System.out.println(c.getPosts().size());
		}
	}
	
	public void testDeleteAlll() throws DomainException {
		List<CategoriaPost> list = categoriaService.findAll(null);
		int total = list.size();
		System.out.println("Categs Total: " + list.size());
		for (CategoriaPost c : list) {
			System.out.println(c.toStringIdDesc());
			try {
				categoriaService.delete(null, c, true);
				System.out.println("Total: " + total--);
			} catch (Exception e) {
				// da erro porque deleta em cascade as filhas..
				System.err.println("ERRO: " + c.toStringIdDesc());
			}
		}
		System.out.println("Total: " + total);
		System.out.println("Total2: " + categoriaService.findAll(null));
	}

	public void testDelete() throws DomainException {
		//234,2
		TimeUtil.start();
		long id = 2;
		CategoriaPost c = categoriaService.get(id);
		assertNotNull("CategoriaPost not found", c);

		System.out.println(c.getNome());
		try {
			categoriaService.delete(null, c,true);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		long time = TimeUtil.end();
		System.out.println(time / 1000 + " segundos");
	}

}
