package testcase.service.fixes;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.livetouch.livecom.domain.Arquivo;
import testcase.infra.spring.InfraTestCase;

public class FixArquivosTransacaoTestCase extends InfraTestCase {

	public void testPopular() throws Exception {
		log.debug("Base populada com sucesso");

		List<Arquivo> list = arquivoService.findAll();
		System.out.println("Corrigindo arquivos " + list.size() + " posts.");
		for (Arquivo a : list) {
			if(StringUtils.contains(a.getUrl(), "https://s3-sa-east-1.amazonaws.com/livecom/")) {
				a.setUrl(StringUtils.replace(a.getUrl(), "https://s3-sa-east-1.amazonaws.com/livecom/","https://s3-sa-east-1.amazonaws.com/livecom-livetouch/"));
				System.out.println("Fix arquivo: " + a.toStringDesc());
				arquivoService.saveOrUpdate(a);
			}
		}
	}
}
