/**
	 * user[0] = Long id
	 * user[1] = String login
	 * user[2] = Long badge
	 */package testcase.service;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class PostServiceTestCase extends SpringTest {
	public void testDelete() throws DomainException {
		long id = 5223L;

		Post post = postService.get(id);
		assertNotNull("Post not found: " + id,post);
		
		System.out.println(post.getTitulo());
		System.out.println(post.getId());
		System.out.println("delete");

		postService.delete(null, post, true);
		
		System.out.println("delete ok");

		post = postService.get(id);

		assertNull(post);
	}
	
	public void testDeleteAllFromUser() throws DomainException {
		long id = 2;
		Usuario u = usuarioService.get(id);
		assertNotNull(u);

		List<Long> ids = postService.findAllOwnerByUser(u);
		System.out.println("Posts: " + ids.size());

		postService.delete(null, ids,false, true);
		
		System.out.println("OK");
	}
	
	/**
	 * A: 7.866667 minutos
	 * B: 3.9666667 minutos
	 * C: 22.0 segundos
	 * D: 20.0 segundos
	 * E: 3.0 segundos
	 * 
	 * @throws DomainException
	 */
	public void testDeleteAll() throws DomainException {

		List<Post> posts = postService.findAll();
		int total = posts.size();

		List<Long> ids = Post.getIds(posts);
		System.out.println("Total: " + ids);

		startTime();

		postService.delete(null,ids, true,true);

		posts = postService.findAll();
		total = posts.size();
		System.out.println("Total: " + total);
	}
	
	public void testDeletePosts() throws DomainException {
		List<Post> posts = postService.findAll();
		
		List<Post> list = new ArrayList<Post>();
		Post p = posts.get(0);
		Post p2 = posts.get(1);
		Post p3 = posts.get(2);
		list.add(p);
		list.add(p2);
		list.add(p3);
		
		postRepository.delete(posts);
	}
	
	public void testPublicar() throws DomainException {
		System.out.println(postService.publicar());
	}
}
