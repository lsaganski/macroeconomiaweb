package testcase.service;

import java.util.List;

import br.livetouch.livecom.domain.Favorito;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class FavoritoserviceTestCase extends SpringTest {

	public void testDeleteAllFromUser() throws DomainException {
		long id = 13;
		Usuario u = usuarioService.get(id);
		assertNotNull(u);

		List<Favorito> list = favoritoService.findAllByUser(u);
		System.out.println(list);

		for (Favorito a : list) {
			System.out.println(a.toString());
			favoritoService.delete(a);
		}
		
		System.out.println("OK");
	}
}
