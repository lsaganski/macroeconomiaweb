package testcase.service;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.vo.NotificationBadge;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class NotificationServiceTestCase extends SpringTest {

	/**
	 * Busca uma NotificationBadge
	 */
	public void testBadgeUsuario() throws DomainException {
		long userId = 2524;

		Usuario u = usuarioService.get(userId);
		assertNotNull("Usuário não existe", u);

		System.err.println(u.toStringIdDesc());

		NotificationBadge badge = notificationService.findBadges(u);

		System.out.println(badge);
	}

	/**
	 * Busca o array otimizado das badges para uma lista de usuarios
	 * 
	 * @throws DomainException
	 */
	public void testBadgeUsuariosToPush() throws DomainException {
		List<Long> ids = new ArrayList<>();
		ids.add(2524L);// lecheta
		ids.add(2525L);// marcio

		List<Object[]> push = notificationService.findBadgesToPush(TipoNotificacao.NEW_POST, ids);

		/**
		 * O array é composto de:
		 * 
		 * badge[0] = userId
		 * badge[1] = login
		 * badge[2] = badge
		 */
		for (Object[] badge : push) {
			System.out.println("--");
			System.out.println(badge[0]);
			System.out.println(badge[1]);
			System.out.println(badge[2]);
		}
	}
}
