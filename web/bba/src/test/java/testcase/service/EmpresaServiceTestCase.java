package testcase.service;

import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.orm.hibernate4.HibernateTransactionManager;

import br.livetouch.livecom.domain.Empresa;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class EmpresaServiceTestCase extends SpringTest {
	/**
	 * Empresa [id=1, nome=LivecomProd, codigo=livecomProd,
	 * host=http://livecom.livetouchdev.com.br] Empresa [id=3, nome=Cetip,
	 * codigo=cetip, host=http://cetip.livetouchdev.com.br] Empresa [id=6,
	 * nome=Porto Cartões, codigo=psc,
	 * host=http://portoecartoes.livetouchdev.com.br] Empresa [id=9,
	 * nome=Livecom Homolog, codigo=LivecomHomolog,
	 * host=http://livecomhomolog.livetouchdev.com.br] Empresa [id=12,
	 * nome=Cielo, codigo=Cielo, host=http://cielo.livetouchdev.com.br] Empresa
	 * [id=13, nome=Tectrain, codigo=Tectrain,
	 * host=http://tectrain.livetouchdev.com.br] Empresa [id=14, nome=Nexohw,
	 * codigo=Nexohw, host=http://nexohw.livetouchdev.com.br] Empresa [id=30,
	 * nome=livrows, codigo=livrows, host=http://livrows.livetouchdev.com.br]
	 * Empresa [id=31, nome=iPhone, codigo=iPhone,
	 * host=http://livroios.livetouchdev.com.br] Empresa [id=32,
	 * nome=livroandroidessencial, codigo=livroandroidessencial,
	 * host=http://livroandroidessencial.livetouchdev.com.br] Empresa [id=33,
	 * nome=livetouch, codigo=livetouch,
	 * host=http://generico.livetouchdev.com.br]
	 */
	public void testFindAll() throws DomainException {
		List<Empresa> list = empresaService.findAll();
		for (Empresa e : list) {
			System.out.println(e);
		}
	}

	public void testDeleteAll() throws DomainException {
		List<Empresa> list = empresaService.findAll();
		for (Empresa e : list) {
			boolean isLivecom = e.getId() == 1;
			boolean isLivecomHomolog = e.getId() == 9;
			if (isLivecom || isLivecomHomolog) {
//				continue;
			}
			System.out.println(e);
			sleep(1500);
			try {
				empresaService.delete(null, e);
			} catch (DomainException ex) {
				System.err.println("ERROR Empresa: " + e.getNome() + "  : " + ex.getMessage());
				fail(ex.getMessage());
			}
		}
	}

	public void testDelete() throws DomainException {
		long id = 9;
		Empresa e = empresaService.get(id);
		assertNotNull("Empresa not found", e);

		System.out.println(e.getNome());
		sleep(500);
		empresaService.delete(null, e);
	}

	/**
	 * select count(*) em todas tabelas
	 */
	public void testPrintAllTables() {
		Map<String, ClassMetadata> m = getSessionFactory().getAllClassMetadata();
		for (String className : m.keySet()) {
			try {
				Long count = (Long) session.createQuery("select count(*) from " + className).uniqueResult();
				if(count == 0) {
//					System.out.println(className + ": " + count);
				} else {
					System.err.println(className + ": " + count);
				}
			} catch (Exception e) {
				System.err.println("\n*** " +className + " : " + e.getMessage());
			}
		}
	}

	private SessionFactory getSessionFactory() {
		HibernateTransactionManager txManager = (HibernateTransactionManager) getBean("transactionManager");
		SessionFactory sessionFactory = txManager.getSessionFactory();
		return sessionFactory;
	}

}
