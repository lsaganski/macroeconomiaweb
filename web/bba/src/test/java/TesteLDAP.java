import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

/**
 * 
 * 
 * @author rlech
 *
 */
public class TesteLDAP {

	private static final String LDAP_PROVIDER_URL = "LDAP_PROVIDER_URL";
	private static final String LDAP_INITIAL_CONTEXT_FACTORY = "LDAP_INITIAL_CONTEXT_FACTORY";
	private static final String LDAP_SECURITY_AUTHENTICATION = "LDAP_SECURITY_AUTHENTICATION";
	private static final String LDAP_SEARCH_FILTER = "LDAP_SEARCH_FILTER";
	private static final String LDAP_RETURN_ATTRS = "LDAP_RETURN_ATTRS";
	private static final String LDAP_SEARCH_BASE = "LDAP_SEARCH_BASE";
	private static final String LDAP_SECURITY_PRINCIPAL = "LDAP_SECURITY_PRINCIPAL";
	private static final String LDAP_SECURITY_PASSWD = "LDAP_SECURITY_PASSWD";
	private static final String LDAP_TIMEOUT = "LDAP_TIMEOUT";
	private DirContext ldapContext;
	private Map<String, String> params;

	public TesteLDAP(Map<String, String> params) {
		this.params = params;
	}

	public DirContext getContext(String login, String senha) throws NamingException {
		log("getContext");
		String urlProvider = getParam(LDAP_PROVIDER_URL, "ldap://visanet.corp:389");
		String contextFactory = getParam(LDAP_INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		String securityAuthentication = getParam(LDAP_SECURITY_AUTHENTICATION, "simple");
		String timeout = getParam(LDAP_TIMEOUT, "1000");

		// String securityPrincipal =
		// getParam(LDAP_SECURITY_PRINCIPAL,"CN=Jaime Peres Servidone
		// Nagase,OU=Users,OU=Alphaville,OU=Sites,DC=visanet,DC=corp");
		// String securityCredentials = getParam(LDAP_SECURITY_PASSWD,
		// "livetouch2013");

		Hashtable<String, String> ldapEnv = new Hashtable<String, String>(11);
		ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
		ldapEnv.put(Context.PROVIDER_URL, urlProvider);
		ldapEnv.put(Context.SECURITY_AUTHENTICATION, securityAuthentication);
		ldapEnv.put(Context.SECURITY_PRINCIPAL, login);
		ldapEnv.put(Context.SECURITY_CREDENTIALS, senha);
		//ldapEnv.put("com.sun.jndi.ldap.read.timeout", timeout);
		log("LDAPUtil map: " + ldapEnv);

		InitialDirContext ctx = new InitialDirContext(ldapEnv);
		
		log("LDAPUtil ctx: " + ctx);
		return ctx;
	}

	public LDAPUser loginByDN(String userCn,String login, String pwd) {
		// Find login
		try {
			log(">> LDAPUtil login userCn: " + userCn);

			ldapContext = getContext(userCn, pwd);

			// Specify the Base for the search
			String searchBase = getSearchBase();

			String searchFilter = getParam(LDAP_SEARCH_FILTER, "(&(objectClass=user)(sAMAccountName=" + login + "))");
			if (searchFilter.contains("%loginOrMail%")) {
				searchFilter = searchFilter.replaceAll("%loginOrMail%", login);
			}

			// Create the search controls
			SearchControls searchCtls = new SearchControls();

			// Specify the attributes to return
			String attrs = getParam(LDAP_RETURN_ATTRS, "sn, givenName, samAccountName, userPrincipalName, MAIL");
			if (attrs != null && attrs.contains(",")) {
				String[] split = attrs.split(",");
				searchCtls.setReturningAttributes(split);
			}

			// Specify the search scope
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			LDAPUser user = find(ldapContext, searchBase, searchFilter, searchCtls);

			log("<< LDAPUtil login: " + user);

			return user;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (ldapContext != null) {
				try {
					log("Fechando conexão do Contexto");
					ldapContext.close();
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getSearchBase() {
		return getParam(LDAP_SEARCH_BASE, "dc=visanet,dc=corp");
	}

	public LDAPUser findUser(String login) {
		// Find login
		try {
			log(">> LDAPUtil findUser: " + login);

			String loginAdmin = getParam(LDAP_SECURITY_PRINCIPAL, "CN=Jaime Peres Servidone Nagase,OU=Users,OU=Alphaville,OU=Sites,DC=visanet,DC=corp");
			String pwdAdmin = getParam(LDAP_SECURITY_PASSWD, "livetouch2013");
			ldapContext = getContext(loginAdmin, pwdAdmin);

			// Create the search controls
			SearchControls searchCtls = new SearchControls();

			// Specify the attributes to return
			String attrs = getParam(LDAP_RETURN_ATTRS, "sn, givenName, samAccountName, userPrincipalName, MAIL");
			if (attrs != null && attrs.contains(",")) {
				String[] split = attrs.split(",");
				searchCtls.setReturningAttributes(split);
			}

			// Specify the search scope
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			// Specify the Base for the search
			String searchBase = getSearchBase();

			// Find by sAMAccountName
			String searchFilter = getParam(LDAP_SEARCH_FILTER, "(&(objectClass=user)(sAMAccountName=" + login + "))");
			if (searchFilter.contains("%loginOrMail%")) {
				searchFilter = searchFilter.replaceAll("%loginOrMail%", login);
			}

			// if(searchFilter.contains("%passwd%")) {
			// searchFilter = searchFilter.replaceAll("%passwd%", pwd);
			// }

			LDAPUser user = find(ldapContext, searchBase, searchFilter, searchCtls);

			log("<< LDAPUtil login: " + user);

			return user;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (ldapContext != null) {
				try {
					ldapContext.close();
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}
		}

	}

	private LDAPUser find(DirContext ctx, String searchBase, String searchFilter, SearchControls searchControls) throws NamingException {

		log(">> LDAPUtil find: searchBase [" + searchBase + "], searchFilter [" + searchFilter + "] ");

		NamingEnumeration<SearchResult> results = ctx.search(searchBase, searchFilter, searchControls);

		SearchResult searchResult = null;
		
		if (results.hasMoreElements()) {
			searchResult = (SearchResult) results.next();

			LDAPUser user = new LDAPUser(searchResult,searchBase);
			log("<< LDAPUtil find LDAPUser: " + user);
			results.close();
			return user;
		}
		results.close();
		return null;
	}

	private String getParam(String key, String defaultValue) {
		if (params == null) {
			throw new IllegalArgumentException("Params is null");
		}
		String value = params.get(key);
		if(value == null) {
			return defaultValue;
		}
		log("getParam [" + key + "] > [" + value + "]");
		return value;
	}

	private void log(String string) {
		System.out.println(string);
	}
	
	public LDAPUser login(String login, String pwd) {
		LDAPUser r = findUser(login);
		if(r == null) {
			return null;
		}
		
		String userDN = r.getUserDN();
		
		r = loginByDN(userDN,login, pwd);
		
		return r;
		
	}
	
	static public class LDAPUser {

		private SearchResult result;
		private String searchBase;

		public LDAPUser(SearchResult r, String searchBase) {
			super();
			this.result = r;
			this.searchBase = searchBase;
		}
		
		public SearchResult getResult() {
			return result;
		}

		public String getName() {
			return result != null ? result.getName() : null;
		}

		public String getNameInNamespace() {
			return result != null ? result.getNameInNamespace() : null;
		}
		
		public String get(String key) {
			if(result != null) {
				Attributes attrs = result.getAttributes();
				Attribute value = attrs.get(key);
				return value.toString();
			}
			return null;
		}
		
		public Attributes geAttrs() {
			return result != null ? result.getAttributes() : null;
		}
		
		/**
		 * uid=rlecheta,ou=tecnologia,dc=livetouchdev,dc=com,dc=br
		 * CN=Jaime Peres Servidone Nagase,OU=Users,OU=Alphaville,OU=Sites,DC=visanet,DC=corp
		 * 
		 * @return
		 */
		public String getUserDN() {
			String name = getName();
			String dn = name+","+searchBase;
			return dn;
		}
		
		@Override
		public String toString() {
			StringBuffer sb = new StringBuffer("UserDN: " + getUserDN());
			if(result != null) {
				sb.append(", Attrs: " + geAttrs());
			}
			return sb.toString();
		}
	}
	
	public static void main(String[] args) {

		Map<String,String> params = new HashMap<>();
		params.put(LDAP_PROVIDER_URL, "ldap://10.82.9.15:389");
		params.put(LDAP_SECURITY_PRINCIPAL	, "CN=SRV_LiveCom,OU=Service Accounts,dc=visanet,dc=corp");
		params.put(LDAP_SECURITY_PASSWD	, "q$rR9Zy6f8NY");
		params.put(LDAP_SEARCH_BASE	, "dc=visanet,dc=corp");
//		params.put(LDAP_SEARCH_FILTER	, "(&(objectClass=user)(MAIL=%loginOrMail%))");
		params.put(LDAP_SEARCH_FILTER	, "(&(objectClass=user)(|(sAMAccountName=%loginOrMail%)(mail=%loginOrMail%)))");
//		params.put(LDAP_SEARCH_FILTER	, "(&(objectClass=user)(sAMAccountName=srv_livecom))");
		
		TesteLDAP l = new TesteLDAP(params);
//		LDAPUser r = l.login("srv_livecom","q$rR9Zy6f8NY");
		LDAPUser r = l.login("SRV_LiveCom_ahshdaishdia", "q$rR9Zay6f8NY");
		
		if(r == null) {
			System.out.println("Not Found.");
		} else {
			System.out.println(r);
			System.out.println(r.getName());
		}
		
		System.out.println("Fim");
	}
}
