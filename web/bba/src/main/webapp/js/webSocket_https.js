//	var wsocket;
//	var serviceLocation = "wss://" + window.location.host + "/livecom/chat/";
	
	function onMessageReceived(evt) {
		var msg = JSON.parse(evt.data);
		msgContent = msg.content;
		
		if (msgContent == undefined) {
			var msgNotificacao = msg.notificacao;
			exibeNotificacao(msgNotificacao);
			
//			{"notificacao": {
//				  "tipo": "like",
//				  "userId": 29,
//				  "postId": 2969,
//				  "title": "[Guilherme Politta] curtiu seu post [Teste foto]",
//				  "timestamp": 0,
//				  "count": {
//				    "countMensagens": 5,
//				    "countNotificacoes": 4,
//				    "countNovosPosts": 0
//				  }
//				}}
			
		} else {
		
			var messageLine = 'recebeu code ' + msg.code;

			if (msg.code == "serverReceivedMessage") {
				//console.log("serverReceivedMessage");
				atualizaMsgChat(msg);
			
				//Marcar mensagem enviada como recebida pelo servidor.
				//msgContent.identifier é um identificador qualquer que vc manda ao enviar uma msg e é respondido aqui
				//assim voce consegue saber qual foi a msg que foi recebida pelo servidor
			} else if (msg.code == "receivedMessage") {
				//Recebeu uma msg e tem que mostrar na tela
				atualizaMsgChat(msg);
				//console.log("recebeu msg: " + msgContent.msg);
				
				//manda confirmacao de recebimento da msg
				var obj = new Object();
				 obj.code = "messageReachDestination";
	
				 var content = new Object();
				 content.conversationID = msgContent.conversationID;
				 content.messageID = msgContent.messageID;
				 content.fromUserID = msgContent.fromUserID;
				 
				 //se tiver algum erro voltar nOk
				 content.status = "Ok";
				 obj.content = content;
				 var jsonString= JSON.stringify(obj);
				 wsocket.send(jsonString);
			} else if (msg.code == "messageReachDestination") {
				//marca a msg com lida
				//msgContent.messageID
				atualizaMsgChat(msg);
				//console.log("msg lida!");
			} else if (msg.code = "typingStatus") {
				//console.log("recebendo");
				atualizaMsgChat(msg);
				
	//			alert("typingStatus");
				//usuario esta digitando ou parou de digitar
				
				//msgContent.conversationID é a conversa onde esta sendo digitado
				//msgContent.fromUserID quem esta digitando
				//msgContent.status é um boleano que diz se esta digitando ou nao
			}
		}
		
	}
	
	function sendTypingStatus(conversationID, fromUserID, fromUserName, status){
		var obj = new Object();
		obj.code = "typingStatus";
	
		var content = new Object();
		content.conversationID = conversationID;
		content.fromUserName = fromUserName;
		content.fromUserID = fromUserID;
		content.status = status;
		
		obj.content = content;
		var jsonString= JSON.stringify(obj);
		wsocket.send(jsonString);
	}
	
	function sendMessage(campo, texto, id) {
		//selecionada campo de msg
		 var obj = new Object();
		 obj.code = "sendMessage";
		 var content = new Object();
		 
		 //identificador é um numero qualquer que vc vai receber de volta quando a msg chegar no servidor 
		 content.identifier = id;
		 
		 //preencher essas informacoes adequadamente 
		 content.conversationID = jQuery.trim(campo.attr("data-conversa"));
		 content.fromUserID = jQuery.trim(campo.attr("data-from"));
		 content.toUserID = jQuery.trim(campo.attr("data-to"));
		 
		 //msg em si
		 content.msg = texto;
		 obj.content = content;

		 var jsonString= JSON.stringify(obj);
		 wsocket.send(jsonString);
	}

	function connectToChatserver() {
		wsocket = new WebSocket(serviceLocation+userId);
		wsocket.onmessage = onMessageReceived;
	}

	function leaveRoom() {
		wsocket.close();
	}
	
	
	