var httpUtils = {
		oauth: {
			credentials: {
				consumerSecret: null,
				consumerKey: null,
				oauthToken: null,
				oauthTokenSecret: null,
			},
			post: function(url, data) {
				return execAjax(url, 'POST', data, this.credentials);
			},
			get: function(url) {
				return execAjax(url, 'GET', null, this.credentials);
			},
			put: function(url, data) {
				return execAjax(url, 'PUT', data, this.credentials);
			},
			delete: function(url) {
				return execAjax(url, 'DELETE', null, this.credentials);
			}
		}
};

function execAjax(url, method, data, credentials) {
	var contentAjax = {
		url: url,
		dataType: "json",
		contentType: "application/json",
		type: method
	}
	
	if(data) {
		contentAjax.data = data;
	}
	
	contentAjax.headers = {
		"Accept": "application/json",
	};

	if(credentials) {
		contentAjax.headers.Authorization = authorizationFlow(credentials, url, method);
	}
	return $.ajax(contentAjax);
};

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function authorizationFlow(credentials, url, method) {
	if(window.location.origin.indexOf("http://livecom.livetouchdev.com.br") > -1) {
		url = "http://localhost:8080" +  url;
	} else {
		url = window.location.origin + url;
	}
	
	if (!credentials.consumerSecret) {
		console.error("Consumer Secret não pode ser nula");
		return;
	}

	if (!credentials.consumerKey) {
		console.error("Consumer Key não pode ser nula");
		return;
	}

	var accessor = {
		consumerSecret : credentials.consumerSecret,
		tokenSecret : credentials.oauthTokenSecret
	};

	var parameters = [];
	parameters.push([ 'oauth_consumer_key', credentials.consumerKey ]);
	parameters.push([ 'oauth_consumer_secret', credentials.consumerSecret ]);
	parameters.push([ 'oauth_signature_method', 'HMAC-SHA1' ]);
	parameters.push([ 'oauth_version', '1.0' ]);
	parameters.push([ 'oauth_token_secret', credentials.oauthTokenSecret ]);
	parameters.push([ 'oauth_token', credentials.oauthToken ]);

	var message = {
		'method' : method,
		'parameters' : parameters,
		'action' : url
	};

	OAuth.setTimestampAndNonce(message);
	OAuth.SignatureMethod.sign(message, accessor);
	var parameterMap = OAuth.getParameterMap(message.parameters);
	parameterMap.oauth_signature = OAuth
			.percentEncode(parameterMap.oauth_signature);

	var authorization = 'OAuth oauth_signature="'
			+ parameterMap.oauth_signature + '"' + ', oauth_version="1.0"'
			+ ', oauth_nonce="' + parameterMap.oauth_nonce + '"'
			+ ', oauth_signature_method="'
			+ parameterMap.oauth_signature_method + '"'
			+ ', oauth_consumer_key="' + credentials.consumerKey + '"'
			+ ', oauth_consumer_secret="' + credentials.consumerSecret + '"'
			+ ', oauth_timestamp="' + parameterMap.oauth_timestamp + '"'
			+ ', oauth_token_secret="' + credentials.oauthTokenSecret + '"'
			+ ', oauth_token="' + credentials.oauthToken + '"';
	
	return authorization;
}


var OAuth;
if (OAuth == null)
	OAuth = {};

OAuth.setProperties = function setProperties(into, from) {
	if (into != null && from != null) {
		for ( var key in from) {
			into[key] = from[key];
		}
	}
	return into;
}

OAuth
		.setProperties(
				OAuth, // utility functions
				{
					percentEncode : function percentEncode(s) {
						if (s == null) {
							return "";
						}
						if (s instanceof Array) {
							var e = "";
							for (var i = 0; i < s.length; ++s) {
								if (e != "")
									e += '&';
								e += OAuth.percentEncode(s[i]);
							}
							return e;
						}
						s = encodeURIComponent(s);
						// Now replace the values which encodeURIComponent
						// doesn't do
						// encodeURIComponent ignores: - _ . ! ~ * ' ( )
						// OAuth dictates the only ones you can ignore are: - _
						// . ~
						// Source:
						// http://developer.mozilla.org/en/docs/Core_JavaScript_1.5_Reference:Global_Functions:encodeURIComponent
						s = s.replace(/\!/g, "%21");
						s = s.replace(/\*/g, "%2A");
						s = s.replace(/\'/g, "%27");
						s = s.replace(/\(/g, "%28");
						s = s.replace(/\)/g, "%29");
						return s;
					},
					decodePercent : function decodePercent(s) {
						if (s != null) {
							// Handle application/x-www-form-urlencoded, which
							// is defined by
							// http://www.w3.org/TR/html4/interact/forms.html#h-17.13.4.1
							s = s.replace(/\+/g, " ");
						}
						return decodeURIComponent(s);
					},
					/**
					 * Convert the given parameters to an Array of name-value
					 * pairs.
					 */
					getParameterList : function getParameterList(parameters) {
						if (parameters == null) {
							return [];
						}
						if (typeof parameters != "object") {
							return OAuth.decodeForm(parameters + "");
						}
						if (parameters instanceof Array) {
							return parameters;
						}
						var list = [];
						for ( var p in parameters) {
							list.push([ p, parameters[p] ]);
						}
						return list;
					},
					/** Convert the given parameters to a map from name to value. */
					getParameterMap : function getParameterMap(parameters) {
						if (parameters == null) {
							return {};
						}
						if (typeof parameters != "object") {
							return OAuth.getParameterMap(OAuth
									.decodeForm(parameters + ""));
						}
						if (parameters instanceof Array) {
							var map = {};
							for (var p = 0; p < parameters.length; ++p) {
								var key = parameters[p][0];
								if (map[key] === undefined) { // first value
																// wins
									map[key] = parameters[p][1];
								}
							}
							return map;
						}
						return parameters;
					},
					getParameter : function getParameter(parameters, name) {
						if (parameters instanceof Array) {
							for (var p = 0; p < parameters.length; ++p) {
								if (parameters[p][0] == name) {
									return parameters[p][1]; // first value
																// wins
								}
							}
						} else {
							return OAuth.getParameterMap(parameters)[name];
						}
						return null;
					},
					formEncode : function formEncode(parameters) {
						var form = "";
						var list = OAuth.getParameterList(parameters);
						for (var p = 0; p < list.length; ++p) {
							var value = list[p][1];
							if (value == null)
								value = "";
							if (form != "")
								form += '&';
							form += OAuth.percentEncode(list[p][0]) + '='
									+ OAuth.percentEncode(value);
						}
						return form;
					},
					decodeForm : function decodeForm(form) {
						var list = [];
						var nvps = form.split('&');
						for (var n = 0; n < nvps.length; ++n) {
							var nvp = nvps[n];
							if (nvp == "") {
								continue;
							}
							var equals = nvp.indexOf('=');
							var name;
							var value;
							if (equals < 0) {
								name = OAuth.decodePercent(nvp);
								value = null;
							} else {
								name = OAuth.decodePercent(nvp.substring(0,
										equals));
								value = OAuth.decodePercent(nvp
										.substring(equals + 1));
							}
							list.push([ name, value ]);
						}
						return list;
					},
					setParameter : function setParameter(message, name, value) {
						var parameters = message.parameters;
						if (parameters instanceof Array) {
							for (var p = 0; p < parameters.length; ++p) {
								if (parameters[p][0] == name) {
									if (value === undefined) {
										parameters.splice(p, 1);
									} else {
										parameters[p][1] = value;
										value = undefined;
									}
								}
							}
							if (value !== undefined) {
								parameters.push([ name, value ]);
							}
						} else {
							parameters = OAuth.getParameterMap(parameters);
							parameters[name] = value;
							message.parameters = parameters;
						}
					},
					setParameters : function setParameters(message, parameters) {
						var list = OAuth.getParameterList(parameters);
						for (var i = 0; i < list.length; ++i) {
							OAuth.setParameter(message, list[i][0], list[i][1]);
						}
					},
					/**
					 * Fill in parameters to help construct a request message.
					 * This function doesn't fill in every parameter. The
					 * accessor object should be like: {consumerKey:'foo',
					 * consumerSecret:'bar', accessorSecret:'nurn',
					 * token:'krelm', tokenSecret:'blah'} The accessorSecret
					 * property is optional.
					 */
					completeRequest : function completeRequest(message,
							accessor) {
						if (message.method == null) {
							message.method = "GET";
						}
						var map = OAuth.getParameterMap(message.parameters);
						if (map.oauth_consumer_key == null) {
							OAuth.setParameter(message, "oauth_consumer_key",
									accessor.consumerKey || "");
						}
						if (map.oauth_token == null && accessor.token != null) {
							OAuth.setParameter(message, "oauth_token",
									accessor.token);
						}
						if (map.oauth_version == null) {
							OAuth.setParameter(message, "oauth_version", "1.0");
						}
						if (map.oauth_timestamp == null) {
							OAuth.setParameter(message, "oauth_timestamp",
									OAuth.timestamp());
						}
						if (map.oauth_nonce == null) {
							OAuth.setParameter(message, "oauth_nonce", OAuth
									.nonce(6));
						}
						OAuth.SignatureMethod.sign(message, accessor);
					},
					setTimestampAndNonce : function setTimestampAndNonce(
							message) {
						OAuth.setParameter(message, "oauth_timestamp", OAuth
								.timestamp());
						OAuth.setParameter(message, "oauth_nonce", OAuth
								.nonce(6));
					},
					addToURL : function addToURL(url, parameters) {
						newURL = url;
						if (parameters != null) {
							var toAdd = OAuth.formEncode(parameters);
							if (toAdd.length > 0) {
								var q = url.indexOf('?');
								if (q < 0)
									newURL += '?';
								else
									newURL += '&';
								newURL += toAdd;
							}
						}
						return newURL;
					},
					/**
					 * Construct the value of the Authorization header for an
					 * HTTP request.
					 */
					getAuthorizationHeader : function getAuthorizationHeader(
							realm, parameters) {
						var header = 'OAuth realm="'
								+ OAuth.percentEncode(realm) + '"';
						var list = OAuth.getParameterList(parameters);
						for (var p = 0; p < list.length; ++p) {
							var parameter = list[p];
							var name = parameter[0];
							if (name.indexOf("oauth_") == 0) {
								header += ',' + OAuth.percentEncode(name)
										+ '="'
										+ OAuth.percentEncode(parameter[1])
										+ '"';
							}
						}
						return header;
					},
					/**
					 * Correct the time using a parameter from the URL from
					 * which the last script was loaded.
					 */
					correctTimestampFromSrc : function correctTimestampFromSrc(
							parameterName) {
						parameterName = parameterName || "oauth_timestamp";
						var scripts = document.getElementsByTagName('script');
						if (scripts == null || !scripts.length)
							return;
						var src = scripts[scripts.length - 1].src;
						if (!src)
							return;
						var q = src.indexOf("?");
						if (q < 0)
							return;
						parameters = OAuth.getParameterMap(OAuth.decodeForm(src
								.substring(q + 1)));
						var t = parameters[parameterName];
						if (t == null)
							return;
						OAuth.correctTimestamp(t);
					},
					/** Generate timestamps starting with the given value. */
					correctTimestamp : function correctTimestamp(timestamp) {
						console.log(new Date());
						console.log(timestamp);
						OAuth.timeCorrectionMsec = (timestamp * 1000)
								- (new Date()).getTime();
					},
					/** The difference between the correct time and my clock. */
					timeCorrectionMsec : 0,
					timestamp : function timestamp() {
						var t = (new Date()).getTime()
								+ OAuth.timeCorrectionMsec;
						return Math.floor(t / 1000);
					},
					nonce : function nonce(length) {
						var chars = OAuth.nonce.CHARS;
						var result = "";
						for (var i = 0; i < length; ++i) {
							var rnum = Math.floor(Math.random() * chars.length);
							result += chars.substring(rnum, rnum + 1);
						}
						return result;
					}
				});

OAuth.nonce.CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";

/**
 * Define a constructor function, without causing trouble to anyone who was
 * using it as a namespace. That is, if parent[name] already existed and had
 * properties, copy those properties into the new constructor.
 */
OAuth.declareClass = function declareClass(parent, name, newConstructor) {
	var previous = parent[name];
	parent[name] = newConstructor;
	if (newConstructor != null && previous != null) {
		for ( var key in previous) {
			if (key != "prototype") {
				newConstructor[key] = previous[key];
			}
		}
	}
	return newConstructor;
}

/** An abstract algorithm for signing messages. */
OAuth.declareClass(OAuth, "SignatureMethod", function OAuthSignatureMethod() {
});

OAuth.setProperties(OAuth.SignatureMethod.prototype, // instance members
{
	/** Add a signature to the message. */
	sign : function sign(message) {
		var baseString = OAuth.SignatureMethod.getBaseString(message);
		var signature = this.getSignature(baseString);
		OAuth.setParameter(message, "oauth_signature", signature);
		return signature; // just in case someone's interested
	},
	/** Set the key string for signing. */
	initialize : function initialize(name, accessor) {
		var consumerSecret;
		if (accessor.accessorSecret != null && name.length > 9
				&& name.substring(name.length - 9) == "-Accessor") {
			consumerSecret = accessor.accessorSecret;
		} else {
			consumerSecret = accessor.consumerSecret;
		}
		this.key = OAuth.percentEncode(consumerSecret) + "&"
				+ OAuth.percentEncode(accessor.tokenSecret);
	}
});

/*
 * SignatureMethod expects an accessor object to be like this: {tokenSecret:
 * "lakjsdflkj...", consumerSecret: "QOUEWRI..", accessorSecret: "xcmvzc..."}
 * The accessorSecret property is optional.
 */
// Class members:
OAuth
		.setProperties(
				OAuth.SignatureMethod, // class members
				{
					sign : function sign(message, accessor) {
						var name = OAuth.getParameterMap(message.parameters).oauth_signature_method;
						if (name == null || name == "") {
							name = "HMAC-SHA1";
							OAuth.setParameter(message,
									"oauth_signature_method", name);
						}
						OAuth.SignatureMethod.newMethod(name, accessor).sign(
								message);
					},
					/** Instantiate a SignatureMethod for the given method name. */
					newMethod : function newMethod(name, accessor) {
						var impl = OAuth.SignatureMethod.REGISTERED[name];
						if (impl != null) {
							var method = new impl();
							method.initialize(name, accessor);
							return method;
						}
						var err = new Error("signature_method_rejected");
						var acceptable = "";
						for ( var r in OAuth.SignatureMethod.REGISTERED) {
							if (acceptable != "")
								acceptable += '&';
							acceptable += OAuth.percentEncode(r);
						}
						err.oauth_acceptable_signature_methods = acceptable;
						throw err;
					},
					/** A map from signature method name to constructor. */
					REGISTERED : {},
					/**
					 * Subsequently, the given constructor will be used for the
					 * named methods. The constructor will be called with no
					 * parameters. The resulting object should usually implement
					 * getSignature(baseString). You can easily define such a
					 * constructor by calling makeSubclass, below.
					 */
					registerMethodClass : function registerMethodClass(names,
							classConstructor) {
						for (var n = 0; n < names.length; ++n) {
							OAuth.SignatureMethod.REGISTERED[names[n]] = classConstructor;
						}
					},
					/**
					 * Create a subclass of OAuth.SignatureMethod, with the
					 * given getSignature function.
					 */
					makeSubclass : function makeSubclass(getSignatureFunction) {
						var superClass = OAuth.SignatureMethod;
						var subClass = function() {
							superClass.call(this);
						};
						subClass.prototype = new superClass();
						// Delete instance variables from prototype:
						// delete subclass.prototype... There aren't any.
						subClass.prototype.getSignature = getSignatureFunction;
						subClass.prototype.constructor = subClass;
						return subClass;
					},
					getBaseString : function getBaseString(message) {
						var URL = message.action;
						var q = URL.indexOf('?');
						var parameters;
						if (q < 0) {
							parameters = message.parameters;
						} else {
							// Combine the URL query string with the other
							// parameters:
							parameters = OAuth.decodeForm(URL.substring(q + 1));
							var toAdd = OAuth
									.getParameterList(message.parameters);
							for (var a = 0; a < toAdd.length; ++a) {
								parameters.push(toAdd[a]);
							}
						}
						return OAuth
								.percentEncode(message.method.toUpperCase())
								+ '&'
								+ OAuth.percentEncode(OAuth.SignatureMethod
										.normalizeUrl(URL))
								+ '&'
								+ OAuth.percentEncode(OAuth.SignatureMethod
										.normalizeParameters(parameters));
					},
					normalizeUrl : function normalizeUrl(url) {
						var uri = OAuth.SignatureMethod.parseUri(url);
						var scheme = uri.protocol.toLowerCase();
						var authority = uri.authority.toLowerCase();
						var dropPort = (scheme == "http" && uri.port == 80)
								|| (scheme == "https" && uri.port == 443);
						if (dropPort) {
							// find the last : in the authority
							var index = authority.lastIndexOf(":");
							if (index >= 0) {
								authority = authority.substring(0, index);
							}
						}
						var path = uri.path;
						if (!path) {
							path = "/"; // conforms to RFC 2616 section 3.2.2
						}
						// we know that there is no query and no fragment here.
						return scheme + "://" + authority + path;
					},
					parseUri : function parseUri(str) {
						/*
						 * This function was adapted from parseUri 1.2.1
						 * http://stevenlevithan.com/demo/parseuri/js/assets/parseuri.js
						 */
						var o = {
							key : [ "source", "protocol", "authority",
									"userInfo", "user", "password", "host",
									"port", "relative", "path", "directory",
									"file", "query", "anchor" ],
							parser : {
								strict : /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@\/]*):?([^:@\/]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/
							}
						};
						var m = o.parser.strict.exec(str);
						var uri = {};
						var i = 14;
						while (i--)
							uri[o.key[i]] = m[i] || "";
						return uri;
					},
					normalizeParameters : function normalizeParameters(
							parameters) {
						if (parameters == null) {
							return "";
						}
						var list = OAuth.getParameterList(parameters);
						var sortable = [];
						for (var p = 0; p < list.length; ++p) {
							var nvp = list[p];
							if (nvp[0] != "oauth_signature") {
								sortable.push([
										OAuth.percentEncode(nvp[0]) + " " // because
																			// it
																			// comes
																			// before
																			// any
																			// character
																			// that
																			// can
																			// appear
																			// in a
																			// percentEncoded
																			// string.
												+ OAuth.percentEncode(nvp[1]),
										nvp ]);
							}
						}
						sortable.sort(function(a, b) {
							if (a[0] < b[0])
								return -1;
							if (a[0] > b[0])
								return 1;
							return 0;
						});
						var sorted = [];
						for (var s = 0; s < sortable.length; ++s) {
							sorted.push(sortable[s][1]);
						}
						return OAuth.formEncode(sorted);
					}
				});

OAuth.SignatureMethod.registerMethodClass(
		[ "PLAINTEXT", "PLAINTEXT-Accessor" ], OAuth.SignatureMethod
				.makeSubclass(function getSignature(baseString) {
					return this.key;
				}));

OAuth.SignatureMethod.registerMethodClass(
		[ "HMAC-SHA1", "HMAC-SHA1-Accessor" ], OAuth.SignatureMethod
				.makeSubclass(function getSignature(baseString) {
					b64pad = '=';
					var signature = b64_hmac_sha1(this.key, baseString);
					return signature;
				}));

try {
	OAuth.correctTimestampFromSrc();
} catch (e) {
}

/*
 * A JavaScript implementation of the Secure Hash Algorithm, SHA-1, as defined
 * in FIPS PUB 180-1
 * Version 2.1a Copyright Paul Johnston 2000 - 2002.
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for details.
 */

/*
 * Configurable variables. You may need to tweak these to be compatible with
 * the server-side, but the defaults work in most cases.
 */
var hexcase = 0;  /* hex output format. 0 - lowercase; 1 - uppercase        */
var b64pad  = ""; /* base-64 pad character. "=" for strict RFC compliance   */
var chrsz   = 8;  /* bits per input character. 8 - ASCII; 16 - Unicode      */

/*
 * These are the functions you'll usually want to call
 * They take string arguments and return either hex or base-64 encoded strings
 */
function hex_sha1(s){return binb2hex(core_sha1(str2binb(s),s.length * chrsz));}
function b64_sha1(s){return binb2b64(core_sha1(str2binb(s),s.length * chrsz));}
function str_sha1(s){return binb2str(core_sha1(str2binb(s),s.length * chrsz));}
function hex_hmac_sha1(key, data){ return binb2hex(core_hmac_sha1(key, data));}
function b64_hmac_sha1(key, data){ return binb2b64(core_hmac_sha1(key, data));}
function str_hmac_sha1(key, data){ return binb2str(core_hmac_sha1(key, data));}

/*
 * Perform a simple self-test to see if the VM is working
 */
function sha1_vm_test()
{
  return hex_sha1("abc") == "a9993e364706816aba3e25717850c26c9cd0d89d";
}

/*
 * Calculate the SHA-1 of an array of big-endian words, and a bit length
 */
function core_sha1(x, len)
{
  /* append padding */
  x[len >> 5] |= 0x80 << (24 - len % 32);
  x[((len + 64 >> 9) << 4) + 15] = len;

  var w = Array(80);
  var a =  1732584193;
  var b = -271733879;
  var c = -1732584194;
  var d =  271733878;
  var e = -1009589776;

  for(var i = 0; i < x.length; i += 16)
  {
    var olda = a;
    var oldb = b;
    var oldc = c;
    var oldd = d;
    var olde = e;

    for(var j = 0; j < 80; j++)
    {
      if(j < 16) w[j] = x[i + j];
      else w[j] = rol(w[j-3] ^ w[j-8] ^ w[j-14] ^ w[j-16], 1);
      var t = safe_add(safe_add(rol(a, 5), sha1_ft(j, b, c, d)),
                       safe_add(safe_add(e, w[j]), sha1_kt(j)));
      e = d;
      d = c;
      c = rol(b, 30);
      b = a;
      a = t;
    }

    a = safe_add(a, olda);
    b = safe_add(b, oldb);
    c = safe_add(c, oldc);
    d = safe_add(d, oldd);
    e = safe_add(e, olde);
  }
  return Array(a, b, c, d, e);

}

/*
 * Perform the appropriate triplet combination function for the current
 * iteration
 */
function sha1_ft(t, b, c, d)
{
  if(t < 20) return (b & c) | ((~b) & d);
  if(t < 40) return b ^ c ^ d;
  if(t < 60) return (b & c) | (b & d) | (c & d);
  return b ^ c ^ d;
}

/*
 * Determine the appropriate additive constant for the current iteration
 */
function sha1_kt(t)
{
  return (t < 20) ?  1518500249 : (t < 40) ?  1859775393 :
         (t < 60) ? -1894007588 : -899497514;
}

/*
 * Calculate the HMAC-SHA1 of a key and some data
 */
function core_hmac_sha1(key, data)
{
  var bkey = str2binb(key);
  if(bkey.length > 16) bkey = core_sha1(bkey, key.length * chrsz);

  var ipad = Array(16), opad = Array(16);
  for(var i = 0; i < 16; i++)
  {
    ipad[i] = bkey[i] ^ 0x36363636;
    opad[i] = bkey[i] ^ 0x5C5C5C5C;
  }

  var hash = core_sha1(ipad.concat(str2binb(data)), 512 + data.length * chrsz);
  return core_sha1(opad.concat(hash), 512 + 160);
}

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
function safe_add(x, y)
{
  var lsw = (x & 0xFFFF) + (y & 0xFFFF);
  var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
  return (msw << 16) | (lsw & 0xFFFF);
}

/*
 * Bitwise rotate a 32-bit number to the left.
 */
function rol(num, cnt)
{
  return (num << cnt) | (num >>> (32 - cnt));
}

/*
 * Convert an 8-bit or 16-bit string to an array of big-endian words
 * In 8-bit function, characters >255 have their hi-byte silently ignored.
 */
function str2binb(str)
{
  var bin = Array();
  var mask = (1 << chrsz) - 1;
  for(var i = 0; i < str.length * chrsz; i += chrsz)
    bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (32 - chrsz - i%32);
  return bin;
}

/*
 * Convert an array of big-endian words to a string
 */
function binb2str(bin)
{
  var str = "";
  var mask = (1 << chrsz) - 1;
  for(var i = 0; i < bin.length * 32; i += chrsz)
    str += String.fromCharCode((bin[i>>5] >>> (32 - chrsz - i%32)) & mask);
  return str;
}

/*
 * Convert an array of big-endian words to a hex string.
 */
function binb2hex(binarray)
{
  var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
  var str = "";
  for(var i = 0; i < binarray.length * 4; i++)
  {
    str += hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8+4)) & 0xF) +
           hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8  )) & 0xF);
  }
  return str;
}

/*
 * Convert an array of big-endian words to a base-64 string
 */
function binb2b64(binarray)
{
  var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  var str = "";
  for(var i = 0; i < binarray.length * 4; i += 3)
  {
    var triplet = (((binarray[i   >> 2] >> 8 * (3 -  i   %4)) & 0xFF) << 16)
                | (((binarray[i+1 >> 2] >> 8 * (3 - (i+1)%4)) & 0xFF) << 8 )
                |  ((binarray[i+2 >> 2] >> 8 * (3 - (i+2)%4)) & 0xFF);
    for(var j = 0; j < 4; j++)
    {
      if(i * 8 + j * 6 > binarray.length * 32) str += b64pad;
      else str += tab.charAt((triplet >> 6*(3-j)) & 0x3F);
    }
  }
  return str;
}


/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
var CryptoJS=CryptoJS||function(s,p){var m={},l=m.lib={},n=function(){},r=l.Base={extend:function(b){n.prototype=this;var h=new n;b&&h.mixIn(b);h.hasOwnProperty("init")||(h.init=function(){h.$super.init.apply(this,arguments)});h.init.prototype=h;h.$super=this;return h},create:function(){var b=this.extend();b.init.apply(b,arguments);return b},init:function(){},mixIn:function(b){for(var h in b)b.hasOwnProperty(h)&&(this[h]=b[h]);b.hasOwnProperty("toString")&&(this.toString=b.toString)},clone:function(){return this.init.prototype.extend(this)}},
q=l.WordArray=r.extend({init:function(b,h){b=this.words=b||[];this.sigBytes=h!=p?h:4*b.length},toString:function(b){return(b||t).stringify(this)},concat:function(b){var h=this.words,a=b.words,j=this.sigBytes;b=b.sigBytes;this.clamp();if(j%4)for(var g=0;g<b;g++)h[j+g>>>2]|=(a[g>>>2]>>>24-8*(g%4)&255)<<24-8*((j+g)%4);else if(65535<a.length)for(g=0;g<b;g+=4)h[j+g>>>2]=a[g>>>2];else h.push.apply(h,a);this.sigBytes+=b;return this},clamp:function(){var b=this.words,h=this.sigBytes;b[h>>>2]&=4294967295<<
32-8*(h%4);b.length=s.ceil(h/4)},clone:function(){var b=r.clone.call(this);b.words=this.words.slice(0);return b},random:function(b){for(var h=[],a=0;a<b;a+=4)h.push(4294967296*s.random()|0);return new q.init(h,b)}}),v=m.enc={},t=v.Hex={stringify:function(b){var a=b.words;b=b.sigBytes;for(var g=[],j=0;j<b;j++){var k=a[j>>>2]>>>24-8*(j%4)&255;g.push((k>>>4).toString(16));g.push((k&15).toString(16))}return g.join("")},parse:function(b){for(var a=b.length,g=[],j=0;j<a;j+=2)g[j>>>3]|=parseInt(b.substr(j,
2),16)<<24-4*(j%8);return new q.init(g,a/2)}},a=v.Latin1={stringify:function(b){var a=b.words;b=b.sigBytes;for(var g=[],j=0;j<b;j++)g.push(String.fromCharCode(a[j>>>2]>>>24-8*(j%4)&255));return g.join("")},parse:function(b){for(var a=b.length,g=[],j=0;j<a;j++)g[j>>>2]|=(b.charCodeAt(j)&255)<<24-8*(j%4);return new q.init(g,a)}},u=v.Utf8={stringify:function(b){try{return decodeURIComponent(escape(a.stringify(b)))}catch(g){throw Error("Malformed UTF-8 data");}},parse:function(b){return a.parse(unescape(encodeURIComponent(b)))}},
g=l.BufferedBlockAlgorithm=r.extend({reset:function(){this._data=new q.init;this._nDataBytes=0},_append:function(b){"string"==typeof b&&(b=u.parse(b));this._data.concat(b);this._nDataBytes+=b.sigBytes},_process:function(b){var a=this._data,g=a.words,j=a.sigBytes,k=this.blockSize,m=j/(4*k),m=b?s.ceil(m):s.max((m|0)-this._minBufferSize,0);b=m*k;j=s.min(4*b,j);if(b){for(var l=0;l<b;l+=k)this._doProcessBlock(g,l);l=g.splice(0,b);a.sigBytes-=j}return new q.init(l,j)},clone:function(){var b=r.clone.call(this);
b._data=this._data.clone();return b},_minBufferSize:0});l.Hasher=g.extend({cfg:r.extend(),init:function(b){this.cfg=this.cfg.extend(b);this.reset()},reset:function(){g.reset.call(this);this._doReset()},update:function(b){this._append(b);this._process();return this},finalize:function(b){b&&this._append(b);return this._doFinalize()},blockSize:16,_createHelper:function(b){return function(a,g){return(new b.init(g)).finalize(a)}},_createHmacHelper:function(b){return function(a,g){return(new k.HMAC.init(b,
g)).finalize(a)}}});var k=m.algo={};return m}(Math);
(function(s){function p(a,k,b,h,l,j,m){a=a+(k&b|~k&h)+l+m;return(a<<j|a>>>32-j)+k}function m(a,k,b,h,l,j,m){a=a+(k&h|b&~h)+l+m;return(a<<j|a>>>32-j)+k}function l(a,k,b,h,l,j,m){a=a+(k^b^h)+l+m;return(a<<j|a>>>32-j)+k}function n(a,k,b,h,l,j,m){a=a+(b^(k|~h))+l+m;return(a<<j|a>>>32-j)+k}for(var r=CryptoJS,q=r.lib,v=q.WordArray,t=q.Hasher,q=r.algo,a=[],u=0;64>u;u++)a[u]=4294967296*s.abs(s.sin(u+1))|0;q=q.MD5=t.extend({_doReset:function(){this._hash=new v.init([1732584193,4023233417,2562383102,271733878])},
_doProcessBlock:function(g,k){for(var b=0;16>b;b++){var h=k+b,w=g[h];g[h]=(w<<8|w>>>24)&16711935|(w<<24|w>>>8)&4278255360}var b=this._hash.words,h=g[k+0],w=g[k+1],j=g[k+2],q=g[k+3],r=g[k+4],s=g[k+5],t=g[k+6],u=g[k+7],v=g[k+8],x=g[k+9],y=g[k+10],z=g[k+11],A=g[k+12],B=g[k+13],C=g[k+14],D=g[k+15],c=b[0],d=b[1],e=b[2],f=b[3],c=p(c,d,e,f,h,7,a[0]),f=p(f,c,d,e,w,12,a[1]),e=p(e,f,c,d,j,17,a[2]),d=p(d,e,f,c,q,22,a[3]),c=p(c,d,e,f,r,7,a[4]),f=p(f,c,d,e,s,12,a[5]),e=p(e,f,c,d,t,17,a[6]),d=p(d,e,f,c,u,22,a[7]),
c=p(c,d,e,f,v,7,a[8]),f=p(f,c,d,e,x,12,a[9]),e=p(e,f,c,d,y,17,a[10]),d=p(d,e,f,c,z,22,a[11]),c=p(c,d,e,f,A,7,a[12]),f=p(f,c,d,e,B,12,a[13]),e=p(e,f,c,d,C,17,a[14]),d=p(d,e,f,c,D,22,a[15]),c=m(c,d,e,f,w,5,a[16]),f=m(f,c,d,e,t,9,a[17]),e=m(e,f,c,d,z,14,a[18]),d=m(d,e,f,c,h,20,a[19]),c=m(c,d,e,f,s,5,a[20]),f=m(f,c,d,e,y,9,a[21]),e=m(e,f,c,d,D,14,a[22]),d=m(d,e,f,c,r,20,a[23]),c=m(c,d,e,f,x,5,a[24]),f=m(f,c,d,e,C,9,a[25]),e=m(e,f,c,d,q,14,a[26]),d=m(d,e,f,c,v,20,a[27]),c=m(c,d,e,f,B,5,a[28]),f=m(f,c,
d,e,j,9,a[29]),e=m(e,f,c,d,u,14,a[30]),d=m(d,e,f,c,A,20,a[31]),c=l(c,d,e,f,s,4,a[32]),f=l(f,c,d,e,v,11,a[33]),e=l(e,f,c,d,z,16,a[34]),d=l(d,e,f,c,C,23,a[35]),c=l(c,d,e,f,w,4,a[36]),f=l(f,c,d,e,r,11,a[37]),e=l(e,f,c,d,u,16,a[38]),d=l(d,e,f,c,y,23,a[39]),c=l(c,d,e,f,B,4,a[40]),f=l(f,c,d,e,h,11,a[41]),e=l(e,f,c,d,q,16,a[42]),d=l(d,e,f,c,t,23,a[43]),c=l(c,d,e,f,x,4,a[44]),f=l(f,c,d,e,A,11,a[45]),e=l(e,f,c,d,D,16,a[46]),d=l(d,e,f,c,j,23,a[47]),c=n(c,d,e,f,h,6,a[48]),f=n(f,c,d,e,u,10,a[49]),e=n(e,f,c,d,
C,15,a[50]),d=n(d,e,f,c,s,21,a[51]),c=n(c,d,e,f,A,6,a[52]),f=n(f,c,d,e,q,10,a[53]),e=n(e,f,c,d,y,15,a[54]),d=n(d,e,f,c,w,21,a[55]),c=n(c,d,e,f,v,6,a[56]),f=n(f,c,d,e,D,10,a[57]),e=n(e,f,c,d,t,15,a[58]),d=n(d,e,f,c,B,21,a[59]),c=n(c,d,e,f,r,6,a[60]),f=n(f,c,d,e,z,10,a[61]),e=n(e,f,c,d,j,15,a[62]),d=n(d,e,f,c,x,21,a[63]);b[0]=b[0]+c|0;b[1]=b[1]+d|0;b[2]=b[2]+e|0;b[3]=b[3]+f|0},_doFinalize:function(){var a=this._data,k=a.words,b=8*this._nDataBytes,h=8*a.sigBytes;k[h>>>5]|=128<<24-h%32;var l=s.floor(b/
4294967296);k[(h+64>>>9<<4)+15]=(l<<8|l>>>24)&16711935|(l<<24|l>>>8)&4278255360;k[(h+64>>>9<<4)+14]=(b<<8|b>>>24)&16711935|(b<<24|b>>>8)&4278255360;a.sigBytes=4*(k.length+1);this._process();a=this._hash;k=a.words;for(b=0;4>b;b++)h=k[b],k[b]=(h<<8|h>>>24)&16711935|(h<<24|h>>>8)&4278255360;return a},clone:function(){var a=t.clone.call(this);a._hash=this._hash.clone();return a}});r.MD5=t._createHelper(q);r.HmacMD5=t._createHmacHelper(q)})(Math);


/*
Copyright (c) 2009 James Padolsey.  All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

   1. Redistributions of source code must retain the above copyright
	  notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright
	  notice, this list of conditions and the following disclaimer in the
	  documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY James Padolsey ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL James Padolsey OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of James Padolsey.

 AUTHOR James Padolsey (http://james.padolsey.com)
 VERSION 1.03.0
 UPDATED 29-10-2011
 CONTRIBUTORS
	David Waller
    Benjamin Drucker

*/

var prettyPrint = (function(){
	
	/* These "util" functions are not part of the core
	   functionality but are  all necessary - mostly DOM helpers */
	
	var util = {
		
		el: function(type, attrs) {
			
			/* Create new element */
			var el = document.createElement(type), attr;
			
			/*Copy to single object */
			attrs = util.merge({}, attrs);
			
			/* Add attributes to el */
			if (attrs && attrs.style) {
				var styles = attrs.style;
				util.applyCSS( el, attrs.style );
				delete attrs.style;
			}
			for (attr in attrs) {
				if (attrs.hasOwnProperty(attr)) {
					el[attr] = attrs[attr];
				}
			}
			
			return el;
		
		},
		
		applyCSS: function(el, styles) {
			/* Applies CSS to a single element */
			for (var prop in styles) {
				if (styles.hasOwnProperty(prop)) {
					try{
						/* Yes, IE6 SUCKS! */
						el.style[prop] = styles[prop];
					}catch(e){}
				}
			}
		},
		
		txt: function(t) {
			/* Create text node */
			return document.createTextNode(t);
		},
		
		row: function(cells, type, cellType) {
			
			/* Creates new <tr> */
			cellType = cellType || 'td';
			
			/* colSpan is calculated by length of null items in array */
			var colSpan = util.count(cells, null) + 1,
				tr = util.el('tr'), td,
				attrs = {
					style: util.getStyles(cellType, type),
					colSpan: colSpan,
					onmouseover: function() {
						var tds = this.parentNode.childNodes;
						util.forEach(tds, function(cell){
							if (cell.nodeName.toLowerCase() !== 'td') { return; }
							util.applyCSS(cell, util.getStyles('td_hover', type));
						});
					},
					onmouseout: function() {
						var tds = this.parentNode.childNodes;
						util.forEach(tds, function(cell){
							if (cell.nodeName.toLowerCase() !== 'td') { return; }
							util.applyCSS(cell, util.getStyles('td', type));
						});
					}
				};
				
			util.forEach(cells, function(cell){
				
				if (cell === null) { return; }
				/* Default cell type is <td> */
				td = util.el(cellType, attrs);
				
				if (cell.nodeType) {
					/* IsDomElement */
					td.appendChild(cell);
				} else {
					/* IsString */
					td.innerHTML = util.shorten(cell.toString());
				}
				
				tr.appendChild(td);
			});
			
			return tr;
		},
		
		hRow: function(cells, type){
			/* Return new <th> */
			return util.row(cells, type, 'th');
		},
		
		table: function(headings, type){
			
			headings = headings || [];
			
			/* Creates new table: */
			var attrs = {
					thead: {
						style:util.getStyles('thead',type)
					},
					tbody: {
						style:util.getStyles('tbody',type)
					},
					table: {
						style:util.getStyles('table',type)
					}
				},
				tbl = util.el('table', attrs.table),
				thead = util.el('thead', attrs.thead),
				tbody = util.el('tbody', attrs.tbody);
				
			if (headings.length) {
				tbl.appendChild(thead);
				thead.appendChild( util.hRow(headings, type) );
			}
			tbl.appendChild(tbody);
			
			return {
				/* Facade for dealing with table/tbody
				   Actual table node is this.node: */
				node: tbl,
				tbody: tbody,
				thead: thead,
				appendChild: function(node) {
					this.tbody.appendChild(node);
				},
				addRow: function(cells, _type, cellType){
					this.appendChild(util.row.call(util, cells, (_type || type), cellType));
					return this;
				}
			};
		},
		
		shorten: function(str) {
			var max = 40;
			str = str.replace(/^\s\s*|\s\s*$|\n/g,'');
			return str.length > max ? (str.substring(0, max-1) + '...') : str;
		},
		
		htmlentities: function(str) {
			return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
		},
		
		merge: function(target, source) {
			
			/* Merges two (or more) objects,
			   giving the last one precedence */
			
			if ( typeof target !== 'object' ) {
				target = {};
			}
			
			for (var property in source) {
				
				if ( source.hasOwnProperty(property) ) {
					
					var sourceProperty = source[ property ];
					
					if ( typeof sourceProperty === 'object' ) {
						target[ property ] = util.merge( target[ property ], sourceProperty );
						continue;
					}
					
					target[ property ] = sourceProperty;
					
				}
				
			}
			
			for (var a = 2, l = arguments.length; a < l; a++) {
				util.merge(target, arguments[a]);
			}
			
			return target;
		},
		
		count: function(arr, item) {
			var count = 0;
			for (var i = 0, l = arr.length; i< l; i++) {
				if (arr[i] === item) {
					count++;
				}
			}
			return count;
		},
		
		thead: function(tbl) {
			return tbl.getElementsByTagName('thead')[0];
		},
		
		forEach: function(arr, max, fn) {
			
			if (!fn) {
				fn = max;
			}

			/* Helper: iteration */
			var len = arr.length,
				index = -1;
			
			while (++index < len) {
				if(fn( arr[index], index, arr ) === false) {
					break;
				}
			}
			
			return true;
		},
		
		type: function(v){
			try {
				/* Returns type, e.g. "string", "number", "array" etc.
				   Note, this is only used for precise typing. */
				if (v === null) { return 'null'; }
				if (v === undefined) { return 'undefined'; }
				var oType = Object.prototype.toString.call(v).match(/\s(.+?)\]/)[1].toLowerCase();
				if (v.nodeType) {
					if (v.nodeType === 1) {
						return 'domelement';
					}
					return 'domnode';
				}
				if (/^(string|number|array|regexp|function|date|boolean)$/.test(oType)) {
					return oType;
				}
				if (typeof v === 'object') {
					return v.jquery && typeof v.jquery === 'string' ? 'jquery' : 'object';
				}
				if (v === window || v === document) {
					return 'object';
				}
				return 'default';
			} catch(e) {
				return 'default';
			}
		},
		
		within: function(ref) {
			/* Check existence of a val within an object
			   RETURNS KEY */
			return {
				is: function(o) {
					for (var i in ref) {
						if (ref[i] === o) {
							return i;
						}
					}
					return '';
				}
			};
		},
		
		common: {
			circRef: function(obj, key, settings) {
				return util.expander(
					'[POINTS BACK TO <strong>' + (key) + '</strong>]',
					'Click to show this item anyway',
					function() {
						this.parentNode.appendChild( prettyPrintThis(obj,{maxDepth:1}) );
					}
				);
			},
			depthReached: function(obj, settings) {
				return util.expander(
					'[DEPTH REACHED]',
					'Click to show this item anyway',
					function() {
						try {
							this.parentNode.appendChild( prettyPrintThis(obj,{maxDepth:1}) );
						} catch(e) {
							this.parentNode.appendChild(
								util.table(['ERROR OCCURED DURING OBJECT RETRIEVAL'],'error').addRow([e.message]).node   
							);
						}
					}
				);
			}
		},
		
		getStyles: function(el, type) {
			type = prettyPrintThis.settings.styles[type] || {};
			return util.merge(
				{}, prettyPrintThis.settings.styles['default'][el], type[el]
			);
		},
		
		expander: function(text, title, clickFn) {
			return util.el('a', {
				innerHTML:  util.shorten(text) + ' <b style="visibility:hidden;">[+]</b>',
				title: title,
				onmouseover: function() {
					this.getElementsByTagName('b')[0].style.visibility = 'visible';
				},
				onmouseout: function() {
					this.getElementsByTagName('b')[0].style.visibility = 'hidden';
				},
				onclick: function() {
					this.style.display = 'none';
					clickFn.call(this);
					return false;
				},
				style: {
					cursor: 'pointer'
				}
			});
		},
		
		stringify: function(obj) {
			
			/* Bit of an ugly duckling!
			   - This fn returns an ATTEMPT at converting an object/array/anyType
				 into a string, kinda like a JSON-deParser
			   - This is used for when |settings.expanded === false| */
			
			var type = util.type(obj),
				str, first = true;
			if ( type === 'array' ) {
				str = '[';
				util.forEach(obj, function(item,i){
					str += (i===0?'':', ') + util.stringify(item);
				});
				return str + ']';
			}
			if (typeof obj === 'object') {
				str = '{';
				for (var i in obj){
					if (obj.hasOwnProperty(i)) {
						str += (first?'':', ') + i + ':' + util.stringify(obj[i]);
						first = false;
					}
				}
				return str + '}';
			}
			if (type === 'regexp') {
				return '/' + obj.source + '/';
			}
			if (type === 'string') {
				return '"' + obj.replace(/"/g,'\\"') + '"';
			}
			return obj.toString();
		},
		
		headerGradient: (function(){
			
			var canvas = document.createElement('canvas');
			if (!canvas.getContext) { return ''; }
			var cx = canvas.getContext('2d');
			canvas.height = 30;
			canvas.width = 1;
			
			var linearGrad = cx.createLinearGradient(0,0,0,30);
			linearGrad.addColorStop(0,'rgba(0,0,0,0)');
			linearGrad.addColorStop(1,'rgba(0,0,0,0.25)');
			
			cx.fillStyle = linearGrad;
			cx.fillRect(0,0,1,30);
			
			var dataURL = canvas.toDataURL && canvas.toDataURL();
			return 'url(' + (dataURL || '') + ')';
		
		})()
		
	};
	
	// Main..
	var prettyPrintThis = function(obj, options) {
		
		 /*
		 *	  obj :: Object to be printed					
		 *  options :: Options (merged with config)
		 */
		
		options = options || {};
		
		var settings = util.merge( {}, prettyPrintThis.config, options ),
			container = util.el('div'),
			config = prettyPrintThis.config,
			currentDepth = 0,
			stack = {},
			hasRunOnce = false;
		
		/* Expose per-call settings.
		   Note: "config" is overwritten (where necessary) by options/"settings"
		   So, if you need to access/change *DEFAULT* settings then go via ".config" */
		prettyPrintThis.settings = settings;
		
		var typeDealer = {
			string : function(item){
				return util.txt('"' + util.shorten(item.replace(/"/g,'\\"')) + '"');
			},
			number : function(item) {
				return util.txt(item);
			},
			regexp : function(item) {
				
				var miniTable = util.table(['RegExp',null], 'regexp');
				var flags = util.table();
				var span = util.expander(
					'/' + item.source + '/',
					'Click to show more',
					function() {
						this.parentNode.appendChild(miniTable.node);
					}
				);
				
				flags
					.addRow(['g', item.global])
					.addRow(['i', item.ignoreCase])
					.addRow(['m', item.multiline]);
				
				miniTable
					.addRow(['source', '/' + item.source + '/'])
					.addRow(['flags', flags.node])
					.addRow(['lastIndex', item.lastIndex]);
					
				return settings.expanded ? miniTable.node : span;
			},
			domelement : function(element, depth) {
				
				var miniTable = util.table(['DOMElement',null], 'domelement'),
					props = ['id', 'className', 'innerHTML', 'src', 'href'], elname = element.nodeName || '';
				
				miniTable.addRow(['tag', '&lt;' + elname.toLowerCase() + '&gt;']);
					
				util.forEach(props, function(prop){
					if ( element[prop] ) {
						miniTable.addRow([ prop, util.htmlentities(element[prop]) ]);
					}
				});
				
				return settings.expanded ? miniTable.node : util.expander(
					'DOMElement (' + elname.toLowerCase() + ')',
					'Click to show more',
					function() {
						this.parentNode.appendChild(miniTable.node);
					}
				);
			},
			domnode : function(node){
				
				/* Deals with all DOMNodes that aren't elements (nodeType !== 1) */
				var miniTable = util.table(['DOMNode',null], 'domelement'),
					data =  util.htmlentities( (node.data || 'UNDEFINED').replace(/\n/g,'\\n') );
				miniTable
					.addRow(['nodeType', node.nodeType + ' (' + node.nodeName + ')'])
					.addRow(['data', data]);
				
				return settings.expanded ? miniTable.node : util.expander(
					'DOMNode',
					'Click to show more',
					function() {
						this.parentNode.appendChild(miniTable.node);
					}
				);
			},
			jquery : function(obj, depth, key) {
				return typeDealer['array'](obj, depth, key, true);
			},
			object : function(obj, depth, key) {
				
				/* Checking depth + circular refs */
				/* Note, check for circular refs before depth; just makes more sense */
				var stackKey = util.within(stack).is(obj);
				if ( stackKey ) {
					return util.common.circRef(obj, stackKey, settings);
				}
				stack[key||'TOP'] = obj;
				if (depth === settings.maxDepth) {
					return util.common.depthReached(obj, settings);
				}
				
				var table = util.table(['Object', null],'object'),
					isEmpty = true;
				
				for (var i in obj) {
					if (!obj.hasOwnProperty || obj.hasOwnProperty(i)) {
						var item = obj[i],
							type = util.type(item);
						isEmpty = false;
						try {
							table.addRow([i, typeDealer[ type ](item, depth+1, i)], type);
						} catch(e) {
							/* Security errors are thrown on certain Window/DOM properties */
							if (window.console && window.console.log) {
								console.log(e.message);
							}
						}
					}
				}
				
				if (isEmpty) {
					table.addRow(['<small>[empty]</small>']);
				} else {
					table.thead.appendChild(
						util.hRow(['key','value'], 'colHeader')
					);
				}
				
				var ret = (settings.expanded || hasRunOnce) ? table.node : util.expander(
					util.stringify(obj),
					'Click to show more',
					function() {
						this.parentNode.appendChild(table.node);
					}
				);
				
				hasRunOnce = true;
				
				return ret;
				
			},
			array : function(arr, depth, key, jquery) {
				
				/* Checking depth + circular refs */
				/* Note, check for circular refs before depth; just makes more sense */
				var stackKey = util.within(stack).is(arr);
				if ( stackKey ) {
					return util.common.circRef(arr, stackKey);
				}
				stack[key||'TOP'] = arr;
				if (depth === settings.maxDepth) {
					return util.common.depthReached(arr);
				}
				
				/* Accepts a table and modifies it */
				var me = jquery ? 'jQuery' : 'Array', table = util.table([me + '(' + arr.length + ')', null], jquery ? 'jquery' : me.toLowerCase()),
					isEmpty = true,
                    count = 0;
				
				if (jquery){
					table.addRow(['selector',arr.selector]);
				}

				util.forEach(arr, function(item,i){
                    if (settings.maxArray >= 0 && ++count > settings.maxArray) {
                        table.addRow([
                            i + '..' + (arr.length-1),
                            typeDealer[ util.type(item) ]('...', depth+1, i)
                        ]);
                        return false;
                    }
					isEmpty = false;
					table.addRow([i, typeDealer[ util.type(item) ](item, depth+1, i)]);
				});

				if (!jquery){
					if (isEmpty) {
						table.addRow(['<small>[empty]</small>']);
					} else {
						table.thead.appendChild( util.hRow(['index','value'], 'colHeader') );
					}
				}
				
				return settings.expanded ? table.node : util.expander(
					util.stringify(arr),
					'Click to show more',
					function() {
						this.parentNode.appendChild(table.node);
					}
				);
				
			},
			'function' : function(fn, depth, key) {
				
				/* Checking JUST circular refs */
				var stackKey = util.within(stack).is(fn);
				if ( stackKey ) { return util.common.circRef(fn, stackKey); }
				stack[key||'TOP'] = fn;
				
				var miniTable = util.table(['Function',null], 'function'),
					argsTable = util.table(['Arguments']),
					args = fn.toString().match(/\((.+?)\)/),
					body = fn.toString().match(/\(.*?\)\s+?\{?([\S\s]+)/)[1].replace(/\}?$/,'');
					
				miniTable
					.addRow(['arguments', args ? args[1].replace(/[^\w_,\s]/g,'') : '<small>[none/native]</small>'])
					.addRow(['body', body]);
					
				return settings.expanded ? miniTable.node : util.expander(
					'function(){...}',
					'Click to see more about this function.',
					function(){
						this.parentNode.appendChild(miniTable.node);
					}
				);
			},
			'date' : function(date) {
				
				var miniTable = util.table(['Date',null], 'date'),
					sDate = date.toString().split(/\s/);
				
				/* TODO: Make this work well in IE! */
				miniTable
					.addRow(['Time', sDate[4]])
					.addRow(['Date', sDate.slice(0,4).join('-')]);
					
				return settings.expanded ? miniTable.node : util.expander(
					'Date (timestamp): ' + (+date),
					'Click to see a little more info about this date',
					function() {
						this.parentNode.appendChild(miniTable.node);
					}
				);
				
			},
			'boolean' : function(bool) {
				return util.txt( bool.toString().toUpperCase() );
			},
			'undefined' : function() {
				return util.txt('UNDEFINED');
			},
			'null' : function() {
				return util.txt('NULL');
			},
			'default' : function() {
				/* When a type cannot be found */
				return util.txt('prettyPrint: TypeNotFound Error');
			}
		};
		
		container.appendChild( typeDealer[ (settings.forceObject) ? 'object' : util.type(obj) ](obj, currentDepth) );
		
		return container;
		
	};
	
	/* Configuration */
	
	/* All items can be overwridden by passing an
	   "options" object when calling prettyPrint */
	prettyPrintThis.config = {
		
		/* Try setting this to false to save space */
		expanded: true,
		
		forceObject: false,
		maxDepth: 3,
		maxArray: -1,  // default is unlimited
		styles: {
			array: {
				th: {
					backgroundColor: '#6DBD2A',
					color: 'white'
				}
			},
			'function': {
				th: {
					backgroundColor: '#D82525'
				}
			},
			regexp: {
				th: {
					backgroundColor: '#E2F3FB',
					color: '#000'
				}
			},
			object: {
				th: {
					backgroundColor: '#1F96CF'
				}
			},
			jquery : {
				th: {
					backgroundColor: '#FBF315'
				}
			},
			error: {
				th: {
					backgroundColor: 'red',
					color: 'yellow'
				}
			},
			domelement: {
				th: {
					backgroundColor: '#F3801E'
				}
			},
			date: {
				th: {
					backgroundColor: '#A725D8'
				}
			},
			colHeader: {
				th: {
					backgroundColor: '#EEE',
					color: '#000',
					textTransform: 'uppercase'
				}
			},
			'default': {
				table: {
					borderCollapse: 'collapse',
					width: '100%'
				},
				td: {
					padding: '5px',
					fontSize: '12px',
					backgroundColor: '#FFF',
					color: '#222',
					border: '1px solid #000',
					verticalAlign: 'top',
					fontFamily: '"Consolas","Lucida Console",Courier,mono',
					whiteSpace: 'nowrap'
				},
				td_hover: {
					/* Styles defined here will apply to all tr:hover > td,
						- Be aware that "inheritable" properties (e.g. fontWeight) WILL BE INHERITED */
				},
				th: {
					padding: '5px',
					fontSize: '12px',
					backgroundColor: '#222',
					color: '#EEE',
					textAlign: 'left',
					border: '1px solid #000',
					verticalAlign: 'top',
					fontFamily: '"Consolas","Lucida Console",Courier,mono',
					backgroundImage: util.headerGradient,
					backgroundRepeat: 'repeat-x'
				}
			}
		}
	};
	
	return prettyPrintThis;
	
})();

