var _idOriginalUploaded = '', _lastDimensao = '', _buttonTxt = '', _idsToRemove = [],
	cropAjaxRunning = null, destaqueAjaxRunning = null, descricaoAjaxRunning = null;

$(document).ready(function(){
	$('.btn-input-trigger').click(function(){
		var btn = $(this), tgt = btn.attr('data-export'), imageSrc = $(tgt).cropit('imageSrc');
		
		if(btn.hasClass('btn-thumb-input-trigger') || btn.hasClass('already-uploaded') || imageSrc == ''){
			$(tgt).find('.cropit-preview-image-container img, .cropit-preview-background-container img').hide();
			$(tgt).cropit('imageSrc', '');
			$(tgt).find('.cropit-image-zoom-input').val(0);
			$(tgt).find('i.delete-cw, i.remove-crop-thumb').remove();
			$(tgt).find('input[type="file"]').val('');
			$('#modal-img-categoria, #modal-thumb-categoria').attr('data-file-id', '');
			
			btn.parent('.clearfix').find('input[type="file"]').trigger('click');
		}else{
			var hasDescription = true;
			if(urlAtual.lastIndexOf('categoria') > -1){
				hasDescription = false;
			}
			
			uploadAndGenerateThumb({
				imgSrc: $(tgt).cropit('export'),
				reference: tgt,
				hasDescription: hasDescription,
				spinnerButton: $(this),
				action: 'ADD_ORG'
			});
		}
	});
	
	$('.export-cw').click(function(){
		var btn = $(this), tgt = btn.attr('data-export');
		
		uploadAndGenerateThumb({
			imgSrc: $(tgt).cropit('export'),
			reference: tgt,
			hasDescription: false,
			spinnerButton: $(this),
			action: 'ADD_THU'
		});
	})
	
	$('body').on('click', '.crop-crown-icon:not(.img-destaque)', function(){
		var oldFile = $('body').find('.crop-crown-icon.img-destaque').length > 0 ? $('body').find('.crop-crown-icon.img-destaque').attr('data-file-id') : _lastDestaque; 
		manageCropDestaque({
			icon: $(this),
			newFile: $(this).attr('data-file-id'),
			oldFile: oldFile
		});
	});
	
	$('body').on('click', '.crop-img-description', function(){
		var id = $(this).attr('data-file-id');
		var descricao = jQuery.trim($(this).text());
		var textarea = '<div class="crop-img-description-wrapper" data-id="'+id+'">';
			textarea += '<textarea data-id="'+id+'" placeholder="Insira aqui a descrição" class="crop-img-description-text" name="crop-img-description-text-'+id+'" id="crop-img-description-text-'+id+'" rows="6" autofocus>'+descricao+'</textarea>';
			textarea += '<span class="charNum crop-img-description-count"></span>';
		textarea += '</div>';
		
		$(this).hide().parent('.clearfix').append(textarea);
		countChar('#crop-img-description-text-'+id, '.crop-img-description-wrapper[data-id="'+id+'"] .crop-img-description-count', PARAMS.QTD_DESCRICAO_ARQUIVO);
	});
	
	$('body').on('keyup', '.crop-img-description-text', function(event){
		var id = $(this).attr('data-id');
		
		if(event.keyCode == 13 && !event.ctrlKey && !event.shiftKey){
			manageCropDescription({
				fileId: id,
				descricao: $(this).val(),
				textarea: $(this)
			});
		}else if(event.keyCode == 27){
			$(this).parent('.crop-img-description-wrapper').fadeOut(function(){
				$(this).parent('.clearfix').find('p').show();
				$(this).remove();
			});
		}
	});
	
	$('body').on('click', '.delete-cw:not(.remove-cropped-file)', function(){
		var target = $(this).attr('data-delete');
		
		$(target).cropit('imageSrc', '');
		$(target).find('.cropit-preview-image-container img, .cropit-preview-background-container img').hide();
		$(target).find('.rotate-cw').addClass('btn-outline btn-default').removeClass('blue-sharp').find('i').removeClass('active');
		$(target).find('.cropit-image-zoom-input').val(0);
		$(target).find('.btn-input-trigger').addClass('already-uploaded');
		
		if(target.lastIndexOf('img-categoria') > -1){
			$(target).find('.btn-input-trigger').html('adicionar imagem');
		}
		
		$(this).remove();
	});
	
	$('body').on('click', '.remove-cropped-file', function(event){
		event.preventDefault();
		event.stopPropagation();
		var btn = $(this);
		var obj = $('.formPost .multiplefileuploader');
		var idAtual = $(this).attr('data-file-id');
		var reference = $(this).attr('data-delete');
		
		swal({
		  title: "Atenção",
		  text: "Você irá remover esse arquivo. Quer continuar?",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  cancelButtonText: "Cancelar",
		  confirmButtonText: "Sim",
		  showLoaderOnConfirm: true,
		}).then(function () {
			jQuery.ajax({
				type: "post",
				url: urlContext+'/ws/deletar.htm?mode=json&form_name=form&id='+idAtual+'&tipo=arquivo',
			 	dataType: "json",
			 	data:{
			 		user: userLogin,
			 		wsVersion: wsVersion,
			 		wstoken:SESSION.USER_INFO.USER_WSTOKEN
			 	},
				beforeSend: function(){
					$('.ajax-file-upload-statusbar.alterarNome[data-id="'+idAtual+'"]').find(".ajax-file-upload-red").addClass("ac_loading");
				},
			 	success: function( data ) {
			 		var thumbModal = $('.crop-thumbs-holder .image-upload-holder[data-id="'+idAtual+'"]');
			 		if(thumbModal.find('.crop-crown-icon').hasClass('img-destaque')){
			 			_deleteDestaque = true;
			 			_lastDestaque = idAtual;
			 		}
			 		
			 		$('.ajax-file-upload-statusbar.alterarNome[data-id="'+idAtual+'"], .anexos li[data-id="'+idAtual+'"], .crop-thumbs-holder .image-upload-holder[data-id="'+idAtual+'"]').remove();
			 		var idsAnexos = typeof reference != 'undefined' && reference.lastIndexOf('modal-') > -1 ? jQuery.trim($('#id-images-categoria').val()) : jQuery.trim(obj.closest(".uploaderNovo").find(".inputAnexos").val());
			 		idsAnexos = idsAnexos.replace(idAtual+',', "");
			 		if(typeof reference != 'undefined' && reference.lastIndexOf('modal-') > -1){
			 			$('#id-images-categoria').val(idsAnexos);
			 		}else{
			 			obj.closest(".uploaderNovo").find(".inputAnexos").val(idsAnexos);
			 		}
			 		var target;
			 		if(btn.hasClass('remove-crop-thumb')){
			 			target = '#thumb-categoria, #modal-thumb-categoria';
			 			$('#cropit-thumb-input, #modal-cropit-thumb-input').val('');
			 		}else{
			 			target = '#img-categoria, #modal-img-categoria';
			 			$('#cropit-thumb-input, #modal-cropit-thumb-input').val('');
			 		}
			 		
			 		$(reference).find('.cropit-preview-image-container img, .cropit-preview-background-container img').hide();
			 		$(reference).cropit('imageSrc', '');
			 		$(target).find('.cropit-preview-image-container img, .cropit-preview-background-container img').hide();
		 			$(target).find('.cropit-image-zoom-input').val(0);
		 			$(target).find('.rotate-cw').addClass('btn-outline btn-default').removeClass('blue-sharp').find('i').removeClass('active');
		 			$(target).find('.delete-cw, .remove-crop-thumb').remove();
		 			$(target).find('.btn-input-trigger').html('adicionar imagem');
		 			btn.remove();
			 		swal("Sucesso", "O arquivo foi removido", "success");
				}
			 });
		});
	});
	
	$('.clear-crop').click(function(){
		clearLastCrop();
	});
	
	$('.btn-anexar-crop').click(function(){
		var btn = $(this), ref = '', cropOpt = {}, modal = urlAtual.lastIndexOf('categoria') > -1 ? '#modalCategoria' : '#modal-crop-image',
		thumbInput = $('#cropit-thumb-input').val(), imgInput = $('#cropit-image-input').val();
		
		if(typeof thumbInput != 'undefined' && thumbInput != ''){
			ref = '#thumb-categoria';
			cropOpt = {
				imgSrc: $(ref).cropit('export'),
				reference: ref,
				hasDescription: false,
				spinnerButton: $(ref).find('.btn-thumb-input-trigger'),
			};
			
			if(!$('#img-categoria').find('.btn-input-trigger').hasClass('already-uploaded')){
				cropOpt.action = 'ADD_THU';
			}else{
				cropOpt.action = 'ADD_LAST_ORG';
			}
			
			var lastThumb = $('#thumb-categoria').attr('data-file-id');
			if(typeof lastThumb != 'undefined'){
				_idThumbUploaded = lastThumb;
			}
			
			if(!$(ref).find('.btn-thumb-input-trigger').hasClass('already-uploaded')){
				uploadAndGenerateThumb(cropOpt);
			}
		}else{
			$(modal).modal('hide');
		}
		
		if(imgInput != ''){
			ref = '#img-categoria';
			cropOpt = {
				imgSrc: $(ref).cropit('export'),
				reference: ref,
				hasDescription: true,
				spinnerButton: $(ref).find('.btn-input-trigger'),
				action: 'ADD_LAST_ORG'
			};
			
			if(!$(ref).find('.btn-input-trigger').hasClass('already-uploaded')){
				setTimeout(function(){
					uploadAndGenerateThumb(cropOpt);
				}, 55);
			}
		}else{
			$(modal).modal('hide');
		}
	});
	
	
	if($('#descricao-original').length > 0){
		countChar('#descricao-original', '#img-categoria .charNum', PARAMS.QTD_DESCRICAO_ARQUIVO);
	}
});

function clearLastCrop(){
	$('#thumb-categoria, #modal-thumb-categoria, #img-categoria, #modal-img-categoria').find('.cropit-preview-image-container img, .cropit-preview-background-container img').hide();
	$('#thumb-categoria, #modal-thumb-categoria, #img-categoria, #modal-img-categoria').cropit('imageSrc', '');
	$('.cropit-image-zoom-input').val(0);
	$('#cropit-thumb-input, #modal-cropit-thumb-input, #cropit-image-input, #modal-cropit-image-input').val('');
	
	$('body').find('.delete-cw, .remove-crop-thumb').remove();
	$('.rotate-cw').addClass('btn-outline btn-default').removeClass('blue-sharp').find('i').removeClass('active');
	$('#img-categoria, #modal-img-categoria').find('.btn-input-trigger').addClass('already-uploaded');
	
	//limpa todos os anexos
	$(".multiplefileuploader").closest(".uploaderNovo").find(".inputAnexos").val('');
	$('.uploaderNovo').find('.ajax-file-upload-statusbar.alterarNome').remove();
	$('.crop-thumbs-holder').find('.image-upload-holder').remove();
}

$(function() {
	$('#thumb-categoria').cropit({
		imageBackground: true,
		imageBackgroundBorderWidth: 20,
		exportZoom: 2,
		onImageError: function(){
			toastr.error('A imagem escolhida não pode ser carregada, por favor, tente uma maior.');
		},
		onImageLoaded: function(){
			manageCropButtons('#thumb-categoria');
			manageCropThumb('#thumb-categoria');
			$('#thumb-categoria').find('.cropit-preview-image-container img, .cropit-preview-background-container img').show();
		},
		onOffsetChange: function(){
			manageCropButtons('#thumb-categoria');
			$('#thumb-categoria').find('.btn-thumb-input-trigger').removeClass('already-uploaded');
		}
	});
	
	$('#modal-thumb-categoria').cropit({
		imageBackground: true,
		imageBackgroundBorderWidth: 20,
		exportZoom: 2,
		onImageError: function(){
			toastr.error('A imagem escolhida não pode ser carregada, por favor, tente uma maior.');
		},
		onImageLoaded: function(){
			manageCropButtons('#modal-thumb-categoria');
			manageCropThumb('#modal-thumb-categoria');
			$('#modal-thumb-categoria').find('.cropit-preview-image-container img, .cropit-preview-background-container img').show();
		},
		onOffsetChange: function(){
			manageCropButtons('#modal-thumb-categoria');
			$('#modal-thumb-categoria').find('.btn-thumb-input-trigger').removeClass('already-uploaded');
		}
	});
	
	$('#img-categoria').cropit({
		imageBackground: true,
		imageBackgroundBorderWidth: 20,
		exportZoom: 2,
		onImageError: function(){
			toastr.error('A imagem escolhida não pode ser carregada, por favor, tente uma maior.');
		},
		onImageLoaded: function(){
			manageCropButtons('#img-categoria');
			manageCropThumb('#img-categoria');
			$('#img-categoria').find('#descricao-original').val('');
			$('#img-categoria').find('.cropit-preview-image-container img, .cropit-preview-background-container img').show();
		},
		onOffsetChange: function(){
			manageCropButtons('#img-categoria');
		}
	});
	
	$('#modal-img-categoria').cropit({
		imageBackground: true,
		imageBackgroundBorderWidth: 20,
		exportZoom: 2,
		onImageError: function(){
			toastr.error('A imagem escolhida não pode ser carregada, por favor, tente uma maior.');
		},
		onImageLoaded: function(){
			manageCropButtons('#modal-img-categoria');
			manageCropThumb('#modal-img-categoria');
			$('#modal-img-categoria').find('.cropit-preview-image-container img, .cropit-preview-background-container img').show();
		},
		onOffsetChange: function(){
			manageCropButtons('#modal-img-categoria');
		}
	});
	
	$('.rotate-cw').click(function() {
		var tgt = $(this).attr('data-rotate');
		$(tgt).cropit('rotateCW');
	});
	
	$('#thumb-categoria .cropit-preview-image-container').dblclick(function(){
		$('#thumb-categoria .btn-input-trigger').trigger('click');
	});
	$('#modal-thumb-categoria .cropit-preview-image-container').dblclick(function(){
		$('#modal-thumb-categoria .btn-input-trigger').trigger('click');
	});
	
	$('#img-categoria .cropit-preview-image-container').dblclick(function(){
		$('#img-categoria .btn-input-trigger').trigger('click');
	});
	$('#modal-img-categoria .cropit-preview-image-container').dblclick(function(){
		$('#modal-img-categoria .btn-input-trigger').trigger('click');
	});
});

function manageCropButtons(reference){
	$(reference).find('.rotate-cw, .export-cw').removeClass('btn-outline btn-default disabled').addClass('blue-sharp').find('i').addClass('active');
	
	if(reference == '#thumb-categoria' || reference == '#modal-thumb-categoria' || reference == '#modal-img-categoria'){
		$(reference).find('.btn-input-trigger').html('substituir imagem').blur();
	}else{
		$(reference).find('.btn-input-trigger').html('adicionar outra').removeClass('already-uploaded').blur();
	}
}

function manageCropThumb(reference){
	if(reference.lastIndexOf('modal') == -1){
		var idThumb = $('#thumb-categoria').attr('data-file-id') != '' ? $('#thumb-categoria').attr('data-file-id') : '';
		var classThumb = typeof idThumb == 'undefined' ? 'delete-cw' : 'remove-cropped-file remove-crop-thumb';
	}else{
		if(reference == '#modal-thumb-categoria'){
			var idThumb = $('#modal-thumb-categoria').attr('data-file-id') != '' ? $('#modal-thumb-categoria').attr('data-file-id') : '';
		}else{
			var idThumb = $('#modal-img-categoria').attr('data-file-id') != '' ? $('#modal-img-categoria').attr('data-file-id') : '';
		}
		
		var classThumb = 'remove-cropped-file remove-crop-thumb';
	}
	
	var wrapper = $(reference).find('.clearfix.cropit-preview');
	var removeCW = '<i class="fancybox-close '+classThumb+'" data-delete="'+reference+'" data-file-id="'+idThumb+'"></i>';
	wrapper.find('i.delete-cw').remove();
	wrapper.append(removeCW);
}

function manageCropDescription(obj){
	var ajax = function(obj){
		if(descricaoAjaxRunning != null) {
			descricaoAjaxRunning.abort();
		}
		
		var formData = new FormData(), descricao = obj.descricao.replace(/\n/ig, '');
		formData.append('id', parseInt(obj.fileId));
		formData.append('descricao', descricao);
		formData.append('dir', 'arquivos');
		formData.append('tipo', '1');
		formData.append('user_id', userId);
		formData.append('mode', 'json');
		formData.append('form_name', 'form');
		formData.append('dimensao', 'original');
		
		descricaoAjaxRunning = jQuery.ajax({
			type: 'POST',
			url: urlContext + '/ws/uploadFile.htm',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function(resp){
				$(obj.textarea).addClass('ac_loading');
			},
			success: function(resp){
				if(!resp.file){
					toastr.error(resp.mensagem.mensagem);
				}else{
					descricaoAjaxRunning = null;
					$(obj.textarea).parent('.crop-img-description-wrapper').fadeOut(function(){
						$(this).parent('.clearfix').find('p').removeClass('no-description').show().html(resp.file.descricao);
						
						var spanDescription = $('.alterarNome[data-id="'+resp.file.id+'"]').find('.descricaoUpload');
						if(spanDescription.length > 0){
							$('.alterarNome[data-id="'+resp.file.id+'"]').find('.descricaoUpload').html(resp.file.descricao);
						}else{
							$('.alterarNome[data-id="'+resp.file.id+'"]').append('<span class="descricaoUpload">'+resp.file.descricao+'</span>');
						}
						
						$(this).remove();
					});
				}
			}
		});
	}
	
	ajax(obj);
}

function manageCropDestaque(obj){
	var ajax = function(obj){
		if(destaqueAjaxRunning != null) {
			destaqueAjaxRunning.abort();
		}
		
		var formData = new FormData();
		formData.append('id', parseInt(obj.newFile));
		formData.append('id_destaque_atual', parseInt(obj.oldFile));
		formData.append('destaque', true);
		formData.append('user_id', userId);
		formData.append('dir', 'arquivos');
		formData.append('mode', 'json');
		formData.append('form_name', 'form');
		formData.append('dimensao', 'original');
		
		destaqueAjaxRunning = jQuery.ajax({
			type: 'POST',
			url: urlContext + '/ws/uploadFile.htm',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function(resp){
				$(obj.icon).addClass('ac_loading');
			},
			success: function(resp){
				$(obj.icon).removeClass('ac_loading');
				
				if(!resp.file){
					toastr.error(resp.mensagem.mensagem);
				}else{
					destaqueAjaxRunning = null;
					$('body').find('.crop-crown-icon').removeClass('img-destaque').attr('title', 'Tornar destaque');
					$(obj.icon).addClass('img-destaque').attr('title', 'Destaque');
				}
			}
		});
	}
	
	ajax(obj);
}

function uploadAndGenerateThumb(obj, paramsCallbackCategorias){
	var name = '', timestamp = Math.round(+new Date()/1000), prevTimestamp = Math.floor((Math.random() * 10) + 1);
	timestamp = prevTimestamp + timestamp;
	
	if(obj.reference.lastIndexOf('modal-') > -1){
		name = obj.reference && obj.reference == '#modal-img-categoria' ? $('#modal-cropit-image-input').val() : $('#modal-cropit-thumb-input').val();
	}else{
		name = obj.reference && obj.reference == '#img-categoria' ? $('#cropit-image-input').val() : $('#cropit-thumb-input').val();	
	}
	
	name = name.substring(name.lastIndexOf('\\') +1, name.length);
	name = timestamp+'_'+name;
	
	var ext = name.substring(name.lastIndexOf('.') +1, name.length), base64 = obj.imgSrc.replace(/^data:image\/(png|jpg);base64,/, "");
	var dimensao = obj.reference && obj.reference == '#img-categoria' || obj.reference == '#modal-img-categoria' ? 'original' : 'thumb';
	var destaque;
	if((obj.reference && obj.reference == '#img-categoria' && $('.crop-thumbs-holder .image-upload-holder').length == 0) || _deleteDestaque){
		destaque = true;
	}else{
		destaque = false;
	}
	
	var blob = base64ToBlob(base64, 'image/'+ext), formData = new FormData(), browser = whatBrowserIs(), formDataFix = {};

	if(browser != '' && browser != 'Safari'){
		var fileField = new File([blob], name);
		formData.append('fileField', fileField);
	}else{
		formData.append('fileBase64', base64);
	}
	
	formData.append('fileName', name);
	formData.append('dimensao', dimensao);
	formData.append('destaque', destaque);
	formDataFix.destaque = destaque;
	formData.append('dir', 'arquivos');
	formData.append('tipo', '1');
	formData.append('user_id', userId);
	formData.append('mode', 'json');
	formData.append('form_name', 'form');
	
	if(obj.hasDescription){
		if($('#descricao-original').length > 0){
			if($('#descricao-original').val() != ''){
				var descricao = $('#descricao-original').val();
				formData.append('descricao', descricao);
			}
		}
	}
	
	var service = function(formData){
		if(cropAjaxRunning != null) {
			if(_lastDimensao == dimensao){
				cropAjaxRunning.abort();
			}
		}
		
		cropAjaxRunning = jQuery.ajax({
			type: 'POST',
			url: urlContext + '/ws/uploadFile.htm',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function(data){
				_lastDimensao = dimensao;
				$(obj.spinnerButton).addClass('disabled');
				_buttonTxt = $(obj.spinnerButton).html();
				$(obj.spinnerButton).html('<i class="fa fa-spinner fa-spin fa-fw font-white"></i>');
			},
			success: function(data){
				if(!data.file){
					ajaxErrorHandler(data, obj, _buttonTxt);
				}else{
					var conteudo = '', html = '', uploaderNovo = $('.uploaderNovo.uploadPost');
					_isShowThumbCrop = !$('#modalCategoria').is(':visible');
					
					var idsAnexos = _isShowThumbCrop ? $(".multiplefileuploader").closest(".uploaderNovo").find(".inputAnexos").val() : $("#id-images-categoria").val();
					
					if(browser != '' && browser != 'Safari'){
						var classDestaque = formData.get('destaque') == 'true' ? 'img-destaque' : '';
						var titleDestaque = formData.get('destaque') == 'true' ? 'Destaque' : 'Tornar destaque';
					}else{
						var classDestaque = formDataFix.destaque == 'true' ? 'img-destaque' : '';
						var titleDestaque = formDataFix.destaque == 'true' ? 'Destaque' : 'Tornar destaque';
					}
					
					var classDescription = data.file.descricao ? '' : 'no-description';
					
					if(obj.imgSrc && obj.imgSrc != ''){
						if(_isShowThumbCrop){
							conteudo += '<div class="image-upload-holder" data-id="'+data.file.id+'">';
								conteudo += '<i class="remove-cropped-file cursor-pointer fancybox-close" data-file-id="'+data.file.id+'"></i>';
								conteudo += '<i class="crop-crown-icon cursor-pointer '+classDestaque+'" title="'+titleDestaque+'" data-file-id="'+data.file.id+'"></i>';
								conteudo += '<img src="'+obj.imgSrc+'" alt="Thumbnail" class="cropped-image-thumb" data-id="'+data.file.id+'" />';
								conteudo += '<div class="clearfix">';
									conteudo += '<p class="crop-img-description '+classDescription+'" data-file-id="'+data.file.id+'">';
										if(data.file.descricao && data.file.descricao != ''){
											conteudo += data.file.descricao;
										}
									conteudo += '</p>';
								conteudo += '</div>';
							conteudo += '</div>';
						
							if(obj.reference == '#thumb-categoria' || obj.reference == '#modal-thumb-categoria'){
								$('.crop-thumbs-holder[data-crop="'+obj.reference+'"]').empty();
								if(_idThumbUploaded != '' && _idThumbUploaded != data.file.id){
									idsAnexos = idsAnexos.replace(_idThumbUploaded, data.file.id);
									
									$(".multiplefileuploader").closest(".uploaderNovo").find(".inputAnexos").val(idsAnexos);
									var anexo = uploaderNovo.find('.alterarNome[data-id="'+_idThumbUploaded+'"]');
									anexo.attr('data-id', data.file.id);
									anexo.find('a.fancybox').attr('href', data.file.url).text(data.file.file);
									_idThumbUploaded = data.file.id;
									$('#thumb-categoria').attr('data-file-id', data.file.id);
								}else{
									_idThumbUploaded = data.file.id;
									$(".multiplefileuploader").closest(".uploaderNovo").find(".inputAnexos").val(idsAnexos +" "+data.file.id+",");
									html += htmlAnexos(data.file, obj.reference);
								}
							}else{
								$(".multiplefileuploader").closest(".uploaderNovo").find(".inputAnexos").val(idsAnexos +" "+data.file.id+",");
								html += htmlAnexos(data.file, obj.reference);
							}
							
							$('.crop-thumbs-holder[data-crop="'+obj.reference+'"] .thumbs-wrapper').append(conteudo);
							uploaderNovo.append(html);
							var visible = $('.crop-thumbs-holder[data-crop="'+obj.reference+'"]').css('opacity'); 
							if(visible == 0){
								$('.crop-thumbs-holder[data-crop="'+obj.reference+'"]').fadeIn().css({'visibility': 'visible', 'opacity': '1'});
							}
						}else{
							manageUploadCropSemThumb({
								reference: obj.reference,
								idsAnexos: idsAnexos,
								file: data.file,
							});
						}
						
						$(obj.spinnerButton).html(_buttonTxt).removeClass('disabled');
						$(obj.spinnerButton).closest('.cropit-wrapper').find('.delete-cw').attr('data-file-id', data.file.id);
						if(urlAtual.lastIndexOf('categoria') == -1){
							$('.btn-anexar-crop').html('Concluir');
						}
						cropAjaxRunning = null;
						
						$('#descricao-original').val('');
						$('#descricao-original').parent('.clearfix').find('.charNum').html(PARAMS.QTD_DESCRICAO_ARQUIVO);
						
						if(paramsCallbackCategorias && typeof paramsCallbackCategorias != 'undefined'){
							paramsCallbackCategorias.arquivo_ids = $('#id-images-categoria').val();
							
							if(urlAtual == 'categorias.htm'){
						    	if(paramsCallbackCategorias.id) {
						    		httpPut(urlContext+"/rest/v1/categoria", paramsCallbackCategorias, success, function(data) {
						    			clearFormCategoria();
						    			toastr.error(data.responseJSON.message)
						    			$('#salvarCateg').html('Salvar');
						    		});
						    	} else {
						    		httpPost(urlContext+"/rest/v1/categoria", paramsCallbackCategorias, success, function(data) {
						    			toastr.error(data.responseJSON.message)
						    			$('#salvarCateg').html('Salvar');
						    		});
						    	}
							}else{
								if(paramsCallbackCategorias.id) {
									httpPut(urlContext+"/rest/v1/categoria", paramsCallbackCategorias, function(res){
										if (res.status != "ERROR") {
							    			var item = res.entity;
								    		var conteudo = '<li class="list-group-item-sideBox bg-white text-center" data-cor="'+item.cor+'" data-id="'+item.id+'" data-name="'+item.nome+'" data-usercreate="' + SESSION.USER_INFO.USER_NOME + '" data-usercreateid="' + SESSION.USER_INFO.USER_ID + '">';
								    			conteudo += '<a style="color:'+item.cor+'" href="'+urlContext+'/pages/mural.htm?categoria='+item.id+'">'+item.nome+'</a></li>';
											$('.list-group-categorias').prepend(conteudo);
											tooltipCateg("ul.list-group-categorias > li");
											clearFormCategoria();
											toastr.success(res.message);
							    		} else {
							    			toastr.error(res.message);
							    			$('#salvarCateg').html('Salvar');
							    		}
									}, function(res) {
										var message = jqXHR.responseJSON;
										toastr.error(message.message);
										$('#salvarCateg').html('Salvar');
						    		});
								} else {
									httpPost(urlContext+"/rest/v1/categoria", paramsCallbackCategorias, function(res){
										if (res.status != "ERROR") {
							    			var item = res.entity;
								    		var conteudo = '<li class="list-group-item-sideBox bg-white text-center" data-cor="'+item.cor+'" data-id="'+item.id+'" data-name="'+item.nome+'" data-usercreate="' + SESSION.USER_INFO.USER_NOME + '" data-usercreateid="' + SESSION.USER_INFO.USER_ID + '">';
								    			conteudo += '<a style="color:'+item.cor+'" href="'+urlContext+'/pages/mural.htm?categoria='+item.id+'">'+item.nome+'</a></li>';
											$('.list-group-categorias').prepend(conteudo);
											tooltipCateg("ul.list-group-categorias > li");
											clearFormCategoria();
											toastr.success(res.message);
							    		} else {
							    			toastr.error(res.message);
							    			$('#salvarCateg').html('Salvar');
							    		}
									}, function(res) {
										var message = jqXHR.responseJSON;
										toastr.error(message.message);
										$('#salvarCateg').html('Salvar');
						    		});
								}
							}
						}
					}
				} 
			}
		});
		
		switch(obj.action){
			case 'ADD_ORG':
				$(obj.spinnerButton).addClass('already-uploaded').click();
				break;
				
			case 'ADD_THU':
				$(obj.spinnerButton).addClass('already-uploaded');
				break;
			
			case 'ADD_LAST_ORG':
				var modal = urlAtual.lastIndexOf('categoria') > -1 ? '#modalCategoria' : '#modal-crop-image';
				$(obj.spinnerButton).addClass('already-uploaded');
				$(modal).modal('hide');
				break;
			
			default:
				console.log(obj.action);
				break;
		}
		
		return cropAjaxRunning;
	}
	
	service(formData);
}

function ajaxErrorHandler(data, obj, _buttonTxt){
	cropAjaxRunning = null;
	
	var modal = urlAtual.lastIndexOf('categoria') > -1 ? '#modalCategoria' : '#modal-crop-image';
	if(urlAtual.lastIndexOf('categoria') == -1){
		$('.btn-anexar-crop').html('Concluir');
	}
	$(modal).modal('hide');
	$(obj.spinnerButton).html(_buttonTxt).removeClass('disabled');
	toastr.error(data.mensagem.mensagem);
}

function manageUploadCropSemThumb(obj){
	var _previousId = obj.reference == '#thumb-categoria' || obj.reference == '#modal-thumb-categoria' ? _idThumbUploaded : _idOriginalUploaded;
	
	if(_previousId != '' && _previousId != obj.file.id){
		if(obj.reference == '#thumb-categoria' || obj.reference == '#modal-thumb-categoria'){
			obj.idsAnexos = obj.idsAnexos.replace(_idThumbUploaded, obj.file.id);
		}else{
			obj.idsAnexos = obj.idsAnexos.replace(_idOriginalUploaded, obj.file.id);
		}
		
		$("#id-images-categoria").val(obj.idsAnexos);
		if(obj.reference == '#thumb-categoria' || obj.reference == '#modal-thumb-categoria'){
			_idThumbUploaded = obj.file.id;
		}else{
			_idOriginalUploaded = obj.file.id;
		}
	}else{
		if(obj.reference == '#thumb-categoria' || obj.reference == '#modal-thumb-categoria'){
			_idThumbUploaded = obj.file.id;
		}else{
			_idOriginalUploaded = obj.file.id;
		}
		$("#id-images-categoria").val(obj.idsAnexos +" "+obj.file.id+",");
	}
}

function htmlAnexos(file, reference){
	var extensao = file.extensao.toLowerCase(), conteudoAnexoPost = '';
	var classeReferencia = reference == '#thumb-categoria' || reference == '#modal-thumb-categoria' ? 'remove-crop-thumb' : 'remove-original-thumb';
	
	conteudoAnexoPost += '<div class="ajax-file-upload-statusbar alterarNome '+file.dimensao+'" data-id="'+file.id+'">';
		conteudoAnexoPost += '<img class="ajax-file-upload-preview" style="width: 100%; height: auto; display: none;">';
		
		conteudoAnexoPost += '<div class="ajax-file-upload-filename">';
			conteudoAnexoPost += '<div class="thumbnail"><i class="file-extensions-icons-medium '+extensao+'"></i><span class="extension">'+extensao+'</span></div>';
			conteudoAnexoPost += '<a href="'+file.url+'" class="fancybox" target="_blank">'+ file.file + '</a>';
			conteudoAnexoPost += '<a href="#" class="adicionarDescricao">editar descrição</a>';
			if(file.descricao && file.descricao != ''){
				conteudoAnexoPost += '<p class="descricaoUpload">'+file.descricao+'</p>';
			}
		conteudoAnexoPost += '</div>';
		
		conteudoAnexoPost += '<div class="ajax-file-upload-progress" style="display: none;">';
			conteudoAnexoPost += '<div class="ajax-file-upload-bar ajax-file-upload-1487249420137" style="width: 100%;"></div>';
		conteudoAnexoPost += '</div>';
		
		conteudoAnexoPost += '<div class="ajax-file-upload-red ajax-file-upload-abort ajax-file-upload-1487249420137" style="display: none;"></div>';
		conteudoAnexoPost += '<div class="ajax-file-upload-red ajax-file-upload-cancel ajax-file-upload-1487249420137" style="display: none;"></div>';
		conteudoAnexoPost += '<div class="ajax-file-upload-green" style="display: block;"></div><div class="ajax-file-upload-green" style="display: none;">Download</div>';
		conteudoAnexoPost += '<div class="ajax-file-upload-red remove-cropped-file '+classeReferencia+'" data-file-id="'+file.id+'"></div>';
	conteudoAnexoPost += '</div>';
	
	return conteudoAnexoPost;
}

function generateCropThumbs(file){
	var conteudo = '', fileExtension = file.name;
	fileExtension = fileExtension.substring(fileExtension.lastIndexOf('.') + 1, fileExtension.length);
	
	if(FILE_EXTENSIONS.image[fileExtension]){
		if(file.dimensao != 'thumb'){
			conteudo += '<div class="image-upload-holder uploaded">';
				conteudo += '<i class="remove-cropped-file cursor-pointer fancybox-close" data-file-id="'+file.id+'"></i>';
				if(file.destaque){
					conteudo += '<i class="crop-crown-icon cursor-pointer img-destaque" title="Destaque" data-file-id="'+file.id+'"></i>';
				}else{
					conteudo += '<i class="crop-crown-icon cursor-pointer" title="Tornar destaque" data-file-id="'+file.id+'"></i>';
				}
				conteudo += '<img src="'+file.url+'" alt="'+file.name+'" class="cropped-image-thumb already-uploaded" title="'+file.name+'" data-id="'+file.id+'">';
				conteudo += '<div class="clearfix">';
					if(file.descricao && file.descricao != ''){
						conteudo += '<p class="crop-img-description" data-file-id="'+file.id+'">'+file.descricao+'</p>';
					}else{
						conteudo += '<p class="crop-img-description no-description" data-file-id="'+file.id+'"></p>';
					}
				conteudo += '</div>';
			conteudo += '</div>'; 
			
			
			$('.crop-thumbs-holder .thumbs-wrapper').append(conteudo);
		}
		
		_cropImgCount++;
		$('.crop-info .crop-count').html(_cropImgCount);
	}
}