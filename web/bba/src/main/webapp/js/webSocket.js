	function onMessageReceived(evt) {
		var msg = JSON.parse(evt.data);
		msgContent = msg.content;
		msgTipo = msg.tipo;
		if (msgContent == undefined) {
			notifications.getNotifications({
				data: {
					user_id: userId,
				},
				playSound: true,
				animateIcon: true,
				isBadges: true,
				isNewNotification: true,
			});
		} else if(PARAMS.CHAT_ON == "1"){
			if(msg.code == 'receivedMessage') {
				var content = msg.content;
				if(content.fromUserID != userId) {
					notifications.getNotifications({
						data: {
							user_id: userId,
							count: 1, 
							tipo: "all"
						},
						playChatSound: true,
						animateChatIcon: true,
						isBadges: true,
						isNewNotification: true,
					});
				}
			}
		}
	}
	
	

	function connectToChatserver() {
		wsocket = new WebSocket(serviceLocationNotification+userId);
		wsocket.onmessage = onMessageReceived;
	}	