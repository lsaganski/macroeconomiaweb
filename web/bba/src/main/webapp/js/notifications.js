var notifications = (function() {
	
	var getNotifications = function(obj) {
		var ajaxRunning = null;
		/**
			Podem ser passados os seguintes parametros no json
			{
				isClear: true | false,
				isScroll: true | false,
				data: parametros de request pro ws de busca
				playSound: true | false
				animateIcon: true | false
				isShowTabNotification: true | false
				isFlipAnimation: true | false
				isSpinner: true | false
				onClick: function | nulo faz a ação default de redirect
				isNewNotification: true | false
				isShowDesktopCountNoti: true | false
				isShowDeskTopMessageNot: true | false
				isMenuConfig: true | false
				isShowBell: true | false -- adicionado para mostrar ícone no sino; sempre passar como false pra não ficar appendando na lista
				isShowNoResult: true | false -- para mostrar "Nenhum resultado encontrado" se não houverem resultados na busca
				isAnalytics: true | false -- true para registrar o evento,
				isAbort: true | false -- força o abort de alguma requisição em andamento
			}
			
		*/
		
		var idLastNot = $('body').find(".listaNotify .notification").first().attr("data-id");

		if(obj.isNewNotification) {
			var isActiveNot;
			if($("#sideBox-notificacoes").hasClass("active") || $(".popMenu.menuNotificacao.alerta").is(":visible")){
				isActiveNot = true;
			}
			if(isActiveNot){
				if(idLastNot) {
					obj.isShowDesktopCountNoti = false;
					obj.isShowDeskTopMessageNot = true;
					obj.data.novas_notifications_id = idLastNot;
				}
			} else {
				obj.isShowDesktopCountNoti = true;
				obj.isShowDeskTopMessageNot = false;
			}
		}
		if(obj.isShowTabNotification) {
			$("#sideBox-cads, #sideBox-user_grupo, #sideBox-links, #sideBox-arquivos").removeClass("active in");
			$("ul.icones-header li").removeClass("active");
			$("#sideBox-notify_solicita, #sideBox-notificacoes, li.li-notificacoes, #sideBox-link-notificacoes").addClass("active");
			$("#sideBox-notify_solicita, #sideBox-notificacoes").addClass("in");
			if(obj.isFlipAnimation) {
				$(".tab-3d-container").addClass("flip");
			}
			$(".header-links").find(".popMenu").fadeOut();
		}
		
		/**
		Função responsável por tratar as badges das notificações que o servidor enviar via websocket
		*/
		var TYPE_NOT = {
			COMENTARIO: {
				id: "COMENTARIO",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_comment_on.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_comment_on.png" class="not-read" />',
				texto: "<b>%usuario%</b> comentou na publicação <b>%titulo%</b>"
			},
			FAVORITO: {
				id: "FAVORITO",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_favorito_on.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_favorito_on.png" class="not-read" />',
				texto: "<b>%usuario%</b> favoritou a publicação <b>%titulo%</b>"
			},
			NEW_POST: {
				id: "NEW_POST",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_publish_on.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_publish_on.png" class="not-read" />',
				texto: "<b>%usuario%</b> publicou <b>%titulo%</b>"
			},
			LIKE: {
				id: "LIKE",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_like_on.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_like_on.png" class="not-read" />',
				texto: "<b>%usuario%</b> curtiu a publicação <b>%titulo%</b>"
			},
			GRUPO_PARTICIPAR: {
				id: "GRUPO_PARTICIPAR",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="not-read" />',
				texto: "<b>%usuario%</b> entrou no grupo <b>%grupo%</b>"
			},
			GRUPO_SAIR: {
				id: "GRUPO_SAIR",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="not-read" />',
				texto: "<b>%usuario%</b> saiu do grupo <b>%grupo%</b>"
			},
			GRUPO_SOLICITAR_PARTICIPAR: {
				id: "GRUPO_SOLICITAR_PARTICIPAR",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="not-read" />',
				texto: "<b>%usuario%</b> solicitou entrar no grupo <b>%grupo%</b>"
			},
			GRUPO_USUARIO_ADICIONADO: {
				id: "GRUPO_USUARIO_ADICIONADO",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="not-read" />',
				texto: "Você foi adicionado ao grupo <b>%grupo%</b>"
			},
			GRUPO_USUARIO_REMOVIDO: {
				id: "GRUPO_USUARIO_REMOVIDO",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="not-read" />',
				texto: "Você foi removido do grupo <b>%grupo%</b>"
			},
			GRUPO_USUARIO_ACEITO: {
				id: "GRUPO_USUARIO_ACEITO",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="not-read" />',
				texto: "<b>%usuario%</b> aprovou sua participacao no grupo <b>%grupo%</b>"
			},
			GRUPO_USUARIO_RECUSADO: {
				id: "GRUPO_USUARIO_RECUSADO",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="not-read" />',
				texto: "<b>%usuario%</b> recusou sua participacao no grupo <b>%grupo%</b>"
			},
			USUARIO_SOLICITAR_AMIZADE: {
				id: "USUARIO_SOLICITAR_AMIZADE",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_user_accept.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_user_accept.png" class="not-read" />',
				texto: "<b>%usuario%</b> quer se conectar com você"
			},
			USUARIO_DESFAZER_AMIZADE: {
				id: "USUARIO_DESFAZER_AMIZADE",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_user_reject.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_user_reject.png" class="not-read" />',
				texto: "<b>%usuario%</b> não é mais uma conexão"
			},
			USUARIO_RECUSAR_AMIZADE: {
				id: "USUARIO_RECUSAR_AMIZADE",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_user_reject.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_user_reject.png" class="not-read" />',
				texto: "<b>%usuario%</b> não aceitou ser sua conexão"
			},
			USUARIO_ACEITAR_AMIZADE: {
				id: "USUARIO_ACEITAR_AMIZADE",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="not-read" />',
				texto: "<b>%usuario%</b> agora é uma conexão"
			},
			REMINDER: {
				id: "REMINDER",
				lida: '<img src="'+urlContext+'/imagens/notifications/notification_reminder.png" class="read" />',
				naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_reminder.png" class="not-read" />',
				texto: "<b>%usuario%</b> marcou reminder na publicação <b>%titulo%</b>"
			},
		}
		
		/**
			Mostra as badges na navbar
		*/
		var showBadges = function(badges, animateIcon, playSound, animateChatIcon) {
			if(!badges) {
				return;
			}
			
			var totalNotificacoes = badges.total,
				totalMensagens = badges.mensagens,
				totalReminders = badges.reminders,
				totalSolicitacoes = badges.grupoSolicitarParticipar + badges.usuarioAmizadeSolicitada;
			
			var spanNoti = $(".notifications.header-badge");
			if(spanNoti.length > 0 && totalNotificacoes > 0){
				spanNoti.text(totalNotificacoes).show();	
				$('#sideBox-link-notificacoes a, .notification-badge-text').text('Notificações ('+totalNotificacoes+')');
				if(totalSolicitacoes > 0){
					$('#sideBox-link-solicitacoes a').text('Solicitações ('+totalSolicitacoes+')');
				}
				
				if(animateIcon) {
					animateCss($(".menuNotificacao.alerta").closest(".header-links"), "shake");
				} else if(animateChatIcon) {
					animateCss($(".menuNotificacao.mensagem").closest(".header-links"), "shake");
				}
				
				var texto = totalNotificacoes+" notificação"
				if(totalNotificacoes > 1){
					texto = totalNotificacoes+" notificações";
				}
				$(".totalNotificacoes").html(texto);
			}
			
			var spanMensagens =  $(".menuNotificacao.mensagem").closest(".header-links").find(">span");
			if(spanMensagens.length > 0 && totalMensagens > 0){
				spanMensagens.html(totalMensagens).show();
				$(".icones.mensagens").closest(".header-links").addClass("ativo");
						
				if(totalMensagens == 1){
					$(".totalMsgs").html(mensagens+" mensagem");
				}else if(totalMensagens > 1){
					$(".totalMsgs").html(mensagens+" mensagens");
				}
			}
			
			var spanReminders =  $(".reminders.header-badge");
			if(spanReminders.length > 0 && totalReminders > 0){
				spanReminders.html(totalReminders).show();
			}
			
			if(spanMensagens.length > 0 || spanNoti.length > 0) {
				if(playSound) {
					
					if($('#chatAudio').length == 0) {
						var sound = '<audio id="chatAudio">';
							sound += '<source src="'+urlContext+'/audios/notify.ogg" type="audio/ogg">';
							sound += '<source src="'+urlContext+'/audios/notify.mp3" type="audio/mpeg">';
						sound += '</audio>';
						$(sound).appendTo('body');
						
						$(sound)[0].play();
					} else {
						$('#chatAudio')[0].play();
					}
				}
			}
			
			if(totalSolicitacoes == 0){
				$('#sideBox-link-solicitacoes a').text('Solicitações');
			}
			
			if(totalNotificacoes == 0){
				$('#sideBox-link-notificacoes a, .notification-badge-text').text('Notificações');
			}
			
			if(!SESSION.PERFILS.PERFIL_isRoot){
				if(totalNotificacoes || totalMensagens){
					if(SESSION.PERMISSOES.ENVIAR_MENSAGEM == 'true'){
						if(totalNotificacoes != 0  && totalMensagens != 0){
							var totalBadges = totalNotificacoes + totalMensagens;
						} else if(totalNotificacoes != 0 && totalMensagens == 0){
							var totalBadges = totalNotificacoes;
						} else if(totalNotificacoes == 0 && totalMensagens != 0){
							var totalBadges = totalMensagens;
						}
					}else{
						totalBadges = totalNotificacoes;
					}
					
					document.title = "("+totalBadges+") " + PARAMS.WEB_TEMPLATE_TITLE;
				}else{
					document.title = PARAMS.WEB_TEMPLATE_TITLE;
					if($('.header-links.chat > span.notifications.chat').is(':visible')){
						$('.header-links.chat > span.notifications.chat').html('').fadeOut();
					}
					
					if($('.header-links.notificacoes > span.notifications.header-badge').is(':visible')){
						$('.header-links.notificacoes > span.notifications.header-badge').html('').fadeOut();
						$('#sideBox-link-notificacoes a, .notification-badge-text').text('Notificações');
					}
					
				}
			}
		};
		
		/**
			Mostra as badges de mensagens não lidas
		*/
		var showBadgesMensagensNaoLidas = function(mensagensNaoLidas, animateIcon, playSound) {
			if(mensagensNaoLidas && mensagensNaoLidas > 0) {
				$('.header-links.chat').find('.notifications.chat').remove();
				$('.header-links.chat').append('<span class="notifications chat" style="display: inline;">'+mensagensNaoLidas+'</span>');
				
				if(animateIcon) {
					animateCss($(".header-links.chat"), "shake");
				}
				
				if(playSound) {
					if($('#chatAudio').length == 0) {
						var sound = '<audio id="chatAudio">';
							sound += '<source src="'+urlContext+'/audios/notify.ogg" type="audio/ogg">';
							sound += '<source src="'+urlContext+'/audios/notify.mp3" type="audio/mpeg">';
						sound += '</audio>';
						$(sound).appendTo('body');
						
						$(sound)[0].play();
					} else {
						$('#chatAudio')[0].play();
					}
				}
			}else{
				$('.header-links.chat').find('.notifications.chat').html('').fadeOut();
			}
		}
		
		var showAllNotifications = function(notifications) {
			$("#buscaNotificacoes").removeClass("ac_loading");
			var conteudo = createLiNotifications(notifications);
			if(conteudo && conteudo != "") {
				var ul = $(".popMenu.menuNotificacao.alerta ul");
				if(obj.isMenuConfig){
	        		setTimeout(function(){
	        			ul.find('li.loading').fadeOut('normal', function(){
	        				$(this).remove();
	        				ul.empty();
	        				$(conteudo).appendTo(ul).each(function(){
	        					$('.popMenu .listaNotify li[data-tipo!="GRUPO_SOLICITAR_PARTICIPAR"]').click(function(e){
	        	        			var obj = {
	        	        				data:{
	        	        					user_id: SESSION.USER_INFO.USER_LOGIN,
	        	        					count: 0,
	        	        					id_notification: $(this).attr('data-id'),
	        	        					markAsRead: 1
	        	        				},
	        	        				callback: function(){
	        	        					if($(this).is('a') && $(this).attr('href') != ""){
	    	        	        				window.location = $(this).attr('href');
	    	        	        			}
	        	        				}
	        	        			};
	        	        			markNotificationAsRead(obj);
	        	        		});
	        				});
	        				tooltipUsuario($('.menuNotificacao .fotoUsuario'));
        				});
	        		}, 10);
	        		
	        		if(!notifications || notifications.length == 0) {
	        			$(".popMenu.menuNotificacao.alerta .listFooter, .popMenu.menuNotificacao.alerta .listHeader p").hide();
	        			$('.header-links.notificacoes > span.notifications.header-badge').html('').fadeOut();
	        		}else{
	        			$(".popMenu.menuNotificacao.alerta .listFooter, .popMenu.menuNotificacao.alerta .listHeader p").show();
	        		}
	        		
				}else{
					ul.find('li.loading').fadeOut('normal', function(){ $(this).remove(); });
				}
				
				if(obj.isClear) {
					$('.list-holder.listaNotify > li').remove();
				}else if($('.list-holder').is(":visible") && !obj.isScroll){
					$('.list-holder > li').remove();
				}
				$('.list-holder.listaNotify').parent('.scroll-list').find('.spinner').show();
				
				$('.list-holder.listaNotify').append(conteudo);
				$('.list-holder.listaNotify').parent('.scroll-list').find('.spinner').hide();
				
				tooltipUsuario($('.list-holder.listaNotify .fotoUsuario'));

				var notiURL = getURLParameter('notId');
				if(urlAtual.lastIndexOf('post.htm') > -1 && notiURL != ""){
					$('ul.listaNotify > li[data-id="'+notiURL+'"]').addClass("active");
				}
			}
		}
		
		/**
			Mostra as ultimas notificações que acabaram de chegar via socket
		*/
		var showLastNotifications = function(notifications) {
			var conteudo = createLiNotifications(notifications);
			if(conteudo && conteudo != "") {
				$('body').find(".listaNotify li").each(function(){
					var postIdVelho = $(this).attr('data-postid');
					var tipoVelho = $(this).attr('data-tipo');
					var naoLida = $(this).hasClass('nao-lida');
					if(naoLida){
						for(var i = 0; i < notifications.length ; i++){
							var postIdNovo = notifications[i].postId;
							var tipoNovo = notifications[i].tipo;
							if(postIdVelho == postIdNovo && tipoVelho == tipoNovo){
								$(this).remove();
							}
						}
					}
				});
				var isActiveNot = $("#sideBox-notificacoes").hasClass("active");
				if(isActiveNot && idLastNot) {
					$(".list-holder.listaNotify").prepend(conteudo);
				}
				if($(".popMenu.menuNotificacao.alerta").is(":visible") && idLastNot) {
					$(".popMenu.menuNotificacao.alerta ul").prepend(conteudo);
				}
			}
			tooltipUsuario($('.listaNotify .fotoUsuario'));
		}
		
		var createLiNotifications = function(notifications) {
			var conteudo = "";
			var lida = 0;
			var nLida = 0;
			if(obj.isShowBell){
				if(!notifications || notifications.length == 0) {
					conteudo += '<li class="sbold text-center noNotify"><img src="'+urlContext+'/imagens/notifyBell.png" class="img-responsive" alt="Nenhuma notificação" /><p>Nenhuma notificação recente.</p></li>';
				}
			}
			
			if(obj.isShowNoResult){
				if(!notifications || notifications.length == 0 && obj.data.page == 0) {
					conteudo += '<li class="sbold text-center noNotify"><img src="'+urlContext+'/imagens/notifyBell.png" class="img-responsive" alt="Nenhuma notificação" /><p>';
						if(obj.data.list == 2){
							conteudo += 'Nenhuma notificação recente.';
						}else{
							conteudo += 'Nenhuma notificação encontrada;';
						}
					conteudo += '</p></li>';
				}
			}
			
			var isBuscaUsuarios = 0;
			
			for(var i in notifications) {
				var not = notifications[i];
				
				if(!not.textoNotificacao || not.textoNotificacao == "") {
					continue;
				}
				
				if(not.lida == true){
					lida++;
				}else{
					nLida++;
				}
				
				var tipoNoti = TYPE_NOT[not.tipo];
				if(!tipoNoti) {
					console.log("TIPO " + not.tipo + " não mapeada");
					continue;
				}
				
				var textNot = tipoNoti.texto;
				textNot = textNot.replace("%usuario%", not.nomeUsuario);
				
				if(not.titulo){
					textNot = textNot.replace("%titulo%", replaceHtml(not.titulo));
				}else{
					textNot = textNot.replace("%titulo%", " ");
				}
				
				if(not.grupo){
					textNot = textNot.replace("%grupo%", not.grupo);
				}else{
					textNot = textNot.replace("%grupo%", " ");
				}
				
				if(not.tipo == 'LIKE' && not.textoNotificacao.lastIndexOf('curtiu seu comentário') > -1){
					textNot = '<b>%usuario%</b> curtiu seu comentário no post <b>%titulo%</b>';
					textNot = textNot.replace("%usuario%", not.nomeUsuario);
					if(not.titulo){
						textNot = textNot.replace("%titulo%", replaceHtml(not.titulo));
					}else{
						textNot = textNot.replace("%titulo%", " ");
					}
				}
				
				if(not.tipo == 'NEW_POST' && not.texto.lastIndexOf('atualizou o comunicado') > -1){
					textNot = '<b>%usuario%</b> atualizou o comunicado <b>%titulo%</b>';
					textNot = textNot.replace("%usuario%", not.nomeUsuario);
					if(not.titulo){
						textNot = textNot.replace("%titulo%", replaceHtml(not.titulo));
					}else{
						textNot = textNot.replace("%titulo%", " ");
					}
				}
				
				var comentario = "";
				if(not.tipo == TYPE_NOT.COMENTARIO.id) {
					if(not.comentario) {
						var comentario = not.comentario;
						if(comentario.msg && comentario.msg != "") {
							comentario = comentario.msg;
						}else{
							if(comentario.arquivos && comentario.arquivos.length > 0){
								comentario = 'Arquivo - ' + comentario.arquivos[0].file;
							}else{
								comentario = '';
							}
						}
					}
				}
				var post = "";
				if(not.tipo == TYPE_NOT.NEW_POST.id) {
					if(not.post) {
						var post = not.post;
						if(post.mensagem && post.mensagem != "") {
							if(post.html){
								var html = post.mensagem;
								html = html.substring(html.lastIndexOf('<body>')+6, html.lastIndexOf('</body>'));
								post = htmlTrim(html);
							}else{
								post = _bbcodeParser.bbcodeToHtml(post.mensagem);
								post = htmlTrim(post);
							}
						}else{
							if(post.arquivos && post.arquivos.length > 0){
								post = 'Arquivo - ' + post.arquivos[0].file;
							}else{
								post = '';
							}
						}
					}
				}
				
				var classActive = not.id == obj.notIdActive ? "active" : "";
				var classLida = not.lida == true ? "lida" : "nao-lida";
				if(not.postId){
					var postID = not.postId;
				}else{
					var postID = "";
				}
				if(not.grupoId){
					var grupoId = not.grupoId;
				}else{
					var grupoId = "";
				}
				
				var urlThumb = not.urlThumbUsuario;
				conteudo += '<li data-tipo="'+not.tipo+'" class="notification '+classActive+' '+classLida+'" data-lida="'+ not.lida +'" data-id="'+not.id+'" data-postId="'+postID+'" data-grupoId="'+ grupoId +'">';
					conteudo += '<div class="row">';
						if(tipoNoti.id == TYPE_NOT.GRUPO_PARTICIPAR.id || tipoNoti.id == TYPE_NOT.GRUPO_SAIR.id) {
							urlThumb = not.urlThumbGrupo;
							if(urlThumb){
								if(urlThumb.substr(0,4).indexOf('img/') > -1) {
									urlThumb = urlContext + '/' + urlThumb;
								}
							}else{
								urlThumb = urlContext + "/imagens/bg_grupo_thumb.jpg";
							}
							conteudo += '<a href="'+urlContext+'/cadastro/grupo.htm?id='+not.grupoId+'&tab=addUser" class="clearfix">';
						} else if(tipoNoti.id == TYPE_NOT.GRUPO_SOLICITAR_PARTICIPAR.id) {
							urlThumb = not.urlThumbGrupo;
							if(urlThumb){
								if(urlThumb.substr(0,4).indexOf('img/') > -1) {
									urlThumb = urlContext + '/' + urlThumb;
								}
							}else{
								urlThumb = urlContext + "/imagens/bg_grupo_thumb.jpg";
							}
							conteudo += '<a class="clearfix" href="'+urlContext+'/grupo.htm?id='+grupoId+'">';
						} else if(tipoNoti.id == TYPE_NOT.GRUPO_USUARIO_REMOVIDO.id){
							urlThumb = not.urlThumbGrupo;
							if(urlThumb){
								if(urlThumb.substr(0,4).indexOf('img/') > -1) {
									urlThumb = urlContext + '/' + urlThumb;
								}
							}else{
								urlThumb = urlContext + "/imagens/bg_grupo_thumb.jpg";
							}
							conteudo += '<a href="'+urlContext+'/pages/mural.htm" class="clearfix">';
						} else if(tipoNoti.id == TYPE_NOT.GRUPO_USUARIO_ADICIONADO.id || tipoNoti.id == TYPE_NOT.GRUPO_USUARIO_ACEITO.id || tipoNoti.id == TYPE_NOT.GRUPO_USUARIO_RECUSADO.id) {
							urlThumb = not.urlThumbGrupo;
							if(urlThumb){
								if(urlThumb.substr(0,4).indexOf('img/') > -1) {
									urlThumb = urlContext + '/' + urlThumb;
								}
							}else{
								urlThumb = urlContext + "/imagens/bg_grupo_thumb.jpg";
							}
							conteudo += '<a href="'+urlContext+'/pages/mural.htm?grupo='+not.grupoId+'" class="clearfix">';
						} else if(tipoNoti.id == TYPE_NOT.USUARIO_SOLICITAR_AMIZADE.id || tipoNoti.id == TYPE_NOT.USUARIO_DESFAZER_AMIZADE.id || tipoNoti.id == TYPE_NOT.USUARIO_RECUSAR_AMIZADE.id || tipoNoti.id == TYPE_NOT.USUARIO_ACEITAR_AMIZADE.id) {
							conteudo += '<a href="'+urlContext+'/usuario.htm?id='+not.idUsuario+'" class="clearfix">';
                            $('#sideBox-usuarios').find('.scroll-list .col-xs-12 ul').empty();
                            isBuscaUsuarios++;
						} else {
							conteudo += '<a href="'+urlContext+'/pages/post.htm?id='+not.postId+'&notId='+not.id+'" class="clearfix">';
						}
							conteudo += '<div class="col-md-3 notification-badge-holder">';
								conteudo += '<div class="fotoUsuario media" data-nome="'+not.nomeUsuario+'" data-foto="'+not.urlThumbUsuario+'" data-userId="'+not.idUsuario+'">';
									conteudo += '<div class="centraliza">';
										conteudo += '<img src="'+urlThumb+'" alt="Foto do '+not.nomeUsuario+'">';
									conteudo += '</div>';
								conteudo += '</div>';	
								conteudo += '<span class="interaction-badge">';
									if(not.lida) {
										conteudo += tipoNoti.lida;
									} else {
										conteudo += tipoNoti.naoLida;
									}
								conteudo += '</span>';
							conteudo += '</div>';	
							conteudo += '<div class="col-md-9 row notification-text-holder">';
								if(not.usuarios && not.usuarios.length > 0){ // se for notificacao agrupada
									conteudo += '<p class="notifyText"><b>'+not.nomeUsuario+' e ';

									if(not.usuarios.length > 1){
										conteudo += 'mais '+not.usuarios.length;
									}else{
										conteudo += not.usuarios[0].nome;
									}
									
									conteudo += '</b>';
									
									var isGrupo = false;
									if(not.tipo && not.tipo == "COMENTARIO"){
										conteudo += ' comentaram na publicação <b>'+not.titulo+'</b></p>';
									}else if(not.tipo && not.tipo == "FAVORITO"){
										conteudo += ' favoritaram a publicação <b>'+not.titulo+'</b></p>';
									}else if(not.tipo && not.tipo == "NEW_POST"){
										conteudo += ' publicaram <b>'+not.titulo+'</b></p>';
									}else if(not.tipo && not.tipo == "LIKE"){
										conteudo += ' curtiram a publicação <b>'+not.titulo+'</b></p>';
									}else if(not.tipo && not.tipo == "GRUPO_PARTICIPAR"){
										isGrupo = true;
										conteudo += ' entraram no grupo <b>'+not.grupo+'</b></p>';
									}else if(not.tipo && not.tipo == "GRUPO_SAIR"){
										isGrupo = true;
										conteudo += ' saíram do grupo <b>'+not.grupo+'</b></p>';
									}else if(not.tipo && not.tipo == "GRUPO_SOLICITAR_PARTICIPAR"){
										isGrupo = true;
										conteudo += ' solicitaram entrar no grupo <b>'+not.grupo+'</b></p>';
									}else if(not.tipo && not.tipo == "GRUPO_USUARIO_ADICIONADO"){
										isGrupo = true;
										conteudo += ' foram adicionados ao grupo <b>'+not.grupo+'</b></p>';
									}else if(not.tipo && not.tipo == "GRUPO_USUARIO_REMOVIDO"){
										isGrupo = true;
										conteudo += ' foram removidos do grupo <b>'+not.grupo+'</b></p>';
									}else if(not.tipo && not.tipo == "GRUPO_USUARIO_ACEITO"){
										isGrupo = true;
										conteudo += ' aprovaram sua participacao no grupo <b>'+not.grupo+'</b></p>';
									}else if(not.tipo && not.tipo == "GRUPO_USUARIO_RECUSADO"){
										isGrupo = true;
										conteudo += ' recusaram sua participacao no grupo <b>'+not.grupo+'</b></p>';
									}else if(not.tipo && not.tipo == "USUARIO_SOLICITAR_AMIZADE"){
										conteudo += ' querem se conectar com você</p>';
									}else if(not.tipo && not.tipo == "USUARIO_DESFAZER_AMIZADE"){
										conteudo += ' deixaram de ser suas conexões</p>';
									}else if(not.tipo && not.tipo == "USUARIO_RECUSAR_AMIZADE"){
										conteudo += ' não aceitaram ser suas conexões</p>';
									}else if(not.tipo && not.tipo == "USUARIO_ACEITAR_AMIZADE"){
										conteudo += ' aceitaram ser suas conexões</p>';
									}else if(not.tipo && not.tipo == "REMINDER"){
										conteudo += ' marcaram reminder</p>';
									}
								}else{
									conteudo += '<p class="notifyText">'+textNot+'</p>';
								}
								if(PARAMS.SIDEBOX_NOTIFICATIONS_AGRUPAR_ON == '1'){
									//BLOCO DE NOT AGRUPADA
									if(not.usuarios && not.usuarios.length > 0){
										var cont = 0;
										conteudo += '<div class="fotoUsuarioAgrupada clearfix">';
										for(var u in not.usuarios) {
											if(isGrupo && cont == 0){
												conteudo += '<div class="fotoUsuario mini" data-nome="'+not.nomeUsuario+'" data-foto="'+not.urlThumbUsuario+'" data-userId="'+not.idUsuario+'">';
													conteudo += '<div class="centraliza">';
														conteudo += '<img src="'+not.urlThumbUsuario+'" alt="Foto do '+not.nomeUsuario+'" >';
													conteudo += '</div>';
												conteudo += '</div>';
												cont++;
											}
											if((cont < 5) || (not.usuarios.length == 6 && cont == 5)){
												conteudo += '<div class="fotoUsuario mini" data-nome="'+not.usuarios[u].nome+'" data-foto="'+not.usuarios[u].urlFotoUsuario+'" data-userId="'+not.usuarios[u].id+'">';
													conteudo += '<div class="centraliza">';
														conteudo += '<img src="'+not.usuarios[u].urlFotoUsuario+'" alt="Foto do '+not.usuarios[u].nome+'" >';
													conteudo += '</div>';
												conteudo += '</div>';
											}else if(not.usuarios.length > 6 && cont == 5){
												var restante = isGrupo ? (not.usuarios.length - 4) : (not.usuarios.length - 5);
												conteudo += '<div class="pull-left">';
													conteudo += '<p style="font-size: 20px;">+'+restante+'</p>';
												conteudo += '</div>';
											}
											cont++;
										}
										conteudo += '</div>';
									}else if(comentario != "") {
										var classeOver = '';
										if(comentario.length > 300){
											classeOver = 'overflow';
										}
										conteudo += '<p class="notifyText '+classeOver+'">'+comentario+'</p>';
									}
								}else{
									 if(comentario != "") {
										var classeOver = '';
										if(comentario.length > 300){
											classeOver = 'overflow';
										}
										conteudo += '<p class="notifyText '+classeOver+'">'+comentario+'</p>';
									}
								}
							 
								if(post != "") {
									var classeOver = '';
									if(post.length > 100){
										classeOver = 'overflow';
									}
									conteudo += '<p class="notifyText '+classeOver+'">'+post+'</p>';
								}

								conteudo += '<span class="dataPost">'+not.dataHora+'</span>';
							conteudo += '</div>';
						conteudo += '</a>';
					conteudo += '</div>';
				conteudo += '</li>';
				if(obj.isShowDeskTopMessageNot && notifications.length == 1) {
					if(typeof notifyMe != 'undefined') {
						notifyMe("Livecom", textNot, not.urlThumbUsuario)
					}
				}
				
				if(notifications.length == lida){
					$(".limparNotificacoes.linear-icons").addClass("disabled");
				}
				if(nLida >= 1){
					$(".limparNotificacoes.linear-icons").removeClass("disabled");
				}
			}
			
			if(isBuscaUsuarios > 0 && obj.isShowTabNotification){
				buscaUsuariosSideBox();
			}
			
			return conteudo;
		}
		
		var showComentariosPost = function(notifications) {
			var contextPath = window.location.pathname;
			if(contextPath.indexOf("post.htm") > -1) {
				var $post = $(".listaMural").children();
				var $btnComentar = $post.find(".botaoComentarios");
				if( $post.length > 0 ) {
					var postId = $($post).attr("data-id");
					for(var i in notifications) {
						var not = notifications[i];
						if(not.tipo == TYPE_NOT.COMENTARIO.id) {
							if(not.postId == postId) {
								$btnComentar.addClass("ativo");
								$btnComentar.closest(".interacoesPost").find(".listaComentarios:first").html('<li class="loading"></li>');
								carregarComentarios(5, 0, obj.data.user_id, postId, $btnComentar);
								break;
							}
						}
					}
				}
			}
		}
		
		/**
		 * Função que executa a chamada do ajax, passando os parametros
		 * necessários
		 */
		var service = function(obj, page) {
			obj.data.page = page ? page : 0;
			
			if(obj.isClear) {
				var animate = $("ul.listaNotify li").length > 0 ? true : false;
				if(animate) {
					$("ul.listaNotify").find('li:not(.spinner-blue)').remove();
				}
			}
			
			if(ajaxRunning != null){
				ajaxRunning.abort();
			}
			
			if(obj.isAbort){
				if(ajaxRunning != null){
					ajaxRunning.abort();
				}
			}
			
			ajaxRunning = jsHttp.post(urlContext + '/ws/notifications.htm', {
				contentType: jsHttp.CONTENT_TYPE_FORM,
				data: obj.data,
				beforeSend: function(res){
					$("#sideBox-notificacoes .scroll-list .spinner").show();
					$(".totalNotificacoes").html("");
				},
				success: function(data) {
					if(data.loginLivecomOk === false) {
						location.href = urlContext + "/logout.htm?clear=true";
					} else {
						$("#sideBox-notificacoes .scroll-list .spinner").hide();
						
						if($(".listaNotify").hasClass("spinner-blue menor")) {
							$(".listaNotify").removeClass("spinner-blue menor");
						}
						
						if(obj.isScroll){
							showAllNotifications(data.list);
						}else {
							showLastNotifications(data.list);
						}
						
						if(obj.isAnalytics){
							if(SESSION.ANALYTICS_KEY && SESSION.ANALYTICS_KEY != ''){
								ga('send', 'event', 'livecom', 'notifications', SESSION.USER_INFO.USER_LOGIN);
							}
						}
						
						if(obj.isNewNotification) {
							showComentariosPost(data.list);
							$(".icone-check-notify").removeClass("disabled").attr("rel", "notificacoes");
						}
						
						if(obj.isBadges) {
							showBadges(data.badges, obj.animateIcon, obj.playSound);
							var mensagensNaoLidas = data.badges ? data.badges.mensagens : 0;
							showBadgesMensagensNaoLidas(mensagensNaoLidas, obj.animateChatIcon, obj.playChatSound);
							if(data.badges && data.badges.total > 0){
								$(".icone-check-notify").removeClass("disabled").attr("rel", "notificacoes");	
							}else{
								$(".icone-check-notify").addClass("disabled").removeAttr("rel");
							}
						}
						
						$('.notifyTextComentario').truncate({
						    width: "auto",
						    token: '&hellip;',
						    side: 'right',
						    multiline: false
						});
						
						setTimeout(function(){
							ajaxRunning = null;
						}, 250);
					}
				}
			});
			
			return ajaxRunning;
		}
		
		/**
		 * Executa a função do ajax pela primeira vez quando chamada
		 */
		var ajaxFindRunning = service(obj, 0);
		

		if(obj.isScroll) {
			var pageScroll = 0;
			$("#sideBox-notificacoes .scroll-list").unbind('scroll');
			$("#sideBox-notificacoes .scroll-list").on('scroll', function(){
				if(ajaxRunning) {
					return;
				}
				
				var x = $("#sideBox-notificacoes .listaNotify").height() - $(this).height();
				
				var isExecute = $(this).scrollTop() >= x - 2;
				if(isExecute) {
					pageScroll += 1;
					obj.isClear = false;
					obj.animateIcon = false; // no scroll não precisa ter animação
					obj.playSound = false; // no scroll não precisa ter som
					service(obj, pageScroll);
				}
			});
		}
		return ajaxFindRunning;
	}
	
	var markNotificationAsRead = function(obj){
		var ajaxRunning = null;
		var service = function(obj) {
			if(ajaxRunning != null) {
				ajaxRunning.abort();
			}
			
			ajaxRunning = jsHttp.post(urlContext + '/ws/notifications.htm?form_name=form&mode=json', {
				contentType: jsHttp.CONTENT_TYPE_FORM,
				data: obj.data,
				success: function(res) {
					if(res.loginLivecomOk === false) {
						location.href = urlContext + "/logout.htm?clear=true";
					} else {
						ajaxRunning = null;
					}
					
					if(obj.callback){
						obj.callback();
					}
				}
			});
			return ajaxRunning;
		}
		
		service(obj);
	}
	
	var markAllNotificationsAsRead = function(obj){
		var ajaxRunning = null;
		
		var service = function() {
			if(ajaxRunning != null) {
				ajaxRunning.abort();
			}
			
			ajaxRunning = jsHttp.post(urlContext + '/ws/notifications.htm?mode=json&form_name=form', {
				contentType: jsHttp.CONTENT_TYPE_FORM,
				data: {
			 		user_id: SESSION.USER_INFO.USER_ID,
			 		markAllAsRead: 1
		 		},
				success: function(data) {
					if(data.loginLivecomOk === false) {
	 					location.href = "$context/logout.htm?clear=true";
	 				} else {
	 					ajaxRunning = null;
	 					$(obj.element).closest(".popMenu").hide().closest(".header-links").find(">span").html(0).hide();
						if(data.status == "OK"){
							toastr.info(data.mensagem);
						}else{
							toastr.error(data.mensagem);
						}
	        		
						$('.notifications-opt').removeClass('open');
						$('.notifications-opt').find('button.dropdown-toggle').attr('aria-expanded', 'false');
						$(".header-links.notificacoes").children("span").html("").fadeOut(80);
						$(".icone-check-notify").addClass("disabled").removeAttr("rel");
	        		
						var tabNotificationsActive = $("#sideBox-notify_solicita").hasClass("active");
						
						var paramsNotifications = {
							data: { 
								user_id: SESSION.USER_INFO.USER_LOGIN
							}, 
							isBadges: true, 
							isClear: true, 
							isScroll: true
						};
	    			
						if(tabNotificationsActive) {
							paramsNotifications.isShowTabNotification = true;
							paramsNotifications.isFlipAnimation = true;
							paramsNotifications.isSpinner = true;
						}else{
							paramsNotifications.isShowTabNotification = false;
							paramsNotifications.isFlipAnimation = false;
							paramsNotifications.isSpinner = false;
							
							$('li.li-notificacoes > .header-links.notificacoes').removeClass('aberto');
						}
						notifications.getNotifications(paramsNotifications);
	 				}
				}
			});
			
			return ajaxRunning;
		}
		
		service(obj);
	}
	
	return {
		getNotifications: getNotifications,
		markNotificationAsRead: markNotificationAsRead,
		markAllNotificationsAsRead: markAllNotificationsAsRead
	}
}());

function markNotificationAsRead(obj){
	notifications.markNotificationAsRead(obj);
}

function markReminderAsRead(idRem){
//	console.log("marcar-lido");
	var ajaxRunning = null;
	var service = function(idRem) {
		ajaxRunning = jsHttp.post(urlContext + '/rest/post/reminder/read/' + idRem, {
			contentType: jsHttp.CONTENT_TYPE_FORM,
			success: function(res) {
				console.log(res);
				if(res.loginLivecomOk === false) {
					location.href = urlContext + "/logout.htm?clear=true";
				} else {
					ajaxRunning = null;
				}
			}
		});
		return ajaxRunning;
	}
	
	service(idRem);
}

function htmlTrim(htmlString){
	var trim = '';
	trim = htmlString.replace(/(<([^>]+)>)/ig, '').replace(/\n/ig, '');
	return trim;
}

function replaceHtml(htmlString){
	var trim = '';
	trim = htmlString.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
	return trim;
}

$(document).ready(function(){
	$('body').on('click', '.listaNotify li[data-tipo!="GRUPO_SOLICITAR_PARTICIPAR"], .popMenu.menuNotificacao .listaNotify li[data-tipo!="GRUPO_SOLICITAR_PARTICIPAR"] a', function(){
		var obj = {
			data:{
				user_id: SESSION.USER_INFO.USER_LOGIN,
				count: 0,
				id_notification: $(this).attr('data-id'),
				markAsRead: 1
			},
			callback: function(){
				if($(this).is('a') && $(this).attr('href') != ""){
					window.location = $(this).attr('href');
				}
			}
		};
		notifications.markNotificationAsRead(obj);
	});
	
	$(".limparNotificacoes").click(function(e){
		var obj = $(this);
		e.preventDefault();
		e.stopPropagation();
		
		notifications.markAllNotificationsAsRead({
			element: $(this)
		});
	});
});