/**
 * Gray theme for Highcharts JS
 * @author Torstein Hønsi
 */

Highcharts.theme = {
	colors: ["#04e21b", "#7798BF", "#55BF3B", "#DF5353", "#aaeeee", "#ff0066", "#eeaaee",
		"#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
	chart: {
		backgroundColor: {
			linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
			stops: [
				[0, 'rgb(96, 96, 96)'],
				[1, 'rgb(16, 16, 16)']
			]
		},
		borderWidth: 0,
		borderRadius: 15,
		plotBackgroundColor: null,
		plotShadow: false,
		plotBorderWidth: 0,
		style: {
			fontFamily: 'Open sans, Arial, Helvetica, sans-serif'
		}
	},
	title: {
		enabled:false
	},
	subtitle: {
		style: {
			color: '#DDD',
			fontSize: '12px'
		}
	},
	xAxis: {
		gridLineWidth: 0,
		lineColor: '#999',
		tickColor: '#999',
		labels: {
			style: {
				color: '#999',
				fontWeight: 'bold',
				fontSize: '12px'
			}
		},
		title: {
			style: {
				color: '#AAA',
				fontWeight: 'bold',
				fontSize: '12px'
			}
		}
	},
	yAxis: {
		alternateGridColor: null,
		minorTickInterval: null,
		gridLineColor: 'rgba(255, 255, 255, .1)',
		lineWidth: 0,
		tickWidth: 0,
		labels: {
			style: {
				color: '#999',
				fontWeight: 'bold',
				fontSize: '12px'
			}
		},
		title: {
			style: {
				color: '#AAA',
				fontWeight: 'bold',
				fontSize: '12px'
			}
		}
	},
	legend: {
		itemStyle: {
			color: '#CCC',
			fontSize: '12px'
		},
		itemHoverStyle: {
			color: '#FFF',
			fontSize: '12px'
		},
		itemHiddenStyle: {
			color: '#333',
			fontSize: '12px'
		}
	},
	labels: {
		style: {
			color: '#CCC',
			fontSize: '12px'
		}
	},
	tooltip: {
		headerFormat: '<span style="font-size: 12px">{point.key}</span><br/>',
		backgroundColor: {
			linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
			stops: [
				[0, 'rgba(96, 96, 96, .8)'],
				[1, 'rgba(16, 16, 16, .8)']
			]
		},
		changeDecimals: 2,
		borderWidth: 0,
		style: {
			color: '#FFF',
			fontSize: '12px'
		},
		valueDecimals : 2
	},


	plotOptions: {
		line: {
			dataLabels: {
				color: '#CCC'
			},
			marker: {
				lineColor: '#333'
			}
		},
		spline: {
			marker: {
				lineColor: '#333'
			}
		},
		scatter: {
			marker: {
				lineColor: '#333'
			}
		},
		candlestick: {
			lineColor: 'white'
		}
	},

	toolbar: {
		itemStyle: {
			color: '#CCC',
			fontSize: '12px'
		}
	},

	navigation: {
		buttonOptions: {
			backgroundColor: {
				linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
				stops: [
					[0.4, '#606060'],
					[0.6, '#333333']
				]
			},
			borderColor: '#000000',
			symbolStroke: '#C0C0C0',
			hoverSymbolStroke: '#FFFFFF'
		}
	},

	exporting: {
		buttons: {
			exportButton: {
				symbolFill: '#55BE3B'
			},
			printButton: {
				symbolFill: '#7797BE'
			}
		}
	},

	// scroll charts
	rangeSelector: {
		buttonTheme: {
			fill: {
				linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
				stops: [
					[0.4, '#888'],
					[0.6, '#555']
				]
			},
			stroke: '#000000',
			style: {
				color: '#CCC',
				fontWeight: 'bold',
				fontSize: '12px'
			},
			states: {
				hover: {
					fill: {
						linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
						stops: [
							[0.4, '#BBB'],
							[0.6, '#888']
						]
					},
					stroke: '#000000',
					style: {
						color: 'white',
						fontSize: '12px'
					}
				},
				select: {
					fill: {
						linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
						stops: [
							[0.1, '#000'],
							[0.3, '#333']
						]
					},
					stroke: '#000000',
					style: {
						color: 'yellow',
						fontSize: '12px'
					}
				}
			}
		},
		inputStyle: {
			backgroundColor: '#333',
			color: 'silver',
			fontSize: '12px'
		},
		labelStyle: {
			color: 'silver',
			fontSize: '12px'
		}
	},

	navigator: {
		height: 50,
		handles: {
			backgroundColor: '#212121',
			borderColor: '#04a6f9'
		},
		outlineColor: '#FFF',
		maskFill: 'rgba(16, 16, 16, 0.5)',
		series: {
			color: '#394656',
			lineColor: '#83a1bd',
		}
	},

	scrollbar: {
		barBorderRadius: 5,
		buttonBorderRadius: 5,
		trackBorderRadius: 6,
		height: 30,
		barBackgroundColor: {
				linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
				stops: [
					[0.4, '#353535'],
					[0.6, '#131313']
				]
			},
		barBorderColor: '#5e5e5e',
		buttonArrowColor: '#04a6f9',
		buttonBackgroundColor: {
				linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
				stops: [
					[0.4, '#353535'],
					[0.6, '#131313']
				]
			},
		buttonBorderColor: '#5e5e5e',
		rifleColor: '#04a6f9',
		trackBorderWidth: 2,
		trackBackgroundColor: {
			linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
			stops: [
				[0, '#000'],
				[1, '#000']
			]
		},
		trackBorderColor: '#434343'
	},

	// special colors for some of the demo examples
	legendBackgroundColor: 'rgba(48, 48, 48, 0.8)',
	legendBackgroundColorSolid: 'rgb(70, 70, 70)',
	dataLabelsColor: '#444',
	textColor: '#E0E0E0',
	maskColor: 'rgba(255,255,255,0.3)'
};

// Apply the theme
var highchartsOptions = Highcharts.setOptions(Highcharts.theme);
