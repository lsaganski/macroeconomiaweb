var reminders = (function() {
	
	var getReminders = function(obj){
		var ajaxRunning = null, pageScroll = 0;
		
		var service = function(obj, page) {
			obj.data.page = page ? page : 0;
			
			if(ajaxRunning != null){
				ajaxRunning.abort();
			}
			
			ajaxRunning = jQuery.ajax({
				type: 'POST',
				data: JSON.stringify({
					page: obj.data.page,
					maxRows: obj.data.maxRows,
					user: {
						id: userId
					}
				}),
				contentType: 'application/json',
				dataType: 'json',
				url: urlContext + '/rest/reminder',
				beforeSend: function(){
					$('#sideBox-reminder .spinner').show();
				},
				success: function(resp){
					var conteudo = '';
					if(resp.entity && resp.entity.length > 0){
						conteudo += createReminder(resp);
					}else{
						$('#sideBox-reminder .listaNotify-reminder').addClass('last-page');
						if(obj.data.page === 0){
							conteudo += '<li class="text-center noNotify"><img src="'+urlContext+'/imagens/notifyClock.png" class="img-responsive" alt="Nenhuma notificação" /><p class="sbold">Nenhum reminder recente.</p></li>';
						}
					}
					if(obj.data.page === 0){
						$('#sideBox-reminder .listaNotify-reminder').empty();
					}
					
					ajaxRunning = null;
					$('#sideBox-reminder .listaNotify-reminder').append(conteudo);
					$('#sideBox-reminder .spinner').hide();
					tooltipUsuario($('.list-holder.listaNotify-reminder .fotoUsuario'));
				}
			});
			
			return ajaxRunning;
		}
		
		var ajaxFindRunning = service(obj, 0);
		
		$("#sideBox-reminder .scroll-list").unbind('scroll');
		$("#sideBox-reminder .scroll-list").on('scroll', function(){
			if(ajaxRunning) {
				return;
			}
			
			var x = $("#sideBox-reminder .listaNotify-reminder").height() - $(this).height();
			
			var isExecute = $(this).scrollTop() > x && !$("#sideBox-reminder .listaNotify-reminder").hasClass('last-page');
			if(isExecute) {
				pageScroll += 1;
				service(obj, pageScroll);
			}
		});
		
		return ajaxFindRunning;
	}
	
	var TYPE_NOT = {
		REMINDER: {
			id: "REMINDER",
			lida: '<img src="'+urlContext+'/imagens/notifications/notification_reminder.png" class="read" />',
			naoLida: '<img src="'+urlContext+'/imagens/notifications/notification_reminder.png" class="not-read" />',
			texto: "<b>%usuario%</b> adicionou um novo reminder para <b>%data%</b>"
		}
	}
	
	var createReminder = function(reminders){
		var wrapper = '',
		postIds = [];
		
		for(var i = 0; i < reminders.entity.length; i++){
			var reminder = reminders.entity[i],
				texto = TYPE_NOT[reminder.tipo].texto,
				interactionBadge = TYPE_NOT[reminder.tipo].naoLida,
				repetiu = false;
			
			
			// evitar mostrar reminders repetidos (referentes ao mesmo post)
			if(postIds.length > 0){
				for(var i = 0; i < postIds.length; i++){
					if(postIds[i] == reminder.postId){
						repetiu = true;
						continue;
					}
				}
			}
			
			if($(".listaNotify-reminder li [data-postId="+ reminder.postId +"]").length > 1){
				repetiu = true;
			}
			
			if(repetiu){
				continue;
			}
			
			postIds[i] = reminder.postId;

			if(reminder.nomeUsuario && reminder.nomeUsuario != ''){
				texto = texto.replace('%usuario%', reminder.nomeUsuario);
			}
			
			if(reminder.texto && reminder.texto != ''){
				var aux = reminder.texto.substring(reminder.texto.lastIndexOf('reminder para')+14, reminder.texto.length);
				texto = texto.replace('%data%', aux);
			}
			
			wrapper += '<li data-id="'+reminder.id+'" data-lida="'+reminder.lida+'" data-postId="'+reminder.postId+'">';
				wrapper += '<div class="row">';
					wrapper += '<a href="'+urlContext+'/pages/post.htm?id='+reminder.postId+'" class="clearfix">';
						wrapper += '<div class="col-md-3 notification-badge-holder">';
							wrapper += '<div class="fotoUsuario media" data-nome="'+reminder.nomeUsuario+'" data-userId="'+reminder.idUsuario+'" data-foto="'+reminder.urlThumbUsuario+'">';
								wrapper += '<div class="centraliza">';
									wrapper += '<img alt="Foto do '+reminder.nomeUsuario+'" src="'+reminder.urlThumbUsuario+'" />';
								wrapper += '</div>';
							wrapper += '</div>';
							wrapper += '<span class="interaction-badge">';
								wrapper += interactionBadge;
							wrapper += '</span>';
						wrapper += '</div>';
						
						wrapper += '<div class="col-md-9 row notification-text-holder">';
							wrapper += '<p class="notifyText">'+texto+'</p>';
							wrapper += '<span class="dataPost">'+reminder.dataHora+'</span>';
						wrapper += '</div>';
					wrapper += '<a>';
				wrapper += '</div>';
			wrapper += '</li>';
		}
		
		return wrapper;
	}
	
	return {
		getReminders: getReminders,
	}
}());

	
function markReminderAsRead(idRem){
	var ajaxRunning = null,
		service = function(idRem) {
			ajaxRunning = jsHttp.post(urlContext + '/rest/reminder/read/' + idRem, {
				contentType: jsHttp.CONTENT_TYPE_FORM,
				success: function(res) {
					if(res.status == 'ERROR'){
						toastr.error(res.message);
					}else{
						toastr.success(res.message);
					}
					ajaxRunning = null;
				}
			});
			return ajaxRunning;
		}
	
	service(idRem);
}

$(document).ready(function(){
	$('body').on('click', '.reminder-marcar-lido', function(){
		markReminderAsRead($(this).data('id'));
	});
});