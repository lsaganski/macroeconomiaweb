function ntInput(){
	var input = document.createElement('input');
	input.type = 'hidden';
	input.name = 'nonce_token';
	input.id = 'nonce_token';
	if(typeof SESSION == 'undefined'){
		input.value = 'notlogged'+Date.now();
	}else{
		input.value = SESSION.USER_INFO.USER_ID+Date.now();
	}
	document.getElementById('form').appendChild(input);
}

ntInput();