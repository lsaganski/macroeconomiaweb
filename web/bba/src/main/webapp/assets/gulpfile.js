var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sassUnicode = require('gulp-sass-unicode');


gulp.task('sass:watch', function () {
    gulp.watch('./sass/**/*.scss', ['sass']);
});

gulp.task('sass', function () {
    return gulp.src('./sass/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sassUnicode())
        .pipe(gulp.dest('./css'));
});

gulp.task('default', ['sass:watch', 'sass']);