function copyToClipboard(obj){
	var clipboard = new Clipboard(obj.target, {
        target: function() {
            return document.querySelector(obj.target);
        },
        text: function(trigger) {
            return trigger.getAttribute(obj.attribute);
        }
	});

   clipboard.on('success', function(e) {
       toastr.success("Link copiado para a área de transferência");
   });

   clipboard.on('error', function(e) {
	   toastr.error("Algo deu errado. Tente novamente");
   });
}

function SaveToDisk(fileURL, fileName, callback) {
    if (!window.ActiveXObject) {
        // not IE
        var save = document.createElement('a');
        save.href = fileURL;
        save.target = '_blank';
        save.download = fileName || 'unknown';

        var evt = new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': false
        });
        save.dispatchEvent(evt);

        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    } else if ( !! window.ActiveXObject && document.execCommand)     {
        // IE < 11
        var _window = window.open(fileURL, '_blank');
        _window.document.close();
        _window.document.execCommand('SaveAs', true, fileName || fileURL)
        _window.close();
    }
    
    if(callback){
    	callback();
    }
}

function timerScrollPosition(timeOut){
	var timer = null;
	timer = setInterval(function(){
		var categURL = getURLParameter('categoria');
		if($('#sideBox-categorias').hasClass('active') && (categURL != "" && categURL != null)){
			var categoriaAtiva = {
				container: '#sideBox-categorias',
				containerClass: 'active',
				activeChildSelector: '#sideBox-categorias .scroll-list .col-xs-12 li.ativo',
			};
			saveScrollPosition(categoriaAtiva);
		}
		
		var tagURL = getURLParameter('tag');
		if($('#sideBox-tags').hasClass('active') && (tagURL != "" && tagURL != null)){
			var tagAtiva = {
				container: '#sideBox-tags',
				containerClass: 'active',
				activeChildSelector: '#sideBox-tags .scroll-list .col-xs-12 li.ativo',
			};
			saveScrollPosition(tagAtiva);
		}
		
		var grupoURL = getURLParameter('grupo');
		if($('#sideBox-grupos').hasClass('active') && (grupoURL != "" && grupoURL != null)){
			var grupoAtivo = {
				container: '#sideBox-grupos',
				containerClass: 'active',
				activeChildSelector: '#sideBox-grupos .scroll-list .col-xs-12 li.active',
			};
			saveScrollPosition(grupoAtivo);
		}
		
		var usuarioURL = getURLParameter('user_post_id');
		if($('#sideBox-usuarios').hasClass('active') && (usuarioURL != "" && usuarioURL != null)){
			var usuarioAtivo = {
				container: '#sideBox-usuarios',
				containerClass: 'active',
				activeChildSelector: '#sideBox-usuarios .scroll-list .col-xs-12 li.active',
				isPageScroll: true,
			};
			var objForSearch = {
				isSpinner: true,
				isShowNoResults: true,
			}
			saveScrollPosition(usuarioAtivo, objForSearch, buscaUsuariosSideBox, timeOut, true);
		}
		
		if(urlAtual.lastIndexOf('post.htm') > -1 && urlAtual.lastIndexOf('notId') == -1){
			var idURL = getURLParameter('id');
			if($('#sideBox-links').hasClass('active')  && (idURL != "" && idURL != null)){
				var linkAtivo = {
					container: '#sideBox-links',
					containerClass: 'active',
					activeChildSelector: '#sideBox-links .scroll-list .listaLinks li.active',
					offsetTop: 160
				};
				var objForSearch = {
						isSpinner: true,
						isShowNoResults: true,
					}
				saveScrollPosition(linkAtivo, objForSearch, buscaLinks, timeOut, true);
			}
		}
		
		var arquivoURL = getURLParameter('arquivo');
		if($('#sideBox-arquivos').hasClass('active') && (arquivoURL != "" && arquivoURL != null)){
			var arquivoAtivo = {
				container: '#sideBox-arquivos',
				containerClass: 'active',
				activeChildSelector: '#sideBox-arquivos .scroll-list .lista-arquivos li.active',
				offsetTop: 180,
				isPageScroll: false
			};
			var objForSearch = {
				isScrollSb: false,
				page: pageScroll1
			}
			
			saveScrollPosition(arquivoAtivo, objForSearch, searchArquivos, timeOut, false);
		}
			
		clearInterval(timer);
    }, timeOut);
}

function saveScrollPosition(obj, objCallback, fnCallback, timeOut, isPageScroll){
	if($(obj.activeChildSelector).length > 0){
		var activeChild = $(obj.activeChildSelector).position().top;
		if(obj.offsetTop){
			$(obj.container).find('.scroll-list').animate({ scrollTop: activeChild - obj.offsetTop },800, 'easeOutExpo');
		}else{
			$(obj.container).find('.scroll-list').animate({ scrollTop: activeChild },800, 'easeOutExpo');
		}
	}else{
		var callbackTimer = null;
		pageScroll1++;
		
		if(objCallback){
			if(isPageScroll){
				objCallback.pageScroll = pageScroll1;
			}else{
				objCallback.page = pageScroll1;
			}
			fnCallback(objCallback);
		}
		
		callbackTimer = setInterval(function(){
			saveScrollPosition(obj, objCallback, fnCallback, timeOut, obj.isPageScroll);
			clearInterval(callbackTimer);
		}, timeOut);
	}
}

function filtroNotifications(filtroAtivo){
	jsHttp.post(urlContext+"/ws/notificationOption.htm", {
		data: {
			filtro: filtroAtivo
		},
		contentType: jsHttp.CONTENT_TYPE_FORM,
		success: function(data) {}
	});
}

function addSideBoxActive(tabActive) {
	jsHttp.post(urlContext+"/ws/sidebox.htm", {
		data: {
			sidebox_tab: tabActive
		},
		contentType: jsHttp.CONTENT_TYPE_FORM,
		success: function(data) {}
	});
}

var ajaxBuscaUsuario = null;
var ajaxBuscaGrupos = null;
var ajaxBuscaTags = null;
var ajaxBuscaCategorias = null;

function searchArquivos(obj) {
	if(!obj.page){
		var pg = 0;
	}else{
		var pg = obj.page;
	}
	var texto = $("#buscarArquivo").val();
	var dataRange = $("#sideBox_filtro_data option:checked").val();
	var fileExt = $("#sideBox_filtro_extensao option:checked").val();
	var data = {
		userId: userId,
		todos: true,
		page: pg,
		max: 20,
		texto: texto,
		extensao: fileExt,
	}
	
	if(dataRange != 'all') {
		if(dateUtils[dataRange]) {
			var range = dateUtils[dataRange]();
			data.start = range.start;
			data.end = range.end;
		}
	}
	
	if(obj.isScrollSb){
		arquivos.sideBox({
			data: data,
			isScroll: true,
			isScrollSidebox: true,
		});
	}else{
		arquivos.sideBox({
			data: data,
			isScroll: true,
		});
	}
}

function buscaLinks() {
	sideBoxLinks({
		data: {
			site: $("#sites-conhecidos").val(),
			userId: userId,
			texto: $("#buscaLinks").val()
		},
		isScrollSidebox: true
	});
}

function buscaGrupos(obj, nome) {
	var ajaxTime;
	
	var user_admin = null;
	var nome = obj.nome ? obj.nome : null;
	if(ajaxBuscaGrupos != null) {
		ajaxBuscaGrupos.abort();
	}
	jsHttp.post(urlContext + "/ws/gruposAutoComplete.htm", {
		data: {
			q: nome,
			user_id: userId,
			tipo: obj.tipo,
			user_admin: user_admin,
			meus_grupos: obj.meus_grupos,
			participo: obj.participo
		},
		contentType: jsHttp.CONTENT_TYPE_FORM,
		beforeSend: function(data){
			if(obj.isScrollSidebox){
				ajaxTime = new Date().getTime();
			}
			$("#buscaSideBarGrupos").addClass("ac_loading");
		},
		success: function(data) {
			var conteudoFavorito = "", conteudoParticipo = "", conteudoNaoParticipo = "", grupoImg = "";
			if(data && data.list) {
				for(var i in data.list) {
					var item = data.list[i];
					grupoImg = item.urlFotoThumb;
					if(grupoImg){
						if(grupoImg.substr(0,4).indexOf('img/') > -1) {
							grupoImg = urlContext + '/' + grupoImg;
						}
					}else{
						grupoImg = urlContext + "/imagens/bg_grupo_thumb.jpg";
					}
					
					if(item.participa && item.favorito){
						conteudoFavorito += '<li data-id="'+item.id+'">';
						
							conteudoFavorito += '<div class="clearfix">';
								conteudoFavorito += '<div class="grupo-pic-holder">';
									conteudoFavorito += '<div class="fotoUsuario media">';
										conteudoFavorito += '<a href="'+urlContext + '/pages/mural.htm?grupo='+item.id+'" class="centraliza">';
											conteudoFavorito += '<img src="'+grupoImg+'" alt="Foto do grupo '+item.nome+'" />';
										conteudoFavorito += '</a>';
									conteudoFavorito += '</div>';
									conteudoFavorito += '<i class="fa fa-star css-favorito" data-favorito="true"></i>';
								conteudoFavorito += '</div>';
								
								conteudoFavorito += '<div class="infoUsuarios">';
									if(item.id == -1) {
										conteudoFavorito += '<p class="first">'+item.nome+'</p>';
									} else {
										conteudoFavorito += '<p class="first"><a class="font-black" href="'+urlContext + '/grupo.htm?id='+item.id+'">'+item.nome+'</a>';
											if(item.virtual && item.countSubgrupos){
												conteudoFavorito += item.countSubgrupos > 1 ? '<span>('+item.countSubgrupos+' subgrupos)</span>' : '<span>('+item.countSubgrupos+' subgrupo)</span>';
											}else if(item.countUsers){
												conteudoFavorito += item.countUsers > 1 ? '<span>('+item.countUsers+' membros)</span>' : '<span>('+item.countUsers+' membro)</span>' ;
											}else{
												conteudoFavorito += '<br/>';
											}
											
											if(item.virtual){
												conteudoFavorito += '<span class="font-blue"><i class="fa fa-desktop"></i>&nbsp; Grupo virtual</span>';
											}
										conteudoFavorito += '</p>';
										
									}
									
									if(item.userAdmin){
										conteudoFavorito += '<p class="second">Admin: '+item.userAdmin+'</p>';
									}
									if(PARAMS.TIPO_GRUPO_ON == '1' && !item.virtual){
										if(item.id != -1){
											conteudoFavorito += '<p class="second">';
												switch(item.tipo){
													case 'ABERTO':
														conteudoFavorito += 'Aberto';
														break;
													case 'ABERTO_FECHADO':
														conteudoFavorito += 'Aberto com Aprovação';
														break;
													case 'PRIVADO_OPCIONAL':
														conteudoFavorito += 'Privado Opcional';
														break;
													case 'PRIVADO':
														conteudoFavorito += 'Privado';
														break;
													default:
														conteudoFavorito += item.tipo;
												}
											conteudoFavorito += '</p>';
										}
									}
									
								conteudoFavorito += '</div>';
							conteudoFavorito += '</div>';
							
							conteudoFavorito += '<div class="clearfix acoes-grupo" data-grupoId="'+item.id+'">';
								conteudoFavorito += '<a class="sbold font-blue" href="'+urlContext+'/pages/mural.htm?grupo='+item.id+'">ver publicações</a>';
								if(PARAMS.CHAT_ON && PARAMS.CHAT_ON == '1' && !item.virual){
									if(SESSION.PERMISSOES.ENVIAR_MENSAGEM == 'true'){
										conteudoFavorito += '<a class="sbold font-blue-madison" href="'+urlContext +'/pages/chat.htm?grupo='+item.id+'">conversar</a>';
									}
								}
							conteudoFavorito += '</div>';
							
						conteudoFavorito += '</li>';
					}
					
					if(item.participa && !item.favorito) {
						conteudoParticipo += '<li data-id="'+item.id+'">';
							
							conteudoParticipo += '<div class="clearfix">';
								conteudoParticipo += '<div class="grupo-pic-holder">';
									conteudoParticipo += '<div class="fotoUsuario media">';
										conteudoParticipo += '<a href="'+urlContext + '/pages/mural.htm?grupo='+item.id+'" class="centraliza">';
											conteudoParticipo += '<img src="'+grupoImg+'" alt="Foto do grupo '+item.nome+'" />';
										conteudoParticipo += '</a>';
									conteudoParticipo += '</div>';
									if(item.id != -1){
										conteudoParticipo += '<i class="fa fa-star css-favorito-not" data-favorito="false"></i>';
									}
								conteudoParticipo += '</div>';
								
								conteudoParticipo += '<div class="infoUsuarios">';
									if(item.id == -1) {
										conteudoParticipo += '<p class="first">'+item.nome+'</p>';
									} else {
										conteudoParticipo += '<p class="first"><a class="font-black" href="'+urlContext + '/grupo.htm?id='+item.id+'">'+item.nome+'</a>';
											if(item.virtual && item.countSubgrupos){
												conteudoParticipo += item.countSubgrupos > 1 ? '<span>('+item.countSubgrupos+' subgrupos)</span>' : '<span>('+item.countSubgrupos+' subgrupo)</span>';
											}else if(item.countUsers){
												conteudoParticipo += item.countUsers > 1 ? '<span>('+item.countUsers+' membros)</span>' : '<span>('+item.countUsers+' membro)</span>' ;
											}else{
												conteudoParticipo += '<br/>';
											}
											
											if(item.virtual){
												conteudoParticipo += '<span class="font-blue"><i class="fa fa-desktop"></i>&nbsp; Grupo virtual</span>';
											}
										conteudoParticipo += '</p>';
									}
									
									if(item.userAdmin){
										conteudoParticipo += '<p class="second">Admin: '+item.userAdmin+'</p>';	
									}
									if(PARAMS.TIPO_GRUPO_ON == '1' && !item.virtual){
										if(item.id != -1){
											conteudoParticipo += '<p class="second">';
												switch(item.tipo){
													case 'ABERTO':
														conteudoParticipo += 'Aberto';
														break;
													case 'ABERTO_FECHADO':
														conteudoParticipo += 'Aberto com Aprovação';
														break;
													case 'PRIVADO_OPCIONAL':
														conteudoParticipo += 'Privado Opcional';
														break;
													case 'PRIVADO':
														conteudoParticipo += 'Privado';
														break;
													default:
														conteudoParticipo += item.tipo;
												}
											conteudoParticipo += '</p>';
										}
									}
									
								conteudoParticipo += '</div>';
							conteudoParticipo += '</div>';
							
							conteudoParticipo += '<div class="clearfix acoes-grupo" data-grupoId="'+item.id+'">';
								conteudoParticipo += '<a class="sbold font-blue" href="'+urlContext+'/pages/mural.htm?grupo='+item.id+'">ver publicações</a>';
								if(PARAMS.CHAT_ON && PARAMS.CHAT_ON == '1' && !item.virtual){
									if(SESSION.PERMISSOES.ENVIAR_MENSAGEM == 'true'){
										conteudoParticipo += '<a class="sbold font-blue-madison" href="'+urlContext +'/pages/chat.htm?grupo='+item.id+'">conversar</a>';
									}
								}
							conteudoParticipo += '</div>';
								
						conteudoParticipo += '</li>';
					} 
					
					if(!item.participa){
						conteudoNaoParticipo += '<li data-id="'+item.id+'">';
						
							conteudoNaoParticipo += '<div class="clearfix">';
								conteudoNaoParticipo += '<div class="fotoUsuario media">';
									conteudoNaoParticipo += '<a href="'+urlContext + '/pages/mural.htm?grupo='+item.id+'" class="centraliza">';
										conteudoNaoParticipo += '<img src="'+grupoImg+'" alt="Foto do grupo '+item.nome+'" />';
									conteudoNaoParticipo += '</a>';
								conteudoNaoParticipo += '</div>';
								
								conteudoNaoParticipo += '<div class="infoUsuarios">';
									if(item.id == -1) {
										conteudoNaoParticipo += '<p class="first">'+item.nome+'</p>';
									} else {
										conteudoNaoParticipo += '<p class="first"><a class="font-black" href="'+urlContext + '/grupo.htm?id='+item.id+'">'+item.nome+'</a>';
											if(item.virtual && item.countSubgrupos){
												conteudoNaoParticipo += item.countSubgrupos > 1 ? '<span>('+item.countSubgrupos+' subgrupos)</span>' : '<span>('+item.countSubgrupos+' subgrupo)</span>';
											}else if(item.countUsers){
												conteudoNaoParticipo += item.countUsers > 1 ? '<span>('+item.countUsers+' membros)</span>' : '<span>('+item.countUsers+' membro)</span>' ;
											}else{
												conteudoNaoParticipo += '<br/>';
											}
											
											if(item.virtual){
												conteudoNaoParticipo += '<span class="font-blue"><i class="fa fa-desktop"></i>&nbsp; Grupo virtual</span>';
											}
										conteudoNaoParticipo += '</p>';
										
									}
									
									if(item.userAdmin){
										conteudoNaoParticipo += '<p class="second">Admin: '+item.userAdmin+'</p>';	
									}
									if(PARAMS.TIPO_GRUPO_ON == '1' && !item.virtual){
										if(item.id != -1){
											conteudoNaoParticipo += '<p class="second">';
												switch(item.tipo){
													case 'ABERTO':
														conteudoNaoParticipo += 'Aberto';
														break;
													case 'ABERTO_FECHADO':
														conteudoNaoParticipo += 'Aberto com Aprovação';
														break;
													case 'PRIVADO_OPCIONAL':
														conteudoNaoParticipo += 'Privado Opcional';
														break;
													case 'PRIVADO':
														conteudoNaoParticipo += 'Privado';
														break;
													default:
														conteudoNaoParticipo += item.tipo;
												}
											conteudoNaoParticipo += '</p>';
										}
									}
								conteudoNaoParticipo += '</div>';
							conteudoNaoParticipo += '</div>';
							
							conteudoNaoParticipo += '<div class="clearfix acoes-grupo" data-grupoId="'+item.id+'">';
							if(PARAMS.TIPO_GRUPO_ON == '1' && !item.virtual){
								if(PARAMS.MURAL_GRUPO_PARTICIPAR_ON && PARAMS.MURAL_GRUPO_PARTICIPAR_ON == '1'){
									if(item.tipo == 'ABERTO_FECHADO'){
										conteudoNaoParticipo += '<a class="sbold font-green-sharp btn-group-participar">solicitar participação</a>';
									}else{
										conteudoNaoParticipo += '<a class="sbold font-green-sharp btn-group-participar">entrar</a>';
									}
								}
							}
							conteudoNaoParticipo += '</div>';
							
						conteudoNaoParticipo += '</li>';
					}
				}
			}
			
			if(conteudoFavorito == "" && conteudoParticipo == "" && conteudoNaoParticipo == "") {
				conteudoParticipo = '<li class="list-group-item text-center error sbold">Nenhum resultado encontrado</li>';
			}
			
			var sideBoxGrupos = $('#sideBox-grupos').find('.scroll-list .col-xs-12');
			$(sideBoxGrupos).empty();
			
			if(conteudoFavorito != ""){
				var favoritos = '<div class="clearfix favoritos">';
					favoritos += '<div class="row toggle-list clearfix cursor-pointer" data-lista="lista-favoritos">';
					favoritos += '<label class="labelCampo control-label pull-left cursor-pointer">Grupos favoritos</label>';
					favoritos += '<div class="icon-holder"><i class="fa fa-angle-up"></i></div>';
					favoritos += '</div>';
					favoritos += '<ul class="row list-group list-group-grupos listaGrupos lista-favoritos clearfix list-holder">';
						favoritos += conteudoFavorito;
					favoritos += '</ul>';
				favoritos += '</div>'
				sideBoxGrupos.append(favoritos);
			}
			
			if(conteudoParticipo != ""){
				var gruposParticipo = '<div class="clearfix comPost">';
				gruposParticipo += '<div class="row toggle-list clearfix cursor-pointer" data-lista="lista-participo">';
				if(!nome){
					gruposParticipo += '<label class="labelCampo control-label pull-left cursor-pointer">Grupos que participo</label>';
				}else{
					gruposParticipo += '<label class="labelCampo control-label pull-left cursor-pointer">Grupos</label>';
				}
				gruposParticipo += '<div class="icon-holder"><i class="fa fa-angle-up"></i></div>';
				gruposParticipo += '</div>';
					gruposParticipo += '<ul class="row list-group list-group-grupos listaGrupos lista-participo clearfix list-holder">'+conteudoParticipo+'</ul>';
				gruposParticipo += '</div>';
				sideBoxGrupos.append(gruposParticipo);
			}
			
			if(conteudoNaoParticipo != ""){
				var gruposNaoParticipo = '<div class="clearfix semPost">';
					gruposNaoParticipo += '<div class="row toggle-list clearfix cursor-pointer" data-lista="lista-nao-participo">';
					gruposNaoParticipo += '<label class="labelCampo control-label pull-left cursor-pointer">Grupos que não participo</label>';
					gruposNaoParticipo += '<div class="icon-holder"><i class="fa fa-angle-up"></i></div>';
					gruposNaoParticipo += '</div>';
					gruposNaoParticipo += '<ul class="row list-group list-group-grupos listaGrupos lista-nao-participo clearfix list-holder">'+conteudoNaoParticipo+'</ul>';
				gruposNaoParticipo += '</div>';
				sideBoxGrupos.append(gruposNaoParticipo);
			}else{
				$('.clearfix.semPost').hide();
			}
			
			$("#buscaSideBarGrupos").removeClass("ac_loading");
			highlightGrupo();
			if(obj.isScrollSidebox){
				var totalTime = new Date().getTime() - ajaxTime;
				timerScrollPosition(totalTime);
			}
		}
	});
}

function buscaUsuariosSideBox(obj) {
	var ajaxTime;
	
	if (typeof obj === "undefined") {
		obj = {};
	}
	if(obj.pageScroll){
		var page = obj.pageScroll;
	}else{
		var page = 0;
	}
	var nome = obj.nome ? obj.nome : null;
	if(ajaxBuscaUsuario != null) {
		ajaxBuscaUsuario.abort();
	}
	ajaxBuscaUsuario = jsHttp.post(urlContext + "/ws/usuariosAutoComplete.htm", {
		data: {
			wstoken: SESSION.USER_INFO.USER_WSTOKEN,
			q: nome,
			maxRows: 20,
			page: page,
			not_users_ids: userId
		},
		contentType: jsHttp.CONTENT_TYPE_FORM,
		beforeSend: function(data){
			if(obj.isScrollSidebox){
				ajaxTime = new Date().getTime();
			}
			if(nome){
				$("#buscaSideBarUsuarios").addClass("ac_loading");
			}
			if(obj.isSpinner){
				$('#sideBox-usuarios').find('.scroll-list .col-xs-12 .spinner').fadeIn();
			}
		},
		success: function(data) {
            if(typeof sem != 'undefined'){
                sem = 0;
            }
			var conteudo = "";
			if(data && data.list) {
				if(data.list.length == 0){
					if(obj.isShowNoResults && page == 0){
						conteudo += '<li class="sbold text-center">Nenhum resultado encontrado</li>';
					}
				}
				
				for(var i in data.list) {
					var item = data.list[i];
					if(item.id != userId){
						conteudo += '<li data-id="'+item.id+'">';
						
							if(PARAMS.CHAT_ON && PARAMS.CHAT_ON == '1') {
								if(SESSION.PERMISSOES.ENVIAR_MENSAGEM == 'true'){
									conteudo += '<div class="user-status-chat">';
										if(item.chatOn) {
											conteudo += '<span class="online"></span>'
										} else {
											conteudo += '<span class=""></span>'
										}
									conteudo += '</div>';
								}
							}
						
							conteudo += '<div class="clearfix">';
								conteudo += '<div class="fotoUsuario media">';
									conteudo += '<span class="status-online"></span>';
									conteudo += '<a href="'+urlContext+'/pages/mural.htm?user_post_id='+item.id+'" class="centraliza">';
										conteudo +='<img src="'+item.urlFotoUsuarioThumb+'" alt="Foto do '+item.nome+'" />';
									conteudo += '</a>';
								conteudo += '</div>';
							
								conteudo += '<div class="infoUsuarios">';
									conteudo += '<p class="first"><a class="font-black" href="'+urlContext+'/usuario.htm?id='+item.id+'">'+item.nome+'</a></p>';
									if(item.funcao){
										conteudo += '<p class="second">'+item.funcao+'</p>';
									}
								conteudo += '</div>';
							conteudo += '</div>';
							
							conteudo += '<div class="clearfix acoes-user user-friendship" data-user="'+item.id+'">';
								conteudo += '<a class="sbold font-blue" href="'+urlContext+'/pages/mural.htm?user_post_id='+item.id+'">ver publicações</a>';
								
								if(PARAMS.MURAL_AMIZADE_ON && PARAMS.MURAL_AMIZADE_ON == 1){
									if(item.amigo == ""){
										conteudo += '<a class="sbold font-green-sharp btn-user-friendship" data-solicita="true">+ conexão</a>';
									}else if(item.amigo == "AGUARDANDO_APROVACAO"){
										conteudo += '<a class="sbold font-yellow-gold">aguardando aprovação</a>';
									}else if(item.amigo == "SOLICITADO"){
										conteudo += '<a class="sbold font-yellow-gold">aguardando sua aprovação</a>';
									}
								}
								if(PARAMS.CHAT_ON && PARAMS.CHAT_ON == '1'){
									if(SESSION.PERMISSOES.ENVIAR_MENSAGEM == 'true'){
										conteudo += '<a class="sbold font-blue-sharp" href="'+urlContext+'/pages/chat.htm?user='+item.id+'">conversar</a>';
									}
								}
							conteudo += '</div>';
							
						conteudo +='</li>';
					}
				}
			}
			
			var sideBoxUsers = $('#sideBox-usuarios').find('.scroll-list .col-xs-12 ul');
			
			if(conteudo != ""){
				sideBoxUsers.append(conteudo);
				if(obj.isSpinner){
					$('#sideBox-usuarios').find('.scroll-list .col-xs-12 .spinner').fadeOut();
				}
			}
			$("#buscaSideBarUsuarios, #buscaAmigosSideBox").removeClass("ac_loading");
			highlightUser();
			
			if(obj.isScrollSidebox){
				var totalTime = new Date().getTime() - ajaxTime;
				timerScrollPosition(totalTime);
			}
		}
	});
}

function buscaAmigos(obj) {
	var ajaxTime;
	
	if(obj.pageScroll){
		var page = obj.pageScroll;
	}else{
		var page = 0;
	}
	
	var nome = obj.nome ? obj.nome : null;
	if(ajaxBuscaUsuario != null) {
		ajaxBuscaUsuario.abort();
	}
	ajaxBuscaUsuario = jsHttp.get(urlContext + "/rest/amizade/solicitacao/amigos/" + userId, {
		data: {
			wstoken: "$!session.userInfo.wstoken",
			nome: nome,
			maxRows: 20,
			page: page,
		},
		contentType: jsHttp.CONTENT_TYPE_FORM,
		beforeSend: function(data){
			if(obj.isScrollSidebox){
				ajaxTime = new Date().getTime();
			}
			
			if(nome){
				$("#buscaAmigosSideBox").addClass("ac_loading");
			}
			if(obj.isSpinner){
				$('#sideBox-amigos').find('.scroll-list .col-xs-12 .spinner').fadeIn();
			}
		},
		success: function(data) {
            if(typeof sem != 'undefined'){
                sem = 0;
            }
			var conteudoNotAmigo = "";
			var conteudoAmigo = "";
			if(data && data.entity) {
				if(data.entity.length == 0){
					conteudoAmigo += '<li class="sbold text-center">Nenhum resultado encontrado</li>';
				}
				
				if(data.entity.length > 0 && obj.isShowPosts){
					var x = jQuery(location).attr('href');
						x = x.substr(x.lastIndexOf('/') + 1);
					if(x == 'mural.htm' || x == 'mural.htm?clear=true' || x.lastIndexOf('jsessionid') > -1){
						var filtro = {};
						filtro.meusAmigos = true;
						filtro.somenteEu = false;
						LiveMural.buscaPosts(filtro, true, modeMural);
						$('#form_visibilidade').val(1).change();
					}
				}
				
				for(var i in data.entity) {
					var item = data.entity[i];
					if(item.id != userId){
						conteudoAmigo += '<li data-id="'+item.id+'">';
						
							if(PARAMS.CHAT_ON && PARAMS.CHAT_ON == '1') {
								if(SESSION.PERMISSOES.ENVIAR_MENSAGEM == 'true'){
									conteudoAmigo += '<div class="user-status-chat">';
										if(item.chatOn) {
											conteudoAmigo += '<span class="online"></span>'
										} else {
											conteudoAmigo += '<span class=""></span>'
										}
									conteudoAmigo += '</div>';
								}
							}
						
							conteudoAmigo += '<div class="clearfix">';
								conteudoAmigo += '<div class="fotoUsuario media">';
									conteudoAmigo += '<span class="status-online"></span>';
									conteudoAmigo += '<a href="'+urlContext+'/pages/mural.htm?user_post_id='+item.id+'" class="centraliza">';
										conteudoAmigo +='<img src="'+item.urlFotoUsuarioThumb+'" alt="Foto do '+item.nome+'" />';
									conteudoAmigo += '</a>';
								conteudoAmigo += '</div>';
								
								conteudoAmigo += '<div class="infoUsuarios">';
									conteudoAmigo += '<p class="first"><a class="font-black" href="'+urlContext+'/usuario.htm?id='+item.id+'">'+item.nome+'</a></p>';
									if(item.funcao){
										conteudoAmigo += '<p class="second">'+item.funcao+'</p>';
									}
								conteudoAmigo += '</div>';
							conteudoAmigo += '</div>';
							
							conteudoAmigo += '<div class="clearfix acoes-user user-friendship" data-user="'+item.id+'">';
								conteudoAmigo += '<a class="sbold font-blue" href="'+urlContext+'/pages/mural.htm?user_post_id='+item.id+'">ver publicações</a>';
								if(PARAMS.CHAT_ON && PARAMS.CHAT_ON == '1'){
									if(SESSION.PERMISSOES.ENVIAR_MENSAGEM == 'true'){
										conteudoAmigo += '<a class="sbold font-blue-sharp" href="'+urlContext+'/pages/chat.htm?user='+item.id+'">conversar</a>';
									}
								}
							conteudoAmigo += '</div>';
							
						conteudoAmigo +='</li>';
					}
				}
			}
			
			var sideBoxAmigos = $('#sideBox-amigos').find('.scroll-list .col-xs-12 ul');
			
			if(conteudoAmigo != ""){
				sideBoxAmigos.append(conteudoAmigo);
				if(obj.isSpinner){
					$('#sideBox-amigos').find('.scroll-list .col-xs-12 .spinner').fadeOut();
				}
			}
			
			$("#buscaSideBarUsuarios, #buscaAmigosSideBox").removeClass("ac_loading");
			highlightUser();
			
			if(obj.isScrollSidebox){
				var totalTime = new Date().getTime() - ajaxTime;
				timerScrollPosition(totalTime);
			}
		}
	});
}

function buscaCategorias(nome, obj) {
	if(!obj){
		obj = {};
	}
	
	var ajaxTime;
	
	var nome = nome ? nome : null;
	if(ajaxBuscaCategorias != null) {
		ajaxBuscaCategorias.abort();
	}
	
	ajaxBuscaCategorias = jsHttp.get(urlContext + "/rest/v1/categoria/autoComplete", {
		data: {
			nome: nome,
			max: null
		},
		beforeSend: function(data){
			if(obj.isScrollSidebox){
				ajaxTime = new Date().getTime();
			}
			
			if(nome){
				$("#buscaSideBarCategorias").addClass("ac_loading");
			}
			$('#sideBox-categorias .scroll-list .col-xs-12 .spinner').fadeIn();
		},
		success: function(data) {
			var conteudo = "",
				conteudoBuscaHeader = '';
			if(data) {
				for(var i in data) {
					var item = data[i];
					var cor = "#0096d0";
					if(item.cor) {
						if(item.cor == '0x000000'){
							cor = "#0096d0";
						}else{
							cor = item.cor.indexOf("#") > -1 ? item.cor : "#"+item.cor;
						}
					}
					
					conteudo += '<li class="list-group-item-sideBox bg-white text-center" data-cor="'+item.cor+'" data-id="'+item.id+'" data-name="'+item.nome+'" data-cod="'+item.codigo+'" data-userCreate="'+item.userCreate+'" data-userCreateID="'+item.userCreateId+'" ';
					
					if(item.categoriaPai){
						conteudo += ' data-pai0-id="'+item.categoriaPai.id+'" data-pai0-cod="'+item.categoriaPai.codigo+'" data-pai0-nome="'+item.categoriaPai.nome+'"';
					}
					
					if(item.categoriaPai && item.categoriaPai.categoriaPai){
						conteudo += ' data-pai1-id="'+item.categoriaPai.categoriaPai.id+'" data-pai1-cod="'+item.categoriaPai.categoriaPai.codigo+'" data-pai1-nome="'+item.categoriaPai.categoriaPai.nome+'"';
					}
					
					if(cor == "#FFF" || cor == "#fff" || cor == "#FFFFFF" || cor == "#ffffff"){
		    			conteudo += '><a style="color:'+cor+'; text-shadow: 0 0 4px #000;" href="'+urlContext+'/pages/mural.htm?categoria='+item.id+'">'+item.nome+'</a>';
		    		}else{
						conteudo += '><a style="color:'+cor+'" href="'+urlContext+'/pages/mural.htm?categoria='+item.id+'">'+item.nome+'</a>';
		    		}
					
					conteudo += '</li>';
					
					conteudoBuscaHeader += '<option value="'+item.id+'">'+item.nome+'</option>';
				}
			}
			
			if(conteudo == "") {
				conteudo = '<li class="list-group-item text-center error sbold">Nenhum resultado encontrado</li>';
			}
			
			if($('#filtroCategoria_id').parent().find('.select2').length > 0)
				$('#filtroCategoria_id').select2('destroy');
			
			$('#filtroCategoria_id option').not('.placeholder').remove();
			$('#filtroCategoria_id').append(conteudoBuscaHeader).select2();
			$('.list-group-categorias').empty().append(conteudo);
			$('#sideBox-categorias .scroll-list .col-xs-12 .spinner').fadeOut();
			$("#buscaSideBarCategorias").removeClass("ac_loading");
			
			if(!obj.isIdiomas)
				tooltipCateg("ul.list-group-categorias > li");
			else
				tooltipCategIdiomas("ul.list-group-categorias > li");
			
			highlightCateg();
			
			if(obj.isScrollSidebox){
				var totalTime = new Date().getTime() - ajaxTime;
				timerScrollPosition(totalTime);
			}
		}
	});
}

function buscaTags(nome, obj) {
	if(!obj){
		obj = {};
	}
	
	var ajaxTime;
	
	var nome = nome ? nome : null;
	if(ajaxBuscaTags != null) {
		ajaxBuscaTags.abort();
	}
	ajaxBuscaTags = jsHttp.post(urlContext + "/ws/tags.htm", {
		data: {
			wstoken: "$!session.userInfo.wstoken",
			nome: nome,
			max: null
		},
		contentType: jsHttp.CONTENT_TYPE_FORM,
		beforeSend: function(data){
			if(obj.isScrollSidebox){
				ajaxTime = new Date().getTime();
			}
			
			if(nome){
				$("#buscaSideBarTags").addClass("ac_loading");
			}
			$('#sideBox-tags .scroll-list .col-xs-12 .spinner').fadeIn();
		},
		success: function(data) {
			var conteudo = "";
			if(data && data.list) {
				for(var i in data.list) {
					var item = data.list[i];
					if(item.nome && item.id) {
						conteudo += '<li class="list-group-item-sideBox grey text-center" data-id="'+item.id+'" data-name="'+item.nome+'" data-userCreate="'+item.userCreate+'" data-userCreateID="'+item.userCreateId+'"><a href="'+urlContext+'/pages/mural.htm?tag='+item.id+'">'+item.nome+'</a></li>';
					}
				}
			}
			
			if(conteudo == "") {
				conteudo = '<li class="list-group-item text-center error sbold">Nenhum resultado encontrado</li>';
			}
			$('.list-group-tags').empty();
			$('.list-group-tags').append(conteudo);
			$('#sideBox-tags .scroll-list .col-xs-12 .spinner').fadeOut();
			$("#buscaSideBarTags").removeClass("ac_loading");
			tooltipTag("ul.list-group-tags > li");
			highlightTag();
			
			if(obj.isScrollSidebox){
				var totalTime = new Date().getTime() - ajaxTime;
				timerScrollPosition(totalTime);
			}
		}
	});
}

var pageScroll1 = 0;

$('document').ready(function() {
	
	//scroll lista usuarios
	var pageScroll2 = 0;
	$("#sideBox-usuarios .scroll-list, #sideBox-amigos .scroll-list").unbind('scroll');
	if($("#buscaSideBarUsuarios").val() != ""){
		var q = $("#buscaSideBarUsuarios").val();
	}
	if($("#buscaAmigosSideBox").val() != ""){
		var am = $("#buscaAmigosSideBox").val();
	}
	sem = 0; // Semaforo de controle de região crítica
	$("#sideBox-usuarios .scroll-list").on('scroll', function(){
		var x = $("#sideBox-usuarios .list-holder").height() - $(this).height();
        var isExecute = ($(this).scrollTop() - 19) >= x;
        if(isExecute && sem === 0) {
            sem++;
            pageScroll1++;
            var obj = {
            	isSpinner: false,
            	nome: q,
            	pageScroll: pageScroll1
            };
            buscaUsuariosSideBox(obj);
        }
	});
	
	function clear() {
		$('.list-group-categorias li').removeClass('ativo');
		$('.list-group-tags li').removeClass('ativo');
		$('.list-group-grupos li').removeClass('active');
		$('.list-group-usuarios li').removeClass('active');
		$('.listaLinks li').removeClass('active');
		$('.lista-arquivos li').removeClass('active');
		$('.notification').removeClass('active');
	}
	
	$('.sidebox-nav-tabs li a:not(.mf), .icones-header li a').click(function(){
		var ma = $('ul.listaMural > li.meus-amigos').length;
		if(ma != 0){
			$('#form_visibilidade').val(0).change();
			LiveMural.buscaPosts('', true, modeMural);
		}
	});
	
	$('#sideBox-link-categorias').click(function() {
		buscaCategorias(null, { isIdiomas: true });
		addSideBoxActive("tabCategorias");
	});
	
	$('#buscaSideBarCategorias').keyup(function() {
		buscaCategorias($(this).val(), { isIdiomas: true });
	});
	
	$("#sideBox-link-tags").click(function(e) {
		buscaTags();
		addSideBoxActive("tabTags");
	});
	
	$('#buscaSideBarTags').keyup(function(e) {
		buscaTags($(this).val());
	});
	
	$('body').on('click', '.header-links.reminder', function(){
		addSideBoxActive("tabReminders");
		reminders.getReminders({
			data: { page: 0, maxRows: 10 }
		});
	});
	
	$('body').on('click', '.list-group-categorias li', function(e) {
		var categoriaId = $(this).attr('data-id');
		if(categoriaId) {
			if(PARAMS.SIDEBOX_RELOAD_AJAX == '1') {
				e.preventDefault();
				clear();
				
				$('.mural-header.col-md-12.grupo').fadeOut();
				
				$(this).addClass('ativo');
				LiveMural.buscaPosts({
					categoria_id: categoriaId
				}, true);
				$("#categoria").val(categoriaId).change();
				
				var pagebar = $('.page-bar.mural');
				$(pagebar).find('.page-bar-second').text('Categoria');
				$(pagebar).find('.page-bar-third').text($(this).find('a').text());
				if(!$(pagebar).is(':visible')) {
					$(pagebar).slideDown();
				}
			} else {
				location.href = urlContext + '/pages/mural.htm?categoria=' + categoriaId;
			}
		}
	});
	
	/***
	 * TAGS
	 */
	$('body').on('click', '.list-group-tags li', function(e) {
		var tagId = $(this).attr('data-id');
		if(tagId) {
			if(PARAMS.SIDEBOX_RELOAD_AJAX == '1') {
				e.preventDefault();
				clear();
				$('.mural-header.col-md-12.grupo').fadeOut();
				$(this).addClass('ativo');
				var tagName = $(this).find('a').text();
				LiveMural.buscaPosts({
					tags: tagId,
				}, true);
				
				$('.interacoesMural #tags').tokenInput('clear');
				$('.interacoesMural #tags').tokenInput("add", {id: tagId, nome: tagName});
				$('.post-tags').find('.mascaraLabel').hide();
				
				var pagebar = $('.page-bar.mural');
				$(pagebar).find('.page-bar-second').text('Tag');
				$(pagebar).find('.page-bar-third').text(tagName);
				if(!$(pagebar).is(':visible')) {
					$(pagebar).slideDown();
				}
			} else {
				location.href = urlContext + '/pages/mural.htm?tag=' + tagId;
			}
		}
	});
	
	/***
	 * USUARIOS
	 */
	$("#sideBox-link-usuarios").click(function() {
		if(!$(this).hasClass('active')){
			$('#sideBox-usuarios').find('.scroll-list .col-xs-12 ul').empty();
			var obj = {
				isSpinner: true
			};
			buscaUsuariosSideBox(obj);
			addSideBoxActive("tabUsuarios");
		}
	});
	$('#buscaSideBarUsuarios').keyup(function(e) {
		$('#sideBox-usuarios').find('.scroll-list .col-xs-12 ul').empty();
		var nome = $(this).val();
		var obj = {
			isSpinner: true,
			nome: nome,
			isShowNoResults: true
		};
		buscaUsuariosSideBox(obj);
	});
	
	$("#sideBox-link-amigos").click(function() {
		if(!$(this).hasClass('active')){
			$('#sideBox-amigos').find('.scroll-list .col-xs-12 ul').empty();
			var obj = {
				isSpinner: true,
				isShowPosts: true
			}
			buscaAmigos(obj);
			addSideBoxActive("tabAmigos");
		}
		
	});
	$('#buscaAmigosSideBox').keyup(function(e) {
		$('#sideBox-amigos').find('.scroll-list .col-xs-12 ul').empty();
		var nome = $(this).val();
		var obj = {
			nome: nome,
			isSpinner: true,
			isShowPosts: false
		}
		buscaAmigos(obj);
	});
	
	$('body').on('click', '.list-group-usuarios li', function(e) {
		var user_post_id = $(this).attr('data-id');
		if(user_post_id) {
			if(PARAMS.SIDEBOX_RELOAD_AJAX == '1') {
				e.preventDefault();
				clear();
				$(this).addClass('active');
				
				LiveMural.buscaPosts({
					user_post_id: user_post_id
				}, true);
			} else {
				location.href = urlContext + '/pages/mural.htm?user_post_id=' + user_post_id;
			}
		} 
	});
	
	$('.aprovar-amizade, .reprovar-amizade').click(function(e){
		e.preventDefault();
		var obj = {
			usuarioId: userId,
			amigoId: $(this).attr('data-id'),
		}
		
		if($(this).hasClass('aprovar-amizade'))
			obj.aprovar = true;
		else
			obj.aprovar = false;
		
		responderAmizade(obj, function(data){
			if(data.status == 'ERROR'){
				toastr.error("<b>Atenção!</b><br>"+data.message);
			}else{
				toastr.success(data.message);
				location.reload();
			}
		})
	});
	
	/***
	 * GRUPOS
	 */
	$("#sideBox-link-grupos").click(function(e) {
		if(!$(this).hasClass('active')){
			buscaGrupos({});
			addSideBoxActive("tabGrupos");
		}
	});
	
	$('#buscaSideBarGrupos').keyup(function(e) {
		var q = $(this).val();
		var tipo = $('select#tipo-grupo option:selected').val();
		var obj = {
			nome: q,
			tipo: tipo
		}
		buscaGrupos(obj);
	});
	
	$('#tipo-grupo').change(function(e) {
		var q = $('#buscaSideBarGrupos').val();
		var tipo = $(this).children('option:selected').val();
		var obj = {
			nome: q,
			tipo: tipo
		}
		buscaGrupos(obj);
	});
	
	$('#participo-grupo').change(function(e) {
		var val = $(this).children('option:selected').val;
		var filtro = $(this).children('option:selected').attr('data-tipo-filtro');
		
		var q = $('#buscaSideBarGrupos').val();
		var tipo = $('#tipo-grupo option:selected').val();
		var obj = {
			nome: q,
			tipo: tipo
		}
		
		if(filtro == 'meusGrupos'){
			obj.meus_grupos = 1;
		}else if(filtro == 'participo'){
			obj.participo = 1;
		}else{
			obj.meus_grupos = 0;
			obj.participo = 0;
		}
		
		buscaGrupos(obj);
	});
	
	
	$('body').on('click', '.list-group-grupos li', function(e) {
		var grupo_ids = $(this).attr('data-id');
		if(grupo_ids) {
			if(PARAMS.SIDEBOX_RELOAD_AJAX == '1') {
				e.preventDefault();
				clear();
				$(this).addClass('active');
				
				LiveMural.buscaPosts({
					grupo_ids: grupo_ids
				}, true);
			} else {
				location.href = urlContext + '/pages/mural.htm?grupo='+ grupo_ids;
			}
		}
	});
	
	$('.page-bar.mural .page-bar-first').click(function(e) {
		if(PARAMS.SIDEBOX_RELOAD_AJAX == '1') {
			e.preventDefault();
			clear();
			$('.page-bar.mural').fadeOut();
			$('.mural-header.col-md-12.grupo').fadeOut();
			LiveMural.buscaPosts({}, true);
		} else {
			location.href = urlContext + '/pages/mural.htm';
		}
	});
	
	$("body").on("click", ".toggle-list", function(e){
		var obj = $(this);
		var i = obj.children('.icon-holder').find('i');
		var lista = $(this).attr("data-lista");
		if(i.hasClass("fa-angle-down")){
			$("body").find("ul.list-group."+lista).slideDown();
			i.addClass("fa-angle-up").removeClass("fa-angle-down");
		}else{
			$("body").find("ul.list-group."+lista).slideUp();
			i.removeClass("fa-angle-up").addClass("fa-angle-down");
		}
	});
	
	$("body").on("click", ".toggle-list a", function(event){
		event.stopPropagation();
	});
	
	$("body").on("click", ".group-favoritar a", function(e){
		var grupoFavoritarID = "";
		var idGrupoURL = getURLParameter("grupo");	//mural
		var id = getURLParameter("id"); 			//cadastro

		if(idGrupoURL){
			grupoFavoritarID = idGrupoURL;
		}else{
			grupoFavoritarID = id;
		}
		
		favoritarRequest($(e.target).closest("a"), grupoFavoritarID, function( data ) {
			buscaGrupos({});
			if(data.status == "ERROR"){
	 			toastr.error("<b>Atenção</b><br>" + data.message);
	 		}else{
 	 			location.reload();
	 		}
	 	});
	});
	
	$("body").on("click", ".listaGrupos i.fa-star", function(e){
		e.stopPropagation();
		var id = $(e.target).closest("li").attr("data-id");
		favoritarRequest($(e.target), id, function(data){
			var favorito = $(e.target).attr("data-favorito");
			if(favorito == 'false') {
				location.href = urlContext + "/pages/mural.htm?grupo=" + id;
			} else {
				location.reload();
			}
		});
	});
	
	$("body").on("click", ".btn-group-participar", function(){
		var obj = $(this);
		var parent = obj.parent(); 
		if(parent.hasClass("acoes-grupo")){
			var grupoId = parent.attr("data-grupoid"); 
		}else{
			var grupoId = $(".mural-header").attr("data-grupoid");
		}
		participarGrupo(grupoId);
	});
	
	$("body").on("click", ".btn-group-sair", function(){
		var obj = $(this);
		var parent = obj.parent(); 
		if(parent.hasClass("acoes-grupo")){
			var grupoId = parent.attr("data-grupoid"); 
		}else{
			var grupoId = $(".mural-header").attr("data-grupoid");
		}
		if(grupoId){
			jConfirm("Deseja mesmo sair desse grupo?", "Atenção", "deletar", function(r){
				if(r){
					sairGrupo(grupoId);
				}else{
					return false;
				}
			});
		}
	});
	
	$("body").on("click", ".btn-user-friendship", function(){
		var amigoId = $(this).parent('.user-friendship').attr('data-user');
		var status = $(this).attr('data-solicita');
		var obj = {
			amigoId: amigoId,
			solicitacao: status
		};
		amizade(obj);
	});
	
	function favoritarRequest(btn_favoritar, idGrupo, successFunction){
		var favorito_value = btn_favoritar.attr("data-favorito") == 'true';
		jQuery.ajax({
			type: "PUT",
			url: urlContext+"/rest/grupo/favorito",
		 	contentType: jsHttp.CONTENT_TYPE_FORM,		 
		 	data: {
		 		usuarioId: userId,
		 		grupoId: idGrupo,
		 		favorito: !favorito_value
			},
		 	success: successFunction
		 });
	}
	
	
	/**
	 * LINKS
	 */
	$("#buscaLinks").keyup(function() {
		if(interval != null) {
			clearInterval(interval);
		}
		
		interval = setInterval(function() {
			buscaLinks();
			clearInterval(interval);
		}, 500);
	});
	
	$("#sites-conhecidos").change(function() {
		buscaLinks();
	});
	
	$("a.links").click(function(){
		addSideBoxActive("tabLinks");
		buscaLinks();
	});
	
	$('body').on('click', '.listaLinks li', function(e) {
		if($(e.target).is('a')){
			e.stopPropagation();
		} else {
			var postId = $(this).attr('data-postId');
			if(postId) {
				if(PARAMS.SIDEBOX_RELOAD_AJAX == '1') {
					$(this).addClass('active');
					e.preventDefault();
					clear();
					LiveMural.buscaPost(postId);
				} else {
					location.href = urlContext + "/pages/post.htm?id=" + postId;
				}
			}
		}
	});
	
	//copiar link
	$('body').on('click', '.listaLinks > li .copy-to-clipboard', function(e){
		var obj = {
			target: '.listaLinks > li .copy-to-clipboard',
			attribute: 'data-copy'
		}
		copyToClipboard(obj);
	});
	
	/**
	 * ARQUIVOS
	 */
	
	$("a.arquivos").click(function(){
		$("ul.lista-arquivos").empty();
		$("#buscaArquivosData button").removeClass('active');
		$("#buscaArquivosData .allFiles").addClass('active');
		$("#buscarArquivo").val("");
		addSideBoxActive("tabArquivos");
		searchArquivos({});
	});
	
	$("#sideBox_filtro_data").change(function() {
		searchArquivos({});
	});
	$("#sideBox_filtro_extensao").change(function() {
		searchArquivos({});
	});
	
	var interval = null;
	$("#buscarArquivo").keyup(function(event) {
		event.preventDefault();
		if(event.which == 32 || event.which == 18 || event.which == 9 || event.which == 13
				|| event.which == 37 || event.which == 39 || event.which == 38 
				|| event.which == 40 || event.which == 16 || event.which == 39) {
			return;
		}
		$(this).addClass("ac_loading");
		
		if(interval != null) {
			clearInterval(interval);
		}
		
		interval = setInterval(function() {
			searchArquivos({});
			clearInterval(interval);
		}, 500);
	});
	
	$('body').on('click', '.lista-arquivos li, .lista-arquivos li .acoes-user a', function(e) {
		if($(e.target).closest('img').length > 0 || $(e.target).closest('.acoes-user').length > 0){
			if(!$(this).hasClass('file-open-post')){
				e.preventDefault();
			}
		} else {
			var arquivoId = $(this).attr('data-id');
			if(arquivoId) {
				if(PARAMS.SIDEBOX_RELOAD_AJAX == '1') {
					e.preventDefault();
					clear();
					$(this).addClass('active');
					
					LiveMural.buscaPosts({
						arquivo_id: arquivoId
					}, true);
				} else {
					location.href = urlContext + '/pages/mural.htm?arquivo='+ arquivoId;
				}
			}
		}
	});
	
	$('body').on('click', '#modalPreviewFile .download-file, .lista-arquivos > li .download-file', function(){
		var name = $(this).attr('data-file-name');
		var url = $(this).attr('data-file-url');
		var callback = function(){
			$('#modalPreviewFile').modal('hide');
		};
		
		SaveToDisk(url, name, callback);
	});
	
	
	/***
	 * NOTIFICACOES
	 */
	$("#sideBox-link-notificacoes").click(function(e) {
		var paramsNotifications = {
			data: { 
				user_id: SESSION.USER_INFO.USER_ID,
			}, 
			isBadges: true, 
			isClear: true, 
			isScroll: true,
			isShowTabNotification: true,
			isFlipAnimation: true,
			isSpinner: true,
			isShowNoResult: true
		};
		
		var list = SESSION.NOTIFICATIONS.LIST_NOTIFICATIONS && SESSION.NOTIFICATIONS.LIST_NOTIFICATIONS != '' ? SESSION.NOTIFICATIONS.LIST_NOTIFICATIONS : 2;
		paramsNotifications.data.list = list;
		
		notifications.getNotifications(paramsNotifications);
		$('#sideBox-link-solicitacoes').removeClass('active');
		$('#sideBox-link-notificacoes').addClass('active');
	});

	$("#sideBox-link-solicitacoes").click(function(e) {
		$('#sideBox-link-notificacoes').removeClass('active');
		$('#sideBox-link-solicitacoes').addClass('active');
		$('ul.listaSolicitacoes').empty();
		showSolicitacoes();
	});
	
	
	$(".ver-todas-notificacoes").click(function(e){
		if($(this).hasClass('not-link')){
			//list all notifications
			e.preventDefault();
		}else{
			$(".header-links.notificacoes").removeClass("aberto");
			if($(this).parent().hasClass('listHeader')){
				addSideBoxActive("tabNotificacoes");
			}
			
			var isActiveNot = $("#sideBox-notify_solicita").hasClass("active");
			if(isActiveNot) {
				$(".header-links").removeClass("aberto").find(".popMenu").fadeOut();
				return;
			}
		}
		filtroNotifications(1);
		
		var paramsNotifications = {
			data: {
				user_id: SESSION.USER_INFO.USER_ID,
				list: 1
			}, 
			isBadges: true, 
			isClear: true,
			isScroll: true,
			isShowTabNotification: true,
			isFlipAnimation: false,
			isSpinner: true,
			isShowNoResult: true
		};
		notifications.getNotifications(paramsNotifications);
	});
	
	$('.ver-notificacoes-nao-lidas').click(function(){
		filtroNotifications(2);
		var paramsNotifications = {
			data: { 
				user_id: SESSION.USER_INFO.USER_ID,
				list: 2
			}, 
			isBadges: true, 
			isClear:true, 
			isScroll: true,
			isShowTabNotification: true,
			isFlipAnimation: true,
			isSpinner: true,
			isShowNoResult: true
		};
		notifications.getNotifications(paramsNotifications);
	});
	
	$('.ver-notificacoes-lidas').click(function(){
		filtroNotifications(3);
		var paramsNotifications = {
			data: { 
				user_id: SESSION.USER_INFO.USER_ID,
				list: 3
			}, 
			isBadges: true, 
			isClear:true, 
			isScroll: true,
			isShowTabNotification: true,
			isFlipAnimation: true,
			isSpinner: true,
			isShowNoResult: true
		};
		notifications.getNotifications(paramsNotifications);
	});
	
	
	$("#buscaNotificacoes").keyup(function(event) {
		event.preventDefault();
		if(event.which == 32 || event.which == 18 || event.which == 9 || event.which == 13
				|| event.which == 37 || event.which == 39 || event.which == 38 
				|| event.which == 40 || event.which == 16 || event.which == 39) {
			return;
		}
		$(this).addClass("ac_loading");
		
		var texto = $(this).val();
		
		if(interval != null) {
			clearInterval(interval);
		}
		interval = setInterval(function() {
			notifications.getNotifications({
				data: {
					user_id: SESSION.USER_INFO.USER_ID,
					texto: texto
				},
				isScroll: true,
				isSpinner: true,
				isClear: true,
				isShowNoResult: true
			});
			clearInterval(interval);
		},500);
	});
	
	$('body').on('click', '.notification', function(event) {
		var postId = $(this).attr("data-postId");
		if(postId) {
			if(PARAMS.SIDEBOX_RELOAD_AJAX == '1') {
				event.preventDefault();
				clear();
				$('.mural-header.col-md-12.grupo').fadeOut();
				$(this).addClass('active');
				var tipo = $(this).attr('data-tipo');
				var abrirComentarios = tipo == 'COMENTARIO' ? true : false;
				LiveMural.buscaPost(postId, abrirComentarios);
			} else {
				location.href = urlContext + "/pages/post.htm?id=" + postId;
			}
		}
	});
	
	if(PARAMS.MURAL_POST_OPCOES && PARAMS.MURAL_POST_OPCOES == "1"){
		//silencia post
		$('body').on('click', '.post-notifications-options', function(){
			var isSilenciar = $(this).hasClass('silenciar-post');
			var obj = {
				element: $(this),
				isSilenciar: isSilenciar,
				data: {
					post : $(this).attr("data-post-id"),
					user: SESSION.USER_INFO.USER_ID,
					status: !isSilenciar
				}
			};
			silenciarPost(obj);
		});
	}
	if(PARAMS.MURAL_POST_TASK && PARAMS.MURAL_POST_TASK == "1"){
		$('body').on('click', '.post-task-options', function(){
			var isAtivo = $(this).hasClass('ativar-task'),
			url = '',
			method = '',
			taskId = $(this).attr('data-task-id');			
			var obj = {
				element: $(this),
				isAtivo: isAtivo 
			};			
			if(isAtivo) {
				obj.url = urlContext + '/rest/postTask';
				obj.method = 'POST'
				obj.data = {
					usuario: { id: SESSION.USER_INFO.USER_ID },				
					post: { id: $(this).attr("data-post-id") },
					notification: isAtivo
				}
			} else {
				obj.url = urlContext + '/rest/postTask/'+taskId
				obj.method = 'DELETE'
			}		
			taskPost(obj);
		});
	}
});