$(document).ready(function(){
	$('body').on('click', '.uploader-hold-icon', function(){
		$(this).next('.uploaderNovo').find('.ajax-file-upload form input[type="file"]').click();
	});
	
	$('body .listaMural').on('click', '.aba-idioma-post-mural', function(){
		var postId = $(this).data('id-post'),
		idiomaId = $(this).data('id-idioma');
		var p = $('.listaMural li#postId-' + postId + ' .post-content .nomeGrupo .chapeuPost');
		var filhos = $(p).find('a.uppercase');
		for(var i = 0; i < $(filhos).length; i++){
			if($(filhos[i]).data('idioma-' + idiomaId)){
				$(filhos[i]).text($(filhos[i]).data('idioma-' + idiomaId))
			}
		}
	});
	
	loadAllIdiomas();
});

var userAdmin = "";

function loadAllIdiomas(){
	if(localStorage.getItem('idiomas') == null){
		ajaxGet(urlContext + "/rest/idioma", null, null, success);
		function success(idiomas) {
			localStorage.setItem('idiomas', JSON.stringify(idiomas));
		}
	}
}
function localizaGrupo(idGrupoURL){
	jQuery.ajax({
		url: urlContext + '/rest/grupo/mural/header/'+idGrupoURL,
		type: 'GET',
		beforeSend: function(){
			$('.formPost').hide();
		},
		success: function(resp){
			if(resp.status == 'OK'){
				var item = resp.entity;
				var gu = item.virtual ? item.countSubgrupos : item.countUsers;
				
				$(".info-name a").html(item.nome);
				$(".info-name a").attr("href", urlContext+"/grupo.htm?id="+item.id);
				
				if(item.postar || (SESSION.PERMISSOES.ALL_GRUPOS != '' && SESSION.PERMISSOES.ALL_GRUPOS == 'true')){
					$('.formPost').fadeIn();
				}
				
				if(PARAMS.TIPO_GRUPO_ON == '1'){
					if(userId == item.userAdminId){
						$(".group-convidar").show();
					}else{
						$(".group-convidar").hide();
					}
					
					if(item.tipo){
						var tipoGrupo = "";
						switch(item.tipo){
							case 'ABERTO':
								tipoGrupo = 'Aberto';
								break;
							case 'ABERTO_FECHADO':
								tipoGrupo = 'Aberto com Aprovação';
								break;
							case 'PRIVADO_OPCIONAL':
								tipoGrupo = 'Privado Opcional';
								break;
							case 'PRIVADO':
								tipoGrupo = 'Privado';
								break;
							default:
								tipoGrupo = item.tipo;
						}
						
						$(".second-info").html('Tipo: '+tipoGrupo);
						
						if(gu){
							$(".third-info").html('Integrantes: '+gu);
						}
					}
				}else{
					if(gu){
						$(".second-info").html('Integrantes: '+gu);
					}
				}
				
				if(item.virtual){
					$('.btn-group-convidar, .btn-group-participar, .btn-group-sair, .group-users, .group-chat').hide();
					$('.group-subgrupos').removeClass('hidden');
					$('.second-info').html('<i class="fa fa-desktop"></i>&nbsp;Grupo virtual');
					$('.third-info').html('Integrantes: '+gu);
				}
				
				if(item.urlFoto){
					var urlFoto = item.urlFoto;
					if(urlFoto.substr(0,4).indexOf('img/') > -1) {
						urlFoto = urlContext + '/' + urlFoto;
					}
					$(".mural-header-photo").attr("src", urlFoto);
					$(".opcoes-grupo .see-group-photo").attr("href", urlFoto);
				}else{
					$(".mural-header-photo").attr("src", urlContext=+"/imagens/bg_grupo.jpg");
					$(".opcoes-grupo .see-group-photo").attr("href", urlContext+"/imagens/bg_grupo.jpg");
				}
				
				
				if(item.tipo == 'PRIVADO' || userId == item.userAdminId) {
					$(".group-participar, .group-sair").hide();
				} else {
					if(!item.virtual){
						if(item.participa){
							$(".group-sair").show();
							$(".group-participar").hide();
						}else{
							$(".group-sair").hide();
							if(item.tipo == 'ABERTO_FECHADO') {
								var solicitar = '<i class="fa fa-check" aria-hidden="true"></i>&nbsp;Solicitar Participação';
								$(".group-participar a").empty().append(solicitar).show();
							} else {
								var participar = '<i class="fa fa-check" aria-hidden="true"></i>&nbsp;Entrar';
								$(".group-participar a").empty().append(participar).show();
							}
						}
					}
				}
				
				if(item.participa && !item.virtual){
					if(item.statusSolicitacao == 'APROVADA'){
						$('li.group-chat a').attr('href', urlContext+'/pages/chat.htm?grupo='+item.id);
					}else{
						$("li.group-chat").hide();
					}
					if(item.favorito){
						$(".group-favoritar a.css-favorito").show();
						$(".group-favoritar a.css-favorito-not").hide();
					}else{
						$(".group-favoritar a.css-favorito").hide();
						$(".group-favoritar a.css-favorito-not").show();
					}
					
					if(item.statusSolicitacao != "APROVADA"){
						$(".formPost.postar").hide();
						$(".muralVazio").html("Aguardando aprovação").show();
					}
				}else{
					$("li.group-chat, .group-favoritar a.css-favorito, .group-favoritar a.css-favorito-not").hide();
				}

				$(".mural-header-photo").attr("alt", "Foto do grupo "+item.nome);
				$("#modalGrupoUsuarios .modal-title, #modalGrupoSubgrupos .modal-title").empty().append('<span class="glyphicon glyphicon-list-alt"></span>&nbsp; Grupo: '+item.nome);
				$("#modalGrupoUsuarios a.link-perfil-grupo, #modalGrupoSubgrupos a.link-perfil-grupo").attr("href", urlContext+"/grupo.htm?id="+item.id+"&tab=users");
				
				userAdmin = item.userAdminId;
				$(".mural-header .mt-info.opcoes-user, .user-photo-container").hide();
				$(".mural-header .mt-info.opcoes-grupo").show();
				$(".mural-header").addClass("grupo").attr("data-grupoid", item.id).fadeIn();
				
				var pagebar = $('.page-bar.mural');
				$(pagebar).find('.page-bar-second').text('Grupo');
				$(pagebar).find('.page-bar-third').text(item.nome);
				$(pagebar).slideDown();
				
				
				$('.interacoesMural #grupos').tokenInput('clear');
				$('.interacoesMural #grupos').tokenInput("add", item);
				$('.post-groups').find('.mascaraLabel').hide();
				
				if(!item.participa && (SESSION.PERMISSOES.ALL_GRUPOS != '' && SESSION.PERMISSOES.ALL_GRUPOS == 'false')) {
					var string = 'Você ainda não participa deste grupo.';
					if(PARAMS.TIPO_GRUPO_ON == '1' && !item.virtual){
						string += '<br/>Caso tenha interesse, clique <a class="btn-group-participar">aqui</a> para participar.';
					}
					$(".muralVazio").html(string).show();
					if(!item.virtual){
						$('.formPost').hide();
					}
				}
			}else{
				toastr.error(resp.message);
			}
		},
	});
}

function loadGroupSubgrupos(idGrupoURL){
	jQuery.ajax({
		type: 'GET',
		url: urlContext+'/rest/grupo/subgrupos/'+idGrupoURL,
		success: function(resp){
			var conteudo = '';
			
			if(resp.entity && resp.entity.length > 0){
				conteudo += '<ul class="listaHorizontal noPaddingTop clearfix listaUsuarios">';
				
				for (var i = 0; i < resp.entity.length; i++){
					var campo = resp.entity[i];
					var gu = campo.countUsers ;
					
			 		conteudo += '<li data-id="'+ campo.id +'">';
				 		conteudo += '<div class="clearfix">';
					 		
				 			var grupoImg = campo.urlFotoThumb;
							if(grupoImg){
								if(grupoImg.substr(0,4).indexOf('img/') > -1) {
									grupoImg = urlContext + '/' + grupoImg;
								}
							}else{
								grupoImg = urlContext + "/imagens/bg_grupo_thumb.jpg";
							}
							
			 				conteudo +=	'<div class="fotoUsuario media">';
								conteudo += '<a class="centraliza" data-nome="'+campo.nome+'" href="'+urlContext+'/usuario.htm?id='+campo.id+'" target="_blank">';
									conteudo += '<img src="'+grupoImg+'" alt="Foto do grupo '+campo.nome+'" />';
								conteudo += '</a>';
							conteudo += '</div>';
							
							conteudo += '<div class="infoUsuarios">';
								conteudo += '<a href="'+urlContext+'/grupo.htm?id='+campo.id+'" class="nomeUsuarioLista">'+campo.nome+'</a>';
								conteudo += '<p class="cargoUsuarioLista">Admin: '+campo.userAdmin+'</p>';
		 						
		 						if(PARAMS.TIPO_GRUPO_ON == '1'){
		 							var gt = "";
		 							switch(campo.tipo){
										case 'ABERTO':
											gt += 'Aberto';
											break;
										case 'ABERTO_FECHADO':
											gt += 'Aberto com Aprovação';
											break;
										case 'PRIVADO_OPCIONAL':
											gt += 'Privado Opcional';
											break;
										case 'PRIVADO':
											gt += 'Privado';
											break;
										default:
											if(g.virtual){
												gt = 'Virtual';
											}else{
												gt += campo.tipo ? campo.tipo : '--';
											}
									}
		 							
		 							conteudo += '<p class="cargoUsuarioLista pull-left">Tipo: '+gt+'</p>';
	 							}else{
	 								conteudo += '<p class="cargoUsuarioLista pull-left">Integrantes: '+gu+'</p>';
		 						}
		 						conteudo += '</div>'; 
	 						conteudo += '</div>';
						conteudo += '</div>';
					conteudo +=  '</li>';
				}
				
				conteudo += '</ul>';
				
				if(resp.mensagem){
					$(".paginaAtual").hide();
					jAlert(resp.mensagem.mensagem, resp.mensagem.status, null);
				}
			}else{
		 		conteudo += '<h4 class="sbold text-center">Este grupo não possui nenhum subgrupo.</h4>';
	 		}
			
			$("#modalGrupoSubgrupos .modal-body .modal-group-content").empty().append(conteudo);
	 		$("#modalGrupoSubgrupos .modal-body .spinner").hide();
		}
	});
}

function loadGroupUsers(idGrupoURL){
	var dataUsuariosAutoComplete = {
 		user: SESSION.USER_INFO.USER_LOGIN,
		grupoId: idGrupoURL,
		maxRows: 0,
 		page: 0,
 		wsVersion: wsVersion,
 		wstoken: SESSION.USER_INFO.USER_WSTOKEN
	}
	
	AjaxServices.usuariosAutoComplete(dataUsuariosAutoComplete).done(function(data){
		var conteudo = "";
 		if(data.list && data.list.length > 0){
	 		conteudo += '<ul class="listaHorizontal noPaddingTop clearfix listaUsuarios">';
		 		for (var i=0;i<data.list.length;i++){
		 			var user = data.list[i];
		 			_usuariosIdsInGrupo += user.id + ",";
		 			
		 			conteudo += '<li>';
			 			conteudo += '<div class="clearfix">';
					 		if(userAdmin == user.id) {
								conteudo += '<i class="icones crownAdmin bs-tooltip" title="Admin do grupo"></i>';
							}
						conteudo +=	'<div class="fotoUsuario media">';
							conteudo += '<a class="centraliza" href="'+urlContext+'/usuario.htm?id='+user.id+'" target="_blank">';
								conteudo += '<img src="'+user.urlFotoUsuario+'" alt="Foto do '+user.nome+'" />';
							conteudo += '</a>';
						conteudo += '</div>';
						conteudo += '<div class="infoUsuarios">';
							conteudo += '<a href="'+urlContext+'/usuario.htm?id='+user.id+'" target="_blank" class="nomeUsuarioLista">'+user.nome+'</a>';
								if(data.list[i].desc1){
									conteudo +=	'<p class="cargoUsuarioLista">'+user.desc1+'</p>';
								}
								if(data.list[i].desc2){
	    							conteudo += '<p class="cargoUsuarioLista">'+user.desc2+'</p>';				
								}
							conteudo +=	'</div>';
						conteudo += '</div>';
					conteudo += '</li>';
		 		}
		 	conteudo += '</ul>';
		 	
	 	}else{
	 		conteudo += '<h4 class="sbold text-center">Este grupo não possui nenhum usuário.</h4>';
 		}
 		
 		$("#modalGrupoUsuarios .modal-body .modal-group-content").empty().append(conteudo);
 		$("#modalGrupoUsuarios .modal-body .spinner").hide();
 		
 		if(data.mensagem){
 			$(".paginaAtual").hide();
 			jAlert(data.mensagem.mensagem, data.mensagem.status, null);
 		}
	});
}

function participarGrupo(grupoId){
	jQuery.ajax({
		type: "PUT",
		url: urlContext+"/rest/grupo/participar",
	 	contentType: jsHttp.CONTENT_TYPE_FORM,		 
	 	data: {
	 		usuarioId: userId,
	 		grupoId: grupoId,
	 		participar: true,
		},
	 	success: function(data){
	 		window.location = urlContext+"/pages/mural.htm?grupo="+grupoId;
	 	},
	 	error: function(data){
	 		buscaGrupos();
	 	}
	 });
}

function sairGrupo(grupoId){
	jQuery.ajax({
		type: "PUT",
		url: urlContext+"/rest/grupo/participar",
		contentType: jsHttp.CONTENT_TYPE_FORM,		 
		data: {
			usuarioId: userId,
			grupoId: grupoId,
			participar: false,
		},
		success: function(data){
			window.location = urlContext+"/pages/mural.htm";
		},
		error: function(data){
			buscaGrupos();
		}
	});
}

function localizaUser(idUserURL, tipo){
	var dataUsuariosAutoComplete = {
		usuarios: idUserURL,
		user_id: userId
	};
	
	AjaxServices.usuariosAutoComplete(dataUsuariosAutoComplete).done(function(data){
		if(data && data.list) {
			for(var i in data.list) {
				var item = data.list[i];
				if(item.urlFotoCapa){
					var urlFoto = item.urlFotoCapa;
					if(urlFoto.substr(0,4).indexOf('img/') > -1) {
						urlFoto = urlContext + '/' + urlFoto;
					}
					$(".mural-header-photo").attr("src", urlFoto);
				}else{
					$(".mural-header-photo").attr("src", urlContext + "/img/capa.png");
				}
				
				if(PARAMS.MURAL_AMIZADE_ON && PARAMS.MURAL_AMIZADE_ON == 1){
					if(item.id != userId){
						$('li.user-friendship').attr('data-user', item.id);
						
						if(item.amigo == ""){
							$('li.user-friendship > a.desfazer-amizade, li.user-friendship > a.cancela-amizade').hide();
							$('li.user-friendship > a.solicita-amizade').show();
						}
						
						if(item.amigo == "SOLICITADO"){
							$('li.user-friendship').hide();
							$('li.aguardando').removeClass('hidden');
							$('li.aguardando > a').attr('href', urlContext+'/usuario.htm?id='+item.id);
						}
						
						if(item.amigo == "AGUARDANDO_APROVACAO"){
							$('li.user-friendship > a.desfazer-amizade, li.user-friendship > a.solicita-amizade').hide();
							$('li.user-friendship > a.cancela-amizade').show();
						}
						
						if(item.amigo == "AMIGOS"){
							$('li.user-friendship > a.solicita-amizade, li.user-friendship > a.cancela-amizade').hide();
							$('li.user-friendship > a.desfazer-amizade').show();
						}
						$('li.somente-eu').hide();
					}else{
						$('li.user-friendship').hide();
						$('li.somente-eu').show();
					}
				}else{
					if(item.id != userId){
						$('li.somente-eu').hide();
					}else{
						$('li.somente-eu').show();
					}
				}
				
				var pagebar = $('.page-bar.mural');
				
				if(tipo == 'meusAmigos'){
					$(".info-name a").html("Minhas conexões");
					$(pagebar).find('.page-bar-second').text('Posts');
					$(pagebar).find('.page-bar-third').text('Minhas Conexões');
					$("li.see-user-profile, li.somente-eu").hide();
				}else{
					$(".info-name a").html(item.nome);
					$(".second-info").html(item.email).fadeIn();
					$(".third-info").html(item.funcao).fadeIn();
					$(pagebar).find('.page-bar-second').text('Usuário');
					$(pagebar).find('.page-bar-third').text(item.nome);
					$(".btn-see-user-profile, .info-name a").attr("href", urlContext+"/usuario.htm?id="+item.id);
					if(tipo == 'somenteEu'){
						$('li.somente-eu > a').addClass('active');
					}
				}
				
				$("li.solicitar-varias-amizades").hide();

				$(".user-photo-container .user-photo-img").attr("src", item.urlFotoUsuarioThumb);
				$(".user-photo-container a.centraliza.fancybox").attr("href", item.urlFotoUsuarioThumb);
				
				if(PARAMS.MURAL_HEADER_FOTO_ON && PARAMS.MURAL_HEADER_FOTO_ON == 1){
					$(".user-photo-container").fadeIn();
				}
				
				$(".mural-header-photo").attr("alt", "Foto de capa do "+item.nome);
				$(".mural-header .mt-info.opcoes-grupo").hide();
				$(".mural-header .mt-info.opcoes-user").show();
				$(".mural-header").addClass("user").attr("data-userid", item.id).fadeIn();
				$(pagebar).slideDown();
			}
		}
	});
}

function amizade(obj, callback){
	jQuery.ajax({
		type: "PUT",
		url: urlContext+"/rest/amizade/solicitacao",
		contentType: jsHttp.CONTENT_TYPE_FORM,		 
		data: {
			usuarioId: userId,
			amigoId: obj.amigoId,
			solicitacao: obj.solicitacao,
		},
		success: function(data){
			if(callback){
				callback(data);
			}
			location.reload();
		},
		error: function(data){
			if(callback){
				callback();
			}
			location.reload();
		}
	});
}

function responderAmizade(obj, success){
	jQuery.ajax({
		type: "PUT",
		url: urlContext+"/rest/amizade/solicitacao/aprovar",
		contentType: jsHttp.CONTENT_TYPE_FORM,		 
		data: {
			usuarioId: userId,
			amigoId: obj.amigoId,
			aprovar: obj.aprovar,
		},
		success: success,
		error: function(data){
		}
	});
}

function shrinkURL(longURL){
	return jQuery.ajax({
		type: 'POST',
		url: 'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyA0c-QMyqWGySMOS01Y54DRtdBKBpcY_hk',
		contentType: 'application/json',
		dataType: 'json',
		data: JSON.stringify({
			longUrl: jQuery.trim(longURL)
		})
	});
}

function expandURL(shortURL){
	return jQuery.ajax({
		type: 'GET',
		url: 'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyA0c-QMyqWGySMOS01Y54DRtdBKBpcY_hk&shortUrl='+shortURL+'&projection=FULL',
		contentType: 'application/json'
	});
}

function createTabsIdiomas(idiomas, id, isMural){
	var conteudo = '';
	if(isMural){
		var idPost = id;

		conteudo += '<div class="clearfix">';
			conteudo += '<ul class="nav nav-tabs pull-right post-idioma-tabs">';
				for(var i in idiomas) {
					var item = idiomas[i];

					conteudo += '<li class="mural-idiomas" data-id-idioma="'+item.idioma.id+'">';
						conteudo += '<a class="aba-idioma-post-mural" href="#tab_post'+idPost+'_idioma'+item.idioma.id+'" data-id-post=' + idPost + ' data-id-idioma=' + item.idioma.id + ' data-cod="'+item.idioma.codigo+'" data-toggle="tab">';
							conteudo += '<div class="idiomas-disponiveis">';
								conteudo += '<div class="idioma '+item.idioma.icone+' bs-tooltip" data-code="'+item.idioma.codigo+'" title="'+item.idioma.nome+'">';
									conteudo += '<i class="md '+item.idioma.icone+'"></i>';
								conteudo += '</div>'
							conteudo += '</div>';
						conteudo += '</a>';
					conteudo += '</li>';
				}
			conteudo += '</ul>';
		conteudo += '</div>';
	}else{ //isPostar
		var idIdioma = id,
			hidden = $('.abas-post-idiomas .post-idioma-tabs li.li-link').length < 2 ? 'hidden' : '';

		conteudo += '<li class="aba-post-idiomas active li-link" data-id-idioma="'+idIdioma+'">';
			conteudo += '<i class="fa fa-close remover-post-idioma '+hidden+'" data-toggle="tooltip" data-idioma="'+idIdioma+'" title="Remover idioma"></i>';
			conteudo += '<a href="#" id="tab-postar-idioma" data-id-idioma="'+idIdioma+'" data-toggle="tab">';
				conteudo += '<div class="idiomas-disponiveis">';
					conteudo += '<div class="idioma '+idiomas.icone+' bs-tooltip" data-code="'+idiomas.codigo+'" title="'+idiomas.nome+'">';
						conteudo += '<i class="md '+idiomas.icone+'"></i>';
					conteudo += '</div>'
				conteudo += '</div>';
			conteudo += '</a>';
		conteudo += '</li>';
	}

	return conteudo;
}

function createMensagemIdioma(postIdioma, post, tema, mensagemConteudo){
	var conteudo = '',
		active = postIdioma == post.idiomas[0] ? 'in active' : '',
		postIdiomaId = postIdioma.idioma && postIdioma.idioma.id ? postIdioma.idioma.id : postIdioma.id;

	conteudo +=	'<div class="tab-pane fade '+ active +' tab-post-idioma" id="tab_post'+ post.id +'_idioma'+ postIdiomaId +'">';
		conteudo +=	'<div class="alturaTitulo"></div>';
		conteudo += '<h3 class="tituloPost"><a href="'+urlContext+'/pages/post.htm?id='+post.id+'">'+postIdioma.titulo+'</a></h3>';

		if(postIdioma.resumo){
			var style = post.statusPost ? 'style= "color: ' + post.statusPost.cor + '"' : "";
			conteudo += '<p class="resumoPost" '+ style +'>'+postIdioma.resumo+'</p>';
		}

		if(SESSION.PERMISSOES.VISUALIZAR_CONTEUDO == 'true'){
			if(postIdioma.mensagem && !postIdioma.resumo){
	            conteudo +=	'<div class="alturaMensagem">';
	                conteudo +=	'<div class="degrade"></div>';
	                conteudo +=	'<div class="mensagemPost '+tema+'">';
	                    if(post.html && postIdioma.mensagem.lastIndexOf('<meta') > -1){
	                        conteudo += '<iframe class="webview" id="webview-post-'+post.id+'" data-post-id="'+post.id+'"></iframe>';
	                    }else{
	                        var body = '';

	                        if(!post.html){
	                            body = XBBCODE.process({ text: postIdioma.mensagem, removeMisalignedTags: false });
	                            body = body.html
	                        }else{
	                            var parser = new DOMParser(),
	                                parsed = parser.parseFromString(postIdioma.mensagem, 'text/html'),
	                                head = parsed.head.innerHTML;
	                            $(parsed.body).find('img').each(function(){
	                            	var img = $(this);
	                            	setTimeout(function(){
	                            		verMaisPost($('ul.listaMural li[data-id="'+post.id+'"]'));
	                            	}, 1000)
	                            })
	                            body = parsed.body.innerHTML;
	                        }

	                        conteudo += '<article class="entry-content" data-post-id="'+post.id+'">'+body+'</article>';
	                    }
	                conteudo +=	'</div>';
	            conteudo +=	'</div>';
	            conteudo +=	'<a class="verMaisPost" href="#"><i class="icones ico-mais"></i>ver mais</a>';
	        }else if(postIdioma.mensagem && postIdioma.resumo){
	            conteudo += '<div class="mensagemPost">';
	                conteudo += '<div class="descricaoPost semResumo">';
	                if(post.html && postIdioma.mensagem.lastIndexOf('<meta') > -1){
	                        conteudo += '<iframe class="webview" id="webview-post-'+post.id+'" data-post-id="'+post.id+'"></iframe>';
	                    }else{
	                        var body = '';

	                        if(!post.html){
	                        	body = XBBCODE.process({ text: postIdioma.mensagem, removeMisalignedTags: false });
	                            body = body.html
	                        }else{
	                            var parser = new DOMParser(),
	                            	parsed = parser.parseFromString(postIdioma.mensagem, 'text/html'),
	                                head = parsed.head.innerHTML;

	                            body = parsed.body.innerHTML;
	                        }

	                        conteudo += '<article class="entry-content" data-post-id="'+post.id+'">'+body+'</article>';
	                    }
	                conteudo += '</div>';
	            conteudo += '</div>';
	            conteudo +=	'<a class="verMaisPost abreDescricao" href="#"><i class="icones ico-mais"></i>ver mais</a>';
	        }
		}
	conteudo += '</div>';

	return conteudo;
}

function formatCategoria(categoria){
	var conteudo = '';
	conteudo += '<p class="chapeuPost" style="color:#'+categoria.cor+'">';
	var catIdiomas = "";
		if(categoria.categoriaPai){
			if(categoria.categoriaPai.categoriaPai){
				if(categoria.categoriaPai.categoriaPai.idiomas){
					var idiomas = categoria.categoriaPai.categoriaPai.idiomas;
					for(var idioma in idiomas){
						catIdiomas += ' data-idioma-' + idiomas[idioma].idioma.id + '="' + idiomas[idioma].nome+'"';
					}
				}
				var cppCor = categoria.categoriaPai.categoriaPai.cor.lastIndexOf('#') > -1 ? categoria.categoriaPai.categoriaPai.cor : '#'+categoria.categoriaPai.categoriaPai.cor,
					cppNome = categoria.categoriaPai.categoriaPai.nome ? categoria.categoriaPai.categoriaPai.nome : categoria.categoriaPai.categoriaPai.idiomas[0].nome;
				conteudo += '<a class="uppercase"' + catIdiomas + ' style="color:'+cppCor+'" href="'+urlContext+'/pages/mural.htm?categoria='+categoria.categoriaPai.categoriaPai.id+'">'+cppNome+'</a> <i class="fa fa-angle-right font-grey-salsa"></i>'; 
			}
			if(categoria.categoriaPai.idiomas){
				var idiomas = categoria.categoriaPai.idiomas;
				catIdiomas = "";
				for(var idioma in idiomas){
					catIdiomas += ' data-idioma-' + idiomas[idioma].idioma.id + '="' + idiomas[idioma].nome+'"';
				}
			}
			var cpCor = categoria.categoriaPai.cor.lastIndexOf('#') > -1 ? categoria.categoriaPai.cor : '#'+categoria.categoriaPai.cor,
				cpNome = categoria.categoriaPai.nome ? categoria.categoriaPai.nome : categoria.categoriaPai.idiomas[0].nome;
			conteudo += '<a class="uppercase"' + catIdiomas + ' style="color:'+cpCor+'" href="'+urlContext+'/pages/mural.htm?categoria='+categoria.categoriaPai.id+'">'+cpNome+'</a> <i class="fa fa-angle-right font-grey-salsa"></i>';
		}
		if(categoria.idiomas){
			var idiomas = categoria.idiomas;
			catIdiomas = "";
			for(var idioma in idiomas){
				catIdiomas += ' data-idioma-' + idiomas[idioma].idioma.id + '="' + idiomas[idioma].nome+'"';
			}
		}
		var cor = categoria.cor.lastIndexOf('#') > -1 ? categoria.cor : '#'+categoria.cor;
		conteudo += '<a class="uppercase"' + catIdiomas + ' style="color:'+cor+'" href="'+urlContext+'/pages/mural.htm?categoria='+categoria.id+'">';
			if(categoria && categoria.nome){
				conteudo += categoria.nome;
			}else{
				if(categoria && categoria.idiomas && categoria.idiomas[0] && categoria.idiomas[0].nome){
					conteudo +=	categoria.idiomas[0].nome;
				}
			}
//			if(post.chapeu && post.chapeu.nome){
//				conteudo +=	" / "+post.chapeu.nome;
//			}
		conteudo +=	'</a>';
	conteudo +=	'</p>';
	
	return conteudo;
}

function welcomeFeed(){
	var paramURL = "";
	if(urlAtual.lastIndexOf('?') > -1){
		paramURL = urlAtual.substr(urlAtual.lastIndexOf('?') + 1);
	}
	
	var x = Math.floor(Math.random() * 5) + 1;
	var y = x / 2;
	if(y > 2){
		var date = new Date();
		var st = String(date);
		st = st.split(" ");
		hr = st[4]
		h = hr.split(":");
		if(h[0] > 4 && h[0] < 12){
			var ct = "Bom dia, "+SESSION.USER_INFO.USER_NOME;
			var icw = '<i class="wf-icon morning"></i>';
		}else if(h[0] >= 12 && h[0] < 18){
			var ct = "Boa tarde, "+SESSION.USER_INFO.USER_NOME;
			var icw = '<i class="wf-icon afternoon"></i>';
		}else{
			var ct = "Boa noite, "+SESSION.USER_INFO.USER_NOME;
			var icw = '<i class="wf-icon night"></i>';
		}
		
		var icc = '<i class="fa fa-close mural-close-icon"></i>';
		
		var wf = "";
		wf += '<li class="wf" style="display: none;">';
			wf += '<div class="alturaMensagem"><i class="livecom-icon"></i><span class="sbold">'+ct+'</span>'+icw+icc+'</div>';
		wf += '</li>';
		
		if($('ul.listaMural li.infoFilter').length == 0 && paramURL == ""){
			var timer = null;
			if(timer != null){
	            clearInterval(timer);
	        }
			
			$('ul.listaMural').prepend(wf);
			
			timer = setInterval(function(){
				$('ul.listaMural > li.wf').slideDown(500);
	        }, 300);
			
		}
		
		$('.listaMural>li.wf .mural-close-icon').tooltip({ html: true, title: 'Remover' });
	}
}


var isImage = {'png': true, 'jpg': true, 'gif': true, 'jpeg': true, 'bmp': true, 'ico': true, 'webp': true, 'svg': true};
var isPlayer = {'mp3': 'fancyJwplayer', 'mp4': 'fancyJwplayer', 'mov': 'fancyJwplayer'}
var LiveMural = (function() {
	var totalPost = 0;
	/**
	 * Aceita todos os argumentos de uma requisição ajax
	 * ex: data, success, beforeSend, em formato json
	 * 
	 */
	var ajaxBuscaPosts = function(obj) {
		obj.url = obj.url ? obj.url : urlContext + "/ws/mural.htm";
		obj.type = "POST";
		obj.dataType = 'json';
		obj.data.mode = 'json';
		obj.data.form_name = 'form';
		jQuery.ajax(obj);
	}
	
	var ajaxBuscaPost = function(obj) {
		obj.url = obj.url ? obj.url : urlContext + "/ws/post.htm";
		obj.type = "POST";
		obj.dataType = 'json';
		obj.data.mode = 'json';
		obj.data.form_name = 'form';
		jQuery.ajax(obj);
	}
	
	var buscaPost = function(postId, openComentario, modeMural) {
		ajaxBuscaPost({
			data: {
				user_id: userId,
				audiencia: 1,
				id: postId,
				wsVersion: wsVersion,
			},
			success: function(data) {
				$(".muralVazio").html("").hide();
				$(".listaMural").empty();
				$(".stream-mural").slideUp();
				$(".paginaAtual").remove();
				if(data.post) {
					var pagebar = $('.page-bar.mural');
					$(pagebar).find('.page-bar-second').text('Post');
					$(pagebar).find('.page-bar-third').text(data.post.titulo);
					$(pagebar).slideDown();
					postLI(data.post, openComentario, modeMural);
		 		} else if(data.mensagem){
		 			var mensagem = data.mensagem;
		 			if(mensagem.mensagem == 'Usuário sem permissão para visualizar o post') {
		 				$(".muralVazio").html("Você não possui permissão para visualizar este post").show();
		 			} else {
		 				$(".muralVazio").html(mensagem.mensagem).show();
		 			}
		 		} else {
		 			$(".muralVazio").html("Post não encontrado").show();
		 		}
			}
		})
	}
	
	var buscaPosts = function(filtro, clear, modeMural) {
		if($(".paginaAtual").length == 0) {
			$(".listaMural").after('<div class="paginaAtual" data-page="-1"></div>');
		}
		if(clear) {
			$(".muralVazio").html("").hide();
			$(".listaMural > li:not(.filtro-incidente)").remove();
			$(".stream-mural").slideUp();
			$(".paginaAtual").attr("data-page", -1);
		}
		
		var formFiltro = $(".filtroPosts");
		
		if(urlAtual.indexOf("?") > -1){
			urlAtual = urlAtual.substr(0, urlAtual.lastIndexOf('?'));
		}
		
		if(!filtro) {
			filtro = {};
		}
		
		if(!filtro.tags) {
			var filtroTagUrl = getURLParameter("tag");
			var valorTag = "";
			if(filtroTagUrl != ""){
				valorTag = filtroTagUrl; 
			}else if(formFiltro.find("#idsFakeTagsFiltro").val() != ""){
				valorTag = formFiltro.find("#idsFakeTagsFiltro").val();
			}
			filtro.tags = valorTag;
		}
		
		if(!filtro.categoria_id) {
			var filtroCategoriaUrl = getURLParameter("categoria");
			var valorCategoria = "";
			if(filtroCategoriaUrl != null && filtroCategoriaUrl != ""){
				valorCategoria = filtroCategoriaUrl; 
			}else {
				var catSideBoxActive = $('.list-group-categorias li.ativo');
				valorCategoria = $(catSideBoxActive).attr('data-id');
				if(valorCategoria) {
					if(formFiltro.find("#filtroCategoria_id").val() != ""){
						valorCategoria = formFiltro.find("#filtroCategoria_id").val();
					}
				}
			}
			filtro.categoria_id = valorCategoria;
		}
		
		if(!filtro.grupo_ids) {
			var filtroGrupoUrl = getURLParameter("grupo");
			var valorGrupo = "";
			if(filtroGrupoUrl != ""){
				valorGrupo = filtroGrupoUrl; 
			}else if(jQuery.trim($("#filtroGrupoIds").val()) != ""){
				valorGrupo = jQuery.trim($("#filtroGrupoIds").val());
			}
			filtro.grupo_ids = valorGrupo;
		}
		
		if(!filtro.arquivo_id) {
			var filtroArquivo = getURLParameter("arquivo");
			if(filtroArquivo != ""){
				filtro.arquivo_id = filtroArquivo; 
			}
		}
		
		if(!filtro.chapeu_id) {
			var filtroChapeuUrl = getURLParameter("chapeu");
			var valorChapeu = "";
			if(filtroChapeuUrl != ""){
				valorChapeu = filtroChapeuUrl; 
			}else if(formFiltro.find("#filtroChapeu").val() != ""){
				valorChapeu = formFiltro.find("#filtroChapeu").val();
			}
			filtro.chapeu_id = valorChapeu;
		}
		
		if(!filtro.texto) {
			filtro.texto = formFiltro.find("#filtroTexto").val();
		}
		
		
		if(!filtro.meusFavoritos) {
			if(jQuery.trim(urlAtual) == "favoritos.htm"){
				filtro.meusFavoritos = "1";
			}
		}
		if(!filtro.user_post_id) {
			if(jQuery.trim(urlAtual) == "meusPosts.htm"){
				filtro.user_post_id = userId;
			} else if(jQuery.trim(urlAtual) == "meusRascunhos.htm"){
				filtro.user_post_id = userId;
				filtro.meusRascunhos = 1;
				filtro.meusFavoritos = 0;
			}else if(jQuery.trim(urlAtual) == "usuario.htm"){
				filtro.user_post_id = getLinkParameter("id",window.location.href);
				filtro.meusFavoritos = 0;
			} else {
				var parametroBuscaPostsUsuario = getURLParameter("user_post_id");
				if(parametroBuscaPostsUsuario != "" && urlAtual == "mural.htm"){
					filtro.user_post_id = parametroBuscaPostsUsuario;
				}
			}
		}
		
		if(!filtro.comAnexo) {
			if(formFiltro.find("#filtroAnexo").is(":checked")){
				filtro.comAnexo = 1;
			}
		}	
		
		if(!filtro.dataInicial) {
			filtro.dataInicial = formFiltro.find("#filtroDataInicial").val();
		}
		if(!filtro.dataFinal) {
			filtro.dataFinal = formFiltro.find("#filtroDataFinal").val();
		}
		
		if(filtro.grupo_ids && !filtro.user_post_id) {
			var grupos = filtro.grupo_ids.split(',');
			if(grupos.length == 1 ) {
				localizaGrupo(filtro.grupo_ids);
			}
		} else if(filtro.user_post_id) {
			localizaUser(filtro.user_post_id);
		}
		
		filtro.user_id = userId;
		filtro.user = SESSION.USER_INFO.USER_LOGIN;
		filtro.buscaUsuarios = 0;
		filtro.buscaPosts = 1;
		filtro.maxRows = 10;
		filtro.buscaResumida = 0;
		filtro.page = parseInt($(".paginaAtual").attr("data-page"))+1;
		filtro.wsVersion = wsVersion;
		
		if(filtro && filtro.meusAmigos){
			var mf = "";
			mf += '<li class="infoFilter meus-amigos" style="display: none;">';
				mf += '<div class="alturaMensagem">';
					mf += '<div class="mensagemPost">';
						mf += '<i class="fa fa-info-circle font-blue" aria-hidden="true"></i>&nbsp;';
						mf += '<span class="descricaoPost">Você está vendo os posts das suas conexões</span>';
						mf += '<i class="fa fa-close mural-close-icon reset-filter-mural" aria-hidden="true"></i>';
					mf += '</div>';
				mf += '</div>';
			mf += '</li>';
			
			if(filtro.page == 0){
				var timer = null;
				if(timer != null){
		            clearInterval(timer);
		        }
				$('ul.listaMural').prepend(mf);
				
				timer = setInterval(function(){
					$('ul.listaMural > li.meus-amigos').slideDown(500);
				}, 300);
				
			}
		}
		
		if(filtro && filtro.texto != ""){
			var infoFilter = "";
			infoFilter += '<li class="infoFilter custom-search" data-text="'+filtro.texto+'" style="display: none;">';
				infoFilter += '<div class="alturaMensagem">';
					infoFilter += '<div class="mensagemPost">';
						infoFilter += '<i class="fa fa-info-circle font-blue" aria-hidden="true"></i>&nbsp;';
						infoFilter += '<span class="descricaoPost">Você está vendo os posts para a busca: "'+filtro.texto+'"</span>';
						infoFilter += '<i class="fa fa-close mural-close-icon reset-filter-mural" aria-hidden="true"></i>';
					infoFilter += '</div>';
				infoFilter += '</div>';
			infoFilter += '</li>';
			
			if(filtro.page == 0){
				var timer = null;
				if(timer != null){
		            clearInterval(timer);
		        }
				
				$('ul.listaMural').prepend(infoFilter);
				
				timer = setInterval(function(){
					$('ul.listaMural > li.custom-search').slideDown(500);
				}, 300);
			}
		}
		
		$(".paginaAtual").addClass("loading").show();
		
		ajaxBuscaPosts({
			data: filtro,
			beforeSend: function(data){
				if(filtro.meusAmigos || !filtro.meusAmigos){
					$('.sidebox-nav-tabs li.mf, .sidebox-nav-tabs li a.mf').addClass('disabled');
				}
			},
			success: function(data) {
				$('.sidebox-nav-tabs li.mf, .sidebox-nav-tabs li a.mf').removeClass('disabled');
				if(clear) {
					var body = $("html, body");
					body.stop().animate({scrollTop:0}, '500', 'swing');
				}
				
				if(data && data.busca && data.busca.posts) {
					showPosts(data.busca.posts, clear, null, modeMural);
				} else {
					if($(".listaMural li").length <= 0){
						if($(".muralVazio").is(":empty")){
							$(".muralVazio").html("Nenhum resultado encontrado").show();
						}
					} else {
						$(".muralVazio").html("Fim das postagens").show();
					}
					$("#contentcolumn .paginaAtual").hide();
				}
				$(".paginaAtual").removeClass("loading").attr("data-page", parseInt($(".paginaAtual").attr("data-page"))+1);
				
				if(data && data.busca && data.busca.posts.length < 10){
					if($(".listaMural li").length <= 0){
						if($(".muralVazio").is(":empty")){
							$(".muralVazio").html("Nenhum resultado encontrado").show();
						}
					}else {
						$(".muralVazio").html("Fim das postagens").show();
					}
					$("#contentcolumn .paginaAtual").hide();
				}else{
					$("#contentcolumn .paginaAtual").show();
				}
				if( data.mensagem){
					$("#contentcolumn .paginaAtual").hide();
					jAlert( data.mensagem.mensagem,  data.mensagem.status, null);
				}
				var textFilter = $(".infoFilter .mensagemPost").text(); 
				if(textFilter != ""){
					$(".infoFilter").removeClass("hidden");
				}
			}
		});
	}
	
	var createPost = function(post, openComentario){
		openComentario = openComentario ? openComentario : false;
		postLI(post, openComentario, modeMural, true);
	}
	
	var showPosts = function(posts, clear, openComentarios, modeMural) {
		var conteudo = "";
		
		for(var i in posts) {
			var post = posts[i];
			this.totalPost += 1;
			postLI(post, openComentarios, modeMural);
		}
	}
	
	function postLI(post, openComentarios, modeMural, isPrepend, openComentario) {
		var isPrepend = isPrepend ? isPrepend : false,
			fav = 0,
			favoritado = "favoritado",
			textoFav = "Remover Favorito",
			curtir = 0,
			curtido = "active",
			textoCurtir = "Curtir (Desfazer)",
			used = "used",
			isDestaque = "",
			isAgendado = post.statusPubStr == "agendado",
			isExpirado = post.statusPubStr == "expirado",
			prioritario = post.prioritario ? 'prioritario' : '',
			task = post.postTask && post.postTask.id ? 'task' : '';

		if(post.favorito == 0){
 			var fav = 1;
 			var favoritado = "";
 			var textoFav = "Favoritar";
		}
		
		if(post.like == 0){ 
 			var curtir = 1;
 			var curtido = "";
 			var textoCurtir = "Curtir";
 			var used = "";
		}
		
		if(post.checkDestaque == 1){
 			var isDestaque = "destaque";
 		}
			
		var arquivos = arquivosPost(post),
			mensagemConteudo,
			tema = "",
			titulo = "",
			isLink = 0,
			isDoc = 0;
		
		if(post.titulo){
			var textTitulo = post.titulo.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
			var titulo = '<a href="'+urlContext+'/pages/post.htm?id='+post.id+'">'+textTitulo+'</a>';
		}
		if(post.mensagem){
			if(PARAMS.WORDPRESS_ON == "1") {
				tema = "themeWordPress";
				mensagemConteudo = {};
				mensagemConteudo.html = post.mensagem;
			} else {
				mensagemConteudo = 	XBBCODE.process({
				    text: post.mensagem,
				    removeMisalignedTags: false
				});
			}
		}
		
		var conteudo = "";
		var hidden = isPrepend ? 'style="display: none;"' : '';
		
		if(modeMural == 'expandido'){
			if(SESSION.USER_INFO.IDIOMA_ID != ""){
				conteudo += '<li id="postId-'+post.id+'" class="post-idioma '+post.statusPubStr+' '+isDestaque+' '+prioritario+' '+task+'" data-id="'+post.id+'" '+hidden+'>';
				conteudo += createTabsIdiomas(post.idiomas, post.id, true);
			}else if(post.idiomas[0] && post.idiomas[0].idioma){
				conteudo += '<li id="postId-'+post.id+'" class="post-idioma '+post.statusPubStr+' '+isDestaque+' '+prioritario+' '+task+'" data-id="'+post.id+'" '+hidden+'>';
				conteudo += createTabsIdiomas(post.idiomas, post.id, true);
			}else{
				conteudo += '<li id="postId-'+post.id+'" class="'+post.statusPubStr+' '+isDestaque+' '+prioritario+' '+task+'" data-id="'+post.id+'" '+hidden+'>';
				// apagar
				conteudo += '<div class="portlet-title tabbable-line pull-right">'
							 + '<ul class="nav nav-tabs">'
						  		+ '<li class="mural-idiomas active">'
						  		   + '<a href="#tab_idioma_1" data-toggle="tab">Idioma 01</a>'
					  		    + '</li>'
						  		+ '<li class="mural-idiomas">'
						  		   + '<a href="#tab_idioma_2" data-toggle="tab">Idioma 02</a>'
					  		    + '</li>'
				  		     + '</ul>'
			  		      + '</div>';
				//
			}
			conteudo += '<div class="post-content clearfix">';
				if(isAgendado){
					conteudo += '<div class="status-publicacao-wrapper agendado"><div class="status-publicacao-shadow"></div><div class="icone-status-publicacao bs-tootlip" title="Post agendado para: '+post.dataPubStr+'"><i class="fa fa-calendar"></i></div></div>';
				}
				if(isExpirado){
					conteudo += '<div class="status-publicacao-wrapper expirado"><div class="status-publicacao-shadow"></div><div class="icone-status-publicacao bs-tooltip" title="Post expirado em: '+post.dataExpStr+'"><i class="fa fa-calendar-times-o"></i></div></div>';
				}
				if(post.prioritario){
					conteudo += '<div class="status-publicacao-wrapper prioritario"><div class="status-publicacao-shadow"></div><div class="icone-status-publicacao bs-tooltip" title="Post prioritário"><i class="fa fa-flag"></i></div></div>';
				}
				if(post.postTask && post.postTask.id){
					conteudo += '<div class="status-publicacao-wrapper task"><div class="status-publicacao-shadow"></div><div class="icone-status-publicacao bs-tooltip" title="Post tarefa"><i class="fa fa-clipboard"></i></div></div>';
				}
				if(post.checkDestaque == "1"){
					conteudo += '<div class="status-publicacao-wrapper destaque"><div class="status-publicacao-shadow"></div><div class="icone-status-publicacao bs-tooltip" title="Post destacado"><i class="fa fa-certificate"></i></div></div>';
				}
				conteudo +=	'<div class="fotoUsuario pequena" data-nome="'+post.usuarioNome+'" data-foto="'+post.urlFotoUsuarioThumb+'" data-id="'+post.usuarioId+'">';
					conteudo +=	'<a href="'+urlContext+'/usuario.htm?id='+post.usuarioId+'" class="nome" data-nome="'+post.usuarioNome+'" data-foto="'+post.urlFotoUsuarioThumb+'" data-id="'+post.usuarioId+'"><img src="'+post.urlFotoUsuarioThumb+'" alt="'+post.usuarioNome+'"/></a>';
				conteudo +=	'</div>';
				conteudo +=	'<div class="nomeGrupo">';
					conteudo +=	'<a class="nomeUsuario" href="'+urlContext+'/usuario.htm?id='+post.usuarioId+'" data-nome="'+post.usuarioNome+'" data-foto="'+post.urlFotoUsuarioThumb+'" data-id="'+post.usuarioId+'">'+post.usuarioNome+'</a>';
					
					if(post.grupos){
						conteudo +=	'<span class="gruposPost">&nbsp; para &nbsp;';
							if(post.grupos.length > 0) {
								var contGrupo = 1;
								for(var a = 0; a < post.grupos.length; a++){
									conteudo +=	'<a class="post-grupo" data-nome="'+post.grupos[a].nome+'" data-tipo="'+post.grupos[a].tipo+'" data-foto="'+post.grupos[a].urlFotoThumb+'" data-virtual="'+post.grupos[a].virtual+'" data-id="'+post.grupos[a].id+'" href="'+urlContext+'/pages/mural.htm?grupo='+post.grupos[a].id+'">'+post.grupos[a].nome+'</a>';
									if(contGrupo < post.grupos.length){
										conteudo +=	', ';
										contGrupo++;
									}
								}
							} else {
								if(post.visibilidade == 'Minhas conexões') {
									conteudo +=	'<a href="'+urlContext+'/pages/mural.htm?meusAmigos=true">'+post.visibilidade+'</a>';
								}
								
								if(post.visibilidade == 'Somente Eu') {
									conteudo +=	'<a href="'+urlContext+'/pages/mural.htm?somenteEu=true">'+post.visibilidade+'</a>';
								}
							}
						conteudo +=	'</span>';
					}
					
					var categorias = post.categorias || post.categoria || post.chapeu;
					if(categorias){
						if(Array.isArray(categorias)){
							for(var j = 0; j < categorias.length; j++){
								conteudo += formatCategoria(categorias[j]);
							}
						}else{
							conteudo += formatCategoria(categorias);
						}
					}else{
						conteudo += '<p class="chapeuPost"><a></a></p>';
					}
				conteudo +=	'</div>';
				
				if(post.timestampEdit && post.timestampEdit != post.timestamp){
					conteudo +=	'<p class="dataPost data-update">';
						conteudo += '<span class="bs-tooltip timeago" title="Publicado em:<br/>'+dateConverter(post.timestampPub)+' <br/><br/>Atualizado em:<br/>'+dateConverter(post.timestampEdit)+'" rel="'+post.timestampEdit+'">'+dateConverter(post.timestampEdit)+'</span>';
						conteudo += '<span>Última atualização</span>';
					conteudo += '</p>';
				}else{
					conteudo +=	'<p class="dataPost timeago bs-tooltip" title="'+timeConverter(post.timestampPub)+'" rel="'+post.timestampPub+'">'+dateConverter(post.timestampPub)+'</p>';
				}
				conteudo +=	'<div class="corpoIdioma tab-content tab-idiomas clearfix">';
					for(var i in post.idiomas) {
						var postIdioma = post.idiomas[i];
						conteudo += createMensagemIdioma(postIdioma, post, tema, mensagemConteudo);
					}
				conteudo +=	'</div>'; // fecha corpoIdioma

				if(SESSION.PERMISSOES.VISUALIZAR_CONTEUDO == 'true'){
					conteudo +=	arquivos.imagensPost;


					if(post.destaque && post.destaque.tipo != 0 && post.destaque.tipo!= 2 && (post.urlVideo || post.urlSite) ){
						var linkUrlDestaque = post.urlSite;
						var classVideo = "";
						var classFancy = 'target="_blank"';
						var detailsStyle = post.urlImagem || post.urlVideo ? '' : 'style="width: 100%";';
						if(post.urlVideo){
							linkUrlDestaque = post.urlVideo;
							classVideo = "comVideo";
							classFancy = 'class="fancyboxYoutube"';
						}
						conteudo += '<div class="liveurl" style="display: block;">';
							conteudo += '<div class="inner">';
								if(post.urlImagem){
									conteudo +=	'<div class="image active '+classVideo+'" style="display: block;">';
										conteudo += '<a href="'+linkUrlDestaque+'" '+classFancy+'>';
											conteudo += '<img src="'+post.urlImagem+'" alt="'+post.destaque.titulo+'" class="active">';
										conteudo += '</a>';
									conteudo += '</div>';
								}else if(post.destaque.tipo == 1){
									conteudo +=	'<div class="image active '+classVideo+'" style="display: block;">';
										conteudo += '<a href="'+linkUrlDestaque+'" data-link="'+linkUrlDestaque+'" class="fancyJwplayer"><img src="'+urlContext+'/img/blank.gif" alt="'+post.destaque.titulo+'" class="active"></a>';
									conteudo += '</div>';
								}
								conteudo +=	'<div class="details" '+detailsStyle+'>';
									conteudo += '<div class="info">';
										conteudo += '<div class="title"><a href="'+linkUrlDestaque+'" target="_blank">'+post.destaque.titulo+'</a></div>';
										conteudo += '<div class="description">'+post.destaque.descricao+'</div>';
										conteudo += '<div class="url"><a href="'+linkUrlDestaque+'" target="_blank">'+linkUrlDestaque+'</a></div>';
									conteudo += '</div>';
								conteudo += '</div>';
							conteudo += '</div>';
						conteudo += '</div>';
					}
				
					conteudo +=	 arquivos.anexosPost;
					if(post.tags){
						conteudo +=	'<div class="clear"></div>';
						conteudo += '<ul class="tagsList">'
							var contTags = 1;
							for(var a=0; a < post.tagsList.length; a++){
								conteudo +=	'<li class="tagName">';
									conteudo +=	'<a href="'+urlContext+'/pages/mural.htm?tag='+post.tagsList[a].id+'">'+post.tagsList[a].nome+'</a>';
								conteudo +=	'</li>';
								contTags += 1;
							}
						conteudo += '</ul>';
					}

					//interacao post
					if(post.audiencia){
						if(urlAtual.indexOf("post.htm") > -1){
							conteudo += '<div class="campoPost postInfo view-count">';
								conteudo +=	'<label>Post visto por '+post.audiencia.iteracao+' de '+post.audiencia.total+' pessoas</label>';
								conteudo += '<div class="progress audiencia">';
									conteudo += '<a class="clearfix"';
										if(SESSION.PERMISSOES.VISUALIZAR_RELATORIOS == 'true' || SESSION.USER_INFO.USER_ID == post.usuarioId){
											conteudo += ' href="'+urlContext+'/report/detalhesAudienciaReport.htm?id='+post.id+'"';
										}
										conteudo += '><div class="progress-bar" role="progressbar" data-iteracao="'+post.audiencia.iteracao+'" style="width:'+(post.audiencia.iteracao / post.audiencia.total) * 100+'%"></div>'
									conteudo +=	'</a>';
								conteudo += '</div>';
							conteudo +=	'</div>';
						}
					}
				}

				if(PARAMS.MANUTENCAO != 1){
					conteudo +=	'<ul class="interacoesPost clearfix">';
				
					if(SESSION.PERMISSOES.CURTIR_PUBLICACAO == 'true'){
						if(post.likeable){
							conteudo +=	'<li class="interactions"><a href="#" class="clearfix botaoLike '+used+' bs-tooltip-bottom" data-post="'+post.id+'" data-usuario="'+SESSION.USER_INFO.USER_ID+'" data-favorito="'+curtir+'" title="'+textoCurtir+'"><i class="linear-icons icone-likeNew '+curtido+'"></i><span ';
							if(!post.likeCount || post.likeCount <= 0){ 
								conteudo +=	'style="display:none;"';	
							}
							conteudo +=	'>';
							if(!post.likeCount){ 
								conteudo +=	0;
							}else{
								conteudo +=	post.likeCount;
							}
							conteudo +=	'</span></a></li>'; //final 1a li
						}
					}
					
					if(SESSION.PERMISSOES.COMENTAR == 'true' || SESSION.PERMISSOES.VISUALIZAR_COMENTARIOS == 'true'){
						if(post.likeable){
							conteudo +=	'<li class="interactions">';
								if(SESSION.PERMISSOES.COMENTAR == 'false' && (!post.commentCount || post.commentCount <= 0)){
									conteudo += '<span></span>';
								}else{
									conteudo +=	'<a href="#" class="clearfix botaoComentarios bs-tooltip-bottom" data-post="'+post.id+'" data-usuario="'+userId+'" title="Comentar"><i class="linear-icons icone-comentarioNew"></i>';
										if(!post.commentCount || post.commentCount <= 0){
											conteudo +=	'<span style="display:none;">';
										} else {
											conteudo +=	'<span>';
										}
										if(!post.commentCount){
											conteudo +=	0;
										}else{
											conteudo +=	post.commentCount;
										}
										conteudo +=	'</span>';
									conteudo +=	'</a>';
								}
							conteudo +=	'</li>';
						}
					}
					
					
					if(SESSION.PERMISSOES.CURTIR_PUBLICACAO == 'true'){
						if(post.likeable){
							conteudo += '<li class="interactions">';
								conteudo +=	'<a href="#" class="clearfix botaoFavorito bs-tooltip-bottom" data-post="'+post.id+'" data-usuario="'+SESSION.USER_INFO.USER_ID+'" data-favorito="'+fav+'" title="'+textoFav+'">';
									conteudo +=	'<i class="linear-icons icone-favoritoNew '+favoritado+'"></i>';
								conteudo +=	'</a>';
							conteudo +=	'</li>';
						}
					}
	
					if((post.usuarioId == userId || (SESSION.PERMISSOES.EDITAR_PUBLICACAO == 'true' || SESSION.PERMISSOES.EXCLUIR_PUBLICACAO == 'true' || SESSION.PERMISSOES.ALL_GRUPOS == 'true')) && PARAMS.WORDPRESS_ON != 1){
						conteudo +=	'<li class="interactions right no-margin-right">';
							conteudo +=	'<div class="botaoConfig">';
								conteudo +=	'<i class="linear-icons icone-postConfigNew"></i>';
								conteudo +=	'<div class="popMenu top';
									if(post.categoria){
										if(post.categoria.id == PARAMS.CATEGORIA_INCIDENTES_ID){
											conteudo += ' incidentes';
										}
									}
									conteudo += '">';
									conteudo +=	'<ul>';
										if(post.usuarioId == userId || SESSION.PERMISSOES.EDITAR_PUBLICACAO == 'true' || SESSION.PERMISSOES.ALL_GRUPOS == 'true'){
											conteudo +=	'<li>';
												if(post.categoria){
													if(post.categoria.id == PARAMS.CATEGORIA_INCIDENTES_ID){
														conteudo +=	'<a class="editar-incidente" data-incidente-id="'+post.id+'">Editar Incidente</a>';
													}else{
														conteudo +=	'<a href="'+urlContext+'/pages/postComunicado.htm?id='+post.id+'" class="postEdit">Editar Post</a>';
													}
												}else{
													conteudo +=	'<a href="'+urlContext+'/pages/postComunicado.htm?id='+post.id+'" class="postEdit">Editar Post</a>';
												}
											conteudo +=	'</li>';
										}
										if(post.usuarioId == userId || SESSION.PERMISSOES.EXCLUIR_PUBLICACAO == 'true'){
											conteudo +=	'<li>';
												if(post.categoria){
													if(post.categoria.id == PARAMS.CATEGORIA_INCIDENTES_ID){
														conteudo +=	'<a class="excluir-incidente" data-incidente-id="'+post.id+'">Excluir Incidente</a>';
													}else{
														conteudo +=	'<a href="#" class="excluirPost" data-post="'+post.id+'">Excluir Post</a>';
													}
												}else{
													conteudo +=	'<a href="#" class="excluirPost" data-post="'+post.id+'">Excluir Post</a>';
												}
											conteudo +=	'</li>';
										}
									conteudo +=	'</ul>';
								conteudo +=	'</div>';
							conteudo +=	'</div>';
						conteudo +=	'</li>';
					}
					conteudo +=	'<li class="interactions right no-margin-right">';
						conteudo +=	'<div class="infoPost">';
							conteudo +=	'<i class="linear-icons icone-infoNew"></i>';
							conteudo += '<div class="popMenu top">';
								conteudo +=	'<ul>';
									if(post.titulo){
										conteudo +=	'<li class="paddingInfoPost cinza tituloInfoPost">';
											conteudo +=	'<a class="post-title sbold" href="'+urlContext+'/pages/post.htm?id='+post.id+'">'+textTitulo+'</a>';
										conteudo +=	'</li>';
									}else{
										conteudo +=	'<li class="paddingInfoPost cinza tituloInfoPost">';
											conteudo +=	'<a class="post-title sbold" href="'+urlContext+'/pages/post.htm?id='+post.id+'">Sem título</a>';
										conteudo +=	'</li>';
									}
									if(post.categoria){
										conteudo +=	'<li class="paddingInfoPost">';
											conteudo +=	'<span class="spandarker">Categoria: </span>';
											conteudo +=	'<a href="'+urlContext+'/pages/mural.htm?categoria='+post.categoria.id+'" class="blue" style="color:#'+post.categoria.cor+'">'+post.categoria.nome+'</a>';
										conteudo +=	'</li>';
										//peridida conteudo +=	'</li>';
									}
									if(post.grupos){
										conteudo +=	'<li class="paddingInfoPost"><span class="spandarker">Para: </span> ';
										var contGrupo = 1;
										for(var a=0; a < post.grupos.length; a++){
											conteudo +=	'<a href="'+urlContext+'/pages/mural.htm?grupo='+post.grupos[a].id+'" class="blue">'+post.grupos[a].nome+'</a>';
											if(contGrupo < post.grupos.length){
												conteudo +=	', ';
											}
											contGrupo += 1;
										}
										conteudo +=	'</li>';
									}

//									if(SESSION.PERFILS.PERFIL_isAdmin){
										conteudo +=	'<li class="paddingInfoPost">';
											conteudo +=	'<span class="spandarker">ID Post: </span>';
											conteudo +=	'<a class="blue" href="'+urlContext+'/pages/post.htm?id='+post.id+'">'+post.id+'</a>';
										conteudo +=	'</li>';
//									}

									if(post.tags){
										conteudo +=	'<li class="paddingInfoPost">';
											conteudo +=	'<span class="spandarker">Tags: </span> ';
											var contTags = 1;
											for(var a=0; a < post.tagsList.length; a++){
												conteudo +=	'<a href="'+urlContext+'/pages/mural.htm?tag='+post.tagsList[a].id+'" class="blue">'+post.tagsList[a].nome+'</a>';
												if(contTags < post.tagsList.length){
													conteudo +=	', ';
												}
												contTags += 1;
											}
										conteudo +=	'</li>';
									}
									conteudo +=	'<li class="paddingInfoPost">';
										if(post.statusPubStr){
											conteudo +=	'<b>Status:</b> '+post.statusPubStr+' ';
										}
										if(post.timestamp){
											conteudo +=	'<br/><b>Data Criação:</b> '+ dateConverter(post.timestamp);
										}else{
											conteudo +=	'<b>Data Criação: </b> -';										
										}
	
										if(post.timestampPub){
											conteudo +=	'<br/><b>Data Publicação:</b> '+dateConverter(post.timestampPub)+' ';
											if(post.timestampPub && post.statusPubStr == "agendado"){
												conteudo +=	' - agendado';
											}
										}else{
											'<br/><b>Data Publicação: </b> - ';
										}
										if(post.timestampEdit && post.timestampEdit != post.timestamp){
											conteudo +=	'<br/><b>Data Atualização:</b> '+dateConverter(post.timestampEdit)+' ';
										}else{
											conteudo +=	'<br/><b>Data Atualização:</b> -';
										}
										if(post.timestampExp){
											conteudo +=	'<br/><b>Data Expiração:</b> '+dateConverter(post.timestampExp)+'';
											if(post.timestampExp && post.status == "expirado"){
												conteudo +=	' - expirado';
											}
										}else{
											conteudo +=	'<br/><b>Data Expiração:</b> -';										
										}
									conteudo +=	'</li>';
									conteudo += '<li class="paddingInfoPost">';
										conteudo +=	'<b>Criado por: </b>';
										conteudo +=	'<a class="blue" href="'+urlContext+'/usuario.htm?id='+post.usuarioId+'">'+post.usuarioNome+'</a>';
										conteudo +=	'<br/>';
										if(post.usuarioUpdateId && post.usuarioUpdateId != post.usuarioId){
											conteudo +=	'<b>Atualizado por: </b>';
											conteudo +=	'<a class="blue" href="'+urlContext+'/usuario.htm?id='+post.usuarioUpdateId+'">'+post.usuarioNomeUpdate+'</a>';										
										}
									conteudo +=	'</li>';
									if(PARAMS.PUSH_CHECK_ON && post.dataPushTimestamp){
										conteudo += '<li class="paddingInfoPost">Push enviado dia: <strong>'+dateConverter(post.dataPushTimestamp)+'</strong> </li>';
									}else{
										conteudo += '<li class="paddingInfoPost">Push não enviado.</li>';									
									}
									if(SESSION.PERMISSOES.VISUALIZAR_RELATORIOS == 'true' || SESSION.USER_INFO.USER_ID == post.usuarioId){
										conteudo +=	'<li class="paddingInfoPost">';
											conteudo +=	'<a class="blue" href="'+urlContext+'/report/detalhesAudienciaReport.htm?id='+post.id+'" target="_blank">Relatório de Audiência</a>';
										conteudo +=	'</li>'; 
									}
								conteudo +=	'</ul>';
							conteudo +=	'</div>';
						conteudo +=	'</div>';
					conteudo += '</li>';

					if(PARAMS.MURAL_POST_OPCOES && PARAMS.MURAL_POST_OPCOES == "1"){
						var classeLink = post.sendNotification ? 'silenciar-post' : 'ressoar-post';
						var classeIcone = post.sendNotification ? 'icone-bell-gray' : 'icone-bell-gray-slash';
						var title = post.sendNotification ? 'Silenciar notificações' : 'Ativar notificações';
						conteudo +=	'<li class="interactions right no-margin-right">';
							conteudo += '<a class="clearfix post-notifications-options '+classeLink+' bs-tooltip-bottom" title="'+title+'" data-post-id="'+post.id+'">';
								conteudo += '<i class="linear-icons '+classeIcone+'"></i>';
							conteudo += '</a>';
						conteudo += '</li>';
					}
					if(PARAMS.MURAL_POST_TASK && PARAMS.MURAL_POST_TASK == "1"){
						var classeIcone = post.postTask ? 'icone-task-ativo' : 'icone-task';
						var classeLink = post.postTask ? 'desativar-task' : 'ativar-task';
						var title = post.postTask ? 'Desativar tarefa' : 'Ativar tarefa';
						conteudo +=	'<li class="interactions right no-margin-right">';
							conteudo += '<a class="clearfix post-task-options '+classeLink+' bs-tooltip-bottom" title="'+title+'" data-post-id="'+post.id+'"'
							if(post.postTask) { conteudo += ' data-task-id="'+ post.postTask.id +'" '; }							 
								conteudo += '><i class="linear-icons '+classeIcone+'"></i>';
							conteudo += '</a>';
						conteudo += '</li>';
					}
					
					conteudo +=	'<div class="abreComentarios">';
						conteudo +=	'<ul class="listaComentarios">';
							conteudo +=	'<li class="loading"></li>';
						conteudo +=	'</ul>';

						if(SESSION.PERMISSOES.COMENTAR == 'true'){
							conteudo +=	'<ul class="listaComentarios comentar">';
								conteudo +=	'<li class="clearfix">';
									conteudo +=	'<div class="fotoUsuario pequena">';
										conteudo +=	'<a>';
											conteudo +=	'<img src="'+SESSION.USER_INFO.USER_FOTO+'" alt="Foto do '+SESSION.USER_INFO.USER_NOME+'" />';
										conteudo +=	'</a>';
									conteudo +=	'</div>';

									conteudo +=	'<div class="comentario clearfix">';
										conteudo +=	'<textarea nome="comentario_'+post.id+'" id="comentario_'+post.id+'" data-post="'+post.id+'" data-usuario="'+userId+'" class="placeholder mentions textAutoSize" placeholder="Escreva seu comentário"></textarea>';
										if(buildType != "cielo"){
											conteudo += '<div class="uploader-hold-icon"><i class="linear-icons icone-clip-grey bs-tooltip" title="Inserir arquivo"></i></div>';
						            		conteudo +=	'<div class="uploaderNovo">';
						            			conteudo +=	'<input type="hidden" id="anexosId_'+post.id+'" name="anexosId_'+post.id+'" class="inputAnexos" value=""/>';
						            			conteudo +=	'<div class="multiplefileuploader" data-postId="" data-commentId="" data-msgId="" class="bs-tooltip" title="Inserir Anexo"></div>';
						            		conteudo +=	'</div>';
										}
						            	conteudo += '<ul class="anexos"></ul>';
						            conteudo +=	'</div>';
				            	conteudo +=	'</li>';
		            		conteudo += '</ul>';
						}
				  conteudo += '</div>'; //final abreComentarios

			   conteudo += '</ul>'; //final interacoesPost
		   conteudo += '</div>';
			} // Final if PARAMS.MANUTENCAO
			conteudo += '</li>'; //final li post
		}else{
			var iframeContent = false;
			
			conteudo += '<li id="postId-'+post.id+'" class="clearfix cursor-pointer consolidado '+post.statusPubStr+' '+isDestaque+'" data-id="'+post.id+'">';
				if(isAgendado){
					conteudo +=	'<i class="icones iconesStatus bs-tooltip ico-postAgendado" title="Post '+post.statusPubStr+'<br/>para '+post.dataPubStr+'"></i>';
				}
				if(isExpirado){
					conteudo +=	'<i class="icones iconesStatus bs-tooltip ico-postExpirado" title="Post '+post.status+'<br/> '+post.dataExpStr+'"></i>';
				}
				if(isRascunho){
					conteudo +=	'<i class="icones iconesStatus bs-tooltip ico-rascunho" title="Rascunho '+post.status+'"></i>';
				}
				
				conteudo += '<table class="table no-footer">';
					conteudo += '<tbody>';
						conteudo += '<tr>';
							conteudo += '<td width="30%"><a class="nomeUsuario" href="'+urlContext+'/usuario.htm?id='+post.usuarioId+'">'+post.usuarioNome+'</a></td>';
							conteudo += '<td width="30%"><h3 class="tituloPost">'+titulo+'</h3></td>';
							conteudo += '<td width="30%"><p class="descricaoPost">';
								if(post.mensagem && !post.resumo){
									if(post.mensagem.indexOf('<iframe') <= -1 && post.mensagem.indexOf('docs.google.com') > -1 && post.mensagem.indexOf('/forms/') > -1 || post.mensagem.indexOf('goo.gl') > -1 && post.mensagem.indexOf('/forms/')){
										iframeContent = true;
										conteudo += '...';
									}else if(post.mensagem.indexOf('<iframe') > -1 && post.mensagem.indexOf('docs.google.com') > -1 && post.mensagem.indexOf('/forms/') > -1 || post.mensagem.indexOf('goo.gl') > -1 && post.mensagem.indexOf('/forms/')){
										iframeContent = true;
										conteudo += '...';
									}else{
										conteudo +=	mensagemConteudo.html;
									}
								}else if(post.mensagem && post.resumo){
									conteudo +=	mensagemConteudo.html;
								}
								
							conteudo += '</p></td>';
							if((post.destaque && post.destaque.tipo != 0 && post.destaque.tipo!= 2 && (post.urlVideo || post.urlSite)) || arquivos.imagensPost || arquivos.anexosPost || iframeContent){
								conteudo += '<td width="5%">';
									conteudo += '<i class="linear-icons icone-clip-grey pull-right"></i>';
								conteudo += '</td>';
								conteudo += '<td width="5%">';	
							}else{
								conteudo += '<td width="10%">';
							}
								conteudo += '<p class="dataPost timeago bs-tooltip" title="'+timeConverter(post.timestampPub)+'" rel="'+post.timestampPub+'">'+dateConverter(post.timestampPub)+'</p></td>';
						conteudo += '</tr>';
					conteudo += '</tbody>';
				conteudo += '</table>';
			conteudo += '</li>';
		}
		
		
		if(isPrepend){
			$(".listaMural:not(.preview)").prepend(conteudo);
			$('body').animate({ scrollTop: 0 }, 800, 'easeOutExpo');
			
			setTimeout(function(){
				$('.cancelarForm').click();
				$('.btPostarComunicado').removeClass('ac_loading').attr('disabled', false);
				
				$('.listaMural:not(.preview)').find('li:first-child').slideDown();
			}, 500);
		}else{
			$(".listaMural:not(.preview)").append(conteudo);
		}
		
		$('.bs-tooltip').tooltip({ html: true });
		$('.bs-tooltip-bottom').tooltip({ html: true, placement: 'bottom' });
		
		tooltipGrupoMural($('#postId-'+post.id+' .post-grupo'));
		tooltipNomeUsuario($('#postId-'+post.id+' .fotoUsuario.pequena'));
		tooltipNomeUsuario($('#postId-'+post.id+' .nomeUsuario'));
		
		$("#postId-"+post.id).each(function(){
			var obj = $(this);
			if(isPrepend){
				setTimeout(function(){
					manageLayoutPost(post, obj);
				}, 550);
			}else{
				manageLayoutPost(post, obj);
			}
		});
		
		$('.textAutoSize').autosize();
		chamaUploader($("#postId-"+post.id+" .multiplefileuploader"));
    	findUrls("#postId-"+post.id+" .descricaoPost", true);
    	findUrls("#postId-"+post.id+" .mensagemPost article", true);
    	conteudo = "";
    	if(openComentarios){
    		$(".botaoComentarios").click();
    	}
    	
	}
	
	/**
	 * Return: 
	 * {
	 * 	anexos: "" | String
	 * 	images: "" | String
	 * }
	 */
	arquivosPost = function(post) {
		if(!post.arquivos || post.arquivos.length == 0) {
			return {imagensPost: "", anexosPost: "", anexosReduzidos: ""};
		}
		/**
		 * Filtra e deixa apenas os anexos, que não são imagens
		 */
		var anexos = post.arquivos.filter(function(arquivo) {
		    return !isImage[arquivo.extensao.toLowerCase()];
		});

		/**
		 * Filtra e deixa apenas as imagens
		 */
		var imagens = post.arquivos.filter(function(arquivo) {
		  return isImage[arquivo.extensao.toLowerCase()];
		});
		
		/** 
		 * Anexos do post
		 * */
		var anexosPost = '<ul class="anexos">';
		for(var i in anexos) {
			anexosPost += createLiAnexo(anexos[i]);
		}
		anexosPost += "</ul>";
		
		/**
		 * Imagens do post
		 */
		var imagensPost = "";
		if(imagens.length > 0) {
			var qtdeImagens = imagens.length;
			var classeImg = ['zero', 'uma', 'duas', 'tres', 'quatro', 'cinco', 'seis'];
			var itens = classeImg[qtdeImagens] ? classeImg[qtdeImagens]  :  classeImg[classeImg.length - 1];
			if(PARAMS.MURAL_LAYOUT_ANEXOS_REDUZIDO_ON == '1'){
				imagensPost += '<div class="clearfix anexos anexos-reduzidos">';
					imagensPost += '<ul>';
						for(var i in imagens) {
							imagensPost += createLiReduzida(post, imagens[i], qtdeImagens, i);
						}
					imagensPost += '</ul>';
				imagensPost += '</div>';
			}else{
				imagensPost += '<div class="imagensPost '+ itens + ' clearfix">';
					imagensPost += '<ul>';
						for(var i in imagens) {
							imagensPost += createLiImage(post, imagens[i], qtdeImagens, i);
						}
					imagensPost += '</ul>';
				imagensPost += '</div>';
			}
		}
		
		return {
			imagensPost: imagensPost,
			anexosPost: anexosPost
		}
		
	}
	
	function createLiReduzida(post, arquivo, qtd, idx){
		var extensao = (arquivo.extensao).toLowerCase(),
			imagemPost = "";

		imagemPost += '<li>';
			imagemPost += '<div class="thumbnail">';
				if(FILE_EXTENSIONS.image[extensao]){
					imagemPost += '<div class="fotoUsuario media no-radius">';
						imagemPost += '<a class="fancybox" href="'+arquivo.url+'" target="_blank" rel="anexos-thumb-'+post.id+'">';
							imagemPost += '<img src="'+arquivo.url+'" alt="'+arquivo.file+'" />';
						imagemPost += '</a>';
					imagemPost += '</div>';
				}else{
					imagemPost += '<i class="file-extensions-icons-medium '+extensao+'"></i>';
					imagemPost += '<span class="extension">'+extensao+'</span>';
				}
			imagemPost += '</div>';

			imagemPost += '<div class="nomeAnexo alterarNome">';
				if(FILE_EXTENSIONS.image[extensao]){
					imagemPost += '<a class="fancybox" href="'+arquivo.url+'" target="_blank" rel="anexos-name-'+post.id+'">';
						imagemPost += arquivo.file + '<span>('+arquivo.lengthHumanReadable+')</span>';
					imagemPost += '</a>';

					if(arquivo.descricao && arquivo.descricao != ''){
						imagemPost += '<span class="descricaoUpload">'+arquivo.descricao+'</span>';
					}
				}else{
					imagemPost += '<a data-link="'+arquivo.url+'" data-url="'+arquivo.url+'" data-ext="'+extensao+'" data-name="'+arquivo.file+'" class="previewFile">'+arquivo.file;
						imagemPost += '<span>'+arquivo.lengthHumanReadable+'</span>';
					imagemPost += '</a>';

					if(arquivo.descricao && arquivo.descricao != ''){
						imagemPost += '<span class="descricaoUpload">'+arquivo.descricao+'</span>';
					}
				}
			imagemPost += '</div>';
		imagemPost += '</li>';

		return imagemPost;
	}

	function createLiImage(post, arquivo, qtdeImagens, position) {
		var extensao = (arquivo.extensao).toLowerCase();
		var imagenPost = "";
		if(arquivo.descricao){
			var descri = arquivo.descricao + " - ";
		}else{
			var descri = "";
		}
		
		if(qtdeImagens == 1){
			imagenPost += '<li>';
				imagenPost += '<div>';
					imagenPost += '<a href="'+arquivo.url+'" class="fancybox" title="'+ descri + '' + arquivo.file+'" rel="categoria-'+post.id+'">';
						imagenPost += '<img src="'+arquivo.url+'"  alt="'+arquivo.file+'" />';
					imagenPost += '</a>';
				imagenPost += '</div>';
				if(arquivo.descricao) {
					imagenPost += '<span class="nomeImagem">'+arquivo.descricao+'</span>';
				}
			imagenPost += '</li>';
		} else {
			var left = '';
			var extensao = (arquivo.extensao).toLowerCase();
			
			if(position == 0 && qtdeImagens > 1){
				left = 'first';
			} else if(position == 1 && qtdeImagens > 2){
				left = 'second';
			} else if(position == 2 && qtdeImagens > 3 && qtdeImagens != 4){
				left = 'third';
			} else if(position == 2 && qtdeImagens == 4){
				left = 'third noMargin';
			} else if(position == 3 && qtdeImagens == 4){
				left = 'fourth';
			} else if(position == 3 && qtdeImagens > 4 && qtdeImagens != 5){
				left = 'fourth noMargin';
			} else if(position == 3 && qtdeImagens == 5){
				left = 'fourth half';
			}else if(position == 4 && qtdeImagens == 5){
				left = 'fifth';
			}else if(position == 5 && qtdeImagens > 6){
				dataCount = 6;
				var count = (qtdeImagens - dataCount);
				imagenPost += '<li class="linkFancyFotos">+ '+count+'</li>';
			}else{
				left = '';
			}
			var showHide = '';
			if(position >= 6){
				showHide = 'style="display:none"';
			}
			
			imagenPost += '<li class="'+left+'" '+showHide+'>';
				imagenPost += '<div>';
					imagenPost += '<a href="'+arquivo.url+'" class="fancybox" title="'+ descri + '' + arquivo.file+'" rel="categoria-'+post.id+'">';
						imagenPost += '<img src="'+arquivo.url+'"  alt="'+arquivo.file+'" />';
					imagenPost += '</a>';
				imagenPost += '</div>';
			if(arquivo.descricao){
				imagenPost += 	'<span class="nomeImagem">'+arquivo.descricao+'</span>';
			}
			imagenPost += 	'</li>';
		}
		return imagenPost;
	}
	
	function createLiAnexo(arquivo) {
		var extensao = arquivo.extensao.toLowerCase(), classePlayer = isPlayer[extensao] ? isPlayer[extensao] : "";
		
		anexoPost = '<li data-id="'+arquivo.id+'">';
			anexoPost += '<div class="thumbnail">';
				anexoPost += '<i class="file-extensions-icons-medium '+extensao+'"></i>';
				anexoPost += '<span class="extension">'+extensao+'</span>';
			anexoPost += '</div>';
			anexoPost += '<div class="nomeAnexo">';
				anexoPost += '<a class="'+classePlayer+'" data-name="'+arquivo.file+'" data-url="'+arquivo.url+'" data-link="'+arquivo.url+'" data-ext="'+extensao+'">'+arquivo.file;
				anexoPost += '<span>('+(arquivo.length/1024).toFixed(2)+'kb)</span></a>';
			
				anexoPost += arquivo.descricao ? ' <span class="descricaoUpload">'+arquivo.descricao+'</span>' : "";
			
			anexoPost += '</div>';
		anexoPost += '</li>';
		
		return anexoPost;
	}
	return {
		totalPost: totalPost,
		buscaPosts: buscaPosts,
		buscaPost: buscaPost,
		createPost: createPost
	}
}());

if(PARAMS.MURAL_POST_OPCOES && PARAMS.MURAL_POST_OPCOES == "1"){
	function silenciarPost(obj){
		jQuery.ajax({
			type: 'POST',
			url: urlContext + '/rest/post/notification',
			data: obj.data,
			beforeSend: function(resp){
				$(obj.element).find('i').addClass('ui-autocomplete-loading');
			},
			success: function(resp){
				$(obj.element).tooltip('destroy');
				var tooltipContent = '';
				
				setTimeout(function(){
					$(obj.element).find('i').removeClass('ui-autocomplete-loading');
				}, 100);
				
				if(obj.isSilenciar){
					$(obj.element).removeClass('silenciar-post').addClass('ressoar-post');
					$(obj.element).attr('title', 'Ativar notificações');
					$(obj.element).find('i').removeClass('icone-bell-gray').addClass('icone-bell-gray-slash');
					tooltipContent = 'Ativar notificações';

				}else{
					$(obj.element).removeClass('ressoar-post').addClass('silenciar-post');
					$(obj.element).attr('title', 'Silenciar notificações');
					$(obj.element).find('i').removeClass('icone-bell-gray-slash').addClass('icone-bell-gray');
					tooltipContent = 'Silenciar notificações';
				}
				
				if(resp.status != 'OK'){
					toastr.error(resp.message);
				}else{
					toastr.success(resp.message);
				}

				setTimeout(function(){
					$(obj.element).tooltip({ html: true, placement: 'bottom', title: tooltipContent });
				}, 200);
			},
			error: function(resp){
				setTimeout(function(){
					obj.element.removeClass('ui-autocomplete-loading');
				}, 100);
				toastr.error(resp.message);
			}
		});
	}
}

if(PARAMS.MURAL_POST_TASK && PARAMS.MURAL_POST_TASK == "1"){
	function taskPost(obj){
		jQuery.ajax({
			type: obj.method,
			url: obj.url,
			data: JSON.stringify(obj.data),
			dataType: 'json',
			contentType: 'application/json',
			beforeSend: function(resp){
				$(obj.element).find('i').addClass('ui-autocomplete-loading');
			},
			success: function(resp){
				if(resp.status != "ERROR"){
					$(obj.element).tooltip('destroy');
					var tooltipContent = '';
					
					setTimeout(function(){
						$(obj.element).find('i').removeClass('ui-autocomplete-loading');
					}, 100);
					
					if(obj.isAtivo){
						$(obj.element).removeClass('ativar-task').addClass('desativar-task');
						$(obj.element).attr('title', 'Desativar task');
						$(obj.element).attr('data-task-id',resp.entity.id);
						$(obj.element).find('i').removeClass('icone-task').addClass('icone-task-ativo');
						tooltipContent = 'Desativar task';
						$(obj.element).parent().parent().closest('li').addClass('task');
						$(obj.element).parent().parent().closest('li').prepend('<div class="status-publicacao-wrapper task"><div class="status-publicacao-shadow"></div><div class="icone-status-publicacao bs-tooltip" title="Post tarefa"><i class="fa fa-clipboard"></i></div></div>');
					}else{
						$(obj.element).removeClass('desativar-task').addClass('ativar-task');
						$(obj.element).attr('title', 'Ativar task');
						$(obj.element).find('i').removeClass('icone-task-ativo').addClass('icone-task');
						tooltipContent = 'Ativar task';
						$(obj.element).parent().parent().closest('li').removeClass('task');
						$(obj.element).parent().parent().closest('li').find('.status-publicacao-wrapper.task').remove();
					}	
					$('.bs-tooltip').tooltip({ html: true });
					if(resp.status != 'OK'){
						toastr.error(resp.message);
					}else{
						toastr.success(resp.message);
					}
	
					setTimeout(function(){
						$(obj.element).tooltip({ html: true, placement: 'bottom', title: tooltipContent });
					}, 200);
				}else{
					setTimeout(function(){
						obj.element.removeClass('ui-autocomplete-loading');
					}, 100);
					toastr.error(resp.message);
				}
			},
			error: function(resp){
				setTimeout(function(){
					obj.element.removeClass('ui-autocomplete-loading');
				}, 100);
				toastr.error(resp.message);
			}
		});
	}
}

function manageLayoutPost(post, obj){
	if(SESSION.USER_INFO.IDIOMA_ID != ""){
		$(obj).find('li[data-id-idioma='+SESSION.USER_INFO.IDIOMA_ID+'] a').click();
	}else{
		$(obj).find('li').first().find('a').click();
	}
	
	/* if($(obj).find('iframe').length > 0){
		var iframe = $(obj).find('iframe'), doc = iframe && iframe[0] ? iframe[0].contentWindow.document : null;
		if(doc != null){
			doc.open();
			doc.write(post.mensagem);
			doc.close();
		}
		
		$(iframe).contents().find('body').css({
			'margin': 0
		});
	} */
	
	verMaisPost(obj);

	if(obj.find(".mensagemPost").hasClass("themeWordPress")){
		var msg = obj.find(".mensagemPost");
		msg.find("img").each(function(){
			var img = $(this);
			if(img.closest("a")){
				img.closest("a").addClass("fancybox");
			}
		});
	}
	
	obj.find(".timeago").each(function(){
		var obj = $(this);
		var timestamp = parseInt(obj.attr("rel"),10);
		if(timeVerify(timestamp)){
			obj.timeago();
		}else{
			obj.html(dateConverter(timestamp)).removeAttr("title");
		}
	});
}