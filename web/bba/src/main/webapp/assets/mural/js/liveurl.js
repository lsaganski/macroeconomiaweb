var sitesConhecidos = [
    {
        url: 'invisionapp',
        title: "INVISION ENTERPRISE EDITION",
        description: "THE DESIGN COLLABORATION CLOUD FOR LARGER TEAMS",
        img: urlContext + '/img/file_extensions/avulso/invision.png',
    },
    {
        url: ['google.com/intl/pt-BR/docs/about/', 'docs.google.com/document'],
        img: urlContext + '/img/file_extensions/avulso/googledocs.png',
    },
    {
        url: ['google.com/intl/pt-BR/sheets/about/', 'docs.google.com/spreadsheets'],
        img: urlContext + '/img/file_extensions/avulso/googlespread.png',
    },
    {
        url: ['google.com/intl/pt-BR/slides/about/', 'docs.google.com/presentation'],
        img: urlContext + '/img/file_extensions/avulso/googlepres.png',
    },
    {
        url: 'youtube.com',
        img: urlContext + '/img/file_extensions/avulso/youtube.png',
    },
    {
        url: 'bitbucket.org',
        img: urlContext + '/img/file_extensions/avulso/bitbucket.png'
    },
    {
        url: 'jira.livetouchdev',
        img: urlContext + '/img/file_extensions/avulso/jira.png',
    },
]


var curImages = new Array(),
	tipo = '0',
    isReplaceImage = false,
	liveurlOptions = {
	        loadStart : function(){
	        	$(".modal .liveurl .image").find('img').remove();
	        	var liveUrlP = $(".formPost #form").find(".liveurl");
	        	if(!liveUrlP.is(":visible") && !$(".modal").hasClass("in")) {
	        		$(".inputliveurl").parent().find(".loadingLinkUrl").show();
	        	}
	        	$("#urlPostarLink").addClass("ac_loading");
	        },
	        loadEnd : function(){
	        	var liveUrlP = $(".formPost #form").find(".liveurl");
	        	if(!liveUrlP.is(":visible") && !$(".modal").hasClass("in")) {
	        		$(".inputliveurl").parent().find(".loadingLinkUrl").hide();
	        	}
	        	$("#urlPostarLink").removeClass("ac_loading");
	        },
	        success : function(data) {
                isReplaceImage = false,
	        	curImages = new Array();
                
                if(data.url.match(__imgRegex)){
	        		return false;
	        	}
                
	        	var liveUrlP = $(".formPost #form").find(".liveurl");
	        	if(liveUrlP.is(":visible") && !$(".modal").hasClass("in")) {
	        		return;
	        	}
	        	if ($(".modal").hasClass("in") === false){
	        		$(".postarLink").addClass("disabled");
	        	} else {
	        		var link = $(".modal #insertLinkPreview").val().replace(/https?:\/\//i, "");
	        		if (data.url.indexOf(link) === -1){
	        			return;
	        		}
	        	}
	            var expressionVideo = /([^\s]+(?=\.(mp3|mp4|avi|cam|m1v|m2v|m4v|mmv|wma|midi|wav|AAC|\|WebM|FLV|MP3|MP4|WEBM|AVI|CAM|M1V|M2V|M4V|MMV|WMA|MIDI|WAV))\.\2)/gm;
	           if(data.url.match(expressionVideo)){
	            	tipo = "1";
		        }else{
	            	tipo = "3";
	            }
	            if (data.video != null) {  
	            	if (data.url.indexOf('youtube.com') > -1 || data.url.indexOf('youtu.be') > -1 || data.url.indexOf('vimeo.com') > -1) {
	            		tipo = "1";
	            	}
	            }
	            if(sitesConhecidos && sitesConhecidos.length > 0) {
	               var isChangeDataUrl = false;
                    for(var i in sitesConhecidos) {
                        var site = sitesConhecidos[i];
                        if(Array.isArray(site.url)) {
                            for(var k in site.url) {
                                isChangeDataUrl = data.url && data.url.indexOf(site.url[k]) > -1;
                                if(isChangeDataUrl) break;
                            }
                        } else {
                            isChangeDataUrl = data.url && data.url.indexOf(site.url) > -1
                        }
                        if(isChangeDataUrl) {
                            var changeTitleAndDescription = data.title == 'Yahoo' && data.description == 'Thank you for your patience.';
                            if(changeTitleAndDescription) {
                                data.title = site.title;
                                data.description = site.description;
                            }
                            if(site.img) {
                                isReplaceImage = true;
                                data.img = site.img;
                            }
                        }
                    }
                }

	            if($(".modal").hasClass("in")) {
                    console.log('com modal')
	            	var output = $(".modal").find('.liveurl-wrapper');
	  	            output.find('.liveTitle').val(data.title);
	  	            output.find('.liveDescricao').val(data.description);
	  	            output.find(".tipoUrl").val(tipo);
	  	            output.find('.url').text(data.url);
	  	            output.find('.image img').remove();
	  	            output.find('.image').show();
	  	            output.addClass('in active');
	  	            output.find('.liveurl').fadeIn();
	  	            
	  	            $("#urlLink").slideUp();
	            } else {
	            	var output = $(".formPost #form").find('.liveurl');
	            	if(data.title) {
	            		var titulo = $("#titulo").val();
	            		if(!titulo || titulo == "") {
	            			$("#titulo").val(data.title.trim());
	            		}
	            	}
		            output.find('.title').text(data.title);
		            output.find('.description').text(data.description);
		            output.find('.url').text(data.url);
		            output.find('.image img').remove();

                    if(data.img) {
                        output.find('.image').show().html("").append('<img class="active" src="'+data.img+'" alt="Preview" />');
                    }

		            output.find("#tipoUrl").val(tipo);
		            output.slideDown();
	            }

	        },
	        addImage : function(image){
                if(isReplaceImage) {
                    return;
                }

	        	var output = null;
	        	if($(".modal").hasClass("in")) {
	        		output  = $(".modal").find('.liveurl');
	        	} else {
	        		output  = $(".formPost #form").find('.liveurl');
	        	}
	            var jqImage = $(image);
	            jqImage.attr('alt', 'Preview');
	            
	            if ((image.width / image.height)  > 7 ||  (image.height / image.width)  > 4 ) {
	                return false;
	            } 

	            curImages.push(jqImage.attr('src'));
	            if(!$(".modal").hasClass("in"))
                    $(".formPost #form").find('.liveurl .image img').remove();
	            	$(".formPost #form").find('.liveurl .image').show().html("").append('<img class="active" src="'+curImages[0]+'" alt="Preview" />');
	            $(".modal .liveurl").find('.image').show();
	            $(".modal .liveurl").find('.image .mt-overlay').before(jqImage);
	            
	            if (curImages.length == 1) {
	            	$(".liveurl").find('.image').show();
	                jqImage.addClass('active');
	            }
	            
	            if (curImages.length == 2) {
	            	$(".liveurl").find('.controls .next').removeClass('inactive');
	            }
	        }
	    };



$('document').ready(function() {
	if($(".inputliveurl").length > 0){
		$('.inputliveurl').liveUrl(liveurlOptions);
		
		$(".modal .liveurl").on("click", ".close", function(e){
	    	e.preventDefault();
	        var liveUrl  = $(".modal").find('.liveurl');
	        liveUrl.find('#liveTitle').val('');
	        liveUrl.find('#liveDescricao').val('');
	        liveUrl.find('.url').html('');
	        liveUrl.find('.video').html('').hide();
	        liveUrl.find('.image > img').remove();
	        liveUrl.find('.controls .prev').addClass('inactive');
	        liveUrl.find('.controls .next').addClass('inactive');
	        liveUrl.find('.image').hide();
	        liveUrl.find("#tipoUrl").val("");
	        $("#insertLinkPreview").trigger('clear'); 
	        curImages = new Array();
	        liveUrl.slideUp();
	        $(".inputLiveUrlModal").val("");
	        $("#urlLink").slideDown();
	    });
		
		$(".modal .liveurl").on("click", ".remove-thumb", function(e){
			e.preventDefault();
			e.stopPropagation();
		
			var curActiveImg = $(this).parent(".image").find("img.active")
			var imagens = $(this).parent(".image").find("img");
			var indexImg = imagens.index(curActiveImg);
			
			curActiveImg.remove();
			curImages.splice(indexImg, 1);
				
			if (curImages.length > 0){
				if (indexImg >= curImages.length){
					imagens = $(this).parent(".image").find("img").eq(indexImg - 1).addClass("active");
				} else {
					imagens = $(this).parent(".image").find("img").eq(indexImg).addClass("active");
				}
			}
		});
	}
	
	$('#insertLinkPreview').click(function(event){
		$('#urlPostarLink').liveUrl(liveurlOptions);
		
		setTimeout(function(){
			$('#urlPostarLink').trigger('keyup');
		}, 100);
	});
	
	$("#addLinkPost").click(function(event) {
		var liveUrlM  = $(".modal").find('.liveurl'),
			title = liveUrlM.find('.liveTitle').val(),
			descricao = liveUrlM.find('.liveDescricao').val(), 
			tipo  = liveUrlM.find(".tipoUrl").val(),
			url = liveUrlM.find('.url').text(),
			imgVisible = liveUrlM.find(".image").is(":visible"),
			image = liveUrlM.find('.image img.active').clone(),
			
			liveUrlP = $(".formPost").find(".liveurl");
		
		if($('.formPostar #titulo').val() != ''){
			$('.formPostar #titulo').val(title); 
		}
		
		if(liveUrlM.is(':visible')){
			liveUrlP.find(".title").text(title);
			liveUrlP.find(".description").text(descricao);
			liveUrlP.find(".url").text(url);
			liveUrlP.find("#tipoUrl").val(tipo);
			if(image && image.length > 0){
				liveUrlP.find(".image > img").remove();
				liveUrlP.find(".image").append(image);
				liveUrlP.find(".image").css("display", "block");
				liveUrlP.find('.details').removeAttr('style');
			}else{
				liveUrlP.find('.image img').remove();
				liveUrlP.find(".image").hide();
			}
		}else{
			liveUrlP.find('.title, .url').text($('#urlPostarLink').val());
			liveUrlP.find(".image").hide();
			liveUrlP.find("#tipoUrl").val('3');
		}
		
		liveUrlP.slideDown();
		$("#modalPostarLink").modal("hide");
		
		if(title) {
    		var titulo = $("#titulo").val();
    		if(!titulo || titulo == "") {
    			$("#titulo").val(title.trim());
    		}
    	}
		
		$(".postarLink").addClass("disabled");
	});
	
	
	$(".postarLinkModal").click(function() {
		var liveUrlP = $(".formPost").find(".liveurl");
		var liveUrlM  = $(".modal").find('.liveurl');
		
		if(liveUrlP.is(":visible")) {
			
			var title = liveUrlP.find(".title").text();
			var descricao = liveUrlP.find(".description").text();
			var url = liveUrlP.find(".url").text();
			var tipoUrl = liveUrlP.find("#tipoUrl").val();
			var image = liveUrlP.find(".image img").attr('src');
			
			liveUrlM.find('.liveTitle').val(title);
			liveUrlM.find('.liveDescricao').val(descricao);
			liveUrlM.find(".tipoUrl").val(tipoUrl);
			liveUrlM.find('.url').text(url);
			liveUrlM.find('.image img').remove();
			if(image != '' && typeof image != 'undefined')
				liveUrlM.find('.image').prepend('<img src="'+image+'" alt="Preview" class="active" />');
			liveUrlM.find('.image').show();
			$("#urlLink").hide();
			liveUrlM.slideDown();
			liveUrlM.parent('.liveurl-wrapper').addClass('in active');
			$("#modalPostarLink").modal("show");
		} else {
			$("#modalPostarLink").modal("show");
			$("#modalPostarLink").find("input").val("");
			liveUrlM.hide();
			liveUrlM.parent('.liveurl-wrapper').removeClass('in active');
			$("#urlLink").show();
			
		}
	});
	
	$('#modalPostarLink').on('shown.bs.modal', function () {
		$(this).find("#urlPostarLink").focus();
	})  
	
	$(".formPost .liveurl").on("click", ".close", function(e){
    	e.preventDefault();
    	e.stopPropagation();
        var liveUrl     = $(this).parent();
        liveUrl.slideUp();
        liveUrl.find('.title').html('');
        liveUrl.find('.description').html('');
        liveUrl.find('.url').html('');
        liveUrl.find('.video').html('').hide();
        liveUrl.find('.image > img').remove();
        liveUrl.find('.controls .prev').addClass('inactive');
        liveUrl.find('.controls .next').addClass('inactive');
        liveUrl.find('.image').hide();
        liveUrl.find("#tipoUrl").val("");
        $("#insertLinkPreview").trigger('clear'); 
        $(".postarLink").removeClass("disabled");
        $('.formPost').find('.liveurl-info').remove();
        curImages = new Array();
    });
	
	$(".formPost .liveurl").on("click", ".remove-thumb", function(e){
		e.preventDefault();
		e.stopPropagation();
		$(this).parent(".image").hide();
	});
	
	
	/*live url images*/
	$('.modal .liveurl').on('click', '.controls .button', function(){
        var liveUrl     = $(this).parents('.liveurl');
        var activeImage = $('img.active', liveUrl.find('.image')).removeClass('active');
        var nextIMG = activeImage.next("img");
        var prevIMG = activeImage.prev("img");
        
        if($(this).hasClass('next')) {
        	$(nextIMG).addClass('active');
        	if($(nextIMG).next('img').length == 0) {
        		$(this).addClass('inactive');
        	}
        	$(this).prev().removeClass('inactive');
        } else {
        	$(prevIMG).addClass('active');
        	
        	if($(prevIMG).prev('img').length == 0) {
        		$(this).addClass('inactive');
        	}
        	$(this).next().removeClass('inactive');
        }
    });
});