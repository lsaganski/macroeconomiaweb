$(document).ready(function(){
	
	versaoReportTable(getFiltro());
	
	var d1 = { versionCodeGrafico: true }
	
	plotarGraficos();
	plotarGraficos(null, d1);
	
	$("#filtrarRel").click(function( e ){
		versaoReportTable(getFiltro());
	});
	
	$(".refresh-cadastro").click(function(){
		versaoReportTable(null);
	});
	
	if($("#filtrarLogin").length > 0){
		var filtro = getFiltro();
		usuarioTokenInput({
			element: "#filtrarLogin",
			url: urlContext+"/ws/usuariosAutoComplete.htm?mode=json&form_name=form",
			hintText: "$messages.get('msg.digitar')",
			noResultsText: "$messages.get('msg.item.notFound.error')",
			searchingText: "$messages.get('carregando.label')",
			placeholder: "Digite os usuários...",
			propertyToSearch: "login",
			tokenLimit: 1,
			prePopulate: usuario
		});
	}
});

var _chart = null;
var usuario = [];
var filtro = null;

function getFiltro() {
	var filtro = {
		usuario : $('#filtrarLogin').val(),
		version: $('#filtrarVersion').val(),
		versionCode: $('#filtrarVersionCode').val(),
		os: $('#filtrarOS').val(),
	};
	return filtro;
}

function versaoReportTable(filtro){
	jsHttp.get(urlContext+"/rest/v1/report/app/versao",{
		data: filtro,
		beforeSend: function(){
			$(".paginaAtual").show();
		},
		success: function (data) {
			var conteudo = "";
			if(data.status != "ERROR"){
				for(var i in data) {
					conteudo += createTable(data[i]);
				}
			} else {
				toastr.error("Problema ao consultar WebService: "+ data.message);
			}
			if( jQuery.fn.DataTable.isDataTable( '.table' ) ){
				$('.table').DataTable().clear().destroy();
			}
			$(".table > tbody").append(conteudo);
			$(".table").DataTable({
				oLanguage :oLanguage,
				columnDefs: [ { orderable: true, targets: '_all'} ]
			});
			$(".paginaAtual").hide();
		},
		error: function(){
			toastr.error("WebService não encontrado");
		}
	});
}

function createTable(campo){
	var rTable = "";

	if(!campo.appVersion)
		campo.appVersion = "&nbsp;";
	if(!campo.appVersionCode)
		campo.appVersionCode = "&nbsp;";
	if(!campo.login)
		campo.login = "&nbsp;";
	
	rTable += '<tr data-id="'+campo.id+'">';
		rTable += "<td><a class='blue sbold' href='"+urlContext+"/usuario.htm?id=" + campo.id + "' >" + campo.nome +"</a></td>";
		rTable += "<td><a class='blue sbold' href='"+urlContext+"/usuario.htm?id=" + campo.id + "' >" + campo.login +"</a></td>";
		rTable += "<td><a class='blue sbold' href='"+urlContext+"/usuario.htm?id=" + campo.id + "' >" + campo.email +"</a></td>";
		rTable += "<td class='text-center'>"+campo.appVersion+"</td>";
		rTable += "<td class='text-center'>"+campo.appVersionCode+"</td>";
	rTable += "</tr>";
	return rTable;
}

function plotarGraficos(grafico, filtro){
	$(window).resize(function() {
		if(_chart){
			_chart.redraw();
			_chart.reflow();
		}
	});
    
	jQuery.ajax({
		type : "get",
		url : urlContext+'/rest/v1/report/app/versao/grafico',
		dataType : "json",
		data : filtro,
		beforeSend : function() {
			$("#graficoVersion, #graficoVersionCode").addClass("spinner-blue");
		},
		success : function(data) {
			if(data.status == "ERROR"){
				toastr.error("Falha ao consultar WebService: " + data.message);
				$("#graficoVersion, #graficoVersionCode").html("<label class='empty clearfix text-center'>Nenhuma versão encontrada</label>").css('height', 'auto');
				return;
			}
			var versoes = new Array();
			var dadosGrafico = new Array();
			var valuesAndroid = new Array();
			var valuesIos = new Array();
			var valuesWeb = new Array();
			
			for (i in data) {
				versoes.push(i);
				var series = data[i];
				var android = false;
				var ios = false;
				
				for (j in series) {
					
					var values = series[j];
					if(values.plataforma == 'ANDROID') {
						valuesAndroid.push(values.quantidade);
						dadosGrafico.ANDROID = valuesAndroid;
						android = true;
					}
					
					if(values.plataforma == 'IOS') {
						valuesIos.push(values.quantidade);
						dadosGrafico.IOS = valuesIos;
						ios = true;
					}
					
				}
				
				if(!android) {
					valuesAndroid.push(0);
				}
				
				if(!ios) {
					valuesIos.push(0);
				}
			}
				
			var series = [];
			
			if (filtro && filtro.versionCodeGrafico) {
				var title = {text : "Version code"};
				var chartRenderTo = 'graficoVersionCode';
				series = [{
					type : 'area',
					name : 'Android',
					data : dadosGrafico.ANDROID,
	            	color: '#A4C639',
				}, {
					type : 'area',
					name : 'iOS',
					data : dadosGrafico.IOS,
	            	color: '#666666',
				}]
			} else {
				var title = {text : "App Version"};
				var chartRenderTo = "graficoVersion";
				series = [{
					type : 'area',
					name : 'Android',
					data : dadosGrafico.ANDROID,
	            	color: '#A4C639',
				}, {
					type : 'area',
					name : 'iOS',
					data : dadosGrafico.IOS,
	            	color: '#666666',
				}]
			}
			
			var options = {
				chart : {
					type : 'area',
					style : {
						fontFamily : 'Open sans'
					},
					renderTo: chartRenderTo,
				},
				title: title,
				exporting: { enabled: false, },
				credits : { enabled : false },
				tooltip : {  valueSuffix : ' Usuário(s)', },
				xAxis: { categories: versoes },
				yAxis : {
					title : {
						text : "Usuários"
					}
				},
				plotOptions : {
					area : {
						cursor : 'pointer',
						point : {
							events : {
								click : function() {
									var buscarAppVersionCode = this.series.chart.title.textStr == "Version code";
									if( !this.selected ){
										if( buscarAppVersionCode ){
											$('#filtrarVersionCode').val(this.category);
										} else {
											$('#filtrarVersion').val(this.category);
										}
										$('#filtrarOS').val(this.series.name.toLowerCase());
									} else {
										if( buscarAppVersionCode ){
											$('#filtrarVersionCode').val("");
										} else {
											$('#filtrarVersion').val("");
										}
										$('#filtrarOS').val("");
									}
									versaoReportTable(getFiltro());
								}
							},
						},
					},
				    series : {
						allowPointSelect : true,
				    	fillOpacity: 0.2
				    }
				},
				series : series
			}
			
			$("#graficoVersion, #graficoVersionCode").removeClass("spinner-blue");
			_chart = new Highcharts.Chart(options);
		},
		error : function(){
			$("#graficoVersion, #graficoVersionCode").removeClass("spinner-blue");
			$("#"+grafico).html("<label class='empty clearfix text-center'>Nenhuma versão encontrada</label>").css('height', 'auto');
			toastr.error("WebService não encontrado");
		}
	});
}