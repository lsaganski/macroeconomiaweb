var jsValidator = (function() {
	var validate = function(obj) {
		jQuery.validator.messages.required = "Campo obrigatório";
		jQuery.validator.messages.email = "E-mail não é um e-mail valido";
		jQuery.validator.messages.number = "Insira apenas números";
		jQuery.validator.messages.minlength = "Tamanho minímo {0} caracteres."
		jQuery.validator.messages.maxlength = "Tamanho máximo {0} caracteres."

		var form = $(obj.form);
		var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        
        jQuery.validator.addMethod("required", function(value, element) {
        	if(jsUtils.isEmpty(value)) {
        		$(element).val(jQuery.trim(value));
        		return false;
        	}
            return jQuery.trim(value);
        }, jQuery.validator.messages.required);
        
		return form.validate({
			ignore: ".ignore",
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
                select_multi: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected"),
                    
                },
                
            },
            rules: obj.rules,
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
            },

            errorPlacement: function (error, element) { // render error placement for each input type
            	if(!$(element).hasClass('ignore')) {
            		
            		if(error.text() == jQuery.validator.messages.uniqueConviteEmail) {
            			jAlert(jQuery.validator.messages.uniqueConviteEmail, "Atenção");
            		}
            		
            		var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");  
                    element.addClass('bs-tooltip');
                    
                    $(element).tooltip({
                	   placement: 'top',
                	   html: true,
                	   title: error.text()
                    });
            	}
            },

            highlight: function (element) { // hightlight error inputs
            	if(!$(element).hasClass('ignore')) {
            		$(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
            		if(obj.useTab != undefined && obj.useTab == false) {
            			return;
            		}
                    tab = form.find('.has-error').first().closest('.tab-pane');
                    if(!tab.hasClass('active')) {
                    	var hasErrors = $('.tab.active').find('.has-error');
                    	if(hasErrors.length == 0) {
                    		$('.tab-pane:not(.subtab)').removeClass('active');
                        	$(".nav-tabs").find('.active').removeClass('active');
                        	$('a[href="#'+tab.attr("id")+'"]').parent().addClass('active');
                        	tab.addClass('active fade in');
                    	}
                    }
            	}
            },
            unhighlight: function (element) { // revert the change done by hightlight
            },
            success: function (label, element) {
            	if(!$(element).hasClass('ignore')) {
            		var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                    $(element).tooltip('destroy');
            	}
            },
            submitHandler: function (form) {
            	$(form).find('input[type="text"]').each(function( index ) {
            		  $(this).val(jQuery.trim($(this).val()));
            	});
                success.show();
                error.hide();
                if(obj.success) {
                	obj.success(form);
                } else {
                	form.submit();
                }
                
            }
        });
	};
	return validate;
}());