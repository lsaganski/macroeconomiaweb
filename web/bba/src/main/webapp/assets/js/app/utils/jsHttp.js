var jsHttp = (function(window, jQuery, jsUtils, tokenInput) {
    if (jsUtils.isEmpty(jQuery))
	throw "jQuery undefined : https://jquery.com/download/";

    var CONTENT_TYPE_JSON = 'application/json';
    var CONTENT_TYPE_FORM = 'application/x-www-form-urlencoded';
    var TYPE_REQUEST = {
	POST : "POST",
	GET : "GET",
	DELETE : "DELETE",
	PUT : "PUT"
    }

    var exec = function(url, properties, type) {
	var self = this;
	if (jsUtils.isEmpty(url))
	    throw "Url nao foi definida";
	properties.type = type;
	if (jsUtils.isEmpty(type) || jsUtils.isEmpty(TYPE_REQUEST[type]))
	    throw "Tipo de request não definida ou invalida " + type;

	
	if(jsUtils.isEmpty(properties) && jsUtils.isNotEmpty(this.properties)) {
	    properties = this.properties;
	}
	
	if (jsUtils.isEmpty(properties))
	    throw "Propriedade nao definida";

	if(!jsUtils.isObject(properties)) {
	    throw "properties não é um json";
	}
	
	if(typeof properties.data != 'undefined'){
		properties.data.wstoken = SESSION.USER_INFO.USER_WSTOKEN;
	}else{
		properties.data = { wstoken: SESSION.USER_INFO.USER_WSTOKEN }
	}
	
	if(PARAMS.NONCE_ON == '1')
		properties.data.nonce_token = SESSION.USER_INFO.USER_ID+Date.now();
	
	
	if (jsUtils.isNotEmpty(properties.success)
		&& !jsUtils.isFunction(properties.success))
	    throw "O parametro success não é uma função"
	if (jsUtils.isNotEmpty(properties.error)
		&& !jsUtils.isFunction(properties.error))
	    throw "O parametro error não é uma função"
	if (jsUtils.isNotEmpty(properties.beforeSend)
		&& !jsUtils.isFunction(properties.beforeSend))
	    throw "O parametro beforeSend não é uma função"

	if (jsUtils.isEmpty(properties.contentType)) {
	    properties.contentType = CONTENT_TYPE_JSON;
	}
	
	if(properties.scroll) {
	    if(jsUtils.isEmpty(properties.data)) {
		properties.data = {};
	    }
	    var page = 0;
	    var max = 50;
	    if(jsUtils.isNotEmpty(properties.data.page)) {
	    	if(properties.data.page < 0) {
	    		page = 0;
	    	} else {
	    		page = properties.data.page;
	    	}
	    }
	    
	    if(jsUtils.isNotEmpty(properties.data.max)) {
	    	max = properties.data.max;
	    }
	    
	    properties.data.page = page;
	    properties.data.max = max;
	}
	
	if (jsUtils.equals(properties.contentType, CONTENT_TYPE_JSON) && properties.type == "POST" || properties.type == "PUT") {
	    properties.data = JSON.stringify(properties.data);
	    properties.dataType = "json";
	} else if(jsUtils.equals(properties.contentType, CONTENT_TYPE_FORM)) {
		if(jsUtils.isEmpty(properties.data)) {
			properties.data ={};
		}
    	properties.data.form_name = 'form';
    	properties.data.mode = 'json';
    }

	properties.url = url;
	properties.type = type;
	self.properties = properties;
	if(properties.scroll) {
	    $(window).bind('scroll', function() {
		if($(window).scrollTop() + $(window).height() == $(document).height()) {
		    properties = self.properties;
		    var data = properties.data;
		    
		    if(!jsUtils.isObject(data) && jsUtils.isString(data)) {
			var data = JSON.parse(data);
		    }
		    if(isNaN(data.page)) {
		    	data.page = 0;
		    }
		    data.page++;
		    if (jsUtils.equals(properties.contentType, CONTENT_TYPE_JSON) && properties.type == "POST" || properties.type == "PUT") {
			properties.data = JSON.stringify(properties.data);
		    } else if(jsUtils.equals(properties.contentType, CONTENT_TYPE_FORM)) {
		    	if(jsUtils.isEmpty(properties.data)) {
					properties.data ={};
				}
		    	properties.data.form_name = 'form';
		    	properties.data.mode = 'json';
		    }
		    self.propeties = properties;
		    var callback = self.propeties.success;
		    var successScroll = function(data) {
			if(jsUtils.isEmpty(data)) {
			    $(window).unbind('scroll');
			} else if(self.propeties.max > data.length) {
			    $(window).unbind('scroll');
			} else {
				$(window).bind('scroll');
			}
			callback(data);
		    }
		    self.properties.success = successScroll;
		    return jQuery.ajax(self.propeties);
		}
	    });
	}
	return jQuery.ajax(properties);
    }

    var post = function(url, properties) {
    	return new exec(url, properties, TYPE_REQUEST['POST']);
    }

    var deletar = function(url, properties) {
    	return new exec(url, properties, TYPE_REQUEST['DELETE']);
    }
    
    var get = function(url, properties) {
	return new exec(url, properties, TYPE_REQUEST['GET']);
    }

    var filtroTokenInput = function(element, url, properties) {
	if (jsUtils.isEmpty(url)) {
	    throw "URL não definida";
	}

	if (jsUtils.isEmpty(element)) {
	    throw "element undefined";
	}

	if ($(element).length == 0) {
	    throw "element " + element + " not found";
	}

	if (jsUtils.isEmpty(properties))
	    throw "Parametros do filtro não definidos";
	if (!jsUtils.isObject(properties))
	    throw "Parametros do filtro não é um objeto";
	if (jsUtils.isEmpty(properties.queryParam))
	    throw "Parametro de busca da query não definido";
	if (jsUtils.isEmpty(properties.propertyToSearch))
	    throw "Parametro de pesquisa não foi definido";
	if (jsUtils.isNotEmpty(properties.onAdd)
		&& !jsUtils.isFunction(properties.onAdd))
	    throw "Ação de onAdd não é uma função";

	if (jsUtils.isEmpty(properties.method))
	    properties.method = "GET";
	if (jsUtils.isEmpty(properties.minChars))
	    properties.minChars = null;
	if (jsUtils.isEmpty(properties.jsonContainer))
	    properties.jsonContainer = null;
	if (jsUtils.isEmpty(properties.hintText))
	    properties.hintText = "Comece a digitar para buscar.";
	if (jsUtils.isEmpty(properties.noResultsText))
	    properties.noResultsText = "Nenhum item encontrado";
	if (jsUtils.isEmpty(properties.searchingText))
	    properties.searchingText = "Carregando...";
	if (jsUtils.isEmpty(properties.preventDuplicates))
	    properties.preventDuplicates = true;
	if (jsUtils.isEmpty(properties.onDelete))
	    properties.onDelete = function(item) {
		$(element).val("");
	    }

	if (jsUtils.isEmpty(properties.onAdd))
	    properties.onAdd = function(item) {
		if (jsUtils.isEmpty(item) || jsUtils.isEmpty(item.id)) {
		    throw "Item: " + item + " está vazio ou nao possui id";
		}
		$(element).val(item.id);
	}

	if (jsUtils.isEmpty(properties.prePopulate)) {
	    properties.prePopulate = [];
	} else if (jsUtils.isObject(properties.prePopulate)) {
	    properties.prePopulate = [ properties.prePopulate ];
	}
	$(element).tokenInput(url, properties);
    }
    
    
    var select2 = function(properties) {
        if(jsUtils.isEmpty(properties.url)) {
            throw "URL não informada";
        }
        
        if(jsUtils.isEmpty(properties.element)) {
            throw "ID, Class ou TAG HTML não informado para a criação do select2";
        }
        
        if(jsUtils.isEmpty(properties.term)) {
            throw "Qual o nome do parametro a ser enviado para o servidor? EX: nome, id, descricao";
        }
        
        if(jsUtils.isEmpty(properties.id)) {
            properties.id = "id";
        }
        
        if(jsUtils.isEmpty(properties.text)) {
            properties.text = "nome";
        }
        
        if(jsUtils.isEmpty(properties.dataType)) {
            properties.dataType = "json";
        }
        
        if(jsUtils.isEmpty(properties.data)) {
            properties.data = function(params) {
                var term = {};
                term[properties.term] = params.term;
                return term;
            }
        }
        
        if(jsUtils.isEmpty(properties.dropdownParent)) {
        	properties.dropdownParent = "body";
        }
        
        if(jsUtils.isEmpty(properties.processResults)) {
            properties.processResults = function (data) {
            	if(data.list && data.list.length > 0) {
            		data = data.list;
            	}
    	       data = jQuery.map(data, function(item) {
                   return { id: item[properties.id], text: item[properties.text], obj: item};
    	        });
    	      	return {
    	           results: data,
    	      	};
            }
        }
        
        if(jsUtils.isEmpty(properties.type)) {
        	properties.type = "GET";
        }
        
        if(jsUtils.isEmpty(properties.cache)) {
            properties.cache = true;
        }
        
        var select2Properties = {
    	  ajax: {
    	    url: properties.url,
    	    dataType: properties.dataType,
    	    data: properties.data,
    	    processResults: properties.processResults,
    	    cache: properties.cache,
    	    type: properties.type,
    	  },
    	  templateSelection: properties.templateSelection,
    	  dropdownParent: $(properties.dropdownParent),
    	  allowClear: true,
    	  placeholder: ''
    	}
        
        $(properties.element).select2(select2Properties);
    }
    
    var jsHttp = {
	post : post,
	get : get,
	delete: deletar,
	select2: select2,
	filtroTokenInput : filtroTokenInput,
	CONTENT_TYPE_FORM: CONTENT_TYPE_FORM,
	CONTENT_TYPE_JSON: CONTENT_TYPE_JSON,
    }

    return jsHttp;

}(window, jQuery, jsUtils));
