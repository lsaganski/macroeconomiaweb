var dateUtils = (function() {
	function format(data) {
		var dataStr = pad(data.getDate()) + "/" + pad((data.getMonth() + 1))
				+ "/" + data.getFullYear();
		return dataStr;
	}

	function getRangeData1W() {
		var data1 = new Date();
		var data2 = new Date();
		data1.setDate(data2.getDate() - 7);

		return {
			start : format(data1),
			end : format(data2),
		};
	}
	
	function getRangeData1M() {
		var data1 = new Date();
		var data2 = new Date();
		data1.setMonth(data2.getMonth() - 1);

		return {
			start : format(data1),
			end : format(data2),
		};
	}
	
	function getRangeToday() {
		var data1 = new Date();
		var data2 = new Date();
		return {
			start : format(data1),
			end : format(data2),
		};
	}

	function pad(str) {
		var max = 2;
		str = str.toString();
		return str.length < max ? pad("0" + str, max) : str;
	}
	
	return {
		"D1W": getRangeData1W,
		"D1M": getRangeData1M,
		"TODAY": getRangeToday,
		getRangeData1W: getRangeData1W,
		getRangeData1M: getRangeData1M,
		getRangeToday: getRangeToday,
		format: format,
	}
	

}());