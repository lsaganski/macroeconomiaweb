/**
 * @Author: Juillian Lee
 * 
 * Funções utilitarias para javascrit
 * 
 */
var jsUtils = (function(){
	var LINK_REGEX = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
	var IMAGE_REGEX = /\.(?:jpe?g|gif|png)$/i;
	
    /**
    * NOTE: retorna o tipo do object em forma de string
    * Alguns tipos: [object String], [object Number], [object Null], etc...
    */
    var prototypeToString = function(obj) {
        return Object.prototype.toString.call(obj);
    }
    //NOTE: Verifica se é uma string
    var isString = function(obj) {
        return this.prototypeToString(obj) == '[object String]';
    }

    //NOTE: Verifica se é um numero
    var isNumber = function(obj) {
        return this.prototypeToString(obj) == '[object Number]';
    }

    //NOTE: Verifica se esta nullo
    var isNull = function(obj) {
        return this.prototypeToString(obj) == '[object Null]';
    }

    //NOTE: Verifica se é uma funcao
    var isFunction = function(obj) {
        return this.prototypeToString(obj) == '[object Function]';
    }

    //NOTE: Verifica se é undefined
    var isUndefined = function(obj) {
        return this.prototypeToString(obj) == '[object Undefined]';
    }

    //NOTE: Verifica se é um object
    var isObject = function(obj) {
        return this.prototypeToString(obj) == '[object Object]' ? true : false;
    }

    //NOTE: Verifica se é um array
    var isArray = function(obj) {
        return this.prototypeToString(obj) == '[object Array]' ? true : false;
    }

    var isNotArray = function(obj) {
        return !this.isArray(obj);
    }

    //NOTE: Verifica se o array está vazio
    var isArrayEmpty = function(obj) {
	if(!obj) {
	    return true;
	}
        if(this.isArray(obj) && obj.length == 0) {
            return true;
        }
        return false;
    }

    //NOTE: Verifica se é não esta vazio
    var isArrayNotEmpty = function(obj) {
        return !this.isArrayEmpty(obj);
    }

    //NOTE: Verifica se obj esta vazio
    var isEmpty = function(obj) {
        if(this.isUndefined(obj) 
        		|| this.isNull(obj) 
        		|| (this.isString(obj) && this.trim(obj) == "") 
        		|| (this.isArray(obj) && this.isArrayEmpty(obj))) {
        	return true;
        }
        return false;
    }

    //NOTE: Verifica se obj não esta vazio
    var isNotEmpty = function(obj) {
        return !this.isEmpty(obj);
    }

    //NOTE: Verifica remove espaço do inicio e do fim
    var trim = function(obj) {
        if(this.isString(obj))
            return obj.trim();
        return obj;
    }

    //NOTE: Verifica se string contain uma palavra
    var contains = function(string, search) {
        if(this.isEmpty(search)) throw "search not defined";
        if(this.isString(string) && string.indexOf(search) > -1)
            return true;
        return false;
    }

    //NOTE: verifica se duas variaveis sao iguais
    var equals = function(obj1, obj2) {
        if(obj1 == obj2) {
            return true;
        }
        return false;
    }
    
    
    var isImage = function(string) {
    	if(this.isEmpty(string) || !this.isString(string)){
    		return false;
    	}
    	if(IMAGE_REGEX.test(string)) {
    		return true;
    	}
    	return false;
    }
    
    var isLink = function(string) {
    	if(this.isEmpty(string) || !this.isString(string)){
    		return false;
    	}
    	if(LINK_REGEX.test(string)) {
    		return true;
    	}
    	return false;
    }
    
    var replaceLinkStringToActiveLink = function(string) {
    	if(this.isEmpty(string) || !this.isString(string)){
    		return string;
    	}
    	return string.replace(LINK_REGEX,function(match){
    			return '<a href="'+match+'">'+match+'</a>';
            }
        );
    }
    
    replaceLinksImageToImage = function(string) {
    	if(this.isEmpty(string) || !this.isString(string)){
    		return string;
    	}
    	return string.replace(LINK_REGEX,function(match){
	    		if(IMAGE_REGEX.test(match)) {
	    			return '<img src="'+match+'" />';
	    		}
            }
        );
    }
    
    var count = function(obj) {
    	if(this.isString(obj) || this.isArray(obj)) {
    		return obj.length;
    	}
    	return null;
    }

    var jsUtils = {
        prototypeToString: prototypeToString,
        isString: isString,
        isNumber: isNumber,
        isNull: isNull,
        isFunction: isFunction,
        isUndefined: isUndefined,
        isObject: isObject,
        isArray: isArray,
        isNotArray: isNotArray,
        isArrayEmpty: isArrayEmpty,
        isArrayNotEmpty: isArrayNotEmpty,
        isEmpty: isEmpty,
        isNotEmpty: isNotEmpty,
        trim: trim,
        equals:equals,
        count: count,
        isImage: isImage,
        replaceLinkStringToActiveLink: replaceLinkStringToActiveLink,
        replaceLinksImageToImage: replaceLinksImageToImage
    }

    return jsUtils;
}());
