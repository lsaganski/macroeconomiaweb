var webpack = require('webpack');

module.exports = {
  entry: './src/js/index.jsx',
  output: {
    path: './build',
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
  },
  devServer: {
    inline: true,
    contentBase: './build',
    port: 3333
  },
  //devtool: 'nosources-source-map',
  devtool: 'source-map',
  module: {
    loaders: [
      {
        loader: "babel-loader",
        test: /\.jsx$/,
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    // new webpack.DefinePlugin({
    //   'process.env': {
    //     NODE_ENV: JSON.stringify('production')
    //   }
    // }),
    // new webpack.optimize.OccurenceOrderPlugin(),
    // new webpack.optimize.DedupePlugin(),
    // new webpack.optimize.UglifyJsPlugin({
    //   compress: { warnings: false },
    //   comments: false,
    //   sourceMap: true,
    //   mangle: true,
    //   minimize: true
    // }),
  ]
};