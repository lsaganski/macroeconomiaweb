var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass:watch', function () {
	  //gulp.watch('./src/sass/**/*.scss', ['sass']); para subdiretorios
	gulp.watch('./src/sass/**/*.scss', ['sass']);
});


gulp.task('sass', function () {
	  return gulp.src('./src/sass/**/*.scss')
	   .pipe(sass().on('error', sass.logError))
	   .pipe(gulp.dest('./css'));
});


gulp.task('default', ["sass:watch", "sass"]);
