import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import ListInfinite from '../../components/lists/ListInfinite';
import Avatar from '../../components/Avatar';

const DragCover = ({foto, nome, data}) => {
	return(
		<div className="chat-drag-cover">
			<div className="chat">
				<Avatar src={foto} />
				<div className="chat-body">
					<div className="chat-main">
						<div className="chat-title">
							<span className="text-title">{nome}</span>
						</div>
					</div>
					<div className="chat-secondary">
						<div className="chat-status">
							<span className="emojitext ellipsify" dir="ltr">
								{data}
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

const Section = ({label, quantidade, children}) => {
	return(
		<div className="animate-enter2 drawer-section well">
			<div className="drawer-section-title title-underlined">
				<div className="col-main">{label}</div>
				<div className="col-side">{quantidade}</div>
			</div>
			<div className="drawer-section-body">
				<span className="textfield-static selectable-text" dir="auto">
					<ListInfinite>
						{children}
					</ListInfinite>
				</span>
			</div>
		</div>
	)
}

export default class InfoMessageGrupo extends Component {
	render() {
		let qtdLida = 0;
		let qtdEntregue = 0;
		let qtdRestante = 0;
		const {statusUsersMessage, message} = this.props;
		let nodeLidas = statusUsersMessage.map((user, index) => {
			if(user.get('lida')) {
				qtdLida += 1;
				return <DragCover key={user.get('userId')} foto={user.get('userUrlFoto')} nome={user.get('userNome')} data={user.get('dataLida')} />
			}
			return "";
		});

		let nodeEntregues = statusUsersMessage.map((user, index) => {
			if(!user.get('lida') && user.get('entregue')) {
				qtdEntregue += 1;
				return <DragCover key={user.get('userId')} foto={user.get('userUrlFoto')} nome={user.get('userNome')} data={user.get('dataEntregue')} />
			}
			return "";
		});

		let nodePendentes = statusUsersMessage.map((user, index) => {
			if(!user.get('lida') && !user.get('entregue')) {
				qtdRestante += 1;
				return <DragCover key={user.get('userId')} foto={user.get('userUrlFoto')} nome={user.get('userNome')} data="" />
			}
			return "";
		});
		return (
			<div className="drawer-body drawer-editable">
				{ qtdLida > 0 ? <Section label="Lida por" quantidade={qtdLida}>{nodeLidas}</Section> : null }
				{ qtdEntregue > 0 ? <Section label="Entregue a" quantidade={qtdEntregue}>{nodeEntregues}</Section> : null }
				{ qtdRestante > 0 ? <Section label="Não recebidas" quantidade={qtdRestante}>{nodePendentes}</Section> : <div></div> }
				{
					message.get('dataPush')
					?
						<div className="animate-enter2 drawer-section well">
							<div className="drawer-section-title title-underlined">
								<div className="col-main"><span className="icon icon-msg-check"></span> Push</div>
							</div>
							<div className="drawer-section-body">
								<span className="textfield-static selectable-text" dir="auto">
									Push enviado às {message.get('dataPush')}
								</span>
							</div>
						</div>
					: null
				}
			</div>
		);
	}
}