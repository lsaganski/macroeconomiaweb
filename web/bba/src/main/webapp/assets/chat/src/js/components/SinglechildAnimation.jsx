import React, {Component} from 'react';
import ReactDOM from 'react-dom';


export default class SinglechildAnimation extends Component {
	render() {
		var children = React.Children.toArray(this.props.children);
  	  	return (
			children[0] || null
		);
	}
}
