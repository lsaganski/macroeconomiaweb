import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {observer, inject} from 'mobx-react';

import ListInfiniteItem from '../../components/lists/ListInfiniteItem';
import Avatar from '../../components/Avatar';
import Badge from '../../components/Badge';
import Mute from '../../components/Mute';
import CopyToClipBoard from '../../components/menu-item/CopyToClipBoard';
import Typing from '../../components/Typing';
import LastMessage from './LastMessage';

import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';


const btnContext = {
    backgroundSize: 'contain',
    width: '20px',
    opacity: '1'
}

const ContextMenu = ({show, open, onOpen, onClose, anchorEl, children}) => {
    if(!show) {
        return <span></span>
    } 
    return(
        <span className="icon icon-down btn-context" onClick={onOpen} style={btnContext}>
            <Popover
                open={open}
                anchorEl={anchorEl}
                anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                targetOrigin={{horizontal: 'left', vertical: 'top'}}
                onRequestClose={onClose}
            >
                {children}
            </Popover>
        </span>
    )
}

@inject('chatStore', 'userInfoStore')
@observer export default class Conversa extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openPopoverUser: false,
            contextMenu: false,
        };
    }
    handleTouchTap(event) {
       event.preventDefault();
       this.setState({
         openPopoverUser: true,
         anchorEl: event.currentTarget,
       });
    }
    handleRequestClose() {
        this.setState({
         openPopoverUser: false,
         contextMenu:false,
       });
    }
    showContextMenu() {
        this.setState({
             contextMenu: true
         });
    }
    hideContextMenu() {
        if(this.state.openPopoverUser == false) {
              this.setState({
                  contextMenu: false
              });
          }
    }
    onClickConversa(event){
        if($(event.target).closest('.btn-context').length == 0) {
            this.props.chatStore.fetchConversa(this.props.conversa);
        }
	}
    onClickShowNotifications() {
        this.props.chatStore.onClickShowNotifications(this.props.conversa);
    }
    render() {
        const conversa = this.props.conversa;
        const chatStore = this.props.chatStore;
        const user = chatStore.getUser(conversa);
        let classActive = this.state.contextMenu ? 'chat hover' :  'chat';
        classActive = chatStore.isConversaActive(conversa) ? 'chat active' : 'chat';
        let badges = conversa.get('badge') ? conversa.get('badge').get('mensagens') : 0;
        let showIconNotificationMute = !this.props.chatStore.notificationActive(conversa) || !this.props.chatStore.notificationSoundActive(conversa);
        return(
            <ListInfiniteItem
                onMouseOver={this.showContextMenu.bind(this)}
                onClick={this.onClickConversa.bind(this)}
                onMouseLeave={this.hideContextMenu.bind(this)}>
                <div className="chat-drag-cover">
                    <div className={classActive}>
                        <Avatar away={user.get('away')} online={user.get('online')} src={user.get('urlFoto')} />
                        <div className="chat-body">
                            <div className="chat-main">
                                <div className="chat-title">
                                    <span className="text-title">{user.get('nome')}</span>
                                </div>
                                <div className="chat-meta">
                                    <span className="chat-time">{chatStore.getDataUltimaMensagem(conversa)}</span>
                                </div>
                            </div>
                            <div className="chat-secondary">
                                {
                                    conversa.get('isTyping')
                                    ?
                                        <Typing label={chatStore.getLabelTyping(conversa)} />
                                    :
                                        <LastMessage conversa={conversa} />
                                }
                                
                                <div className="chat-meta">
                                    {showIconNotificationMute ? <Mute onClick={this.onClickShowNotifications.bind(this)}/> : null}
                                    <Badge badge={chatStore.getBadgeConversa(conversa)} />
                                    <ContextMenu
                                        show={this.state.contextMenu}
                                        open={this.state.openPopoverUser}
                                        anchorEl={this.state.anchorEl}
                                        onOpen={this.handleTouchTap.bind(this)}
                                        onClose={this.handleRequestClose.bind(this)}
                                    >
                                        <Menu>
                                            <CopyToClipBoard 
                                                valueToCopy={chatStore.getLinkConversa(conversa)}
                                                label="Copy Link"
                                                onCopy={() => {
                                                    chatStore.onShowSnackBar(`Link ${chatStore.getLinkConversa(conversa)} copiado`);
                                                }}
                                            />
                                        </Menu>
                                    </ContextMenu>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ListInfiniteItem>
        );
    }
}

