function pad(str) {
	var max = 2;
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

export function newDate(timestamp) {
  let date = new Date(timestamp);
  let hora = date.getHours();
  let min     = date.getMinutes();
  return hora +":"+ pad(min);
}
