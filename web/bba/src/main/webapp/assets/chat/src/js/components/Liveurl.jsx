import React, {Component} from 'react';

export default class Liveurl extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showImg: true,
        }
    }
    onErroImg() {
        let showImg = false;
        this.setState({showImg});
    }
    onLoadImg() {
        let showImg = true;
        this.setState({showImg});
    }
    render(){
        return(
            <div className="liveurl">
                <button className="close icones ico-excluir" onClick={this.props.onClose}></button>
                <div className="liveurl-first">
                    {
                        this.state.showImg
                        ?
                            <img 
                                onError={this.onErroImg.bind(this)}
                                onLoad={this.onLoadImg.bind(this)} 
                                className="imagem"
                                src={this.props.img}
                            />
                        :
                        null
                    }
                    
        	        <h2 className="titulo">{this.props.title}</h2>
                    <p className="descricao">{this.props.description}</p>
                </div>
            </div>
        );
    }
}