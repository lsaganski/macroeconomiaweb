import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class ListTitle extends Component {
    render() {
        return (
            <span key={Math.random()}>
                <div className="chat-drag-cover" key={Math.random()}>
                    <div className="list-title" key={Math.random()}>{this.props.title}</div>
                </div>
                {this.props.children}
            </span>
        );
    }
}