import React, {Component} from 'react';
import ReactDOM from 'react-dom';


export default class Container extends Component {
    render() {
        return(
            <div className="drawer-manager">
	            {this.props.children}
	        </div>
        );
    }
}