import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class Typing extends Component {
    render() {
        return(
            <div className="chat-status typing ellipsify">{this.props.label}</div>
        )
    }
}