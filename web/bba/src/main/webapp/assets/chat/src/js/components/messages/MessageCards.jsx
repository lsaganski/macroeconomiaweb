import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {blue500} from 'material-ui/styles/colors';
import ArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import ArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right';

const CardButton = ({button, onClickLinkPerfil}) => {
	let btn = null;

	let action = button.get('action') ? button.get('action') : '';
	let label = button.get('label') ? button.get('label') : '';
	let value = button.get('value') ? button.get('value') : '';
	switch (action) {
		case 'PERFIL_USER':
			btn = <a href={`${SERVER_URL}/usuario.htm?id=${value}`} onClick={onClickLinkPerfil} target="_blank" className='card-button link'>{label}</a>
			break;
		default:
			<div></div>
			break;
	}
	return btn;
}

const CreateCard = ({card, onClickLinkPerfil}) => {
	let nodeCardButtons = null;
	let title = card.get('title') ? card.get('title') : '';
    let subTitle = card.get('subTitle') ? card.get('subTitle') : '';
    let imageUrl = card.get('imageUrl') ? card.get('imageUrl') : '';
    let buttons = card.get('buttons') ? card.get('buttons') : '';

	if(buttons != null && buttons.size > 0) {
        nodeCardButtons = buttons.map((button) => {
        	return (
            	<CardButton button={button} key={button.get('id')} onClickLinkPerfil={onClickLinkPerfil} />
        	);
        });
    }

    return (
        <li className="card">
			<div className="card-header">
				<a className="card-fancybox" href={imageUrl}>
					<img className="card-image" src={imageUrl} alt={title} />
				</a>
			</div>
			<div className="card-footer">
				<h3 className="card-title">{title}</h3>
				{
					subTitle && subTitle != ''
					?
						<p className="card-subtitle">{subTitle}</p>
					:
						null
				}
				{
					nodeCardButtons && nodeCardButtons != null
					?
						<div className="card-buttons">
							{ nodeCardButtons }
						</div>
					:
						null
				}
			</div>
		</li>
    )
}

export default class MessageCards extends Component {
	componentDidMount() {
        $('.card-fancybox').fancybox({
        	type: 'image',
			openEffect: 'elastic',
			closeEffect: 'elastic',
			maxHeight: '90%',
			loop: false,
			helpers: {
	            overlay: {
	                closeClick: true,
	                locked: false
	            }
	        }
		});

		let list = ReactDOM.findDOMNode(this.refs.cardList);
		list.addEventListener('scroll', this.manageCarouselBtn.bind(this));
    }
    manageCarouselBtn(e){
    	var tgt = $(e.target).parent();
    	tgt.find('.arrow-back').addClass('active');
    }
    manageCarousel(e){
    	let target = $(e.target).parent();
    	
    	let cardList = ReactDOM.findDOMNode(this.refs.cardList);
    	let arrowBack = ReactDOM.findDOMNode(this.refs.arrowBack);
    	let arrowFront = ReactDOM.findDOMNode(this.refs.arrowFront);

    	let scroll = $(cardList).scrollLeft();
    	let newScroll = 0;
    	if($(target).hasClass('arrow-front')){
    		$(arrowBack).addClass('active');
    		newScroll = scroll + 250;
    	}else{
    		$(arrowFront).addClass('active');
    		newScroll = scroll - 250;
    	}
    	
    	$(cardList).animate({
    		scrollLeft: newScroll
    	}, 800, 'easeOutExpo');
    }
    onClickLinkPerfilPerfil(e){
    	e.preventDefault();
    	let target = $(e.target);
    	window.open(target.attr('href'), "lvcm-h");
    }
	render(){
		const {cards} = this.props;
        let nodeCards = null;
        let classCarousel = '';

        if(cards != null && cards.size > 0) {
            nodeCards = cards.map((card) => {
				let key = `${card.get('id')}_${Date.now()}`;
                return (
                	<CreateCard card={card} key={key} onClickLinkPerfil={this.onClickLinkPerfilPerfil.bind(this)} />
            	);
            });

            if(cards.size > 3){
            	classCarousel = 'border';
            }
        }

		return(
			<div className={`card-carousel ${classCarousel}`}>
				<a className="card-carousel-arrows arrow-back" onClick={this.manageCarousel.bind(this)} ref="arrowBack">
					<ArrowLeft color={blue500} />
				</a>
				<ul className="card-list" ref="cardList">
					{ nodeCards }
				</ul>
				<a className="card-carousel-arrows arrow-front active" onClick={this.manageCarousel.bind(this)} ref="arrowFront">
					<ArrowRight color={blue500} />
				</a>
			</div>
		);
	}
}