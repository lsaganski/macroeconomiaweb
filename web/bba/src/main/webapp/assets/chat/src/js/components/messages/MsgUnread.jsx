import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class MsgUnread extends Component {
    render() {
        return (
            <div className="msg-unread">
                <span className="msg-unread-body">
                    <span className="hidden-token">{this.props.total} Mensagem não lida</span>
                </span>
            </div>
        )
    }
}