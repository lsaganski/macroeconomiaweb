const __urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;

/**
 * Retorna uma string com href nos links do texto
 */
export function parseURL(string) {
    if(!string) {
		return "";
	}
    
    return string.replace(__urlRegex,function(match){
            return '<a href="'+match+'" target="_blank">'+match+'</a>';
        }
    );
}

export function matchURL(string){
    if(!string) {
        return "";
    }

    return string.match(__urlRegex);
}

/**
 * Substitui todos os caracteres do search da string para um novo valor definido
 */
export function replaceAll(string, searchValue, newValue) {
    if(!string) {
        return "";
    }
    let regExp = new RegExp(searchValue, 'g');
    return string.replace(regExp, string, newValue);
}

export function pad(str) {
	var max = 2;
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

export function getURLParameter(param) {
    return decodeURIComponent((new RegExp('[?|&]' + param + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}