import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {grey700, blue500} from 'material-ui/styles/colors';
import MenuItem from 'material-ui/MenuItem';
import AssessmentIcon from 'material-ui/svg-icons/action/assessment';

export default class Roadmap extends Component {
	render() {
		return (
			<MenuItem 
				primaryText="Roadmap Chat" 
				leftIcon={<AssessmentIcon color={blue500} />} 
				value="1" onTouchTap={this.props.onClick} 
			/>
		);
	}
}