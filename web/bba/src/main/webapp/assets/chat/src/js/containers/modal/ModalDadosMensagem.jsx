import React, {Component} from 'react';
import Dialog from 'material-ui/Dialog';
import {observer, inject} from 'mobx-react';

import Message from '../center/Message';
import InfoMessageUsuario from './InfoMessageUsuario';
import InfoMessageGrupo from './InfoMessageGrupo';

@inject('infoMessageStore', 'userInfoStore')
@observer export default class ModalDadosMensagem extends Component {
    onCloseModal = () => {
        this.props.infoMessageStore.onCloseDrawerInfoMessage();
    }
    render() {
        const isGroup = this.props.infoMessageStore.isGroup;
        return(
            <Dialog
                title="Dados da Mensagem"
                modal={false}
                open={true}
                onRequestClose={this.onCloseModal}
                autoScrollBodyContent={true}
            >
                <div className="pane-preview-wrapper pane-preview-wrapper-bg">
                    <div className="pane-preview">
                        <div className="pane-chat-tile"></div>
                        <Message contextMenu={false} message={this.props.infoMessageStore.message} />
                    </div>
                </div>
                {
                    isGroup
                        ? <InfoMessageGrupo statusUsersMessage={this.props.infoMessageStore.statusUsersMessage} message={this.props.infoMessageStore.message} />
                        : <InfoMessageUsuario message={this.props.infoMessageStore.message} />
                }
            </Dialog>
        );
    }
}