import React, {Component} from 'react';
import {observer, inject} from 'mobx-react';

import Spinner from '../../components/SpinnerMessage';

import Search from '../../components/Search';
import ListInfinite from '../../components/lists/ListInfinite';
import EmptyTop from '../../components/EmptyTop';
import ListTitle from '../../components/lists/ListTitle';
import Contato from '../../components/Contato';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/RaisedButton';


const NotFoundContato = () => <EmptyTop>Nenhum contato encontrado</EmptyTop>

const FetchContatos = ({show}) => <EmptyTop>Carregando seus contatos, aguarde...<Spinner show={show} title="Carregar contatos" /></EmptyTop>

const ListaUm = ({nodeUsuarios, nodeGrupos}) => {
    return nodeUsuarios.size == 0 && nodeGrupos.size == 0 ? <NotFoundContato /> : null;
}

const ListaDois = ({nodeUsuarios}) => {
    return nodeUsuarios.size > 0 ? <ListTitle title="Usuários">{nodeUsuarios}</ListTitle> : null;
}

const ListaTres = ({nodeGrupos}) => {
    return nodeGrupos.size > 0 ? <ListTitle title="Grupos">{nodeGrupos}</ListTitle> : null;
}

@inject('contatoStore', 'chatStore')
@observer export default class ModalContatos extends Component {
    handleClose = () => {
        this.props.chatStore.messageForward = null;
        this.props.contatoStore.openDrawer = false;
    };
    componentDidMount() {
        this.props.contatoStore.fetchContatos();
    }
    onClickContato(contato, isGrupo) {
        this.props.onClickContato(contato, isGrupo);
    }
    onSearch(valueSearch) {
        this.props.contatoStore.fetchContatos(valueSearch);
    }
    render() {
        const {chatStore} = this.props;
        let openModal = chatStore.messageForward != null;
        const isFetchContatos = this.props.contatoStore.isFetchContatos;

        const usuarios = this.props.contatoStore.usuarios;
        const grupos = this.props.contatoStore.grupos;

        let nodeUsuarios = usuarios.map((contato) => {
            return <Contato avatar={contato.get('urlFotoUsuario')} onClick={this.onClickContato.bind(this)} contato={contato} key={contato.get('id')} isGroup={false} />
        });

        let nodeGrupos = grupos.map((contato) => {
            return <Contato avatar={contato.get('urlFotoUsuario')} onClick={this.onClickContato.bind(this)} contato={contato} key={`${contato.get('id')}_${Math.random()}`} isGroup={true} />
        });

        const actions = [
            <FlatButton
                label="Cancelar"
                onTouchTap={this.handleClose}
            />
        ];
        
        return(
            <div>
                <Dialog
                    title={this.props.title}
                    actions={actions}
                    modal={false}
                    open={true}
                    onRequestClose={this.handleClose}
                    autoScrollBodyContent={true}
                    titleClassName="dialog-title"
                    actionsContainerClassName="dialog-title"
                >
                    <Search
                        spinner={this.props.contatoStore.isSpinnerSearch}
                        onSearch={this.onSearch.bind(this)}
                        placeholder="Buscar contatos"
                        clearSearch={this.props.contatoStore.clearSearch}
                        focus={this.props.contatoStore.searchInputFocus}
                    />
                    <div className="drawer-body">
                        <ListInfinite>
                            {isFetchContatos
                                ?
                                    <FetchContatos show={isFetchContatos} />
                                :
                                    <div>
                                        <ListaUm nodeUsuarios={nodeUsuarios} nodeGrupos={nodeGrupos} />
                                        <ListaDois nodeUsuarios={nodeUsuarios} />
                                        <ListaTres nodeGrupos={nodeGrupos} />
                                    </div>
                            }
                        </ListInfinite>
                    </div>
                </Dialog>
            </div>
        )
    }
}