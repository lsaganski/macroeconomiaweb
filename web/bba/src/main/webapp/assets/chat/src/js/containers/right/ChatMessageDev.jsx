import React, {Component} from 'react';
import {observer, inject} from 'mobx-react';

import TextInput from '../../components/TextInput';
import BtnSend from '../../components/BtnSend';

const DrawerSectionTitle = ({label, icone}) => {
    return (
        <div className="drawer-section-title  title-underlined">
            <div className="col-main">
                {label}
                <i className={`icon-session ${icone}`}></i>
            </div>
        </div>
    )
}

const DrawerSectionBody = ({label}) => {
    return(
        <div className="drawer-section-body">
        <span className="textfield-static selectable-text" dir="auto">
        {label}
        </span>
        </div>
    )
}


const FooterMessage = ({onChange, sendMessage, onClick}) => {
    return(
        <footer className="pane-footer pane-chat-footer" style={{position: "absolute", bottom: 0, background: "#0096d0"}}>
            <div className="block-compose">
                <div className={`input-container`}>
                    <TextInput onChange={onChange} sendMessage={sendMessage} />
                </div>
            </div>
        </footer>
    )
}

@inject('chatStore')
@observer export default class ChatMessageDev extends Component{
    constructor(props) {
        super(props);

        this.state = {
            message: "",
            messages: []
        };
    }
    onChange(message) {
        console.log(message);
        this.setState({message: message});
    }
    sendMessage() {
        let messages = this.state.messages;
        let message = this.state.message;
        if(!message || message == "") {
            return;
        }
        try {
            let jsonString = JSON.stringify(message);
            let json = eval(`(${message})`);
            messages.push(jsonString);
            this.setState({messages, message: ""});
            this.props.chatStore.sendMessageCustom(this.props.chatStore.conversa, json);
        } catch(e) {

            try {
                let jsonString = JSON.stringify(message);
                let json = JSON.parse(`(${message})`);
                messages.push(jsonString);
                this.setState({messages, message: ""});
                this.props.chatStore.sendMessageCustom(this.props.chatStore.conversa, json);
            } catch(e) {
                messages.push(`JSON Invalido ${e.message}`);
            }
        }
        this.setState({messages, message: ""});
    }
    onClick(e) {
        console.log(this.state.message);
    }
    onClickClose(e) {
        if(typeof e != 'undefined'){
            e.preventDefault();
        }
        this.props.chatStore.closeDevChatMessage();
    }
    render() {
        let nodeMessages = this.state.messages.map((m) => {
            return (
                <li key={Math.random()}>{m}</li>
            )
        });
        return(
            <div className="drawer drawer-info">
                <header className="drawer-header" style={{height: "auto"}}>
                    <div className="pane-header drawer-title" style={{padding: '0px', opacity: '1', transform: 'translateX(0px)', backgroundColor: 'transparent'}}>
                        <div className="drawer-title-action" style={{textAlign: 'initial', width: '54px'}}>
                            <span onClick={this.onClickClose.bind(this)} className="icon btn-close-drawer-left icon-back-light"></span>
                        </div>
                        <span className="drawer-title-body" style={{fontWeight: '500', fontSize: '19px', lineHeight: 'normal'}}>Dev Messages</span>
                    </div>
                </header>
                <div>
                    <ul>
                        {nodeMessages}
                    </ul>
                </div>
                <FooterMessage onChange={this.onChange.bind(this)} sendMessage={this.sendMessage.bind(this)} onClick={this.onClick.bind(this)} />
            </div>
        )
    }
}