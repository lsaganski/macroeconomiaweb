import React, {Component} from 'react';
import {observer, inject} from 'mobx-react';


const DrawerSectionTitle = ({label, icone}) => {
    return (
        <div className="drawer-section-title  title-underlined">
            <div className="col-main">
                {label}
                <i className={`icon-session ${icone}`}></i>
            </div>
        </div>
    )
}

const DrawerSectionBody = ({label}) => {
    return(
        <div className="drawer-section-body">
			<span className="textfield-static selectable-text" dir="auto">
				{label}
			</span>
        </div>
    )
}


@inject('infoContatoStore', 'chatStore')
@observer export default class ChatStatusUser extends Component{
    onClickClose(e) {
        if(typeof e != 'undefined'){
            e.preventDefault();
        }
        this.props.infoContatoStore.closeChatStatusUser();
    }
    render() {
        const infoChatStatusUser = this.props.infoContatoStore.infoChatStatusUser;
        let nodeSessions = null;
        if(infoChatStatusUser != null && infoChatStatusUser.get('sessions') && infoChatStatusUser.get('sessions').size > 0) {
            nodeSessions = infoChatStatusUser.get('sessions').map((session) => {
                let actorRef = session.get('actorRef');
                if(actorRef && actorRef != null && actorRef != ''){
                    let tipoAcesso = session.get('tipo')[0].toUpperCase() + session.get('tipo').slice(1);
                    let icon = session.get('away') ? 'away' : 'online';
                    icon = infoChatStatusUser.get('chatOn') ? icon : 'offline';
                    let dataLogin = session.get('dataLogin') ? session.get('dataLogin') : "--";
                    let dataUltTransacao = session.get('dataUltTransacao') ? session.get('dataUltTransacao') : "--";
                    let dataLoginChat = session.get('dataLoginChat') ? session.get('dataLoginChat') : "--";
                    let dataUltChat = session.get('dataUltChat') ? session.get('dataUltChat') : "--";
                    return(
                        <div className="animate-enter2 drawer-section well" key={`${Math.random()}_${session.get('tipo')}`}>
                            <DrawerSectionTitle label={tipoAcesso} icone={icon} />
                            <DrawerSectionBody label={`Data Login: ${dataLogin}`} />
                            <DrawerSectionBody label={`Data Últ. Acesso: ${dataUltTransacao}`} />
                            <DrawerSectionBody label={`Data Login Chat: ${dataLoginChat}`} />
                            <DrawerSectionBody label={`Data Últ. Chat: ${dataUltChat}`} />
                        </div>
                    )
                }

                return false;
            });
        }
        return(
            <div className="drawer drawer-info">
                <header className="drawer-header" style={{height: "auto"}}>
                    <div className="pane-header drawer-title" style={{padding: '0px', opacity: '1', transform: 'translateX(0px)', backgroundColor: 'transparent'}}>
                        <div className="drawer-title-action" style={{textAlign: 'initial', width: '54px'}}>
                            <span onClick={this.onClickClose.bind(this)} className="icon btn-close-drawer-left icon-back-light"></span>
                        </div>
                        <span className="drawer-title-body" style={{fontWeight: '500', fontSize: '19px', lineHeight: 'normal'}}>Informações de Acesso</span>
                    </div>
                </header>
                {
                    infoChatStatusUser != null && infoChatStatusUser.get('chatOn') != false
                    ?
                        <div className="drawer-body drawer-editable">
                            {nodeSessions}
                        </div>
                    :
                        <div className="drawer-body drawer-editable">
                            <div className="animate-enter2 drawer-section well">
                                <DrawerSectionTitle label="Dados de Acesso" icone="offline" />
                                <DrawerSectionBody label={`Status: Offline`} />
                            </div>
                        </div>
                }
            </div>
        )
    }
}