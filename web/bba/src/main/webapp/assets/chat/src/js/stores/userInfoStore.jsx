import {observable, action, computed, inject} from 'mobx';
let debug = require('debug')('chat:UserInfoStore');
import immutable from 'immutable';


class UserInfoStore {
    @observable userInfo = null;

    @action setUserInfo(user) {
        this.userInfo = immutable.fromJS(user);
    }
}

const userInfoStore = new UserInfoStore();
export default userInfoStore;
export {UserInfoStore};