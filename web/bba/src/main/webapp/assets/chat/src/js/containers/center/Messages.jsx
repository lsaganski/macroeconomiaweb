import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {observer, inject} from 'mobx-react';

import SpinnerMessage from '../../components/SpinnerMessage';
import Message from './Message';
import MessageSystem from '../../components/messages/MessageSystem';
import MsgUnread from '../../components/messages/MsgUnread';

import IncomingMsgs from '../../components/IncomingMsgs';


const isContinuation = (mensagens, mensagem, index) => {
    let nextMessage = mensagens.get(index + 1);
    if(nextMessage && nextMessage.size > 0) {
        nextMessage = mensagens.get(index + 1);
        return nextMessage && nextMessage.get('fromId') == mensagem.get('fromId');
    }
    return false;
    
}

@inject('chatStore', 'userInfoStore')
@observer export default class Messages extends Component {
    constructor(props) {
        super(props);
    }
    componentDidUpdate(props, state) {
        this.onScrollBottom();
        this.goPositionScroll();
    }
    componentDidMount() {
        this.addEventListners();
        this.onScrollBottom();
    }
    goPositionScroll() {
        if(this.props.chatStore.positionScrollMsgs) {
            let scroll = $(ReactDOM.findDOMNode(this.refs.chatContent));
            let lastScrollHeight = this.props.chatStore.positionScrollMsgs;
            let spinner = scroll.children('.more');
            let scrollDiff = scroll[0].scrollHeight - lastScrollHeight;
            scroll[0].scrollTop += scrollDiff - spinner.innerHeight();
        }
    }
    addEventListners() {
        let node = ReactDOM.findDOMNode(this.refs.chatContent);
		node.addEventListener('scroll', this.onScroll.bind(this));

		$(document).on('dragenter', (event) => {
            let dt = event.originalEvent.dataTransfer;
            if (dt.types && (dt.types.indexOf ? dt.types.indexOf('Files') != -1 : dt.types.contains('Files'))) {
                this.props.chatStore.onOpenAnexarArquivos();
            }
        });
    }
    onScroll(e) {
        const isBottom = this.isScrollBottom(e);
        let scroller = $(".pane-chat-msgs")[0];
        let hasScroll = scroller.scrollHeight>scroller.clientHeight;
        if(hasScroll) {
            if(e.target.scrollTop == 0) {
                let positionScroll = e.target.scrollHeight;
                this.props.chatStore.findOldMessages(positionScroll);
            } else {
                this.props.chatStore.clearPositionScrollMsgs();
                if(isBottom) {
                    this.props.chatStore.goScrollToBottomMsgsRead();
                } else {
                    this.props.chatStore.goScrollToBottomMsgs = false;
                }
            }
        }
    }
    isScrollBottom(e) {
        let scroller = $(".pane-chat-msgs");
		let contents = scroller.children('.message-list');
		let spinner = scroller.children('.more');
        let scrollerHeight = scroller.innerHeight();
        let height = contents.innerHeight();
        let isBottom = (height - scrollerHeight) == (e.target.scrollTop - spinner.innerHeight()) ? true : false;
        return isBottom;
    }
    onScrollBottom() {
        if(this.props.chatStore.goScrollToBottomMsgs) {
            setTimeout(() => {
                let scroller = $(".pane-chat-msgs");
                let contents = scroller.children('.message-list');
                let height = contents.innerHeight();
                scroller.scrollTop(height)   
            }, 0);
        }
	}
    onClickGoBottom() {
        this.props.chatStore.goScrollToBottomMsgsRead();
    }
    getNodeMessages() {
        const {chatStore} = this.props;
        const conversa = chatStore.conversa;
        const mensagens = conversa.get('mensagens');
        let badges = chatStore.getBadgeConversa(conversa);
        let nodeMensagens = [];
        let showMsgUnread = true;
        if(mensagens) {
            let qtdMsgs = mensagens.size;
            for(let i = 0; qtdMsgs > i; i++) {
                const mensagem = mensagens.get(i);
                if(badges > 0 && mensagem.get('novaMsg') && showMsgUnread) {
                    nodeMensagens.push(<MsgUnread key={Math.random()} total={badges} />);
                    showMsgUnread = false;
                }
                let key = `${mensagem.get('id')}_${mensagem.get('identifier')}`;
                if(mensagem.get('msgAdmin') == 1) {
                    nodeMensagens.push(<MessageSystem text={mensagem.get('msg')} key={key} />);
                } else {
                    nodeMensagens.push(<Message message={mensagem} key={key} continuation={isContinuation(mensagens, mensagem, i)} />);
                }
            }
        }
        return nodeMensagens;
    }
    render() {
        const chatStore = this.props.chatStore;
        const conversa = chatStore.conversa;
        const mensagens = conversa.get('mensagens');
        
        return(
            <div className="pane-body pane-chat-tile-container">
                {this.props.chatStore.goScrollToBottomMsgs
                    ? null
                    : <IncomingMsgs onClick={this.onClickGoBottom.bind(this)} badge={chatStore.getBadgeConversa(conversa)} />}
				<div className="pane-chat-msgs pane-chat-body lastTabIndex" ref="chatContent">
                    <SpinnerMessage show={chatStore.isFetchOldMessages || chatStore.isFetchConversa} title="Carregar mensagens anteriores" />
                    {!chatStore.isFetchConversa ? <div className="message-list">{this.getNodeMessages()}</div> : null}

				</div>
				<div className="pane-chat-tile"></div>
			</div>
        );
    }
}