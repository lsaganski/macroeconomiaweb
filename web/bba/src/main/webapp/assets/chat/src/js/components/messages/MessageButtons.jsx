import React, {Component} from 'react';
import {observer, inject} from 'mobx-react';
import {blue500} from 'material-ui/styles/colors';
import ArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import ArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right';

@inject('chatStore')
@observer export default class MessageButtons extends Component {
    onClickButton(e){
    	e.preventDefault();
    	let tgt = $(e.target);
    	let action = $(tgt).attr('data-action');
    	let value = $(tgt).attr('data-value');
    	if(action == 'postback'){
    		this.props.chatStore.sendMessage(this.props.chatStore.conversa, value);
    	}else{
    		return false;
    	}
    }
	render(){
		const {buttons} = this.props;
        let nodeButtons = null;

        if(buttons != null && buttons.size > 0) {
            nodeButtons = buttons.map((button) => {
				let key = `${button.get('id')}_${Date.now()}`;
                return (
                	<button className="btn" key={key} data-value={button.get('value')} data-action={button.get('action')} onClick={this.onClickButton.bind(this)}>{button.get('label')}</button>
            	);
            });
        }

		return(
			<div className="chat-msg-buttons">
				<p className="btn-choose">Escolha uma ação:</p>
				{ nodeButtons }
			</div>
		);
	}
}