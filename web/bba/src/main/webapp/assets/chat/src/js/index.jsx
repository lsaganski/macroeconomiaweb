import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "mobx-react";
import stores from "./stores";
import App from "./components/App";
import Chat from "./containers/Chat";


let conversaSelected = GRUPO_SELECTED ? GRUPO_SELECTED : USER_SELECTED;
conversaSelected ? conversaSelected : null;
let isConversaGrupo = GRUPO_SELECTED ? true : false;
stores.chatStore.setChaveGoogleMap(API_GOOGLE_MAPS);
stores.chatStore.setContextUrl(SERVER_URL);
stores.chatStore.setConversaSelected(conversaSelected, isConversaGrupo);
stores.userInfoStore.setUserInfo(USER_INFO);

localStorage.debug = 'chat:*';
navigator.getUserMedia  = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
URL = window.URL || window.webkitURL;

$(document).ready(function () {
    $(window).focus(function() {
        stores.chatStore.changeStatusUserChat(true);
        if($('.block-compose .input').length > 0){
            $('.block-compose .input').focus();
        }
    }).blur(function() {
        stores.chatStore.changeStatusUserChat(false);
    });

    $(window).mousemove(function() {
        if(!stores.chatStore.userActive && document.hasFocus()) {
            stores.chatStore.changeStatusUserChat(true);
        }
    });

    $(window).keydown(function() {
        if(!stores.chatStore.userActive && document.hasFocus()) {
            stores.chatStore.changeStatusUserChat(true);
        }
    });
    
    
    $('body').keyup(function(event) {
        if(event.keyCode === 27){
            if($('.fancybox-wrap').length > 0){
                return false;
            }else{
                stores.chatStore.closeDrawerOpened();
            }
        }
    });
    
});


stores.chatStore.fetchConversas();
stores.chatStore.getCountBadges();
stores.webSocketStore.connect(SOCKET_URI);

ReactDOM.render(
    <Provider {...stores}>
        <App>
        	<Chat />
        </App>
    </Provider>,
    document.getElementById('chat')
);