import {observable, action, inject, computed, autorun} from "mobx";
import request from "superagent";
import immutable, {List, Map} from "immutable";
import userInfoStore from "./userInfoStore";
import infoContatoStore, {DRAWER_INFO_CONTATO, DRAWER_CONTATO_STATUS} from "./infoContatoStore";
import infoMessageStore, {DRAWER_INFO_MESSAGE} from "./infoMessageStore";
import contatoStore, {DRAWER_NOVA_CONVERSA} from "./contatoStore";
import webSocketStore, {
    MESSAGE_REACH_DESTINATION,
    CONVERSATION_WAS_READ,
    MESSAGE_WAS_READ,
    SERVER_RECEIVED_MESSAGE,
    CONVERSATION_WAS_RECEIVED,
    ACCESS_DENIED
} from "./webSocketStore";
import * as DateUtils from "../utils/DateUtils";
import * as FileUtils from "../utils/FileUtils";
import moment from "moment";

import * as StringUtils from '../utils/StringUtils';


let debug = require('debug')('chat:ChatStore');
let prefix = require('superagent-prefix')(SERVER_URL);

const initSnack = () => {
    return new Map({
        show: false,
        message: ""
    });
}

const diasSemana = [
    "Domingo",
    "Segunda-feira",
    "Terça-feira",
    "Quarta-feira",
    "Quinta-feira",
    "Sexta-feira",
    "Sábado"
];

export const MessageStatus = {
    READ: 'icon icon-msg-dblcheck-ack',
    DELIVERED: 'icon icon-msg-dblcheck',
    SENT: 'icon icon-msg-check',
    SENDING: 'icon icon-msg-time'
}

export const ImageMessageStatus = {
    SENT: '',
    SENDING: 'image-sending'
}

class ChatStore {
    @observable conversas = new List();
    @observable conversa = null;
    @observable page = 0;
    @observable isFetchConversas = false;
    @observable isFetchOldMessages = false;
    @observable isSpinnerSearch = false;
    @observable isFetchConversa = false;
    @observable clearSearch = false;
    @observable contextUrl = null;
    @observable snackbar = initSnack();
    @observable positionScrollMsgs = null;
    @observable goScrollToBottomMsgs = false;
    @observable openAnexarArquivo = false;
    @observable conversaSelected = null;
    @observable conversaSelectedIsGrupo = false;
    @observable playSound = false;
    @observable userActive = true;
    @observable countBadges = 0;
    @observable message = "";
    @observable focusInputMessage = false;
    @observable clearInputMessage = false;
    @observable colorsUsersGrupo = {};
    @observable chaveGoogleMap = null;
    @observable messageForward = null;
    @observable requestFindConversas = null;
    @observable devChatMessage = false;

    @observable openModalNotification = false;

    @observable requestLiveURL = null;
    @observable liveUrl = null;

    drawersOpened  = [];

    @action setModalNotification(status) {
        this.openModalNotification = status;
    }

    /***
     * Busca todas as conversas do usuário logado,
     * no caso o userInfo
     */
    @action fetchConversas(search) {
        const userInfo = userInfoStore.userInfo;
        this.isSpinnerSearch = search && search != "";
        this.isFetchConversas = true;
        let dataSend = {
            wsVersion: 3,
            form_name: 'form',
            mode: 'json',
            search: search ? search : '',
            user_id: userInfo.get('id'),
            user: userInfo.get('login'),
            wstoken: userInfo.get('token')
        };

        if(this.requestFindConversas != null) {
            this.requestFindConversas.abort();
        }

        const endRequest = (err, data) => {
            this.requestFindConversa = null;
            this.isFetchConversas = false;
            this.isSpinnerSearch = false;
            if(err) throw new Error('Falha ao busca as conversas');
            
            if(data.body.status == 'OK') {
                this.conversas = immutable.fromJS(data.body.mensagens);
                this.sendMessageReachDestination();
                if(this.conversaSelected) {
                    this.novaConversa(this.conversaSelected, this.conversaSelectedIsGrupo).then((conversa) => {
                        this.fetchConversa(conversa);
                    });
                }

            } else if(data.body.status == 'NOK') {
                swal('Atenção', data.body.mensagem, 'error');
                this.conversas = [];
            }
        }
        this.requestFindConversas = request
            .post("/ws/conversas.htm")
            .use(prefix)
            .type('form')
            .set('nonce_token', userInfo.get('id') + Date.now())
            .send(dataSend)
            .end(endRequest);
    }

    @action getLiveUrl(message){
        if(message && message == '') return;

        let uri = StringUtils.matchURL(message);
        if(uri == null || uri.length == 0) return;
        uri = uri[0];
        let query = 'select * from htmlstring where url="' + uri + '" AND xpath="//head|//body"';

        if(this.requestLiveURL) {
            this.requestLiveURL.abort();
        }

        const endRequest = (err, data) => {
            this.requestLiveURL = null;
            this.liveUrl = null;
            if(err) {
                this.liveUrl = null;
            }

            if(data && data.text){
                let json = JSON.parse(data.text);
                if(json.query.results && json.query.results.result){
                    let result = json.query.results.result;
                    
                    let parser = new DOMParser();
                    let rendered = parser.parseFromString(result, 'text/html');

                    let title = '', img = '', description = '';

                    title = $(rendered, uri).find('title');
                    if(title.length > 0){
                        title = title[0].innerHTML;
                    }else{
                        title = uri;
                    }
                    
                    img = $(rendered, uri).find('link[rel^="apple-touch-icon"]');
                    if(img.length > 0){
                        img = $(img[0]).attr('href');
                    }else{
                        img = $(rendered, uri).find('link[rel~="icon"]').attr('href');
                    }
                    
                    description = $(rendered, uri).find('meta[name="description"]');
                    if(description.length > 0){
                        description = $(description[0]).attr('content');
                    }else{
                        description = $(rendered, uri).find('meta[property="og:description"]').attr('content');
                    }

                    let liveUrl = {
                        title, 
                        img,
                        description
                    };
                   this.liveUrl = liveUrl;
                }
            }
        }

        this.requestLiveURL = request.get("http://query.yahooapis.com/v1/public/yql?q=" + encodeURIComponent(query) + "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&_="+Date.now())
                                     .use(prefix)
                                     .end(endRequest);
    }

    /**
     * Open dev chat message
     */
    @action openDevChatMessage() {
        this.devChatMessage = true;
    }

    /**
     * Close dev chat message
     */
    @action closeDevChatMessage() {
        this.devChatMessage = false;
    }

    /**
    * Limpa os dados do LiveUrl
    */
    @action clearLiveUrl() {
        this.liveUrl = null;
    }

    /**
     * Metodo para buscar uma conversa selecionadas
     */
    findConversa(cId, page) {
        this.cancelRequestFindConversa();
        const userInfo = userInfoStore.userInfo;
        let dataSend = {
            id: cId,
            wsVersion: 3,
            form_name: "form",
            mode: "json",
            user_id: userInfo.get('id'),
            user: userInfo.get('login'),
            page: page,
            maxRows: 30,
            wstoken: userInfo.get('token')
        }
        return request.post('/ws/conversa.htm').use(prefix).set('nonce_token', userInfo.get('id') + Date.now()).type('form').send(dataSend);
    }

    /**
     * Busca a conversa que o usuário selecionou
     */
    @action fetchConversa(conversa, messageToForward) {
        this.conversa = conversa;
        const cId = conversa.get('conversaId');
        infoContatoStore.closeInfoUsuario();
        infoMessageStore.onCloseDrawerInfoMessage();
        this.clearSearch = true;
        this.message = "";
        this.isFetchConversa = true;
        this.focusInputMessage = true;
        this.clearInputMessage = true;
        setTimeout(() => {
            this.focusInputMessage = false;
            this.clearInputMessage = false;
        }, 0);

        const endRequest = (err, data) => {
            delete this.requestFindConversa;
            this.clearSearch = false;
            this.isFetchConversa = false;
            this.goScrollToBottomMsgs = true;


            this.page = 0;

            if(err) throw new Error('Falha ao busca as mensagens da conversa');

            if(data.body.status == 'OK') {
                this.conversa = immutable.fromJS(data.body.conversa);
                webSocketStore.conversationWasRead(this.conversa);
                this.clearBadges(cId);

                if(messageToForward) {
                    let identifier = this.generateIdentifier;
                    webSocketStore.forwardMessage(this.conversa, messageToForward, identifier);
                    this.addNewMessage(identifier, messageToForward.get('msg'), messageToForward.get('arquivos'), cId, conversa.get('isGroup'));
                    this.messageForward = null;
                }

            } else if(data.body.status == 'NOK') {
                swal('Atenção', data.body.mensagem, 'error');
            }

        }
        this.requestFindConversa = this.findConversa(cId, 0).end(endRequest);        
    }

    /***
     * Busca a mensagens antigas de uma conversa, quando o
     * usuario faz o scroll na lista de mensagens, guarda 
     * a referencia da posicao do scroll no momento em que foi feito
     * a request para o servidor, para manter o usuario na posicao
     * correta
     */
    @action findOldMessages(positionScrollMsgs) {
        this.isFetchOldMessages = true;
        this.goScrollToBottomMsgs = false;
        const userInfo = userInfoStore.userInfo;
        const cId = this.conversa.get('conversaId') ? this.conversa.get('conversaId') : this.conversa.get('id');

        const endRequest = (err, data) => {
            delete this.requestFindConversa;
            this.positionScrollMsgs = positionScrollMsgs;
            this.isFetchOldMessages = false;
            
            if(err) throw new Error('Falha ao busca as mensagens da conversa');

            if(data.body.status == 'OK') {
                this.page++;
                if(data.body.conversa.mensagens && data.body.conversa.mensagens.length > 0) {
                    let mensagens = immutable.fromJS(data.body.conversa.mensagens);
                    mensagens = mensagens.concat(this.conversa.get('mensagens'));
                    this.conversa = this.conversa.set('mensagens', mensagens);
                }
            } else if(data.body.status == 'NOK') {
                swal('Atenção', data.body.mensagem, 'error');
            }
        }

        this.requestFindConversa = this.findConversa(cId, this.page + 1).end(endRequest);
    }

    /**
     * Seta a conversa selecionada, geralmente vem por link da url
     * o da conversa e se ela e grupo ou nao
     */
    @action setConversaSelected(conversaSelected, isGrupo) {
        this.conversaSelected = conversaSelected;
        this.conversaSelectedIsGrupo = isGrupo;
    }

    /**
    * Zera a posicao do scroll da list de mensagens
    */
    @action clearPositionScrollMsgs() {
        this.positionScrollMsgs = null;
    }

    /**
        Cancela a request de uma nova conversa
    */
    @action cancelRequestFindConversa() {
        if(this.requestFindConversa) {
            this.requestFindConversa.abort();
            delete this.requestFindConversa;
        }
    }

    /**
     * Busca a quantidade de badges no servidor, adiciona
     * ao titulo da pagina o total de badges
     */
    getCountBadges() {
        request.post("/ws/notifications.htm")
        .use(prefix)
        .type('form')
        .send({
            user_id: userInfoStore.userInfo.get('login'),
            count: 1,
            tipo: 'chat',
            form_name: 'form',
            mode: 'json'
        }).end((err, data) => {
            this.countBadges = data.body.mensagens;
            this.setTitleBadges();
        });
    }

    /**
    *   Coloca no titulo as badges caso o valor das badges seja maior que 0
    *   caso contrario deixa o titulo de LIVECOM Chat mesmo
    */
    @action setTitleBadges() {

        if(this.countBadges > 0) {
            document.title = `(${this.countBadges}) ${WEB_TEMPLATE_TITLE} Chat`;
        } else {
            document.title = `${WEB_TEMPLATE_TITLE} Chat`;
        }
    }

    @action sairGrupo(conversa) {
        if(conversa.get('isGroup')) {
            swal({
                title: 'Você tem certeza?',
                text: 'Deseja realmente sair do grupo "' + conversa.get('nomeGrupo') + '"',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    const userInfo = userInfoStore.userInfo;
                    const grupoId = conversa.get('grupoId');
                    return request.post("/ws/sairGrupoMensagem.htm")
                        .use(prefix)
                        .type('form')
                        .send({
                            user_id: userInfo.get('id'),
                            grupo_id: grupoId,
                            form_name: 'form',
                            mode: 'json'
                        })
                }
            }).then((resp) => {
                const data = resp.body.mensagem;
                if(data.status == 'OK') {
                    swal({
                        type: 'success',
                        title: 'Sucesso!',
                        text: 'Conversa deletada com sucesso'
                    }).then(() => {
                        infoContatoStore.closeInfoUsuario();
                        this.removeConversa(conversa.get('id'));
                    });
                } else {
                    swal(
                        'Oops...',
                        data.mensagem,
                        'error'
                    );
                }
            }, (dissmiss) => {});
        }
    }
    

    /**
     * zera as badges da conversa selecionada depois de terminar
     * de carregar as mensagens da conversa.
     */
    @action clearBadges(cId) {
        for(let i = 0; this.conversas.size > i; i++) {
            let c = this.conversas.get(i);
            if(c.get('conversaId') == cId) {
                let badge = c.get('badge')
                if(badge) {
                    this.countBadges -= badge.get('mensagens');
                    badge = badge.set('mensagens', 0);
                } else {
                    badge = new Map({mensagens: 0});
                }
                c = c.set('badge', badge);
                this.conversas = this.conversas.set(i, c);
                break;
            }
        }
        this.setTitleBadges();
    }

    /**
     * Recebeu uma mensagen do socket que o status do usuário foi alterado
     * para online | offline, busca o usuario na lista de conversas 
     * e atualiza o status dele caso seja encontrado o usuário na minha lista
     * de conversas
     */
    @action onUserStatusChanged(message) {
        let {from, online, dateOnlineChat, away} = message;
        for(let i = 0; this.conversas.size > i; i++) {
            let c = this.conversas.get(i);
            if(!c.get('isGroup')) {
                if((c.get('fromId') == from || c.get('toId') == from)) {
                    if(c.get('fromId') == from) {
                        if(away) {
                            c = c.set('fromChatOn', online).set('fromChatAway', true).set('fromDateOnlineChat', dateOnlineChat);
                        } else {
                            c = c.set('fromChatOn', online).set('fromChatAway', false).set('fromDateOnlineChat', dateOnlineChat);
                        }
                    } else {
                        if(away) {
                            c = c.set('toChatOn', online).set('toChatAway', true).set('toDateOnlineChat', dateOnlineChat);
                        } else {
                            c = c.set('toChatOn', online).set('toChatAway', false).set('toDateOnlineChat', dateOnlineChat)
                        }
                    }
                    this.conversas = this.conversas.set(i,c);
                    break;
                }
            }
        }

        let c = this.conversa;
        if(c != null) {
            if(!c.get('isGroup')) {
                if(c.get('fromId') == from || c.get('toId') == from) {
                    if(c.get('fromId') == from) {
                        if(away) {
                            this.conversa = c.set('fromChatOn', online).set('fromChatAway', true).set('fromDateOnlineChat', dateOnlineChat);
                        } else {
                            this.conversa = c.set('fromChatOn', online).set('fromChatAway', false).set('fromDateOnlineChat', dateOnlineChat);
                        }
                    } else {
                        if(away) {
                            this.conversa = c.set('toChatOn', online).set('toChatAway', true).set('toDateOnlineChat', dateOnlineChat);
                        } else {
                            this.conversa = c.set('toChatOn', online).set('toChatAway', false).set('toDateOnlineChat', dateOnlineChat);
                        }
                    }
                }
            }
        }
        const contato = infoContatoStore.contato;
        if((contato != null && contato.get('id') == from) && infoContatoStore.chatStatusUser == true) {
            infoContatoStore.openChatStatusUser(infoContatoStore.contato);
        }
    }
    
    /**
     * Seta a url de contexto do servidor
     */
    @action setContextUrl(url) {
        this.contextUrl = url;
    }

    isNovaConversa(cId) {
        return !this.conversas.some(c => c.get('conversaId') == cId);
    }

    changeToFromConversa(conversa) {
        return conversa.set('fromId', conversa.get('toId'))
                .set('fromNome', conversa.get('toNome'))
                .set('fromChatOn', conversa.get('toChatOn'))
                .set('from', conversa.get('to'))
                .set('fromUrlFoto', conversa.get('toUrlFoto'))
                .set('fromUrlFotoThumb', conversa.get('toUrlFotoThumb'))
                .set('fromChatAway', conversa.get('toChatAway'))
                .set('toId', conversa.get('fromId'))
                .set('toNome', conversa.get('fromNome'))
                .set('toChatOn', conversa.get('fromChatOn'))
                .set('toChatAway', conversa.get('fromChatAway'))
                .set('to', conversa.get('from'))
                .set('toUrlFoto', conversa.get('fromUrlFoto'))
                .set('toUrlFotoThumb', conversa.get('fromUrlFotoThumb'));
    }

    formatDateUltimaMsg(date) {
        let data = moment(date).toDate();
        let isHoje = moment(data).isSame(new Date(), "day");
        if(isHoje) {
            return moment(data).format("HH:mm");
        }
        let diffData = moment().diff(data, "days", false);
        if(diffData > 7) {
            return moment(data).format("DD/MM/YYYY");
        }
        let day = moment(data).day()
        return diasSemana[day];
    }

    formatDateMsg(date) {
        let data = moment(date).toDate();
        let isHoje = moment(data).isSame(new Date(), "day");
        if(isHoje) {
            return moment(data).format("HH:mm");
        }
        return moment(data).format("DD/MM HH:mm");
    }

    @action addMsgOnListConversa(raw) {
        const {cId, msg, msgId, from, fromName, files, date, msgAdmin, loc, cards, buttons} = raw;


        const userInfo = userInfoStore.userInfo;
        let conversaId = null;
        if(this.conversa) {
            conversaId = this.conversa.get('conversaId') ? this.conversa.get('conversaId') : this.conversa.get('id')
        }
        let isShowBadges = !this.goScrollToBottomMsgs || conversaId != cId || !this.userActive;
        
        for(let i = 0; this.conversas.size > i; i++) {
            let c = this.conversas.get(i);
            if(c.get('conversaId') == cId) {
                c = c
                    .set('msg', msg)
                    .set('id', msgId)
                    .set('arquivos', immutable.fromJS(files))
                    .set('entregue', userInfo.get('id') != from ? 1 : 0)
                    .set('lida', userInfo.get('id') != from ? 1 : 0)
                    .set('msgAdmin', msgAdmin)
                    .set('data', date)
                    .set("buttons", immutable.fromJS(buttons))
                    .set('cards', immutable.fromJS(cards));

                if(loc) {
                    c = c.set('location', immutable.fromJS(loc));
                } else {
                    c = c.set('location', null);
                }

                if(c.get('isGroup')) {
                    c = c.set('fromId', from).set('fromNome', fromName);
                } else if(c.get('fromId') != from){ 
                    c = this.changeToFromConversa(c);
                }

                // caso não esteja aberta envia apenas que o usuário recebeu a mensagem
                if(isShowBadges && userInfo.get('id') != from) {
                    let badge = c.get('badge')
                    badge = badge.set('mensagens', badge.get('mensagens') + 1);
                    let isNotify = c.get('notifications').get('notification');
                    if(isNotify) {
                        this.countBadges += 1;
                        this.setTitleBadges();
                    }
                    c = c.set('badge', badge);
                    webSocketStore.messageReachDestination(raw);
                    if(!this.userActive) {
                        this.showNotification(c);
                    }
                    if(this.notificationSoundActive(c) && isNotify) {
                        this.setPlaySound(cId);
                    }
                }

                /**
                 * Caso a conversa não seja a primeira da lista altera ela para
                 * ser a primeira conversa a lista
                 */
                if(i != 0) {
                    this.conversas = this.conversas.splice(i, 1); // remove o elemento do map
                    this.conversas = this.conversas.unshift(c); // adiciona o elemento na primeira posicao
                } else {
                    this.conversas = this.conversas.set(i, c); // seta os dados na conversa, como ela é a primeira da lista mesmo
                }
                break;
            }
            
        }
    }


    @action addMsgOnListMessages(raw) {
        const {cId, msg, from, fromName, files, msgId, date, loc, msgAdmin, cards, buttons} = raw;
        const userInfo = userInfoStore.userInfo;
        let conversaId = null;
        if(this.conversa) {
            conversaId = this.conversa.get('conversaId') ? this.conversa.get('conversaId') : this.conversa.get('id')
        }
        let isShowBadges = !this.goScrollToBottomMsgs || conversaId != cId || !this.userActive;

        if(this.conversa != null && conversaId == cId) {
            let newMessage = new Map({
                toId: userInfo.get('id'),
                toNome: userInfo.get('nome'),
                msg: msg,
                id: msgId,
                fromId: from,
                fromNome: fromName,
                msgAdmin: msgAdmin,
                arquivos: immutable.fromJS(files),
                buttons: immutable.fromJS(buttons),
                cards: immutable.fromJS(cards),
                entregue: userInfo.get('id') != from ? true : false,
                novaMsg: userInfo.get('id') != from ? true : false,
                isGroup: this.conversa.get('isGroup'),
                grupoId: this.conversa.get('isGroup') ? this.conversa.get('grupoId') : null,
                data: date
            });

            if(loc) {
                newMessage = newMessage.set('location', immutable.fromJS(loc));
            }

            let mensagens = this.conversa.get('mensagens');
            mensagens = mensagens.push(newMessage);
            this.conversa = this.conversa.set('mensagens', mensagens);

            if(isShowBadges && userInfo.get('id') != from)  {
                let badge = this.conversa.get('badge');
                if(!badge) {
                    badge = new Map({
                        mensagens: 0
                    });
                }
                badge = badge.set('mensagens', badge.get('mensagens') + 1);
                this.conversa = this.conversa.set('badge', badge);
            }

            if(userInfo.get('id') != from && this.userActive)  {
                webSocketStore.messageWasRead(raw);
            }
        }
    }
    

    /**
     * Recebeu uma nova mensagem pelo socket e busca a conversa
     * e adiciona na lista de conversa a mensagem caso a conversa exista 
     * na lista, caso contrario busca a conversa no servidor e adiciona
     * a conversa na lista de conversas.
     */
    @action onReceivedMessage(raw) {
        const {cId, gId, from} = raw;
        const isNovaConversa = this.isNovaConversa(cId);
        if(isNovaConversa) {
            this.novaConversa(gId ? gId : from, gId ? true : false);
            this.setPlaySound(cId);
        } else {

            let isNova = this.isNovaMensagem(raw);
            if(isNova) {
                this.addMsgOnListConversa(raw);
                this.addMsgOnListMessages(raw);
            }

        }
        
    }


    isNovaMensagem(raw) {
        const {cId, msgId} = raw;
        for(let i = 0; this.conversas.size > i; i++) {
            let c = this.conversas.get(i);
            if(c.get('id') == msgId) {
                return false;
            }
        }

        if(this.conversa != null && this.conversa.get('id') == cId) {
            let mensagens = this.conversa.get('mensagens');
            if(!mensagens || mensagens.size == 0) {
                return true;
            }

            for(let i = mensagens.size - 1; i >= 0; i--) {
                let m = mensagens.get(i);
                if(m.get('id') == msgId) {
                    return false;
                }
            }
        }
        return true;
    }

    @action setPlaySound(cId) {
        let conversaId = null;
        if(this.conversa) {
            conversaId = this.conversa.get('conversaId') ? this.conversa.get('conversaId') : this.conversa.get('id');
        }
        if(!this.userActive || conversaId != cId) {
            this.playSound = true;
            setTimeout(() => {
                this.playSound = false;
            }, 1000);
        }
    }
    

    notificationActive(conversa) {
        return conversa.get('notifications').get('notification');
    }

    notificationSoundActive(conversa) {
        return conversa.get('notifications').get('sound');
    }

    @action onClickShowNotifications() {
        infoContatoStore.changeStatusBrowerNotification(true, this.conversa);
    }

    /**
     *  retorna a quantidade de badges de uma conversa,
     * pode ser usado tanto na lista de conversas, e ate mesmo na conversa
     */
    getBadgeConversa(conversa) {
        if(!conversa || !conversa.get('badge')) return 0;
        let badge = conversa.get('badge');
        return badge.get('mensagens');
    }

    /**
     * Usado apenas em caso de grupo, caso seja um grupo
     * ele busca no array de colors, para adicionar uma cor
     * ao nome do usuario
     */
    getColorUserName(message) {
        if(message.get('isGroup') && !this.isMyMessage(message)) {
            let colors = this.colorsUsersGrupo[message.get('grupoId')];
            colors = colors ? colors : {};
            let fromId = message.get('fromId');
            let classColorName = colors[fromId];
            if(!classColorName) {
                let colorRandom = Math.floor(Math.random()*(20-0+1)+0);
                classColorName = `color-${colorRandom}`;
                colors[fromId] = classColorName;
                this.colorsUsersGrupo[message.get('grupoId')] = colors;
            }
            return classColorName;
        }
        return "color-15";
    }

    

    @action goScrollToBottomMsgsRead() {
        const statusScrollBefore = this.goScrollToBottomMsgs;
        this.goScrollToBottomMsgs = true;
        let badge = this.getBadgeConversa(this.conversa);
        if(badge > 0 && statusScrollBefore == false) {
            this.clearBadgeConversaAtiva();
        }
    }


    @action changeStatusUserChat(status) {
        this.userActive = status;
        if(this.conversa != null && status && this.goScrollToBottomMsgs) {
            setTimeout(() => {
                this.clearBadgeConversaAtiva();
            }, 2000);
            webSocketStore.sendAway(false);
        } else if(status) {
            webSocketStore.sendAway(false);
        } else {
            webSocketStore.sendAway(true);
        }
    }

    @action clearBadgeConversaAtiva() {
        const conversaId = this.conversa.get('conversaId') ? this.conversa.get('conversaId') : this.conversa.get('id');
        for(let i = 0; this.conversas.size > i; i++) {
            let c = this.conversas.get(i);
            if(c.get('conversaId') == conversaId) {
                let badge = c.get('badge')
                if(badge.get('mensagens') > 0) {
                    this.countBadges -= badge.get('mensagens');
                    badge = badge.set('mensagens', 0);
                    this.setTitleBadges();
                    c = c.set('badge', badge);
                    this.conversas = this.conversas.set(i, c);
                    webSocketStore.conversationWasRead(this.conversa);
                }
            }
        }

        this.conversa = this.conversa.set('badge', new Map({
            mensagens: 0
        }));

        let mensagens = this.conversa.get('mensagens');
        if(mensagens) {
            for(let i = mensagens.size - 1; i >= 0; i--) {
                let m = mensagens.get(i);
                if(m.get('novaMsg')) {
                    m = m.set('novaMsg', false);
                    mensagens = mensagens.set(i, m);
                } else {
                    break;
                }
            }
            this.conversa = this.conversa.set('mensagens', mensagens);
        }
        
    }

    @action novaConversa(contatoId, isGroup) {
        let deferred = $.Deferred();
        const userInfo = userInfoStore.userInfo;
        this.clearSearch = true;
        let req = null;
        let data = null;
        if(isGroup) {
            req = request.post("/ws/getConversaUserGrupo.htm");
            data = {
                form_name: "form",
                mode: "json",
                user_logado_id: userInfo.get('id'),
                grupo_id: contatoId,
                wsVersion: 3
            }
        } else {
            req = request.post("/ws/getConversaUser.htm");
            data = {
                wsVersion: 3,
                form_name: "form",
                mode: "json",
                user_from_id: userInfo.get('id'),
                user_to_id: contatoId,

            }
        }
        req.use(prefix)
            .type('form')
            .set('nonce_token', userInfo.get('id') + Date.now())
            .send(data)
            .end((err, resp) => {
                if(err) throw err;
                this.clearSearch = false;
                let conversa = immutable.fromJS(resp.body.msg);
                let isNovaConversa = this.isNovaConversa(conversa.get('conversaId'));
                if(isNovaConversa) {
                    this.conversas = this.conversas.unshift(conversa); // adiciona o elemento na primeira posicao
                }
                deferred.resolve(conversa);
            })
        return deferred.promise();
    }

    /**
     * Mostra uma barra de notificacao
     */
    @action onShowSnackBar(message) {
        if(this.timeoutSnack) {
            clearTimeout(this.timeoutSnack);
            delete this.timeoutSnack;
        }

        this.snackbar = new Map()
            .set('show', true)
            .set('message', message);
        this.timeoutSnack = setTimeout(() => {
           this.onCloseSnackBar();
        }, 2000);
    }

    /**
     * Fecha a barra de notificacao
     */
    @action onCloseSnackBar() {
        this.snackbar = initSnack();
    }

    /**
     * Recebe que o usuário esta digitando
     * adiciona o status de isTyping e depois de 5000
     * remove o status de digitando
     */
    @action onTypingStatus(message) {
        const {cId, typing, fromName} = message;
        for(let i = 0; this.conversas.size > i; i++) {
            let c = this.conversas.get(i);
            if(c.get('conversaId') == cId) {
                c = c.set('isTyping', typing).set('typingLabel', c.get('isGroup') ? `${fromName} está digitando...` : 'digitando...');
                this.conversas = this.conversas.set(i, c);
                break;
            }
        }
        let conversaId = null;
        if(this.conversa) {
            conversaId = this.conversa.get('conversaId') ? this.conversa.get('conversaId') : this.conversa.get('id');
        }
        if(conversaId == cId) {
            this.conversa = this.conversa.set('isTyping', typing).set('typingLabel', this.conversa.get('isGroup') ? `${fromName} está digitando...` : 'digitando...');
        }

        setTimeout(() => {
            for(let i = 0; this.conversas.size > i; i++) {
                let c = this.conversas.get(i);
                if(c.get('conversaId') == cId) {
                    c = c.set('isTyping', false).set('typingLabel', '');
                    this.conversas = this.conversas.set(i, c);
                    break;
                }
            }

            if(conversaId == cId) {
                this.conversa = this.conversa.set('isTyping', false).set('typingLabel','');
            }
        }, 5000);
    }

    @computed get generateIdentifier() {
        return Date.now();
    }

    /**
     * Envia uma mensagem pelo websocket 
     * e adiciona na lista de mensagens a msg enviada
     */
    @action sendMessage(conversa, message) {
        const isOnlineWebsocket = webSocketStore.status;
        this.liveUrl = null;
        if(isOnlineWebsocket) {
            const identifier = Date.now();
            const conversaId = conversa.get('id');
            const isGroup = conversa.get('isGroup');
            webSocketStore.sendMessage(identifier, message, conversa);
            this.addNewMessage(identifier, message, null, conversaId, isGroup);
            this.goScrollToBottomMsgs = true;
            this.clearInputMessage = true;
            setTimeout(() => {
                this.clearInputMessage = false;
            }, 0);
        } else {
            console.log('mostra alerta');
        }
    }

    @action sendMessageCustom(conversa, message) {
        const isOnlineWebsocket = webSocketStore.status;
        if(isOnlineWebsocket) {
            const identifier = Date.now();
            const conversaId = conversa.get('id');
            const isGroup = conversa.get('isGroup');
            webSocketStore.sendMessageCustom(identifier, message, conversa);
        } else {
            console.log('mostra alerta');
        }
    }


    /**
    * Envia um arquivo como mensagem para o usuario
    */
    @action sendFile(conversa, file) {
        const userInfo = userInfoStore.userInfo;

        FileUtils.toBinaryString(file).then((data) => {
            let base64 = FileUtils.encode(data);
            const identifier = this.generateIdentifier;

            let arquivos = new List();
            arquivos = arquivos.set(0, new Map({
                    file: file.name,
                    extensao: FileUtils.getExtension(file.name),
                    url: file.preview,
                    usuario: {
                        id: userInfo.get('id'),
                        nome: userInfo.get('nome')
                }
            }));
            this.addNewMessage(identifier, null, arquivos, conversa.get('id'), conversa.get('isGroup'));
            this.onCloseAnexarArquivos();
            //
            request.post('/rest/chat')
            .use(prefix)
            .send({
                identifier: identifier,
                conversationID : conversa.get('id'),
                fromUserID: userInfo.get('id'),
                arquivos: [{
                    base64: base64,
                    nome: file.name
                }]
            }).end((err, data) => {
                if(err) throw err;
                let resp = data.body;
                let message = {
                    code: SERVER_RECEIVED_MESSAGE,
                    identifier: resp.identifier,
                    cId: resp.conversaId,
                    msgId: resp.id,
                    status: 'ok',
                    arquivos: resp.arquivos
                }
                this.onServerReceivedMessage(message);
            });
        });
    }

    /**
     * Adiciona uma nova mensagem a conversa
     */
    @action addNewMessage(identifier, msg, arquivos, conversaId, isGroup) {
        const userInfo = userInfoStore.userInfo;
        const data = DateUtils.newDate(identifier);

        
        /**
         * Busca na lista de conversas pelo conversaId e adiciona
         * a mensagem enviada na minha lista de mensagens das conversa
         */
        let conversas = this.conversas;
        for(let i = 0; conversas.size > i; i++) {
            let c = conversas.get(i);
            if(c.get('conversaId') == conversaId) {
                c = c.set('identifier', identifier)
                    .set('id', null)
                    .set('msg', msg)
                    .set('lida', 0)
                    .set('entregue', 0)
                    .set('arquivos', arquivos)
                    .set('dataUpdatedString', data)
                    .set('dataWeb', data);

                if(c.get('fromId') != userInfo.get('id')) {
                    c = c
                        .set('toId', c.get('fromId'))
                        .set('toNome', c.get('fromNome'))
                        .set('toChatOn', c.get('fromChatOn'))
                        .set('to', c.get('from'))
                        .set('toUrlFoto', c.get('fromUrlFoto'))
                        .set('toUrlFotoThumb', c.get('fromUrlFotoThumb'))
                        .set('fromId', userInfo.get('id'))
                        .set('fromNome', userInfo.get('nome'))
                        .set('fromChatOn', true)
                        .set('from', userInfo.get('nome'))
                        .set('fromUrlFoto', userInfo.get('foto'))
                        .set('fromUrlFotoThumb', userInfo.get('foto'));
                }

                if(i != 0) {
                    this.conversas = this.conversas.splice(i, 1); // remove o elemento do map
                    this.conversas = this.conversas.unshift(c); // adiciona o elemento na primeira posicao
                } else {
                    this.conversas = this.conversas.set(i, c); // seta os dados na conversa, como ela é a primeira da lista mesmo
                }
                break;
            }
        }
        let cAtivaId = null;
        if(this.conversa) {
            cAtivaId = this.conversa.get('conversaId') ? this.conversa.get('conversaId') : this.conversa.get('id');
        }
        if(cAtivaId == conversaId) {
            let newMessage = new Map({
                conversaId,
                msg,
                identifier,
                isGroup,
                arquivos: arquivos,
                fromId: userInfo.get('id'),
                fromNome: userInfo.get('nome'),
                fromFoto: userInfo.get('foto'),
                fromUrlFotoThumb: userInfo.get('foto'),
                dataWeb: data,
            });
            let mensagens = this.conversa.get('mensagens');
            if(!mensagens) {
                mensagens = new List();
            }
            mensagens = mensagens.push(newMessage);
            this.conversa = this.conversa.set('mensagens', mensagens);
        }

    }

    /**
     * Marca que as mensagens da conversa
     * foi lida
     */
    @action onChangeStatusMessage(message) {
        const {cId, msgId, code, g2C, g2A, from, to} = message;
        const userInfo = userInfoStore.userInfo;
        if(userInfo.get('id') == from && !to && code == CONVERSATION_WAS_READ) {
            this.clearBadges(cId);
            return;
        }


        let conversaId = null;
        if(this.conversa) {
            conversaId = this.conversa.get('conversaId') ? this.conversa.get('conversaId') : this.conversa.get('id');
        }
        if(this.conversa && conversaId == cId) {
            let mensagens = this.conversa.get('mensagens');
            for(let i = mensagens.size - 1; i >= 0; i--) {
                let m = mensagens.get(i);
                if(this.conversa.get('isGroup')) {
                    if(code == CONVERSATION_WAS_READ && g2A == 1) {
                        m = m.set('entregue', 1).set('lida', 1);
                    } else if(code == CONVERSATION_WAS_RECEIVED  && g2C == 1) {
                        m = m.set('entregue', 1);
                    } else if(code == MESSAGE_WAS_READ  && m.get('lida') != 1 && g2A == 1 && m.get('id') == msgId) {
                        m = m.set('entregue', 1).set('lida', 1);
                    } else if(code == MESSAGE_REACH_DESTINATION && m.get('lida') != 1 && g2C == 1 && m.get('id') == msgId) {
                        m = m.set('entregue', 1).set('lida', 0);
                    }
                    mensagens = mensagens.set(i, m);
                } else {
                    if(code == CONVERSATION_WAS_READ) {
                        m = m.set('entregue', 1).set('lida', 1);
                    } else if(code == MESSAGE_WAS_READ && m.get('lida') != 1 && m.get('id') == msgId) {
                        m = m.set('entregue', 1).set('lida', 1);
                    } else if(code == MESSAGE_REACH_DESTINATION && m.get('lida') != 1 && m.get('id') == msgId) {
                        m = m.set('entregue', 1).set('lida', 0);
                    }
                    mensagens = mensagens.set(i, m);
                }
               
            }
            this.conversa = this.conversa.set('mensagens', mensagens);
        }

        for(let i = 0; this.conversas.size > i; i++) {
            let c = this.conversas.get(i);
            if(c.get('conversaId') == cId) {
                if(c.get('isGroup')) {
                    if(code == CONVERSATION_WAS_READ && g2A == 1) {
                        c = c.set('entregue', 1).set('lida', 1);
                    } else if(code == CONVERSATION_WAS_RECEIVED  && g2C == 1) {
                        c = c.set('entregue', 1);
                    } else if(code == MESSAGE_WAS_READ   && c.get('lida') != 1 && g2A == 1 && c.get('id') == msgId) {
                        c = c.set('entregue', 1).set('lida', 1);
                    } else if(code == MESSAGE_REACH_DESTINATION && c.get('lida') != 1 && g2C == 1) {
                        c = c.set('entregue', 1).set('lida', 0);
                    }
                    this.conversas = this.conversas.set(i, c);    
                } else {
                    if(code == CONVERSATION_WAS_READ) {
                        c = c.set('entregue', 1).set('lida', 1);
                    } else if(code == CONVERSATION_WAS_RECEIVED) {
                        c = c.set('entregue', 1);
                    } else if(code == MESSAGE_WAS_READ && c.get('lida') != 1 && c.get('id') == msgId) {
                        c = c.set('entregue', 1).set('lida', 1);
                    } else if(code == MESSAGE_REACH_DESTINATION && c.get('lida') != 1) {
                        c = c.set('entregue', 1).set('lida', 0);
                    }
                    this.conversas = this.conversas.set(i, c);
                }
                break;
            }
        }

        if(infoMessageStore.message && infoMessageStore.drawerActive && this.conversa && conversaId == cId) {
            infoMessageStore.fetchInfoMessage(this.conversa, infoMessageStore.message.get('id'));
        }
    }

    /**
     * Servidor avisa que recebeu a mensagem e
     * podemos marcar a mensagem como recebida pelo servidor
     */
    @action onServerReceivedMessage(message) {
        const {cId, identifier, msgId, arquivos} = message;
        let conversas = this.conversas;
        for(let i = 0; conversas.size > i; i++) {
            let c = conversas.get(i);
            if(c.get('conversaId') == cId && c.get('identifier') == identifier) {
                c = c.set('id', msgId)
                this.conversas = this.conversas.set(i, c);
                break;
            }
        }
        
        if(this.conversa) {
            const conversaId = this.conversa.get('conversaId') ? this.conversa.get('conversaId') : this.conversa.get('id');
            if(conversaId == cId) {
                let mensagens = this.conversa.get('mensagens');
                for(let i = mensagens.size - 1; i >= 0; i--) {
                    let m = mensagens.get(i);
                    if(m.get('identifier') == identifier) {
                        m = m.set('id', msgId);
                        if(arquivos) {
                            m = m.set('arquivos', immutable.fromJS(arquivos));
                        }
                        mensagens = mensagens.set(i, m);
                        this.conversa = this.conversa.set('mensagens', mensagens);
                        break;
                    }
                }
            }
        }
        
    }

    @action onCloseAnexarArquivos() {
        this.openAnexarArquivo = false;
    }

    @action onOpenAnexarArquivos() {
        this.openAnexarArquivo = true;
    }

    @action onDeleteConversa(conversa, conversaId) {
        const userInfo = userInfoStore.userInfo;
        const user = this.getUser(conversa);
        swal({
            title: 'Você tem certeza?',
            text: 'Deseja deletar a conversa com "' + user.get('nome') + '"',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return request
                    .post('/ws/deletar.htm')
                    .use(prefix)
                    .type('form')
                    .send({
                        id: conversaId,
                        wsVersion: 2,
                        form_name: 'form',
                        mode: 'json',
                        user_id: userInfo.get('id'),
                        user: userInfo.get('login'),
                        tipo: 'conversa'
                    });
            }
      }).then((resp) => {
          const data = resp.body.mensagem;
          if(data.status == 'OK') {
              swal({
                  type: 'success',
                  title: 'Sucesso!',
                  text: 'Conversa deletada com sucesso'
              }).then(() => {
                    this.conversa = null;
                    infoContatoStore.closeInfoUsuario();
                    this.removeConversa(conversa.get('id'));
              });
          } else {
              swal(
                  'Oops...',
                  data.mensagem,
                  'error'
              );
          }
      }, (dissmiss) => {});
    }

    @action removeConversa(cId, code, msg) {
        for(let i = 0; this.conversas.size > i; i++) {
            if(this.conversas.get(i).get('conversaId') == cId) {
                this.conversas = this.conversas.splice(i, 1);
                break;
            }
        }
        let conversaId
        if(this.conversa != null) {
            conversaId = this.conversa.get('conversaId');
            conversaId = conversaId ? conversaId : this.conversa.get('id');
        }

        if(conversaId == cId) {
            if(code && code == ACCESS_DENIED) {
                swal({
                    type: 'warning',
                    title: 'Aviso!',
                    text: msg
                }).then(() => {
                    this.conversa = null;
                    infoContatoStore.closeInfoUsuario();
                });
            } else {
                this.conversa = null;
            }
        }
    }

    /**
    * Envia para o servidor que o usuario recebeu as mensagens
    */
    sendMessageReachDestination() {
        if(webSocketStore.status) {
            const userInfo = userInfoStore.userInfo;
            this.conversas.forEach(c => {
                if(this.isSendMessageReachDestination(c)) {
                    webSocketStore.socket.send(JSON.stringify({
                        "code": MESSAGE_REACH_DESTINATION,
                        "cId": c.get('conversaId'),
                        "msgId": c.get('id'),
                        "from": c.get('fromId'),
                        "to": userInfo.get('id'),
                        "status": "Ok"
                    }));
                }
            });
        }
    }

    /**
    * Realiza as validacoes necessarias se pode ou nao enviar o status messageReachDestination
    * para o webSocket
    * @return boolean
    */
    isSendMessageReachDestination(conversa) {
        let badge = conversa.get('badge');
        let qtdBadges = badge ? badge.get('mensagens') : 0;
        let bool = webSocketStore.status && qtdBadges > 0 && conversa.get('entregue') != 1 && conversa.get('lida') != 1;
        return bool;
    }

    /**
     * Envia para o webSocketStore que o usuario esta digitando
     */
    sendTypingStatus(conversa) {
        webSocketStore.sendTypingStatus(conversa);
    }
 
    /**
     * Verifica se a conversa esta ativa
     */
    isConversaActive(conversa) {
        if(this.conversa) {
            const cAtivaId = this.conversa.get('conversaId') ? this.conversa.get('conversaId') : this.conversa.get('id');
            return cAtivaId == conversa.get('conversaId');
        }
        return false;
    }

    @computed get hasConversaActive() {
        return this.conversa != null;
    }

    /***
     * Pega a data da ultima mensagem
     */
    getDataUltimaMensagem(conversa) {
        return this.formatDateUltimaMsg(conversa.get('data'));
    }

    isMyMessage(message) {
        return message.get('fromId') == userInfoStore.userInfo.get('id');
    }

    /**
     * Verifica se a conversa tem arquivos
     */
    conversaHasFile(conversa) {
        return conversa.get('arquivos') && conversa.get('arquivos').size > 0;
    }

    /**
     * Verifica se a conversa tem cards
     */
    conversaHasCard(conversa) {
        return conversa.get('cards') && conversa.get('cards').size > 0;
    }

    /**
     * Verifica se a conversa tem cards
     */
    conversaHasButtons(conversa) {
        return conversa.get('buttons') && conversa.get('buttons').size > 0;
    }

    /**
     * Verifica se a conversa tem localização
     */
    conversaHasLocation(conversa) {
        return conversa.get('location') && conversa.get('location').size > 0;
    }

    /**
     * verifica se a conversa esta lida
     */
    isLida(conversa) {
        return conversa.get('lida') == 1;
    }

    /**
     * Verifica se a conversa esta entregue
     */
    isEntregue(conversa) {
        return conversa.get('entregue') == 1;
    }

    /**
     * Verifica se a conversa esta enviada
     */
    isEnviada(conversa) {
        return conversa.get('id') ? true : false;
    }

    

    /**
     * Retorna o link da conversa, com a url inteira para caso o usuario queira
     * copiar e colar para outro usuario
     */
    getLinkConversa(conversa) {
        const user = this.getUser(conversa);
        if(conversa.get('isGroup')) {
            return `${location.protocol}//${location.host}${this.contextUrl}/pages/chat.htm?grupo=${user.get('id')}`;
        } else {
            return `${location.protocol}//${location.host}${this.contextUrl}/pages/chat.htm?user=${user.get('id')}`;
        }
    }

    /**
     * Return o label de usuario digitando
     */
    getLabelTyping(conversa) {
        return conversa.get('typingLabel');
    }

    /**
     * Busca o from da conversa que nao seja o userInfo, caso o from seja o proprio userInfo retorna null
     */
    getFromUserConversa(conversa) {
        if(conversa.get('fromId') == userInfoStore.userInfo.get('id')) {
            return null;
        }
        return new Map({
            id: conversa.get('fromId'),
            nome: conversa.get('fromNome'),
            urlFoto: conversa.get('fromUrlFoto'),
            urlFotoThumb: conversa.get('fromUrlFotoThumb')
        });
    }

    /**
     * Mostra uma notificacao de browser.
     */
    showNotification(conversa){
        if(window.Notification) {
            let self = this;
            let isNotify = conversa.get('notifications').get('notification');
            if(isNotify) {
                const user = this.getUser(conversa);
                let msg = "";
                let userFrom = "";
                if(conversa.get('isGroup')) {
                    userFrom = conversa.get('fromNome');
                }
                const location = conversa.get('location');
                const arquivos = conversa.get('arquivos');
                if(arquivos && arquivos.size > 0) {
                    let arquivo = arquivos.get(0);
                    let ext = arquivo.get('extensao').toLowerCase();
                    if (FileUtils.isImage(ext)) {
                        msg = `${userFrom} enviou uma Imagem`;
                    } else if (FileUtils.isVideo(ext)) {
                        msg = `${userFrom} enviou um Video`;
                    } else if (FileUtils.isAudio(ext)) {
                        msg = `${userFrom} enviou um Audio`;
                    } else if (FileUtils.isDoc(ext) || FileUtils.isProgram(ext) || FileUtils.isExecutable(ext) || FileUtils.isCompressed(ext)) {
                        msg = `${userFrom} enviou um Documento`;
                    }
                }else if(location && location.get('lat') != 0 && location.get('lng') != 0) {
                    msg = `${userFrom} enviou uma Localização`;
                } else {
                    msg = `${userFrom} ${conversa.get('msg')}`;
                }

                var notification = new Notification(user.get('nome'), {
                    body: msg,
                    tag: 'livecom-chat',
                    icon: user.get('urlFoto'),
                    data: {
                        contextUrl: this.contextUrl,
                        user: user.toJS(),
                    }
                });
                notification.onclick = function() {
                    self.fetchConversa(conversa);
                    window.focus();
                    this.close();
                }
                setTimeout(notification.close.bind(notification), 4000);
            }
        }
    }

    /**
     * Pega o usuário da conversa que não seja o userInfo,
     * e retorna as informações que for necessária
     */
    getUser(conversa) {
        let user = new Map();
        if(!conversa) return user;

        const userInfo = userInfoStore.userInfo;
        if(conversa.get('isGroup')) {
            user =  {
                id: conversa.get('grupoId'),
                nome: conversa.get('nomeGrupo'),
                dateOnline: null,
            }
            if(conversa.get('fotoGrupo')) {
                user.urlFoto = conversa.get('fotoGrupo').get('url');
                user.urlFotoThumb = conversa.get('fotoGrupo').get('urlThumb');
            } else {
                user.urlFoto = `${this.contextUrl}/img/grupo.jpg`;
                user.urlFotoThumb = `${this.contextUrl}/img/grupo.jpg`;
            }
        } else {
            if(userInfo.get('id') == conversa.get('fromId')) {
                user = {
                    id: conversa.get('toId'),
                    nome: conversa.get('toNome'),
                    urlFoto: conversa.get('toUrlFoto'),
                    urlFotoThumb: conversa.get('toUrlFotoThumb'),
                    online: conversa.get('toChatOn'),
                    dataOnline: conversa.get('toDateOnlineChat'),
                    away: conversa.get('toChatAway')

                };

                if(!user.urlFoto) {
                    user.urlFoto = `${this.contextUrl}/img/user.jpg`;
                }
                if(!user.urlFotoThumb) {
                    user.urlFotoThumb = `${this.contextUrl}/img/user.jpg`;
                }
            } else {
                user = {
                    id: conversa.get('fromId'),
                    nome: conversa.get('fromNome'),
                    urlFoto: conversa.get('fromUrlFoto'),
                    urlFotoThumb: conversa.get('fromUrlFotoThumb'),
                    online: conversa.get('fromChatOn'),
                    dataOnline: conversa.get('fromDateOnlineChat'),
                    away: conversa.get('fromChatAway')
                };
                if(!user.urlFoto) {
                    user.urlFoto = `${this.contextUrl}/img/user.jpg`;
                }
                if(!user.urlFotoThumb) {
                    user.urlFotoThumb = `${this.contextUrl}/img/user.jpg`;
                }
            }
        }

        if(!user.online ) {
            if(user.dataOnline) {
                let data = moment(user.dataOnline).toDate();
                let isHoje = moment(data).isSame(new Date(), "day");
                let horaString =  moment(data).format("HH:mm");
                let dataString = moment(data).format("DD/MM/YYYY");
                if(isHoje) {
                    user.status = `Visto por último hoje às ${horaString}`;
                } else {
                    user.status = `Visto por último: ${dataString} às ${horaString}`;
                }
            } else {
                user.status = "offline";
            }
        } else {
            user.status = 'Online';
        }
        user.isGroup = conversa.get('isGroup');
        return new Map(user);
    }

    @action setChaveGoogleMap(chaveGoogleMap) {
        this.chaveGoogleMap = chaveGoogleMap;
    }

    @action forwardMessage(contato, message, isGrupo) {
        this.novaConversa(contato.get('id'), isGrupo).then((conversa) => {
            this.fetchConversa(conversa, message);
        });
    }

    closeDrawerOpened() {
        if(this.drawersOpened && this.drawersOpened.length > 0) {
            let drawer = this.drawersOpened[0];
            this.drawersOpened = this.drawersOpened.splice(1);
            switch(drawer) {
                case DRAWER_NOVA_CONVERSA:
                    contatoStore.closeDrawerNovaConversa();
                    break;
                case DRAWER_INFO_CONTATO:
                    infoContatoStore.closeInfoUsuario();
                    break;
                case DRAWER_CONTATO_STATUS:
                    infoContatoStore.closeChatStatusUser();
                    break;
                case DRAWER_INFO_MESSAGE:
                    infoMessageStore.onCloseDrawerInfoMessage();
                    break;
            }
        }
    }

    addDrawerOpened(drawerName) {
        this.drawersOpened ? this.drawersOpened : [];
        this.removeDrawerOpened(drawerName);
        this.drawersOpened.unshift(drawerName);
    }

    removeDrawerOpened(drawerName) {
        this.drawersOpened ? this.drawersOpened : [];
        for(let i = 0; this.drawersOpened.length > i; i++) {
            if(this.drawersOpened[i] == drawerName) {
                this.drawersOpened = this.drawersOpened.splice(i + 1, 1);
                break;
            }
        }
    }


}

const chatStore = new ChatStore();
export default chatStore;
export {ChatStore};
