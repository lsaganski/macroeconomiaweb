import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import ListConversaHeader from './ListConversaHeader';

export default class ListConversa extends Component {
    render() {
        return(
            <div className="pane pane-list pane-one">
               {this.props.children}
            </div>
        )
    }
}