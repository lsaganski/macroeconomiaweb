import React, {Component} from 'react';

import {observer, inject} from 'mobx-react';
import {black} from 'material-ui/styles/colors';
import MenuItem from 'material-ui/MenuItem';

import ListConversa from '../../components/lists/ListConversa';
import ListConversaHeader from '../../components/lists/ListConversaHeader';
import ListConversaControl from '../../components/lists/ListConversaControl';
import ComeBackLivecom from '../../components/menu-item/ComeBackLivecom';
import NovaConversa from '../../components/menu-item/NovaConversa';
import GroupAdd from 'material-ui/svg-icons/social/group-add';
import Avatar from '../../components/Avatar';
import Search from '../../components/Search';
import Butterbar from '../../components/Butterbar';
import Conversas from './Conversas';
import MenuHeader from '../../components/MenuHeader';

@inject('chatStore', 'userInfoStore', 'contatoStore', 'webSocketStore')
@observer export default class LeftContainer extends Component {
    constructor(props) {
		super(props);
		this.state = {
			open: false,
			anchorEl: null
		}
	}
    onOpenHeaderMenu = (e) => {
        this.setState({open: true, anchorEl: e.currentTarget});
    }
    onCloseHeaderMenu = () => {
        this.setState({open: false, anchorEl: null});
    }
    onSearch(valueSearch) {
        this.props.chatStore.fetchConversas(valueSearch);
    }
    onClickBackLivecom() {
		window.open(`${this.props.chatStore.contextUrl}/pages/mural.htm`, 'lvcm-h');
    }
    onClickNovaConversa() {
        this.props.contatoStore.openDrawerNovaConversa();
    }
    onClickNovoGrupo() {
        this.setState({open: false});
        const chatStore =  this.props.chatStore;
        const contato = chatStore.getUser(chatStore.conversa);
        window.open(`${chatStore.contextUrl}/cadastro/grupo.htm`, 'lvcm-h');
    }
    onClickChangelog() {
        this.onCloseHeaderMenu();
        window.open(`${this.props.chatStore.contextUrl}/changeLogChat.htm`, 'cr-ch');
    }
    onClickRoadmap() {
        this.onCloseHeaderMenu();
        window.open(`${this.props.chatStore.contextUrl}/roadmapChat.htm`, 'cr-ch');
    }
    render() {
        const {userInfoStore, webSocketStore} = this.props;
        const userInfo = userInfoStore.userInfo;
        return(
            <ListConversa>
                <ListConversaHeader>
                    <div className="pane-list-user">
                        <Avatar src={userInfo.get('foto')} small={true}/>
                    </div>
                    <ListConversaControl>
                        <ComeBackLivecom onClick={this.onClickBackLivecom.bind(this)} />
                        <NovaConversa onClick={this.onClickNovaConversa.bind(this)} />
                        <MenuHeader
                            open={this.state.open}
                            onClose={this.onCloseHeaderMenu}
                            onOpen={this.onOpenHeaderMenu}
                            anchorEl={this.state.anchorEl}
                        >
                            <MenuItem
                                primaryText="Novo Grupo"
                                leftIcon={<GroupAdd color={black} />}
                                onClick={this.onClickNovoGrupo.bind(this)}
                            />
                        </MenuHeader>
                    </ListConversaControl>
                </ListConversaHeader>
                {webSocketStore.socketStatusOnline ? null : <Butterbar /> }
                <Search 
                    spinner={this.props.chatStore.isSpinnerSearch} 
                    placeholder="Procurar uma conversa"
                    onSearch={this.onSearch.bind(this)}
                    clearSearch={this.props.chatStore.clearSearch}
                />
                <Conversas />
            </ListConversa>
        );
    }
}