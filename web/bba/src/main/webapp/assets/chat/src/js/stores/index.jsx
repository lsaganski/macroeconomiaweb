// importar stores aqui
import chatStore from './chatStore';
import userInfoStore from './userInfoStore';
import webSocketStore from './webSocketStore';
import contatoStore from './contatoStore';
import infoContatoStore from './infoContatoStore';
import infoMessageStore from './infoMessageStore';

export default {
    chatStore,
    userInfoStore,
    webSocketStore,
    contatoStore,
    infoContatoStore,
    infoMessageStore
}