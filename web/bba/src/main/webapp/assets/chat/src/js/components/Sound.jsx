import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class Sound extends Component {
    componentDidMount() {
        this.play(this.props);
    }
    componentWillReceiveProps(props) {
        this.play(props);
    }
    play(props) {
        if(props.play) {
            $(ReactDOM.findDOMNode(this.refs.playAudio))[0].play();
        } else {
            $(ReactDOM.findDOMNode(this.refs.playAudio))[0].pause();
        }
    }
    render() {
        return (
            <audio 
                ref="playAudio"
                onTimeUpdate={this.props.onTimeUpdate}
                preload="auto"
                src={this.props.src}
                type={this.props.type ? this.props.type : "audio/mpeg"}
                >
            </audio>
        )
    }
}