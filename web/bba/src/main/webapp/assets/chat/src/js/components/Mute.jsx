import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class Mute extends Component {
    render() {
        return(
            <span onClick={this.props.onClick} className="icon icon-meta icon-muted"></span>
        )
    }
}