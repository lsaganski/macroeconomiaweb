import React, {Component} from 'react';
import {observer, inject} from 'mobx-react';

import Search from '../../components/Search';
import ListInfinite from '../../components/lists/ListInfinite';
import EmptyTop from '../../components/EmptyTop';
import ListTitle from '../../components/lists/ListTitle';
import Contato from '../../components/Contato';
import Spinner from '../../components/SpinnerMessage';

const FetchContato = ({show}) => <EmptyTop>Carregando seus contatos aguarde...<Spinner show={true} title="Carregar contatos" /></EmptyTop>;
const NotFoundContato = () => <EmptyTop>Nenhum contato encontrado</EmptyTop>;

@inject('contatoStore')
@observer export default class ListContatos extends Component {
    onClickContato(contato, isGroup) {
        this.props.contatoStore.novaConversa(contato.get('id'), isGroup);
    }
    onSearch(valueSearch) {
        this.props.contatoStore.fetchContatos(valueSearch);
    };
    showNotFoundContato = () => {
        const {usuarios, grupos, isFetchContatos} = this.props.contatoStore;
        return usuarios.size == 0 && grupos.size == 0 && !isFetchContatos
    };
    showSpinnerLoadContato = () => {
        const {isFetchContatos} = this.props.contatoStore;
        return isFetchContatos
    };

    showUsuarios = () => {
        const {usuarios} = this.props.contatoStore;
        return usuarios.size > 0
    };
    showGrupos = () => {
        const {grupos} = this.props.contatoStore;
        return grupos.size > 0
    };

    render() {
        const usuarios = this.props.contatoStore.usuarios;
        const grupos = this.props.contatoStore.grupos;

        let nodeUsuarios = usuarios.map((contato) => {
            return <Contato avatar={contato.get('urlFotoUsuario')} onClick={this.onClickContato.bind(this)} contato={contato} key={`${contato.get('id')}_${Math.random()}`} isGroup={false} />
        });

        let nodeGrupos = grupos.map((contato) => {
            return <Contato avatar={contato.get('urlFotoUsuario')} onClick={this.onClickContato.bind(this)} contato={contato} key={`${contato.get('id')}_${Math.random()}`} isGroup={true} />
        });

        return(
            <span className="drawer-container-left flow-drawer-container">
				<div className="drawer">
					<header className="drawer-header">
						<div className="pane-header drawer-title" style={{padding: '0px', opacity: '1', transform: 'translateX(0px)', backgroundColor: 'transparent'}}>
							<div className="drawer-title-action" style={{textAlign: 'initial', width: '54px'}}>
								<span onClick={this.props.onClickClose} className="icon btn-close-drawer-left icon-back-light"></span>
							</div>
							<span className="drawer-title-body" style={{fontWeight: '500', fontSize: '19px', lineHeight: 'normal'}}>Nova conversa</span>
						</div>
					</header>
                    <Search
                        spinner={this.props.contatoStore.isSpinnerSearch}
                        onSearch={this.onSearch.bind(this)}
                        placeholder="Buscar contatos"
                        clearSearch={this.props.contatoStore.clearSearch}
                        focus={this.props.contatoStore.searchInputFocus}
                    />
                    <div className="drawer-body">
                        <ListInfinite>
                            {this.showSpinnerLoadContato() ? <FetchContato /> : null}
                            {this.showNotFoundContato() ? <NotFoundContato /> : null}
                            {this.showUsuarios() ? <ListTitle title="Usuários">{nodeUsuarios}</ListTitle> : null}
                            {this.showGrupos() ? <ListTitle title="Grupos">{nodeGrupos}</ListTitle> : null}
                        </ListInfinite>
                    </div>
				</div>
			</span>
        )
    }
}