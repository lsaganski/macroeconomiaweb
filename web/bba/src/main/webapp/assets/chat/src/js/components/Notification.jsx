import React, {Component} from 'react';
import ReactDOM from 'react-dom';



export default class Notification extends Component {
	render() {
		return(
			<div className="butterbar-wrapper" style={{display: 'block'}}>
				<div className="butterbar butterbar-notification">
					<div className="butterbar-icon">
						<span className="icon icon-alert icon-alert-notification"></span>
					</div>
					<div className="butterbar-body">
						<div className="butterbar-title">Receba notificações desta conversa</div>
						<div className="butterbar-text">
							<span className="action" onClick={this.props.onClick}>Ativar notificações na área de trabalho</span>
						</div>
					</div>
				</div>
			</div>
		)
	}
}