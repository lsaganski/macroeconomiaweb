import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {observer, inject} from 'mobx-react';

import ListInfinite from '../../components/lists/ListInfinite';
import EmptyTop from '../../components/EmptyTop';
import Spinner from '../../components/SpinnerMessage';
import Conversa from './Conversa';

const NotFoundConversa = () => <EmptyTop>Nenhuma conversa encontrada</EmptyTop>

const FetchConversas = ({show}) => <EmptyTop>Carregando suas conversas aguarde...<Spinner show={show} title="Carregar conversas recentes" /></EmptyTop>



@inject('chatStore', 'userInfoStore')
@observer export default class Conversas extends Component {
    render() {
        const isFetchConversas = this.props.chatStore.isFetchConversas;
        let nodeConversas = this.props.chatStore.conversas.map((conversa, index) => 
            <Conversa key={conversa.get('conversaId')} conversa={conversa}/>
        );

        if(nodeConversas && nodeConversas.size == 0) {
            nodeConversas = <NotFoundConversa />
        }

        return(
            <div className="list-container">
                <ListInfinite>
                    {isFetchConversas ? <FetchConversas show={isFetchConversas} /> : nodeConversas}
                </ListInfinite>
            </div>
        );
    }
}