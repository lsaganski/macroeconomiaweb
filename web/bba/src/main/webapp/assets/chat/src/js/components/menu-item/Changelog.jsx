import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {blue500} from 'material-ui/styles/colors';
import MenuItem from 'material-ui/MenuItem';
import AssignmentIcon from 'material-ui/svg-icons/action/assignment';

export default class Changelog extends Component {
    render() {
        return(
            <MenuItem 
                primaryText="Changelog Chat" 
                leftIcon={<AssignmentIcon color={blue500} />} 
                value="1" onTouchTap={this.props.onClick} />
        )
    }
}