import AttachFile from 'material-ui/svg-icons/editor/attach-file';
import {white} from 'material-ui/styles/colors';

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class Anexar extends Component {
    render() {
        return(
            <div className="menu-item item-anexar">
                <button onClick={this.props.onClick} className=""><AttachFile color={white} /></button>
            </div>
        )
    }
}