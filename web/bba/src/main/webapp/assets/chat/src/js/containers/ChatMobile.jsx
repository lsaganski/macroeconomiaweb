import React, {Component} from 'react';
import {observer, inject} from 'mobx-react';
import Drawer from 'material-ui/Drawer';
import Snackbar from 'material-ui/Snackbar';


import BoxChat from '../components/BoxChat';
import DrawerManager from '../components/Manager';
import LeftContainer from './left/LeftContainer';
import CenterContainer from './center/Container';
import ListContatos from './left/ListContatos';
import DrawerInfoContato from './right/DrawerInfoContato';
import DrawerInfoMessage from './right/DrawerInfoMessage';
import DragAndDrop from './center/DragAndDrop';
import PaneSpinner from '../components/PaneSpinner';
import Sound from '../components/Sound';
import ChatStatusUser from './right/ChatStatusUser';
import ForWardMessage from './modal/ModalContatos';


const styleDrawerActive = {
    width: '100%',
    transform: 'translate(0, 0)',
    boxShadow: 'none',
    position: 'absolute'
}

const styleDrawerInactive = {
    width: '100%',
    boxShadow: 'none',
    transform: 'translate(-100%, 0px)',
    position: 'absolute'
}

@inject('chatStore', 'userInfoStore', 'contatoStore', 'infoContatoStore', 'infoMessageStore')
@observer export default class Chat extends Component {
    onClickCloseDrawerNovaConversa() {
        this.props.contatoStore.closeDrawerNovaConversa();
    }
    isDrawerActive() {
        const {infoContatoStore, infoMessageStore} = this.props;

        return infoContatoStore.drawerInfoActive || infoContatoStore.chatStatusUser || infoMessageStore.drawerInfoMessageActive;
    }
    render() {

        const {chatStore, contatoStore, infoContatoStore, infoMessageStore} = this.props;
        const hasConversaActive = chatStore.hasConversaActive;
        const drawerLeftAnimation =  contatoStore.drawerNovaConversaAtivo ? styleDrawerActive : styleDrawerInactive;
        return(
            <BoxChat drawerActive={this.isDrawerActive()}>
                <Sound
                    src={`${this.props.chatStore.contextUrl}/audios/notify.mp3`}
                    play={this.props.chatStore.playSound}
                />
                {this.props.chatStore.openAnexarArquivo ? <DragAndDrop /> : null }
                <DrawerManager>
                    <span className="pane pane-two" ></span>
                    <span className="pane pane-three">
                        {infoContatoStore.chatStatusUser && infoContatoStore.drawerInfoActive ? <ChatStatusUser /> : null}
                        {infoContatoStore.drawerInfoActive && !infoContatoStore.chatStatusUser ? <DrawerInfoContato /> : null }
                        {infoMessageStore.drawerInfoMessageActive ? <DrawerInfoMessage /> : null}
                    </span>
                </DrawerManager>
                {hasConversaActive ? <CenterContainer /> : <PaneSpinner />}
                <Snackbar
                    open={chatStore.snackbar.get('show')}
                    message={chatStore.snackbar.get('message')}
                />
                {this.props.chatStore.messageForward != null ? <ForWardMessage /> : null}
            </BoxChat>
        )
    }
}

