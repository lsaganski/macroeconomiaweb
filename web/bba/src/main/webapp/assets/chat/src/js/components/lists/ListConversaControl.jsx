import React, {Component} from 'react';
import ReactDOM from 'react-dom';


class ListConversaControl extends Component {
    render() {
        return(
            <div className="pane-list-controls">
                <div className="menu menu-horizontal">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default ListConversaControl;