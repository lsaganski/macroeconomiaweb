import React, {Component} from 'react';
import Checkbox from 'material-ui/Checkbox';
import {observer, inject} from 'mobx-react';
import Avatar from '../../components/Avatar';
import MenuHeader from '../../components/MenuHeader';
import InformacoesAcesso from '../../components/menu-item/InformacoesAcesso';

import {grey700, blue500} from 'material-ui/styles/colors';
import MenuItem from 'material-ui/MenuItem';
import AssessmentIcon from 'material-ui/svg-icons/action/assessment';

const styles = {
  block: {
    maxWidth: 250,
  },
  checkbox: {
    marginBottom: 16,
  },
};


const getInfoContact = (string) => {
	let stingFormat;
	if(string && string != ''){
		stingFormat = string;
	}else{
		stingFormat = '--';
	}

	return stingFormat;
}



const DrawerSectionTitle = ({label}) => {
	return (
		<div className="drawer-section-title  title-underlined">
			<div className="col-main">{label}</div>
		</div>
	)
}

const DrawerSectionBody = ({label}) => {
	return(
		<div className="drawer-section-body">
			<span className="textfield-static selectable-text" dir="auto">
				{label}
			</span>
		</div>
	)
}

const DrawerSectionBodyCheck = ({checked, onCheck, label}) => {
	return(
		<div className="drawer-section-body">
			<span className="textfield-static selectable-text" dir="auto">
				<Checkbox
					onCheck={onCheck}
					checked={checked}
      				label={label}
      				labelPosition="left"
      				style={styles.checkbox}
    			/>
			</span>
		</div>
	)
}

@inject('infoContatoStore', 'chatStore') 
@observer export default class DadosUsuario extends Component {
	constructor(props) {
		super(props);
		const conversa = this.props.chatStore.conversa;
		const notifications = conversa.get('notifications');
		this.state = {
			notifications: notifications,
			open: false,
			anchorEl: null
		}
	}
	componentDidMount() {
        $('.avatar-fancybox').fancybox({
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			maxHeight	: '90%',
			loop		: false,
			helpers     : {
	            overlay : {
	                closeClick: true,
	                locked: false
	            }
	        }
		});
    }
	onChangeSoundNotification(e) {
		let notifications = this.state.notifications;
      	const status = !notifications.get('sound');
      	notifications = notifications.set('sound', status);
      	this.setState({notifications: notifications});
      	const conversa = this.props.chatStore.conversa;
      	this.props.infoContatoStore.changeStatusSoundNotification(status, conversa);
	}
	onChangePushNotification(e) {
		let notifications = this.state.notifications;
      	const status = !notifications.get('sendPush');
      	notifications = notifications.set('sendPush', status);
      	this.setState({notifications: notifications});
      	const conversa = this.props.chatStore.conversa;
      	this.props.infoContatoStore.changeStatusPushNotification(status, conversa);
	}
	onChangeNotification(e) {
		let notifications = this.state.notifications;
      	const status = !notifications.get('notification');
      	notifications = notifications.set('notification', status);
      	this.setState({notifications: notifications});
      	const conversa = this.props.chatStore.conversa;
      	this.props.infoContatoStore.changeStatusBrowerNotification(status, conversa);
	}
	onClickOpenDadosUsuarioLivecom() {
		const contato = this.props.infoContatoStore.contato;
		const chatStore =  this.props.chatStore;
		window.open(`${chatStore.contextUrl}/usuario.htm?id=${contato.get('id')}`, 'lvcm-h');
	}
	onClickDeleteConversa() {
		const conversa = this.props.chatStore.conversa;
		this.props.chatStore.onDeleteConversa(conversa, conversa.get('id'));
	}
	onClickClose() {
		this.props.infoContatoStore.closeInfoUsuario();
	}
	onOpenHeaderMenu(e) {
		this.setState({open: true, anchorEl: e.currentTarget});
	}
	onCloseHeaderMenu() {
		this.setState({open: false, anchorEl: null});
	}
    onClickInformacoesAcesso() {
        this.props.infoContatoStore.openChatStatusUser(this.props.infoContatoStore.contato);
    }
    onClickDevChatMessage() {
	    this.props.chatStore.openDevChatMessage();
    }
	render() {
		let contato = this.props.infoContatoStore.contato;
		let nome = getInfoContact(contato.get('nome'));
		let email = getInfoContact(contato.get('email'));
		let login = getInfoContact(contato.get('login'));
		let funcao = getInfoContact(contato.get('funcao'));

		let telefone = "--";
		if(contato.get('celular')){
			let ddd = contato.get('celular').substr(0, 2);
            let n = contato.get('celular').substr(2, contato.get('celular').length);
            let formated = "("+ddd+") "+ n;
            telefone = formated;
        }else if(contato.get('fixo')){
            let ddd = contato.get('fixo').substr(0, 2);
            let n = contato.get('fixo').substr(2, contato.get('fixo').length);
            let formated = "("+ddd+") "+n;
            telefone = formated;
        }
		return (
			<div className="drawer drawer-info">
				<header className="pane-header">
					<div className="header-close">
						<button onClick={this.onClickClose.bind(this)}>
							<span className="icon icon-x"></span>
						</button>
					</div>
					<div className="header-body">
						<div className="header-main">
							<div className="header-title">Informações do Contato</div>
						</div>
					</div>
					<MenuHeader
						open={this.state.open}
						onClose={this.onCloseHeaderMenu.bind(this)}
						onOpen={this.onOpenHeaderMenu.bind(this)}
						anchorEl={this.state.anchorEl}
                    >
						<InformacoesAcesso onClick={this.onClickInformacoesAcesso.bind(this)} />
                        <MenuItem
                            primaryText="Dev Message"
                            leftIcon={<AssessmentIcon color={blue500} />}
                            value="1" onClick={this.onClickDevChatMessage.bind(this)}
                        />
					</MenuHeader>
				</header>
				<div className="drawer-body drawer-editable">
					<div className="drawer-scale drawer-section-photo drawer-section">
						<a className="avatar-fancybox" href={contato.get('urlFotoUsuarioThumb')}>
							<Avatar large={true} src={contato.get('urlFotoUsuarioThumb')} />
						</a>
					</div>
					<div className="animate-enter2 drawer-section well">
						<DrawerSectionTitle label="Dados de Cadastro" />
						<DrawerSectionBody label={`Nome: ${nome}`} />
						<DrawerSectionBody label={`Login: ${login}`} />
						<DrawerSectionBody label={`E-mail: ${email}`} />
						<DrawerSectionBody label={`Telefone: ${telefone}`} />
						<DrawerSectionBody label={`Cargo: ${funcao}`} />
					</div>
					<div className="animate-enter2 drawer-section well">
						<DrawerSectionTitle label="Notificações" />
						<DrawerSectionBodyCheck 
							label="Som de notificação" 
							checked={this.state.notifications.get('sound')}
							onCheck={this.onChangeSoundNotification.bind(this)}
						/>
						<DrawerSectionBodyCheck 
							label="Notificação" 
							checked={this.state.notifications.get('notification')}
							onCheck={this.onChangeNotification.bind(this)}
						/>
					</div>
					<div className="animate-enter drawer-section-action drawer-section">
						<div className="btn">
							<a onClick={this.onClickOpenDadosUsuarioLivecom.bind(this)}>Ver no Livecom</a>
						</div>
					</div>
					<div className="animate-enter drawer-section-action last drawer-section" onClick={this.onClickDeleteConversa.bind(this)}>
						<div className="btn btn-danger">Apagar conversa</div>
					</div>
				</div>
			</div>
		);
	}
}