import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Dropzone from 'react-dropzone'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import {observer, inject} from 'mobx-react';

import * as FileUtils from '../../utils/FileUtils';


const modalCustomStyle = {
  width: '70%',
  maxWidth: 'none',
  boxShadow: '2px 2px 8px #aaa'
};

@inject('chatStore')
@observer export default class DragAndDrop extends Component {
	constructor(props) {
		super(props);
		this.state = this.resetStates();
	}
	resetStates() {
		return {
			dragZoneClass: 'drag-drop-zone',
			imgHolderClass: 'drag-drop-zone-img-preview hide',
			file: {},
			btnDisabled: true,
			btnSendStyle: { opacity: '.5' }
		}
	}
	sendFile() {
		const file = this.state.file;
		this.props.chatStore.sendFile(this.props.chatStore.conversa, file);
    	this.setState(this.resetStates());
    }
	handleOpen(){
		this.setState({open: true});
	}
	handleClose(){
		this.setState(this.resetStates());
		this.props.chatStore.onCloseAnexarArquivos();
	}
	removeFile(){
		this.setState(this.resetStates());
	}
	onDrag(){
		this.setState({dragZoneClass: 'drag-drop-zone dragging'});
	}
	onDrop(file) {
		let name = file[0].name;
		let ext = name.substring(name.lastIndexOf('.') + 1, name.length).toLowerCase();
		if(FileUtils.FILE_EXTENSIONS.image[ext] || FileUtils.FILE_EXTENSIONS.video[ext] || FileUtils.FILE_EXTENSIONS.audio[ext] || FileUtils.FILE_EXTENSIONS.docs[ext] || FileUtils.FILE_EXTENSIONS.programs[ext] || FileUtils.FILE_EXTENSIONS.compressed[ext] || FileUtils.FILE_EXTENSIONS.executable[ext]){
			this.setState({dragZoneClass: 'drag-drop-zone hide', imgHolderClass: 'drag-drop-zone-img-preview', btnDisabled: false, btnSendStyle: { opacity: 1 }, file: file[0]});
		}else{
			this.setState(this.resetStates());
			this.props.chatStore.onCloseAnexarArquivos();
			swal({
				title: "Ops",
  				text: "Esse arquivo não é suportado para fazer upload",
  				type: "error",
  				showCancelButton: false,
  				confirmButtonColor: "#3085d6",
  				confirmButtonText: "OK",
  				closeOnConfirm: true,
			});
		}
    }
	render() {
		let file = this.state.file;
		let imgClass = 'img-responsive';

		let image;

		if(typeof file.name != 'undefined' && file.name != ""){
			let fileExt = file.name.substr(file.name.lastIndexOf('.') + 1, file.name.length).toLowerCase();
			if(FileUtils.FILE_EXTENSIONS.image[fileExt]){
				image = <figure>
		        	    	<img className={imgClass} src={file.preview} alt={file.name} />
		            		<figcaption>{file.name}</figcaption>
		            	</figure>;
			}else{
				image = <div className="icon-file">
							<i className={`${imgClass} file-extensions ${fileExt}`}></i>
		                    <span className="file-name">{file.name}</span>
		                    <span className="file-ext">{fileExt}</span>
		                </div>;
			}
		}
		
		return(
			<div>
		        <Dialog title="Anexar imagem" modal={false} open={this.props.chatStore.openAnexarArquivo} onRequestClose={this.handleClose.bind(this)} contentStyle={modalCustomStyle} actions={[
		        	<FlatButton
				      	backgroundColor="#fcfcfc"
				      	hoverColor="#e6e6e6"
				      	labelStyle={{ color: '#333' }}
				        label="Cancelar"
				        style={{ marginRight: 12 }}
				        onTouchTap={this.handleClose.bind(this)}
					/>,
			      	<FlatButton
				      	backgroundColor="#3085D6"
				      	hoverColor="#286090"
				      	labelStyle={{ color: '#fff' }}
				      	style={this.state.btnSendStyle}
				        label="Enviar"
				        disabled={this.state.btnDisabled}
				        onTouchTap={this.sendFile.bind(this)}
			      	/>
		        ]}>
		        	<div className="drag-drop-container">
		        		<div className="drag-drop-header">
		        			<h2>Upload</h2>
		        		</div>
			          	<Dropzone className={this.state.dragZoneClass} onDrop={this.onDrop.bind(this)} onDrag={this.onDrag.bind(this)} multiple={false}>
			              	<p className="">Arraste o arquivo aqui, ou</p>
		          			<a className="btn btn-primary">Clique aqui</a>
			            </Dropzone>
			            <div className={this.state.imgHolderClass}>
			            	{ image }
			            	<IconButton className="btn-remove" onTouchTap={this.removeFile.bind(this)} tooltip="Remover imagem">
								<CloseIcon />
							</IconButton>
			            </div>
			        </div>
		        </Dialog>
			</div>
		)
	}
}