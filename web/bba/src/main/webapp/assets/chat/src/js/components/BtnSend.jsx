import React, {Component} from 'react';

export default class SendMessage extends Component {
    render() {
        return(
            <button className="icon btn-icon icon-send send-container" onClick={this.props.onClick}></button>
        )
    }
}