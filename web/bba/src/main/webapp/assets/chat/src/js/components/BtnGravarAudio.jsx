import React, {Component} from 'react';

export default class BtnGravarAudio extends Component {
    render() {
        return(
            <button className="icon btn-icon icon-ptt send-container" onClick={this.props.onClick}></button>
        );
    }
}