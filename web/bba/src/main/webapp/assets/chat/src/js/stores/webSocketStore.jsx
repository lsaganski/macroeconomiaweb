import {observable, action, inject, computed} from 'mobx';
import userInfoStore from './userInfoStore';
import chatStore from './chatStore';
var debug = require('debug')('chat:WebSocketStore');


export const LOGIN_RESPONSE_CODE = 'loginResponse';
export const USER_STATUS_CHANGED = 'userStatus';
export const RECEIVED_MESSAGE = 'msg';
export const MESSAGE_REACH_DESTINATION = 'msg2C';
export const MESSAGE_WAS_READ = 'msg2A';
export const CONVERSATION_WAS_READ = 'c2A';
export const CONVERSATION_WAS_RECEIVED = 'c2C';
export const SERVER_RECEIVED_MESSAGE = 'msg1C';
export const KEEP_ALIVE = 'keepAlive';
export const KEEP_ALIVE_OK = 'keepAliveOk';
export const TYPING_STATUS = 'typing';
export const LOGIN = 'login';
export const SEND_MESSAGE = 'msg';
export const SEND_MESSAGE_CUSTOM = 'msgCustom';
export const FORWARD_MESSAGE = "forward";
export const ACCESS_DENIED = 'accessDenied';


export const KEY_SOCKET_LOG = "socket_log";

class WebSocketStore {
    @observable socket = null;
    @observable isSocketOn = true;
    @observable uri_socket = null;
    waitSocket = false;

    socketLogin() {
        const userInfo = userInfoStore.userInfo;
        this.send({
            "code": LOGIN,
            "user":userInfo.get('login'),
            "pass":userInfo.get('password'),
            "so":"web"
        });
    }

    sendFile(file) {
        this.socket.binaryType = 'blob';
        this.socket.send(JSON.stringify({
            code: 'fileBlob',
            file: file
        }));
    }

    @action connect(uri) {
        if(this.waitSocket == true) {
            return;
        }
        this.waitSocket = true;
        this.socket = null;
        this.uri_socket = uri;
        let conn = new WebSocket(uri);
        conn.onopen = () => {
            this.socket = conn;
            this.isSocketOn = true;
            this.socketLogin();
            
        };

        conn.onerror = () => {
            conn.onclose();
        };

        conn.onclose = () => {
            this.waitSocket = false;
            this.isSocketOn = false;
            this.socket = null;
            setTimeout(() => {
                this.connect (uri);
            }, 5000);
            
        };

        conn.onmessage = (evt) => {
            try {
                this.onReceiveMessage(JSON.parse(evt.data));
            } catch(e) {

            }
        };
    }

    @computed get status() {
        return this.isSocketOn;
    }

    @computed get socketStatusOnline() {
        return this.isSocketOn;
    }

    sendMessage(identifier, message, conversa) {
        const userInfo = userInfoStore.userInfo;
        let json = {
            "code": SEND_MESSAGE,
            "identifier":identifier,
            "cId":conversa.get('id'),
            "from":userInfo.get('id'),
            "msg":message
        };
        if(conversa.get('isGroup')) {
            json.gId = conversa.get('grupoId');
        }
        this.send(json);
    }

    sendMessageCustom(identifier, message, conversa) {
        const userInfo = userInfoStore.userInfo;
        let json = {
            "identifier":identifier,
            "cId":conversa.get('id'),
            "from":userInfo.get('id'),
            "console": 1
        };
        if(conversa.get('isGroup')) {
            json.gId = conversa.get('grupoId');
        }
        let jsonSend = Object.assign({}, json, message);

        this.send(jsonSend);
    }

    onReceiveMessage(message) {
        switch (message.code) {
            case LOGIN_RESPONSE_CODE:
                chatStore.sendMessageReachDestination();
                break;
            case USER_STATUS_CHANGED: {
                chatStore.onUserStatusChanged(message);
                break;
            }
            case RECEIVED_MESSAGE: {
                chatStore.onReceivedMessage(message);
                break;
            }
            case KEEP_ALIVE: {
                this.sendKeepAlive(message);
                break;
            }
            case TYPING_STATUS: {
                chatStore.onTypingStatus(message);
                break;
            }
            case SERVER_RECEIVED_MESSAGE: {
                chatStore.onServerReceivedMessage(message);
                this.sendCallback(message);
                break;
            }
            case ACCESS_DENIED: {
                chatStore.removeConversa(message.cId, message.code, message.msg);
                break;
            }
            case CONVERSATION_WAS_READ:
            case MESSAGE_WAS_READ:
            case MESSAGE_REACH_DESTINATION:
            case CONVERSATION_WAS_RECEIVED:
                chatStore.onChangeStatusMessage(message);
                this.sendCallback(message);
                break;
            default:
                debug(message);
        }
    }
    send(json) {
        if(this.socket != null) {
            return this.socket.send(JSON.stringify(json));
        }
    }
    sendCallback(message) {
        let messageCallback = message;
        messageCallback.code = messageCallback.code + "C";
        this.send(messageCallback);
    }
    /**
     * Responde para o socket que essa conexão ainda esta ativa
     * e o usuário esta utilizando ela
     */
    sendKeepAlive(message) {
        message.code = KEEP_ALIVE_OK;
        message.fromUserID = message.userID;
        delete message.userID;
        this.send(message);
    }

    /**
     * Envia uma msg pelo socket avisando que o usuário recebeu a mensagem
     * mas não leu ainda.
     */
    messageReachDestination(rawMessage) {
        const userInfo = userInfoStore.userInfo;
        let json = {
            "code": MESSAGE_REACH_DESTINATION,
            "msgId": rawMessage.msgId,
            "cId": rawMessage.cId,
            "from": rawMessage.from,
            "to": userInfo.get('id'),
	    };
        if(rawMessage.gId) {
            json.gId = rawMessage.gId;
        }
        this.send(json);
    }

    /**
     * Envia que o usuário ja leu a mensagem
     */
    messageWasRead(rawMessage) {
        const userInfo = userInfoStore.userInfo;
        let json = {
            "code": MESSAGE_WAS_READ,
            "msgId": rawMessage.msgId,
            "from": rawMessage.from,
            "to": userInfo.get('id'),
            "cId": rawMessage.cId
	    };
        if(rawMessage.gId) {
            json.gId = rawMessage.gId;
        }
        this.send(json);
    }

    forwardMessage(conversa, message, identifier) {
        const userInfo = userInfoStore.userInfo;
        let json = {
            "code": FORWARD_MESSAGE,
            "cId": conversa.get('id'),
            "msgs": [{
                "msgId" : message.get('id'),
                "identifier" : identifier
            }],
            "from": userInfo.get('id'),
        };
        this.send(json);
    }

    /**
     * Envia para o socket que o usuário leu a conversa
     */
    conversationWasRead(conversa) {
        const userInfo = userInfoStore.userInfo;
        let json = {
            "code": CONVERSATION_WAS_READ,
            "cId": conversa.get('id'),
            "from": userInfo.get('id')
	    };
        if(conversa.get('isGroup')) {
            json.gId = conversa.get('grupoId');
        } 
        this.send(json);
    }

    sendTypingStatus(conversa) {
        const userInfo = userInfoStore.userInfo;
        this.send({
            "code": TYPING_STATUS,
            "cId": conversa.get('id'),
            "from": userInfo.get('id'),
            "fromName": userInfo.get('nome'),
            "typing":true
        });
    }

    sendAway(status) {
        const userInfo = userInfoStore.userInfo;
        this.send({
            "code":USER_STATUS_CHANGED,
            "away":status,
            "from":userInfo.get('id')
        });
    }
}

const webSocketStore = new WebSocketStore();
export default webSocketStore;
export {WebSocketStore};