import IconButton from 'material-ui/IconButton/IconButton';
import Notification from 'material-ui/svg-icons/social/notifications-active';
import {white} from 'material-ui/styles/colors';

import React, {Component} from 'react';


export default class Notificacao extends Component {
    render() {
        return(
            <div className="menu-item">
                <IconButton
                    onTouchTap={this.props.onClick}
                    tooltip="Notificação">
                    <Notification color={white} />
                </IconButton>
            </div>
        )
    }
}