import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class BoxChat extends Component {
    render() {
        let {drawerActive} = this.props;
        let classApp = drawerActive ? "app three" : "app two";
        return(
            <div id="app">
                <div className="app-wrapper app-wrapper-web app-wrapper-main">
                    <div className={classApp}>
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}