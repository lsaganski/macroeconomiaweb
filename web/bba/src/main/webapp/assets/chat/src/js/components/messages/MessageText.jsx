import React, {Component} from 'react';
import ReactDOM from 'react-dom';


export default class MessageText extends React.Component {
	render() {
		return(
            <div className="message-text">
                <span className="emojitext selectable-text" dir="ltr" dangerouslySetInnerHTML={{__html: this.props.text}}></span>
            </div>
		);
	}
}
