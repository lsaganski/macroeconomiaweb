import React, {Component} from 'react';
import {observer, inject} from 'mobx-react';
import Snackbar from 'material-ui/Snackbar';

import BoxChat from '../components/BoxChat';
import DrawerManager from '../components/Manager';
import LeftContainer from './left/LeftContainer';
import CenterContainer from './center/Container';
import DrawerInfoContato from './right/DrawerInfoContato';
import DrawerInfoMessage from './right/DrawerInfoMessage';
import DragAndDrop from './center/DragAndDrop';
import PaneIntro from '../components/PaneIntro';
import Sound from '../components/Sound';
import ChatMessageDev from './right/ChatMessageDev';

// modals
import ModalContatos from './modal/ModalContatos'
import ModalInfoAcesso from './modal/ModalInfoAcesso';
import ModalNotification from './modal/ModalNotification';
import ModalDadosMensagem from './modal/ModalDadosMensagem';


const styleDrawerActive = {
    width: '100%',
    transform: 'translate(0, 0)',
    boxShadow: 'none',
    position: 'absolute'
}

const styleDrawerInactive = {
    width: '100%',
    boxShadow: 'none',
    transform: 'translate(-100%, 0px)',
    position: 'absolute'
}

@inject('chatStore', 'userInfoStore', 'contatoStore', 'infoContatoStore', 'infoMessageStore')
@observer export default class Chat extends Component {
    onClickCloseDrawerNovaConversa() {
        this.props.contatoStore.closeDrawerNovaConversa();
    }
    isDrawerActive() {
        return false;
    }
    forwardMessage = (contato, isGrupo) => {
        let message = this.props.chatStore.messageForward;
        this.props.chatStore.forwardMessage(contato, message, isGrupo);
    }
    novaConversa = (contato, isGrupo) => {
        this.props.contatoStore.novaConversa(contato.get('id'), isGrupo);
    }
    render() {

        const {chatStore, contatoStore, infoContatoStore, infoMessageStore} = this.props;
        const hasConversaActive = chatStore.hasConversaActive;
        const openModalContatos = chatStore.messageForward != null || contatoStore.drawerNovaConversaAtivo;

        const onClickContato = chatStore.messageForward != null ? this.forwardMessage : this.novaConversa;
        const titleModalContatos = chatStore.messageForward != null ? "Encaminhar Mensagem" : "Nova Conversa";
        return(
            <BoxChat drawerActive={this.isDrawerActive()}>
                <Sound 
                    src={`${this.props.chatStore.contextUrl}/audios/notify.mp3`} 
                    play={this.props.chatStore.playSound} 
                />
                {this.props.chatStore.openAnexarArquivo ? <DragAndDrop /> : null } 
                <DrawerManager>
                    <span className="pane pane-one"></span>
                    <span className="pane pane-two" ></span>
                    <span className="pane pane-three">
                        {infoMessageStore.drawerInfoMessageActive ? <DrawerInfoMessage /> : null}
                    </span>
                </DrawerManager>
                <LeftContainer />
                {hasConversaActive ? <CenterContainer /> : <PaneIntro />}
                <Snackbar
                    open={chatStore.snackbar.get('show')}
                    message={chatStore.snackbar.get('message')}
                />
                {hasConversaActive ? <ModalNotification conversa={chatStore.conversa} /> : null}
                {openModalContatos ? <ModalContatos onClickContato={onClickContato} title={titleModalContatos} /> : null}
                {infoContatoStore.chatStatusUser ? <ModalInfoAcesso /> : null}
                {infoMessageStore.drawerInfoMessageActive ? <ModalDadosMensagem /> : null}
            </BoxChat>
        )
    }
}

