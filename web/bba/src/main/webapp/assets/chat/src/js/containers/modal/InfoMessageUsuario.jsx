import React, {Component} from 'react';
import ReactDOM from 'react-dom';


export default class InfoMessageUsuario extends Component {
	render() {
		const message = this.props.message;
		return (
			<div className="drawer-body drawer-editable">
				<div className="animate-enter2 drawer-section well">
					<div className="drawer-section-title title-underlined">
						<div className="col-main"><span className="icon icon-msg-dblcheck-ack"></span> Lida</div>
					</div>
					<div className="drawer-section-body">
						<span className="textfield-static selectable-text" dir="auto">
							{message.get('dataLida') ? message.get('dataLida') : '-'}
						</span>
					</div>
				</div>
				<div className="animate-enter2 drawer-section well">
					<div className="drawer-section-title title-underlined">
						<div className="col-main"><span className="icon icon-msg-dblcheck"></span> Entregue</div>
					</div>
					<div className="drawer-section-body">
						<span className="textfield-static selectable-text" dir="auto">
							{message.get('dataEntregue') ? message.get('dataEntregue') : '-'}
						</span>
					</div>
				</div>
				{
					message.get('dataPush')
					?
						<div className="animate-enter2 drawer-section well">
							<div className="drawer-section-title title-underlined">
								<div className="col-main"><span className="icon icon-msg-check"></span> Push</div>
							</div>
							<div className="drawer-section-body">
								<span className="textfield-static selectable-text" dir="auto">
									Push enviado às {message.get('dataPush')}	
								</span>
							</div>
						</div>
					:
						null
				}
			</div>
		);
	}
}