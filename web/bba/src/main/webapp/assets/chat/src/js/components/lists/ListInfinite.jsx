import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {List} from 'material-ui/List';


let listStyle = {pointerEvents: 'auto'};

class InfinitList extends Component {
	render() {	
		return(
			<div className="infinite-list chatlist">
				<div className="infinite-list-viewport" style={listStyle}>
					<List style={{paddingTop: '0px'}}>
						{this.props.children}
					</List>
				</div>
			</div>
		);
	}
}

export default InfinitList;