import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class Badge extends Component {
    render() {
        if(this.props.badge > 0) {
            return <span className="icon-meta unread-count" style={{transform: 'scaleX(1) scaleY(1)', opacity : '1'}}>{this.props.badge}</span>
        } else {
            return <span></span>
        }
    }
}