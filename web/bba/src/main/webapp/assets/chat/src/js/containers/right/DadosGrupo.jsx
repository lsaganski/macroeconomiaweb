import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {observer, inject} from 'mobx-react';
import Checkbox from 'material-ui/Checkbox';
import ListInfinite from '../../components/lists/ListInfinite';
import Contato from '../../components/Contato';
import Avatar from '../../components/Avatar';


const styles = {
	toggle: {
		marginBottom: 16,
	},
};

const DrawerSectionBodyCheck = ({checked, onCheck, label}) => {
	return(
		<div className="drawer-section-body">
			<span className="textfield-static selectable-text" dir="auto">
				<Checkbox
					onCheck={onCheck}
					checked={checked}
      				label={label}
      				labelPosition="left"
      				style={styles.checkbox}
    			/>
			</span>
		</div>
	)
}

const DrawerSectionTitle = ({label}) => {
	return (
		<div className="drawer-section-title  title-underlined">
			<div className="col-main">{label}</div>
		</div>
	)
}

@inject('infoContatoStore', 'chatStore') 
@observer export default class DadosGrupo extends Component {
	constructor(props) {
		super(props);
		const conversa = this.props.chatStore.conversa;
		const notifications = conversa.get('notifications');
		this.state = {notifications: notifications}
	}
	componentDidMount() {
		$('.avatar-fancybox').fancybox({
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			maxHeight	: '90%',
			loop		: false,
			helpers     : {
	            overlay : {
	                closeClick: true,
	                locked: false
	            }
	        }
		});

    }
	onClickClose() {
		this.props.infoContatoStore.closeInfoUsuario();
	}
	onChangeSoundNotification(e) {
		let notifications = this.state.notifications;
      	const status = !notifications.get('sound');
      	notifications = notifications.set('sound', status);
      	this.setState({notifications: notifications});
      	const conversa = this.props.chatStore.conversa;
      	this.props.infoContatoStore.changeStatusSoundNotification(status, conversa);
	}
	onChangePushNotification(e) {
		let notifications = this.state.notifications;
      	const status = !notifications.get('sendPush');
      	notifications = notifications.set('sendPush', status);
      	this.setState({notifications: notifications});
      	const conversa = this.props.chatStore.conversa;
      	this.props.infoContatoStore.changeStatusPushNotification(status, conversa);
	}
	onChangeNotification(e) {
		let notifications = this.state.notifications;
      	const status = !notifications.get('notification');
      	notifications = notifications.set('notification', status);
      	this.setState({notifications: notifications});
      	const conversa = this.props.chatStore.conversa;
      	this.props.infoContatoStore.changeStatusBrowerNotification(status, conversa);
	}
	onClickVerLivecom() {
		const chatStore =  this.props.chatStore;
		window.open(`${chatStore.contextUrl}/grupo.htm?id=${chatStore.conversa.get('grupoId')}`, 'lvcm-h');
	}
	onClickSair() {
		const chatStore =  this.props.chatStore;
		const {conversa} = chatStore;
		chatStore.sairGrupo(conversa);
	}
	render() {
		const {infoContatoStore, chatStore} = this.props;
		const conversa = chatStore.conversa;
		let fotoGrupo = conversa.get('fotoGrupo') ? conversa.get('fotoGrupo').get('url') : `${SERVER_URL}/img/grupo.jpg`;
		return (
			<div className="drawer drawer-info">
				<header className="pane-header">
					<div className="header-close">
						<button onClick={this.onClickClose.bind(this)}>
							<span className="icon icon-x"></span>
						</button>
					</div>
					<div className="header-body">
						<div className="header-main">
							<div className="header-title">
								Informações do Grupo
							</div>
						</div>
					</div>
				</header>
				<div className="drawer-body drawer-editable">
					<div className="drawer-scale drawer-section-photo drawer-section">
						<a className="avatar-fancybox" href={fotoGrupo}>
							<Avatar large={true} src={fotoGrupo} />
						</a>
					</div>
					<div className="animate-enter2 drawer-section well">
						<DrawerSectionTitle label="Nome do grupo" />
						<div className="drawer-section-body">
							<span className="textfield-static selectable-text" dir="auto">
								{conversa.get('nomeGrupo')}
							</span>
						</div>
					</div>
					<div className="animate-enter2 drawer-section well">
						<div className="drawer-section-title title-underlined">
							<div className="col-main">Participantes</div>
							<div className="col-side">{infoContatoStore.usersGrupo.size}</div>
						</div>
						<div className="drawer-section-body">
							<span className="textfield-static selectable-text" dir="auto">
								<ListInfinite>
									{infoContatoStore.usersGrupo.map(u => <Contato key={u.get('id')} avatar={u.get('urlFotoUsuario')} contato={u} />)}
								</ListInfinite>
							</span>
						</div>
					</div>
					<div className="animate-enter2 drawer-section well">
						<DrawerSectionTitle label="Notificações" />
						<DrawerSectionBodyCheck 
							label="Som de notificação" 
							checked={this.state.notifications.get('sound')}
							onCheck={this.onChangeSoundNotification.bind(this)}
						/>
						<DrawerSectionBodyCheck 
							label="Notificação" 
							checked={this.state.notifications.get('notification')}
							onCheck={this.onChangeNotification.bind(this)}
						/>
					</div>
					<div className="animate-enter drawer-section-action drawer-section">
						<div className="btn">
							<a onClick={this.onClickVerLivecom.bind(this)}>Ver no Livecom</a>
						</div>
					</div>
					<div className="animate-enter drawer-section-action last drawer-section" onClick={this.onClickSair.bind(this)}>
						<div className="btn btn-danger">Sair do Grupo</div>
					</div>
				</div>
			</div>
		);
	}
}