package br.infra.livetouch.dialect;

import java.util.List;

import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.SQLServerDialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.Type;

public class ConvertSqlFunction extends SQLFunctionTemplate{
	
	public ConvertSqlFunction(Type type, String template) {
		super(type, template);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public String render(Type argumentType, List args, SessionFactoryImplementor factory) {
		if(args.size() >= 1) {
			Dialect dialect = factory.getDialect();
			if(dialect instanceof SQLServerDialect || dialect instanceof OracleDialect) {
				String convert = (String) args.get(0);
				args.remove(0);
				args.add(0, convert);
			}
			
		}
		return super.render(argumentType, args, factory);
	}

}
