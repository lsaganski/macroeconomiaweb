package br.infra.livetouch.dialect;

import java.sql.Types;

import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.type.StandardBasicTypes;

public class OracleDialect extends org.hibernate.dialect.Oracle10gDialect {

	/**
	 * Initializes a new instance of the {@link Oracle10gDialect} class.
	 */
	public OracleDialect() {
		super();
		registerColumnType(Types.BOOLEAN, "smallint");
		registerFunction("DATE_FORMAT", new DateFormatSqlFunction(StandardBasicTypes.STRING, "to_char(?1, ?2)"));
	}
}
