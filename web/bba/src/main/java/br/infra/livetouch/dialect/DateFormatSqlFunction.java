package br.infra.livetouch.dialect;

import java.util.List;

import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.SQLServerDialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.Type;

public class DateFormatSqlFunction extends SQLFunctionTemplate{
	
	public DateFormatSqlFunction(Type type, String template) {
		super(type, template);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public String render(Type argumentType, List args, SessionFactoryImplementor factory) {
		if(args.size() > 1) {
			Dialect dialect = factory.getDialect();
			if(dialect instanceof SQLServerDialect || dialect instanceof OracleDialect) {
				String dateFormat = (String) args.get(1);
				dateFormat = dateFormat.contains("%H hrs") ? dateFormat.replace("%H hrs", "HH \"hrs\"") :dateFormat;
				dateFormat = dateFormat.contains("%H") ? dateFormat.replace("%H", "HH") :dateFormat;
				dateFormat = dateFormat.contains("%d") ? dateFormat.replace("%d", "dd") :dateFormat;
				dateFormat = dateFormat.contains("%m") ? dateFormat.replace("%m", "MM") :dateFormat;
				dateFormat = dateFormat.contains("%M") ? dateFormat.replace("%M", "MMMM") :dateFormat;
				dateFormat = dateFormat.contains("%Y") ? dateFormat.replaceAll("%Y", "yyyy") :dateFormat;
				args.remove(1);
				args.add(1, dateFormat);
			}
			
		}
		return super.render(argumentType, args, factory);
	}

}
