package br.infra.livetouch.dialect;

import java.sql.Types;

import org.hibernate.type.StandardBasicTypes;

public class SQLServerDialect extends org.hibernate.dialect.SQLServer2012Dialect {

	public SQLServerDialect() {
		super();
		registerColumnType(Types.CLOB, "varchar(max)");
		registerColumnType(Types.CLOB, "nvarchar(max)");
		registerFunction("date_format", new DateFormatSqlFunction(StandardBasicTypes.STRING, "format(?1, ?2, 'pt-br')"));
		registerFunction("STR_TO_DATE", new DateFormatSqlFunction(StandardBasicTypes.DATE, "convert(DATE, ?1, 103)"));
		registerFunction("CONVERT_DATE_TO_TIME", new ConvertSqlFunction(StandardBasicTypes.TIME, "convert(TIME,?1,8)"));
//		registerFunction( "DATE_FORMAT", new
//		SQLFunctionTemplate(StandardBasicTypes.STRING, "format(?1, ?2)") );
//		CONVERT(datetime, @mystring, @format)
	}

}