package br.infra.web.click;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.enums.TipoNotificacao;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboTipoNotificacao extends Select{

       /**
        * 
        */
       private static final long serialVersionUID = -5349652621703786323L;

       public ComboTipoNotificacao() {
               super("tipoNotificacao");

               TipoNotificacao[] values = TipoNotificacao.values();
               add(new Option("-1", "Todos"));

               for (TipoNotificacao tipo : values) {
                       int value = tipo.ordinal();
                       String label = tipo.toString();
                       add(new Option(String.valueOf(value), label));
               }
       }

       /**
        * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
        */
       @Override
       public void setValueObject(Object value) {
               if (value != null && value instanceof TipoNotificacao) {
            	   TipoNotificacao tipo = (TipoNotificacao) value;
                       String ordinal = String.valueOf(tipo.ordinal());
                       super.setValueObject(ordinal);
               }
       }

       @Override
       public Object getValueObject() {
               String value = super.getValue();
               if (value != null && NumberUtils.isNumber(value.toString())) {
                       Integer ordinal = Integer.parseInt(value.toString());
                       if(ordinal == -1) {
                               return null;
                       }
                       TipoNotificacao tipo = TipoNotificacao.values()[ordinal];
                       return tipo;
               }
               return super.getValueObject();
       }

       public TipoNotificacao getStatusMensagem() {
               String value = super.getValue();
               if (value != null && NumberUtils.isNumber(value.toString())) {
                       Integer ordinal = Integer.parseInt(value.toString());
                       TipoNotificacao tipo = TipoNotificacao.values()[ordinal];
                       return tipo;
               }
               return null;
       }
}

