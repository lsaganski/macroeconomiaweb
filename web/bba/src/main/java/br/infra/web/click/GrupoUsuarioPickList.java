package br.infra.web.click;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.service.GrupoService;
import net.sf.click.extras.control.PickList;

/**
 * Cadastro de Grupos e Permissoes
 * 
 * @author ricardo
 *
 */
@SuppressWarnings("unchecked")
public class GrupoUsuarioPickList extends PickList {
	private static final long serialVersionUID = 1471004337340478606L;
	private final GrupoService grupoService;

	public GrupoUsuarioPickList(GrupoService grupoService, Empresa empresa){
		super("grupos");
		this.grupoService = grupoService;

		setHeaderLabel("Grupos", "Grupos do usuário");

		List<Grupo> list = grupoService.findAll(empresa);

		addAll(list,"id","nome");
	}

	public void setSelectedPermissoes(List<Grupo> grupo) {
		super.setSelectedValues(grupo,"id");
	}

	public Set<Grupo> getPermissoes() {
		Set<Grupo> list = new HashSet<Grupo>();

		List<String> ids = getSelectedValues();

		for (String id : ids) {
			Grupo g = grupoService.get(new Long(id));

			list.add(g);

		}
		return list;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getSelectedValues() {
		return super.getSelectedValues();
	}

	@Override
	public Object getValueObject() {
		Set<Grupo> permissoes = new HashSet<Grupo>();

		List<String> ids = getSelectedValues();
		if(ids != null && ids.size() > 0) {
			for (String id : ids) {
				Grupo p = grupoService.get(new Long(id));
				permissoes.add(p);
			}
			return permissoes;
		}

		return super.getValueObject();
	}

	@Override
	public void setValueObject(Object object) {
		if(object != null) {
			Collection<Grupo> permissoes = (Collection<Grupo>) object;
			for (Grupo p : permissoes) {
				addSelectedValue(p.getId().toString());
			}
		}

		super.setValueObject(object);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setSelectedValues(Collection objects, String value) {
		super.setSelectedValues(objects, value);
	}
}
