package br.infra.web.click;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Cidade;
import br.livetouch.livecom.domain.service.CidadeService;
import net.sf.click.control.TextField;

/**
 * Select que permite copiar os valores diretamente do Form para o Domain Model
 * 
 * @author ricardo
 *
 */
public class CidadeTextField extends TextField {
	private static final long serialVersionUID = 7324379192779327235L;

	private Cidade entity;

	/** <? super T> para o caso de a Entity em questão usar o service da classe pai */
	private final CidadeService service;

	public CidadeTextField(String name, boolean required,CidadeService service) {
		super(name, required);
		this.service = service;
	}

	public CidadeTextField(String name, String label, boolean required,CidadeService service) {
		super(name, label, required);
		this.service = service;
	}

	public CidadeTextField(String name, String label,CidadeService service) {
		super(name, label);
		this.service = service;
	}


	/**
	 * Retorna a entidade que entao sendo editada ou inserida
	 * 
	 * @return
	 */
	public Cidade getCidade() {

		if(service == null){
			throw new IllegalStateException("O service não foi definido");
		}
		
		String id = getValue();
		if(isRequired() && StringUtils.isEmpty(id)){
			throw new IllegalArgumentException("id inválido");
		}

		entity = service.get(new Long(id));

		return entity;
	}
	
	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Cidade) {
			Cidade obj = (Cidade) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Cidade q = getCidade();
			return q;
		}
		return super.getValueObject();
	}
}
