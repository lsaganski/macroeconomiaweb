package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Complemento;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.service.ComplementoService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboComplemento extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final ComplementoService service;
	
	/**
	 * 
	 */
	public ComboComplemento(ComplementoService service, Empresa empresa) {
		super("complemento");
		this.service = service;
		
		List<Complemento> values = service.findAll(empresa);
		
		for (Complemento p : values) {
			if(p.getId() != null && p.getNome() != null) {
				Option option = new Option(p.getId(), p.getNome());
				add(option);
			}
		}
	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Complemento) {
			Complemento obj = (Complemento) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Complemento q = getComplemento();
			return q;
		}
		return super.getValueObject();
	}

	public Complemento getComplemento() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			Complemento q = service.get(id);
			return q;
		}
		return null;
	}
}
