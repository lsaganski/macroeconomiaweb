package br.infra.web.click;

import java.util.List;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.service.LogService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboVersao extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final LogService logService;
	
	public ComboVersao(LogService service, Empresa empresa) {
		super("versao");
		this.logService = service;
		
		List<String> values = logService.findAllVersoes(empresa);
		
		add(new Option("","Todas"));
		setValue("");
		
		for (String versao : values) {
			if(versao != null) {
				Option option = new Option(versao, versao);
				add(option);
			}
		}

	}
}


