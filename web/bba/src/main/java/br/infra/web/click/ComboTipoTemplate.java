package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.enums.TipoTemplate;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboTipoTemplate extends Select{

       /**
        * 
        */
       private static final long serialVersionUID = -5349652721703786323L;

       public ComboTipoTemplate() {
               super("tipoTemplate");

               TipoTemplate[] values = TipoTemplate.values();
               add(new Option("-1", "Não definir por enquanto"));

               for (TipoTemplate tipo : values) {
                   int value = tipo.ordinal();
                   String label = tipo.toString();
                   add(new Option(String.valueOf(value), label));
               }
       }

       /**
        * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
        */
       @Override
       public void setValueObject(Object value) {
               if (value != null && value instanceof TipoTemplate) {
            	   TipoTemplate tipo = (TipoTemplate) value;
                       String ordinal = String.valueOf(tipo.ordinal());
                       super.setValueObject(ordinal);
               }
       }

       @Override
       public Object getValueObject() {
               String value = super.getValue();
               if (value != null && NumberUtils.isNumber(value.toString())) {
                       Integer ordinal = Integer.parseInt(value.toString());
                       if(ordinal == -1) {
                               return null;
                       }
                       TipoTemplate tipo = TipoTemplate.values()[ordinal];
                       return tipo;
               }
               return super.getValueObject();
       }

       public TipoTemplate getTipoTemplate() {
           String value = super.getValue();
           if (value != null && NumberUtils.isNumber(value.toString())) {
                   Integer ordinal = Integer.parseInt(value.toString());
                   TipoTemplate tipo = TipoTemplate.values()[ordinal];
                   return tipo;
           }
           return null;
       }
       
       public void setValues(List<TipoTemplate> templates) {
    	   
    	   this.setOptionList(null);
    	   
    	   for (TipoTemplate tipo : templates) {
               int value = tipo.ordinal();
               String label = tipo.toString();
               add(new Option(String.valueOf(value), label));
           }
       }
       
       
}

