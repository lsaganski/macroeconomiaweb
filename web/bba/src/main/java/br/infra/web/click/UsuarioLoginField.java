package br.infra.web.click;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.UsuarioService;
import net.sf.click.control.TextField;

/**
 * Select que permite copiar os valores diretamente do Form para o Domain Model
 * 
 * @author ricardo
 *
 */
public class UsuarioLoginField extends TextField {
	private static final long serialVersionUID = 7324379192779327235L;

	private Usuario entity;

	/** <? super T> para o caso de a Entity em questão usar o service da classe pai */
	private final UsuarioService service;

	private Empresa empresa;

	public UsuarioLoginField(String name, boolean required,UsuarioService service, Empresa empresa) {
		super(name, required);
		this.service = service;
		this.empresa = empresa;
	}

	public UsuarioLoginField(String name, String label, boolean required,UsuarioService service, Empresa empresa) {
		super(name, label, required);
		this.service = service;
		this.empresa = empresa;
	}

	public UsuarioLoginField(String name, String label,UsuarioService service, Empresa empresa) {
		super(name, label);
		this.service = service;
		this.empresa = empresa;
	}

	public UsuarioLoginField(String name,UsuarioService service, Empresa empresa) {
		super(name);
		this.service = service;
		this.empresa = empresa;
	}

	/**
	 * Retorna a entidade que entao sendo editada ou inserida
	 * 
	 * @return
	 */
	public Usuario getEntity() {

		if(service == null){
			throw new IllegalStateException("O service nao foi definido");
		}

		String login = getValue();
		if(login == null) {
			// TODO remover depois que apps estiverem ok.
			login = getParam("user_id");
			if(login == null) {
				login = getParam("user");
			}
		}

		if(StringUtils.isEmpty(login)){
			if(isRequired()) {
				throw new IllegalArgumentException("login inválido");
			} else {
				return null;
			}
		}

		if(NumberUtils.isNumber(login)) {
			entity = service.get(new Long(login));
		}
		
		if(entity == null) {
			entity = service.findByLogin(login, null);
		}

		return entity;
	}
	
	protected String getParam(String key) {
		String s = getContext().getRequestParameter(key);
		return s;
	}
}
