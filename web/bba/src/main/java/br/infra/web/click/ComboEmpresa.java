package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.service.EmpresaService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboEmpresa extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final EmpresaService service;
	
	/**
	 * 
	 */
	public ComboEmpresa(EmpresaService service) {
		super("empresa");
		this.service = service;
		
		List<Empresa> values = service.findAll();
		
		for (Empresa p : values) {
			Option option = new Option(p.getId(), p.getNome());
			add(option);
		}

	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Empresa) {
			Empresa obj = (Empresa) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Empresa q = getEmpresa();
			return q;
		}
		return super.getValueObject();
	}

	public Empresa getEmpresa() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			Empresa q = service.get(id);
			return q;
		}
		return null;
	}
}
