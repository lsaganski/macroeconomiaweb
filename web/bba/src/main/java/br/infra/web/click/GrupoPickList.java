package br.infra.web.click;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.service.GrupoService;
import net.sf.click.extras.control.PickList;

/**
 * Cadastro de Grupos e Permissoes
 * 
 * @author rlecheta
 *
 */
@SuppressWarnings("unchecked")
public class GrupoPickList extends PickList {

	private static final long serialVersionUID = 3944177078412607309L;
	private final GrupoService grupoService;

	public GrupoPickList(GrupoService grupoService, Empresa empresa){
		super("grupos");
		this.grupoService = grupoService;

		setHeaderLabel("Permissões", "Permissões ligadas");

		List<Grupo> list = grupoService.findAll(empresa);

		addAll(list,"id","nome");

	}

	public void setSelectedGrupos(List<Grupo> grupo) {
		super.setSelectedValues(grupo,"id");
	}

	public Set<Grupo> getGrupos() {
		Set<Grupo> list = new HashSet<Grupo>();

		List<String> ids = getSelectedValues();

		for (String id : ids) {
			Grupo g = grupoService.get(new Long(id));

			list.add(g);

		}
		return list;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getSelectedValues() {
		return super.getSelectedValues();
	}

	@Override
	public Object getValueObject() {
		List<Grupo> grupos = new ArrayList<Grupo>();

		List<String> ids = getSelectedValues();
		if(ids != null && ids.size() > 0) {
			for (String id : ids) {
				Grupo p = grupoService.get(new Long(id));
				grupos.add(p);
			}
			return grupos;
		}

		return super.getValueObject();
	}

	@Override
	public void setValueObject(Object object) {
		if(object != null) {
			Collection<Grupo> grupos = (Collection<Grupo>) object;
			for (Grupo p : grupos) {
				addSelectedValue(p.getId().toString());
			}
		}

		super.setValueObject(object);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setSelectedValues(Collection objects, String value) {
		super.setSelectedValues(objects, value);
	}

	@Override
	public void setReadonly(boolean readonly) {
		super.setReadonly(readonly);
	}
}
