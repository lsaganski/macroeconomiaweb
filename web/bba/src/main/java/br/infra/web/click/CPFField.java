package br.infra.web.click;

import br.infra.util.Utils;
import net.sf.click.control.TextField;

/**
 * 
 * @author 
 */
public class CPFField extends TextField {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8026712854633932089L;

	public CPFField(String name,boolean required) {
		super(name,required);
	}

	@Override
	public String getValue() {
		String s = super.getValue();
		if(s != null) {
			s = Utils.getCPFFormatado(s);
		}
		return s;
	}

	@Override
	public void setValue(String value) {
		if(value != null) {
			value = Utils.getCPFDesformatado(value);
		}
		super.setValue(value);
	}

	@Override
	public void validate() {
		if(super.isRequired()){
				
		String value = getValue();
		
		if (value == null) {
			
				setErrorMessage("field-required-error");
				return;
			
		} else {

			int length = value.length();

			if (length > 0) {
				if (getMinLength() > 0 && length < getMinLength()) {
					setErrorMessage("field-minlength-error", getMinLength());
					return;
				}

				if (getMaxLength() > 0 && length > getMaxLength()) {
					setErrorMessage("field-maxlength-error", getMaxLength());
					return;
				}

			} else {
				if (isRequired()) {
					setErrorMessage("field-required-error");
				}
			}

		}
		
		}
		String cpf = this.value;
		
		

		
		if(!cpf.isEmpty()) {
			boolean ok = ValidaCPF.verificaCPF(cpf);
			if(!ok) {
				setError("O CPF ["+Utils.getCPFFormatado(cpf)+"] é inválido. ");
				return;
			}
		}	
		
		
	}

	@Override
	public Object getValueObject() {
		return super.getValueObject();
	}

	@Override
	public void setValueObject(Object object) {
		super.setValueObject(object);
	}
}
