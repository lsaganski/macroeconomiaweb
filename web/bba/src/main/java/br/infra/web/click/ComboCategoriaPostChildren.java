package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.service.CategoriaPostService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboCategoriaPostChildren extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final CategoriaPostService service;
	
	public ComboCategoriaPostChildren(CategoriaPostService service, Empresa empresa) {
		this("categoria",service, empresa);
	}
	/**
	 * 
	 */
	public ComboCategoriaPostChildren(String name,CategoriaPostService service, Empresa empresa) {
		super(name);
		this.service = service;
		
		List<CategoriaPost> values = service.findAllParent(empresa);
		
		Option header = new Option("0","- Categ -");
		add(header);

		for (CategoriaPost c : values) {
			Option option = new Option(c.getId(), c.getId()+":"+c.getPath());
			
			addCategOption(c,option," --- ");
		}

	}

	private void addCategOption(CategoriaPost c, Option option, String space) {
		add(option);

		if(c.hasChildren()) {
			for (CategoriaPost child : c.getCategorias()) {
				Option o = new Option(child.getId(),space + child.getPath());
				if(child.hasChildren()) {
					addCategOption(child, o,space+" --- ");
				} else {
					add(o);
				}
			}
		}
	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof CategoriaPost) {
			CategoriaPost obj = (CategoriaPost) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			CategoriaPost q = getCategoriaPost();
			return q;
		}
		return super.getValueObject();
	}

	public CategoriaPost getCategoriaPost() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			CategoriaPost q = service.get(id);
			return q;
		}
		return null;
	}
}
