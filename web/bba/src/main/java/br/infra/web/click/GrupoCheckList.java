package br.infra.web.click;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.service.GrupoService;

/**
 * Cadastro de Grupos e Permissoes
 * 
 * @author rlecheta
 *
 */
public class GrupoCheckList extends net.livetouch.click.control.list.CheckList {

	private static final long serialVersionUID = 3944177078412607309L;
	protected final GrupoService grupoService;

	public GrupoCheckList(GrupoService grupoService, Empresa empresa){
		super("grupos");
		this.grupoService = grupoService;

//		setHeaderLabel("Permissões", "Permissões ligadas");
		
		setRequired(true);

		List<Grupo> list = grupoService.findAll(empresa);
		addAll(list,"id","nome");

	}

	@SuppressWarnings("unchecked")
	public Set<Grupo> getGrupos() {
		Set<Grupo> list = new HashSet<Grupo>();

		List<String> ids = getSelectedValues();

		for (String id : ids) {
			Grupo g = grupoService.get(new Long(id));

			list.add(g);
		}
		return list;
	
	}
	
	@Override
	public Object getValueObject() {
		Object o  = super.getValueObject();
		return getGrupos();
	}
}
