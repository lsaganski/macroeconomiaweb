package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.service.CategoriaPostService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboCategoriaPost extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final CategoriaPostService service;
	
	public ComboCategoriaPost(CategoriaPostService service, Empresa empresa) {
		this(service,true,true, empresa);
	}

	/**
	 * 
	 */
	public ComboCategoriaPost(CategoriaPostService service, boolean parent, boolean required, Empresa empresa) {
		super("categoria");
		this.service = service;
		
		if(!required) {
			add(new Option("", "Selecione a categoria pai"));
		}
		
		List<CategoriaPost> values = parent ? service.findAllParent(empresa) : service.findAll(empresa);
		
		for (CategoriaPost p : values) {
			if(p.getNome() != null) {
				Option option = new Option(p.getId(), p.getNome());
				add(option);
			}
		}
	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof CategoriaPost) {
			CategoriaPost c = (CategoriaPost) value;
			super.setValueObject(c.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			CategoriaPost q = getCategoriaPost();
			return q;
		}
		return super.getValueObject();
	}

	public CategoriaPost getCategoriaPost() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			CategoriaPost q = service.get(id);
			return q;
		}
		return null;
	}
}
