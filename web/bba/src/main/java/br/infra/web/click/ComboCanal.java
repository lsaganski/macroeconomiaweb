package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Canal;
import br.livetouch.livecom.domain.service.CanalService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboCanal extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final CanalService service;
	
	/**
	 * 
	 */
	public ComboCanal(CanalService service) {
		super("canal");
		this.service = service;
		
		List<Canal> values = service.findAll();
		
		for (Canal p : values) {
			if(p.getId() != null && p.getNome() != null) {
				Option option = new Option(p.getId(), p.getNome());
				add(option);
			}
		}

	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Canal) {
			Canal obj = (Canal) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Canal q = getCanal();
			return q;
		}
		return super.getValueObject();
	}

	public Canal getCanal() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			Canal q = service.get(id);
			return q;
		}
		return null;
	}
}
