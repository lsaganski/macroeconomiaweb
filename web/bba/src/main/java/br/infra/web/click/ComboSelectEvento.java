package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Evento;
import br.livetouch.livecom.domain.service.EventoService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboSelectEvento extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final EventoService service;
	
	public ComboSelectEvento(EventoService service) {
		super("evento");
		this.service = service;
		
		List<Evento> values = service.findAll();
		
		add(new Option("","Selecione"));
		setValue("0");
		
		for (Evento e : values) {
			Option option = new Option(e.getId(), e.getNome());
			add(option);
		}

	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Evento) {
			Evento obj = (Evento) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Evento q = getEvento();
			return q;
		}
		return super.getValueObject();
	}

	public Evento getEvento() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			Evento e = service.get(id);
			return e;
		}
		return null;
	}
}
