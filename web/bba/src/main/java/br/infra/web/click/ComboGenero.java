package br.infra.web.click;

import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboGenero extends Select{
	private static final long serialVersionUID = 2266546939424630508L;

	public ComboGenero() {
		super("genero");

		add(new Option("M", "Masculino"));
		add(new Option("F", "Feminino"));
	}
}


