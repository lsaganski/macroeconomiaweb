package br.infra.web.click;

import java.util.HashMap;
import java.util.Map;

import net.sf.click.Context;
import net.sf.click.control.Decorator;
import net.sf.click.util.PropertyUtils;

/**
 * @author Ricardo Lecheta
 *
 */
@SuppressWarnings("rawtypes")
public class OnOffZeroUmDecorator implements Decorator {

	private final String property;
	private Map methodCache = new HashMap();

	public OnOffZeroUmDecorator(String property) {
		this.property = property;
	}

	@Override
	public String render(Object object, Context context) {
		try {
			Object value = PropertyUtils.getValue(object, property, methodCache );
			if (value != null) {
				boolean on = "1".equals(value.toString());
				//imgSrc = on ? imgSrc+"on.png' width='12px' height='12px' >" : imgSrc+"off.png' width='12px' height='12px' >";
				if (on) {
					String imgSrc = "<img src='" + context.getRequest().getContextPath() + "/img/";
					imgSrc += "on.png' width='12px' height='12px' >";
					return imgSrc;
				}
			}
		} catch (Exception e) {
			return object.toString();
		}
		return "";
	}
}
