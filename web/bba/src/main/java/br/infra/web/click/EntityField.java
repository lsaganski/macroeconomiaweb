package br.infra.web.click;

import br.livetouch.livecom.domain.service.Service;
import net.livetouch.tiger.ddd.Entity;
import net.sf.click.control.TextField;

/**
 * Select que permite copiar os valores diretamente do Form para o Domain Model
 * 
 * @author ricardo
 *
 */
public class EntityField<T extends Entity> extends TextField {
	private static final long serialVersionUID = 7324379192779327235L;

	private T entity;

	/** <? super T> para o caso de a Entity em questão usar o service da classe pai */
	private final Service<? super T> service;

	public EntityField(String name, boolean required,Service<T> service) {
		super(name, required);
		this.service = service;
	}

	public EntityField(String name, String label, boolean required,Service<T> service) {
		super(name, label, required);
		this.service = service;
	}

	public EntityField(String name, String label,Service<T> service) {
		super(name, label);
		this.service = service;
	}

	public EntityField(String name,Service<T> service) {
		super(name);
		this.service = service;
	}

	/**
	 * Retorna a entidade que entao sendo editada ou inserida
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T getEntity() {

		if(service == null){
			throw new IllegalStateException("O service não foi definido");
		}
		
		Long id = getEntityId();
		if(id == null){
			return null;
		}

		entity = (T) service.get(id);

		return entity;
	}
	
	@Override
	public Object getValueObject() {
		return getEntity();
	}

	private Long getEntityId() {
		Long id = getLong();
		return id;
	}
	
	public Long getLong() {
        String value = getValue();
        if (value != null && value.length() > 0) {
            try {
                return Long.valueOf(value);

            } catch (NumberFormatException nfe) {
                return null;
            }
        } else {
            return null;
        }
    }
}
