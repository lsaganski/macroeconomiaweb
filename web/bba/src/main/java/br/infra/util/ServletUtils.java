package br.infra.util;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

/**
 * TODO LIB: mover para lib LivetouchWeb
 * 
 * @author rlech
 *
 */
public class ServletUtils {

	public static String getRequestUrl(ServletRequest request) {
		HttpServletRequest req = (HttpServletRequest) request;
    	StringBuffer requestURL = req.getRequestURL();
    	if (req.getQueryString() != null) {
    	    requestURL.append("?").append(req.getQueryString());
    	}
    	String completeURL = requestURL.toString();
    	return completeURL;
	}

	public static String getRequestPath(ServletRequest request) {
		return request.getServletContext().getContextPath();
	}

	public static String getHost(HttpServletRequest req) {
		try {
			if(req != null) {
				StringBuffer url = req.getRequestURL();
				String uri = req.getRequestURI();
				String ctx = req.getContextPath();
				String host = url.substring(0, url.length() - uri.length() + ctx.length());
				return host;
			}
		} catch (Exception e) {
			e.printStackTrace();
//			logger.error("\n\n####### Erro getHost(): " + e.getMessage(), e);
		}
		return "";
	
	}

	public static String getRequestParams(ServletRequest request) {
		return ServletUtil.getRequestParams((HttpServletRequest) request);
	}

}
