package br.infra.util;

import java.io.File;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.livetouch.livecom.domain.Expediente;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.enums.Semana;
import br.livetouch.livecom.domain.exception.DataInvalidaException;
import br.livetouch.livecom.domain.exception.PeriodoBuscaInvalidoException;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.LogSistemaFiltro;
import br.livetouch.livecom.domain.vo.LogTransacaoFiltro;
import br.livetouch.livecom.domain.vo.RelatorioConversaFiltro;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.utils.FileExtensionUtils;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;

/**
 * Classe utilitaria com regras gerais de validação
 * 
 * @author ricardo
 * 
 */
public class Utils {
	private static final Locale localePtBR = new Locale("pt", "BR");

	protected static final Logger log = Log.getLogger(Utils.class);

	public static final String CHARSET = "ISO-8859-1";
	
	public static final double SPACE_KB = 1024;
    public static final double SPACE_MB = 1024 * SPACE_KB;
    public static final double SPACE_GB = 1024 * SPACE_MB;
    public static final double SPACE_TB = 1024 * SPACE_GB;

	/**
	 * - Celular com DDD; O formato de digitação será (XX)XXXX-XXXX entretanto o
	 * formato gravado no banco será XXXXXXXXXX; Números de telefones (excluindo
	 * o DDD) iniciados com 1/2/3/4/5 não serão aceitos.
	 * 
	 * @param celular
	 * @return
	 */
	public static boolean validaCelular(String celular) {
		if (StringUtils.isEmpty(celular)) {
			// pode celular vazio? Nao vem no arquivo de lote entao como
			// validar?
			// usuario ser do tipo celular..."
			return true;
		} else {

			int length = celular.length();
			if (length < 8) {
				return false;
			}

			// char[] numeros = celular.toCharArray();
			char[] numeros = Utils.getCelularDesformatado(celular).toCharArray();
			char c = numeros[2];
			if (c == '1' || c == '2' || c == '3' || c == '4' || c == '5') {
				return false;
			} else {
				return true;
			}

		}
		// boolean b = NumberUtils.isNumber(celular);
		// return b;
	}

	public static String getCelularFormatado(String celular) {
		if (StringUtils.isEmpty(celular)) {
			return null;
		}
		if (celular.length() != 10) {
			return celular;
		}

		String s = "(";
		char c = celular.charAt(0);
		s = s + c;
		c = celular.charAt(1);
		s = s + c;
		s = s + ")";
		c = celular.charAt(2);
		s = s + c;
		c = celular.charAt(3);
		s = s + c;
		c = celular.charAt(4);
		s = s + c;
		c = celular.charAt(5);
		s = s + c;
		s = s + "-";
		c = celular.charAt(6);
		s = s + c;
		c = celular.charAt(7);
		s = s + c;
		c = celular.charAt(8);
		s = s + c;
		c = celular.charAt(9);
		s = s + c;

		return s;
	}

	public static String getCelularDesformatado(String celular) {
		// desformata
		String s = StringUtils.remove(celular, "(");
		s = StringUtils.remove(s, ")");
		s = StringUtils.remove(s, "-");
		return s;
	}

	public static boolean loginValido(String login) {
		if (StringUtils.isEmpty(login)) {
			return false;
		}
		char[] x = login.toCharArray();
		for (char c : x) {
			if (c == '!' || c == '"' || c == '#' || c == '@' || c == ' ' || c == '$' || c == '¨' || c == '&' || c == '*' || c == '(' || c == ')' || c == '-' || c == '_' || c == '+' || c == '=' || c == '}' || c == ']' || c == '[' || c == '{' || c == 'ª' || c == '^' || c == '~'
					|| c == '?' || c == '/' || c == '°' || c == ';' || c == ':' || c == '>' || c == '<' || c == '|' || c == '§') {
				return false;
			}
		}
		return true;
	}

	public static String getCPFFormatado(String cpf) {
		if (StringUtils.isEmpty(cpf)) {
			return null;
		}

		if (cpf.length() != 11) {
			return cpf;
		}
		String s = "";
		char c = cpf.charAt(0);
		s = s + c;
		c = cpf.charAt(1);
		s = s + c;
		c = cpf.charAt(2);
		s = s + c;
		s = s + ".";
		c = cpf.charAt(3);
		s = s + c;
		c = cpf.charAt(4);
		s = s + c;
		c = cpf.charAt(5);
		s = s + c;
		s = s + ".";
		c = cpf.charAt(6);
		s = s + c;
		c = cpf.charAt(7);
		s = s + c;
		c = cpf.charAt(8);
		s = s + c;
		s = s + "-";
		c = cpf.charAt(9);
		s = s + c;
		c = cpf.charAt(10);
		s = s + c;
		return s;
	}

	public static String getCPFDesformatado(String cpf) {
		// desformata
		String s = StringUtils.remove(cpf, ".");
		s = StringUtils.remove(cpf, ".");
		s = StringUtils.remove(s, "-");
		return s;
	}

	public static boolean periodoBuscaValido(LogTransacaoFiltro log) throws DomainException {
		if (log.getDataFinal() != null && log.getDataInicial() != null) {

			if (dataValida(log)) {
				// se o periodo inicial for menor ou igual que o periodo final,
				// retorna TRUE
				if (DateUtils.isMenor(log.getDataInicial(), log.getDataFinal()) || DateUtils.isIgual(log.getDataInicial(), log.getDataFinal())) {
					return true;
				} else {
					throw new PeriodoBuscaInvalidoException("exception.periodo.invalido");
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static boolean periodoBuscaValido(LogSistemaFiltro log) throws DomainException {
		if (log.getDataFinal() != null && log.getDataInicial() != null) {

			if (dataValida(log)) {
				// se o periodo inicial for menor ou igual que o periodo final,
				// retorna TRUE
				if (DateUtils.isMenor(log.getDataInicial(), log.getDataFinal()) || DateUtils.isIgual(log.getDataInicial(), log.getDataFinal())) {
					return true;
				} else {
					throw new PeriodoBuscaInvalidoException("exception.periodo.invalido");
				}
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	public static boolean dataValida(LogTransacaoFiltro log) throws DomainException {
		if (log.getDataFinal() != null && log.getDataInicial() != null) {

			String dataInicio = log.getDataInicioStringDMY();
			String dataFim = log.getDataFimStringDMY();

			int diaI;
			int mesI;
			int anoI;

			int diaF;
			int mesF;
			int anoF;

			if (dataInicio.length() == 10) {
				diaI = Integer.parseInt(dataInicio.substring(0, 2));
				mesI = Integer.parseInt(dataInicio.substring(3, 5));
				anoI = Integer.parseInt(dataInicio.substring(6, 10));
			} else {
				throw new DataInvalidaException("exception.data.invalida");
			}

			if (dataFim.length() == 10) {
				diaF = Integer.parseInt(dataFim.substring(0, 2));
				mesF = Integer.parseInt(dataFim.substring(3, 5));
				anoF = Integer.parseInt(dataFim.substring(6, 10));
			} else {
				throw new DataInvalidaException("exception.data.invalida");
			}

			// verifico de forma simples a data
			// o SQL server inicia o ano em 1753,se for realizada um query
			// buscando ano < 1753, da pau
			if (diaI <= 31 && diaF <= 31 && mesI <= 12 && mesF <= 12 && anoI > 1753 && anoF > 1753) {
				return true;
			} else {
				throw new DataInvalidaException("exception.data.invalida");
			}
		} else {
			return false;
		}
	}

	public static boolean dataValida(RelatorioConversaFiltro conversaFiltro) throws DomainException {
		RelatorioFiltro filtro = new RelatorioFiltro();
		filtro.setDataInicial(conversaFiltro.getDataInicial());
		filtro.setDataFinal(conversaFiltro.getDataFinal());
		return dataValida(filtro);
	}

	public static boolean dataValida(RelatorioFiltro filtro) throws DomainException {
		if (filtro.getDataFinal() != null && filtro.getDataInicial() != null) {

			String dataInicio = filtro.getDataInicialStringDMY();
			String dataFim = filtro.getDataFinalStringDMY();

			int diaI;
			int mesI;
			int anoI;

			int diaF;
			int mesF;
			int anoF;

			if (dataInicio.length() == 10) {
				diaI = Integer.parseInt(dataInicio.substring(0, 2));
				mesI = Integer.parseInt(dataInicio.substring(3, 5));
				anoI = Integer.parseInt(dataInicio.substring(6, 10));
			} else {
				throw new DataInvalidaException("exception.data.invalida");
			}

			if (dataFim.length() == 10) {
				diaF = Integer.parseInt(dataFim.substring(0, 2));
				mesF = Integer.parseInt(dataFim.substring(3, 5));
				anoF = Integer.parseInt(dataFim.substring(6, 10));
			} else {
				throw new DataInvalidaException("exception.data.invalida");
			}

			// verifico de forma simples a data
			// o SQL server inicia o ano em 1753,se for realizada um query
			// buscando ano < 1753, da pau
			if (diaI <= 31 && diaF <= 31 && mesI <= 12 && mesF <= 12 && anoI > 1753 && anoF > 1753) {
				return true;
			} else {
				throw new DataInvalidaException("exception.data.invalida");
			}
		} else {
			return false;
		}
	}

	public static boolean dataValida(LogSistemaFiltro log) throws DomainException {
		if (log.getDataFinal() != null && log.getDataInicial() != null) {

			String dataInicio = log.getDataInicialStringDMY();
			String dataFim = log.getDataFinalStringDMY();

			int diaI;
			int mesI;
			int anoI;

			int diaF;
			int mesF;
			int anoF;

			if (dataInicio.length() == 10) {
				diaI = Integer.parseInt(dataInicio.substring(0, 2));
				mesI = Integer.parseInt(dataInicio.substring(3, 5));
				anoI = Integer.parseInt(dataInicio.substring(6, 10));
			} else {
				throw new DataInvalidaException("exception.data.invalida");
			}

			if (dataFim.length() == 10) {
				diaF = Integer.parseInt(dataFim.substring(0, 2));
				mesF = Integer.parseInt(dataFim.substring(3, 5));
				anoF = Integer.parseInt(dataFim.substring(6, 10));
			} else {
				throw new DataInvalidaException("exception.data.invalida");
			}

			// verifico de forma simples a data
			// o SQL server inicia o ano em 1753,se for realizada um query
			// buscando ano < 1753, da pau
			if (diaI <= 31 && diaF <= 31 && mesI <= 12 && mesF <= 12 && anoI > 1753 && anoF > 1753) {
				return true;
			} else {
				throw new DataInvalidaException("exception.data.invalida");
			}
		} else {
			return false;
		}
	}

	public static boolean validaEmail(String email) {
		if (StringUtils.isEmpty(email)) {
			return false;
		}
		if (email.length() > 0) {
			int length = email.length();

			int atIndex = email.indexOf("@");
			if (atIndex < 1 || atIndex == length - 1) {
				return false;
			}

			int dotIndex = email.lastIndexOf(".");
			if (dotIndex == -1 || dotIndex < atIndex || dotIndex == length - 1) {
				return false;
			}

			if (!Character.isLetterOrDigit(email.charAt(0))) {
				return false;
			}

			if (!Character.isLetterOrDigit(email.charAt(length - 1))) {
				return false;
			}
		}
		return true;
	}

	public static String geStringMoney(Number number) {
		try {
			Locale pt = Locale.getDefault();
			if (!"pt".equalsIgnoreCase(pt.getLanguage())) {
				pt = localePtBR;
			}

			NumberFormat format = NumberFormat.getCurrencyInstance(pt);
			String s = format.format(number);
			return s;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return number.toString();
		}
	}

	public static String formataMoeda(double valor) {
		NumberFormat nf = NumberFormat.getCurrencyInstance(localePtBR);
		String valorMoeda = nf.format(valor);
		return valorMoeda;
	}

	public static String desformataMoeda(String valor) {
		if (valor != null) {
			valor = valor.replace("R$", "");
			valor = valor.replace(" ", "");
			valor = valor.replace(",", "");
			valor = valor.replace(".", "");

			return valor;
		} else {
			return "";
		}
	}

	public static boolean isLocalMachine() {
		try {
			String hostName = Inet4Address.getLocalHost().getHostName();
			if ("RicardoLecheta".equals(hostName) || "User".equals(hostName)) {
				return true;
			}
			return false;
		} catch (UnknownHostException e) {
			return false;
		}
	}

	/**
	 * 
	 * Substitui a url padrão da Amazon S3 pela url padrão do Cloudfront
	 * 
	 * @param files
	 * @return List<FileVO>
	 * 
	 * 
	 */
	public static List<FileVO> s3ToEndpoint(List<FileVO> files) {
		if (files == null) {
			return null;
		}
		for (FileVO f : files) {
			f.url = s3ToEndpoint(f.url);
			f.urlThumb = s3ToEndpoint(f.urlThumb);
			if (f.thumbs != null) {
				for (ThumbVO thumb : f.thumbs) {
					thumb.url = s3ToEndpoint(thumb.url);
				}
			}
		}
		return files;
	}

	/**
	 * 
	 * Substitui a url padrão da Amazon S3 pela url padrão do Cloudfront
	 * 
	 * @param urlS3
	 * @return String
	 * 
	 * 
	 */
	public static String s3ToCloudfront(String urlS3) {

		if (StringUtils.isEmpty(urlS3)) {
			return urlS3;
		}

		// urlS3 = urlS3.replace("https://", "http://");

		if (!FileExtensionUtils.isVideo(urlS3)) {
			// return urlS3;
			return s3ToEndpoint(urlS3);

		}

		String cloudfront = ParametrosMap.getInstance().get(Params.AMAZON_CLOUDFRONT_ARQUIVOS);
		if (StringUtils.isEmpty(cloudfront)) {
			return urlS3;
		}
		// String ext = FileExtensionUtils.getExtensao(urlS3);
		String bucket = ParametrosMap.getInstance().get(Params.FILE_MANAGER_AMAZON_BUCKET);
		String server = ParametrosMap.getInstance().get(Params.FILE_MANAGER_AMAZON_SERVER);
		server = server.replace("https://", "");
		String url = server + "/" + bucket;

		if (!StringUtils.contains(urlS3, server)) {
			return urlS3;
		}

		String urlCloudFront = urlS3.replace(url, cloudfront);

		return urlCloudFront;
	}

	/**
	 * 
	 * Substitui a url padrão da Amazon S3 pela url padrão do Endpoint
	 * 
	 * @param urlS3
	 * @return String
	 * 
	 * 
	 */
	public static String s3ToEndpoint(String urlS3) {

		if (StringUtils.isEmpty(urlS3)) {
			return urlS3;
		}

		ParametrosMap params = ParametrosMap.getInstance();
		boolean cloudfront = params.getBoolean(Params.AMAZON_CLOUDFRONT_ON, false);
		if (cloudfront && FileExtensionUtils.isVideo(urlS3)) {
			return s3ToCloudfront(urlS3);
		}

		String endpoint = params.get(Params.AMAZON_S3_ARQUIVOS);
		if (StringUtils.isEmpty(endpoint)) {
			return urlS3;
		}

		String bucket = params.get(Params.FILE_MANAGER_AMAZON_BUCKET);
		String server = params.get(Params.FILE_MANAGER_AMAZON_SERVER);
		String url = server + "/" + bucket;

		if (!StringUtils.contains(urlS3, server)) {
			return urlS3;
		}

		String urlEndpoint = urlS3.replace(url, endpoint);
		urlEndpoint = "http://" + urlEndpoint;

		return urlEndpoint;
	}

	public static String getDurationString(long seconds) {
		long hours = seconds / 3600;
		long minutes = (seconds % 3600) / 60;
		seconds = seconds % 60;
		return twoDigitString(hours) + ":" + twoDigitString(minutes) + ":" + twoDigitString(seconds);
	}
	
	public static String getDurationStringMillis(long milliseconds) {
		long hours = (milliseconds / (1000*60*60));
		long minutes = ((milliseconds / (1000*60)) % 60);
		long seconds = (milliseconds / 1000) % 60;
		return twoDigitString(hours) + ":" + twoDigitString(minutes) + ":" + twoDigitString(seconds);
	}

	public static String twoDigitString(long number) {
		if (number == 0) {
			return "00";
		}
		if (number / 10 == 0) {
			return "0" + number;
		}
		return String.valueOf(number);
	}
	
	public static String getDDD(String telefone) {
		if(hasDDD(telefone)) {
			return telefone.substring(0, 2);
		}
		return "";
	}

	public static String getTelefoneSemDDD(String telefone) {
		if(hasDDD(telefone)) {
			return telefone.substring(2);
		}
		return telefone;
	}
	
	private static boolean hasDDD(String telefone) {
		if(StringUtils.length(telefone) > 9) {
			return true;
		}
		return false;
	}
	
	public static boolean isExpedienteValido(Expediente expediente) {
		if(expediente != null) {
			Semana dia = Semana.today();
			switch(dia) {
			case SEG:
				return expediente.isSegunda();
			case TER:
				return expediente.isTerca();
			case QUA:
				return expediente.isQuarta();
			case QUI:
				return expediente.isQuinta();
			case SEX:
				return expediente.isSexta();			
			case SAB:
				return expediente.isSabado();
			case DOM:
				return expediente.isDomingo();
			}
			
			return false;
		}
		
		return true;
	}
	
	public static boolean isHorarioValido(Date horaAbertura, Date horaFechamento) {
		
		if(horaAbertura == null && horaFechamento == null) {
			return true;
		} else {
			
			Date agora = new Date();
			
			if(DateUtils.isMenor(horaAbertura, agora, "HH:mm:ss") && DateUtils.isMaior(horaFechamento, agora, "HH:mm:ss")) {
				return true;
			}
			
			if(DateUtils.isIgual(horaAbertura, horaFechamento, "HH:mm:ss")) {
				return true;
			}
			
			return false;
		}
		
	}
	
	public static String readFileItem(FileItem fileItem) throws Exception {

		if(fileItem != null && fileItem.get().length > 0) {
			File f = new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString());
			fileItem.write(f);
			String html = FileUtils.readFileToString(f, "UTF-8");
			FileUtils.deleteQuietly(f);
			return html;
		} else {
			return "";
		}
		
	}

	public static String validaSexo(String sexo) {
		if(StringUtils.isEmpty(sexo)) {
			return "M";
		}
		
		sexo = StringUtils.upperCase(sexo);
		if("F".equals(sexo) || "M".equals(sexo)) {
			return sexo;
		}
		
		return "M";
	}

	public static void sleep(String string) {
//		try {
//			System.err.println(string);
//			Thread.sleep(500);
//		} catch (InterruptedException e) {
//		}
	}

	// http://stackoverflow.com/questions/8499698/trim-a-string-based-on-the-string-length
	public static String truncate(String s, int length) {
		if(s == null) {
			return null;
		}
		if(s.length() <= length) {
			return s;
		}
		s = s.substring(0, Math.min(s.length(), length));
		return s;
	}
	
	public static String sizeToHumanReadable(long sizeInBytes) {

	    NumberFormat nf = new DecimalFormat();
	    nf.setMaximumFractionDigits(2);

	    try {
	      if ( sizeInBytes < SPACE_KB ) {
	        return nf.format(sizeInBytes) + " Byte(s)";
	      } else if ( sizeInBytes < SPACE_MB ) {
	        return nf.format(sizeInBytes/SPACE_KB) + " KB";
	      } else if ( sizeInBytes < SPACE_GB ) {
	        return nf.format(sizeInBytes/SPACE_MB) + " MB";
	      } else if ( sizeInBytes < SPACE_TB ) {
	        return nf.format(sizeInBytes/SPACE_GB) + " GB";
	      } else {
	        return nf.format(sizeInBytes/SPACE_TB) + " TB";
	      }          
	    } catch (Exception e) {
	      return sizeInBytes + " Byte(s)";
	    }
	}
	
	public static List<Long> getIds(String[] list) {
		List<Long> ids = new ArrayList<Long>();
		if(list == null || list.length == 0) {
			return ids;
		}
		for (String s : list) {
			if(StringUtils.isNotEmpty(s)) {
				ids.add(Long.parseLong(s.trim()));
			}
		}
		return ids;
	}
	
	public static List<String> getListCods(String s) {
		Set<String> cods = new HashSet<String>();
		String[] split = StringUtils.split(s, ",");
		if (split != null) {
			for (String cod : split) {
				cod = StringUtils.trim(cod);
				cods.add(cod);
			}
		}
		return new ArrayList<String>(cods);
	}
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortMap(final Map<K, V> mapToSort) {
		List<Map.Entry<K, V>> entries = new ArrayList<Map.Entry<K, V>>(mapToSort.size());
 
		entries.addAll(mapToSort.entrySet());
 
		// Sorts the specified list according to the order induced by the specified comparator
		Collections.sort(entries, new Comparator<Map.Entry<K, V>>() {
			@Override
			public int compare(final Map.Entry<K, V> entry1, final Map.Entry<K, V> entry2) {
				// Compares this object with the specified object for order
				return entry1.getValue().compareTo(entry2.getValue());
			}
		});
 
		Map<K, V> sortedMap = new LinkedHashMap<K, V>();
 
		// The Map.entrySet method returns a collection-view of the map
		for (Map.Entry<K, V> entry : entries) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}
 
		return sortedMap;
	}
	
	public static int calculateProportion(int thumbSize, int width, int height) {
		Float aspectRatio = (float) width / height;
		height = (int) (thumbSize * aspectRatio);
		return height;
	}
	
}
