package br.infra.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.dao.DataIntegrityViolationException;

public class TempExceptionUtils {

	public static boolean isErrorContraint(DataIntegrityViolationException e) {
		Throwable root = ExceptionUtils.getRootCause(e);
		if(root == null) {
			root = e;
		}
		String name = root.getClass().getSimpleName();
		if(StringUtils.contains(name.toLowerCase(), "constraint")) {
			return true;
		}
		
		return false;
	}

	public static boolean isErrorTruncate(DataIntegrityViolationException e) {
		Throwable root = ExceptionUtils.getRootCause(e);
		if(root == null) {
			root = e;
		}
		String name = root.getClass().getSimpleName();
		if(StringUtils.contains(name.toLowerCase(), "truncation")) {
			return true;
		}
		
		return false;
	}
}
