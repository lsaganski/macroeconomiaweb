package br.infra.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import br.livetouch.livecom.domain.Usuario;

public class LongOperation implements Serializable {
	private static final long serialVersionUID = 1L;

	private static Map<String, LongOperation> map = new HashMap<String, LongOperation>();

	private static final int RUNNING = 1;
	private static final int FINISHED = 2;
	private int status;
	private String erro;
	
	public static abstract class LongOperationRunner {

		public abstract void run(Usuario userInfo) throws Exception;
		
	}

	private LongOperation() {
		status = RUNNING;
	}

	public static boolean isRunning(String cod) {
		LongOperation l = get(cod);
		boolean b = l != null && l.status == RUNNING;
		return b;
	}

	public static LongOperation get(String cod) {
		LongOperation l = map.get(cod);
		return l;
	}
	
	public static boolean isFinished(String cod) {
		LongOperation l = get(cod);
		boolean b = l != null && l.status == FINISHED;
		if(b) {
			map.remove(cod);
		}
		return b;
	}
	
	public static void start(String cod) {
		map.put(cod, new LongOperation());
	}

	public static void finish(String cod) {
		LongOperation l = get(cod);
		if(l != null) {
			l.status = FINISHED;
		}
	}
	
	public static void setErro(String cod, String erro) {
		LongOperation l = get(cod);
		if(l != null) {
			l.erro = erro;
		}
	}

	public static String getErro(String cod) {
		LongOperation l = get(cod);
		if(l != null) {
			return l.erro;
		}
		return "";
	}

}