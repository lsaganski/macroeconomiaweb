package br.livetouch.livecom.rest.oauth;

import java.util.concurrent.ConcurrentHashMap;

public class ConsumerKeyMap {
	
	public static final ConsumerKeyMap instance = new ConsumerKeyMap();
	private ConcurrentHashMap<String, LivecomConsumer> consumers;
	
	private ConsumerKeyMap() {
		this.consumers = new ConcurrentHashMap<>();
	}
	
	public static ConsumerKeyMap getInstance() {
		return instance;
	}
	
	public void clear() {
		this.consumers.clear();
	}
	
	public LivecomConsumer get(String key) {
		LivecomConsumer livecomConsumer = consumers.get(key);
		return livecomConsumer != null ? livecomConsumer : null;
	}
	
	public void put(String owner, String consumerKey, String secretKey) {
		consumers.put(consumerKey, new LivecomConsumer(consumerKey, secretKey, owner));
	}
	
	public void removeConsumer(String key) {
		consumers.remove(key);
	}
	
	public ConcurrentHashMap<String, LivecomConsumer> getAll() {
		return consumers;
	}
	
	public Integer size() {
		return consumers.size();
	}
}
