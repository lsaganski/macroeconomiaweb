package br.livetouch.livecom.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.gson.Gson;

import br.livetouch.livecom.domain.vo.EmpresaVO;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Empresa extends JsonEntity{
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "EMPRESA_SEQ")
	private Long id;

	private String nome;
	private String codigo;
	private String dominio;
	
	private String urlLogo;
	private String urlBg;
	private String urlFav;

	@OneToMany(mappedBy = "empresa", fetch = FetchType.LAZY)
	private Set<TemplateEmail> templates;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario admin;
	
	private String trialCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isLivecom() {
		return id == 1;
	}

	public String getDominio() {
		return dominio;
	}

	public void setDominio(String dominio) {
		this.dominio = dominio;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Usuario getAdmin() {
		return admin;
	}

	public void setAdmin(Usuario admin) {
		this.admin = admin;
	}

	public String getUrlLogo() {
		return urlLogo;
	}

	public void setUrlLogo(String urlLogo) {
		this.urlLogo = urlLogo;
	}

	public String getUrlBg() {
		return urlBg;
	}

	public void setUrlBg(String urlBg) {
		this.urlBg = urlBg;
	}

	public String getUrlFav() {
		return urlFav;
	}

	public void setUrlFav(String urlFav) {
		this.urlFav = urlFav;
	}

	@Override
	public String toString() {
		return "Empresa [id=" + id + ", nome=" + nome + ", codigo=" + codigo  + ", host=" + dominio + "]";
	}

	public Set<TemplateEmail> getTemplates() {
		return templates;
	}

	public void setTemplates(Set<TemplateEmail> templates) {
		this.templates = templates;
	}

	public String getTrialCode() {
		return trialCode;
	}

	public void setTrialCode(String trialCode) {
		this.trialCode = trialCode;
	}

	@Override
	public String toJson() {
		EmpresaVO vo = new EmpresaVO(this);
		return new Gson().toJson(vo);
	}

}
