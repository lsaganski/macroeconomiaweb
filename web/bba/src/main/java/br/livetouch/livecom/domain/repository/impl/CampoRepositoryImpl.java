package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Campo;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.repository.CampoRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class CampoRepositoryImpl extends StringHibernateRepository<Campo> implements CampoRepository {

	public CampoRepositoryImpl() {
		super(Campo.class);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Campo> findAll(Empresa e) {
		StringBuffer sb = new StringBuffer("from Campo where 1=1" );
		
		if(e != null) {
			sb.append(" and empresa = :empresa");
		}
		
		Query q = createQuery(sb.toString());
		q.setCacheable(true);
		
		if(e != null) {
			q.setParameter("empresa", e);
		}
		List<Campo> list = q.list();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Campo findByNameValid(Campo campo, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Campo c where c.nome = :nome");
		
		if(campo.getId() != null) {
			sb.append(" and c.id != :id");
		}

		if(empresa.getId() != null) {
			sb.append(" and c.empresa.id = :empresa");
		}

		Query q = createQuery(sb.toString());
		
		q.setParameter("nome", campo.getNome());
		
		if(campo.getId() != null) {
			q.setParameter("id", campo);
		}
		
		if(empresa.getId() != null) {
			q.setParameter("empresa", empresa.getId());
		}
		
		List<Campo> list = q.list();
		Campo a = (Campo) (list.size() > 0 ? q.list().get(0) : null);
		return a;
	}
}