package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Usuario;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class TestePage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	
	@Override
	public void onInit() {
		
		form.setMethod("post");
		
		form.add(new Submit("ok"));
		
		super.onInit();
	}
	
	@Override
	protected void xstream(XStream x) {
		super.xstream(x);
		x.alias("user", Usuario.class);
	}

	@Override
	protected Object execute() throws Exception {
		
		categoriaPostService.mountMap(getEmpresa());
		return new MensagemResult("ok","ERRO");
	}
}
