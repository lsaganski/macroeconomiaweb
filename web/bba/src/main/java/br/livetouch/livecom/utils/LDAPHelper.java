package br.livetouch.livecom.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPSearchResults;

import br.infra.util.Log;
import br.livetouch.livecom.connector.login.LoginConnector;

public class LDAPHelper {
	protected static final Logger log = Log.getLogger(LoginConnector.class);
	
	
	public static LDAPConnection connect(String host, int port, String loginAdmin, String passwdAdmin, String baseDn) throws LDAPException {
		LDAPConnection conn = new LDAPConnection();
		conn.connect(host, port);
		conn.bind(LDAPConnection.LDAP_V3, loginAdmin, passwdAdmin.getBytes());
		return conn;
	}
	
	public static List<LDAPAttribute> getAttributes(LDAPConnection conn, String search) throws LDAPException {
		if(conn == null) {
			return null;
		}
		String baseDn = conn.getAuthenticationDN();
		ArrayList<LDAPAttribute> attrs = new ArrayList<LDAPAttribute>();
		if(StringUtils.isNotEmpty(search)) {
			LDAPSearchResults results = conn.search(baseDn, LDAPConnection.SCOPE_SUB, search, null, false);
			
			while (results.hasMore()) {
				LDAPEntry entry = results.next();
				LDAPAttributeSet attribute = entry.getAttributeSet();

				@SuppressWarnings("rawtypes")
				java.util.Iterator it = attribute.iterator();
				while (it.hasNext()) {
					LDAPAttribute attr = (LDAPAttribute) it.next();
					attrs.add(attr);
				}
			}
		}
		return attrs;
	}
}
