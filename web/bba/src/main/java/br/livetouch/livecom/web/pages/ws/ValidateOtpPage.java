package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.token.TokenHelper;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextArea;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class ValidateOtpPage extends WebServiceXmlJsonPage {
	
	public Form form = new Form();
	public String otp;
	public String secret;
	
	public void onInit() {
		form.add(new TextField("secret"));
		form.add(new TextField("otp"));
		
		form.add(new Submit("validate"));
		
		setFormTextWidth(form, 600);
	};

	@Override
	protected Object execute() throws Exception {
		if(form.isValid()) {
			boolean ok = TokenHelper.validateTimeToken(secret, otp, 30, true);
			msg = ok ? "OK": "INVALID OTP";
			
		}
		return msg;
	}
}
