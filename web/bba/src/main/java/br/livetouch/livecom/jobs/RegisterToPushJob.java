package br.livetouch.livecom.jobs;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.pushserver.lib.PushService;

/**
 * Job para registrar no Push
 * 
 * @author rlech
 *
 */
@Service
public class RegisterToPushJob extends SpringJob {

	protected static final Logger log = Log.getLogger(RegisterToPushJob.class);

	public static final String EMPRESA_KEY = "empresaId";

	public static final String LOGIN_KEY = "login";
	public static final String IMEI_KEY = "IMEI_KEY";
	public static final String REG_ID_KEY = "REG_ID_KEY";
	public static final String DEVICE_NOME_KEY = "DEVICE_NOME_KEY";
	public static final String DEVICE_SO_KEY = "DEVICE_SO_KEY";
	public static final String DEVICE_SO_VERSION_KEY = "DEVICE_SO_VERSION_KEY";
	public static final String APP_VERSION_KEY = "APP_VERSION_KEY";
	public static final String APP_VERSION_CODE_KEY = "APP_VERSION_CODE_KEY";
	public static final String TOKEN_VOIP = "TOKEN_VOIP";

	@Override
	protected void execute(Map<String, Object> params) throws Exception {
		log.debug("RegisterToPushJob.execute()");

		if (params != null) {
			Long empresaId  = (Long) params.get(EMPRESA_KEY);
			String login = (String) params.get(LOGIN_KEY);
			String deviceIMEI= (String) params.get(IMEI_KEY);
			String deviceRegId= (String) params.get(REG_ID_KEY);
			String deviceNome= (String) params.get(DEVICE_NOME_KEY);
			String deviceSO= (String) params.get(DEVICE_SO_KEY);
			String deviceSOVersion= (String) params.get(DEVICE_SO_VERSION_KEY);
			String deviceAppVersion= (String) params.get(APP_VERSION_KEY);
			String deviceAppVersionCode= (String) params.get(APP_VERSION_CODE_KEY);
			String tokenVoip = (String) params.get(TOKEN_VOIP);

			ParametrosMap parametrosMap = ParametrosMap.getInstance(empresaId);

			PushService service = Livecom.getPushService(parametrosMap);

			String projeto = parametrosMap.get(Params.PUSH_SERVER_PROJECT, "Livecom");

			log.debug(">> Livecom Push Registrando device [" + deviceRegId + "], login: " + login + " - projeto: " + projeto);
			String json = service.register(projeto, login, deviceIMEI, deviceRegId, deviceSO, deviceSOVersion, deviceAppVersion, deviceAppVersionCode, deviceNome, "json", tokenVoip, null);
			log.debug("<< Livecom Push " + json);
		}
	}
}
