package br.livetouch.livecom.web.pages.cielo;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.admin.LivecomAdminPage;

/**
 * 
 */
@Controller
@Scope("prototype")
public class GraficoTransacoesPage extends LivecomAdminPage {

	public String msgErro;
	public int page;

	@Override
	public void onInit() {
		super.onInit();
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
