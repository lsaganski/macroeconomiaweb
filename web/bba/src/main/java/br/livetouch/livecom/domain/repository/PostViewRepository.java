package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostView;
import br.livetouch.livecom.domain.Usuario;

@Repository
public interface PostViewRepository extends net.livetouch.tiger.ddd.repository.Repository<PostView> {

	List<PostView> findAllByUser(Usuario u);

	PostView findByUserPost(Usuario u, Post p);

	List<PostView> findAllByPost(Post post);

	void visualizarPosts(List<Post> posts, Usuario user);

	void insertPostView(List<Post> posts, Usuario user);

}