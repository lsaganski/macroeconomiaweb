package br.livetouch.livecom.web.pages.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.report.RelatorioPage;

/**
 * Relatório de acessos
 * 
 */
@Controller
@Scope("prototype")
public class RelatoriosPage extends RelatorioPage {

	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
