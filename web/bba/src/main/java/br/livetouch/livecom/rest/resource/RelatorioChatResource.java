package br.livetouch.livecom.rest.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.RelatorioConversaFiltro;
import br.livetouch.livecom.domain.vo.StatusMensagemUsuarioVO;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/report")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class RelatorioChatResource extends MainResource{
	@GET
	@Path("/conversa")
	public List<ConversaVO> getConversas(@BeanParam RelatorioConversaFiltro filtro) throws DomainException {

		setAttributeSession(RelatorioConversaFiltro.SESSION_FILTRO_KEY, filtro);

		if (filtro.getUsuario() != null) {
			Usuario user = usuarioService.get(filtro.getUsuario());
			filtro.setUser(user);
		} else {
			filtro.setUser(null);
		}

		Long grupo = filtro.getGrupo();
		if (grupo != null) {
			Grupo group = grupoService.get(grupo);
			filtro.setGroup(group);
		} else {
			filtro.setGroup(null);
		}

		List<ConversaVO> conversas = mensagemService.findConversasByFilter(filtro);
		return conversas;
	}

	@GET
	@Path("/conversa/detalhes")
	public List<MensagemVO> getDetalhesConversa(@BeanParam RelatorioConversaFiltro filtro) throws DomainException {

		if (filtro == null) {
			return new ArrayList<MensagemVO>();
		}

		List<MensagemVO> mensagens = mensagemService.findDetalhesByConversa(filtro);
		return mensagens;
	}

	@GET
	@Path("/conversa/mensagem/detalhes/{id}")
	public List<StatusMensagemUsuarioVO> getMensagemDetalhes(@PathParam("id") Long id) throws DomainException {
		
		if (id == null) {
			return new ArrayList<StatusMensagemUsuarioVO>();
		}
		
		Mensagem mensagem = mensagemService.get(id);
		if(mensagem.isGrupo()) {
			List<StatusMensagemUsuarioVO> mensagens = mensagemService.findDetalhesByMensagem(id);
			return mensagens;
		} else {
			ArrayList<StatusMensagemUsuarioVO> list = new ArrayList<StatusMensagemUsuarioVO>();
			list.add(new StatusMensagemUsuarioVO(mensagem));
			return list;
		}
	}

}
