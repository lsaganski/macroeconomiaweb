package br.livetouch.livecom.web.pages.root;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.LivecomRootPage;
import net.livetouch.click.control.link.Link;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.hibernate.HibernateUtil;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.extras.control.LinkDecorator;
import net.sf.ehcache.Cache;

@Controller
@Scope("prototype")
public class CachePage extends LivecomRootPage {

	public PaginacaoTable table = new PaginacaoTable();
	public ActionLink detalhes = new ActionLink("detalhes", this, "detalhes");
//	public ActionLink editLink = new ActionLink("editar", this,"editar");
	public Link deleteLink = new Link("deletar", this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public List<Cache> caches;
	
	@Override
	public void onInit() {
		super.onInit();

		table();

	}
	
	private void table() {
		Column c = null;
		
		c = new Column("name");
		table.addColumn(c);

		ActionLink[] links = new ActionLink[] {deleteLink, detalhes };
		c = new Column("detalhes");
		c.setDecorator(new LinkDecorator(table, links, "name"));
		table.addColumn(c);
		table.setClass("tabelaListagens");

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean deletar() {
		try {
			String value = deleteLink.getValue();
			HibernateUtil.deleteCache(value);
			setFlashAttribute("msg", "Cache deletado com sucesso: " + value);
			
			refresh();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = "Feriado possui relacionamentos. Não foi possível excluir.";
		}
		return true;
	}
	
	public boolean detalhes() {
		DetalhesCachePage page = (DetalhesCachePage) getContext().createPage(DetalhesCachePage.class);
		page.key = detalhes.getValue();
		setForward(page);
		return false;
	}
	
	@Override
	public void onRender() {
		super.onRender();

		caches = HibernateUtil.getCaches();
		
		addModel("caches", caches);

		// Count(*)
		int pageSize = 100;

		table.setPageSize(pageSize);
		table.setRowList(caches);
	}
}
