package br.livetouch.livecom.web.pages.report;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;

import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

@Controller
@Scope("prototype")
public class DetalhesLoginReportPage extends RelatorioPage {

	public String data;
	public Form form = new Form();
	public Checkbox agrupar = new Checkbox();
	public String filtroJson;
	public RelatorioFiltro filtro;

	@Override
	public void onInit() {
		
		bootstrap_on = true;
		escondeChat = true;

		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			setRedirect(LoginReportPage.class);
		}

		if (filtro != null) {
			if (data != null) {
				filtro.setDataIni(data);
				filtro.setDataFim(data);
			}
			filtro.setUsuario(null);
			filtroJson = new Gson().toJson(filtro);
		}

		form();
	}

	public void form() {

		form.add(agrupar);

		Submit tfiltrar = new Submit("filtrar", getMessage("filtrar.label"), this, "filtrar");
		tfiltrar.setAttribute("class", "btn btn-primary min-width");
		form.add(tfiltrar);

		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "btn btn-outline blue min-width");
		form.add(tExportar);

	}

	public boolean exportar() throws DomainException {
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new RelatorioFiltro();
			getContext().setSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);
		filtro.setExpandir(true);

		String csv = reportService.csvDetalhesLoginReport(filtro);
		download(csv, "detalhes-login-report.csv");

		return true;
	}

	@Override
	public String getContentType() {
		return super.getContentType();
	}

	@Override
	public String getTemplate() {
		return super.getTemplate();
	}

	@Override
	public void onRender() {
		super.onRender();

	}
}
