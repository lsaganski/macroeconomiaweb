package br.livetouch.livecom.domain.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.ArquivoRepository;
import br.livetouch.livecom.domain.repository.ArquivoThumbRepository;
import br.livetouch.livecom.domain.repository.impl.ArquivoRepositoryImpl.TipoBuscaArquivo;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.vo.ArquivoFiltro;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.files.FileManagerFactory;
import br.livetouch.livecom.utils.FileExtensionUtils;
import br.livetouch.livecom.utils.HostUtil;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ArquivoServiceImpl extends LivecomService<Arquivo> implements ArquivoService {
	protected static final Logger log = Log.getLogger(ArquivoService.class);

	@Autowired
	private ArquivoRepository rep;
	
	@Autowired
	private ArquivoThumbRepository repThumb;
	
	@Autowired
	protected NotificationService notificationService;
	
	@Override
	public Arquivo get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Arquivo c) throws DomainException {
		rep.saveOrUpdate(c);
	}
	
	@Override
	public void saveOrUpdate(ArquivoThumb t) {
		repThumb.saveOrUpdate(t);
	}
	
	@Override
	public void delete(ArquivoThumb t) {
		repThumb.delete(t);
	}

	@Override
	public List<Arquivo> findAll() {
		return rep.findAll(true);
	}
	
	@Override
	public List<Long> findIdsByUser(Usuario u) {
		return rep.findIdsByUser(u);
	}
	
	@Override
	public void delete(Arquivo a) throws DomainException {
		delete(null,a, false);
	}
	
	@Override
	public void delete(Usuario userInfo, Arquivo a, boolean deleteArquivosAmazon) throws DomainException {
		List<Long> ids = new ArrayList<>();
		ids.add(a.getId());
		delete(userInfo, ids, deleteArquivosAmazon);
	}

	@Override
	public void delete(Usuario userInfo, List<Long> ids, boolean deleteArquivosAmazon) throws DomainException {
		if(ids == null || ids.isEmpty()) {
			return;
		}
		
		// Params
		Arquivo first = get(ids.get(0));
		ParametrosMap map = getParams(first);
		
		Arquivo a = null;
		
		try {
			/**
			 * Batchs
			 */
			//  remover Thumbs nao amazon.
			executeIn("delete from ArquivoThumb where arquivo.id in (:ids)","ids", ids);
			
			executeNativeIn("delete from arquivos_tags where arquivo_id in (:ids)","ids", ids);
			executeIn("update Evento set foto = null where foto.id in (:ids)","ids", ids);
			executeIn("update Grupo set foto = null where foto.id in (:ids)","ids", ids);
//			executeIn("update GrupoMensagem set foto = null where foto.id in (:ids)","ids", ids);
//			executeIn("update GrupoMensagem set foto = null where foto.id in (:ids)","ids", ids);
			executeIn("update Palestrante set foto = null where foto.id in (:ids)","ids", ids);
			executeIn("update Usuario set foto = null where foto.id in (:ids)","ids", ids);
			executeIn("update Favorito set arquivo = null where arquivo.id in (:ids)","ids", ids);
			
			executeIn("update Arquivo set arquivoRef = null where id in (:ids)","ids", ids);
			executeIn("delete Arquivo where arquivoRef.id in (:ids)","ids", ids);
			
			notificationService.deleteBy(Arquivo.class,ids);

			/**
			 * 1 a 1
			 */
			for (Long id : ids) {

				// Atualmente nao exclui da Amazon.
				// Cuidado para não excluir se tiver no localhost
				boolean localhost = HostUtil.isLocalhost();
				if(deleteArquivosAmazon && !localhost) {
					
					a = get(id);
					
					String url = a.getUrl();
					if(url != null) {
						try {
							FileManagerFactory.getFileManager(map).delete(url);
						} catch (IOException e) {
							log.error(e.getMessage(), e);
						}
					}
				}
			}

			executeIn("delete Arquivo where id in (:ids)","ids", ids);

		} catch (Exception e) {
			log.error(e.getMessage(), e);

			throw new DomainException("Não foi possível excluir o arquivo [" + a + "]", e);
		}
	}

	private ParametrosMap getParams(Arquivo a) {
		Empresa empresa = a != null ? a.getEmpresa() : null;
		ParametrosMap map = null;
		if(empresa == null) {
			map = ParametrosMap.getInstance(1L);
		} else {
			map = ParametrosMap.getInstance(empresa.getId());
		}
		return map;
	}

	@Override
	public List<Arquivo> findAllByIds(List<Long> ids) {
		if(ids == null || ids.isEmpty()) {
			return new ArrayList<Arquivo>();
		}
		return rep.findAllByKeys(ids);
	}

	@Override
	public List<Arquivo> findAllByUser(Usuario usuario, String nome,List<Tag> tags, TipoBuscaArquivo tipoBusca, int page, int pageSize) {
		return rep.findAllByUser(usuario, nome,tags, tipoBusca,page, pageSize);
	}
	
	@Override
	public List<Arquivo> findAllByTipo(Usuario usuario, String[] tipoArquivo, TipoBuscaArquivo tipoBusca, int page, int pageSize, String nome) {
		return rep.findAllByTipo(usuario, tipoArquivo, tipoBusca, page, pageSize, nome);
	}
	
	@Override
	public List<FileVO> toListVo(Usuario user, List<Arquivo> arquivos, String tipo) {
		List<FileVO> filesVos = new ArrayList<FileVO>();
		for (Arquivo a : arquivos) {
			if(a != null) {
				boolean isImage = FileExtensionUtils.isImage(a.getNome());
				boolean isVideo = FileExtensionUtils.isVideo(a.getNome());
				if(StringUtils.equalsIgnoreCase("image", tipo) && isImage) {
					FileVO vo = toListVO(user, a);
					filesVos.add(vo);
				} else if(StringUtils.equalsIgnoreCase("video", tipo) && isVideo) {
					FileVO vo = toListVO(user, a);
					filesVos.add(vo);
				} else if(StringUtils.equalsIgnoreCase("arquivo", tipo) && !isVideo && !isImage) {
					FileVO vo = toListVO(user, a);
					filesVos.add(vo);
				} else if(StringUtils.isEmpty(tipo)) {
					FileVO vo = toListVO(user, a);
					filesVos.add(vo);
				}
			}
		}
		return filesVos;
	}

	@Override
	public FileVO toListVO(Usuario user, Arquivo arquivo) {
		FileVO vo = new FileVO();
		vo.setArquivo(arquivo, true);
		return vo;
	}

	@Override
	public Arquivo duplicar(Arquivo a, Long destaqueId) throws DomainException {
		Arquivo ar = new Arquivo();

		ar.copyArquivo(a);
		setDestaque(a, destaqueId, ar);
		
		saveOrUpdate(ar);
		return ar;
	}

	@Override
	public void setDestaque(Arquivo old, Long destaqueId, Arquivo novo) {
		novo.setDestaque(false);
		if(destaqueId != null && old.getId().equals(destaqueId)) {
			novo.setDestaque(true);
		}
	}

	@Override
	public void setDestaque(Long destaqueId, Arquivo ar) {
		ar.setDestaque(false);
		if(destaqueId != null && ar.getId().equals(destaqueId)) {
			ar.setDestaque(true);
		}
	}

	@Override
	public ArquivoThumb find(Long id) {
		return rep.find(id);
	}

	@Override
	public List<Arquivo> findAllByUser(ArquivoFiltro arquivoFiltro) {
		return rep.findAllByUser(arquivoFiltro);
	}

	@Override
	public List<Arquivo> findAll(Empresa e) {
		return rep.findAll(e);
	}

	@Override
	public List<Arquivo> findAllByTag(Tag tag) {
		return rep.findAllByTag(tag);
	}

	@Override
	public List<Long> findIdsByComentarios(Comentario c) {
		return rep.findIdsByComentarios(c);
	}

	@Override
	public void updateFileDestaquePost(Post post) {
		rep.updateFileDestaquePost(post);
	}

	@Override
	public void deleteNotInCategorias(List<Long> arquivosIds, CategoriaPost categoria) {
		rep.deleteNotInCategorias(arquivosIds, categoria);
	}

	@Override
	public List<Long> getIdsArquivosNotIn(Post p, List<Long> arquivoIds) {
		return rep.getIdsArquivosNotIn(p, arquivoIds);
	}

}
