package br.livetouch.livecom.rest.resource.cielo;

import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.OperacoesCielo;
import br.livetouch.livecom.domain.vo.OperacoesCieloVO;
import br.livetouch.livecom.rest.resource.MainResource;
import br.livetouch.livecom.utils.DateUtils;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/transacoes")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class OperacoesCieloResource extends MainResource {

	protected static final Logger log = Log.getLogger(OperacoesCieloResource.class);

	@GET
	@Path("/picotps")
	public Response getPicoTps() throws DomainException {
		
		List<OperacoesCieloVO> picos = transacaoService.graficoPicoTps();
		return Response.ok(picos).build();

	}
	
	@GET
	@Path("/momento")
	public Response getMomento() throws DomainException {
		
		List<OperacoesCieloVO> momentos = transacaoService.graficoMomento();
		return Response.ok(momentos).build();

	}
	
	@GET
	@Path("/transacao")
	public Response getTransacao() throws DomainException {
		
		List<OperacoesCieloVO> transacoes = transacaoService.graficoTransacao();
		return Response.ok(transacoes).build();

	}
	
	@GET
	@Path("/media")
	public Response getMedia() throws DomainException {
		
		//Data atual e data ano anterior
		Date dataAtual = new Date();
		Date dataPassada = DateUtils.sameWeekDayOtherYear(dataAtual);
		
		//Transações em referencia as datas
		OperacoesCielo transacao = transacaoService.getByData(dataAtual);
		OperacoesCielo transacaoPassado = transacaoService.getByData(dataPassada);
		
		if(transacao == null || transacaoPassado == null) {
			return Response.ok("Impossivel calcular a média, não há valores referencias para essas datas!").build();
		}
		
		Double atual = transacao.getVolumetria();
		Double anterior = transacaoPassado.getVolumetria();
		
		Double coeficiente = (atual - anterior) / anterior;
		
		coeficiente = coeficiente * 100;
		
		return Response.ok(coeficiente).build();

	}

}