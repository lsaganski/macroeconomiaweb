package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.enums.TipoTemplate;

@Repository
public interface TemplateEmailRepository extends net.livetouch.tiger.ddd.repository.Repository<TemplateEmail> {

	List<TemplateEmail> findAll(Empresa e);

	TemplateEmail findByType(Empresa empresa, TipoTemplate tipo);

	TemplateEmail existe(Empresa empresa, TemplateEmail templateEmail);

	List<TemplateEmail> findByTipo(Empresa empresa, TipoTemplate tipo);
	
}