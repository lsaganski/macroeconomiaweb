package br.livetouch.livecom.domain.repository;

import java.util.List;

import br.livetouch.livecom.domain.Token;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.repository.Repository;

public interface TokenRepository extends Repository<Token>{

	List<Token> findAllByUser(Usuario u);
	
	Token findLastToken(Long userId, String token, String so);

}