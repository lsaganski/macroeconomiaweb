package br.livetouch.livecom.web.pages.ws;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.EntityField;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.vo.CategoriaVO;
import net.sf.click.control.Form;
import net.sf.click.control.TextField;

/**
 * @author Juillian Lee 
 * 
 */
@Controller
@Scope("prototype")
public class CategoriaPage extends WebServiceXmlJsonPage {
	public Form form = new Form();
	private EntityField<CategoriaPost> tCategoria;
	private EntityField<CategoriaPost> tCategoriaPai;
	private TextField tPadrao;

	protected void form() {
		form.add(tCategoria = new EntityField<>("id", categoriaPostService));
		form.add(new TextField("nome", true));
		form.add(new TextField("cor"));
		form.add(new TextField("codigo"));
		form.add(tPadrao = new TextField("padrao"));
		form.add(new TextField("arquivo_ids"));
		form.add(tCategoriaPai = new EntityField<>("categoria_pai", categoriaPostService));
	}
	
	@Override
	public void onInit() {
		super.onInit();
		form();
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			CategoriaPost categoriaPost = tCategoria.getEntity();
			if(categoriaPost == null) {
				categoriaPost = new CategoriaPost();
			}
			form.copyTo(categoriaPost);
			if(categoriaPost.getId() == null){
				categoriaPost.setOrdem(99999);
			}

			CategoriaPost categ = tCategoriaPai.getEntity();
			if (categ != null) {
				categoriaPost.setCategoriaParent(categ);
			} else {
				categoriaPost.setCategoriaParent(null);
			}
			
			
			if(StringUtils.equals("true", tPadrao.getValue())) {
				categoriaPost.setPadrao(true);
			} else {
				categoriaPost.setPadrao(false);
			}
			
			if(categoriaPost.getCor() != null) {
				if(StringUtils.contains(categoriaPost.getCor(), "#")) {
					categoriaPost.setCor(categoriaPost.getCor().replace("#", ""));
				}
			}
			
			categoriaPostService.saveOrUpdate(getUsuario(), categoriaPost);
			
			List<Long> arquivoIds = getListIds("arquivo_ids");
			categoriaPostService.deleteArquivos(categoriaPost.getId(), arquivoIds);
			Set<Arquivo> arquivos = new HashSet<>();
			for (Long id : arquivoIds) {
				Arquivo arquivo = arquivoService.get(id);
				if(arquivo != null) {
					arquivo.setCategoria(categoriaPost);
					arquivoService.saveOrUpdate(arquivo);
					arquivos.add(arquivo);
				}
			}
			categoriaPost.setArquivos(arquivos);
			
			CategoriaVO categoriaVO = new CategoriaVO(categoriaPost);
			if(categoriaPost.getCategoriaParent() != null){
				CategoriaPost pai = categoriaPost.getCategoriaParent();
				categoriaVO.setCategoriaPai(new CategoriaVO(pai));
			}
			return categoriaVO;
			
		}
		return new MensagemResult("NOK", "Formulario invalido");
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("categoria", CategoriaVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
