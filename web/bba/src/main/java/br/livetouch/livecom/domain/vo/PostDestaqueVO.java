package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostDestaque;
import br.livetouch.livecom.domain.Usuario;

public class PostDestaqueVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String url;
	private String descricao;
	private String titulo;
	private int tipo;
	private String link;
	private String textoNotificacao;
	private Long postId;
	private String usuario;

	public void setDestaque(PostDestaque postDestaque) {
		this.url = postDestaque.getUrlImagem();
		this.descricao = postDestaque.getDescricaoUrl();
		this.titulo = postDestaque.getTituloUrl();
		this.tipo = postDestaque.getTipo();
		Post post = postDestaque.getPost();
		if(post != null) {
			this.postId = post.getId();
			Usuario usuario = post.getUsuario();
			this.usuario = usuario.getNome();
		}
		this.setLink(postDestaque.getUrlSite());
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getTitulo() {
		return titulo;
	}
	
	public String setTextoNotificacao() {
		return this.textoNotificacao;
	}
	
	public String getTextoNotificacao() {
		return textoNotificacao;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public static List<PostDestaqueVO> fromList(List<PostDestaque> all) {
		if(all != null && all.size() > 0) {
			List<PostDestaqueVO> list = new ArrayList<>();
			for (PostDestaque p : all) {
				PostDestaqueVO vo = new PostDestaqueVO();
				vo.setDestaque(p);
				list.add(vo);
			}
			return list;
		}
		return new ArrayList<>();
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
