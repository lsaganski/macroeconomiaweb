package br.livetouch.livecom.domain.exception;

import org.apache.commons.lang.StringUtils;

import net.livetouch.tiger.ddd.DomainException;

public class SenhaInvalidaException extends DomainException{
	private static final long serialVersionUID = 3579513859913195559L;

	private String errorCode;
	
	public SenhaInvalidaException(String message) {
		super(message);
	}

	public SenhaInvalidaException(String message, String errorCode) {
		super(message);
		this.setErrorCode(errorCode);
	}

	public String getErrorCode() {
		return StringUtils.isNotEmpty(errorCode) ? errorCode : "senha";
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}
