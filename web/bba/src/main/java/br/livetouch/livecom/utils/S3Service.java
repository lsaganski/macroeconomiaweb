package br.livetouch.livecom.utils;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.amazonaws.services.s3.model.Owner;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import br.livetouch.livecom.domain.ParametrosMap;

public class S3Service {
	private S3ObjectSummary o;
	private ParametrosMap map;

	public S3Service(S3ObjectSummary obj, ParametrosMap map) {
		o = obj;
		this.map = map;
	}

	public String getUrl() {
		String path = getKey();
		S3Helper s3Helper = new S3Helper(this.map);
		return s3Helper.getFileURL(path);
	}

	public String getFileName() {
		String path = getKey();
		String[] split = StringUtils.split(path, "/");
		if (split != null && split.length == 2) {
			return split[1];
		}
		return path;
	}

	public String getFileExt() {
		String path = getKey();
		String[] split = StringUtils.split(path, ".");
		if (split != null && split.length == 2) {
			return StringUtils.lowerCase(split[1]);
		}
		return path;
	}

	public boolean equals(Object arg0) {
		return o.equals(arg0);
	}

	public String getBucketName() {
		return o.getBucketName();
	}

	public String getETag() {
		return o.getETag();
	}

	public String getKey() {
		return o.getKey();
	}

	public Date getLastModified() {
		return o.getLastModified();
	}

	public Owner getOwner() {
		return o.getOwner();
	}

	public long getSize() {
		return o.getSize();
	}

	public String getStorageClass() {
		return o.getStorageClass();
	}

	public int hashCode() {
		return o.hashCode();
	}

	public void setBucketName(String bucketName) {
		o.setBucketName(bucketName);
	}

	public void setETag(String eTag) {
		o.setETag(eTag);
	}

	public void setKey(String key) {
		o.setKey(key);
	}

	public void setLastModified(Date lastModified) {
		o.setLastModified(lastModified);
	}

	public void setOwner(Owner owner) {
		o.setOwner(owner);
	}

	public void setSize(long size) {
		o.setSize(size);
	}

	public void setStorageClass(String storageClass) {
		o.setStorageClass(storageClass);
	}

	public String toString() {
		return o.toString();
	}

}
