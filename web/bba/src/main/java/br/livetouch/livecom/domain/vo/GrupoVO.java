package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.DateUtils;

public class GrupoVO implements Comparable<GrupoVO> {
	public Long id;
	public String nome;
	public String codigo;
	public boolean padrao;
	public boolean favorito;
	public boolean participa;
	public boolean postar;
	public String statusSolicitacao;
	public Long countUsers;
	public Long countSubgrupos;
	public Long countPosts;
	public String dataEntrada;
	public String dataSaida;
	public String userAdmin;
	public Long userAdminId;
	public Boolean havePost;
	public String urlFoto;
	public String urlFotoThumb;
	public String tipo;
	public boolean virtual;
	public CategoriaVO categoria;
	
	public GrupoVO() {
	}
	
	public GrupoVO(Grupo grupo, GrupoUsuarios gu, Long countUsers, Long countSubgrupos) {
		this.setGrupo(grupo);
		
		if(gu != null) {
			this.countUsers = countUsers;
			this.countSubgrupos = countSubgrupos;
			this.favorito = gu.isFavorito();
			this.participa = true;
			this.statusSolicitacao = gu.getStatusParticipacaoString();
		} else {
			this.countUsers = countUsers;
			this.countSubgrupos = countSubgrupos;
			this.favorito = false;
			this.participa = false;
			this.statusSolicitacao = "";
		}
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setPadrao(boolean padrao) {
		this.padrao = padrao;
	}
	
	public boolean isPadrao() {
		return padrao;
	}

	@Override
	public String toString() {
		return "GrupoVO [id=" + id + ", nome=" + nome + ", padrao=" + padrao + ", favorito=" + favorito + "]";
	}
	
	public Long getCountUsers() {
		return countUsers;
	}

	public Long getCountSubgrupos() {
		return countSubgrupos;
	}

	public void setGrupo(Grupo g) {
		if(g != null) {
			this.setId(g.getId());
			this.setNome(g.getNome());
			this.setPadrao(g.isPadrao());
			this.urlFoto = g.getUrlFoto();
			this.urlFotoThumb = g.getFoto() != null ? g.getFoto().getUrlThumb() : g.getUrlFotoCapa();
			this.dataEntrada = DateUtils.toString(g.getHoraAbertura(), "HH:mm");
			this.dataSaida = DateUtils.toString(g.getHoraFechamento(), "HH:mm");
			Usuario usuario = g.getUserCreate();
			this.userAdmin = "";
//			this.countPosts = g.getPosts() != null ? g.getPosts().size() : 0L;
			if(usuario != null) {
				this.userAdmin = usuario.getNome();
				this.userAdminId = usuario.getId();
			}
			
			this.havePost = false;		
			this.tipo = g.getTipoGrupo() != null ? g.getTipoGrupo().name() : "";
			this.virtual = g.isGrupoVirtual();
		}
	}
	
	public static List<GrupoVO> toList(List<Grupo> grupos) {
		List<GrupoVO> newList = new ArrayList<GrupoVO>();
		for (Grupo g : grupos) {
			GrupoVO vo = new GrupoVO();
			vo.setGrupo(g);
			newList.add(vo);
		}
		return newList;
	}

	@Override
	public int compareTo(GrupoVO o) {
        return (this.nome.compareTo(o.nome));
	}

}
