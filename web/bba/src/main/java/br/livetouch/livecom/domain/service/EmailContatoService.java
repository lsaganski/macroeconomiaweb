package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.EmailContato;
import net.livetouch.tiger.ddd.DomainException;

public interface EmailContatoService extends Service<EmailContato> {

	List<EmailContato> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(EmailContato f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(EmailContato f) throws DomainException;

}
