package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.EmailReport;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.vo.RelatorioEmailVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.tiger.ddd.DomainException;

public interface EmailReportService extends Service<EmailReport> {

	List<EmailReport> findAll(Empresa e);
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(EmailReport e) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(EmailReport e) throws DomainException;

	List<RelatorioEmailVO> reportEmail(RelatorioFiltro filtro) throws DomainException;
	
}
