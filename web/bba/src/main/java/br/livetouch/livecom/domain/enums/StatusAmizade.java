package br.livetouch.livecom.domain.enums;

/**
 * @author Ricardo Lecheta
 * @created 23/02/2013
 */
public enum StatusAmizade {

	APROVADA("APROVADA","Conexão aprovada"),
	RECUSADA("RECUSADA","Conexão recusada"),
	SOLICITADA("SOLICITADA","Conexão solicitada");
	private final String s;
	private final String desc2;

	StatusAmizade(String s,String desc2){
		this.s = s;
		this.desc2 = desc2;
	}

	@Override
	public String toString() {
		return s != null ? s : "?";
	}

	public String getDesc() {
		return s;
	}
	
	public String getDesc2() {
		return desc2;
	}
}
