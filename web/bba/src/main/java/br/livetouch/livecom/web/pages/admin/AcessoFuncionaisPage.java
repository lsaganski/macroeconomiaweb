package br.livetouch.livecom.web.pages.admin;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioFuncionaisVO;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.Context;
import net.sf.click.control.Column;
import net.sf.click.control.Decorator;
import net.sf.click.control.Form;
import net.sf.click.control.Option;
import net.sf.click.control.Select;
import net.sf.click.control.Submit;
import net.sf.click.control.Table;
import net.sf.click.control.TextArea;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.DateField;
import net.sf.click.extras.control.IntegerField;

/**
 * Relatório de acessos
 * 
 */
@Controller
@Scope("prototype")
public class AcessoFuncionaisPage extends RelatorioPage {

	private RelatorioFiltro filtro;
	
	public PaginacaoTable table = new PaginacaoTable();
	public Table tableNaoExpandido = new Table();
	public Form form = new Form();
	public TextArea textarea = new TextArea();
	//
	public Select expandir = new Select();
	DateField tDataInicio = new DateField("dataInicial", getMessage("dataInicio.label"));
	DateField tDataFim = new DateField("dataFinal", getMessage("dataFim.label"));
	String dataInicio;
	String dataFim;
	int page;
	private IntegerField tMax = new IntegerField("max");
	public TextField tAcessoFuncionais;

	@Override
	public void onInit() {
		super.onInit();
		
		bootstrap_on = true;
		escondeChat = true;

		if (clear) {
			getContext().removeSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		}
		
		this.textarea.setCols(100);
		this.textarea.setRows(4);
		this.textarea.setReadonly(true);
		
		table();
		
		tableNaoExpandido();

		form();
	}

	private void table() {
		
		Column c = new Column("login", getMessage("coluna.funcional.label"));
		c.setAttribute("align", "left");
		c.setDecorator(new Decorator() {
			@Override
			public String render(Object object, Context context) {
				RelatorioFuncionaisVO vo = (RelatorioFuncionaisVO) object;
				String link = String.format("<a href=\"%s/usuario.htm?id=%s\">%s</a>", getContextPath(), vo.getUserId(), vo.getLogin());
				return link;
			}

		});
		table.addColumn(c);

	}
	
	private void tableNaoExpandido() {
		Column c = new Column("count", getMessage("funcionais.acessaram.label"));
		c.setAttribute("align", "left");
		tableNaoExpandido.addColumn(c);

	}

	public void form() {
		tDataInicio.setAttribute("class", "input data datepicker");
		tDataFim.setAttribute("class", "input data datepicker");
		tDataInicio.setFormatPattern("dd/MM/yyyy");
		tDataFim.setFormatPattern("dd/MM/yyyy");

		tDataInicio.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		tDataFim.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		
		tDataInicio.setMaxLength(10);
		tDataFim.setMaxLength(10);

		form.add(tDataInicio);
		form.add(tDataFim);
		
		expandir.add(new Option("consolidado", "Consolidado"));
		expandir.add(new Option("expandido", "Expandido"));
		form.add(expandir);
		
		tAcessoFuncionais = new TextField("acessoFuncionais", getMessage("funcionais.acessaram.label"));
		tAcessoFuncionais.setAttribute("class", "input");
		tAcessoFuncionais.setReadonly(true);
		form.add(tAcessoFuncionais);

		Submit tFiltrar = new Submit("filtrar", getMessage("filtrar.label"), this, "filtrar");
		tFiltrar.setAttribute("class", "btn btn-primary min-width");
		form.add(tFiltrar);
		
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "btn btn-outline blue min-width");
		form.add(tExportar);

	}

	public boolean filtrar() {
		
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new RelatorioFiltro();
			getContext().setSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);
		
		if (!expandir.getValue().equals("expandido")) {
			filtrarResumido();
			return true;
		}
		
		return true;
		
	}

	public boolean filtrarResumido() {
		List<RelatorioFuncionaisVO> acessos;
		
		try {
			acessos = usuarioService.getFuncionais(filtro, true, getEmpresa());
			//String result = getMessage("funcionais.acessaram.label") + " = ";
			//result += String.valueOf(acessos.get(0).getCount());
//			tAcessoFuncionais.setValue(String.valueOf(acessos.get(0).getCount()));
			tableNaoExpandido.setRowList(acessos);

			//this.textarea.setValue(result);
		} catch (Exception e) {
			this.msg = e.getMessage();
		}
		return true;
	}
	
	public boolean exportar() throws DomainException {
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new RelatorioFiltro();
			getContext().setSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);
		filtro.setExpandir(true);
		
		String csv = logService.csvFuncionais(filtro, getEmpresa());
		download(csv, "relatorio-funcionais.csv");
		
		return true;
	}

	@Override
	public void onGet() {
		super.onGet();

		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro != null) {
			form.copyFrom(filtro);
			if(expandir.getValue().equals("expandido")) {
				filtro.setExpandir(true);
			} else {
				filtro.setExpandir(false);
			}
			filtro.setPage(page);
		}
	}
	
	@Override
	public void onRender() {
		super.onRender();

		if (filtro != null) {
			Integer max = tMax.getInteger();
			if (max == null) {
				max = 20;
				filtro.setMax(max);
			}

			List<RelatorioFuncionaisVO> funcionais;
			try {
				funcionais = usuarioService.getFuncionais(filtro, false, getEmpresa());
			} catch (DomainException e) {
				form.setError(e.getMessage());
				return;
			}

			table.setRowList(funcionais);
		}
	}
}
