package br.livetouch.livecom.web.pages.training;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 
 */
@Controller
@Scope("prototype")
public class SobrePage extends TrainingPage {
	
}
