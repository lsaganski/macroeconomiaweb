package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Faq;
import net.livetouch.tiger.ddd.DomainException;

public interface FaqService extends Service<Faq> {

	List<Faq> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Faq f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Faq f) throws DomainException;

}
