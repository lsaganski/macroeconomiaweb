package br.livetouch.livecom.push;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.chat.webSocket.WebSocketNotification;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PushReport;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.enums.TipoPush;
import br.livetouch.livecom.domain.service.ComentarioService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.PushReportService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.PushBadgeVO;
import br.livetouch.livecom.domain.vo.PushIdsVO;
import br.livetouch.pushserver.lib.DeviceFirebaseVO;
import br.livetouch.pushserver.lib.FirebaseVO;
import br.livetouch.pushserver.lib.PushService;
import br.livetouch.pushserver.lib.UserVO;

@Service
public class PushLivecomServiceImpl implements PushLivecomService {
	
	private static Logger log = Log.getLogger(PushLivecomService.class);	
	
	@Autowired
	private PushReportService pushReportService;
	
	@Autowired
	private ComentarioService comentarioService;
	
	@Autowired
	private PostService postService;

	@Autowired
	private NotificationService notificationService;

	private Empresa e;

	@Autowired
	private UsuarioService usuarioService;

	@Override
	public String push(long pushId, Usuario user, PushNotificationVO notification) throws IOException {
		List<Long> users = new ArrayList<>();
		users.add(user.getId());
		return push(pushId, users, notification);
	}

	@Override
	public String push(long pushId, Empresa empresa, PushNotificationVO notification) throws IOException {
		ParametrosMap params = ParametrosMap.getInstance(empresa.getId());
		String pushVersion = params.get(Params.PUSH_VERSION, "2");

		boolean firebaseOn = params.getBoolean(Params.PUSH_FIREBASE_ON, false);
		if(firebaseOn){
			return pushFirebase(params,pushId, null, notification, pushVersion);
		} else {
			return pushStrings(params,pushId, null, notification, pushVersion);
		}
	}

	@Override
	public String push(long pushId, List<Long> users, PushNotificationVO notification) throws IOException {
		if (users != null && users.size() > 0) {

			/**
			 * O push V2 é no formato json
			 */
			Usuario firstUser = usuarioService.get(users.get(0));
			e = firstUser.getEmpresa();
			ParametrosMap params = ParametrosMap.getInstance(e.getId());
			String pushVersion = params.get(Params.PUSH_VERSION, "2");
			PushIdsVO logins = null;

			if("1".equals(pushVersion)) {
				// Lista com logins com apenas o login
				logins = getLoginsListV1(notification,users);
			} else if("2".equals(pushVersion)) {
				// Lista com logins no formato JSON
				// [{"id":1,"login":"admin@livetouch.com.br","badge":"1"}, {. . . }]
				logins = getLoginsListV2(params,notification, users);
				
			}

			if(logins.size() > 0) {
				boolean firebaseOn = params.getBoolean(Params.PUSH_FIREBASE_ON, false);
				if(firebaseOn){
					return pushFirebase(params,pushId, logins, notification, pushVersion);
				} else {
					return pushStrings(params,pushId, logins, notification, pushVersion);
				}
			}
		}
		return null;
	}

	@Override
	public String silent(List<Long> users, PushNotificationVO notification) throws IOException {
		if (users != null && users.size() > 0) {
			
			Usuario firstUser = usuarioService.get(users.get(0));
			e = firstUser.getEmpresa();
			ParametrosMap params = ParametrosMap.getInstance(e.getId());
			PushIdsVO logins = null;
			
			// Lista com logins com apenas o login
			logins = getLoginsListV1(notification,users);
			
			if(logins.size() > 0) {
				return silent(params, logins, notification);
			}
		}
		return null;
	}
	
	
	@Override
	public String sendToAll(ParametrosMap params, long pushId, PushNotificationVO notification) throws IOException {
		boolean firebaseOn = params.getBoolean(Params.PUSH_FIREBASE_ON, false);
		String pushVersion = params.get(Params.PUSH_VERSION, "2");
		if(params.isPushOn()) {
			if(firebaseOn){
				return pushFirebaseToAll(params,pushId, notification, pushVersion);
			} else {
				return pushStringsToAll(params,pushId, notification, pushVersion);
			}
		}
		return null;
	}

	private String pushStringsToAll(ParametrosMap params, long pushId, PushNotificationVO notification, String pushVersion) {
		String json = notification.toJson();
		String projeto = params.get("push.server.project","Livecom");
		String jsonResponse = null;
		String erro = null;

		try {
			PushService s = Livecom.getPushService(params);
			
			boolean sync = false;
			String title = notification.getTitle();
			String tituloOriginal = notification.getTituloOriginal();
			String dataAgendamento = notification.getDataAgendamento();
			String tipo = notification.getTipo();
			String subTituloNot = notification.getSubTitleNot();
			String tituloNot = notification.getTitleNot();
			Long msgId = notification.getMsgId();
			boolean processando = notification.isProcessando();

			// TODO PUSH MELHORAR. Aqui passar JSON?
			jsonResponse = s.push(projeto, "*toAll", pushId, title, tituloOriginal, tituloNot, subTituloNot, dataAgendamento, json, 0,sync, "json", tipo, msgId, processando, notification.isVoip());
			
			if(jsonResponse != null && jsonResponse.equals("{error: Connection refused: connect}")) {
				log.error("Erro ao fazer push: " + jsonResponse);
				jsonResponse = null;
			}
		} catch (Error e) {
			erro = e.getMessage();
			log.error("Erro ao fazer push: " + e.getMessage(),e);
		}
		
		if(jsonResponse != null) {
			try {
				JSONObject response = new JSONObject(jsonResponse);
				erro = response.optString("error");
				logPushReport(notification, erro);
			} catch (JSONException e) {
				log.error("Erro ao criar o json de retorno " + jsonResponse + " - " + e.getMessage(), e);
				return null;
			}
		}
		
		return jsonResponse;
	}

	private String pushFirebaseToAll(ParametrosMap params, long pushId, PushNotificationVO notification, String pushVersion) {
		
		String projeto = params.get("push.server.project","Livecom");
		String authorization = params.get(Params.PUSH_API_KEY,"");
		String jsonResponse = null;
		String erro = null;

		try {
			PushService s = Livecom.getPushService(params);
			String dataAgendamento = notification.getDataAgendamento();
			String tipo = notification.getTipo();
			
			
			String sound = params.get(Params.PUSH_NOTIFICATION_SOUND, "default");
			if(StringUtils.isNotEmpty(sound)) {
				notification.setSound(sound);				
			}
			
			FirebaseVO firebase = new FirebaseVO();
			JSONObject data = setData(pushId, notification, dataAgendamento, tipo);
			firebase.setProject(projeto);
			firebase.setAuthorization(authorization);
			firebase.setNotification(notification.toFirebaseJson());
			firebase.setData(data.toString());
			
			firebase.setMsgId(notification.getMsgId());
			firebase.setProcessando(notification.isProcessando());
			
			//Parametros
			setParams(params, notification, firebase);
			
			// marca que vai enviar push para todo mundo
			List<br.livetouch.pushserver.lib.UserVO> usuarios = new ArrayList<br.livetouch.pushserver.lib.UserVO>();
			UserVO userVO = new UserVO();
			userVO.codigo = "*toAll";
			usuarios.add(userVO);
			
			firebase.setUsers(usuarios);
			firebase.setVoip(notification.isVoip());
			jsonResponse = s.pushFirebase(firebase);
			
			if(jsonResponse != null && jsonResponse.equals("{error: Connection refused: connect}")) {
				log.error("Erro ao fazer push: " + jsonResponse);
				erro = "Erro ao fazer push: " + jsonResponse;
				logPushReport(notification, erro);
				jsonResponse = null;
			}
		} catch (Error e) {
			erro = e.getMessage();
			logPushReport(notification, erro);
			log.error("Erro ao fazer push: " + e.getMessage(),e);
		}
		
		return jsonResponse;
	}

	/**
	 * Lista de logins
	 * @param notification 
	 */
	private PushIdsVO getLoginsListV1(PushNotificationVO notification, List<Long> ids) {
		Long empresaId = notification.getEmpresaId();
		int batchSize = ParametrosMap.getInstance(empresaId).getInt(Params.PUSH_BATCH_SIZE, 1000);
		
		List<String> logins = new ArrayList<String>();
		List<Long> list = new ArrayList<>();
		Integer i = 0;
		for (Long id : ids) {
			list.add(id);
			i++;
			if(i == batchSize) {
				i = 0;
				list.clear();
				logins.addAll(usuarioService.findLoginsByIds(list));
			}
		}

		if(list.size() > 0) {
			logins.addAll(usuarioService.findLoginsByIds(list));
		}

		PushIdsVO p = new PushIdsVO();
		p.setIds(ids);
		p.setLogins(logins);
		return p;
	}
	
	/**
	 * Lista de logins no formato JSON (id usuario, login, badge)
	 * 
	 * <pre>
	[{"id":1,"login":"admin@livetouch.com.br","badge":"1"}, {. . . }]
	 * </pre>
	 * @param params 
	 * 
	 * @param userIds
	 * @return
	 * @throws IOException 
	 */
	private PushIdsVO getLoginsListV2(ParametrosMap params, PushNotificationVO notification, List<Long> allUserIds) throws IOException {
		int batchSize = params.getInt(Params.PUSH_BATCH_SIZE, 1000);
		
		// Filtra para enviar push somente para usuários dentro do horário.
		List<Long> ids = getUserIdsDentroHorario(params,allUserIds);
		
		PushIdsVO p = new PushIdsVO();
		
		/**
		 * Long userId
		 * String login
		 * Long badge
		 */
		List<PushBadgeVO> badges = new ArrayList<>();

		/**
		 * Faz a busca em lotes de 1000 porque o subselect pode travar.
		 */
		int i = 0;
		List<Long> userIds = new ArrayList<>();
		for (Long id : ids) {
			userIds.add(id);
			i++;
			if(i == batchSize) {
				// 100 de cada vez
				List<PushBadgeVO> userBadges = getBadges(params,userIds);
				badges.addAll(userBadges);
				userIds.clear();
				i = 0;
			}
		}
		if(userIds.size() > 0 ) {
			List<PushBadgeVO> userBadges = getBadges(params,userIds);
			badges.addAll(userBadges);
		}
		
		/**
		 * Percorre as badges, validando o horario para ver se manda push.
		 */
		for (PushBadgeVO badge : badges) {

			JSONObject json = new JSONObject();
			try {
				Long userId = badge.getUserId();
				String login = badge.getLogin();
				Long count = badge.getCount();
				String nome = badge.getNome();
				String email = badge.getEmail();
				
				json.put("id", userId);
				json.put("codigo", login);
				json.put("nome", nome);
				json.put("email", email);
				json.put("badge", count);

				String jsonString = json.toString();

				p.add(userId,jsonString);
				
			} catch (JSONException e) {
				log.error("Erro ao criar o json " + e.getMessage());
				throw new IOException("Erro ao criar o json do push: " + e.getMessage(), e);
			}
		}
		
		return p;
	}

	/**
	 * Recebe lista de ids de usuarios e retorna apenas usuarios dentro do horário.
	 * 
	 * Controle execução por lote de 1000 em 1000 para subselect.
	 * @param allUserIds 
	 * @param params 
	 * 
	 * @param userIds
	 * @return
	 */
	private List<Long> getUserIdsDentroHorario(ParametrosMap params, List<Long> userIds) {
		int batchSize = params.getInt(Params.PUSH_BATCH_SIZE, 1000);
		
		List<Long> idsOk = new ArrayList<>();
		int i = 0;
		List<Long> list = new ArrayList<>();
		for (Long id : userIds) {
			list.add(id);
			i++;
			if(i == batchSize) {
				// 100 de cada vez
				idsOk.addAll(usuarioService.findAllUsuarioIdsDentroHorario(list));
				list.clear();
				i = 0;
			}
		}
		if(list.size() > 0 ) {
			idsOk.addAll(usuarioService.findAllUsuarioIdsDentroHorario(list));
		}
		return idsOk;
	}

	/**
	 * Retorna as badges dos usuarios
	 *
	 * user[0] = Long id
	 * user[1] = String login
	 * user[2] = Long badge
	 * 
	 * @param userIds
	 * @return
	 */
	private List<PushBadgeVO> getBadges(ParametrosMap params, List<Long> userIds) {
		boolean badgeChatOn = params.getBoolean(Params.PUSH_BADGES_CHAT_ON, true);
		boolean badgeComentarioOn = params.getBoolean(Params.PUSH_BADGES_COMENTARIO_ON, true);
		boolean badgeFavoritoOn = params.getBoolean(Params.PUSH_BADGES_FAVORITO_ON, true);
		boolean badgeLikeOn = params.getBoolean(Params.PUSH_BADGES_LIKE_ON, true);
		boolean badgePostOn = params.getBoolean(Params.PUSH_BADGES_POST_ON, true);
		

		// Map chave=userId
		Map<Long, PushBadgeVO> map = new LinkedHashMap<>();
		// Inicia o map
		updateBadges(map, userIds, null);

		if(badgeChatOn) {
			// TODO fazer badge de chat.
		} 
		if(badgeComentarioOn) {
			updateBadges(map, userIds, TipoNotificacao.COMENTARIO);
		} 
		if(badgeFavoritoOn) {
			updateBadges(map, userIds, TipoNotificacao.FAVORITO);
		} 
		if(badgeLikeOn) {
			updateBadges(map, userIds, TipoNotificacao.LIKE);
		} 
		if(badgePostOn) {
			updateBadges(map, userIds, TipoNotificacao.NEW_POST);
		}
		
		// Lista com todas as badges por usuario.
		List<PushBadgeVO> badges = new ArrayList<>(map.values());
		return badges;
	}

	/**
	 * Atualiza o map de badges dos usuarios. 
	 * Isso é para somar new_post + comentario + like + etc...
	 * 
	 * @param userIds
	 * @param map
	 * @param tipo
	 */
	private void updateBadges(Map<Long, PushBadgeVO> map, List<Long> userIds, TipoNotificacao tipo) {
		/**
		 * array[0] = Long userId
		 * array[1] = String login
		 * array[2] = badge post
		 * array[3] = nome do usuario
		 * array[4] = email do usuario
		 */
		List<Object[]> badges = notificationService.findBadgesToPush(tipo,userIds);
		for (Object[] array : badges) {
			Long userId = (Long) array[0];
			String login = (String) array[1];
			Long count = NumberUtils.toLong(String.valueOf(array[2]),0);
			String nome = (String) array[3];
			String email = (String) array[4];

			PushBadgeVO b = map.get(userId);
			if(b == null) {
				b = new PushBadgeVO(userId, login, count, nome, email);
				map.put(userId, b);
			} else {
				b.increment(count);
			}
		}
	}

	/**
	 * List<String> users = contém os logins para receber push
	 * @param pushVersion 
	 */
	public String pushStrings(ParametrosMap params,long pushId, PushIdsVO users, PushNotificationVO notification, String pushVersion) throws IOException {
		
		boolean pushOn = params.isPushOn();
		if(!pushOn) { 
			return null;
		}
		
		// ids pro web socket
		List<Long> ids = users != null ? users.getIds() : new ArrayList<Long>();
		
		// json com logins para push server, ex: {"badge":0,"codigo":"admin@livetouch.com.br"
		List<String> logins = users != null ? users.getLogins() : new ArrayList<String>();

		StringBuffer sb = new StringBuffer();
		if("1".equals(pushVersion)) {
			// Separa logins por ','
			boolean first = true;
			for (String login : logins) {
				if(!first) {
					sb.append(",");
				}
				first = false;
				sb.append(login);
			}
		} else if("2".equals(pushVersion)){
			sb.append(logins);
		}

		// Separa logins por ','
		String loginsSeparadosPorVirgula = sb.toString();
		
		String json = notification.toJson();

		String projeto = params.get("push.server.project","Livecom");

		String jsonResponse = null;
		
		String erro = null;

		try {
			PushService s = Livecom.getPushService(params);
			boolean qtde50 = logins.size() < 50;
			if(qtde50) {
				log.debug(">> Push users ["+loginsSeparadosPorVirgula+"], msg: " + json);
			} else {
				log.debug(">> Push users qtde ["+logins.size()+"], msg: " + json);
			}
			
			boolean sync = false;
			String title = notification.getTitle();
			String tituloOriginal = notification.getTituloOriginal();
			String dataAgendamento = notification.getDataAgendamento();
			String tipo = notification.getTipo();
			String subTituloNot = notification.getSubTitleNot();
			String tituloNot = notification.getTitleNot();
			Long msgId = notification.getMsgId();
			boolean processando = notification.isProcessando();

			// TODO PUSH MELHORAR. Aqui passar JSON?
			jsonResponse = s.push(projeto, loginsSeparadosPorVirgula, pushId, title, tituloOriginal, tituloNot, subTituloNot, dataAgendamento, json, 0,sync, "json", tipo, msgId, processando, notification.isVoip());
			
			if(jsonResponse != null && jsonResponse.equals("{error: Connection refused: connect}")) {
				log.error("Erro ao fazer push: " + jsonResponse);
				jsonResponse = null;
			}
			
			log.debug("Push web socket");
			WebSocketNotification.sendMsg(ids, notification);
			
			if(qtde50) {
				log.debug("<< Push users ["+loginsSeparadosPorVirgula+"], response:" + jsonResponse);
			} else {
				log.debug("<< Push users qtde ["+logins.size()+"], response:" + jsonResponse);
			}
			
		} catch (Error e) {
			erro = e.getMessage();
			log.error("Erro ao fazer push: " + e.getMessage(),e);
		}
		
		if(jsonResponse != null) {
			try {
				JSONObject response = new JSONObject(jsonResponse);
				erro = response.optString("error");
				logPushReport(notification, erro);
			} catch (JSONException e) {
//				e.printStackTrace();
				log.error("Erro ao criar o json de retorno " + jsonResponse + " - " + e.getMessage(), e);
				return null;
			}
		}
		
		return jsonResponse;
	}
	
	/**
	 * List<String> users = contém os logins para receber push
	 * @param pushVersion 
	 * @throws JSONException 
	 */
	public String pushFirebase(ParametrosMap params,long pushId, PushIdsVO users, PushNotificationVO notification, String pushVersion) throws IOException {

		boolean pushOn = params.isPushOn();
		if(!pushOn) {
			return null;
		}
		
		List<Long> ids = users != null ? users.getIds() : new ArrayList<Long>();
		List<String> logins = users != null ? users.getLogins() : new ArrayList<String>();
		List<br.livetouch.pushserver.lib.UserVO> usuarios = new ArrayList<br.livetouch.pushserver.lib.UserVO>();

		for (String login : logins) {
			JSONObject obj;
			try {
				obj = new JSONObject(login);
				br.livetouch.pushserver.lib.UserVO u = new br.livetouch.pushserver.lib.UserVO();
				u.codigo = obj.optString("codigo");
				u.email = obj.optString("email");
				u.nome = obj.optString("nome");
				u.badge = obj.optInt("badge");
				usuarios.add(u);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		String json = notification.toJson();

		String projeto = params.get("push.server.project","Livecom");
		
		String authorization = params.get(Params.PUSH_API_KEY,"");

		String jsonResponse = null;
		
		String erro = null;

		try {
			PushService s = Livecom.getPushService(params);
			boolean qtde50 = logins.size() < 50;
			if(qtde50) {
				log.debug(">> Push users ["+logins+"], msg: " + json);
			} else {
				log.debug(">> Push users qtde ["+logins.size()+"], msg: " + json);
			}
			
			String dataAgendamento = notification.getDataAgendamento();
			String tipo = notification.getTipo();
			String sound = params.get(Params.PUSH_NOTIFICATION_SOUND, "default");
			
			if(StringUtils.isNotEmpty(sound)) {
				notification.setSound(sound);				
			}
			
			FirebaseVO firebase = new FirebaseVO();
			JSONObject data = setData(pushId, notification, dataAgendamento, tipo);
			firebase.setProject(projeto);
			firebase.setAuthorization(authorization);
			firebase.setNotification(notification.toFirebaseJson());
			firebase.setData(data.toString());
			
			firebase.setMsgId(notification.getMsgId());
			firebase.setProcessando(notification.isProcessando());
			
			//Parametros
			setParams(params, notification, firebase);
			
			firebase.setUsers(usuarios);
			firebase.setVoip(notification.isVoip());
			jsonResponse = s.pushFirebase(firebase);
			
			if(jsonResponse != null && jsonResponse.equals("{error: Connection refused: connect}")) {
				log.error("Erro ao fazer push: " + jsonResponse);
				erro = "Erro ao fazer push: " + jsonResponse;
				logPushReport(notification, erro);
				jsonResponse = null;
			}
			
			if(!ids.isEmpty()) {
				log.debug("Push web socket");
				WebSocketNotification.sendMsg(ids, notification);
			}
			
			if(qtde50) {
				log.debug("<< Push users ["+logins+"], response:" + jsonResponse);
			} else {
				log.debug("<< Push users qtde ["+logins.size()+"], response:" + jsonResponse);
			}
			
		} catch (Error e) {
			erro = e.getMessage();
			logPushReport(notification, erro);
			log.error("Erro ao fazer push: " + e.getMessage(),e);
		}
		
		return jsonResponse;
	}

	/**
	 * List<String> users = contém os logins para receber push
	 * @param pushVersion 
	 * @throws JSONException 
	 */
	public String silent(ParametrosMap params, PushIdsVO users, PushNotificationVO notification) throws IOException {
		
		List<String> logins = users != null ? users.getLogins() : new ArrayList<String>();
		
		String json = notification.toJson();
		
		String projeto = params.get("push.server.project","Livecom");
		
		String authorization = params.get(Params.PUSH_API_KEY,"");
		
		String jsonResponse = null;
		
		String erro = null;
		
		try {
			PushService s = Livecom.getPushService(params);
			boolean qtde50 = logins.size() < 50;
			if(qtde50) {
				log.debug(">> Push users ["+logins+"], msg: " + json);
			} else {
				log.debug(">> Push users qtde ["+logins.size()+"], msg: " + json);
			}
			
			String tipo = TipoPush.silent.toString();
			notification.setTipo(tipo);
			
			FirebaseVO firebase = new FirebaseVO();
			JSONObject data = setData(null, notification, "", tipo);
			firebase.setProject(projeto);
			firebase.setAuthorization(authorization);
			firebase.setData(data.toString());
			
			//Parametros
			setParams(params, notification, firebase);
			
			firebase.setUsers(logins);
			jsonResponse = s.pushFirebase(firebase);
			
			if(jsonResponse != null && jsonResponse.equals("{error: Connection refused: connect}")) {
				log.error("Erro ao fazer push: " + jsonResponse);
				erro = "Erro ao fazer push: " + jsonResponse;
				logPushReport(notification, erro);
				jsonResponse = null;
			}
			
			if(qtde50) {
				log.debug("<< Push users ["+logins+"], response:" + jsonResponse);
			} else {
				log.debug("<< Push users qtde ["+logins.size()+"], response:" + jsonResponse);
			}
			
		} catch (Error e) {
			erro = e.getMessage();
			logPushReport(notification, erro);
			log.error("Erro ao fazer push: " + e.getMessage(),e);
		}
		
		return jsonResponse;
	}
	
	public String readAllPushs(Usuario u) throws IOException {
		
		List<Long> users = new ArrayList<>();
		users.add(u.getId());
		
		e = u.getEmpresa();
		ParametrosMap params = ParametrosMap.getInstance(e.getId());
		PushIdsVO logins = null;

		logins = getLoginsListV2(params, null, users);
		
		return readAllPushs(params, logins);
		
		
	}
	
	public String readAllPushs(ParametrosMap params, PushIdsVO users) throws IOException {

		boolean pushOn = params.isPushOn();
		if(!pushOn) {
			return null;
		}
		
		List<String> logins = users != null ? users.getLogins() : new ArrayList<String>();
		List<br.livetouch.pushserver.lib.UserVO> usuarios = new ArrayList<br.livetouch.pushserver.lib.UserVO>();

		for (String login : logins) {
			JSONObject obj;
			try {
				obj = new JSONObject(login);
				br.livetouch.pushserver.lib.UserVO u = new br.livetouch.pushserver.lib.UserVO();
				u.codigo = obj.optString("codigo");
				u.email = obj.optString("email");
				u.nome = obj.optString("nome");
				u.badge = obj.optInt("badge");
				usuarios.add(u);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		String projeto = params.get("push.server.project","Livecom");
		
		String authorization = params.get(Params.PUSH_API_KEY,"");

		String jsonResponse = null;
		
		try {
			PushService s = Livecom.getPushService(params);
			
			FirebaseVO firebase = new FirebaseVO();
			JSONObject data = setDataReadAll();
			firebase.setProject(projeto);
			firebase.setAuthorization(authorization);
			firebase.setData(data.toString());
			
			//Parametros
			setParams(params, null, firebase);
			
			firebase.setUsers(usuarios);
			firebase.setNotification(null);
			
			jsonResponse = s.pushFirebase(firebase);
			
			if(jsonResponse != null && jsonResponse.equals("{error: Connection refused: connect}")) {
				log.error("Erro ao fazer push: " + jsonResponse);
				jsonResponse = null;
			}
			
			
		} catch (Error e) {
			log.error("Erro ao fazer push: " + e.getMessage(),e);
		}
		
		return jsonResponse;
	}

	private void setParams(ParametrosMap params, PushNotificationVO notification, FirebaseVO firebase) {
		// FIXME DEBUGAR PUSH DE CHAT
		
		String tipo = notification != null ? notification.getTipo() : "";
		
		if(StringUtils.equalsIgnoreCase(tipo, "mensagem")) {
			String collapseKey = params.get(Params.PUSH_FIREBASE_COLLAPSEKEY_CHAT);
			if(StringUtils.isNoneEmpty(collapseKey)) {
				firebase.setCollapseKey(collapseKey);
			}
			
			Boolean contentAvaliable = params.getBoolean(Params.PUSH_FIREBASE_CONTENTAVALIABLE_CHAT, true);
			firebase.setContentAvailable(contentAvaliable);
			
//			Boolean delayWhileIdle = params.getBoolean(Params.PUSH_FIREBASE_DELAYWHILEIDLE_CHAT, true);
//			firebase.setDelayWhileIdle(delayWhileIdle);
			
			String priority = params.get(Params.PUSH_FIREBASE_PRIORITY_CHAT);
			firebase.setPriority(priority);
			
			String ttl = params.get(Params.PUSH_FIREBASE_TIMETOLIVE_CHAT);
			if(StringUtils.isNotEmpty(ttl)) {
				Long timeToLive = Long.parseLong(ttl, 10);
				firebase.setTimeToLive(timeToLive);
			}
		} else if(StringUtils.equalsIgnoreCase(tipo, "silent")) {
			
			String collapseKey = params.get(Params.PUSH_FIREBASE_COLLAPSEKEY_CHAT);
			if(StringUtils.isNoneEmpty(collapseKey)) {
				firebase.setCollapseKey(collapseKey);
			}
			
			firebase.setContentAvailable(true);
			firebase.setPriority("normal");
			
		}else {
			String collapseKey = params.get(Params.PUSH_FIREBASE_COLLAPSEKEY);
			if(StringUtils.isNoneEmpty(collapseKey)) {
				firebase.setCollapseKey(collapseKey);
			}
			
			if(notification != null) {
				firebase.setSound(notification.getSound());
			}
			
			Boolean contentAvaliable = params.getBoolean(Params.PUSH_FIREBASE_CONTENTAVALIABLE, false);
			firebase.setContentAvailable(contentAvaliable);
			
//			Boolean delayWhileIdle = params.getBoolean(Params.PUSH_FIREBASE_DELAYWHILEIDLE, true);
//			firebase.setDelayWhileIdle(delayWhileIdle);
			
			String priority = params.get(Params.PUSH_FIREBASE_PRIORITY);
			firebase.setPriority(priority);
			
			String ttl = params.get(Params.PUSH_FIREBASE_TIMETOLIVE);
			if(StringUtils.isNotEmpty(ttl)) {
				Long timeToLive = Long.parseLong(ttl, 10);
				firebase.setTimeToLive(timeToLive);
			}
			
		}
	}

	private JSONObject setData(Long pushId, PushNotificationVO notification, String dataAgendamento, String tipo) {
		JSONObject data = new JSONObject();
		try {
			if(pushId != null) {
				data.putOpt("pushId", pushId);
			}
			if(notification != null) {
				data.putOpt("postId", notification.getPostId());
				data.putOpt("comentarioId", notification.getComentarioId());
				data.putOpt("conversaId", notification.getConversaId());
				data.putOpt("mensagemId", notification.getMensagemId());
			}
			if(StringUtils.isNoneEmpty(dataAgendamento)) {
				data.putOpt("agendamento", dataAgendamento);
			}
			data.putOpt("tipo", tipo);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return data;
	}

	private JSONObject setDataReadAll() {
		JSONObject data = new JSONObject();
		try {
			data.putOpt("markAllAsRead", true);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return data;
	}
	
	public void logPushReport(PushNotificationVO notification, String erro) {
		Post p = null;
		Long postId = notification.getPostId();
		if(postId != null) {
			p = postService.get(postId);
		}
		
		Comentario c = null;
		Long comentarioId = notification.getComentarioId();
		if(comentarioId != null) {
			c = comentarioService.get(comentarioId); 
		}
		try {
			PushReport pushReport = new PushReport();
			pushReport.setDataEnvio(new Date());
			pushReport.setEmpresa(e);
			pushReport.setMensagem(notification.toJson());
			pushReport.setTipoPush(TipoPush.valueOf(notification.getTipo()));
			pushReport.setStatus(StringUtils.isEmpty(erro));
			pushReport.setComentario(c);
			pushReport.setPost(p);
			pushReport.setErro(erro);
			pushReportService.saveOrUpdate(pushReport);
		} catch(Exception e) {
			log.error("Erro ao salvar o push Report" + e.getMessage(), e);
		}
		
		
	}
	
	public String unregister(Empresa empresa, DeviceFirebaseVO device) throws IOException {
		
		ParametrosMap params = ParametrosMap.getInstance(empresa.getId());

		String projeto = params.get(Params.PUSH_SERVER_PROJECT, "LivecomFirebase");
		String apiKey = params.get(Params.PUSH_API_KEY,"");
		
		device.setProjectCode(projeto);
		
		String jsonResponse = null;
		
		try {
			PushService s = Livecom.getPushService(params);
			jsonResponse = s.unregister(device, apiKey);
			
			log.debug("<< Unregister response:" + jsonResponse);
			
		} catch (Error e) {
			log.error("Erro ao fazer unregister: " + e.getMessage(),e);
		}
		
		return jsonResponse;
	}
}
