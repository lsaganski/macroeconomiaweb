package br.livetouch.livecom.rest.resource;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Chapeu;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.vo.ChapeuVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import io.swagger.annotations.ApiOperation;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/chapeu")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class ChapeuResource extends MainResource {
	protected static final Logger log = Log.getLogger(ChapeuResource.class);
	
	@POST
	public Response create(Chapeu chapeu) {
		
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			//Validar se ja existe um chapeu com o mesmo nome
			boolean existe = chapeuService.findByName(chapeu, getEmpresa()) != null;
			if(existe) {
				log.debug("Ja existe um chapéu com este nome");
				return Response.ok(MessageResult.error("Ja existe um chapéu com este nome")).build();
			}
			
			CategoriaPost categoria = chapeu.getCategoria();
			if(categoria != null) {
				CategoriaPost categoriaPost = categoriaPostService.get(categoria.getId());
				chapeu.setCategoria(categoriaPost);
			}
			chapeuService.saveOrUpdate(chapeu);
			ChapeuVO chapeuVO = new ChapeuVO(chapeu);
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", chapeuVO)).build();
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel efetuar o cadastro do chapéu")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@PUT
	public Response update(Chapeu chapeu) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			if(chapeu != null && chapeu.getId() != null) {
				
				//Validar se ja existe um chapeu com o mesmo nome
				boolean existe = chapeuService.findByName(chapeu, getEmpresa()) != null;
				if(existe) {
					log.debug("Ja existe um chapéu com este nome");
					return Response.ok(MessageResult.error("Ja existe um chapéu com este nome")).build();
				}
				
				CategoriaPost categoria = chapeu.getCategoria();
				if(categoria != null) {
					CategoriaPost categoriaPost = categoriaPostService.get(categoria.getId());
					chapeu.setCategoria(categoriaPost);
				}
				chapeuService.saveOrUpdate(chapeu);
			} else {
				log.debug("Não foi possivel atualizar o cadastro do chapéu");
				return Response.ok(MessageResult.error("Não foi possivel atualizar o cadastro do chapéu")).build();
			}
			
			ChapeuVO chapeuVO = new ChapeuVO(chapeu);
			return Response.ok(MessageResult.ok("Chapéu atualizado com sucesso", chapeuVO)).build();
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar o chapéu")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@GET
	public Response findAll() {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		List<Chapeu> chapeis = chapeuService.findAll();
		List<ChapeuVO> vos = ChapeuVO.create(chapeis);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Chapeu chapeu = chapeuService.get(id);
		if(chapeu == null) {
			return Response.ok(MessageResult.error("Chapéu não encontrado")).build();
		}
		ChapeuVO vo = new ChapeuVO(chapeu);
		return Response.ok().entity(vo).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Chapeu chapeu = chapeuService.get(id);
			if(chapeu == null) {
				return Response.ok(MessageResult.error("Chapéu não encontrado")).build();
			}
			chapeuService.delete(chapeu);
			return Response.ok().entity(MessageResult.ok("Chapéu deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Chapéu  " + id)).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Chapéu  " + id)).build();
		}
	}
	
	@GET
	@Path("/exportar")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response exportar() throws IOException {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Empresa empresa = getEmpresa();
		String csv = chapeuService.csvChapeu();
		
		String charset = ParametrosMap.getInstance(empresa).get(Params.EXPORTACAO_ARQUIVO_CHARSET, "ISO-8859-1");

		String attachament = "attachment; filename=" + URLEncoder.encode("chapeus.csv", "UTF-8");
		ResponseBuilder b = Response.status(Status.OK);
		b.encoding(charset);
		b.header("Content-Disposition", attachament);
		b.header("Content-Type", "application/octet-stream"+";charset="+charset);
		b.entity(csv);
		Response r = b.build();
		return r;
	}
	
	@POST
	@Path("/ordem")
	@ApiOperation(value = "salva/atualiza ordem dos capitulos.", notes = "WS salvar ordem.")
	public Response post(List<ChapeuVO> vos) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			for (ChapeuVO vo : vos) {
				Chapeu chapeu = chapeuService.get(vo.getId());
				chapeu.setOrdem(vo.getOrdem());
				chapeuService.saveOrUpdate(chapeu);
			}
			Response response = Response.ok(MessageResult.ok("Ordem de [" + vos.size() + "] chapeus(s) atualizada com sucesso.")).build();
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			String erro = "Não foi possível salvar a ordem dos chapeus.";
			log.error(erro , e);
			return Response.ok(MessageResult.error(erro)).build();
		}
	}	
}
