package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.LinhaImportacao;
import br.livetouch.livecom.domain.repository.LinhaImportacaoRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class LinhaImportacaoRepositoryImpl extends StringHibernateRepository<LinhaImportacao> implements LinhaImportacaoRepository {

	public LinhaImportacaoRepositoryImpl() {
		super(LinhaImportacao.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LinhaImportacao> findAllByLogId(Long id) {
		StringBuffer sb = new StringBuffer("from LinhaImportacao l where l.logImportacao.id=?");

		Query q = createQuery(sb.toString());

		q.setLong(0, id);
		List<LinhaImportacao> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LinhaImportacao> findAllByIdLog(Long id) {
		Query q = createQuery("FROM LinhaImportacao l where logImportacao.id=:id ");
		q.setLong("id", id);
		List<LinhaImportacao> list = q.list();
		return list;
	}

}