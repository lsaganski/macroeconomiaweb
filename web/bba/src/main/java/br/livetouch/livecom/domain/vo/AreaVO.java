package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Area;

public class AreaVO implements Serializable{
	private static final long serialVersionUID = 511847560768136130L;
	public Long id;
	public String nome;
	public String codigo;
	public String codigoDesc;
	public boolean padrao;
	public DiretoriaVO diretoria;
	
	
	public static List<AreaVO> fromList(List<Area> areas) {
		List<AreaVO> vos = new ArrayList<>();
		if(areas == null) {
			return vos;
		}
		for (Area a : areas) {
			vos.add(new AreaVO(a, true));
		}
		return vos;
	}
	
	public AreaVO(Area area, Boolean isFindDirExc) {
		setData(area, isFindDirExc);
	}

	private void setData(Area area, Boolean isFindDirExc) {
		this.id = area.getId();
		this.nome = area.getNome();
		this.codigo = area.getCodigo();
		this.padrao = area.isPadrao();
		this.codigoDesc = codigo != null ? codigo : id.toString();
		if(area.getDiretoria() != null) {
			this.diretoria = new DiretoriaVO(area.getDiretoria());
		}
	}

	public AreaVO(Area area) {
		setData(area, false);
	}

	public static AreaVO from(Area area) {
		return new AreaVO(area);
	}
}
