package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.CategoriaPostAuditoria;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.repository.CategoriaPostRepository;
import br.livetouch.livecom.domain.vo.CategoriaVO;

@Repository
public class CategoriaPostRepositoryImpl extends LivecomRepository<CategoriaPost> implements CategoriaPostRepository {

	public CategoriaPostRepositoryImpl() {
		super(CategoriaPost.class);
	}
	
	@Override
	public CategoriaPost findPadrao(Empresa e) {
		List<CategoriaPost> list = query("from CategoriaPost where padrao=1 and empresa.id=?",false, e.getId());
		return list.isEmpty() ? null : list.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findAllDefault(Empresa empresa) {
		StringBuffer sb = new StringBuffer("from CategoriaPost c WHERE c.padrao = 1 ");
		sb.append(" AND c.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setParameter("empresa", empresa);
		q.setCacheable(true);
		List<CategoriaPost> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findAllFetchChildren(Empresa empresa) {
		StringBuffer sb = new StringBuffer("select c from CategoriaPost c inner join c.categorias WHERE 1=1");
		sb.append(" c.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setParameter("empresa", empresa);
		q.setCacheable(true);
		List<CategoriaPost> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findAllParent(Empresa empresa) {
		StringBuffer sb = new StringBuffer("select c from CategoriaPost c WHERE 1=1 ");
		sb.append(" AND c.categoriaParent is null AND c.empresa = :empresa");
		sb.append(" order by c.ordem,c.nome");
		
		Query q = createQuery(sb.toString());
		q.setParameter("empresa", empresa);
		q.setCacheable(true);
		List<CategoriaPost> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findAllParentByIdioma(Empresa empresa, List<Idioma> idiomas) {
		StringBuffer sb = new StringBuffer("select distinct c from CategoriaPost c inner join c.idiomas i WHERE 1=1 ");
		sb.append(" AND c.categoriaParent is null AND c.empresa = :empresa");

		if(idiomas != null && !idiomas.isEmpty()) {
			sb.append(" AND i.idioma in (:idiomas) ");
		}
		
		sb.append(" order by c.id desc, c.nome");
		
		Query q = createQuery(sb.toString());

		q.setParameter("empresa", empresa);
		
		if(idiomas != null && !idiomas.isEmpty()) {
			q.setParameterList("idiomas", idiomas);
		}
		
		q.setCacheable(true);
		List<CategoriaPost> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findAllChildren(Empresa empresa) {
		StringBuffer sb = new StringBuffer("select c from CategoriaPost c WHERE 1=1 ");
		sb.append(" AND c.categoriaParent is not null AND c.empresa = :empresa");
		sb.append(" order by c.ordem,c.nome");
		
		Query q = createQuery(sb.toString());
		q.setParameter("empresa", empresa);
		q.setCacheable(true);
		List<CategoriaPost> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findByParent(CategoriaPost pai) {
		StringBuffer sb = new StringBuffer("select c from CategoriaPost c WHERE 1=1 ");
		
		if(pai != null) {
			sb.append(" AND c.categoriaParent = :pai");
		}

		sb.append(" order by c.ordem,c.nome");
		
		Query q = createQuery(sb.toString());
		
		if(pai != null) {
			q.setParameter("pai", pai);
		}
		
		q.setCacheable(true);
		List<CategoriaPost> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findByParentAndIdiomas(CategoriaPost pai, List<Idioma> idiomas) {
		StringBuffer sb = new StringBuffer("select c from CategoriaPost c WHERE 1=1 ");
		
		if(pai != null) {
			sb.append(" AND c.categoriaParent = :pai");
		}
		
		for (Idioma idioma : idiomas) {
			sb.append(" AND c.id = (select categoria.id from CategoriaIdioma where categoria.id = c.id and idioma.id = " + idioma.getId() + ")");
		}
		
		sb.append(" order by c.ordem,c.nome");
		
		Query q = createQuery(sb.toString());
		
		if(pai != null) {
			q.setParameter("pai", pai);
		}
		
		q.setCacheable(true);
		List<CategoriaPost> list = q.list();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findByNameAndIdioma(String nome, Idioma idioma, Empresa empresa) {
		StringBuffer sb = new StringBuffer("select c from CategoriaPost c WHERE 1=1 ");
		
		if(idioma != null) {
			sb.append(" AND c.id = (");
			sb.append("select categoria.id from CategoriaIdioma where categoria.id = c.id and idioma.id = " + idioma.getId());
			if(StringUtils.isNotEmpty(nome)) {
				sb.append(" AND (c.nome like :nome or nome like :nome)");
			}
			sb.append(")");
		}
		
		if(empresa != null) {
			sb.append(" AND c.empresa = :empresa");
		}
		
		sb.append(" order by c.ordem,c.nome");
		
		Query q = createQuery(sb.toString());
		
		if(StringUtils.isNotEmpty(nome)) {
			q.setParameter("nome", "%" + nome + "%");
		}
		
		if(empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		q.setCacheable(true);
		
		q.setMaxResults(10);
		
		List<CategoriaPost> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findParentByIdiomas(List<Idioma> idiomas) {
		StringBuffer sb = new StringBuffer("select c from CategoriaPost c WHERE 1=1 ");
		
		sb.append(" AND c.categoriaParent is null");
		
		for (Idioma idioma : idiomas) {
			sb.append(" AND c.id = (select categoria.id from CategoriaIdioma where categoria.id = c.id and idioma.id = " + idioma.getId() + ")");
		}
		
		sb.append(" order by c.ordem,c.nome");
		
		Query q = createQuery(sb.toString());
		
		q.setCacheable(true);
		List<CategoriaPost> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findByParent(CategoriaVO pai) {
		StringBuffer sb = new StringBuffer("select c from CategoriaPost c WHERE 1=1 ");
		
		if(pai != null) {
			sb.append(" AND c.categoriaParent.id = :pai");
		}
		
		sb.append(" order by c.ordem,c.nome");
		
		Query q = createQuery(sb.toString());
		
		if(pai != null) {
			q.setParameter("pai", pai.id);
		}
		
		q.setCacheable(true);
		List<CategoriaPost> list = q.list();
		return list;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<CategoriaPost> findAll(Empresa empresa) {
		
		String ordemOn = ParametrosMap.getInstance(empresa).get(Params.CATEGORIA_ORDEM_ON, "0");
		
		StringBuffer sb = new StringBuffer("select distinct c from CategoriaPost c WHERE 1=1 ");
		if(empresa != null) {
			sb.append(" AND c.empresa = :empresa");
		}
		if("1".equals(ordemOn)) {
			sb.append(" order by c.ordem, c.nome ");
		} else {
			sb.append(" order by c.nome ");
		}
		
		Query q = createQuery(sb.toString());
		if(empresa != null) {
			q.setParameter("empresa", empresa);
		}
		q.setCacheable(true);
		List<CategoriaPost> list = q.list();
		return list;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<CategoriaVO> findByIdioma(Empresa empresa, Idioma idioma) {
		
		String ordemOn = ParametrosMap.getInstance(empresa).get(Params.CATEGORIA_ORDEM_ON, "0");
		
		StringBuffer sb = new StringBuffer("select new br.livetouch.livecom.domain.vo.CategoriaVO(c, ci.nome, ci.idioma) from CategoriaPost c inner join c.idiomas ci WHERE 1=1 ");
		
		if(idioma != null) {
			sb.append(" AND ci.idioma = :idioma");
		}

		if(empresa != null) {
			sb.append(" AND c.empresa = :empresa");
		}
		
		if("1".equals(ordemOn)) {
			sb.append(" order by c.ordem, c.nome ");
		} else {
			sb.append(" order by c.nome ");
		}
		
		Query q = createQuery(sb.toString());
		
		if(empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		if(idioma != null) {
			q.setParameter("idioma", idioma);
		}
		
		q.setCacheable(true);
		List<CategoriaVO> list = q.list();
		return list;
	}

	@Override
	public boolean exists(String nome, Empresa empresa) {
		StringBuffer sb = new StringBuffer("select count(*) from CategoriaPost c where c.nome = ? AND c.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setParameter(0, nome);
		q.setParameter("empresa", empresa);
		long count = getCount(q);
		return count > 0;
	}

	@Override
	public long countPosts(CategoriaPost c, Empresa empresa) {
		
		StringBuffer sb  = new StringBuffer("select count(p.id) from CategoriaPost c ");
		sb.append(" inner join c.posts p where c.id = ? AND c.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setLong(0, c.getId());
		q.setParameter("empresa", empresa);
		long count = getCount(q);
		return count;
	}

	@Override
	public void saveOrUpdate(CategoriaPostAuditoria a) {
		getSession().saveOrUpdate(a);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findAllByNomeLikeWithMax(String nome, int max, Empresa empresa) {
		String sql = "select distinct c from CategoriaPost c left join c.idiomas i where c.empresa = :empresa ";

		if(StringUtils.isNotEmpty(nome)) {
			sql += " and ( ";
			sql += " c.nome like :nome ";
			sql += " or i.nome like :nome ";
			sql += " ) ";
		}
		
		sql += " order by c.nome, i.nome ";

		Query q = createQuery(sql);
		
		if(StringUtils.isNotEmpty(nome)) {
			q.setParameter("nome", "%" + nome + "%");
		}
		q.setParameter("empresa", empresa);
		q.setMaxResults(max);
		q.setCacheable(true);
		List<CategoriaPost> list = (List<CategoriaPost>) q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CategoriaPost findByName(CategoriaPost categoria, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from CategoriaPost c where c.nome = :nome");
		sb.append(" and c.empresa = :empresa");
		
		if(categoria.getId() != null) {
			sb.append(" and c.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("empresa", empresa);
		q.setParameter("nome", categoria.getNome());
		
		if(categoria.getId() != null) {
			q.setParameter("id", categoria.getId());
		}
		
		List<CategoriaPost> list = q.list();
		CategoriaPost c = (CategoriaPost) (list.size() > 0 ? q.list().get(0) : null);
		return c;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CategoriaPost findByCodigo(Empresa empresa, String codigo) {
		StringBuffer sb = new StringBuffer("from CategoriaPost c where c.codigo = :codigo");
		sb.append(" and c.empresa = :empresa");
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("empresa", empresa);
		q.setParameter("codigo", codigo);
		
		List<CategoriaPost> list = q.list();
		CategoriaPost c = (CategoriaPost) (list.size() > 0 ? q.list().get(0) : null);
		return c;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findMenuCategoria(Empresa e) {
		StringBuffer sb = new StringBuffer("select c from CategoriaPost c WHERE 1=1 ");
		sb.append(" AND c.empresa = :empresa AND c.menu = :menu");
		sb.append(" order by c.ordem,c.nome");
		Query q = createQuery(sb.toString());
		q.setParameter("empresa", e);
		q.setParameter("menu", Boolean.TRUE);
		q.setCacheable(true);
		List<CategoriaPost> list = q.list();
		return list;
	}

	@Override
	public void delete(List<CategoriaPost> categorias) {
		StringBuffer sb = new StringBuffer("delete from CategoriaPost c WHERE c in(:list)");
		Query q = createQuery(sb.toString());
		q.setParameterList("list", categorias);
		q.executeUpdate();
	}

	@Override
	public void deleteArquivos(Long categoriaId, List<Long> arquivosIds) {
		StringBuffer sb = new StringBuffer("delete from Arquivo a WHERE a.categoria.id = :id AND a.id not in(:ids)");
		Query q = createQuery(sb.toString());
		q.setParameter("id", categoriaId);
		q.setParameterList("ids", arquivosIds);
		q.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CategoriaPost> findByIdsAndIdioma(Idioma idioma, List<Long> ids) {
		StringBuffer sb = new StringBuffer("select distinct c from CategoriaPost c inner join c.idiomas ci WHERE 1=1 ");
		
		if(idioma != null) {
			sb.append(" AND ci.idioma = :idioma");
		}
		
		if(!CollectionUtils.isEmpty(ids)) {
			sb.append(" AND c.id in (:ids)");
		}
		
		Query q = createQuery(sb.toString());
		
		if(idioma != null) {
			q.setParameter("idioma", idioma);
		}
		
		if(!CollectionUtils.isEmpty(ids)) {
			q.setParameterList("ids", ids);
		}
		
		q.setCacheable(true);
		
		List<CategoriaPost> list = q.list();
		return list;
	}
}