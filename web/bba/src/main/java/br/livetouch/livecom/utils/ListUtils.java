package br.livetouch.livecom.utils;

import java.util.List;

public class ListUtils {
	public static boolean isEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }

    public static boolean isNotEmpty(List<?> list) {
        return list != null && list.size() > 0;
    }

    public static int size(List<?> list) {
        return list != null?list.size():0;
    }
}
