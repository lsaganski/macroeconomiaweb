package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.Usuario;

public class UsuarioSimpleVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public String urlFotoUsuario;
	public String nome;
	
	public void setUsuario(Usuario u) {
		this.id = u.getId();
		this.nome = u.getNome();
		this.urlFotoUsuario = u.getFotoS3();
	}
}

