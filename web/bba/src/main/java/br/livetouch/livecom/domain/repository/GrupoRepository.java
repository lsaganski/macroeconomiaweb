package br.livetouch.livecom.domain.repository;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoAuditoria;
import br.livetouch.livecom.domain.GrupoSubgrupos;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusParticipacao;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.vo.Filtro;
import br.livetouch.livecom.domain.vo.GrupoFilter;
import br.livetouch.livecom.domain.vo.GrupoUsuariosVO;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.UserToPushVO;

@Repository
public interface GrupoRepository extends net.livetouch.tiger.ddd.repository.Repository<Grupo> {
	int MAX = 20;

	List<Grupo> findAllDefault(Usuario userInfo);

	Grupo findByNome(String nome,Usuario userInfo);
	List<Grupo> findAllByNomeLike(String nome,Usuario userInfo);
	Grupo findByCodigo(Grupo grupo,Usuario userInfo);

	long getCountByFilter(Grupo filtro,Usuario userInfo);

	List<GrupoUsuariosVO> findbyFilter(Grupo filtro,Usuario userInfo, int page, int pageSize);
	List<Usuario> findUsuarios(Grupo grupo);
	List<UserToPushVO> findUsers(Grupo grupo, Post p, TipoNotificacao tipoNotificacao);
	List<Object[]> findUsuariosArrayIdNome(Grupo grupo,Usuario userInfo);
	
	void removeUsuario(Usuario userInfo, Grupo grupo, Usuario usuario);

	List<GrupoUsuarios> findMeusGrupos(GrupoFilter filter, Usuario user);

	List<GrupoVO> findGrupos(GrupoFilter filter, Usuario user);

	List<Grupo> findGruposParticipar(GrupoFilter filter, Usuario user, List<Grupo> grupos);

	long getCountMeusGrupos(GrupoFilter filter, Usuario user);
	
	Long countUsers(Grupo g);
	
	Long countUsersSubgrupos(Grupo grupo);

	Long countSubgrupos(Grupo g);
	
	List<Grupo> getGrupos(Usuario u);

	List<GrupoUsuariosVO> findGrupoUsuariosByUser(Usuario user);
	List<GrupoUsuarios> findGrupoUsuariosByUsuario(Usuario user);

	List<GrupoUsuariosVO> findGrupoUsuariosNaoAtivos(Usuario userInfo);
	List<GrupoUsuariosVO> findGrupoUsuariosAtivos(Usuario userInfo);
	List<GrupoUsuariosVO> findGrupoUsuariosEnviadoConvite(Usuario userInfo);
	List<GrupoUsuariosVO> findGrupoUsuariosByStatusAtivacao(String status,Usuario userInfo);
	List<GrupoUsuarios> findUsuariosByStatusParticipacao(StatusParticipacao status, Grupo g);

	boolean exists(Grupo grupo, Empresa empresa);

	void saveOrUpdate(GrupoAuditoria a);

	List<Grupo> findAllWithoutUsers(int page, int pageSize);

	List<Grupo> findAllWithoutUsers(GrupoFilter filter, Usuario userInfo);

	List<Object[]> findIdCodGrupos(Long idEmpresa);

	List<Grupo> findAll(Empresa empresa, Integer page, Integer maxRows);
	List<GrupoVO> findAll(GrupoFilter filter, Usuario user);

	Grupo findDefaultByEmpresa(Long empresaId);

	List<Grupo> findAllByUser(Usuario u);

	List<Long> findUsuariosDentroHorarioGrupoPadrao(Grupo g, Post p);

	List<Long> findUsuariosToPush(Grupo g, Post p, Idioma idioma);

	List<Long> findUsuariosReminder(Grupo g, Post p);

	List<Object[]> findIdNomeGrupos(Long empresaId);

	boolean isGrupoPadraoDentroHorario(Usuario u);

	boolean isGruposDentroHorario(Usuario u);

	Grupo findByName(Grupo g, Empresa empresa);

	List<Grupo> getGrupos(GrupoFilter filter);

	List<Long> findIdUsuarios(Grupo g);

	List<Grupo> findGruposByPost(Post post);

	Long getCountPostsByGrupo(Grupo g);

	List<Long> gruposUsuariosDentroHorario(List<Long> users);

	List<Long> grupoPadraoUsuariosDentroHorario(List<Long> users);

	Long countPostsSomenteEu(Usuario user);

	Grupo findByNomeAndUser(String nome, Usuario user);

	void movePostsToAdminGrupo(Usuario usuario, Usuario admin, Grupo grupo, List<Post> posts);
	
	GrupoUsuarios findByGrupoEUsuario(Grupo g, Usuario u);

	List<Grupo> findAllByCodigos(List<String> codigos);

	GrupoSubgrupos getGrupoSubgrupo(Grupo grupo, Grupo subgrupo);
	
	void saveOrUpdate(GrupoSubgrupos gs);

	void removeSubgrupo(Usuario userInfo, Grupo grupo, Grupo subgrupo);

	List<Grupo> findSubgruposByGrupo(Grupo grupo);

	List<Grupo> findAllSubgrupos(Filtro filtro, Empresa empresa);

	List<Grupo> findAllSubgruposWithAcesso(Filtro filtro, Usuario userInfo);

	List<Grupo> findSubgruposInGrupos(Set<Grupo> grupos);

	void changePermissaoPostarUsers(Grupo grupo, boolean postar);

	List<Grupo> findGruposVirtuais(GrupoFilter filter, Usuario user, List<Grupo> grupos);

	List<GrupoUsuarios> isAdmin(Usuario usuario, Grupo grupo);

	List<Usuario> findSubadminsByGrupo(Grupo grupo);

	Long countAll(GrupoFilter filter, Usuario userInfo);

	Long countFindGrupos(GrupoFilter filter, Usuario userInfo);

}