package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.utils.JSONUtils;

/**
 * Badge mostrada na web lá no sino e no mobile.
 * 
 * WEB Usa
 * var totalNotificacoes = badges.total;
 * var totalMensagens = badges.mensagens;
 * var totalSolicitacoes = badges.grupoSolicitarParticipar + badges.usuarioAmizadeSolicitada;
 * 
 * MOBILE Usa
 * badges.total;
 * badges.mensagens;
 */
public class NotificationBadge implements Serializable {

	private static final long serialVersionUID = 2634714980619075130L;

	public long total;

	// post
	public long posts;
	public long likes;
	public long favoritos;
	public long comentarios;

	// grupos
	public long grupoParticipar;
	public long grupoSair;
	public long grupoSolicitarParticipar;
	public long grupoUsuarioAdicionado;
	public long grupoUsuarioAceito;
	public long grupoUsuarioRemovido;
	public long grupoUsuarioRecusado;

	// amizade
	public long usuarioAmizadeSolicitada;
	public long usuarioAmizadeDesfeita;
	public long usuarioAmizadeAceita;
	public long usuarioAmizadeRecusada;

	// Este é inserido por fora no LivecomPage
	public Long mensagens;

	public Long reminders;

	@Override
	public String toString() {
		return JSONUtils.toJSON(this);
	}

	public NotificationBadge() {
	}

	/**
	 * Construtor com tudo
	 */
	public NotificationBadge(Long posts, Long likes, Long favoritos, Long comentarios, Long participar, Long sair,
			Long solicitarParticipar, Long grupoUsuarioAdicionado, Long grupoUsuarioRemovido, Long grupoUsuarioAceito,
			Long grupoUsuarioRecusado, Long usuarioAmizadeSolicitada, Long usuarioAmizadeDesfeita,
			Long usuarioAmizadeAceita, Long usuarioAmizadeRecusada) {

		// Mural
		this.posts = posts;
		this.likes = likes;
		this.favoritos = favoritos;
		this.comentarios = comentarios;

		// grupo
		this.grupoParticipar = participar;
		this.grupoSair = sair;
		this.grupoSolicitarParticipar = solicitarParticipar;
		this.grupoUsuarioAdicionado = grupoUsuarioAdicionado;
		this.grupoUsuarioRemovido = grupoUsuarioRemovido;
		this.grupoUsuarioAceito = grupoUsuarioAceito;
		this.grupoUsuarioRecusado = grupoUsuarioRecusado;

		// amizade
		this.usuarioAmizadeSolicitada = usuarioAmizadeSolicitada;
		this.usuarioAmizadeDesfeita = usuarioAmizadeDesfeita;
		this.usuarioAmizadeAceita = usuarioAmizadeAceita;
		this.usuarioAmizadeRecusada = usuarioAmizadeRecusada;

		this.total = likes + favoritos + posts + comentarios + participar + sair + solicitarParticipar
				+ grupoUsuarioAdicionado + grupoUsuarioAceito + grupoUsuarioRemovido + grupoUsuarioRecusado
				+ usuarioAmizadeSolicitada + usuarioAmizadeDesfeita + usuarioAmizadeAceita + usuarioAmizadeRecusada;
	}

	/**
	 * Construtor com apenas Mural
	 */
	public NotificationBadge(Long posts, Long likes, Long favoritos, Long comentarios) {
		this(posts, likes, favoritos, comentarios, 
				0L, 0L, 0L, 0L, 0L, 0L, 0L, 
				0L, 0L, 0L, 0L);
	}

	/**
	 * Construtor com Mural + Grupos
	 */
	public NotificationBadge(Long posts, Long likes, Long favoritos, Long comentarios, Long participar, Long sair,
			Long solicitarParticipar, Long grupoUsuarioAdicionado, Long grupoUsuarioRemovido, Long grupoUsuarioAceito,
			Long grupoUsuarioRecusado) {

		this(posts, likes, favoritos, comentarios, 
				participar, sair, solicitarParticipar, grupoUsuarioAdicionado,
				grupoUsuarioRemovido, grupoUsuarioAceito, grupoUsuarioRecusado, 
				0L, 0L, 0L, 0L);
	}

	/**
	 * Construtor com Mural + Amizade
	 */
	public NotificationBadge(Long posts, Long likes, Long favoritos, Long comentarios, 
			Long usuarioAmizadeSolicitada, Long usuarioAmizadeDesfeita,
			Long usuarioAmizadeAceita, Long usuarioAmizadeRecusada) {

		this(posts, likes, favoritos, comentarios, 
				0L, 0L, 0L, 0L,  0L, 0L, 0L, 
				usuarioAmizadeSolicitada, usuarioAmizadeDesfeita, usuarioAmizadeAceita, usuarioAmizadeRecusada);
	}
}
