package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.StatusPost;
import br.livetouch.livecom.domain.repository.StatusPostRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class StatusPostRepositoryImpl extends StringHibernateRepository<StatusPost> implements StatusPostRepository {

	public StatusPostRepositoryImpl() {
		super(StatusPost.class);
	}
	
	@Override
	public StatusPost findByCodigo(StatusPost status, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from StatusPost where 1=1 ");
		
		if(StringUtils.isNotEmpty(status.getCodigo())) {
			sb.append(" and codigo like :codigo ");
		}
		
		if(status.getId() != null) {
			sb.append(" and id != :id");
		}

		if(empresa != null) {
			sb.append(" and empresa = :empresa ");
		}

		Query q = createQuery(sb.toString());
		
		if(StringUtils.isNotEmpty(status.getCodigo())) {
			q.setParameter("codigo", status.getCodigo());
		}
		
		if(status.getId() != null) {
			q.setParameter("id", status.getId());
		}
		
		if(empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		StatusPost statusPost = (StatusPost) (q.list().isEmpty() ? null : q.list().get(0));
		return statusPost;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StatusPost> findAll(Empresa empresa) {
		StringBuffer sb = new StringBuffer("from StatusPost where 1=1 ");
		
		if(empresa != null) {
			sb.append(" and empresa = :empresa ");
		}

		Query q = createQuery(sb.toString());
		
		if(empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		return q.list();
	}

}