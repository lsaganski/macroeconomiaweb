 package br.livetouch.livecom.rest.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.PermissaoCategoria;
import br.livetouch.livecom.domain.vo.PermissaoCategoriaVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/permissaoCategoria")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class PermissaoCategoriaResource extends MainResource {
	protected static final Logger log = Log.getLogger(PermissaoCategoriaResource.class);
	
	@POST
	public Response create(PermissaoCategoria permissaoCategoria) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		//Validação
		Response messageResult = existe(permissaoCategoria);
		if(messageResult != null){
			return messageResult;
		}
		
		try {
			permissaoCategoriaService.saveOrUpdate(permissaoCategoria, getUserInfo().getEmpresa());
			PermissaoCategoriaVO categoriaVO = new PermissaoCategoriaVO(permissaoCategoria);
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", categoriaVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel realizar o cadastro da categoria " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel realizar o cadastro da categoria ")).build();
		}
	}
	
	@PUT
	public Response update(PermissaoCategoria permissaoCategoria) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		if(permissaoCategoria == null || permissaoCategoria.getId() == null) {
			return Response.ok(MessageResult.error("Permissão inexistente")).build();
		}
		Response messageResult = existe(permissaoCategoria);
		if(messageResult != null){
			return messageResult;
		}
		
		try {
			permissaoCategoriaService.saveOrUpdate(permissaoCategoria, getUserInfo().getEmpresa());
			PermissaoCategoriaVO categoriaVO = new PermissaoCategoriaVO(permissaoCategoria);
			return Response.ok(MessageResult.ok("Categoria atualiza com sucesso com sucesso", categoriaVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel atualizar o cadastro da categoria " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar o cadastro da categoria ")).build();
		}
	}
	
	@GET
	public Response findAll() {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Long empresaId = getUserInfoVO().getEmpresaId();
		Empresa empresa = empresaService.get(empresaId);
		List<PermissaoCategoria> categorias = permissaoCategoriaService.findAll(empresa);
		List<PermissaoCategoriaVO> vos = PermissaoCategoriaVO.fromList(categorias);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		PermissaoCategoria categoria = permissaoCategoriaService.get(id);
		if(categoria == null) {
			return Response.ok(MessageResult.error("Categoria não encontrada")).build();
		}
		PermissaoCategoriaVO categoriaVO = new PermissaoCategoriaVO(categoria);
		return Response.ok().entity(categoriaVO).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		PermissaoCategoria permissaoCategoria = permissaoCategoriaService.get(id);
		if(permissaoCategoria == null) {
			return Response.ok(MessageResult.error("Categoria não encontrada")).build();
		}
		try {
			permissaoCategoriaService.delete(permissaoCategoria);
			return Response.ok().entity(MessageResult.ok("Categoria deletada com sucesso")).build();
		} catch (DomainException e) {
			log.error("Não foi possível excluir a categoria id: " + id + " ERRO:" + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir a categoria  " + id)).build();
		}
	}
	
	public Response existe(PermissaoCategoria permissaoCategoria) {
		
		if(permissaoCategoriaService.findByNomeValid(permissaoCategoria, getEmpresa()) != null) {
			log.debug("Ja existe uma categoria com este nome");
			return Response.ok(MessageResult.error("Ja existe uma categoria com este nome")).build();
		}
		if(permissaoCategoriaService.findByCodigoValid(permissaoCategoria, getEmpresa()) != null){
			log.debug("Ja existe uma categoria com este codigo");
			return Response.ok(MessageResult.error("Ja existe uma categoria com este codigo")).build();
			
		}

		return null;
	}
	
}
