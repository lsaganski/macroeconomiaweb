package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.ImportarArquivoResponse;
import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;

@Controller
@Scope("prototype")
public class GetResponsePage extends WebServiceFormPage {
	public JobInfo jobInfo;

	@Override
	protected void form() {
	}

	@Override
	protected Object go() throws Exception {
		jobInfo = JobInfo.getInstance(getEmpresaId());

		ImportarArquivoResponse response = jobInfo.response;
		if (response == null) {
			return new MensagemResult("NOK", "Response nula");
		}
		if (jobInfo.getEmpresa().equals(getEmpresa())) {
			return response;
		} else {
			return new MensagemResult("NOK", "Response nula");
		}
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("response", ImportarArquivoResponse.class);
	}

	@Override
	public boolean isSaveLogTransacao() {
		return false;
	}
}
