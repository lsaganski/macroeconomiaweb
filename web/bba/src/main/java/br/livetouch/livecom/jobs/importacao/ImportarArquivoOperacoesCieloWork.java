package br.livetouch.livecom.jobs.importacao;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.dialect.Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Service;

import br.infra.livetouch.dialect.OracleDialect;
import br.infra.util.Log;
import br.infra.util.SQLUtils;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ImportarArquivoResponse;
import br.livetouch.livecom.domain.LinhaImportacao;
import br.livetouch.livecom.domain.LogImportacao;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.LinhaImportacaoService;
import br.livetouch.livecom.domain.service.LogImportacaoService;
import br.livetouch.livecom.domain.service.OperacoesCieloService;
import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;
import br.livetouch.livecom.utils.DateUtils;
import net.livetouch.hibernate.HibernateUtil;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ImportarArquivoOperacoesCieloWork extends ImportacaoArquivoWork {

	protected static final Logger log = Log.getLogger("importacao_transacao");
	protected static final Logger logError = Log.getLogger("importacao_transacao_error");
	private File file;
	private Scanner scanner;
	private ImportarArquivoResponse response;

	private int linhas;
	private double porcentagem;

	@Autowired
	protected LogImportacaoService logImportacaoService;
	@Autowired
	protected LinhaImportacaoService linhaImportacaoService;
	@Autowired
	protected OperacoesCieloService operacoesCieloService;
	@Autowired
	protected EmpresaService empresaService;

	private Session session;
	private List<LinhaImportacao> linhaErros = new ArrayList<LinhaImportacao>();

	private Long idEmpresa;
	private Dialect dialect;

	@Override
	public void init(Session session, File file, Dialect dialect) throws DomainException, IOException {
		this.session = session;
		this.file = file;
		this.dialect = dialect;

		idEmpresa = JobImportacaoUtil.getIdEmpresa(file);

		linhas = countLines(file.getAbsolutePath());
		JobInfo jobInfo = JobInfo.getInstance(idEmpresa);
		jobInfo.setFim(linhas);
		jobInfo.setTemArquivo(true);
		jobInfo.setStatus("");

		Empresa empresa = empresaService.get(idEmpresa);
		if (empresa == null) {
			throw new DomainException("Empresa não cadastrada no sistema");
		}
		jobInfo.setEmpresa(empresa);

		// Salva map com todos os grupos
		log.debug("Carregando grupos");
		operacoesCieloService.deleteAll(idEmpresa);
	}

	@Override
	public void execute(Connection conn) throws SQLException {
		if (file == null || !file.exists()) {
			throw new IllegalArgumentException("Arquivo inválido");
		}

		response = new ImportarArquivoResponse();
		int countOk = 0;
		int countError = 0;

		long timeA = System.currentTimeMillis();

		Date inicio = new Date();
		
		/**	 https://docs.oracle.com/javase/7/docs/api/java/nio/charset/Charset.html  **/
		String set = ParametrosMap.getInstance(idEmpresa).get(Params.IMPORTACAO_ARQUIVO_CHARSET, "UTF-8");
		final Charset ENCODING = Charset.isSupported(set) ? Charset.forName(set): Charset.forName("UTF-8");
		Path path = Paths.get(file.getAbsolutePath());

		PreparedStatement stmtInsert = createStatementInsert(dialect, conn);
		PreparedStatement stmtUpdate = conn.prepareStatement("update operacoes_cielo set volumetria=?,pico_tps=?,media=?, momento=?, empresa_id=? where id=?");

		int row = 0;
		JobInfo jobInfo = JobInfo.getInstance(idEmpresa);
		try {
			log.debug("Iniciou ImportarArquivoWork com Batch ...");

			scanner = new Scanner(path, ENCODING.name());

			String linha = "";
			jobInfo.setStatus("Inserindo transacoes");
			jobInfo.setSuccessful(true);
			while (scanner.hasNextLine()) {
				try {
					row++;

					boolean header = row == 1;

					linha = scanner.nextLine();
					String[] split = StringUtils.split(linha, ";");

					if (!header) {
						// String cod = JobImportacaoUtil.get(split, 1);
						String data = JobImportacaoUtil.get(split, 2);
						String volumetria = JobImportacaoUtil.get(split, 3);
						String picoTps = JobImportacaoUtil.get(split, 4);
						String media = JobImportacaoUtil.get(split, 5);
						String momento = JobImportacaoUtil.get(split, 6);


						// insert into transacao
						log("insert row[" + row + "] " + data);
						stmtInsert.setDate(1, DateUtils.toSqlDate(DateUtils.toDate(data)));
						
						if (NumberUtils.isNumber(volumetria)) {
							stmtInsert.setDouble(2, Double.parseDouble(volumetria));
						} else {
							stmtInsert.setNull(2, java.sql.Types.DOUBLE);
						}
						
						if (NumberUtils.isNumber(picoTps)) {
							stmtInsert.setDouble(3, Double.parseDouble(picoTps));
						} else {
							stmtInsert.setNull(3, java.sql.Types.DOUBLE);
						}
						
						if (NumberUtils.isNumber(media)) {
							stmtInsert.setDouble(4, Double.parseDouble(media));
						} else {
							stmtInsert.setNull(4, java.sql.Types.DOUBLE);
						}

						if (NumberUtils.isNumber(momento)) {
							stmtInsert.setDouble(5, Double.parseDouble(momento));
						} else {
							stmtInsert.setNull(5, java.sql.Types.DOUBLE);
						}
						stmtInsert.setLong(6, idEmpresa);
						stmtInsert.execute();

						Long id = SQLUtils.getGeneratedId(stmtInsert);
						if (id == null) {
							LinhaImportacao erro = new LinhaImportacao();
							erro.setLinha(linha);
							erro.setMensagem("Erro ao inserir transacao " + data);
							erro.setNumeroLinha(row);
							linhaErros.add(erro);
							logError.error("Erro row [" + row + "] ao inserir transacao data: " + data);
							continue;
						}
						response.countInsert++;
						countOk++;

						porcentagem = ((double) (row - 1) * 100) / (double) linhas;
						jobInfo.setPorcentagem(porcentagem);
						jobInfo.setAtual(row);
					} else {
						jobInfo.setHeader(linha);
					}

				} catch (Exception e) {
					countError++;
					LinhaImportacao erro = new LinhaImportacao();
					erro.setLinha(linha);
					erro.setMensagem(e.getMessage());
					erro.setNumeroLinha(row);
					linhaErros.add(erro);
					jobInfo.setSuccessful(false);
					logError.error("Erro transacao linha [" + row + "]: " + e.getMessage(), e);
				}
			}

			long timeB = System.currentTimeMillis();
			log("TimeB min: " + (timeB - timeA) / 1000 / 60);

		} catch (Exception e) {
			jobInfo.setSuccessful(false);
			String msgError = "Erro importar areas cargos[" + row + "] " + e.getMessage();
			logError.error(msgError, e);
			throw new SQLException(msgError, e);
		} finally {
			
			JdbcUtils.closeStatement(stmtInsert);
			JdbcUtils.closeStatement(stmtUpdate);

			HibernateUtil.clearCache(session);

			response.countOk = countOk;
			response.countError = countError;
			response.nomeArquivo = JobImportacaoUtil.getNomeFileSemEmpresa(file);
			jobInfo.setResponse(response);

			finish(jobInfo);
			
			log.debug("Início: " + inicio);
			log.debug("Final: " + new Date());
			long timeC = System.currentTimeMillis();
			log("TimeC min: " + (timeC - timeA) / 1000 / 60);
		}
	}

	private void finish(JobInfo jobInfo) {
		// Log_Importacao
		LogImportacao logImportacao = new LogImportacao(jobInfo);
		try {
			logImportacaoService.saveOrUpdate(logImportacao);
			jobInfo.response.idArquivo = logImportacao.getId();
			for (LinhaImportacao erro : linhaErros) {
				erro.setLogImportacao(logImportacao);
				linhaImportacaoService.saveOrUpdate(erro);
			}
		} catch (DomainException e) {
			jobInfo.setSuccessful(false);
			log.error("ERRO" + e.getMessage(),e);
		}
		jobInfo.setStatus("Fim da Importação");
		jobInfo.setTemArquivo(false);
		jobInfo.setFim(0);
		jobInfo.setAtual(0);
		jobInfo.setAtual(0);
	}

	private void log(String string) {
		JobInfo.getInstance(idEmpresa).lastMessageImportacao = string;
		log.debug(string);
	}

	public ImportarArquivoResponse getResponse() {
		return response;
	}
	
	private PreparedStatement createStatementInsert(Dialect dialect, Connection conn) throws SQLException {
		
		if(dialect instanceof OracleDialect) {
			return conn.prepareStatement("insert into operacoes_cielo (id,data,volumetria,pico_tps,media,momento, empresa_id) VALUES(TRANSACAO_SEQ.nextval,?,?,?,?,?,?)", new String [] {"id"});
		}

		return conn.prepareStatement("insert into operacoes_cielo (data,volumetria,pico_tps,media,momento, empresa_id) VALUES(?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
		
	}
}
