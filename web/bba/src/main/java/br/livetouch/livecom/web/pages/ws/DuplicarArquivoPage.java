package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.ThumbVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class DuplicarArquivoPage extends WebServiceXmlJsonPage {

	public Form form = new Form();

	private TextField tId;
	private TextField tMode;
	

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tId = new TextField("id", "ids dos arquivos", true));
		
		form.add(tMode = new TextField("mode", "mode: xml, json"));
		
		form.add(new Submit("Upload File"));
		tMode.setValue("json");
		setFormTextWidth(form,350);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			
			String ids = tId.getValue();
			String[] split = StringUtils.split(ids, ";");
			if(split == null) {
				return new MensagemResult("Erro", "Form inválido.");
			}
			
			List<FileVO> files = new ArrayList<FileVO>();
			for (String string : split) {
				Long id = Long.parseLong(string);
				Arquivo a = arquivoService.get(id);
				if(a != null) {
					Arquivo ar = arquivoService.duplicar(a, null);
					FileVO f = new FileVO();
					f.setArquivo(ar, true);
					files.add(f);
				}
			}				
			return files;
		}
		return new MensagemResult("Erro", "Form inválido.");
	}

	protected void xstream(XStream x) {
		x.alias("file", FileVO.class);
		x.alias("thumb", ThumbVO.class);
		super.xstream(x);
	}
}
