package br.livetouch.livecom.web.pages.cadastro;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.vo.CamposMap;
import br.livetouch.livecom.web.pages.admin.LivecomAdminPage;


/**
 * 
 * @author ricardo
 *
 */
@Controller
@Scope("prototype")
public class CadastrosPage extends LivecomAdminPage {

	public CamposMap campos = getCampos();
	
	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
	}
}
