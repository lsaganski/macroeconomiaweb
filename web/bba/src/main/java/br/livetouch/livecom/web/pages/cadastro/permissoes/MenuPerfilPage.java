package br.livetouch.livecom.web.pages.cadastro.permissoes;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.web.pages.LivecomRootEmpresaPage;


/**
 * 
 * @author ricardo
 *
 */
@Controller
@Scope("prototype")
public class MenuPerfilPage extends LivecomRootEmpresaPage {

	public Long id;
	public Perfil perfil;
	
	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
		
		if(id != null) {
			perfil = perfilService.get(id);
		}
	}
	
}
