package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LinhaImportacao;
import br.livetouch.livecom.domain.LogImportacao;
import br.livetouch.livecom.domain.repository.LinhaImportacaoRepository;
import br.livetouch.livecom.domain.repository.LogImportacaoRepository;
import br.livetouch.livecom.domain.service.LogImportacaoService;
import br.livetouch.livecom.domain.vo.LogImportacaoFiltro;
import br.livetouch.livecom.domain.vo.LogImportacaoVO;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class LogImportacaoServiceImpl implements LogImportacaoService {
	@Autowired
	private LogImportacaoRepository rep;
	@Autowired
	private LinhaImportacaoRepository repLinhaImportacao;

	@Override
	public LogImportacao get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(LogImportacao c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<LogImportacao> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	@Override
	public void delete(LogImportacao c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public List<LogImportacaoVO> findAllVO(Empresa empresa) {
		List<LogImportacaoVO> vos = new ArrayList<LogImportacaoVO>();
		List<LogImportacao> findAll = findAll(empresa);
		for (LogImportacao logImportacao : findAll) {
			LogImportacaoVO vo = new LogImportacaoVO(logImportacao);
			vos.add(vo);
		}
		return vos;
	}

	@Override
	public List<String> findAllTiposCsv(Empresa empresa) {
		return rep.findAllTiposCsv(empresa);
	}

	@Override
	public List<LogImportacaoVO> findAllVOByFiltro(LogImportacaoFiltro filtro, Empresa empresa) {
		List<LogImportacao> all = findAllByFiltro(filtro, empresa);
		List<LogImportacaoVO> vos = new ArrayList<LogImportacaoVO>();
		for (LogImportacao logImportacao : all) {
			LogImportacaoVO vo = new LogImportacaoVO(logImportacao);
			vos.add(vo);
		}
		return vos;
	}

	@Override
	public List<LogImportacao> findAllByFiltro(LogImportacaoFiltro filtro, Empresa empresa) {
		return rep.findAllByFiltro(filtro, empresa);
	}

	@Override
	public long countByFiltro(LogImportacaoFiltro filtro, Empresa empresa) {
		return rep.countByFiltro(filtro, empresa);
	}

	@Override
	public String csvLogImportacao(Long id) {
		List<LinhaImportacao> findAllByIdLog = repLinhaImportacao.findAllByIdLog(id);
		LogImportacao logImportacao = get(id);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add(logImportacao.getHeader());
		for (LinhaImportacao d : findAllByIdLog) {
			body.add(d.getLinha());
			body.end();
		}
		return generator.toString();
	}

}
