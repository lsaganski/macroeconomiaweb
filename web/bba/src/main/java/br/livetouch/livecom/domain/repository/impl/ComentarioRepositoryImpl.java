package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusPublicacao;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.repository.ComentarioRepository;
import br.livetouch.livecom.domain.vo.PostCountVO;
import br.livetouch.livecom.domain.vo.UserToPushVO;

@SuppressWarnings("unchecked")
@Repository
public class ComentarioRepositoryImpl extends LivecomRepository<Comentario> implements ComentarioRepository {

	public ComentarioRepositoryImpl() {
		super(Comentario.class);
	}

	@Override
	
	public List<Comentario> findAllByUserAndPost(long userId, long postId, int page, int max) {
		StringBuilder sql = new StringBuilder("from Comentario c where 1=1");
		if(userId > 0) {
			sql.append(" and c.usuario.id = :userId");
		}
		if(postId > 0) {
			sql.append(" and c.post.id = :postId");
		}
		sql.append(" order by c.id desc");
		Query q = createQuery(sql.toString());
		if(userId > 0) {
			q.setParameter("userId", userId);
		}
		if(postId > 0) {
			q.setParameter("postId", postId);
		}

		if( max > 0) {
			int firstResult =  page * max;
			q.setFirstResult(firstResult);
			q.setMaxResults(max);
		}
		
		q.setCacheable(true);
		List<Comentario> list = q.list();
		return list;
	}

	
	@Override
	public List<Comentario> findAllComentariosNotificationByUser(Usuario user) {
		Query q = null;

		StringBuffer sb = new StringBuffer("select f from Comentario f inner join f.post p where 1=1 ");

		if(user != null) {
			// se o post é seu, e o favorito é do outro. Nao deixa ler seu proprio favorito
			sb.append(" and p.usuario.id = :userId and f.usuario.id != :userId");
		}

		sb.append(" and (f.lidaNotification=0 or f.lidaNotification is null) ");

		sb.append(" order by f.id desc");

		// Query
		q = createQuery(sb.toString());

		if(user != null) {
			q.setParameter("userId", user.getId());
		}

//		if( maxRows > 0) {
//			int firstResult =  page * maxRows;
//			q.setFirstResult(firstResult);
//			q.setMaxResults(maxRows);
//		}

		q.setCacheable(true);
		List<Comentario> list = q.list();

		return list;
	}
	
	@Override
	public List<PostCountVO> findCountByPosts(List<Long> ids) {
		
		StringBuffer sql = new StringBuffer("select new br.livetouch.livecom.domain.vo.PostCountVO(p,count(f.id)) from Post p ");
		
		// Like
		sql.append(" inner join p.comentarios f");

		// Where
		sql.append(" where 1=1 ");
		
		if(ids != null && ids.size() > 0) {
			sql.append(" and p.id in (:postsIds)");
		}

		// Like
		//sql.append(" and (f.favorito = 1 and f.comentario is null)");

		// Group by
		sql.append(" group by p");

		Query q = createQuery(sql.toString());

		if(ids != null && ids.size() > 0) {
			q.setParameterList("postsIds", ids);
		}

		q.setCacheable(true);

		List<PostCountVO> list = q.list();
		
		return list;
	}
	
	@Override
	public List<PostCountVO> findBadgesByPosts(Usuario userInfo, List<Long> ids) {
		
		StringBuffer sql = new StringBuffer("SELECT new br.livetouch.livecom.domain.vo.PostCountVO(n.post,count(notUser.id)) from NotificationUsuario notUser inner join notUser.notification n inner join n.post p where "
				+ "n.tipo =:tipo "
				+ "and notUser.lida = 0 "
				+ "and notUser.usuario.id = :usuarioId "
				+ "and p.statusPublicacao = :publicado");

		if(ids != null && ids.size() > 0) {
			sql.append(" and n.post.id in (:postsIds)");
		}
		
		// group by pra fazer count
		sql.append(" group by n.post ");

		Query q = createQuery(sql.toString());

		if(ids != null && ids.size() > 0) {
			q.setParameterList("postsIds", ids);
		}

		q.setParameter("usuarioId", userInfo.getId());
		q.setParameter("tipo", TipoNotificacao.COMENTARIO);
		q.setParameter("publicado", StatusPublicacao.PUBLICADO);

		q.setCacheable(true);

		List<PostCountVO> list = q.list();
		
		return list;
	}

	@Override
	public Long countByPost(Post p) {
		Criteria c = createCriteria();
		if(p != null) {
			c.add(Restrictions.eq("post.id", p.getId()));
		}
		c.setProjection(Projections.rowCount());
		c.setCacheable(true);
		long count = getCount(c);
		return count;
	}

	
	@Override
	public List<Long> findAllIdsByUser(Usuario user) {
		return queryIds("select c.id from Comentario c where c.usuario.id=?",true,user.getId());
	}
	
	@Override
	public List<Long> findAllByPost(Post p) {
		List<Long> list = queryIds("select id from Comentario c where c.post.id=?",true,p.getId());
		return list;
	}

	@Override
	public List<Comentario> findByPost(Post p) {
		List<Comentario> list = query("from Comentario c where c.post.id=?",true,p.getId());
		return list;
	}

	@Override
	public List<Usuario> findAllUsuariosQueComentaramPost(Post p, Usuario usuarioNot) {
		Query q = null;

		StringBuffer sb = new StringBuffer("select distinct u from Comentario c ");
		sb.append("inner join c.usuario u where c.post.id=? and u.id <> ? ");
		sb.append("AND u.permissao.visualizarComentario = 1");

		// Query
		q = createQuery(sb.toString());

		q.setParameter(0, p.getId());
		q.setParameter(1, usuarioNot.getId());

		q.setCacheable(true);
		List<Usuario> list = q.list();

		return list;
	}

	@Override
	public List<UserToPushVO> findAllUsersQueComentaramPost(Post p, Usuario usuarioNot) {
		Query q = null;
		
		StringBuffer sb = new StringBuffer("select distinct u.id,u.login,u.email, pu.push, pu.notification from Comentario c ");
		sb.append("inner join c.usuario u left join u.postUsuarios pu where c.post.id= :id and u.id <> :user ");
		sb.append("AND u.permissao.visualizarComentario = 1 and u.perfilAtivo = 1");
		sb.append("AND u.permissao.id in (select pp.perfil.id from PerfilPermissao pp where pp.perfil.id = u.permissao.id and pp.ligado = 1 and pp.permissao.codigo = :codigo)");
		
		// Query
		q = createQuery(sb.toString());
		
		q.setParameter("id", p.getId());
		q.setParameter("user", usuarioNot.getId());
		q.setParameter("codigo", ParamsPermissao.VIZUALIZAR_COMENTARIOS);
		
//		q.setCacheable(true);
		
		List<UserToPushVO> list = UserToPushVO.hibernate(q.list());
		
		return list;
	}
	
	@Override
	public List<Comentario> findAllByDate(Date data) {
		
		StringBuffer sb = new StringBuffer("from Comentario c where c.data >= :data");
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("data", data);
		
		return q.list();
	}
}