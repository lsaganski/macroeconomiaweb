package br.livetouch.livecom.web.pages.ws;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.PostField;
import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Playlist;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.PlaylistVO;
import net.sf.click.control.FieldSet;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class PlaylistPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private TextField tAction;
	private UsuarioLoginField tUser;
	private PostField postField;
	public List<PlaylistVO> playlists;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tUser = new UsuarioLoginField("user", true,usuarioService, getEmpresa()));
		form.add(tAction = new TextField("action","list , save , delete"));
		form.add(tMode = new TextField("mode"));
		
		FieldSet set = new FieldSet("Save / Delete");
		set.add(postField = new PostField(postService));
		form.add(set);
		
		tAction.setValue("list");

		tUser.setFocus(true);

		tMode.setValue("json");

		form.add(new Submit("consultar"));

		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario u = tUser.getEntity();
			if(u == null) {
				return new MensagemResult("NOK" ,"Usuario nao encontrado");
			}

			boolean actionList = "list".equalsIgnoreCase(tAction.getValue());
			boolean actionSave = "save".equalsIgnoreCase(tAction.getValue());
			boolean actionDelete = "delete".equalsIgnoreCase(tAction.getValue());
			
			if(actionList) {
				List<Playlist> list = playlistService.findAllByUser(u);
				playlists = PlaylistVO.toListVO(list);
				return playlists;
			} else if(actionSave || actionDelete) {

				Post p = postField.getPost();
				if(p == null) {
					return new MensagemResult("NOK" ,"Post nao encontrado.");
				}
				
				Playlist h = playlistService.findByUserPost(u,p);

				if(h == null) {
					h = new Playlist();
					h.setPost(p);
					h.setUsuario(u);
				}
				
				if(actionSave) {
					h.setData(new Date());

					playlistService.saveOrUpdate(h);
					
					PlaylistVO vo = new PlaylistVO();
					vo.setPlaylist(h);
					
					return vo;
				} else {
					playlistService.delete(h);
					
					return new MensagemResult("OK","Playlist excluido com sucesso.");
				}
			}

			return new MensagemResult("NOK","Invalid action.");
		}

		return new MensagemResult("NOK","Invalid parameters.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("playlist", PlaylistVO.class);
		super.xstream(x);
	}
}
