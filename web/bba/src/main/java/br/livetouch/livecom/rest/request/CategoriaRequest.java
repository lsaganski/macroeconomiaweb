package br.livetouch.livecom.rest.request;

import java.io.Serializable;

import br.livetouch.livecom.domain.CategoriaPost;

public class CategoriaRequest implements Serializable{
	private static final long serialVersionUID = -437795313681769467L;
	
	public Long id;
	public String arquivo_ids;
	public String codigo;
	public String cor;
	public Boolean hideData;
	public Boolean likeable;
	public Boolean menu;
	public String nome;
	public Boolean padrao;
	public String urlIcone;
	public CategoriaPost categoriaParent;
	
	public CategoriaPost toCategoria() {
		CategoriaPost c = new CategoriaPost();
		c.setId(id);
		c.setCategoriaParent(categoriaParent);
		c.setCodigo(codigo);
		c.setCor(cor.replace("#", ""));
		c.setHideData(hideData);
		c.setLikeable(likeable);
		c.setMenu(menu);
		c.setNome(nome);
		c.setPadrao(padrao);
		c.setUrlIcone(urlIcone);
		return c;
	}
	
	
}
