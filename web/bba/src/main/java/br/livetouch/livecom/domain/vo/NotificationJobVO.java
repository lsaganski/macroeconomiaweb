package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;

public class NotificationJobVO implements Serializable {
	private static final long serialVersionUID = 1L;

	public Post post;
	public boolean sendPushAutor;
	public Usuario usuario;
	public Notification notification;
	public boolean isSendPush;
	
	public NotificationJobVO(Post post, boolean sendPushAutor, Usuario usuario, Notification notification, boolean isSendPush) {
		super();
		this.post = post;
		this.sendPushAutor = sendPushAutor;
		this.usuario = usuario;
		this.notification = notification;
		this.isSendPush = isSendPush;
	}
	
}
