package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.StatusMensagemUsuario;
import br.livetouch.livecom.domain.Usuario;

@Repository
public interface StatusMensagemUsuarioRepository extends net.livetouch.tiger.ddd.repository.Repository<StatusMensagemUsuario> {
	int MAX = 20;
	
	List<StatusMensagemUsuario> findAllByUserConversa(Usuario u, MensagemConversa c);

	List<StatusMensagemUsuario> findAllByConversa(MensagemConversa c);

	int updateStatusMensagem(Long userId, Long conversaId, int status);

	List<StatusMensagemUsuario> findAllByMensagem(Mensagem m);
	
	List<Mensagem> findAllMessagesNaoRecebidasUserConversa(Usuario u, MensagemConversa c);

	List<Mensagem> findAllMessagesNaoExcluidasUserConversa(Usuario user, MensagemConversa conversa);

	int updateStatusMensagemGrupo(Long userId, Long mensagemId, Integer status);

	void delete(Usuario u, Grupo grupo);

	StatusMensagemUsuario findByMsgUsuario(Mensagem msg, Usuario u);
	
}