package br.livetouch.livecom.domain;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.domain.service.StatusMensagemUsuarioService;
import br.livetouch.livecom.utils.DateUtils;

/**
 * @author Ricardo Lecheta
 *
 */

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Mensagem extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;

	public static final int TIPO_ENTRADA = 1;
	public static final int TIPO_ENVIADA = 2;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "MENSAGEM_SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "conversa_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private MensagemConversa conversa;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_from_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Usuario from;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_to_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Usuario to;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "location_id")
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Location location;

	@Column(length=2000)
	private String msg;

	private Date dataCreated;
	private Date dataUpdated;
	private Date dataPush;

	/**
	 * Usuario leu: 2 check azul.
	 */
	private boolean lida;
	private Date dataLida;

	/**
	 * Usuario recebeu: 2 check cinza.
	 */
	private boolean entregue;
	private Date dataEntregue;
	
	/**
	 * Callbacks para garantir entrega
	 */
	private boolean callbackCheck1Cinza;
	private boolean callbackCheck2Cinza; // ok
	private boolean callbackCheck2Azul; // ok
	
	/**
	 * 1 - recebida
	 * 2 - enviada
	 */
	private int status_in;
	
	/**
	 * 0 - nao lida
	 * 1 - lida 
	 */
	private int status_out;
	
	private int excluida_from;
	private int excluida_to;
	
	@OneToMany(mappedBy = "mensagem", fetch = FetchType.LAZY)
	@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
	private Set<Arquivo> arquivos;
	
	@OneToMany(mappedBy = "mensagem", fetch = FetchType.LAZY)
	@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
	private Set<Card> cards;
	
	@OneToMany(mappedBy = "mensagem", fetch = FetchType.LAZY)
	@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
	private Set<StatusMensagemUsuario> statusUsuarios;

	private Long identifier;
	
	/**
	 * Msgs de sistema do admin, ex: add user, remover user.
	 */
	private boolean msgAdmin;

	@OneToMany(mappedBy = "mensagem", fetch = FetchType.LAZY)
	@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
	private Set<CardButton> buttons;
	
	@Override
	public Long getId() {
		return id;
	}

	
	public String getDataUpdatedString() {
		return DateUtils.toDateChat(dataUpdated);
	}
	
	public String getDataNotificacaoString() {
		String dataStr = null;
		Date dt =  dataUpdated != null ? dataUpdated : dataCreated;
		if(dt == null) {
			return "Hoje";
		}
		long timestamp = dt.getTime();
		if(DateUtils.isToday(timestamp)) {
			dataStr = "Hoje";
		} else if(DateUtils.isYesterday(timestamp)) {
			dataStr = "Ontem";
		} else {
			dataStr = net.livetouch.extras.util.DateUtils.toString(dt, "dd/MM/yyyy HH:mm:ss");;
		}
		return dataStr;
	}
	
	public void setArquivos(Set<Arquivo> arquivos) {
		this.arquivos = arquivos;
	}

	public MensagemConversa getConversa() {
		return conversa;
	}

	public void setConversa(MensagemConversa conversa) {
		this.conversa = conversa;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Date getDataCreated() {
		return dataCreated;
	}
	
	public Date getDataPush() {
		return dataPush;
	}
	
	public void setDataPush(Date dataPush) {
		this.dataPush = dataPush;
	}

	public void setDataCreated(Date dataCreated) {
		this.dataCreated = dataCreated;
	}

	public Date getDataUpdated() {
		return dataUpdated;
	}

	public void setDataUpdated(Date dataUpdated) {
		this.dataUpdated = dataUpdated;
	}

	public int getStatus_out() {
		return status_out;
	}

	public void setStatus_out(int status_out) {
		this.status_out = status_out;
	}

	public int getStatus_in() {
		return status_in;
	}

	public void setStatus_in(int status_in) {
		this.status_in = status_in;
	}

	public Set<Arquivo> getArquivos() {
		return arquivos;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public boolean isLida() {
		return lida;
	}
	
	public void setLida(boolean lida) {
		this.lida = lida;
	}
	
	public boolean isCallbackCheck1Cinza() {
		return callbackCheck1Cinza;
	}
	
	public boolean isCallbackCheck2Azul() {
		return callbackCheck2Azul;
	}
	
	public boolean isCallbackCheck2Cinza() {
		return callbackCheck2Cinza;
	}
	
	public void setCallbackCheck1Cinza(boolean callbackCheck1Cinza) {
		this.callbackCheck1Cinza = callbackCheck1Cinza;
	}
	
	public void setCallbackCheck2Azul(boolean callbackCheck2Azul) {
		this.callbackCheck2Azul = callbackCheck2Azul;
	}
	
	public void setCallbackCheck2Cinza(boolean callbackCheck2Cinza) {
		this.callbackCheck2Cinza = callbackCheck2Cinza;
	}
	
	public void setFrom(Usuario from) {
		this.from = from;
	}
	
	public void setEntregue(boolean entregue) {
		this.entregue = entregue;
	}

	public boolean isEntregue() {
		return entregue;
	}
	
	public Date getDataEntregue() {
		return dataEntregue;
	}
	
	public Date getDataLida() {
		return dataLida;
	}
	
	public void setDataEntregue(Date dataEntregue) {
		this.dataEntregue = dataEntregue;
	}
	
	public void setDataLida(Date dataLida) {
		this.dataLida = dataLida;
	}

	public void setTo(Usuario to) {
		this.to = to;
	}
	
	public Usuario getFrom() {
		return from;
	}
	
	public Usuario getTo() {
		return to;
	}
	
	public static void sortByIdLastFirst(List<Mensagem> list) {

		Collections.sort(list, new Comparator<Mensagem>() {
			@Override
			public int compare(Mensagem m1, Mensagem m2) {
				return m1.getId().compareTo(m2.getId());
			}
		});		
	
	}
	
	public static void sortById(List<Mensagem> list) {
		sortByIdLastFirst(list);
		Collections.reverse(list);
	}

//	public static void sortDescByNaoLidasAndId(List<Mensagem> list) {
//		Collections.sort(list, new Comparator<Mensagem>() {
//			@Override
//			public int compare(Mensagem m1, Mensagem m2) {
//				if(m1.isLida() && m2.isLida()) {
//					return m1.getId().compareTo(m2.getId());
//				}
//				if(m1.isLida() && !m2.isLida()) {
//					return 1;
//				}
//				
//				return 0;
//			}
//		});
////		Collections.reverse(list);
//	}

	public boolean isTo(Usuario user) {
		if(user == null || getTo() == null) {
			return false;
		}
		return user.getId().equals(getTo().getId());
	}
	
	public boolean isFrom(Usuario user) {
		if(user == null || getFrom() == null) {
			return false;
		}
		return user.getId().equals(getFrom().getId());
	}
	
	public String getDataCreatedString() {
		if(dataCreated == null) {
			return "";
		}
		return net.livetouch.extras.util.DateUtils.toString(dataCreated, "dd/MM/yyyy HH:mm:ss");
	}
	
	public String getDataCreatedStringChat() {
		return DateUtils.toDateStringChat(dataCreated);
	}

	public String getDataCreatedStringHojeOntem() {
		if(dataCreated == null) {
			return "";
		}
		return DateUtils.toDateStringHojeOntem(dataCreated);
	}
	
	public String getDataUpdatedStringChat() {
		return DateUtils.toDateStringChat(dataUpdated);
	}

	public String getDataUpdatedStringHojeOntem() {
		if(dataUpdated == null) {
			return "";
		}
		return DateUtils.toDateStringHojeOntem(dataUpdated);
	}
	
	public String getDataEntregueString() {
		if(dataEntregue == null) {
			return "";
		}
		return net.livetouch.extras.util.DateUtils.toString(dataEntregue, "dd/MM/yyyy HH:mm:ss");
	}

	public String getDataEntregueStringHojeOntem() {
		return DateUtils.toDateChat(dataEntregue);
	}

	public String getDataPushStringHojeOntem() {
		return DateUtils.toDateChat(dataPush);
	}
	
	public String getDataLidaString() {
		if(dataLida == null) {
			return "";
		}
		// TODO fazer lib retornar "" e gerar nova versao.
		return net.livetouch.extras.util.DateUtils.toString(dataLida, "dd/MM/yyyy HH:mm:ss");
	}

	public String getDataLidaStringHojeOntem() {
		return DateUtils.toDateChat(dataLida);
	}

	public int getExcluida_from() {
		return excluida_from;
	}

	public void setExcluida_from(int excluida_from) {
		this.excluida_from = excluida_from;
	}

	public int getExpluida_to() {
		return excluida_to;
	}

	public void setExcluida_to(int excluida_to) {
		this.excluida_to = excluida_to;
	}

	public boolean isExcluidaFrom(Usuario userInfo) {
		return false;
		/*
		if(userInfo == null) {
			return false;
		}
		if (conversa.getGrupo() == null && isToFrom()) {
			Long userInfoId = userInfo.getId();
			boolean excluidaFrom = getFrom().getId() == userInfoId  &&   getExcluida_from() == 1;
			boolean excluidaTo = getTo().getId() == userInfoId  &&   getExpluida_to() == 1;
			
			boolean excluida = excluidaFrom || excluidaTo;
			
			return excluida;
		} else {
			//  SQL aqui ao consultar conversas de grupo faz select no usuario
			// Removi pois nao estamos tratanto o excluida
//			for (StatusMensagemUsuario smu :  statusUsuarios) {
//				if (smu.getUsuario().getId().equals(userInfo.getId())) {
//					return smu.isExcluida();
//				}
//			}
			
			return true;
		}	*/	
	}


	protected boolean isToFrom() {
		return getTo() != null && getFrom() != null;
	}

	public static void print(List<Mensagem> mensagens) {
		if (mensagens != null) {
			for (Mensagem mensagem : mensagens) {
				System.out.println(mensagem.getId() + " - " + mensagem.getMsg());
			}
		}	
	}
	
	public static List<Long> getIds(Collection<Mensagem> list) {
		List<Long> ids = new ArrayList<Long>();
		if(list == null || list.size() == 0) {
			return ids;
		}
		for (net.livetouch.tiger.ddd.Entity e : list) {
			ids.add(e.getId());
		}
		return ids;
	}

	public Set<StatusMensagemUsuario> getStatusUsuarios() {
		return statusUsuarios;
	}

	public void setStatusUsuarios(Set<StatusMensagemUsuario> statusUsuarios) {
		this.statusUsuarios = statusUsuarios;
	}

	/**
	 * Se pode visualizar a msg
	 * 
	 * @param user
	 * @return
	 */
	public boolean isFromTo(Usuario user) {
		if(user == null) {
			return false;
		}
		if(isGrupo()) {
			Grupo grupo = conversa.getGrupo();
			Set<GrupoUsuarios> usuarios = grupo.getUsuarios();
			for (GrupoUsuarios u : usuarios) {
				if(u.getUsuario().getId().equals(user.getId())) {
					return true;
				}
			}
			return false;
		}
		return isFrom(user) || isTo(user);
	}

	public boolean isGrupo() {
		return conversa.getGrupo() != null;
	}

	public Grupo getGrupo() {
		return conversa.getGrupo();
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}
	
	public Long getIdentifier() {
		return this.identifier;
	}
	
	public boolean isMsgAdmin() {
		return msgAdmin;
	}
	
	public void setMsgAdmin(boolean msgAdmin) {
		this.msgAdmin = msgAdmin;
	}

	public String toStringDesc() {
		return toString();
	}
	
	@Override
	public String toString() {
		return "id=" + id + ", msg=" + msg;
	}

	public String getHoraCriada() {
		return net.livetouch.extras.util.DateUtils.toString(dataCreated, "HH:mm");
	}

	public String getDataWeb() {
		boolean isHoje = net.livetouch.extras.util.DateUtils.equals(new Date(), dataCreated);
		if(isHoje) {
			return getHoraCriada();
		} 
		return net.livetouch.extras.util.DateUtils.toString(dataCreated, "dd/MM/yyyy");
	}

	public String getDataString() {
		return DateUtils.toDateChat(dataCreated);
	}


	public void setLida(Mensagem mensagem, MensagemConversa c, Usuario user,
			StatusMensagemUsuarioService statusMensagemUsuarioService) {

		
		if(!mensagem.isFrom(user)) {
			if(c.isGrupo()) {
				/**
				 * Se msg nao for minha e eh de grupo.
				 * Marca como lida, se eu ja li. 
				 * O que importa eh meu status, e nao dos outros.
				 */
				StatusMensagemUsuario status = statusMensagemUsuarioService.findByMsgUsuario(mensagem, user);
				if(status != null) {
					mensagem.setLida(status.isLida());
				}
			}
		}
	}


	public Set<Card> getCards() {
		return cards;
	}


	public void setCards(Set<Card> cards) {
		this.cards = cards;
	}


	public void setButtons(Set<CardButton> cardButtons) {
		this.buttons = cardButtons;
	}
	
	public Set<CardButton> getButtons() {
		return buttons;
	}
}
