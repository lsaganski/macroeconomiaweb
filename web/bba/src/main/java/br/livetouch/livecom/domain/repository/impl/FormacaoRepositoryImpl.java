package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Formacao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.FormacaoRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class FormacaoRepositoryImpl extends StringHibernateRepository<Formacao> implements FormacaoRepository {

	public FormacaoRepositoryImpl() {
		super(Formacao.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Formacao> findAllFormacao(Usuario userInfo) {
		StringBuffer sql = new StringBuffer("from Formacao f where 1=1 ");

		sql.append(" and f.empresa.id=:empresaId ");

		sql.append(" order by f.nome");

		Query q = createQuery(sql.toString());

		q.setParameter("empresaId", userInfo.getEmpresa().getId());

		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Formacao> findAll(Empresa empresa) {
		StringBuffer sql = new StringBuffer("from Formacao f where f.empresa=:empresa ");

		Query q = createQuery(sql.toString());

		q.setParameter("empresa", empresa);

		return q.list();
	}

}