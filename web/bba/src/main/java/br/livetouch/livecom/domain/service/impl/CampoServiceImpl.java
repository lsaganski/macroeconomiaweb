package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Campo;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.repository.CampoRepository;
import br.livetouch.livecom.domain.service.CampoService;
import br.livetouch.livecom.domain.vo.CamposMapCache;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class CampoServiceImpl implements CampoService {
	@Autowired
	private CampoRepository rep;

	@Override
	public Campo get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Campo c, Empresa e) throws DomainException {
		if(StringUtils.isEmpty(c.getNome())) {
			throw new DomainException("Informe o nome do campo");
		}

		if(e != null) {
			c.setEmpresa(e);
		}
		
		rep.saveOrUpdate(c);
		
		init(e);
	}

	@Override
	public List<Campo> findAll(Empresa e) {
		return rep.findAll(e);
	}

	@Override
	public void delete(Campo c, Empresa e) throws DomainException {
		rep.delete(c);
		
		init(e);
	}
	
	@Override
	public CamposMapCache init(Empresa e) {
		List<Campo> list = findAll(e);
		CamposMapCache params = CamposMapCache.getInstance(e);
		CamposMapCache.setCampos(list);
		return params;
	}
	
	public Campo findByNameValid(Campo campo, Empresa empresa){
		return rep.findByNameValid(campo, empresa);
	}
	
	
	
	
}