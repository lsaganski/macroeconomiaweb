package br.livetouch.livecom.web.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;

@Controller
@Scope("prototype")
public class LivecomRootPage extends LivecomLogadoPage {
	
	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
	}

	public boolean onSecurityCheck() {

		Usuario u = getUserInfo();
		boolean logado = u != null;
		if (logado) {
			if(u.isRoot()) {
				return true;
			}
		}
		
		setRedirect(AcessoNegadoPage.class);
		return false;
	}
}
