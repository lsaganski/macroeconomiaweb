package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.vo.AudienciaQuantidadeVO;
import br.livetouch.livecom.domain.vo.AudienciaVO;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.livecom.domain.vo.RelatorioAudienciaVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.TagVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

@Controller
@Scope("prototype")
public class AudienciaPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private LongField tPostId;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tPostId = new LongField("postId", true));
		
		form.add(new TextField("wsVersion"));

		form.add(tMode = new TextField("mode"));
		tMode.setValue("json");

		form.add(new Submit("ok"));

		tPostId.setFocus(true);

	}
	
	@Override
	protected Object execute() throws Exception {

		if (form.isValid()) {
			Post post = postService.get(tPostId.getLong());

			if (post == null) {
				if (isWsVersion3()) {
					return Response.error("Post não encontrado");
				}
				return new MensagemResult("NOK", "Post não encontrado");
			}

			AudienciaVO audienciaVO = notificationService.getCountAudienciaByPost(post);
			if (audienciaVO != null) {
				long total = postService.getCountUsuariosByPost(post);
				audienciaVO.setTotal(total);
				Long iteracao = audienciaVO.getIteracao() != null ? audienciaVO.getIteracao() : 0;
				audienciaVO.setRestante(total - iteracao);
			}

			RelatorioFiltro filtro = new RelatorioFiltro();
			filtro.setPostId(tPostId.getLong());
			filtro.setAgrupar(false);

			List<RelatorioAudienciaVO> audiencia = reportService.audienciaDetalhes(filtro);

			AudienciaQuantidadeVO retorno = new AudienciaQuantidadeVO(audienciaVO, audiencia);

			if (isWsVersion3()) {
				Response r = Response.ok("OK");
				r.audienciaQuantidade = retorno;
				return r;
			}

			return retorno;
		}

		if (isWsVersion3()) {
			return Response.error("Form invalido");
		}
		return new MensagemResult("NOK", "Form invalido");
	}


	@Override
	protected void xstream(XStream x) {
		x.alias("audienciaQuanidade", AudienciaQuantidadeVO.class);
		x.alias("post", PostVO.class);
		x.alias("arquivo", FileVO.class);
		x.alias("grupo", GrupoVO.class);
		x.alias("tags", TagVO.class);
		super.xstream(x);
	}
	
}
