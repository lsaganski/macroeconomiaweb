package br.livetouch.livecom.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.utils.LivecomUtil;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Palestrante extends net.livetouch.tiger.ddd.Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8771734275567884350L;

	public static final String SESSION_FILTRO_KEY = "filtro";

	public static final long TEMPO_EXPIRAR_OTP = 60;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "PALESTRANTE_SEQ")
	private Long id;
	
	private String nome;
	private String email;
	private String fotoS3;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "arquivo_foto_id", nullable = true)
	private Arquivo foto;
	
	@Column(columnDefinition = "clob")
	private String curriculo;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	public String getCurriculo() {
		return curriculo;
	}

	public void setCurriculo(String curriculo) {
		this.curriculo = curriculo;
	}

	public Arquivo getFoto() {
		return foto;
	}

	public void setFoto(Arquivo foto) {
		this.foto = foto;
	}
	
	public String getUrlFotoPalestrante(ParametrosMap map) {
		if(foto != null) {
			return foto.getUrl();
		}
		String url = getFotoS3();
		if (LivecomUtil.isUrl(url)) {
			return url;
		}
		return LivecomUtil.getUrlFotoUsuario(map, url);
	}

	public String getUrlFoto(ParametrosMap map) {
		return getUrlFotoPalestrante(map);
	}


	public String getFotoS3() {
		return fotoS3;
	}

	public void setFotoS3(String fotoS3) {
		this.fotoS3 = fotoS3;
	}
	
	

}
