package br.livetouch.livecom.web.pages.report.push;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
import net.livetouch.extras.util.DateUtils;

@Controller
@Scope("prototype")
public class UsuariosPushReportPage extends RelatorioPage {

	public RelatorioFiltro filtro;
	public int page;
	public String empresa;
	
	@Override
	public void onInit() {
		super.onInit();
		
		empresa = ParametrosMap.getInstance(getEmpresa()).get(Params.PUSH_SERVER_PROJECT, "");

		if (clear) {
			getContext().removeSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			filtro = new RelatorioFiltro();
			filtro.setDataIni(DateUtils.toString(br.livetouch.livecom.utils.DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
			filtro.setDataFim(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		} else {
			filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			if (filtro != null) {
				if (filtro.getUsuarioId() != null) {
					Usuario usuario = usuarioService.get(filtro.getUsuarioId());
					if (usuario != null) {
						filtro.setUsuario(usuario);
					}
				}
			}
		}
	}
}
