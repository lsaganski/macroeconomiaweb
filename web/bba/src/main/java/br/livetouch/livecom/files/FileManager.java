package br.livetouch.livecom.files;

import java.io.IOException;
import java.util.List;

public interface FileManager {

	List<LivecomFile> getFiles();
	LivecomFile getFile(String name);
	
	public List<LivecomFile> getFilesInFolder(String folder);
	
	public List<String> getFoldersInFolder(String folder);
	
	public String putFile(String dir, String fileName, String contentType, byte[] bytes) throws IOException;

	public String putFileLambda(String dir, String fileName, String contentType, byte[] bytes) throws IOException;

	public String putFileThumb(String dir, String fileName, String contentType, byte[] bytes) throws IOException;
	
	public void delete(String path) throws IOException;
	
	public String getFileUrl(String dir, String nome);
	
	public void createFolder(String dir) throws IOException;
	
	public boolean somenteFotosProfile(String dir);
	
	void close() throws IOException;
	
	public String getFileLambdaUrl(String dir, String nomeS3);

	public String getThumbLambdaURL(String dir, String nomeS3);
}
	