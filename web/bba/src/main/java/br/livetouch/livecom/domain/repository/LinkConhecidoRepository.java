package br.livetouch.livecom.domain.repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LinkConhecido;

public interface LinkConhecidoRepository extends net.livetouch.tiger.ddd.repository.Repository<LinkConhecido>  {

	LinkConhecido findByName(LinkConhecido link);

	LinkConhecido findByDominio(LinkConhecido link);

	void save(LinkConhecido link, Empresa empresa);

}
