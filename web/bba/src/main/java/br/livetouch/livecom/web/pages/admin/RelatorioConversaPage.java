package br.livetouch.livecom.web.pages.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.vo.RelatorioConversaFiltro;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
//import br.infra.web.click.ComboVersao;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * Logs das auditorias.
 * 
 */
@Controller
@Scope("prototype")
public class RelatorioConversaPage extends RelatorioPage {	

	public Form form = new Form();
	private TextField tId;
	private TextField tUsuario;
	private TextField tGrupo;
	public RelatorioConversaFiltro filtro;

	@Override
	public void onInit() {
		super.onInit();

		if(clear) {
			getContext().removeSessionAttribute(RelatorioConversaFiltro.SESSION_FILTRO_KEY);
		}

		form();
	}
	
	public void form() {
		
		tId = new TextField("id");
		tId.setAttribute("class", "form-control");
		tId.setId("id");
		form.add(tId);

		tUsuario = new TextField("usuario");
		tUsuario.setAttribute("class", "inputClean");
		tUsuario.setId("usuario");
		form.add(tUsuario);
		
		tGrupo = new TextField("grupo");
		tGrupo.setAttribute("class", "inputClean");
		form.add(tGrupo);
		
		Submit tfiltrar = new Submit("filtrar", getMessage("filtrar.label"), this, "filtrar");
		tfiltrar.setAttribute("class", "btn btn-primary min-width");
		form.add(tfiltrar);
		
	}

	@Override
	public void onGet() {
		super.onGet();

		filtro = (RelatorioConversaFiltro) getContext().getSessionAttribute(RelatorioConversaFiltro.SESSION_FILTRO_KEY);
		if (filtro != null) {
			form.copyFrom(filtro);
			filtro.setPage(page);
		}
	}
	
	@Override
	public String getContentType() {
		return super.getContentType();
	}
	
	@Override
	public String getTemplate() {
		return super.getTemplate();
	}

}
