package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Diretoria;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.Filtro;
import net.livetouch.tiger.ddd.DomainException;

public interface DiretoriaService extends Service<Diretoria> {

	List<Diretoria> findAll(Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Usuario userInfo, Diretoria f, Empresa empresa) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo, Diretoria f) throws DomainException;

	List<Diretoria> filterDiretoria(Filtro filtroDiretoria, Empresa empresa);

	List<Object[]> findIdCodDiretorias(Long idEmpresa);

	Diretoria findDefaultByEmpresa(Long empresaId);

	String csvDiretoria(Empresa empresa);

	List<Object[]> findIdNomeDiretorias(Long empresaId);

	List<Diretoria> findByNome(String nome, int page, int maxRows, Empresa empresa);

	Long countByNome(String nome, int page, int maxRows, Empresa empresa);

	Diretoria findByName(Diretoria diretoria, Empresa empresa);

	Diretoria findByCodigo(Diretoria diretoria, Empresa empresa);

}
