package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Diretoria;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.vo.Filtro;

@Repository
public interface DiretoriaRepository extends net.livetouch.tiger.ddd.repository.Repository<Diretoria> {

	List<Diretoria> filterDiretoria(Filtro filtroDiretoria, Empresa empresa);

	List<Object[]> findIdCodDiretorias(Long idEmpresa);

	List<Diretoria> findAll(Empresa empresa);

	Diretoria findDiretoriaDefaultByEmpresa(Long empresaId);
	Diretoria findDiretoriaExecutivaDefaultByEmpresa(Long empresaId);

	List<Object[]> findIdNomeDiretorias(Long empresaId);

	List<Diretoria> findByNome(String nome, int page, int maxRows, Empresa empresa);

	Long countByNome(String nome, int page, int maxRows, Empresa empresa);

	Diretoria findByName(Diretoria diretoria, Empresa empresa);

	Diretoria findByCodigo(Diretoria Diretoria, Empresa empresa);

}