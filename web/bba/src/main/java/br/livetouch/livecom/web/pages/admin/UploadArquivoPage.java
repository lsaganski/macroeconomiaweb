package br.livetouch.livecom.web.pages.admin;

import java.io.File;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;
import br.livetouch.livecom.web.pages.LivecomRootPage;
import net.sf.click.control.FileField;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

/**
 * 
 */
@Controller
@Scope("prototype")
public class UploadArquivoPage extends LivecomRootPage {
	public Form form = new Form();

	private FileField fileField;

	public String entrada;

	@Override
	public void onInit() {
		super.onInit();

		form.add(fileField = new FileField("file", true));

		entrada = ParametrosMap.getInstance(getEmpresaId()).get(Params.PASTAENTRADA);

		Submit tImportar = new Submit("upload", getMessage("upload.label"), this, "upload");
		tImportar.setAttribute("class", "botao info");
		form.add(tImportar);
	}

	public boolean upload() {
		if (form.isValid()) {
			FileItem fileItem = fileField.getFileItem();
			try {
				InputStream in = fileItem.getInputStream();
				if (in == null) {
					setMessageError(getMessage("msg.arquivo.invalido.error"));
					return false;
				}
				byte[] bytes = fileItem.get();
				if (StringUtils.isEmpty(entrada)) {
					setMessageError(getMessage("pastaDeEntrada.error"));
					return false;
				}
				File f = new File(StringUtils.trim(entrada) + "/" + StringUtils.trim(fileItem.getName()));
				Log.debug(getMessage("msg.importando.arquivo.info") + f);
				FileUtils.writeByteArrayToFile(f, bytes);
				JobInfo.getInstance(getEmpresaId()).temArquivo = true;
				return true;
			} catch (Exception e) {
				logError(e.getMessage(), e);
				return false;
			}
		}
		setMessageError(getMessage("msg.importacao.arquivo.salvar.error"));
		return false;
	}
}
