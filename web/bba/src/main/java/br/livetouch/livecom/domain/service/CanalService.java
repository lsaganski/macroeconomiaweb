package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Canal;
import net.livetouch.tiger.ddd.DomainException;

public interface CanalService extends Service<Canal> {

	List<Canal> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Canal f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Canal f) throws DomainException;

	String csvCanal();

}
