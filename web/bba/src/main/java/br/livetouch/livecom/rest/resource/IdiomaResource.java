package br.livetouch.livecom.rest.resource;

import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.CategoriaIdioma;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.vo.CategoriaIdiomaVO;
import br.livetouch.livecom.domain.vo.IdiomaVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/idioma")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class IdiomaResource extends MainResource {
	protected static final Logger log = Log.getLogger(IdiomaResource.class);
	
	@POST
	public Response create(Idioma idioma) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}

		Idioma.isFormValid(idioma, false);
		
		//Validar codigo
		Response messageResult = existe(idioma);
		if(messageResult != null){
			return messageResult;
		}
		
		idiomaService.saveOrUpdate(idioma);
		return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", new IdiomaVO(idioma))).build();
	}

	@POST
	@Path("/categoria")
	public Response createIdioma(CategoriaIdioma categoriaIdioma) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		if(categoriaIdioma == null) {
			return Response.ok(MessageResult.error("Erro ao cadastrar idioma para a categoria")).build();
		}
		
		CategoriaIdioma.isFormValid(categoriaIdioma, false);
		
		categoriaIdiomaService.saveOrUpdate(categoriaIdioma);
		return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", new CategoriaIdiomaVO(categoriaIdioma))).build();
	}
	
	@PUT
	public Response update(Idioma idioma) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Idioma.isFormValid(idioma, true);
		
		//Validar codigo
		Response messageResult = existe(idioma);
		if(messageResult != null){
			return messageResult;
		}
		
		idiomaService.saveOrUpdate(idioma);
		return Response.ok(MessageResult.ok("Idioma atualizado com sucesso", new IdiomaVO(idioma))).build();
	}
	
	@PUT
	@Path("/categoria")
	public Response updateIdioma(CategoriaIdioma categoriaIdioma) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		if(categoriaIdioma == null) {
			return Response.ok(MessageResult.error("Erro ao editar idioma para a categoria")).build();
		}
		
		CategoriaIdioma.isFormValid(categoriaIdioma, true);
		
		categoriaIdiomaService.saveOrUpdate(categoriaIdioma);
		return Response.ok(MessageResult.ok("Idioma atualizado com sucesso", new CategoriaIdiomaVO(categoriaIdioma))).build();
	}
	
	@GET
	public Response findAll() {
		
		List<Idioma> idiomas = idiomaService.findAll();
		List<IdiomaVO> vos = IdiomaVO.fromList(idiomas);
		
		br.livetouch.livecom.web.pages.ws.Response r = br.livetouch.livecom.web.pages.ws.Response.ok("OK");
		r.idiomas = vos;
		
		return Response.ok().entity(r).build();
	}

	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Idioma idioma = idiomaService.get(id);
		if(idioma == null) {
			return Response.ok(MessageResult.error("Idioma não encontrado")).build();
		}
		IdiomaVO vo = new IdiomaVO(idioma);
		return Response.ok().entity(vo).build();
	}
	
	@GET
	@Path("/categoria/{id}")
	public Response findByCategoria(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		CategoriaPost categ = categoriaPostService.get(id);
		if(categ == null) {
			return Response.ok(MessageResult.error("Categoria não encontrada")).build();
		}
		
		Set<CategoriaIdioma> idiomas = categoriaIdiomaService.findByCategoria(categ);
		if(idiomas == null) {
			return Response.ok(MessageResult.error("Idioma não encontrado")).build();
		}
		List<CategoriaIdiomaVO> vos = CategoriaIdiomaVO.fromList(idiomas);
		return Response.ok().entity(vos).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Idioma idioma = idiomaService.get(id);
			if(idioma == null) {
				return Response.ok(MessageResult.error("Idioma não encontrado")).build();
			}
			idiomaService.delete(idioma);
			return Response.ok().entity(MessageResult.ok("Idioma deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error(e.getMessage())).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Idioma  " + id)).build();
		}
	}
	
	public Response existe(Idioma idioma) {
		
		if(idiomaService.findByCodigo(idioma) != null) {
			log.debug("Ja existe este idioma");
			return Response.ok(MessageResult.error("Ja existe este idioma")).build();
		}

		return null;
	}
	
}
