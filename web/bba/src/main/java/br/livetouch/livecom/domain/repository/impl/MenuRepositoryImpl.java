package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Menu;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.repository.MenuRepository;

@Repository
public class MenuRepositoryImpl extends LivecomRepository<Menu> implements MenuRepository {

	public MenuRepositoryImpl() {
		super(Menu.class);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> findAll(Empresa empresa) {
		StringBuffer sb = new StringBuffer("SELECT distinct m FROM Menu m where 1=1");
		
		if(empresa != null) {
			sb.append(" and m.empresa = :empresa");
		}
		
		sb.append(" order by m.ordem");
		
		Query q = createQuery(sb.toString());
		
		if(empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		q.setCacheable(true);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> findAllDefault() {
		StringBuffer sb = new StringBuffer("SELECT distinct m FROM Menu m where 1=1");

		sb.append(" and m.empresa is null");

		sb.append(" order by m.parent, m.ordem");

		Query q = createQuery(sb.toString());

		q.setCacheable(true);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> findByLabel(String label, Empresa e) {
		StringBuffer sb = new StringBuffer("SELECT distinct m FROM Menu m where 1=1 ");
		
		if(StringUtils.isNotEmpty(label)) {
			sb.append(" and m.label like :label ");
		}
		
		if(e != null) {
			sb.append(" and m.empresa = :empresa");
		}
		 
		Query q = createQuery(sb.toString());
		
		if(StringUtils.isNotEmpty(label)) {
			q.setParameter("label", "%" + label + "%");
		}
		
		if(e != null) {
			q.setParameter("empresa", e);
		}
		
		q.setCacheable(true);
		
		List<Menu> list = q.list();
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> findPaiByLabel(String label, Empresa e) {
		StringBuffer sb = new StringBuffer("SELECT distinct m FROM Menu m where 1=1 ");
		
		if(StringUtils.isNotEmpty(label)) {
			sb.append(" and m.label like :label");
		}
		
		if(e != null) {
			sb.append(" and m.empresa = :empresa");
		}
		 
		sb.append(" and m.parent is null");
		
		Query q = createQuery(sb.toString());
		
		if(StringUtils.isNotEmpty(label)) {
			q.setParameter("label", "%" + label + "%");
		}
		
		if(e != null) {
			q.setParameter("empresa", e);
		}
		
		q.setCacheable(true);
		
		List<Menu> list = q.list();
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> findByPerfil(Perfil p, Empresa e) {
		StringBuffer sb = new StringBuffer("SELECT distinct mp.menu FROM MenuPerfil mp WHERE 1=1 ");
		
		if(p != null) {
			sb.append(" and mp.perfil = :perfil) ");
		}
		
		if(e != null) {
			sb.append(" and mp.menu.empresa = :empresa");
		}
		 
		sb.append(" order by m.ordem ");
		
		Query q = createQuery(sb.toString());
		
		if(p != null) {
			q.setParameter("perfil", p);
		}
		
		if(e != null) {
			q.setParameter("empresa", e);
		}
		
		List<Menu> list = q.list();
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> findMenusNotInPerfil(Perfil p, Empresa e) {
		StringBuffer sb = new StringBuffer("SELECT distinct m FROM Menu m WHERE 1=1 ");
		
		if(p != null) {
			sb.append(" and m.id not in (select mp.menu.id from MenuPerfil mp where mp.perfil = :perfil) ");
		}
		
		if(e != null) {
			sb.append(" and m.empresa = :empresa");
		}
		 
		sb.append(" order by m.ordem ");
		
		Query q = createQuery(sb.toString());
		
		if(p != null) {
			q.setParameter("perfil", p);
		}
		
		if(e != null) {
			q.setParameter("empresa", e);
		}
		
		List<Menu> list = q.list();
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> findPaiByPerfil(Perfil p, Empresa e, boolean in) {
		StringBuffer sb = new StringBuffer("SELECT distinct m FROM Menu m WHERE 1=1 ");
		
		if(p != null) {
			if(in) {
				sb.append(" and m.id ");
				sb.append(" in ");
				sb.append(" (select distinct mp.menu.id from MenuPerfil mp where mp.perfil = :perfil)");
			} 
		}
		
		if(e != null) {
			sb.append(" and m.empresa = :empresa");
		}
		
		sb.append(" and m.parent is null");
		sb.append(" and (m.mobile is null or m.mobile = 0) ");
		 
		sb.append(" order by m.ordem ");
		
		Query q = createQuery(sb.toString());
		
		if(p != null) {
			if(in) {
				q.setParameter("perfil", p);
			}
		}
		
		if(e != null) {
			q.setParameter("empresa", e);
		}
		
		List<Menu> list = q.list();
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> findFilhoByPerfil(Perfil p, Empresa e, boolean in) {
		StringBuffer sb = new StringBuffer("SELECT distinct m FROM Menu m WHERE 1=1 ");
		
		if(p != null) {
			sb.append(" and m.id ");
			if(in) {
				sb.append(" in ");
			} else {
				sb.append(" not in ");
			}
			sb.append(" (select distinct mp.menu.id from MenuPerfil mp where mp.perfil = :perfil) ");
		}
		
		if(e != null) {
			sb.append(" and m.empresa = :empresa");
		}
		
		sb.append(" and m.parent is not null");
		sb.append(" and (m.mobile is null or m.mobile = 0) ");
		 
		sb.append(" order by m.ordem ");
		
		Query q = createQuery(sb.toString());
		
		if(p != null) {
			q.setParameter("perfil", p);
		}
		
		if(e != null) {
			q.setParameter("empresa", e);
		}
		
		List<Menu> list = q.list();
		return list;
		
	}
	
	@Override
	public void delete(Menu m) {
		//Remover Menus associados a Perfil
		execute("delete from MenuPerfil mp where mp.menu.id = ?)", m.getId());
		execute("delete from Menu m where m.id = ?)", m.getId());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> findFilhos(Menu m) {
		StringBuffer sb = new StringBuffer("SELECT distinct m FROM Menu m WHERE 1=1 ");
		
		if(m != null) {
			sb.append(" and m.parent = :menu ");
		}
		
		Query q = createQuery(sb.toString());
		
		if(m != null) {
			q.setParameter("menu", m);
		}
		
		List<Menu> list = q.list();
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> paisByPerfil(Perfil p, Empresa e) {
		StringBuffer sb = new StringBuffer("SELECT distinct mp.menu FROM MenuPerfil mp WHERE 1=1 ");
		
		if(p != null) {
			sb.append(" and mp.perfil = :perfil ");
		}
		
		if(e != null) {
			sb.append(" and mp.menu.empresa = :empresa");
		}
		 
		sb.append(" and mp.menu.parent is null ");
		sb.append(" order by mp.ordem ");
		
		Query q = createQuery(sb.toString());
		
		if(p != null) {
			q.setParameter("perfil", p);
		}
		
		if(e != null) {
			q.setParameter("empresa", e);
		}
		
		List<Menu> list = q.list();
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> filhosByPerfil(Perfil p, Empresa e) {
		StringBuffer sb = new StringBuffer("SELECT distinct mp.menu FROM MenuPerfil mp WHERE 1=1 ");
		
		if(p != null) {
			sb.append(" and mp.perfil = :perfil ");
		}
		
		if(e != null) {
			sb.append(" and mp.menu.empresa = :empresa");
		}
		 
		sb.append(" and mp.menu.parent is not null ");
		sb.append(" order by mp.ordem ");
		
		Query q = createQuery(sb.toString());
		
		if(p != null) {
			q.setParameter("perfil", p);
		}
		
		if(e != null) {
			q.setParameter("empresa", e);
		}
		
		List<Menu> list = q.list();
		return list;
		
	}

	@Override
	public void deleteFromEmpresa(Empresa empresa) {
		execute("delete from Menu m where m.parent is not null and m.empresa = ?)", empresa);
		execute("delete from Menu m where m.empresa = ?)", empresa);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> findAllMobile(Empresa empresa) {
		Query q = createQuery("FROM Menu m where m.mobile = 1 and m.empresa.id = :empresaId order by m.ordem asc");
		q.setParameter("empresaId", empresa.getId());
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> findMenuMobileDefault() {
		Query q = createQuery("FROM Menu m where m.mobile = 1 AND m.empresa is null");
		return q.list();
	}

	@Override
	public void delete(Empresa empresa) {
		Query q = createQuery("DELETE FROM Menu m where m.mobile = 1 AND m.empresa = :empresa");
		q.setParameter("empresa", empresa);
		q.executeUpdate();
	}

    @Override
    public void removeDefaultMenuMobile(Empresa empresa) {
        Query q = createQuery("UPDATE Menu m Set m.menuDefault = 0 WHERE m.mobile = 1 and m.empresa = :empresa");
        q.setParameter("empresa", empresa);
        q.executeUpdate();
    }
}
