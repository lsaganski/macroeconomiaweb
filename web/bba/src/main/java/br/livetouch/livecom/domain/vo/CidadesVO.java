package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.List;

import br.livetouch.livecom.domain.Cidade;

public class CidadesVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public List<Cidade> cidades;
	public Long total;
	
}