package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LinkConhecido;
import br.livetouch.livecom.domain.repository.LinkConhecidoRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
@SuppressWarnings("unchecked")
public class LinkConhecidoRepositoryImpl extends StringHibernateRepository<LinkConhecido> implements LinkConhecidoRepository {

	public LinkConhecidoRepositoryImpl() {
		super(LinkConhecido.class);
	}
	
	@Override
	public List<LinkConhecido> findAll() {
		Query q = createQuery("from LinkConhecido");
		q.setCacheable(true);
		return q.list();
	}

	@Override
	public LinkConhecido findByName(LinkConhecido link) {
		StringBuffer sb = new StringBuffer("from LinkConhecido l where l.nome = :nome");
		
		if(link.getId() != null) {
			sb.append(" and l.id != :id");
		}
		
		Query q = createQuery(sb.toString());

		q.setParameter("nome", link.getNome());
		
		if(link.getId() != null) {
			q.setParameter("id", link.getId());
		}
		
		q.setCacheable(true);
		
		List<LinkConhecido> list = q.list();
		LinkConhecido d = (LinkConhecido) (list.size() > 0 ? q.list().get(0) : null);
		return d;
	}

	@Override
	public LinkConhecido findByDominio(LinkConhecido link) {
		StringBuffer sb = new StringBuffer("from LinkConhecido l where l.dominio = :dominio");
		
		if(link.getId() != null) {
			sb.append(" and l.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("dominio", link.getDominio());
		
		if(link.getId() != null) {
			q.setParameter("id", link.getId());
		}
		
		q.setCacheable(true);
		
		List<LinkConhecido> list = q.list();
		LinkConhecido d = (LinkConhecido) (list.size() > 0 ? q.list().get(0) : null);
		return d;
	}

	@Override
	public void save(LinkConhecido link, Empresa empresa) {
		
		
		
	}

}
