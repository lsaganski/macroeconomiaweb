package br.livetouch.livecom.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.domain.enums.StatusParticipacao;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name="grupo_usuarios")
public class GrupoUsuarios extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = -547576753741012371L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "GRUPOUSUARIOS_SEQ")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Usuario usuario;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "grupo_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Grupo grupo;
	
	private boolean postar;

	private boolean favorito;
	private Boolean push;
	private Boolean mostraMural;

	private Boolean admin;

	@Enumerated(EnumType.STRING)
	private StatusParticipacao statusParticipacao;
	
	@Override
	public Long getId() {
		return id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isPostar() {
		return postar;
	}
	
	public void setPostar(boolean postar) {
		this.postar = postar;
	}

	@Override
	public String toString() {
		return "GrupoUsuarios [id=" + id + ", usuario=" + usuario + ", grupo=" + grupo + ", postar=" + postar + ", favorito=" + favorito + "]";
	}

	public boolean isFavorito() {
		return favorito;
	}

	public void setFavorito(boolean favorito) {
		this.favorito = favorito;
	}

	public StatusParticipacao getStatusParticipacao() {
		return statusParticipacao;
	}

	public void setStatusParticipacao(StatusParticipacao statusPaticipacao) {
		this.statusParticipacao = statusPaticipacao;
	}
	
	public String getStatusParticipacaoString() {
		return statusParticipacao != null ? statusParticipacao.name() : "";
	}

	public boolean isPush() {
		return push != null ? push : false;
	}

	public void setPush(boolean push) {
		this.push = push;
	}

	public boolean isMostraMural() {
		return mostraMural != null ? mostraMural : false;
	}

	public void setMostraMural(boolean mostraMural) {
		this.mostraMural = mostraMural;
	}

	public boolean isAdmin() {
		return admin != null ? admin : false;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
}
