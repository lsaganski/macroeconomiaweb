package br.livetouch.livecom.domain.service;

import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.Card;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Location;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.RelatorioConversaFiltro;
import br.livetouch.livecom.domain.vo.StatusMensagemUsuarioVO;
import br.livetouch.livecom.domain.vo.UpdateStatusMensagemResponseVO;
import net.livetouch.tiger.ddd.DomainException;

public interface MensagemService extends Service<Mensagem> {

	List<Mensagem> findAll();
	List<MensagemConversa> findAllConversas();
	
	List<Mensagem> findAllByUser(Usuario userFrom, Usuario userTo, int tipo, int page, int maxRows);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Mensagem c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(MensagemConversa c) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void delete(Mensagem c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void deleteMensagens(List<Long> msgsIds) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void deleteConversas(List<Long> msgsIds) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void delete(MensagemConversa c) throws DomainException;

	@Transactional(rollbackFor = Exception.class)
	UpdateStatusMensagemResponseVO updateStatusMensagemLida(Usuario u, long msgId, int status) throws DomainMessageException;

	@Transactional(rollbackFor = Exception.class)
	int markConversationAsRead(Long userId, Long conversaId) throws DomainException;
	
	boolean isTodasMensagensLidas(Long conversaId, Long userId, boolean isValidateAllGroup);
	
	MensagemConversa getMensagemConversa(Long id);

	public List<MensagemConversa> findAllConversasByUser(Usuario user, String search,int page, int max);

	MensagemConversa getMensagemConversaByUser(Usuario userFrom,Usuario userTo) throws DomainException;
	MensagemConversa getMensagemConversaByUser(Usuario userFrom,Usuario userTo, boolean create) throws DomainException ;
	MensagemConversa getMensagemConversaByUserGrupo(Usuario userFrom,Grupo grupo, boolean create) throws DomainException;
	
	List<Mensagem> findMensagensNaoLidas(MensagemConversa conversa);

	List<Mensagem> findMensagensCallback(MensagemConversa c, Usuario user, Long lastReceivedMessageID);
	
	
	List<Mensagem> getMensagensByConversa(MensagemConversa conversa, Usuario user,int page, int maxRows, boolean orderAsc);
	
	Mensagem getMensagem(MensagemConversa conversa, Long id, Usuario userFrom, Usuario userTo, String msg);
	MensagemConversa getMensagemConversa(Long id, Usuario userFrom, Usuario userTo, String msg) throws DomainException;
	
	List<Long> findUsuariosComQuemTemConversa(long userId);
	
	@Transactional(rollbackFor = Exception.class)
	Mensagem saveMensagem(Long msgId, Long conversaId, Long userFromLogin, Long userToLogin, String msg, List<Long> arquivoIds, Location location, Long identifier, Set<Card> cards) throws DomainException;
	
	@Transactional(rollbackFor = Exception.class)
	Mensagem saveMensagemGrupo(Long msgId, Long conversaId, Long grupoId, Long userFromLogin, String msg, List<Long> arquivoIds, Location location, Long identifier, Set<Card> cards) throws DomainException;
	
	List<ConversaVO> findConversasByFilter(RelatorioConversaFiltro filtro) throws DomainMessageException, DomainException;
	
	long getCountConversasByFilter(RelatorioConversaFiltro filtro) throws DomainException;
	
	List<MensagemVO> findDetalhesByConversa(RelatorioConversaFiltro filtro);
	List<StatusMensagemUsuarioVO> findDetalhesByMensagem(Long id);
	long getCountDetalhesByConversa(RelatorioConversaFiltro filtro);
	
	void checkHibernateProxy(Mensagem mensagem);
	
	List<Usuario> findUsersFromConversation(Long conversaId, Long userId, boolean includeUser);

	MensagemConversa findByGrupo(Grupo g);
	
	Boolean isUltimaMensagem(Long mensagemId);
	Long countMensagensNaoLidas(Usuario u);

	Long countMensagensNaoLidas();

	List<Long> usersToSilent(Empresa empresa);
	
	List<Long> findAllIdsConversasByUser(Usuario u);
	List<MensagemConversa> findAllConversasByUser(long userId);
	
	List<Long> findAllMensagensIdsOwnerByUser(Usuario u);
	
	@Transactional(rollbackFor = Exception.class)
	void updateDataPushMsg(Long msgId);
	
	@Transactional(rollbackFor = Exception.class)
	void updateCallbackMensagem(long conversaId,Long msgId,boolean callback1cinza, boolean callback2cinza, boolean callback2azul);
	
	Mensagem findByIdentifier(Long conversaId, Long idUserFrom, Long identifier);
	Long getBadge(MensagemConversa c, Usuario user);
	
	List<Mensagem> findMensagensMobile(Usuario user, Long lastMsgId);
	List<Mensagem> findMensagensById(List<Long> msgIds);
	List<Long> findIdsConversasLidasById(List<Long> ids, Long userId);
	
	@Transactional(rollbackFor = Exception.class)
	void forward(Mensagem msg, MensagemConversa c);
	
	boolean isTodasEntregue(Long conversaId, Long userId);
	@Transactional(rollbackFor = Exception.class)
	boolean markMessagesWasReceived(MensagemConversa conversa, Usuario from) throws DomainMessageException;
	
	
}
