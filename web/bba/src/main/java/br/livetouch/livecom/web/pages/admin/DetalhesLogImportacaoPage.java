package br.livetouch.livecom.web.pages.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.LogImportacao;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.vo.LogImportacaoFiltro;
import br.livetouch.livecom.domain.vo.LogImportacaoVO;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

@Controller
@Scope("prototype")
public class DetalhesLogImportacaoPage extends RelatorioPage {

	public Long id;
	public LogImportacaoVO logVO;
	public Form form = new Form();
	public LogImportacaoFiltro filtro;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.IMPORTACAO)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;

		LogImportacao log = null;
		if (id != null) {
			log = new LogImportacao();
			log = logImportacaoService.get(id);
			logVO = new LogImportacaoVO(log);
			filtro = (LogImportacaoFiltro) getContext().getSessionAttribute(LogImportacaoFiltro.SESSION_FILTRO_KEY);
			if(filtro != null) {
				filtro.setId(id);
			}
		}
//		if (log == null) {
//			setRedirect(LogImportacaoPage.class);
//			return;
//		}
		form();
	}

	@Override
	public void onRender() {
		super.onRender();
//		LinhaImportacaoVO findAllVOByLogId = linhaImportacaoService.findAllVOByLogId(id);
//		addModel("tabela", findAllVOByLogId);
	}
	public void form() {
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip btn btn-outline blue");
		form.add(tExportar);
	}
	public boolean exportar(){
		filtro = (LogImportacaoFiltro) getContext().getSessionAttribute(LogImportacaoFiltro.SESSION_FILTRO_KEY);
		String csv = logImportacaoService.csvLogImportacao(filtro.getId());
		LogImportacao logImportacao = logImportacaoService.get(filtro.getId());
		download(csv, logImportacao.getArquivoNome());
		return true;
	}

}