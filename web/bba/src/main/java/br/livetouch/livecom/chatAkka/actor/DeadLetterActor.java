package br.livetouch.livecom.chatAkka.actor;

import org.apache.log4j.Logger;

import akka.actor.ActorRef;
import akka.actor.DeadLetter;
import akka.actor.UntypedActor;
import br.infra.util.Log;

public class DeadLetterActor extends UntypedActor {
	private static Logger log = Log.getLogger("DeadLetterActor");

	public void onReceive(Object message) {
		if (message instanceof DeadLetter) {
			DeadLetter d = (DeadLetter) message;
			//DeadLetter(br.livetouch.livecom.chatAkka.actor.UserActor$Stop@3c106eaa,Actor[akka://ChatLivecom/user/mainActor/userActor_2524#-1722289530],Actor[akka://ChatLivecom/user/mainActor/tcpActor/$a#-137330209])
			log.debug(d);
			ActorRef sender = d.sender();
			log.debug(sender);
			ActorRef actor = d.recipient();
			log.debug(actor);
			
			Object msg = d.message();
			log.debug(msg);
		}
	}
}