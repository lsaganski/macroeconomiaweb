package br.livetouch.livecom.web.pages.pages;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Favorito;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.vo.PostVO;

/**
 * 
 */
@Controller
@Scope("prototype")
public class FavoritosPage extends PostComunicadoPage {
	public List<Favorito> favoritos;
	public List<PostVO> posts;

	@Override
	public void onRender() {
		super.onRender();

		favoritos = favoritoService.findAllByUser(getUsuario());

		List<Post> list = new ArrayList<Post>();
		for (Favorito f : favoritos) {
			Post p = f.getPost();
			list.add(p);
		}
		
		posts = postService.toListVo(getUsuario(), list);
		
		formAberto = false;
		
	}
}
