package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.List;

public class ImportarArquivoResponse {

	public List<Object> list = new ArrayList<Object>();
	public String nomeArquivo;
	public Long idArquivo;
	public int countOk;
	public int countInsert;
	public int countUpdate;
	public int countError;
	public int countDeleteOk;
	public int countDeleteError;

	@Override
	public String toString() {
		return "ImportarArquivoResponse [list=" + (list != null ? list.size() : "null") + ", countOk=" + countOk + ", countError=" + countError + ", countDeleteOk=" + countDeleteOk + ", countDeleteError=" + countDeleteError + "]";
	}

}
