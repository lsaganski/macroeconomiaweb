package br.livetouch.livecom.jobs.importacao;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ImportarArquivoFactory {
	
	protected static Logger log = Logger.getLogger(ImportarArquivoFactory.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private ImportarArquivoItauRevistaWork revistaWork;
	
	@Autowired
	private ImportarArquivoUsuarioDefaultWork importarArquivoUsuarioDefaultWork;
	
	@Autowired
	private ImportarArquivoGrupoDefaultWork importarArquivoGrupoDefaultWork;
	
	@Autowired
	private ImportarArquivoDiretoriaWork importarArquivoDiretoriaWork;
	
	@Autowired
	private ImportarArquivoAreaWork importarArquivoAreaWork;
	
	@Autowired
	private ImportarArquivoOperacoesCieloWork importarArquivoTransacoesWork;
	
	@Autowired
	private ImportarArquivoCargoWork importarArquivoCargoWork;

	Dialect dialect;
	
	public ImportacaoArquivoWork getImportacaoWork(Session session, File file) throws DomainException, IOException {

		this.dialect = ((SessionFactoryImplementor) sessionFactory).getDialect();
		
		boolean isGrupo = JobImportacaoUtil.isFileGrupo(file);
		boolean isUsuario = JobImportacaoUtil.isFileUsuario(file);
		boolean isDiretoria = JobImportacaoUtil.isFileDiretoria(file);
		boolean isArea = JobImportacaoUtil.isFileArea(file);
		boolean isTransacao = JobImportacaoUtil.isFileTransacao(file);
		boolean isCargo = JobImportacaoUtil.isFileCargo(file);
//		boolean isConvite = JobImportacaoUtil.isFileConvite(file);
		
		if(isGrupo) {
			return getGrupoWork(session,file);
		} else if(isUsuario){
			return getUsuarioWork(session,file);
		}else if(isDiretoria){
			return getDiretoriaWork(session,file);
		}else if(isArea){
			return getAreaWork(session,file);
		} else if(isTransacao){
			return getTransacaoWork(session,file);
		} else if(isCargo){
			return getCargosWork(session,file);
		} else {
			log.error("Arquivo ["+file+"] não reconhecido");
			return null;
		}
	}

	private ImportacaoArquivoWork getUsuarioWork(Session session, File file) throws DomainException, IOException {
		ImportacaoArquivoWork work = null;
		
		boolean itau = ParametrosMap.getInstance(JobImportacaoUtil.getIdEmpresa(file)).getBoolean(Params.IMPORTACAO_USUARIO_ITAU);

		if(itau) {
			log.debug("Retornando");
			revistaWork.init(session, file, dialect);
			work = revistaWork;
		} else {
			importarArquivoUsuarioDefaultWork.init(session, file, dialect);
			work = importarArquivoUsuarioDefaultWork;
		}
		
		return work;
	}

	private ImportacaoArquivoWork getGrupoWork(Session session, File file) throws DomainException, IOException  {
		importarArquivoGrupoDefaultWork.init(session, file, dialect);
		return importarArquivoGrupoDefaultWork;
	}
	
	private ImportarArquivoDiretoriaWork getDiretoriaWork(Session session, File file) throws DomainException, IOException  {
		importarArquivoDiretoriaWork.init(session, file, dialect);
		return importarArquivoDiretoriaWork;
	}
	
	private ImportarArquivoAreaWork getAreaWork(Session session, File file) throws DomainException, IOException  {
		importarArquivoAreaWork.init(session, file, dialect);
		return importarArquivoAreaWork;
	}
	
	private ImportarArquivoOperacoesCieloWork getTransacaoWork(Session session, File file) throws DomainException, IOException  {
		importarArquivoTransacoesWork.init(session, file, dialect);
		return importarArquivoTransacoesWork;
	}
	
	private ImportarArquivoCargoWork getCargosWork(Session session, File file) throws DomainException, IOException  {
		importarArquivoCargoWork.init(session, file, dialect);
		return importarArquivoCargoWork;
	}

}
