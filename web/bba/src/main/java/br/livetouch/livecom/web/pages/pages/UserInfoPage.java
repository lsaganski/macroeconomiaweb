package br.livetouch.livecom.web.pages.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;



/**
 * Alterar senha do usuario logado
 * 
 * @author ricardo
 *
 */
@Controller
@Scope("prototype")
public class UserInfoPage extends LivecomLogadoPage {
	public Long id;
	public Usuario usuario;
	
	@Override
	public void onGet() {
		super.onGet();
		
		if(id != null) {
			usuario = usuarioService.get(id);
		}
	}
}
