package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Historico;

public class HistoricoVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public long userId;
	public String urlFotoUsuario;
	public String userNome;
	public String userLogin;
	public long postId;
	public String postTitulo;
	public Long comentarioId;

	public void setHistorico(Historico f) {
		this.id = f.getId();
		this.userId 	= f.getUsuario().getId();
		this.userNome 	= f.getUsuario().getNome();
		this.userLogin 	= f.getUsuario().getLogin();
		this.urlFotoUsuario = f.getUsuario().getUrlFoto();
		if(f.getPost() != null) {
			this.postId 	= f.getPost().getId();
			this.postTitulo	= f.getPost().getTitulo();
		}
	}

	public static List<HistoricoVO> toListVO(List<Historico> list) {
		List<HistoricoVO> vos = new ArrayList<HistoricoVO>();
		for (Historico h : list) {
			HistoricoVO vo = new HistoricoVO();
			vo.setHistorico(h);
			vos.add(vo);
		}
		return vos;
	}


	@Override
	public String toString() {
		return "HistoricoVO [userId=" + userId + ", postId=" + postId + ", titulo=" + postTitulo + "]";
	}
}
