package br.livetouch.livecom.web.pages.admin;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.enums.TipoLogSistema;
import br.livetouch.livecom.domain.repository.LogRepository;
import br.livetouch.livecom.domain.vo.LogSistemaFiltro;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.extras.util.ServletUtil;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.DateField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LinkDecorator;

/**
 * Logs de convite de e-mail
 * 
 */
@Controller
@Scope("prototype")
public class LogSistemaTipoConvitePage extends RelatorioPage {	

	public PaginacaoTable table = new PaginacaoTable();
	public ActionLink detalhes = new ActionLink("detalhes", getMessage("detalhes.label"), this, "detalhes");
	public Form form = new Form();
	public int page;
	private LogSistemaFiltro filtro;
	
	public int qtdeResultados;

	@Override
	public void onInit() {
		super.onInit();

		if(clear) {
			getContext().removeSessionAttribute(LogSistema.SESSION_FILTRO_KEY);
		}

		table();

		form();
	}

	protected void table() {
		
		table.setClass("simple logSistemaTipoConvite");
		Column c = new Column("id",getMessage("coluna.id.label"));
		c.setAttribute("align",  "center");
		table.addColumn(c);

		c = new Column("dataString",getMessage("coluna.data.label"));
		c.setTextAlign("center");
		c.setAttribute("align",  "center");		
		table.addColumn(c);
		
		c = new Column("usuario.login",getMessage("coluna.login.label"));
		c.setAttribute("align",  "left");		
		table.addColumn(c);

		c = new Column("msg",getMessage("coluna.mensagem.label"));
		c.setAttribute("nowrap", "true");
		// limita o maximo de caracteres para 100 , e ainda coloca as reticiencias ...
		c.setMaxLength(100);
		c.setAttribute("align",  "left");		
		table.addColumn(c);
		
		c = new Column("detalhes",getMessage("coluna.detalhes.label"));
		c.setAttribute("align",  "center");		
		c.setDecorator(new LinkDecorator(table, detalhes, "id"));
		table.addColumn(c);
	}

	public void form() {

		DateField tDataInicio = new DateField("dataInicial", getMessage("periodoDe.label"), false);
		DateField tDataFim = new DateField("dataFinal", getMessage("ate.label"));		
		tDataInicio.setAttribute("class", "input data datepicker");
		tDataFim.setAttribute("class", "input data datepicker");
		tDataInicio.setFormatPattern("dd/MM/yyyy");
		tDataFim.setFormatPattern("dd/MM/yyyy");

		tDataInicio.setValue(DateUtils.toString(new Date(),"dd/MM/yyyy"));
		tDataFim.setValue(DateUtils.toString(new Date(),"dd/MM/yyyy"));
		
		tDataInicio.setMaxLength(10);
		tDataFim.setMaxLength(10);

		form.setColumns(1);
		
		TextField tLogin = new TextField("usuario.login", getMessage("coluna.login.label"), false);
		tLogin.setFocus(true);
		
		form.add(tDataInicio);
		form.add(tDataFim);
		
		IntegerField tMaxResults = new IntegerField("qtdeResultados", getMessage("log.convite.qtdeResultados.label"));
		tMaxResults.setAttribute("class", "input");
		tMaxResults.setInteger(getMaxResults());
		form.add(tMaxResults);
		
		Submit tfiltrar = new Submit("filtrar", getMessage("filtrar.label"), this, "filtrar");
		tfiltrar.setAttribute("class", "botao salvar");
		form.add(tfiltrar);
		
		Submit texportar = new Submit("exportar", getMessage("exportar.csv.label"), this, "exportar");
		texportar.setAttribute("class", "botao export");
		form.add(texportar);
	}

	@Override
	public void onGet() {
		super.onGet();
		filtro = (LogSistemaFiltro) getContext().getSessionAttribute(LogSistema.SESSION_FILTRO_KEY);
		if(filtro != null) {
			form.copyFrom(filtro);
			form.copyFrom(filtro.getLogSistema());
		}
	}

	public boolean filtrar() {
		filtro = (LogSistemaFiltro) getContext().getSessionAttribute(LogSistema.SESSION_FILTRO_KEY);
		if(filtro == null) {
			LogSistema l = new LogSistema();
			filtro = new LogSistemaFiltro(l);

			getContext().setSessionAttribute(LogSistema.SESSION_FILTRO_KEY, filtro);
		}

		LogSistema l = filtro.getLogSistema();
		form.copyTo(l);
		form.copyTo(filtro);
		filtro.getLogSistema().setTipo(TipoLogSistema.Convite);
		return true;
	}
	
	public boolean detalhes() {
		Long id = detalhes.getValueLong();
		setRedirect(DetalhesLogSistemaPage.class,"id",id.toString());
		return false;
	}

	public boolean exportar() {
		LogSistema log = new LogSistema();
		form.copyTo(log);

		LogSistemaFiltro filtro = new LogSistemaFiltro(log);
		form.copyTo(filtro);
		filtro.getLogSistema().setTipo(TipoLogSistema.Convite);

		String csv;
		try {
			csv = logService.exportarLogsSistema(filtro,getMaxResults(),getEmpresa());
			try {
//				log.info("Encode padrao: " + getContext().getRequest().getCharacterEncoding());
				ServletUtil.exportBytesDownload(getContext().getResponse(), csv.getBytes(Utils.CHARSET), "log.cvs");
			} catch (IOException e) {
				logError(e.getMessage(),e);
			}
		} catch (DomainException e1) {
			form.setError(e1.getMessage());
		}

		setPath(null);

		return false;
	}

	@Override
	public void onRender() {
		super.onRender();
		try {
			if(filtro != null) {
				
				// Count(*)
				long count = logService.getCountByFilter(filtro, getEmpresa());
				table.setCount(count);

				List<LogSistema> logs = logService.findbyFilter(filtro, page,getMaxResults(), getEmpresa());

				int max = LogRepository.MAX;
				table.setPageSize(max);

				table.setRowList(logs);
			}
		} catch (DomainException e) {
			form.setError(e.getMessage());
			return;
		}
	}
	
	private int getMaxResults() {
		return qtdeResultados == 0 ? 50 : qtdeResultados;
	}
}
