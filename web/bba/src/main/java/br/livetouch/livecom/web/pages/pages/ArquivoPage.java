package br.livetouch.livecom.web.pages.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;

/**
 * Page mae para usar o Autowired do Spring
 * 
 * @author ricardo
 * 
 */
@Controller
@Scope("prototype")
public class ArquivoPage extends LivecomLogadoPage {
	
	public Long id;
	public Arquivo arquivo;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.ARQUIVOS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
		
		if(id != null) {
			arquivo = arquivoService.get(id);
		}
	}

	@Override
	public void onRender() {
		super.onRender();
	}

}
