package br.livetouch.livecom.jobs.importacao;

import java.io.File;
import java.io.IOException;

import org.hibernate.Session;
import org.hibernate.dialect.Dialect;
import org.hibernate.jdbc.Work;

import br.livetouch.livecom.domain.ImportarArquivoResponse;
import br.livetouch.livecom.utils.BaseImportacaoArquivoWork;
import net.livetouch.tiger.ddd.DomainException;

/**
 * Work base para importar usuarios
 * 
 * @author rlech
 *
 */
public abstract class ImportacaoArquivoWork extends BaseImportacaoArquivoWork implements Work {

	public abstract void init(Session session, File file, Dialect dialect) throws DomainException, IOException;

	public abstract ImportarArquivoResponse getResponse();
}
