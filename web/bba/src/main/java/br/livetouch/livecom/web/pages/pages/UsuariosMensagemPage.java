package br.livetouch.livecom.web.pages.pages;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboGrupo;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.UsuarioAutocompleteFiltro;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import net.livetouch.click.control.link.Link;
import net.sf.click.control.Form;
import net.sf.click.control.HiddenField;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class UsuariosMensagemPage extends LivecomLogadoPage {

	@Autowired
	public UsuarioService usuarioService;

	public Form form = new Form();
	public String msgErro;
	public int page;
	
	public List<Usuario> usuarios;
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	
	protected Usuario filtro;

	@Override
	public void onInit() {
		super.onInit();
		
		form();
	}

	public void form(){
		
		form.add(new HiddenField("ids",String.class));
		
		TextField tNome = new TextField("nome", true);
		tNome.setAttribute("class", "mascara");
		tNome.setAttribute("autocomplete", "off");
		tNome.setAttribute("rel", "Buscar Usuário");
		tNome.setFocus(false);
		form.add(tNome);

		ComboGrupo tGrupo = new ComboGrupo(grupoService,getUsuario());
		
		tGrupo.setAttribute("width", "100%");
		form.add(tGrupo);
		
		Submit tconsultar = new Submit("consultar",this,"consultar");
		tconsultar.setAttribute("class", "botaoFundo");
		form.add(tconsultar);
		
//		setFormTextWidth(form);
	}
	
	public boolean filtrar() {
		if(form.isValid()) {
			filtro = (Usuario) getContext().getSessionAttribute(Usuario.SESSION_FILTRO_KEY);
			if(filtro == null) {
				filtro = new Usuario();
				getContext().setSessionAttribute(Usuario.SESSION_FILTRO_KEY, filtro);
			}
			form.copyTo(filtro);
			return true;
		
		}
		return false;
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean consultar() {
		if (form.isValid()) {
			try {
				
				return true;

			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return false;
	}

	@Override
	public void onRender() {
		super.onRender();

		try {
			
			usuarios = getUsuarios();

		} catch (Exception e) {
			logError(e.getMessage(), e);
			setFlashAttribute("msg", "Erro: " + e.getMessage());
			return;
		}
	}

	public List<Usuario> getUsuarios() {
		int maxSize = 10;

		List<Usuario> usuarios = null;

		if(usuarios == null) {
			UsuarioAutocompleteFiltro filtro = new UsuarioAutocompleteFiltro();
			filtro.setPage(page);
			filtro.setMaxRows(maxSize);
			usuarios = usuarioService.findByFiltro(filtro, getEmpresa());
		}

		return usuarios;
	}
}
