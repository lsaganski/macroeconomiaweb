package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.impl.ArquivoRepositoryImpl.TipoBuscaArquivo;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.ThumbVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ArquivosPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tUserId;
	private TextField tTexto;
	private TextField tMode;
	private TextField tModoBusca;
	private TextField tTipo;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tUserId = new TextField("user_id", true));
		form.add(tTexto = new TextField("texto"));
		
		form.add(new TextField("maxRows"));
		form.add(new TextField("page"));
		form.add(tTipo = new TextField("tipo"));
		
		
		form.add(tModoBusca = new TextField("modoBusca", "Modo busca (todos ou meusArquivos)	", true) );
		tModoBusca.setValue("todos");
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		tUserId.setFocus(true);

		form.add(new Submit("consultar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			String userIdText = tUserId.getValue();
			String texto = tTexto.getValue();
			String modoBusca = tModoBusca.getValue();
			long userId = -1;

//			Usuario u = usuarioService.get(userId);
			if(StringUtils.isNotEmpty(userIdText)) {
				if(NumberUtils.isNumber(userIdText)) {
					userId = new Long(userIdText);
				}
				else {
					return new MensagemResult("ERROR","Usuário incorreto.");
				}
			} else {
				return new MensagemResult("ERROR","Informe usuário");
			}
			
			Usuario u = usuarioService.get(userId);
			if(u == null) {
				return new MensagemResult("ERROR","Usuário inválido.");
			}

			
			int maxRows = NumberUtils.toInt(getContext().getRequestParameter("maxRows"),20);

			List<Tag> tags = tagService.getTags(texto, getEmpresa());
			
			
			TipoBuscaArquivo tipo;
			if (modoBusca.equalsIgnoreCase("todos")) {
				tipo = TipoBuscaArquivo.TODOS_ARQUIVOS_QUE_USUARIO_POSSUI_ACESSO;
			} else if (modoBusca.equalsIgnoreCase("meusArquivos")) {
				tipo = TipoBuscaArquivo.POSTADOS_PELO_USUARIO;
			} else {
				return new MensagemResult("ERROR","Tipo de busca inválido");
			}
			
			
			List<Arquivo> list = arquivoService.findAllByUser(u, texto, tags, tipo, page, maxRows);
			
			List<FileVO> listVo = arquivoService.toListVo(u, list, tTipo.getValue());
	
			return listVo;
		}
		
		return new MensagemResult("NOK","Erro ao buscar comentarios.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("arquivos", FileVO.class);
		x.alias("thumb", ThumbVO.class);
		x.alias("thumb", ArquivoThumb.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
