package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.chatAkka.LivecomChatInterface;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.LocationVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.StatusMensagemUsuarioVO;
import br.livetouch.livecom.domain.vo.ThumbVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class GetMensagensNaoLidasPage extends WebServiceXmlJsonPage {

	@Autowired
	private LivecomChatInterface livecomChatInterface;
	
	public Form form = new Form();
	private TextField tMode;
	
	private UsuarioLoginField tUser;
	public Long lastMsgId;
	
	public int maxRows;
	public int page;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		form.add(tUser = new UsuarioLoginField("user_id", false, usuarioService, getEmpresa()));
		
		form.add(new LongField("lastMsgId"));
		
		form.add(new IntegerField("page"));
		form.add(new IntegerField("maxRows"));
			
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));
		
		tMode.setValue("json");

		form.add(new Submit("Enviar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario user = tUser.getEntity();

			List<MensagemVO> mensagens = livecomChatInterface.findMensagensMobile(user.getId(), lastMsgId);

			Response r = Response.ok("OK");
			r.mensagens = mensagens;
			return r;
		}
		return new MensagemResult("NOK","Erro ao listar mensagens.");
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return super.isSaveLogTransacao();
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("conversa", ConversaVO.class);
		x.alias("msg", MensagemVO.class);
		x.alias("arquivo", FileVO.class);
		x.alias("statusMensagemUsuario", StatusMensagemUsuarioVO.class);
		x.alias("thumb", ThumbVO.class);
		x.alias("loc", LocationVO.class);
		super.xstream(x);
	}
}
