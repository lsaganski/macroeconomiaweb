package br.livetouch.livecom.web.pages.report.push;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.report.RelatorioPage;



@Controller
@Scope("prototype")
public class DetalhesRelatorioComunicadosPage extends RelatorioPage {

	public Long id;
	
	@Override
	public void onInit() {
		super.onInit();
	}
	
}
