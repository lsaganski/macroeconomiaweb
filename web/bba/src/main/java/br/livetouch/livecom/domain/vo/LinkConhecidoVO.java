package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.LinkConhecido;

public class LinkConhecidoVO {
	
	//ATRIBUTOS
	private Long id;
	private String nome;
	private String dominio;
	
	//CONSTRUTORES
	public LinkConhecidoVO() {

	}
	
	public LinkConhecidoVO(LinkConhecido link) {
		this.id = link.getId();
		this.nome = link.getNome();
		this.dominio = link.getDominio();
	}
	
	//METODOS
	public static List<LinkConhecidoVO> fromList (List<LinkConhecido> links){
		List<LinkConhecidoVO> vos = new ArrayList<>();
		if(links == null) {
			return vos;
		}
		for (LinkConhecido a : links) {
			vos.add(new LinkConhecidoVO(a));
		}
		return vos;
	}
	
	//ENCAPSULAMENTO
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDominio() {
		return dominio;
	}
	public void setDominio(String dominio) {
		this.dominio = dominio;
	}
	
}
