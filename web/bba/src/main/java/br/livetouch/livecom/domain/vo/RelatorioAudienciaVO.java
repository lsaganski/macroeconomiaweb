package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.utils.DateUtils;

public class RelatorioAudienciaVO implements Serializable {

	private static final long serialVersionUID = 1524516347542576158L;
	
	private String data;
	private Date dataPublicacao;
	private Long count;
	private Long id;
	private String titulo;
	private Long postId;
	private String usuario;
	private String fotoUser;
	private String tags;
	private Long usuarioId;
	private Long countLikes;
	private Long countFavoritos;
	private Long countComentarios;
	private Long countVisualizacoes;
	private boolean lida;
	private boolean visualizado;
	
	public RelatorioAudienciaVO(Post post) {
		this.titulo = post.getTitulo();
		this.postId = post.getId();
		this.usuario = post.getUsuario().getNome();
		this.usuarioId = post.getUsuario().getId();
		this.data = DateUtils.toString(post.getDataPublicacao(), "dd/MM/yyyy");
	}
	
	public RelatorioAudienciaVO(String data, Post post, Long count) {
		this.titulo = post.getTitulo();
		this.postId = post.getId();
		this.usuario = post.getUsuario().getNome();
		this.usuarioId = post.getUsuario().getId();
		this.data = data;
		this.count = count;
	}
	
	public RelatorioAudienciaVO() {
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Long getCountLikes() {
		return countLikes;
	}

	public void setCountLikes(Long countLikes) {
		this.countLikes = countLikes;
	}

	public Long getCountFavoritos() {
		return countFavoritos;
	}

	public void setCountFavoritos(Long countFavoritos) {
		this.countFavoritos = countFavoritos;
	}

	public Long getCountComentarios() {
		return countComentarios;
	}

	public void setCountComentarios(Long countComentarios) {
		this.countComentarios = countComentarios;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Date getDataPublicacao() {
		return dataPublicacao;
	}

	public void setDataPublicacao(Date dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
		this.data = DateUtils.toString(dataPublicacao,"dd/MM/yyyy");
	}

	public String getFotoUser() {
		return fotoUser;
	}

	public void setFotoUser(String fotoUser) {
		this.fotoUser = fotoUser;
	}

	public Long getCountVisualizacoes() {
		return countVisualizacoes;
	}

	public void setCountVisualizacoes(Long countVisualizacoes) {
		this.countVisualizacoes = countVisualizacoes;
	}

	public boolean isLida() {
		return lida;
	}

	public void setLida(boolean lida) {
		this.lida = lida;
	}
	
	public boolean isVisualizado() {
		return visualizado;
	}

	public void setVisualizado(boolean visualizado) {
		this.visualizado = visualizado;
	}

}
