package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioConversaFiltro;
import br.livetouch.livecom.domain.vo.StatusMensagemUsuarioVO;
import br.livetouch.livecom.domain.vo.UpdateStatusMensagemResponseVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;

@Repository
public interface MensagemRepository extends net.livetouch.tiger.ddd.repository.Repository<Mensagem> {

	List<Mensagem> findAllByUser(Usuario usuario, Usuario userTo, int tipo, int page, int maxRows);

	List<MensagemConversa> findAllConversasByUser(Usuario user,String search, int page, int maxRows);

	MensagemConversa getMensagemConversaByUser(Usuario userFrom,Usuario userTo);
	MensagemConversa getMensagemConversaByUserGrupo(Usuario user,Grupo grupo);

	List<Mensagem> findMensagensNaoLidas(MensagemConversa conversa);
	
	List<MensagemConversa> findAllConversas();

	List<Mensagem> getMensagensByConversa(MensagemConversa conversa, Usuario user, int page, int maxRows, boolean orderAsc);

	List<Long> findUsuariosComQuemTemConversa(long userId);

	List<Mensagem> findDetalhesByConversa(RelatorioConversaFiltro filtro);
	List<StatusMensagemUsuarioVO> findDetalhesByMensagem(Long id);
	
	long getCountDetalhesByConversa(RelatorioConversaFiltro filtro);

	/**
	 * status: 1 - recebida, 2 lida
	 */
	int updateStatusConversa(Long userId, Long conversaId, Integer status);

	/**
	 * status: 1 - recebida, 2 lida
	 */
	int updateStatusMensagem(Long userId, Long mensagemId, Integer status);

	void checkHibernateProxy(Mensagem mensagem);

	Long findConversaByMensagem(Long mensagemId);

	Long getBadges(Usuario usuario);

	Long countMensagensNaoLidas();

	List<Long> usersToSilent(Empresa e);

	List<Long> findAllIdsConversasByUser(Usuario u);

	List<Long> findAllMensagensIdsOwnerByUser(Usuario u);

	/**
	 * Retonra a qtde de msgs enviadas aos usuarios, e quantos ja receberam e leram.
	 * 
	 * @param msgId
	 * @return
	 */
	UpdateStatusMensagemResponseVO countStatusMensagemGrupo(long msgId);

	List<UsuarioVO> findUsuariosGrupo(Mensagem msg);

	List<MensagemConversa> findAllConversasByUser(long userId);

	List<Mensagem> findMensagensCallback(MensagemConversa c, Usuario user, Long lastReceivedMessageID);

	boolean updateDataPushMsg(Long msgId);

	boolean updateCallbackMensagem(long conversaId,Long msgId,boolean callback1cinza, boolean callback2cinza, boolean callback2azul);

	Mensagem findByIdentifier(Long conversaId, Long idUserFrom, Long identifier);

	Long getBadges(MensagemConversa c, Usuario user);

	List<Mensagem> findMensagensMobile(Usuario usuario, long lastMsgId);
	
	List<Mensagem> findMensagensById(List<Long> msgIds);

	void forward(Mensagem msg, MensagemConversa c);
	
}