package br.livetouch.livecom.domain.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.FuncaoRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class FuncaoRepositoryImpl extends StringHibernateRepository<Funcao> implements FuncaoRepository {

	public FuncaoRepositoryImpl() {
		super(Funcao.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findIdNomeFuncao(Long idEmpresa) {
		Query query = createQuery("select id,codigo from Funcao where empresa.id = :idEmpresa");
		query.setLong("idEmpresa", idEmpresa);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Funcao> findAll(Usuario userInfo) {
		StringBuffer sql = new StringBuffer("from Funcao f where 1=1 ");

		sql.append(" and f.empresa.id=:empresaId ");

		Query q = createQuery(sql.toString());
		if(userInfo == null || userInfo.getEmpresa() == null) {
			return new ArrayList<>();
		}
		q.setParameter("empresaId", userInfo.getEmpresa().getId());

		return q.list();
	}

	@Override
	public Funcao findByNome(String nome, Long idEmpresa) {
		StringBuffer sb = new StringBuffer("FROM Funcao f where 1=1 AND f.empresa.id = :empresaId AND f.nome = :nome");
		Query q = createQuery(sb.toString());
		q.setLong("empresaId", idEmpresa);
		q.setParameter("nome", nome);
		return (Funcao) (q.list().size() > 0 ? q.list().get(0) : null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Funcao findDefaultByEmpresa(Long empresaId) {
		Query q = createQuery("FROM Funcao f where f.empresa.id = :empresaId");
		q.setLong("empresaId", empresaId);
		List<Funcao> list = q.list();
		Funcao f = (Funcao) (list.size() > 0 ? q.list().get(0) : null);
		return f;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Funcao> findAll(Empresa empresa) {
		Query q = createQuery("FROM Funcao f where f.empresa.id = :empresaId");
		q.setLong("empresaId", empresa.getId());
		List<Funcao> list = q.list();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Funcao> findByNome(String nome, int page, int maxRows, Empresa empresa) {
		Query query = getQueryFindByNome(nome, false, empresa);

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * maxRows;
			
		}
		query.setFirstResult(firstResult);
		
		if(maxRows != 0) {
			query.setMaxResults(maxRows);
		}

		query.setCacheable(true);
		List<Funcao> list = query.list();

		return list;
	}
	
	private Query getQueryFindByNome(String nome, boolean count, Empresa empresa) {
		StringBuffer sb = new StringBuffer();

		if (count) {
			sb.append("select count(distinct f.id) ");
		} else {
			sb.append("select distinct f ");
		}

		sb.append(" from Funcao f ");

		sb.append(" where 1=1 ");

		sb.append(" AND f.empresa = :empresa");
		
		if (StringUtils.isNotBlank(nome)) {
			sb.append(" and (upper(f.nome) like :nome)");
		}

		if (!count) {
			sb.append(" order by f.nome ");
		}

		Query query = createQuery(sb.toString());

		if (StringUtils.isNotBlank(nome)) {
			query.setParameter("nome", "%" + nome.toUpperCase() + "%");
		}

		query.setParameter("empresa", empresa);

		query.setCacheable(true);

		return query;
	}

	@Override
	public Long countByNome(String nome, int page, int maxRows, Empresa empresa) {
		Query query = getQueryFindByNome(nome, true, empresa);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Funcao findByName(Funcao funcao, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Funcao f where f.nome = :nome");
		sb.append(" and f.empresa = :empresa");
		
		if(funcao.getId() != null) {
			sb.append(" and f.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("empresa", empresa);
		q.setParameter("nome", funcao.getNome());
		
		if(funcao.getId() != null) {
			q.setParameter("id", funcao.getId());
		}
		
		List<Funcao> list = q.list();
		Funcao f = (Funcao) (list.size() > 0 ? q.list().get(0) : null);
		return f;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findIdCodFuncao(Long idEmpresa) {
		Query query = createQuery("select id,codigo from Funcao where empresa.id = :idEmpresa");
		query.setLong("idEmpresa", idEmpresa);
		return query.list();
	}

	@Override
	public void delete(List<Funcao> funcoes) {
		Query query = createQuery("delete from Funcao f where f in(:list)");
		query.setParameterList("list", funcoes);
		query.executeUpdate();
	}

}