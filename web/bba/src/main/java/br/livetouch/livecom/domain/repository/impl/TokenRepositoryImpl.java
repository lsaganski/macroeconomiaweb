package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Token;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.TokenRepository;

@Repository
public class TokenRepositoryImpl extends LivecomRepository<Token> implements TokenRepository {

	public TokenRepositoryImpl() {
		super(Token.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Token> findAllByUser(Usuario u) {
		StringBuffer sb = new StringBuffer("from Token t where t.usuario.id=?");

		Query q = createQuery(sb.toString());

		q.setLong(0, u.getId());
		List<Token> list = q.list();
		return list;
	}

	@Override
	public Token findLastToken(Long userId, String token, String so) {
		StringBuffer sb = new StringBuffer("from Token t where 1=1 ");
		
		if(userId != null) {
			sb.append(" and t.usuario.id = :user");
		}
		
		if(StringUtils.isNotEmpty(token)) {
			sb.append(" and t.token = :token");
		}
		
		if(StringUtils.isNotEmpty(so)) {
			sb.append(" and t.so = :so");
		}

		Query q = createQuery(sb.toString());

		if(userId != null) {
			q.setParameter("user", userId);
		}
		
		if(StringUtils.isNotEmpty(token)) {
			q.setParameter("token", token);
		}
		
		if(StringUtils.isNotEmpty(so)) {
			q.setParameter("so", StringUtils.lowerCase(so));
		}

		Token t = (Token) (!q.list().isEmpty() ? q.list().get(0) : null);
		return t;
	}

}