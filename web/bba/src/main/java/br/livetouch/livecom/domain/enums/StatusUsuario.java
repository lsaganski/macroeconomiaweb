package br.livetouch.livecom.domain.enums;

/**
 * @author Ricardo Lecheta
 * @created 23/02/2013
 */
public enum StatusUsuario {

	NOVO("Novo","Novo: Foi cadastrado recentemente, e ainda não foi enviado o convite"),
	ENVIADO_CONVITE("Enviado convite","Enviado o convite mas ainda não se logou"),
	ENVIADO_CONVITE_JA_LOGOU("Enviado convite - já logou","Enviado o convite, se logou pela Web no sistema e não salvou o cadastro"),
	ENVIADO_CONVITE_JA_LOGOU_MOBILE("Enviado convite - logou mobile","Enviado o convite, se logou pelo Mobile"),
	ATIVO("Ativo","Ativo: Fez login no sistema e salvou o cadastro");

	private final String s;
	private final String desc2;

	StatusUsuario(String s,String desc2){
		this.s = s;
		this.desc2 = desc2;
	}

	@Override
	public String toString() {
		return s != null ? s : "?";
	}

	public String getDesc() {
		return s;
	}
	
	public String getDesc2() {
		return desc2;
	}
}
