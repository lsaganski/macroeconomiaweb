package br.livetouch.livecom.domain.repository.impl;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Palestrante;
import br.livetouch.livecom.domain.repository.PalestranteRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class PalestranteRepositoryImpl extends StringHibernateRepository<Palestrante> implements PalestranteRepository {

	public PalestranteRepositoryImpl() {
		super(Palestrante.class);
	}

}