package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;

import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoLogSistema;
import net.livetouch.extras.util.DateUtils;

/**
 * Filtro com todos os campos de LogSistema, mas a dataInicial e dataFinal
 * 
 * @author ricardo
 *
 */
public class LogSistemaFiltro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String SESSION_FILTRO_KEY = "LogSistemaFiltro";

	private final LogSistema logSistema;

	private Date dataInicial;
	private Date dataFinal;
	private Usuario usuario;
	private Long usuarioId;
	
	private TipoLogSistema tipo;

	public LogSistemaFiltro(LogSistema logSistema) {
		super();
		this.logSistema = logSistema;
	}

	public LogSistema getLogSistema() {
		return logSistema;
	}
	
	public LogSistemaFiltro() {
		super();
		this.logSistema = null;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public String getDataInicialStringDMY() {
		return DateUtils.toString(dataInicial, "dd/MM/yyyy");
	}

	public String getDataFinalStringDMY() {
		return DateUtils.toString(dataFinal, "dd/MM/yyyy");
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public TipoLogSistema getTipo() {
		return tipo;
	}

	public void setTipo(TipoLogSistema tipo) {
		this.tipo = tipo;
	}

}
