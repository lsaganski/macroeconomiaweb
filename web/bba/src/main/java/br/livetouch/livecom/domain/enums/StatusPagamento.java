package br.livetouch.livecom.domain.enums;

/**
 * @author Ricardo Lecheta
 * @created 23/02/2013
 */
public enum StatusPagamento {

	NOVO("Novo"),
	AGUARDANDO_PAGAMENTO("Aguardando Pagamento"),
	PAGO("Pago"),
	EXPIROU("Expirou");

	private final String s;

	StatusPagamento(String s){
		this.s = s;
	}

	@Override
	public String toString() {
		return s != null ? s : "?";
	}
}
