package br.livetouch.livecom.domain.repository;

import java.util.Set;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostIdioma;

@Repository
public interface PostIdiomaRepository extends net.livetouch.tiger.ddd.repository.Repository<PostIdioma> {

	Set<PostIdioma> findByPost(Post p);
	Set<PostIdioma> findByIdioma(Idioma i);

}