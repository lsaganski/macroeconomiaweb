package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Evento;
import br.livetouch.livecom.domain.Grupo;

@Repository
public interface EventoRepository extends net.livetouch.tiger.ddd.repository.Repository<Evento> {

	List<Evento> findByAtivo(List<Grupo> grupos);

}