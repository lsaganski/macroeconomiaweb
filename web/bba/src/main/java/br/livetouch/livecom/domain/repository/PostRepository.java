package br.livetouch.livecom.domain.repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostAuditoria;
import br.livetouch.livecom.domain.PostUsuarios;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.BuscaPost;

@Repository
public interface PostRepository extends net.livetouch.tiger.ddd.repository.Repository<Post> {

	Post get(Long id, Idioma idioma);
	
	/**
	 * Mural
	 * 
	 * @param b
	 * @param page
	 * @param maxRows
	 * @return
	 */
	List<Post> findAllPosts(BuscaPost b, int page, int maxRows);
	
	List<Post> findAllByDate(Date data);

	/**
	 * Relatorios
	 * 
	 * @param titulo
	 * @return
	 */
	List<Post> findAllByTituloLike(String titulo);
	List<Post> findAllByTituloLikeWithMax(String titulo, int max, Empresa empresa);
	List<Post> findAllRascunhosByUser(Usuario user, int page, int maxSize);
	
	///////////////////////////////////
	
	void saveOrUpdate(PostAuditoria a);
	
	br.livetouch.livecom.domain.Post findByIdWordpress(Long id);
	
	long getCountUsuariosByPost(Post p);
	
	List<Post> findByCategorias(List<CategoriaPost> categorias);
	
	void delete(List<Post> posts);
	
	/**
	 * Posts do usuario ou feito por usuarios dentro de um grupo no qual o user é admin.
	 * 
	 * @param u
	 * @return
	 */
	List<Long> findAllOwnerByUser(Usuario u);

	/**
	 * Posts feitos pelo usuario
	 * 
	 * @param u
	 * @return
	 */
	List<Long> findAllCriadosByUser(Usuario u);
	
	List<Long> findidsByCategoria(CategoriaPost c);

	List<Post> findPostsByTag(Tag tag);

	List<Post> findPostsByGrupo(Grupo g, boolean unicosDesteGrupo);

	/**
	 * Publica os posts agendados
	 * @return
	 */
	int publicar();

	/**
	 * Expira os posts com data de expiracao
	 * @return
	 */
	int expirar();

	List<PostUsuarios> findPostUsuarios(Usuario user, List<Post> posts);

	PostUsuarios getPostUsuarios(Usuario user, Post p);

	PostUsuarios getPostUsuarios(Long user, Long p);

	void saveOrUpdate(PostUsuarios postUsuarios);

	Set<Arquivo> getArquivos(Post post);

}