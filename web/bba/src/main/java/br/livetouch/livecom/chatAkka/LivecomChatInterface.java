package br.livetouch.livecom.chatAkka;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONException;

import akka.actor.ActorRef;
import br.livetouch.livecom.chatAkka.router.ChatVO;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.ControleVersaoVO;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.UpdateStatusMensagemResponseVO;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import net.livetouch.tiger.ddd.DomainException;

public interface LivecomChatInterface {

	@Transactional
	public Usuario findByLogin(String login);

	@Transactional
	public Usuario findByUserId(Long id);

	@Transactional
	public int chatPort();

	@Transactional
	public List<UsuarioVO> findUsersFromConversation(Long conversaID, Long userID, boolean includeUser);

	@Transactional
	public void sendPush(MensagemVO mensagem, UsuarioVO u) ;
	
	@Transactional
	public void sendPush(MensagemVO mensagem, List<UsuarioVO> users) ;

	@Transactional
	public List<Long> findUsuariosComQuemTemConversa(long userId);

	@Transactional
	public UpdateStatusMensagemResponseVO updateStatusMensagem(long userId,long msgId, int status) throws DomainException;

	@Transactional
	MensagemVO saveMessage(ChatVO c) throws Exception;

	@Transactional
	public void saveMessageAdminAndSendAkka(Usuario admin, Grupo g, MensagemConversa conversa, String message) throws DomainException;

	@Transactional
	public UsuarioVO getUsuario(Long userId);

	@Transactional
	public ConversaVO findConversaUsers(Long userFromId, Long userToId);

	@Transactional
	Date updateLoginChat(Long id);

	@Transactional
	boolean markConversationAsRead(long fromUserId, long conversationId) throws DomainException;

	@Transactional
	public List<MensagemVO> findMensagensCallback(long userId, Long lastReceivedMessageID);

	@Transactional
	public UserInfoVO login(String user, String pwd, String so, ActorRef tcpWebActor, boolean away);

	@Transactional
	public MensagemVO getMensagem(long msgId);

	/**
	 * 1 - check1cinza, 2 check2cinza, 3 check2azul, 4 conversationWasRead
	 */
	@Transactional
	void updateStatusMensagemCallback(long conversaId,long msgId, int code) throws DomainException;

	@Transactional
	public List<MensagemVO> findMensagensMobile(Long from, Long lastMsgId);

	public ControleVersaoVO validateVersion(Long empresaId, String appVersion, Integer appVersionCode,
			String userAgentSO);

	@Transactional
	public MensagemVO forward(ChatVO chat) throws DomainException;

	List<Long> findIdsUsersFromConversation(Long conversaID, Long userID, boolean includeUser);

	@Transactional
	public void markConversasReceivedMessages(List<Long> cIds, Long fromId) throws DomainException, JSONException;

	@Transactional
	public boolean isUsuarioDentroDoGrupo(long from, long grupoId);

	@Transactional
	List<MensagemVO> findMensagensById(List<Long> ids);

	@Transactional
	/**
	 * A partir de uma lista de ids enviadas, retorna apenas ids das conversas que estão lidas.
	 * @param ids
	 * @return
	 */
	List<Long> findIdsConversasLidasById(List<Long> ids, Long userId);
}