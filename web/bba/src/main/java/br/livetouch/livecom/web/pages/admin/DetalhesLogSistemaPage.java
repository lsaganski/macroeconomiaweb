package br.livetouch.livecom.web.pages.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
import net.livetouch.click.control.IdField;
import net.sf.click.control.ActionButton;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Form;


@Controller
@Scope("prototype")
public class DetalhesLogSistemaPage extends RelatorioPage {

	public Long id;
	public LogSistema log;		

	public ActionButton btnVoltar = new ActionButton("voltar", getMessage("voltar.label"), this, "voltar");
	public ActionLink linkDeletar = new ActionLink("deletar",getMessage("deletar.label"),this,"deletar");
	public Form form = new Form();

	@Override
	public void onGet() {
		super.onGet();
		if(id != null) {
			log = logService.getLogSistema(id);
			form.add(new IdField());
			btnVoltar.setAttribute("class", "btn btn-primary min-width");
			form.add(btnVoltar);

			if(isRoot()) {
				linkDeletar.setValueObject(id);
			}
		}
	}
	
	public void onRender() {
		linkDeletar.setAttribute("class", "botao branco");		
	}

	public boolean voltar(){
		setRedirect(LogSistemaPage.class);
		return true;
	}
	
	
	public boolean deletar(){
		if(isRoot()) {
			Long id = linkDeletar.getValueLong();
			log = logService.getLogSistema(id);
			logService.delete(log);
			setFlashAttribute("msg", getMessage("msg.logSistema.excluir.sucess"));
			setRedirect(LogSistemaPage.class);
		}
		return false;
	}
}
