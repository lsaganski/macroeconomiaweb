package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;

import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.StringUtils;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.enums.TipoPush;
import br.livetouch.livecom.domain.enums.TipoTemplate;
import net.livetouch.extras.util.DateUtils;

/**
 * Filtro com todos os campos de LogSistema, mas a dataInicial e dataFinal
 * 
 * @author ricardo
 *
 */
public class RelatorioFiltro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8800402609370460617L;

	/**
	 * 
	 */
	
	public static final String SESSION_FILTRO_KEY = "relatorioFiltro";

	private Date dataInicial;
	private Date dataFinal;
	@QueryParam("page")
	private int page;
	@QueryParam("max")
	private int max;
	
	@QueryParam("expandir")
	private boolean expandir;
	@QueryParam("agrupar")
	private boolean agrupar;
	@QueryParam("lida")
	private boolean lida;
	
	private String versao;
	
	@QueryParam("dataIni")
	private String dataIni;
	@QueryParam("dataFim")
	private String dataFim;
	
	@QueryParam("usuario")
	private Long usuarioId;
	
	@QueryParam("usuarioLogin")
	private String usuarioLogin;
	
	@QueryParam("usuarioNome")
	private String usuarioNome;
	
	@QueryParam("postTitulo")
	private String postTitulo;
	
	@QueryParam("post")
	private Long postId;
	
	@QueryParam("categoria")
	private Long categoriaId;
	@QueryParam("categoriaNome")
	private String categoriaNome;
	
	private Usuario usuario;
	private Post post;
	private CategoriaPost categoria;
	
	@QueryParam("modo")
	private String modo;
	
	@QueryParam("tipo")
	private String tipo;
	
	@QueryParam("tipoEntidade")
	private String tipoEntidade;
	
	@QueryParam("tipoTemplate")
	private String tipoTemplate;
	
	@QueryParam("tipoPush")
	private String tipoPush;
	
	@QueryParam("acao")
	private String acao;

	@QueryParam("projeto")
	private String projeto;
	
	@QueryParam("status")
	private boolean status;

	@QueryParam("prioritario")
	private boolean prioritario;

	private String dataDetalhes;
	
	private Long empresaId;
	
	@QueryParam("versionCodeGrafico")
	private boolean versionCodeGrafico;
	
	@QueryParam("version")
	private String version;

	@QueryParam("versionCode")
	private String versionCode;
	
	@QueryParam("os")
	private String os;

	public RelatorioFiltro(Date dataInicial, Date dataFinal, int page, int max, boolean expandir) {
		super();
		this.dataInicial = dataInicial;
		this.dataFinal = dataFinal;
		this.page = page;
		this.max = max;
		this.expandir = expandir;
	}
	
	public RelatorioFiltro(Date dataInicial, Date dataFinal, int page, int max, String versao) {
		super();
		this.dataInicial = dataInicial;
		this.dataFinal = dataFinal;
		this.page = page;
		this.max = max;
		this.versao = versao;
	}
	
	public RelatorioFiltro() {
		
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public String getDataInicialStringDMY() {
		return DateUtils.toString(dataInicial, "dd/MM/yyyy");
	}

	public String getDataFinalStringDMY() {
		return DateUtils.toString(dataFinal, "dd/MM/yyyy");
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public boolean isExpandir() {
		return expandir;
	}

	public void setExpandir(boolean expandir) {
		this.expandir = expandir;
	}

	public String getVersao() {
		return versao;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public String getDataIni() {
		return dataIni;
	}

	public void setDataIni(String dataIni) {
		this.dataIni = dataIni;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public boolean isAgrupar() {
		return agrupar;
	}

	public void setAgrupar(boolean agrupar) {
		this.agrupar = agrupar;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	}

	public String getDataDetalhes() {
		return dataDetalhes;
	}

	public void setDataDetalhes(String dataDetalhes) {
		this.dataDetalhes = dataDetalhes;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public String getPostTitulo() {
		return postTitulo;
	}

	public void setPostTitulo(String postTitulo) {
		this.postTitulo = postTitulo;
	}

	public String getUsuarioLogin() {
		return usuarioLogin;
	}

	public void setUsuarioLogin(String usuarioLogin) {
		this.usuarioLogin = usuarioLogin;
	}

	public String getUsuarioNome() {
		return usuarioNome;
	}

	public void setUsuarioNome(String usuarioNome) {
		this.usuarioNome = usuarioNome;
	}

	public Long getEmpresaId() {
		return empresaId;
	}

	public void setEmpresaId(Long empresaId) {
		this.empresaId = empresaId;
	}

	public CategoriaPost getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaPost categoria) {
		this.categoria = categoria;
	}

	public Long getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Long categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getCategoriaNome() {
		return categoriaNome;
	}

	public void setCategoriaNome(String categoriaNome) {
		this.categoriaNome = categoriaNome;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public TipoNotificacao getTipoEnum() {
		return StringUtils.equals(tipo, "Todos") ? null : TipoNotificacao.valueOf(tipo);
	}

	public boolean isLida() {
		return lida;
	}

	public void setLida(boolean lida) {
		this.lida = lida;
	}

	public String getTipoEntidade() {
		return tipoEntidade;
	}

	public void setTipoEntidade(String tipoEntidade) {
		this.tipoEntidade = tipoEntidade;
	}
	
	public AuditoriaEntidade getTipoEntidadeEnum() {
		return StringUtils.equals(tipoEntidade, "Todos") ? null : AuditoriaEntidade.valueOf(StringUtils.upperCase(tipoEntidade));
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}
	
	public AuditoriaAcao getAcaoEnum() {
		return StringUtils.equals(acao, "Todos") ? null : AuditoriaAcao.valueOf(StringUtils.upperCase(acao));
	}

	public String getTipoPush() {
		return tipoPush;
	}

	public void setTipoPush(String tipoPush) {
		this.tipoPush = tipoPush;
	}
	
	public TipoPush getTipoPushEnum() {
		return StringUtils.equals(tipoPush, "Todos") ? null : TipoPush.valueOf(tipoPush);
	}

	public String getTipoTemplate() {
		return tipoTemplate;
	}

	public void setTipoTemplate(String tipoTemplate) {
		this.tipoTemplate = tipoTemplate;
	}
	
	public TipoTemplate getTipoTemplateEnum() {
		if(tipoTemplate != null) {
			return StringUtils.equals(tipoTemplate, "Não definir por enquanto") ? null : TipoTemplate.valueOfLabel(Integer.valueOf(tipoTemplate));
		}
		return null;
	}

	public boolean isEnviado() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getProjeto() {
		return projeto;
	}

	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}

	public boolean isVersionCodeGrafico() {
		return versionCodeGrafico;
	}

	public void setVersionCodeGrafico(boolean versionCodeGrafico) {
		this.versionCodeGrafico = versionCodeGrafico;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public boolean isPrioritario() {
		return prioritario;
	}

	public void setPrioritario(boolean prioritario) {
		this.prioritario = prioritario;
	}


}
