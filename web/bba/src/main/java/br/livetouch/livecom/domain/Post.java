package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.OrderColumn;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.google.gson.Gson;

import br.livetouch.livecom.domain.enums.Status;
import br.livetouch.livecom.domain.enums.StatusPublicacao;
import br.livetouch.livecom.domain.enums.Visibilidade;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.livecom.utils.DateUtils;


@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Post extends JsonEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "POST_SEQ")
	private Long id;

	@Column(nullable = true)
	private String titulo;

	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinTable(name = "post_tags", joinColumns = { @JoinColumn(name = "post_id") }, inverseJoinColumns = { @JoinColumn(name = "tag_id") })
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Tag> tagsList;

	/**
	 * http://forums.mysql.com/read.php?10,157804,157806#msg-157806
	 */
	@Column(columnDefinition = "clob")
	private String mensagem;

	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.REMOVE)
	@JoinTable(name = "post_grupos", joinColumns = { @JoinColumn(name = "post_id") }, inverseJoinColumns = { @JoinColumn(name = "grupo_id") })
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Grupo> grupos;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categoria_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private CategoriaPost categoria;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "post_categorias", joinColumns = { @JoinColumn(name = "post_id") }, inverseJoinColumns = { @JoinColumn(name = "categoria_id") })
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@OrderColumn(insertable=true)
	private List<CategoriaPost> categorias;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "status_post_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private StatusPost statusPost;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario usuario;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_update_id", nullable = true)
	private Usuario usuarioUpdate;

	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@OrderBy("id asc")
	private Set<Comentario> comentarios;
	
	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@NotFound(action = NotFoundAction.IGNORE)
	private Set<Arquivo> arquivos;
	
	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Favorito> favoritos;
	
	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Likes> likes;

	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<PostView> postViews;

	@Column(length=1000)
	private String urlImagem;
	private String urlSite;
	private String urlVideo;
	
	@Column(length=50)
	private String videoPlayer;

	private Date data;
	private Date dataUpdated;
	private Date dataPublicacao;
	private Date dataPublicacaoAprov;
	private Date dataExpiracao;
	
	private Date reminder;
	
	private Date dataPush;

	private Long likeCount;
	private Long commentCount;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_destaque_id", nullable = true)
	private PostDestaque postDestaque;
	
	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY)
	private Set<Historico> historicos;
	
	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY)
	private Set<Rate> rates;
	
	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY)
	private Set<Playlist> playLists;
	
	private int tipo;

	private Long postViewCount;

	private Long rateCount;
	
	@Enumerated(EnumType.STRING)
	private Status status;

	private Boolean destaque;

	private Boolean prioritario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "chapeu_id", nullable = true)
	private Chapeu chapeu;
	
	@Column(length=255)
	private String resumo;
	
	private Long idWordpress;

	private boolean rascunho;

	@Enumerated(EnumType.ORDINAL)
	private StatusPublicacao statusPublicacao;

	@Enumerated(EnumType.STRING)
	private Visibilidade visibilidade = Visibilidade.GRUPOS;

	private Boolean webview = false;
	private Boolean html = false;
	
	@OneToMany(mappedBy = "post", fetch = FetchType.LAZY)
	private Set<PostIdioma> idiomas;
	
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTags() {
		String tags = getTagsString();
		return tags;
	}

	public Set<Tag> getTagsList() {
		return tagsList;
	}

	public void setTagsList(Set<Tag> tagsList) {
		this.tagsList = tagsList;
	}

	public Set<Grupo> getGrupos() {
		return grupos;
	}

	public void setGrupos(Set<Grupo> grupos) {
		this.grupos = grupos;
	}

	public CategoriaPost getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaPost categoria) {
		this.categoria = categoria;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		if(this.usuario == null) {
			this.usuario = usuario;
		}
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlFoto) {
		this.urlImagem = urlFoto;
	}

	public String getUrlSite() {
		return urlSite;
	}

	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

	public String getUrlVideo() {
		return urlVideo;
	}
	public String getUrlVideoEmbed() {
		String s = urlVideo;
		s = StringUtils.replace(s, "https://www.youtube.com/watch?v=", "//www.youtube.com/embed/");
		return s;
	}

	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}

	public Date getData() {
		return data;
	}
	
	public Long getDataTimestamp() {
		return data != null ? data.getTime() : 0;
	}
	
	public String getDataStringHojeOntem() {
		Date dt = getData();
		return br.livetouch.livecom.utils.DateUtils.toDateStringHojeOntem(dt);
	}
	
	public String getDataPublicacaoStringHojeOntem() {
		Date dt = getDataPublicacao();
		if(dt == null) {
			return null;
		}
		return br.livetouch.livecom.utils.DateUtils.toDateStringHojeOntem(dt);
	}
	
	public String getDataExpiracaoStringHojeOntem() {
		Date dt = getDataExpiracao();
		if(dt == null) {
			return null;
		}
		return br.livetouch.livecom.utils.DateUtils.toDateStringHojeOntem(dt);
	}
	
	public String getDataStringHojeOntemHora() {
		Date dt = getData();
		return br.livetouch.livecom.utils.DateUtils.toDateStringHojeOntemHora(dt);
	}
	
	public String getDataStringTimeAgo() {
		Date dt = getData();
		return br.livetouch.livecom.utils.DateUtils.toDateStringTimeAgo(dt);
	}

	public String getDataString() {
		String dataStr = net.livetouch.extras.util.DateUtils.toString(getData(),"hh:mm");;
		return dataStr;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	public Date getDataUpdated() {
		return dataUpdated;
	}
	
	public void setDataUpdated(Date dataUpdated) {
		this.dataUpdated = dataUpdated;
	}

	public Long getLikeCount() {
		if (likeCount == null) {
			return 0L;
		}
		return likeCount;
	}

	public void setLikeCount(Long likeCount) {
		this.likeCount = likeCount;
	}

	public void like(boolean favorito) {
		long count = getLikeCount();
		if (favorito) {
			count++;
		} else {
			count--;
		}
		if (count < 0) {
			count = 0;
		}
		setLikeCount(count);
	}

	public void comment() {
		long count = getCommentCount();
		count++;
		if (count < 0) {
			count = 0;
		}
		setCommentCount(count);
	}

	public Long getCommentCount() {
		if (commentCount == null) {
			return 0L;
		}
		return commentCount;
	}

	public void setCommentCount(Long commentCount) {
		this.commentCount = commentCount;
	}

	public Set<Comentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(Set<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

	public PostDestaque getPostDestaque() {
		return postDestaque;
	}

	public void setPostDestaque(PostDestaque postDestaque) {
		this.postDestaque = postDestaque;
	}
	
	public int getTipo() {
		return tipo;
	}
	
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	
	public Set<Arquivo> getArquivos() {
		return arquivos;
	}
	
	public void setArquivos(Set<Arquivo> arquivos) {
		this.arquivos = arquivos;
	}

	public Set<Favorito> getFavoritos() {
		return favoritos;
	}
	
	public Set<Likes> getLikes() {
		return likes;
	}
	
	public void setFavoritos(Set<Favorito> favoritos) {
		this.favoritos = favoritos;
	}
	
	public void setLikes(Set<Likes> likes) {
		this.likes = likes;
	}

	public String getTituloDesc() {
		if(StringUtils.isNotEmpty(titulo)) {
			return titulo;
		}
		if(StringUtils.isNotEmpty(mensagem)) {
			if(mensagem.length() > 40) {
				return mensagem.substring(0,40)+"...";
			}
			return mensagem;
		}
		return null;
	}
	
	public Set<Historico> getHistoricos() {
		return historicos;
	}
	public Set<Playlist> getPlayLists() {
		return playLists;
	}
	public Set<Rate> getRates() {
		return rates;
	}
	public void setHistoricos(Set<Historico> historicos) {
		this.historicos = historicos;
	}
	public void setPlayLists(Set<Playlist> playLists) {
		this.playLists = playLists;
	}
	public void setRates(Set<Rate> rates) {
		this.rates = rates;
	}

	public String getTagsString() {
		String s = "";
		if(tagsList == null || tagsList.isEmpty()) {
			return s;
		}
		for (Tag tag : tagsList) {
			if(StringUtils.isNotEmpty(s)) {
				s+=",";
			}
			s += tag.getNome();
		}
		return s;
	}
	
	public String getVideoPlayer() {
		return videoPlayer;
	}
	public void setVideoPlayer(String videoPlayer) {
		this.videoPlayer = videoPlayer;
	}

	public Long getPostViewCount() {
		if(postViewCount == null) {
			return 0L;
		}
		return postViewCount;
	}
	public void setPostViewCount(Long postViewCount) {
		this.postViewCount = postViewCount;
	}
	
	public void incrementPostView() {
		long count = getPostViewCount();
		count++;
		setPostViewCount(count);
	}
	
	public void incrementRate() {
		long count = getRateCount();
		count++;
		setRateCount(count);
	}

	public Long getRateCount() {
		if(rateCount == null) {
			return 0L;
		}
		return rateCount;
	}
	
	public void setRateCount(Long rateCount) {
		this.rateCount = rateCount;
	}
	
	public static List<Long> getIds(Collection<? extends net.livetouch.tiger.ddd.Entity> list) {
		List<Long> ids = new ArrayList<Long>();
		if(list == null || list.size() == 0) {
			return ids;
		}
		for (net.livetouch.tiger.ddd.Entity e : list) {
			ids.add(e.getId());
		}
		return ids;
	}
	
	public Usuario getUsuarioUpdate() {
		return usuarioUpdate;
	}
	
	public void setUsuarioUpdate(Usuario usuarioUpdate) {
		this.usuarioUpdate = usuarioUpdate;
	}
	
	public Date getDataPublicacao() {
		return dataPublicacao;
	}

	public void setDataPublicacao(Date dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}

	public Date getDataPublicacaoAprov() {
		return dataPublicacaoAprov;
	}
	
	public Date getDataExpiracao() {
		return dataExpiracao;
	}

	public void setDataPublicacaoAprov(Date dataPublicacaoAprov) {
		this.dataPublicacaoAprov = dataPublicacaoAprov;
	}
	
	public void setDataExpiracao(Date dataExpiracao) {
		this.dataExpiracao = dataExpiracao;
	}
	
	public StatusPublicacao getStatusPublicacao() {
		return statusPublicacao;
	}
	
	public void setStatusPublicacao(StatusPublicacao statusPublicacao) {
		this.statusPublicacao = statusPublicacao;
	}
	
	public String getStatus() {
		String status = ""; 
		
		if(dataExpiracao != null) {
//			if(DateUtils.isMaiorToday(getDataExpiracao())) {
			Date now = new Date();
			boolean maior = DateUtils.compareTo(now,getDataExpiracao(),"yyyyMMdd HH:mm:s") > 0;
			if(maior) {
				status = "expirado";
				return status;
			} 
		}
		
		if(getDataPublicacao() == null) {
			status = "publicado";
		} else {
			if(DateUtils.isMaiorToday(getDataPublicacao())) {
				status = "agendado";
			} else {
				status = "aprovado";
			}
		}
		return status;
	}
	
	public void setStatus(Status status) {
		this.status = status;
	}

	public Date setDataPublicacao(String s) {
		if(StringUtils.isNotEmpty(s)) {
			try {
				Date dtPub = DateUtils.toDate(s,"dd/MM/yyyy HH:mm:ss");
				
//				if(DateUtils.isToday(dtPub.getTime())) {
//					dtPub = DateUtils.setTimeCurrent(dtPub);	
//				} else {
//					dtPub = DateUtils.setTimeInicioDia(dtPub);
//				}
				
				this.dataPublicacao = dtPub;
				
			} catch (Exception e) {
				this.dataPublicacao = this.data;
			}
		}
		return this.dataPublicacao;
	}
	
	public Date setDataExpiracao(String s) {
		if(StringUtils.isNotEmpty(s)) {
			try {
				Date dtExp = DateUtils.toDate(s,"dd/MM/yyyy HH:mm:ss");
				this.dataExpiracao = dtExp;
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return this.dataExpiracao;
	}
	
	public Boolean getDestaque() {
		return isDestaque();
	}

	public Boolean isDestaque() {
		if(destaque == null) {
			return false;
		}
		return destaque;
	}

	public void setDestaque(Boolean destaque) {
		this.destaque = destaque;
	}

	public Boolean isPrioritario() {
		if(prioritario == null) {
			return false;
		}
		return prioritario;
	}

	public void setPrioritario(Boolean prioritario) {
		this.prioritario = prioritario;
	}

	public void setDestaque(String destaque) {
		this.destaque = "1".equals(destaque);
	}

	public Chapeu getChapeu() {
		return chapeu;
	}
	
	public void setChapeu(Chapeu chapeu) {
		this.chapeu = chapeu;
	}

	public String getResumo() {
		return resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}
	
	public Date getDataPush() {
		return dataPush;
	}
	
	public Long getDataPushTimestamp() {
		return dataPush != null ? dataPush.getTime() : 0;
	}

	public String getDataExpiracaoString() {
		String dataStr = net.livetouch.extras.util.DateUtils.toString(getDataExpiracao(),"dd/MM/yyyy hh:mm");;
		return dataStr;
	}
	
	public String getDataPushString() {
		String dataStr = net.livetouch.extras.util.DateUtils.toString(getDataPush(),"dd/MM/yyyy hh:mm");;
		return dataStr;
	}
	
	public void setDataPush(Date dataPush) {
		this.dataPush = dataPush;
	}
	
	public boolean isPushEnviado() {
		return dataPush != null;
	}

	public Long getIdWordpress() {
		return idWordpress;
	}

	public void setIdWordpress(Long idWordpress) {
		this.idWordpress = idWordpress;
	}

	public boolean isRascunho() {
		return rascunho;
	}

	public void setRascunho(boolean rascunho) {
		this.rascunho = rascunho;
	}

	@Override
	public String toJson() {
		PostVO vo = new PostVO();
		vo.setPost(this, this.getUsuario());
		return new Gson().toJson(vo);
	}
	
	@Override
	public String getValue2() {
		return titulo;
	}
	
	@Override
	public String getValue3() {
		return mensagem;
	}
	
	@Override
	public String getValue4() {
		if(dataPublicacao != null) {
			return DateUtils.toString(dataPublicacao, "dd/MM/yyyy HH:mm");
		}
		return "";
	}
	
	@Override
	public String getValue5() {
		return DateUtils.toString(dataUpdated, "dd/MM/yyyy HH:mm");
	}

	@Override
	public String getValue6() {
		if(categoria != null) {
			return categoria.getNome();
		}
		return "";
	}

	@Override
	public String getValue7() {
		String groups = "";
		if(grupos != null) {
			for (Grupo grupo : grupos) {
				if(grupo != null) {
					groups += grupo.getNome() + ", ";
				}
			}
		}
		return groups;
	}

	@Override
	public String getValue8() {
		if(usuario != null) {
			return usuario.getLogin() + " - " + usuario.getNome();
		}
		return "";
	}

	@Override
	public String getValue9() {
		return getValue6();
	}

	@Override
	public String getValue10() {
		return getValue7();
	}

	@Override
	public String getValue11() {
		if(usuarioUpdate != null) {
			return usuarioUpdate.getLogin() + " - " + usuarioUpdate.getNome();
		}
		return "";
	}

	@Override
	public String toString() {
		return toStringDesc();
	}
	
	public String toStringAll() {
		String s = toJson();
		return s;
	}
	
	public String toStringDesc() {
		return getId()+":"+getTitulo();
	}

	public Set<PostView> getPostViews() {
		return postViews;
	}

	public void setPostViews(Set<PostView> postViews) {
		this.postViews = postViews;
	}

	public Visibilidade getVisibilidade() {
		return visibilidade;
	}

	public void setVisibilidade(Visibilidade visibilidade) {
		this.visibilidade = visibilidade;
	}

	public Boolean isAgendado() {
		Date data = this.getDataPublicacao();
		if(DateUtils.isMaior(data, new Date())) {
			return true;
		}
		return false;

	}

	public StatusPost getStatusPost() {
		return statusPost;
	}

	public void setStatusPost(StatusPost statusPost) {
		this.statusPost = statusPost;
	}

	public Boolean isWebview() {
		if(webview == null) {
			return false;
		}
		return webview;
	}

	public void setWebview(Boolean webview) {
		this.webview = webview;
	}

	public Boolean isHtml() {
		if(html == null) {
			return false;
		}
		return html;
	}
	
	public void setHtml(Boolean html) {
		this.html = html;
	}
	
	public Date getReminder() {
		return reminder;
	}

	public void setReminder(Date reminder) {
		this.reminder = reminder;
	}

	public void setReminderString(String s) {
		if(StringUtils.isNotEmpty(s)) {
			try {
				Date reminder = DateUtils.toDate(s,"dd/MM/yyyy HH:mm:ss");
				this.reminder = reminder;
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public String getReminderString() {
		String dataStr = net.livetouch.extras.util.DateUtils.toString(getReminder(),"dd/MM/yyyy HH:mm");;
		return dataStr;
	}

	public Set<PostIdioma> getIdiomas() {
		return idiomas;
	}

	public void setIdiomas(Set<PostIdioma> idiomas) {
		this.idiomas = idiomas;
	}
	
	public void translate() {
		if(this.idiomas != null && !this.idiomas.isEmpty()) {
			translate(this.idiomas.iterator().next());
		}
	}

	public void translate(PostIdioma idioma) {
		this.titulo = idioma.getTitulo();
		this.mensagem = idioma.getMensagem();
		this.resumo = idioma.getResumo();
	}

	public void translate(Idioma idioma) {
		
		if(idioma == null) {
			return;
		}
		
		if(this.idiomas != null && !this.idiomas.isEmpty()) {
			Iterator<PostIdioma> it = this.idiomas.iterator();
			while(it.hasNext()) {
				PostIdioma pi = it.next();
				if(pi.getIdioma().getId().equals(idioma.getId())) {
					this.titulo = pi.getTitulo();
					this.mensagem = pi.getMensagem();
					this.resumo = pi.getResumo();
					return;
				}
			}
		}
	}

	public List<CategoriaPost> getCategorias() {
		return categorias;
	}

	public void setCategorias(LinkedList<CategoriaPost> categorias) {
		this.categorias = categorias;
	}

//	public void setExcluido(boolean excluido) {
//		this.excluido = excluido;
//	}
//	
//	public boolean isExcluido() {
//		return excluido;
//	}
}
