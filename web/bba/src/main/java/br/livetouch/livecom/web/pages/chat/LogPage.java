package br.livetouch.livecom.web.pages.chat;



import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.chatAkka.ChatLogMessages;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.admin.LivecomAdminPage;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.sf.click.control.ActionLink;

@Controller
@Scope("prototype")
public class LogPage extends LivecomAdminPage {
	
	public ChatLogMessages logChat = ChatLogMessages.getInstance();
	
	public ActionLink linkClear = new ActionLink("linkClear","Limpar",this,"clear");
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissoes(true, ParamsPermissao.VIZUALIZAR_RELATORIOS, ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}
	
	@Override
	public void onInit() {
		super.onInit();
		linkClear.setAttribute("class", "btn btn-primary min-width sbold");
	}
	

	@Override
	public void onGet() {
		super.onGet();
	}
	
	public boolean clear() {
		logChat.clear();
		refresh();
		return true;
	}
	
}
