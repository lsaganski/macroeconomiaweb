package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Expediente;
import net.livetouch.tiger.ddd.DomainException;

public interface ExpedienteService extends Service<Expediente> {

	List<Expediente> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Expediente e) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Expediente e) throws DomainException;

	List<Object[]> findIdNomeExpediente();

	Expediente findByCodigo(Expediente expediente, Empresa empresa);

}
