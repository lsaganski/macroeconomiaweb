package br.livetouch.livecom.web.pages.ws;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

public abstract class WebServiceFormPage extends WebServiceXmlJsonPage {

	protected static Logger log = Log.getLogger("livecom_web_services");
	
	public Form form = new Form();
	
	public int maxRows;
	public int page;

	private TextField tMode;

	@Override	
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		
		form();
		
		form.add(new TextField("wstoken"));
		form.add(new TextField("wsVersion"));
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		form.add(new Submit("consultar"));

		setFormTextWidth(form);
	}

	protected abstract void form();
	protected abstract Object go() throws Exception;

	@Override
	protected Object execute() throws Exception {
//		if (form.isValid()) {
			try {
				return go();
				// Login
			} catch (DomainException e) {
				logError(e.getMessage(), e);
				return new MensagemResult("NOK",e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(), e);
				return new MensagemResult("NOK","Erro ao executar o web service: " + e.getMessage());
			} catch (Throwable e) {
				logError(e.getMessage(), e);
				return new MensagemResult("NOK","Problema ao executar o web service: " + e.getMessage());
			}
//		}
	}
}
