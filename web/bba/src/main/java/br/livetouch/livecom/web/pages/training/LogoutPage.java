package br.livetouch.livecom.web.pages.training;

import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.web.pages.admin.LivecomPage;

@Controller
@Scope("prototype")
public class LogoutPage extends LivecomPage {

	@Override
	public void onInit() {

		UserInfoVO userInfo = getUserInfoVO();
		Livecom.getInstance().removeUserInfo(userInfo,"web");

		HttpSession session = getContext().getSession();
		session.invalidate();

		// index home
		setRedirect(br.livetouch.livecom.web.pages.IndexPage.class);
	}

}
