package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.PermissaoCategoria;
import br.livetouch.livecom.domain.repository.PermissaoCategoriaRepository;
import br.livetouch.livecom.domain.service.PermissaoCategoriaService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class PermissaoCategoriaServiceImpl implements PermissaoCategoriaService {
	@Autowired
	private PermissaoCategoriaRepository rep;

	@Override
	public PermissaoCategoria get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(PermissaoCategoria p, Empresa e) throws DomainException {
		p.setEmpresa(e);
		rep.saveOrUpdate(p);
	}

	@Override
	public List<PermissaoCategoria> findAll(Empresa e) {
		return rep.findAll(e);
	}

	@Override
	public void delete(PermissaoCategoria p) throws DomainException {
		rep.delete(p);
	}

	@Override
	public PermissaoCategoria findByNomeValid(PermissaoCategoria p, Empresa empresa) {
		return rep.findByNomeValid(p, empresa);
	}

	@Override
	public PermissaoCategoria findByCodigoValid(PermissaoCategoria p, Empresa empresa) {
		return rep.findByCodigoValid(p, empresa);
	}

}
