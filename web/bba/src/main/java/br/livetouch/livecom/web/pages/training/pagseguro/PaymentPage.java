package br.livetouch.livecom.web.pages.training.pagseguro;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.web.pages.training.TrainingLogadoPage;
import net.livetouch.click.page.BorderPage;
import net.livetouch.tiger.ddd.DomainException;

/**
 * @author Ricardo Lecheta
 * @created 20/03/2013
 */
@Controller
@Scope("prototype")
public class PaymentPage extends BorderPage {

	@Autowired
	protected LogService logService;
	
	@Override
	public String getTemplate() {
		UserInfoVO userInfo = UserInfoVO.getHttpSession(getContext());
		if(userInfo == null) {
			return TrainingLogadoPage.TEMPLATE;
		}
		return TrainingLogadoPage.TEMPLATE;
	}
	
	protected LogSistema logError(String codigo, DomainException e, Empresa empresa) {
		LogSistema l = LogSistema.logError(null, e);
		l.setCodigo(codigo);
		logService.saveOrUpdate(l, empresa);
		
		return l;
	}
	
	public String getRequestParams() {
		HttpServletRequest request = getContext().getRequest();
		StringBuffer sb = null;
		Map<String, String[]> parameterMap = request.getParameterMap();
		for (String key : parameterMap.keySet()) {
			if (sb != null) {
				sb.append("&");
			} else {
				sb = new StringBuffer();
			}
			String value = request.getParameter(key);
			if ("password".equals(key)) {
				value = "***";
			}
			sb.append(key).append("=").append(value);
		}

		// String s = null;
		// s.length();
		if(sb != null) {
			return sb.toString();
		}
		return "";
	}
}