package br.livetouch.livecom.domain.repository;

import java.util.List;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.MenuPerfil;
import br.livetouch.livecom.domain.Perfil;
import net.livetouch.tiger.ddd.DomainException;
import net.livetouch.tiger.ddd.repository.Repository;

public interface MenuPerfilRepository extends Repository<MenuPerfil> {

	void removeIn(List<MenuPerfil> menus) throws DomainException;

	List<MenuPerfil> findPaiByPerfil(Perfil perfil, Empresa empresa, boolean in);

	List<MenuPerfil> findFilhoByPerfil(Perfil perfil, Empresa empresa, boolean in);
	
	List<MenuPerfil> findByPerfil(Perfil p, Empresa e);

	void removeNotIn(List<MenuPerfil> menus, Perfil perfil) throws DomainException;

}