package br.livetouch.livecom.web.pages.training;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.ServletUtil;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.LogonPage;
import br.livetouch.livecom.web.pages.admin.LivecomPage;

@Controller
@Scope("prototype")
public class LoginPage extends LogonPage {
	
	@Override
	public String getTemplate() {
		return TrainingLogadoPage.TEMPLATE;
	}
	
	@Override
	public void onInit() {
		super.onInit();
		
		TrainingUtil.setTrainingStyle(form);
	}
	
	@Override
	public void onPost() {
		super.onPost();
	}
	
	@Override
	protected boolean loginOk(Usuario u) {
		
		setRedirect(IndexPage.class);
		
		return true;
	}
	
	public static void redirect(LivecomPage page) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("from",page.getPath());
		HttpServletRequest request = page.getContext().getRequest();
		String s = ServletUtil.getRequestParams(request);
		if (StringUtils.isNotEmpty(s)) {
			params.put("fromParams", s);
		}
		page.setRedirect(LoginPage.class,params);
	}
}