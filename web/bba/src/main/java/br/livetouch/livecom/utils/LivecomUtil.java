package br.livetouch.livecom.utils;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.files.FileManagerFactory;

public class LivecomUtil {

	public static String getUrlFotoUsuario(ParametrosMap map, String file) {
		try {
			if (StringUtils.isEmpty(file)) {
				return FileManagerFactory.getFileManager(map).getFileUrl("Fotos", "usuario.png");
			} else {
				if (LivecomUtil.isUrl(file)) {
					return FileManagerFactory.getFileManager(map).getFileUrl("Fotos", file);
				} else {
					return file;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String gerarSenha(Long empresaId) {
		ParametrosMap p = ParametrosMap.getInstance(empresaId);
		String on = p.get(Params.USER_SENHA_PADRAO_ON, "0");
		String senha = p.get(Params.USER_SENHA_PADRAO, "abc123");
		if("1".equals(on)) {
			if (StringUtils.isNotEmpty(senha)) {
				return senha;
			}
			return "abc123";
		} else {
			int length = p.getInt(Params.USER_SENHA_RANDOM_SIZE,6);
			senha = createRandomPassword(length);
			return senha;
		}
	}

	public static boolean hasSenhaPadrao(Empresa empresa) {
		Long empresaId = empresa.getId();
		String senha = ParametrosMap.getInstance(empresaId).get(Params.USER_SENHA_PADRAO);
		if (StringUtils.isNotEmpty(senha)) {
			return true;
		}
		return false;
	}

	public static String getUrlFotoUsuario(ParametrosMap map, String file, Usuario user) {

		try {
			if (StringUtils.isEmpty(file)) {
				return FileManagerFactory.getFileManager(map).getFileUrl("Fotos", "usuario.png");
			} else {
				return FileManagerFactory.getFileManager(map).getFileUrl(user.getChave() + "/fotoProfile", file);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean isUrl(String url) {
		String[] schemes = { "http", "https", "ftp" }; // DEFAULT schemes =
														// "http", "https",
														// "ftp"
		UrlValidator urlValidator = new UrlValidator(schemes);
		if (urlValidator.isValid(url)) {
			return true;
		}
		return false;
	}

	/**
	 * Se tamanho for 6, vai criar 3 letras, 3 numeros.
	 * 
	 * Ex: a2b3d4
	 * 
	 * @return
	 */
	public static String createRandomPassword(int length) {

		char[] letras;
		char[] numeros;

		// array de numeros
		StringBuilder tmp = new StringBuilder();
		for (char ch = '0'; ch <= '9'; ++ch) {
			tmp.append(ch);
		}
		numeros = tmp.toString().toCharArray();
		
		// array de caracteres
		tmp = new StringBuilder();
		for (char ch = 'a'; ch <= 'z'; ++ch) {
			tmp.append(ch);
		}
		letras = tmp.toString().toCharArray();

		// gera senha random
		Random random = new Random();
		char[] buf = new char[length];
		
		// Cria a senha com letra + numero (6 vezes)
		for (int i = 0; i < length; i+=2) {
			buf[i] = letras[random.nextInt(letras.length)];
			buf[i+1] = numeros[random.nextInt(numeros.length)];
		}
		
		String pwd = new String(buf);
		return pwd;
	}

	public static void main(String[] args) {
		for (int i = 0; i < 50; i++) {
			String pwd = LivecomUtil.createRandomPassword(6);
			System.out.println(pwd);
		}
	}
	
	public File writeToFile(String csv, String name, String charset) throws IOException {
		File file = getTmpFile(name);
		FileUtils.writeStringToFile(file, csv,charset);
		System.out.println(file);
		return file;
	}

	public static File getTmpFile(String name) {
		File tmpDir = getTmpDir();
		File file = new File(tmpDir, name);
		return file;
	}

	public static  File getTmpDir() {
		return new File(System.getProperty("java.io.tmpdir"));
	}

	public static String getUrlFotoCapa(ParametrosMap map, String file) {
		try {
			if (StringUtils.isEmpty(file)) {
				return FileManagerFactory.getFileManager(map).getFileUrl("Fotos", "capa.png");
			} else if (LivecomUtil.isUrl(file)) {
				return FileManagerFactory.getFileManager(map).getFileUrl("Fotos", file);
			}else {
				return file;
			}
		} catch (Exception e) {
			return null;
		}
	}
}