package br.livetouch.livecom.connector.login;

import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

/**
 * Connector para validar a senha de um usuário em um sistema externo.
 * 
 * Default é logar no banco de dados do Livecom.
 * 
 * @author rlech
 *
 */
public interface LoginConnector {

	public boolean validate(Usuario user, String password) throws DomainException;
}
