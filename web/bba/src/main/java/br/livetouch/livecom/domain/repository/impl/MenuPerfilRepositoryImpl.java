package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.MenuPerfil;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.repository.MenuPerfilRepository;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public class MenuPerfilRepositoryImpl extends LivecomRepository<MenuPerfil> implements MenuPerfilRepository {

	public MenuPerfilRepositoryImpl() {
		super(MenuPerfil.class);
	}

	@Override
	public void removeIn(List<MenuPerfil> menus) throws DomainException {

		if(menus.isEmpty()) {
			return;
		}
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("delete from MenuPerfil mp where 1=1 ");
		
		sb.append(" and mp in (:menus)");
		
		Query q = createQuery(sb.toString());
		
		q.setParameterList("menus", menus);
		
		q.executeUpdate();
		
	}

	@Override
	public void removeNotIn(List<MenuPerfil> menus, Perfil perfil) throws DomainException {
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("delete from MenuPerfil mp where 1=1 ");
		
		if(!menus.isEmpty()) {
			sb.append(" and mp not in (:menus)");
		}

		sb.append(" and mp.perfil = :perfil ");
		
		Query q = createQuery(sb.toString());
		
		if(!menus.isEmpty()) {
			q.setParameterList("menus", menus);
		}

		q.setParameter("perfil", perfil);
		
		q.executeUpdate();
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MenuPerfil> findPaiByPerfil(Perfil p, Empresa e, boolean in) {
		StringBuffer sb = new StringBuffer("SELECT distinct mp FROM MenuPerfil mp left join mp.menu m WHERE 1=1 ");
		
		if(p != null) {
			if(in) {
				sb.append(" and mp.perfil = :perfil");
			} else {
				sb.append(" and m.id not in (select distinct menu.id from MenuPerfil where perfil = :perfil and menu.empresa = :empresa)");
			}
		}

		sb.append(" and m.parent is null");
		 
		sb.append(" group by m ");
		sb.append(" order by mp.ordem ");
		
		Query q = createQuery(sb.toString());
		
		if(p != null) {
		}
		
		if(p != null) {
			q.setParameter("perfil", p);
			if(!in) {
				q.setParameter("empresa", e);
			}
		}
		
		List<MenuPerfil> list = q.list();
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MenuPerfil> findFilhoByPerfil(Perfil p, Empresa e, boolean in) {
		
		StringBuffer sb = new StringBuffer("SELECT distinct mp FROM MenuPerfil mp left join mp.menu m WHERE 1=1 ");
		
		if(p != null) {
			if(in) {
				sb.append(" and mp.perfil = :perfil");
			} else {
				sb.append(" and m.id not in (select distinct menu.id from MenuPerfil where perfil = :perfil and menu.empresa = :empresa)");
			}
		}
		
		sb.append(" and m.parent is not null");

		sb.append(" group by m ");
		sb.append(" order by mp.ordem ");
		
		Query q = createQuery(sb.toString());
		
		if(p != null) {
		}
		
		if(p != null) {
			q.setParameter("perfil", p);
			if(!in) {
				q.setParameter("empresa", e);
			}
		}
		
		List<MenuPerfil> list = q.list();
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MenuPerfil> findByPerfil(Perfil p, Empresa e) {
		StringBuffer sb = new StringBuffer("SELECT distinct mp FROM MenuPerfil mp WHERE 1=1 ");
		
		if(p != null) {
			sb.append(" and mp.perfil = :perfil ");
		}
		
		if(e != null) {
			sb.append(" and mp.menu.empresa = :empresa");
		}
		 
		sb.append(" order by mp.ordem ");
		
		Query q = createQuery(sb.toString());
		
		if(p != null) {
			q.setParameter("perfil", p);
		}
		
		if(e != null) {
			q.setParameter("empresa", e);
		}
		
		List<MenuPerfil> list = q.list();
		return list;
		
	}

}
