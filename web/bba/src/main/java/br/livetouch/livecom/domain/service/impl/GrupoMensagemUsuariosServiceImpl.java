/*package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.GrupoMensagemUsuarios;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class GrupoMensagemUsuariosServiceImpl implements GrupoMensagemUsuariosService {
	
	@Autowired
	private GrupoMensagemUsuariosRepository rep;

	@Override
	public GrupoMensagemUsuarios get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(GrupoMensagemUsuarios c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public void delete(GrupoMensagemUsuarios gu) throws DomainException {
		rep.delete(gu);
	}
	
	@Override
	public GrupoMensagemUsuarios findByGrupoEUsuario(GrupoMensagem g, Usuario u) {
		return rep.findByGrupoEUsuario(g, u);
	}

	@Override
	public List<GrupoMensagemUsuarios> adminsByGrupo(GrupoMensagem g) {
		return rep.adminsByGrupo(g);
	}
	
	@Override
	public List<GrupoMensagemUsuarios> usersByGrupo(GrupoMensagem g) {
		return rep.usersByGrupo(g);
	}

	@Override
	public int getUltimaInsercao(GrupoMensagem g) {
		return rep.getUltimaInsercao(g);
	}

	@Override
	public GrupoMensagemUsuarios getByOrdemGrupo(GrupoMensagem g, int ordem) {
		return rep.getByOrdemGrupo(g,ordem);
	}
}
*/