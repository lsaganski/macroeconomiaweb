package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.StatusChat;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.StatusChatRepository;
import br.livetouch.livecom.domain.service.StatusChatService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class StatusChatServiceImpl implements StatusChatService {
	@Autowired
	private StatusChatRepository rep;

	@Override
	public StatusChat get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(StatusChat c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public void delete(StatusChat c) throws DomainException {
		rep.delete(c);
	}
	
	@Override
	public List<StatusChat> findAll() {
		return rep.findAll(true);
	}

	@Override
	public List<StatusChat> findAll(Usuario user) {
		return rep.findAll(user);
	}
	
	@Override
	public List<StatusChat> getStatusUsuario() {
		return rep.getStatusUsuario();
	}
}
