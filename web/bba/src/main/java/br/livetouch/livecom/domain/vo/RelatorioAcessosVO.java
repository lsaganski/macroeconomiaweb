package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;

import net.livetouch.extras.util.DateUtils;

public class RelatorioAcessosVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2353343218707587848L;
	
	private Date data;
	private Long logados;
	private Long distinctLogados;
	private String date;
	private Long count;
	
	public RelatorioAcessosVO(String d, Long logados, Long distinctLogados) {
		Date data = DateUtils.toDate(d);
		this.setData(data);
		this.setLogados(logados);
		this.setDistinctLogados(distinctLogados);
	}
	
	public RelatorioAcessosVO(Long count) {
		this.setCount(count);
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Long getLogados() {
		return logados;
	}

	public void setLogados(Long logados) {
		this.logados = logados;
	}

	public Long getDistinctLogados() {
		return distinctLogados;
	}

	public void setDistinctLogados(Long distinctLogados) {
		this.distinctLogados = distinctLogados;
	}
	
	public String getDate() {
		return date = DateUtils.toString(data, "dd/MM/yyyy");
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

}
