package br.livetouch.livecom.web.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import br.livetouch.livecom.web.pages.training.TrainingLogadoPage;

@Controller
@Scope("prototype")
public class IndexPage extends LivecomLogadoPage {

	@Override
	public void onInit() {

		if(isBuildBJJ()) {
			setPath("index_bjj.htm");
//			posts = postService.findLasts(4);
//			setIncludeHeader(true);
		} else {
			forward(MuralPage.class);
		}
		
	}

	public String getTemplate() {
		if(isBuildBJJ()) {
			return TrainingLogadoPage.TEMPLATE;
		} else {
			return super.getTemplate();
		}
	}
}
