package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.vo.ProfileVO.Type;

public class CampoUsersVO implements Serializable {

	private static final long serialVersionUID = 2215729727494456226L;

	private String key;
	private String value;
	private Type type;
	
	public CampoUsersVO() {
	}
	
	public CampoUsersVO(String key, String value, Type type) {
		super();
		this.key = key;
		this.value = value;
		this.type = type;
	}

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	
}
