package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import br.livetouch.livecom.domain.Card;
import br.livetouch.livecom.domain.CardButton;
import br.livetouch.livecom.domain.repository.CardButtonRepository;
import br.livetouch.livecom.domain.repository.CardRepository;
import br.livetouch.livecom.domain.service.CardService;

@org.springframework.stereotype.Service
public class CardServiceImpl implements CardService{

	@Autowired 
	CardRepository rep;
	
	@Autowired
	CardButtonRepository repButton;
	
	@Override
	public Card get(Long id) {
		return rep.get(id);
	}
	
	@Override
	public void saveOrUpdate(Card card) {
		rep.saveOrUpdate(card);
	}
	
	public void delete(Card card) {
		rep.delete(card);
	}
	
	public List<Card> findAll() {
		return rep.findAll();
	}


	@Override
	public void saveOrUpdate(CardButton cardButton) {
		repButton.saveOrUpdate(cardButton);
	}
}
