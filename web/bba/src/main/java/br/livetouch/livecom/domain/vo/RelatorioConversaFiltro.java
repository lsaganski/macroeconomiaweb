package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;

import javax.ws.rs.QueryParam;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.extras.util.DateUtils;

/**
 * Filtro com todos os campos de LogSistema, mas a dataInicial e dataFinal
 * 
 * @author ricardo
 *
 */
public class RelatorioConversaFiltro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2120316871641764085L;

	public static final String SESSION_FILTRO_KEY = "relatorioConversaFiltro";

	private Date dataInicial;
	private Date dataFinal;
	private int page;
	private int max;
	@QueryParam("usuario")
	private Long usuario;
	@QueryParam("grupo")
	private Long grupo;
	private String termo;
	@QueryParam("id")
	private Long id;
	private Usuario user;
	private Grupo group;
	@QueryParam("mensagemId")
	private Long mensagemId;
	
	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public String getDataInicialStringDMY() {
		return DateUtils.toString(dataInicial, "dd/MM/yyyy");
	}

	public String getDataFinalStringDMY() {
		return DateUtils.toString(dataFinal, "dd/MM/yyyy");
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public Long getUsuario() {
		return usuario;
	}

	public void setUsuario(Long usuario) {
		this.usuario = usuario;
	}

	public String getTermo() {
		return termo;
	}

	public void setTermo(String termo) {
		this.termo = termo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGrupo() {
		return grupo;
	}

	public void setGrupo(Long grupo) {
		this.grupo = grupo;
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

	public Grupo getGroup() {
		return group;
	}

	public void setGroup(Grupo group) {
		this.group = group;
	}

	public Long getMensagemId() {
		return mensagemId;
	}

	public void setMensagemId(Long mensagemId) {
		this.mensagemId = mensagemId;
	}

}
