package br.livetouch.livecom.domain.service.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.internal.util.SerializationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.Menu;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.MenuRepository;
import br.livetouch.livecom.domain.service.MenuService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class MenuServiceImpl extends LivecomService<Menu> implements MenuService {

    @Autowired
    private MenuRepository rep;

    @Override
    public Menu get(Long id) {
        return rep.get(id);
    }

    @Override
    public void saveOrUpdate(Menu c) throws DomainException {
        rep.saveOrUpdate(c);
    }

    @Override
    public List<Menu> findAll(Empresa e) {
        return rep.findAll(e);
    }

    @Override
    public List<Menu> findAllDefault() {
        return rep.findAllDefault();
    }

    @Override
    public void delete(Menu m, Usuario user) throws DomainException {
        try {
            rep.delete(m);
        } catch (DataIntegrityViolationException e) {
            Throwable root = ExceptionUtils.getRootCause(e);
            if (root == null) {
                root = e;
            }
            log.error("Erro ao excluir o menu [" + m + "]: " + root.getMessage(), root);

            LogSistema.logError(user, e);

            throw e;
        } catch (Exception e) {
            Throwable root = ExceptionUtils.getRootCause(e);
            if (root == null) {
                root = e;
            }
            String msg = "Erro ao excluir o menu [" + m + "]: " + root.getMessage();
            log.error(msg, root);
            LogSistema.logError(user, e);

            throw e;
        }
    }

    @Override
    public List<Menu> findByLabel(String label, Empresa e) {
        return rep.findByLabel(label, e);
    }

    @Override
    public List<Menu> findPaiByLabel(String label, Empresa e) {
        return rep.findPaiByLabel(label, e);
    }

    @Override
    public List<Menu> findByPerfil(Perfil p, Empresa e) {
        return rep.findByPerfil(p, e);
    }

    @Override
    public List<Menu> findMenusNotInPerfil(Perfil p, Empresa e) {
        return rep.findMenusNotInPerfil(p, e);
    }

    @Override
    public List<Menu> findPaiByPerfil(Perfil perfil, Empresa empresa, boolean in) {
        return rep.findPaiByPerfil(perfil, empresa, in);
    }

    @Override
    public List<Menu> findFilhoByPerfil(Perfil perfil, Empresa empresa, boolean in) {
        return rep.findFilhoByPerfil(perfil, empresa, in);
    }

    @Override
    public List<Menu> findFilhos(Menu m) {
        return rep.findFilhos(m);
    }

    @Override
    public List<Menu> paisByPerfil(Perfil p, Empresa e) {
        return rep.paisByPerfil(p, e);
    }

    @Override
    public List<Menu> filhosByPerfil(Perfil p, Empresa e) {
        return rep.filhosByPerfil(p, e);
    }

    @Override
    public List<Menu> findAllMobile(Empresa empresa) throws DomainException {
        List<Menu> menu = rep.findAllMobile(empresa);
        if(menu == null || menu.size() == 0) {
            restoreMenu(empresa);
            menu = findAllMobile(empresa);
        }
        return menu;
    }

    @Override
    public List<Menu> findMenuMobileDefault() {
        return rep.findMenuMobileDefault();
    }

    @Override
    public void deleteFromEmpresa(Empresa empresa) throws DomainException {
        rep.deleteFromEmpresa(empresa);
    }

    @Override
    public void restoreMenu(Empresa empresa) throws DomainException {
        List<Menu> menu = findMenuMobileDefault();
        if (menu == null || menu.size() == 0) {
            throw new DomainException("Menu padrão não encontrado. Contate o Admin do sistema");
        }
        delete(empresa);
        cloneDefaultMenu(empresa);
    }

    @Override
    public void cloneDefaultMenu(Empresa empresa) throws DomainException {
        List<Menu> menu = findMenuMobileDefault();
        for (Menu m : menu) {
            Menu clone = (Menu) SerializationHelper.clone((Serializable) m);
            clone.setId(null);
            clone.setEmpresa(empresa);
            saveOrUpdate(clone);
        }
    }

    @Override
    public void delete(Empresa empresa) {
        rep.delete(empresa);
    }


    @Override
    public void removeDefaultMenuMobile(Empresa empresa) {
        rep.removeDefaultMenuMobile(empresa);
    }

	@Override
	public void generateMenus(Empresa e) throws DomainException {
		List<Menu> menus = rep.findAllDefault();
		
		Map<Long, Menu> map = new HashMap<Long, Menu>();
		
		for (Menu m : menus) {
			Menu menu = (Menu) SerializationHelper.clone((Serializable) m); 
			menu.setId(null);
			menu.setEmpresa(e);
			
			Menu parent = m.getParent();
			if(parent == null) {
				saveOrUpdate(menu);
				map.put(m.getId(), menu);
			} else {
				Menu menuParent = map.get(parent.getId());
				menu.setParent(menuParent);
				saveOrUpdate(menu);
			}
			
		}
		
	}
}
