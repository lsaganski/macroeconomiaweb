package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostTask;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.PostTaskRepository;

@Repository
public class PostTaskRepositoryImpl extends LivecomRepository<PostTask> implements PostTaskRepository {

	public PostTaskRepositoryImpl() {
		super(PostTask.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PostTask> findAllByUser(Usuario u) {
		StringBuffer sb = new StringBuffer("from PostTask pt where pt.usuario.id=?");

		Query q = createQuery(sb.toString());

		q.setLong(0, u.getId());
		List<PostTask> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PostTask findByUserPost(Usuario u, Post p) {
		StringBuffer sb = new StringBuffer("from PostTask pt where pt.usuario = :usuario and pt.post = :post");

		Query q = createQuery(sb.toString());

		q.setParameter("usuario", u);
		q.setParameter("post", p);
		List<PostTask> list = q.list();
		PostTask v = list.size() > 0 ? list.get(0) : null;
		return v;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PostTask> findAllByPost(Post post) {
		StringBuffer sb = new StringBuffer("from PostTask pt where pt.post.id=?");

		Query q = createQuery(sb.toString());

		q.setLong(0, post.getId());
		List<PostTask> list = q.list();
		return list;
	}
	
}