package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.repository.LogAuditoriaRepository;
import br.livetouch.livecom.domain.service.LogAuditoriaService;
import br.livetouch.livecom.domain.vo.RelatorioEnvioPostVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class LogAuditoriaServiceImpl implements LogAuditoriaService {
	protected static final Logger log = Log.getLogger(LogAuditoriaService.class);

	@Autowired
	private LogAuditoriaRepository rep;

	@Override
	public LogAuditoria get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(LogAuditoria audi) {
		rep.saveOrUpdate(audi);
	}

	@Override
	public List<LogAuditoria> reportAuditoria(RelatorioFiltro filtro) throws DomainException {
		return rep.reportAuditoria(filtro);
	}

	@Override
	public List<RelatorioEnvioPostVO> envioPosts(RelatorioFiltro filtro) throws DomainException {
		List<LogAuditoria> envioPosts = rep.envioPosts(filtro);
		List<RelatorioEnvioPostVO> envios = new ArrayList<>();
		if(envioPosts != null && envioPosts.size() > 0) {
				for (LogAuditoria e : envioPosts) {
					if(e.getIdEntity() != null) {
						LogAuditoria log = rep.findFirstReport(e);
						envios.add(new RelatorioEnvioPostVO(e, log));
					}
				}
		}
		return envios;
	}

	@Override
	public Long countEnvioPosts(RelatorioFiltro filtro) throws DomainException {
		Long count = rep.countEnvioPosts(filtro);
		return count;
	}

	@Override
	public String csvEnvioPostReport(RelatorioFiltro filtro) throws DomainException {
		filtro.setPage(0);
		filtro.setMax(-1);
		List<RelatorioEnvioPostVO> posts = envioPosts(filtro);
		if(posts != null && posts.size() > 0 ) {
			CSVGenerator generator = new CSVGenerator();
			CSVHeader header = generator.getHeader();
			CSVBody body = generator.getBody();
			header
				.add("Data Cadastro")
				.add("Autor")
				.add("Titulo")
				.add("Conteúdo")
				.add("Categorias")
				.add("Grupos")
				.add("Data Atualização")
				.add("Autor Atualização")
				.add("Titulo Atualizado")
				.add("Conteudo Atualizado")
				.add("Categorias Atualizadas")
				.add("Grupos Atualizados");
			for (RelatorioEnvioPostVO r : posts) {
				String dataPublicacao = StringUtils.replace(r.getDataPublicacao(), ";", ",");
				String autor = StringUtils.replace(r.getUser(), ";", ",");
				String titulo = StringUtils.replace(r.getTitulo(), ";", ",");
				String categorias = StringUtils.replace(r.getCategorias(), ";", ",");
				String grupos = StringUtils.replace(r.getGrupos(), ";", ",");
				String mensagem = StringUtils.replace(r.getMensagem(), ";", ",");
				String dataUpdate = StringUtils.replace(r.getDataUpdate(), ";", ",");
				String autorUpdate = StringUtils.replace(r.getUserUpdate(), ";", ",");
				String tituloOriginal = StringUtils.replace(r.getTituloOriginal(), ";", ",");
				String conteudoOriginal = StringUtils.replace(r.getConteudoOriginal(), ";", ",");
				String categoriasAtualizadas = StringUtils.replace(r.getCategoriasUpdate(), ";", ",");
				String gruposAtualizados = StringUtils.replace(r.getGruposUpdate(), ";", ",");
				body
					.add(StringUtils.defaultString(StringUtils.normalizeSpace(dataPublicacao)))
					.add(StringUtils.defaultString(StringUtils.normalizeSpace(autor)))
					.add(StringUtils.defaultString(StringUtils.normalizeSpace(tituloOriginal)))
					.add(StringUtils.isEmpty(conteudoOriginal) ? "" : "\"" + conteudoOriginal + "\"")
					.add(StringUtils.defaultString(StringUtils.normalizeSpace(categorias)))
					.add(StringUtils.defaultString(StringUtils.normalizeSpace(grupos)))
					.add(StringUtils.defaultString(StringUtils.normalizeSpace(dataUpdate)))
					.add(StringUtils.defaultString(StringUtils.normalizeSpace(autorUpdate)))
					.add(StringUtils.defaultString(StringUtils.normalizeSpace(titulo)))
					.add(StringUtils.isEmpty(mensagem) ? "" : "\"" + mensagem + "\"")
					.add(StringUtils.defaultString(StringUtils.normalizeSpace(categoriasAtualizadas)))
					.add(StringUtils.defaultString(StringUtils.normalizeSpace(gruposAtualizados)))
					.end();
			}
			return generator.toString();
		}
		return null;
	}

	@Override
	public List<LogAuditoria> getAuditoriasByEntity(Long id, AuditoriaEntidade entidade) {
		return rep.getAuditoriasByEntity(id, entidade);
	}
}