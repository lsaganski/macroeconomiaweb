package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Funcao;

public class FuncaoVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Long id;
	public String nome;
	public String codigo;
	
	public FuncaoVO(Funcao f) {
		this.id = f.getId();
		this.nome = f.getNome();
		this.codigo = f.getCodigo();
	}

	public FuncaoVO() {
		
	}
	
	public static List<FuncaoVO> toListVO(List<Funcao> funcoes) {
		
		List<FuncaoVO> list = new ArrayList<FuncaoVO>();
		
		for (Funcao funcao : funcoes) {
			FuncaoVO vo = new FuncaoVO(funcao);
			list.add(vo);
		}
		
		return list;
	}
	
}
