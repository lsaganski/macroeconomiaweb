package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.UsuarioChatVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class GetUsersGrupoMensagemPage extends WebServiceFormPage {
	
	private UsuarioLoginField tUser;
	public Long grupo_id;


	@Override
	protected void form() {
		form.add(tUser = new UsuarioLoginField("user", true,usuarioService, getEmpresa()));
		form.add(new LongField("grupo_id", true));
	}

	@SuppressWarnings("null")
	@Override
	protected Object go() throws Exception {
		if (form.isValid()) {
			Usuario userRequest = tUser.getEntity();
			
			Grupo grupo = grupoService.get(grupo_id);
			
			if (grupo == null) {
				return new MensagemResult("NOK" ,"Grupo inválido");
			}
			
			List<Usuario> usuarios = grupoService.findUsuarios(grupo);
			
			boolean canAccess = false;
			
			for (Usuario u : usuarios) {
				if (u.getId().equals(userRequest.getId())){
					canAccess = true;
					break;
				}
			}
			
			if (!canAccess) {
				return new MensagemResult("NOK" ,"Você não tem acesso a esse grupo");
			}
			
			if (usuarios == null || usuarios.size() ==0) {
				return new MensagemResult("NOK" ,"Grupo não contem usuários");
			}
			
			List<UsuarioChatVO> list = UsuarioChatVO.toList(grupo, usuarios);
			
			if (list == null) {
				return new MensagemResult("NOK" ,"Erro ao procurar usuários");
			} else {
				return list;	
			}	
		}
		
		return new MensagemResult("NOK" ,"Grupo inválido");
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("usuarios", UsuarioVO.class);
		super.xstream(x);
	}
}
