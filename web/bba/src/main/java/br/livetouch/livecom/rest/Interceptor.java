package br.livetouch.livecom.rest;

import java.io.IOException;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang.StringUtils;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.RequestMap;
import br.livetouch.livecom.rest.resource.MainResource;
import br.livetouch.livecom.security.LivecomSecurity;

@Provider
public class Interceptor extends MainResource implements ContainerRequestFilter {
	 
	public RequestMap requestMap = RequestMap.getInstance();

	@Override
	public void filter(ContainerRequestContext req) throws IOException {
		
		boolean securityAttackReplay = ParametrosMap.getInstance(getEmpresa()).getBoolean(Params.SECURITY_ATTACK_REPLAY_ON, false);
		
		if(securityAttackReplay) {
			if(StringUtils.equalsIgnoreCase(req.getMethod(), HttpMethod.POST)) {
				String nonce = (String) req.getProperty(LivecomSecurity.NONCE_TOKEN_PARAM);
				if(StringUtils.isEmpty(nonce)) {
					throw new WebApplicationException(Status.UNAUTHORIZED);
				}
				
				String requestToken = requestMap.get(nonce);
				
				if(StringUtils.isNotEmpty(requestToken)) {
					throw new WebApplicationException(Status.UNAUTHORIZED);
				} else {
					requestMap.put(nonce, nonce);
				}
			}
		}
		
	}
}

