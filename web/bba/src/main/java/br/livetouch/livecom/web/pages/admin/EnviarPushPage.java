package br.livetouch.livecom.web.pages.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.GrupoUsuariosVO;
import br.livetouch.livecom.jobs.EmailMarketingJob;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.livetouch.click.control.IdField;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.HiddenField;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class EnviarPushPage extends LivecomAdminPage {
	protected static final Logger logger = Log.getLogger(EnviarPushPage.class);

	public Form form = new Form();
	public Long id;
	public List<GrupoUsuariosVO> gruposUsuarios;
	private TextField tAssunto;
	
	public boolean todos;
	
	public TemplateEmail templateEmail;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;

		form();
		
		/*if (id != null) {
			templateEmail = templateEmailService.get(id);
		} else {
			setMessageError("Template não encontrado");
			setRedirect(TemplatesPage.class);
		}*/
	}
	
	public void form() {
		form.setMethod("post");

		form.add(new IdField());
		form.add(new HiddenField("grupos_ids",String.class));
		form.add(new HiddenField("usuarios_ids",String.class));
		form.add(new HiddenField("todos",Boolean.class));

		TextField tNome = new TextField("nome", getMessage("nome.label"), true);
		tNome.setAttribute("class", "mascara");
		tNome.setAttribute("rel", "Buscar Grupo");
		tNome.setAttribute("autocomplete", "off");
		tNome.setFocus(false);
		form.add(tNome);

		tAssunto = new TextField("assunto", "Assunto", true);
		tAssunto.setAttribute("class","form-control");
		tAssunto.setAttribute("placeholder","Assunto do e-mail");
		tAssunto.setAttribute("maxlength", "150");
		tAssunto.setFocus(true);
		form.add(tAssunto);

		Submit tconsultar = new Submit("enviar", getMessage("enviar.label"), this, "enviar");
		tconsultar.setAttribute("class", "botao btSalvar");
		form.add(tconsultar);
	}

	@Override
	public void onPost() {
		
		enviar();
	}
	
	public boolean enviar() {
		try {
			List<Long> grupos_ids = getListIds("grupos_ids");
			List<Long> usuarios_ids = getListIds("usuarios_ids");
			
			if(todos) {
				usuarios_ids = usuarioService.findAllIds(getEmpresa());
			}
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("grupos_ids", grupos_ids);
			params.put("usuarios_ids", usuarios_ids);
			params.put("contextPath", getContextPath());
			params.put("userInfo", getUsuario());
			params.put("templateId", id);
			params.put("subject", tAssunto.getValue());
			params.put("empresa", getEmpresa());
			
			int count = enviarEmailParaGrupos(grupos_ids);
			count += enviarEmailParaUsuarios(usuarios_ids);

			executeJob(EmailMarketingJob.class, params);

			msg = "Envio de e-mail disparado com sucesso para ["+count+"] usuários.";
			setFlashAttribute("id", String.valueOf(id));

			logger.debug(msg);
			
		} catch (Exception e) {
			logError(e.getMessage(), e);
			msgError = e.getMessage();
			setRedirect(TemplatesPage.class);
		}

		return true;
	}

	private int enviarEmailParaUsuarios(List<Long> ids) throws DomainException {
		int count = 0;
		if(ids != null) {
			List<Usuario> usuarios = usuarioService.findAllByIds(ids);
			count = usuarios.size();
		}
		return count;
	}

	private int enviarEmailParaGrupos(List<Long> ids) throws DomainException {
		int count = 0;
		if(ids != null) {
			List<Grupo> grupos = grupoService.findAllByIds(ids);
			for (Grupo g : grupos) {
				List<Usuario> usuarios = usuarioService.findUsuariosByGrupo(g,0,0);
				count += usuarios.size();
			}
		}
		return count;
	}

	@Override
	public void onRender() {
		super.onRender();
		
		gruposUsuarios = grupoService.findGrupoUsuariosByUser(getUsuario());
		
	}
}
