package br.livetouch.livecom.connector.login;

import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.exception.SenhaInvalidaException;

@Service
public class DefaultLoginConnector implements LoginConnector {

	public boolean validate(Usuario user, String pwd) throws SenhaInvalidaException {
		boolean ok = user.validaSenha(pwd);
		return ok;
	}

}
