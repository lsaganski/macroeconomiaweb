package br.livetouch.livecom.ldap;

import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;

public class LDAPUser {

	private SearchResult result;
	private String searchBase;

	public LDAPUser(SearchResult r, String searchBase) {
		super();
		this.result = r;
		this.searchBase = searchBase;
	}
	
	public SearchResult getResult() {
		return result;
	}

	public String getName() {
		return result != null ? result.getName() : null;
	}

	public String getNameInNamespace() {
		return result != null ? result.getNameInNamespace() : null;
	}
	
	public String get(String key) {
		if(result != null) {
			Attributes attrs = result.getAttributes();
			Attribute value = attrs.get(key);
			return value.toString();
		}
		return null;
	}
	
	public Attributes geAttrs() {
		return result != null ? result.getAttributes() : null;
	}
	
	/**
	 * uid=rlecheta,ou=tecnologia,dc=livetouchdev,dc=com,dc=br
	 * CN=Jaime Peres Servidone Nagase,OU=Users,OU=Alphaville,OU=Sites,DC=visanet,DC=corp
	 * 
	 * @return
	 */
	public String getUserDN() {
		String name = getName();
		String dn = name+","+searchBase;
		return dn;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("UserDN: " + getUserDN());
		if(result != null) {
			sb.append(", Attrs: " + geAttrs());
		}
		return sb.toString();
	}
}
