package br.livetouch.livecom.domain.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.CategoriasMap;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.CategoriaVO;
import net.livetouch.tiger.ddd.DomainException;

public interface CategoriaPostService extends Service<CategoriaPost> {

	List<CategoriaPost> findAll(Empresa empresa);
	List<CategoriaVO> findByIdioma(Empresa empresa, Idioma idioma);
	
	List<CategoriaPost> findAllParent(Empresa empresa);

	List<CategoriaPost> findAllParentByIdioma(Empresa empresa, Idioma idiomas);

	List<CategoriaPost> findAllParentByIdioma(Empresa empresa, List<Idioma> idiomas);
	
	List<CategoriaPost> findAllChildren(Empresa empresa);
	
	List<CategoriaPost> findAllFetchChildren(Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Usuario userInfo,CategoriaPost c) ;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo,CategoriaPost c) throws DomainException ;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo, CategoriaPost c, boolean force) throws DomainException ;

	List<CategoriaPost> findAllDefault(Empresa empresa) ;
	void updateOrdemByIds(Usuario user, String ids) ;
	List<CategoriaPost> findAllByNomeLikeWithMax(String nome, int max, Empresa empresa);
	String csvCategoria(Empresa empresa);
	CategoriaPost findByName(CategoriaPost categoria, Empresa empresa);

	CategoriaPost findByCodigo(Empresa empresa, String codigo);
	
	List<CategoriaPost> findMenuCategoria(Empresa e);
	void deleteArquivos(Long categoriaId, List<Long> arquivoIds);
	
	List<CategoriaPost> findByParent(CategoriaPost pai);
	List<CategoriaPost> findByParentAndIdiomas(CategoriaPost pai, List<Idioma> idiomas);
	List<CategoriaPost> findParentByIdiomas(List<Idioma> idiomas);
	List<CategoriaPost> findByParent(CategoriaVO pai);
	List<CategoriaPost> findByNameAndIdioma(String nome, Idioma idioma, Empresa empresa);

	Map<Long, Set<Long>> mountMap(Empresa empresa);

	CategoriasMap init();
	
	List<CategoriaVO> mountCategoriasVO(List<CategoriaPost> pais, Idioma i);
	List<CategoriaPost> findByIdsAndIdioma(Idioma idioma, List<Long> categoriaIds);
	
}