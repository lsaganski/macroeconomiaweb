package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.LogTransacao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.LogRepository;
import br.livetouch.livecom.domain.vo.DeviceVO;
import br.livetouch.livecom.domain.vo.LogSistemaFiltro;
import br.livetouch.livecom.domain.vo.LogTransacaoFiltro;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.spring.StringHibernateRepository;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;


@Repository
@SuppressWarnings("unchecked")
public class LogRepositoryImpl extends StringHibernateRepository<LogSistema> implements LogRepository {

	public LogRepositoryImpl() {
		super(LogSistema.class);
	}
	
	public List<LogSistema> findLogsSistema(int page, Empresa empresa) {
		int firstResult = 0;
		if (page != 0) {
			firstResult = page * LogRepository.MAX;
		}

		Query q = createQuery("FROM LogSistema log WHERE log.empresa.id = :empresaId ORDER BY id DESC");
		q.setParameter("empresaId", empresa.getId());		
		q.setFirstResult(firstResult);
		q.setMaxResults(LogRepository.MAX);

		List<LogSistema> lista = q.list();

		return lista;
	}

	public List<LogTransacao> findLogsTransacao(int page, Empresa empresa) {
		int firstResult = 0;
		if (page != 0) {
			firstResult = page * LogRepository.MAX;
		}
		Query q = createQuery("FROM LogTransacao log WHERE log.empresa.id = :empresaId ORDER BY id DESC");
		q.setLong("empresaId", empresa.getId());
		q.setFirstResult(firstResult);
		q.setMaxResults(LogRepository.MAX);

		List<LogTransacao> lista = q.list();
		return lista;
	}

	@Override
	public List<LogSistema> findbyFilter(LogSistemaFiltro filtro, int page, int max, Empresa empresa) throws DomainException {	
		Query query = getQueryFindByFilter(filtro,page,max, false, empresa);

		List<LogSistema> logs = query.list();

		return logs;
	}


	@Override
	public List<LogTransacao> findbyFilter(LogTransacaoFiltro filtro, Usuario usuarioSistema, int page, int max, Empresa empresa) throws DomainException {		
		Query query = getQueryFindByFilter(filtro,usuarioSistema,page,max, false, empresa);

		List<LogTransacao> logs = query.list();

		return logs;
	}

	@Override
	public LogTransacao getLogTransacao(Long id) {
		return (LogTransacao) getSession().get(LogTransacao.class, id);
	}

	@Override
	public LogSistema getLogSistema(Long id) {
		return (LogSistema) getSession().get(LogSistema.class, id);
	}

	@Override
	public void saveOrUpdate(LogTransacao log, Empresa empresa) {
		log.setEmpresa(empresa);
		getSession().saveOrUpdate(log);
	}

	@Override
	public void delete(LogTransacao log) {
		getSession().delete(log);
	}

	@Override
	public long getLogTransacaoMaxId(Empresa empresa) {
		Query q = createQuery("FROM max(id) WHERE log.empresa.id = :empresaId");
		q.setParameter("empresaId", empresa.getId());
		if(q.list().size() > 0) {
			return Long.parseLong(q.list().get(0).toString());
		}
		return 0;
	}

	@Override
	public LogTransacao findByOtp(Usuario usuario, String otp, Empresa empresa) {
		Query q = createQuery("from LogTransacao log where log.usuario.id = ? and log.otp=? AND log.empresa.id = :empresaId");
		q.setLong(0, usuario.getId());
		q.setString(1, otp);
		q.setLong("empresaId", empresa.getId());
		List<LogTransacao> list = q.list();
		return list.isEmpty() ? null : list.get(0);
	}

	@Override
	public long getCountByFilter(LogTransacaoFiltro filtro,Usuario usuarioSistema, Empresa empresa) throws DomainException {
		Query query = getQueryFindByFilter(filtro,usuarioSistema,-1,-1, true, empresa);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	private Query getQueryFindByFilter(LogTransacaoFiltro filtro,Usuario usuarioSistema, int page, int max, boolean count, Empresa empresa) throws DomainException  {
		Date dataInicio = null;
		Date dataFim = null;
		
		if(Utils.dataValida(filtro)){
			if(filtro.getDataInicial() != null){
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if(filtro.getDataFinal() != null){
				dataFim 	= DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		Usuario usuario = filtro.getUsuario();
		String requestPath = filtro.getRequestPath();
		String status = filtro.getStatus() != null ? filtro.getStatus().name() : null;

		StringBuffer sb = new StringBuffer();
		if(count) {
			sb.append("select count(id) from LogTransacao where 1=1 ");
		} else {
			sb.append("from LogTransacao where 1=1 ");
		}

		if(dataInicio != null){
			sb.append(" and dataInicio >= :dataInicio");
		}
		if(dataFim != null){
			sb.append(" and dataFim <= :dataFim");
		}		
		if(status != null){			
			sb.append(" and status = :status");
		}
		if(usuario != null){
			sb.append(" and (usuario = :usuario)");
		}
		if(requestPath != null){
			sb.append(" and (requestPath like :requestPath)");
		}
		if(filtro.getId()!= null){
			sb.append(" and id = :id");
		}

		sb.append( " AND empresa.id = :empresaId");
		
		if(!count) {
			sb.append(" order by id desc");
		}

		Query query = createQuery(sb.toString());

		if(dataInicio != null){
			query.setParameter("dataInicio",dataInicio);			
		}

		if(dataFim != null){
			query.setParameter("dataFim",dataFim);
		}		

		if(status != null){
			query.setString("status",status);		
		}

		if(usuario != null){
			query.setParameter("usuario", usuario);			
		}
		
		if(requestPath != null){
			query.setParameter("requestPath","%"+requestPath+"%");			
		}

		if(filtro.getId()!= null){
			query.setParameter("id", filtro.getId());
		}
		
		query.setLong("empresaId", empresa.getId());
		
		if(!count) {
			if(page != -1 && max != -1) {
				int firstResult = 0;
				if (page != 0) {
					firstResult = page * max;
				}
				query.setFirstResult(firstResult);
				query.setMaxResults(max);
			}
		}
		
		return query;
	}

	@Override
	public long getCountByFilter(LogSistemaFiltro filtro, Empresa empresa)throws DomainException {
		Query query = getQueryFindByFilter(filtro,-1,-1, true, empresa);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	private Query getQueryFindByFilter(LogSistemaFiltro filtro, int page, int max, boolean count, Empresa empresa) throws DomainException {
		Date dataInicio = null;
		Date dataFim = null;
		
		if(Utils.dataValida(filtro))
		if(filtro.getDataInicial() != null){
			dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
		}
		if(filtro.getDataFinal() != null){
			dataFim 	= DateUtils.setTimeFimDia(filtro.getDataFinal());
		}
//		else{
//			throw new DataInvalidaException("exception.data.invalida");
//		}

		Usuario u = filtro.getUsuario();
		String tipo = filtro.getTipo() != null ? filtro.getTipo().name() : null;

		StringBuffer sb = new StringBuffer();
		if(count) {
			sb.append("select count(id) from LogSistema where 1=1 ");
		} else {
			sb.append("from LogSistema where 1=1 ");
		}

		if (dataInicio != null) {
			sb.append(" and data >= :dataInicio");
		}
		if (dataFim != null) {
			sb.append(" and data <= :dataFim");
		}
		if (u != null) {
			sb.append(" and (usuario = :usuario)");
		}

		if(tipo != null) {
			sb.append(" and tipo = :tipo");
		}
		
		sb.append(" AND empresa.id = :empresaId");

		if(!count) {
			sb.append(" order by id desc");
		}

		Query query = createQuery(sb.toString());
		if (dataInicio != null) {
			dataInicio = DateUtils.setTimeInicioDia(dataInicio);
			query.setParameter("dataInicio", dataInicio);
		}
		if (dataFim != null) {
			dataFim = DateUtils.setTimeFimDia(dataFim);
			query.setParameter("dataFim", dataFim);
		}
		if (u != null) {
			query.setParameter("usuario", u);
		}

		if(tipo != null){
			query.setString("tipo",tipo);
		}		
		
		query.setLong("empresaId", empresa.getId());
		
		if(!count) {
			if(page != -1) {
				int firstResult = 0;
				if (page != 0) {
					firstResult = page * max;
				}
				query.setFirstResult(firstResult);
				query.setMaxResults(max);
			}
		}

		return query;
	}

	@Override
	public List<DeviceVO> getRegistrationIds(Empresa empresa) {
		StringBuffer sb = new StringBuffer("select new br.livetouch.livecom.domain.vo.DeviceVO (login,deviceSo,requestParams,deviceSoVersion) from LogTransacao ");
		sb.append(" where requestPath like '/ws/login.htm'");
		sb.append(" AND empresa.id = :empresaId");
		Query q = createQuery(sb.toString());
		q.setLong("empresaId", empresa.getId());
		List<DeviceVO> list = q.list();
		return list;
	}

	@Override
	public List<LogTransacao> findAllLogTransacao(Empresa empresa) {
		Query q = createQuery("from LogTransacao WHERE empresa.id = :empresaId");
		q.setLong("empresaId", empresa.getId());
		List<LogTransacao> list = q.list();
		return list;
	}

	public Query queryLogsByVersao(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException {
		Date dataInicio = null;
		Date dataFim = null;
		String versao = filtro.getVersao();

		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		if(count) {
			sb.append("select count(distinct login) ");
		} else {
			sb.append("select distinct new br.livetouch.livecom.domain.vo.RelatorioAcessosVersaoVO (dataFim, deviceSo, deviceSoVersion,versaoAplicativo,login) ");
		}
		sb.append(" from LogTransacao ");

		sb.append(" where 1=1 ");
		
		if (dataInicio != null) {
			sb.append(" and dataFim >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and dataFim <= :dataFim");
		}

		sb.append(" and requestPath = '/ws/login.htm'");

		if (StringUtils.isNotEmpty(versao)) {
			sb.append(" and versaoAplicativo like :versao");
		}

		
		sb.append(" AND empresa.id = :empresaId");
		
		if(!count) {
			sb.append(" order by login");
		}

		Query q = createQuery(sb.toString());
		
		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}
		
		if (StringUtils.isNotEmpty(versao)) {
			q.setParameter("versao", versao);
		}
		q.setLong("empresaId", empresa.getId());
		
		q.setCacheable(true);

		return q;
	}
	
	@Override
	public long getCountLogsByVersao(RelatorioFiltro filtro, Empresa empresa) throws DomainException {
		Query query = queryLogsByVersao(filtro, true, empresa);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	@Override
	public List<RelatorioAcessosVersaoVO> findLogsByVersao(RelatorioFiltro filtro, Empresa empresa) throws DomainException {
		Query query = queryLogsByVersao(filtro, false, empresa);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		query.setCacheable(true);
		List<RelatorioAcessosVersaoVO> acessos = query.list();

		return acessos;
	}

	@Override
	public List<String> findAllVersoes(Empresa empresa) {
		Query q = createQuery("select distinct versaoAplicativo from LogTransacao WHERE empresa.id = :empresaId order by versaoAplicativo");
		q.setLong("empresaId", empresa.getId());
		List<String> list = q.list();
		return list;
	}

	@Override
	public List<LogSistema> findAll(Empresa empresa) {
		Query q = createQuery("FROM LogSistema log where log.empresa.id = ?");
		q.setLong(0, empresa.getId());
		return q.list();
	}
}
