package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.TagAuditoria;
import br.livetouch.livecom.domain.vo.TagFilter;

@Repository
public interface TagRepository extends net.livetouch.tiger.ddd.repository.Repository<Tag> {

	List<Tag> findAllByFilter(TagFilter filter, Empresa empresa);

	boolean exists(String nome, Empresa empresa);

	void saveOrUpdate(TagAuditoria a);

	void deleteAll(Empresa empresa);

	List<Tag> findAll(Empresa empresa);

	List<Tag> findAllByKeys(List<Long> ids, Empresa empresa);

	List<Tag> findByNome(String nome, Empresa empresa);

	Tag findByName(Tag tag, Empresa empresa);
	
}