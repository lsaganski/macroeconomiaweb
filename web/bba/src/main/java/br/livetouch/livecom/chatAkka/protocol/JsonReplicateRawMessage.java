package br.livetouch.livecom.chatAkka.protocol;

import akka.actor.ActorRef;

/**
 * Replica o json enviado por este ator (tcp ou web) para as outras connections.
 * 
 * @author rlecheta
 *
 */
public class JsonReplicateRawMessage {
	public ActorRef sender;
	public Long from;
	
	/**
	 * Raw para replicar
	 */
	public JsonRawMessage raw;

	public JsonReplicateRawMessage(ActorRef tcpWebActor, Long from, JsonRawMessage raw) {
		super();
		this.sender = tcpWebActor;
		this.from = from;
		this.raw = raw;
	}

	@Override
	public String toString() {
		return "JsonReplicateRawMessage [sender=" + sender + ", userId=" + from + ", raw=" + raw + "]";
	}
	
	
}
