package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.StringUtils;

import br.livetouch.livecom.domain.enums.TipoPush;

public class PushReportFiltro implements Serializable {

	private static final long serialVersionUID = -8800402609370460617L;

	public static final String SESSION_FILTRO_KEY = "pushReportFiltro";

	@QueryParam("dataInicial")
	private String dataInicialString;
	@QueryParam("dataFinal")
	private String dataFinalString;
	@QueryParam("page")
	private int page;
	@QueryParam("max")
	private int max;
	@QueryParam("post")
	private Long postId;
	@QueryParam("status")
	private boolean status;
	@QueryParam("tipoPush")
	private String tipoPush;
	
	public String getDataInicialString() {
		return dataInicialString;
	}
	public void setDataInicialString(String dataInicialString) {
		this.dataInicialString = dataInicialString;
	}
	public String getDataFinalString() {
		return dataFinalString;
	}
	public void setDataFinalString(String dataFinalString) {
		this.dataFinalString = dataFinalString;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getMax() {
		return max;
	}
	public void setMax(int max) {
		this.max = max;
	}
	public Long getPostId() {
		return postId;
	}
	public void setPostId(Long postId) {
		this.postId = postId;
	}
	
	public String getTipoPush() {
		return tipoPush;
	}
	public void setTipoPush(String tipoPush) {
		this.tipoPush = tipoPush;
	}
	
	public TipoPush getTipoPushEnum() {
		return StringUtils.equals(tipoPush, "Todos") ? null : TipoPush.valueOf(tipoPush);
	}
	public boolean isEnviada() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}

}
