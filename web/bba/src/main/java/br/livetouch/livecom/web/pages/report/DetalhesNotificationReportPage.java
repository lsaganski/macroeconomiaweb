package br.livetouch.livecom.web.pages.report;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Notification;

@Controller
@Scope("prototype")
public class DetalhesNotificationReportPage extends RelatorioPage {

	public int page;
	public Long id;
	
	public Notification notification;

	@Override
	public void onInit() {
		super.onInit();
		
		if(id != null) {
			notification = notificationService.get(id);
		}
	}

}
