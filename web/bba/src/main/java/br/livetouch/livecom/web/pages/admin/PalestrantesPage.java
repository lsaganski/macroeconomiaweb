package br.livetouch.livecom.web.pages.admin;


import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Palestrante;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Table;
import net.sf.click.extras.control.LinkDecorator;

/**
 * 
 */
@Controller
@Scope("prototype")
public class PalestrantesPage extends LivecomPage {

	public Table table = new Table();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public Palestrante device;

	public List<Palestrante> Palestrantes;
	
	@Override
	public void onInit() {
		super.onInit();

		table();

		if (id != null) {
			device = palestranteService.get(id);
		} else {
			device = new Palestrante();
		}
	}
	
	private void table() {
		Column c = null;
		
		c = new Column("nome", getMessage("nome.label"));
		c.setWidth("150px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("email", getMessage("email.label"));
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes", getMessage("detalhes.label"));
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean novo() {
		setRedirect(PalestrantesPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		device = palestranteService.get(id);
		setRedirect(PalestrantePage.class,"id",String.valueOf(id));
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Palestrante e = palestranteService.get(id);
			palestranteService.delete(e);
			setRedirect(getClass());
			setFlashAttribute("msg", getMessage("msg.palestrante.excluir.sucess"));
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.palestrante.excluir.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		Palestrantes = palestranteService.findAll();

		// Count(*)
		int pageSize = 100;

		table.setPageSize(pageSize);
		table.setRowList(Palestrantes);
	}
}
