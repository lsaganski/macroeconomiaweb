package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Likes;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.PostCountVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioLikesVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public interface LikeRepository extends net.livetouch.tiger.ddd.repository.Repository<Likes> {

	Likes findLikeByUserAndPost(Usuario u, Post post);
	Likes findLikeByUserAndComentario(Usuario u, Comentario c);

	List<Likes> findAllLikesPostByUserAndIds(Usuario user, List<Long> ids);
	List<Likes> findAllLikesComentarioByUserAndIds(Usuario user, List<Long> ids);

	List<Likes> findAllLikesOfPostByUser(Usuario u);
	List<Likes> findAllLikesByUser(Usuario u);
	List<Likes> findAllLikesOfComentarioByPost(Post post);
	
	List<Likes> findAllLikesByPost(Post post);
	long getCountLikesByPost(Post p);

	List<Likes> findAllLikesByComentario(Comentario c);

	long countByPost(Post p);
	Long countByComentario(Comentario c);

	List<Likes> findAllLikesNotificationByUser(Usuario user);
	List<Likes> findAllByPosts(Usuario user, List<Long> ids);
	List<PostCountVO> findCountByPosts(List<Long> ids);
	
	long getCountByFilter(RelatorioFiltro filtro, boolean count, boolean geral) throws DomainException;
	long getCountDetalhesByDate(RelatorioFiltro filtro, boolean count);
	
	List<RelatorioLikesVO> findbyFilter(RelatorioFiltro filtro, boolean count) throws DomainException;
	List<Likes> findDetalhesByDate(RelatorioFiltro filtro, boolean count);
	List<RelatorioLikesVO> findbyFilterConsolidado(RelatorioFiltro filtro) throws DomainException;
	
	List<RelatorioVisualizacaoVO> reportLikes(RelatorioFiltro filtro) throws DomainException;
	

}