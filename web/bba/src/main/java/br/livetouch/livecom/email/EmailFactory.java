package br.livetouch.livecom.email;

import br.livetouch.livecom.domain.Empresa;

/**
 * http://docs.aws.amazon.com/ses/latest/DeveloperGuide/mailbox-simulator.html
 * 
 * http://docs.aws.amazon.com/ses/latest/DeveloperGuide/deliverability-and-ses.html
 * 
 * @author Ricardo Lecheta
 *
 */
public class EmailFactory {

	@Deprecated
	public static EmailManager getEmailManager()  {
		return new SmtpEmailManager();
	}
	
	public static EmailManager getEmailManager(Empresa e)  {
		SmtpEmailManager manager = new SmtpEmailManager();
		return manager;
	}
}
