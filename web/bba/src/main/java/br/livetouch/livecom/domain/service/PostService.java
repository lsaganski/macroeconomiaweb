package br.livetouch.livecom.domain.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostDestaque;
import br.livetouch.livecom.domain.PostUsuarios;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.BuscaPost;
import br.livetouch.livecom.domain.vo.PostInfoVO;
import br.livetouch.livecom.domain.vo.PostVO;
import net.livetouch.tiger.ddd.DomainException;

public interface PostService extends Service<Post> {

	List<Post> findAll();

	List<Post> findAllByDate(Date data);
	
	List<Post> findAllPostsByUserAndTitle(BuscaPost b, int page, int maxRows);
	
	List<Post> findAllRascunhosByUser(Usuario user, int page, int maxSize);
	
	@Transactional(rollbackFor=Exception.class)
	public Post post(Post p, Usuario user, PostInfoVO info, boolean insert) throws DomainException, IOException;

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Usuario userInfo,Post c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Usuario userInfo,PostDestaque postDestaque);
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo, Post c, boolean deleteArquivosAmazon, boolean force) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo, List<Long> postIds, boolean deleteArquivosAmazon, boolean force) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo,Post c, boolean force) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo,PostDestaque postDestaque);

	List<PostVO> toListVo(Usuario user, List<Post> posts);
	List<PostVO> toListVo(Usuario user, List<Post> posts, Idioma idioma);

	PostVO toVO(Usuario user, Post p);
	PostVO toVO(Usuario user, Post p, Idioma i);

	/**
	 * Relatorios
	 * 
	 * @param string
	 * @return
	 */
	List<Post> findAllByTituloLike(String string);

	List<Post> findAllByTituloLikeWithMax(String titulo, int max, Empresa empresa);
	
	/**
	 * Total de usuarios que podem ver o post. Usado nos relatorios.
	 * 
	 * A FAZER: ir para ReportService
	 * 
	 * @param p
	 * @return
	 */
	long getCountUsuariosByPost(Post p);

	List<Post> findByCategorias(List<CategoriaPost> categorias);

	List<Long> findAllOwnerByUser(Usuario u);
	List<Long> findIdsCriadosByUser(Usuario u);

	PostVO setPost(Post post, Usuario u);
	PostVO setPost(Post post, Usuario u, Idioma i);
	
	List<Long> findIdsByCategoria(CategoriaPost c);
	List<Post> findPostsByTag(Tag tag);

	List<Post> findPostsByGrupo(Grupo g, boolean unicosDesteGrupo);

	@Transactional(rollbackFor=Exception.class)	
	int publicar();
	
	@Transactional(rollbackFor=Exception.class)	
	int expirar();

	boolean isVisivel(Usuario usuario, Post p);
	
	List<PostUsuarios> findPostUsuarios(Usuario user, List<Post> posts);
	
	PostUsuarios getPostUsuarios(Usuario user, Post p);

	PostUsuarios getPostUsuarios(Long user, Long p);
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(PostUsuarios postUsuarios) throws DomainException;

	List<Long> getUsuariosPushPost(Post post, Idioma idioma);

	List<Long> getUsuariosReminder(Post post, Idioma idioma);

	Set<Arquivo> getArquivos(Post post);
}
