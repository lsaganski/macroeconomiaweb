package br.livetouch.livecom.domain.repository;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Agenda;

@Repository
public interface AgendaRepository extends net.livetouch.tiger.ddd.repository.Repository<Agenda> {

}