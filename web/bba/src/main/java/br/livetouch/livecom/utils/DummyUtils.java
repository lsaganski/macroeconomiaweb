package br.livetouch.livecom.utils;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


/**
 * @author Ricardo Lecheta
 * @created 01/03/2013
 */
public abstract class DummyUtils {

	public static String getStringFromHtml(String str) {
		return getStringFromHtml(str, null);
	}

	public static String getStringFromHtml(String str, Integer max) {

		if(StringUtils.isBlank(str)) {
			return str;
		}

		Document document = Jsoup.parse(str);
		String text = document.text();
		if(max != null && text.length() > max) {
			text = text.substring(0, max) + "...";
		}

		return text;
	}

	public static String getHtmlFromString(String str) {

		str = StringEscapeUtils.escapeHtml(str);
		str = str.replace("\n", "<br>");
		str = str.replace("  ", "&nbsp;&nbsp;");

		return str;
	}

	public static String toRight(String str, int tam, String preench) {
		str = StringUtils.leftPad(str, tam, preench);
		str = StringUtils.right(str, tam);
		return str;
	}

	public static String getExtensao(String fileName) {

		if(StringUtils.isBlank(fileName)) {
			return null;
		}

		int lastIndexOf = fileName.lastIndexOf('.');
		String extensao = fileName.substring(lastIndexOf + 1, fileName.length());
		extensao = StringUtils.lowerCase(extensao);
		return extensao;
	}
}
