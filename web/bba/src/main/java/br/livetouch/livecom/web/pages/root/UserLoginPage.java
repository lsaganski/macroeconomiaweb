package br.livetouch.livecom.web.pages.root;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.web.pages.admin.LivecomAdminPage;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class UserLoginPage extends LivecomAdminPage {
	public String msg;
	public Form form = new Form();
	public String from;
	private TextField tLogin;

	@Override
	public void onInit() {
		super.onInit();
		
		form.setMethod("post");
		form.add(tLogin = new TextField("login", true));
		tLogin.setAttribute("class", "form-control");
		
		Submit btlogon = new Submit("logon", "Entrar", this, "logon");
		btlogon.setAttribute("class", "botao salvar");
		form.add(btlogon);
	}

	@Override
	public void onGet() {
		super.onGet();
	}
	
	public boolean logon() {
		if(form.isValid()) {
			Usuario u = usuarioService.findByLogin(tLogin.getValue());
			if(u != null) {
				UserInfoVO userInfo = UserInfoVO.create(u);
				userInfo.addSession("web");
				UserInfoVO.setHttpSession(getContext(),userInfo);
				setRedirect(MuralPage.class);
			}
		}
		return false;
	}
	
	@Override
	public void onPost() {}

}

