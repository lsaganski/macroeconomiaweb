package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Cidade;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.enums.Estado;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.repository.CidadeRepository;
import br.livetouch.livecom.domain.service.CidadeService;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class CidadeServiceImpl implements CidadeService {
	@Autowired
	private CidadeRepository rep;

	@Override
	public Cidade get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Cidade c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Cidade> findAll() {
		return rep.findAllOrderByEstado();
	}

	@Override
	public void delete(Cidade c) throws DomainException {
		try {
			rep.delete(c);
		} catch (DataIntegrityViolationException e) {
			throw new DomainMessageException("validate.cidade.delete.possui.relacionamentos");
		}
	}

	@Override
	public List<Cidade> findAllByEstado(Estado estado) {
		return rep.findAllByEstado(estado);
	}

	@Override
	public String csvCidade() {
		List<Cidade> findAll = findAll();
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("CODIGO").add("NOME").add("ESTADO");
		for (Cidade c : findAll) {
			body.add(c.getId().toString()).add(c.getNome()).add(c.getEstado().getSigla());
			body.end();
		}
		return generator.toString();
	}

	@Override
	public List<Cidade> findByNome(String nome, Estado estado, int page, int maxRows) {
		return rep.findByNome(nome, estado, page, maxRows);
	}

	@Override
	public Long countByNome(String nome, Estado estado, int page, int maxRows) {
		return rep.countByNome(nome, estado, page, maxRows);
	}

	@Override
	public Cidade findByName(Cidade cidade, Empresa empresa) {
		return rep.findByName(cidade, empresa);
	}
}
