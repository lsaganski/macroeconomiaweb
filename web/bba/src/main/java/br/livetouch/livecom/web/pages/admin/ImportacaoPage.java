package br.livetouch.livecom.web.pages.admin;

import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jfree.util.Log;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.enums.TipoLogSistema;
import br.livetouch.livecom.domain.vo.LogImportacaoFiltro;
import br.livetouch.livecom.domain.vo.LogImportacaoVO;
import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;
import br.livetouch.livecom.jobs.importacao.JobImportacaoUtil;
import br.livetouch.livecom.jobs.info.LogSistemaJob;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.extras.util.DateUtils;
import net.sf.click.Context;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Decorator;
import net.sf.click.control.FileField;
import net.sf.click.control.Form;
import net.sf.click.control.Select;
import net.sf.click.control.Submit;
import net.sf.click.extras.control.DateField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LinkDecorator;

@Controller
@Scope("prototype")
public class ImportacaoPage extends LivecomLogadoPage {

	public Form formImportacao = new Form();
	private FileField fileField;
	public String entrada;
	public String count;
	public String countOk;
	public String countError;
	public String countInsert;
	public String countUpdate;
	JobInfo jobInfo;

	public DateField tDataInicio = new DateField("dataInicial", getMessage("dataInicio.label"));
	public DateField tDataFim = new DateField("dataFinal", getMessage("dataFim.label"));
	public String dataInicio;
	public String dataFim;
	public int page;
	private IntegerField tMax;
	public Form form = new Form();
	private LogImportacaoFiltro filtro;

	public PaginacaoTable table = new PaginacaoTable();
	public ActionLink detalhes = new ActionLink("detalhes", this, "detalhes");

	public Select combo = new Select();
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.IMPORTACAO)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
		
		if (clear) {
			getContext().removeSessionAttribute(LogImportacaoFiltro.SESSION_FILTRO_KEY);
		}

		combo.add("- Todos -");
		combo.setValue("- Todos -");
		combo.addAll(logImportacaoService.findAllTiposCsv(getEmpresa()));
		combo.setName("arquivoNome");
		form.add(combo);

		table();

		form();

		formImportacao();

		filtro = (LogImportacaoFiltro) getContext().getSessionAttribute(LogImportacaoFiltro.SESSION_FILTRO_KEY);
		if (filtro != null) {
			form.copyFrom(filtro);
			combo.setValue(filtro.getArquivoNome());
		}
	}

	public void formImportacao() {
		jobInfo = JobInfo.getInstance(getEmpresaId());

		formImportacao.add(fileField = new FileField("file", true));
		fileField.setAttribute("style", "display:none;");

		entrada = ParametrosMap.getInstance(getEmpresaId()).get(Params.PASTAENTRADA);

		Submit tImportar = new Submit("upload", "Importar", this, "upload");
		tImportar.setAttribute("class", "btn blue fix-padding");
		tImportar.setAttribute("style", "display:none;");
		formImportacao.add(tImportar);
	}

	public void form() {

		tDataInicio.setAttribute("class", "input data datepicker");
		tDataFim.setAttribute("class", "input data datepicker");
		tDataInicio.setFormatPattern("dd/MM/yyyy");
		tDataFim.setFormatPattern("dd/MM/yyyy");

		tDataInicio.setValue(DateUtils.toString(DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
		tDataFim.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));

		tDataInicio.setMaxLength(10);
		tDataFim.setMaxLength(10);

		form.add(tDataInicio);
		form.add(tDataFim);

		Submit tfiltrar = new Submit("filtrar", getMessage("filtrar.label"), this, "filtrar");
		tfiltrar.setAttribute("class", "btn btn-primary min-width");
		form.add(tfiltrar);

		Submit exportar = new Submit("visualizar", this, "visualizar");
		exportar.setAttribute("class", "botao pequeno btTitulo azul");
		form.add(exportar);

		Submit baixar = new Submit("baixar", this, "baixar");
		baixar.setAttribute("class", "botao pequeno btTitulo azul");
		form.add(baixar);

		tMax = new IntegerField("max");
		tMax.setAttribute("class", "input");
		form.add(tMax);
	}

	private void table() {
		// table.setClass("tabelaPedidos erroImportacao");

		Column c = new Column("arquivoNome", "Arquivo");
		table.addColumn(c);
		c = new Column("dataIni", "Início");
		table.addColumn(c);
		c = new Column("dataFim", "Fim");
		table.addColumn(c);
		// c = new Column("dataDif", "Diferença");
		c = new Column("dataDif", "Duração");
		table.addColumn(c);
		// c = new Column("porcentagem", "Sucesso %");
		c = new Column("porcentagem", "Progresso %");
		table.addColumn(c);
		c = new Column("status");
		c.setDecorator(new Decorator() {
			@Override
			public String render(Object object, Context context) {
				LogImportacaoVO log = (LogImportacaoVO) object;
				if (StringUtils.equals(log.getStatus(), "ERRO") || !StringUtils.equals(log.getPorcentagem(), "100%")) {
					/*
					 * return "<span class=\"label label-danger\">Falha</span>";
					 */
					return "<i class=\"icones falha tooltip\" title=\"Falha\"></i>";
				}
				return "<i class=\"icones success tooltip\" title=\"Sucesso\"></i>";
			}
		});
		table.addColumn(c);

		c = new Column("detalhes");
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, detalhes, "id"));
		table.addColumn(c);

		table.setPageSize(10);
		// table.setRowList(list);
	}

	public Object filtrar() {
		filtro = (LogImportacaoFiltro) getContext().getSessionAttribute(LogImportacaoFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new LogImportacaoFiltro();
			getContext().setSessionAttribute(LogImportacaoFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);
		return true;
	}

	public boolean detalhes() {
		Long id = detalhes.getValueLong();
		setRedirect(DetalhesLogImportacaoPage.class, "id", String.valueOf(id));
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();
		if (filtro == null) {
			filtro = new LogImportacaoFiltro();
			getContext().setSessionAttribute(LogImportacaoFiltro.SESSION_FILTRO_KEY, filtro);
			form.copyTo(filtro);
		}
		Integer max = tMax.getInteger();
		if (max == null) {
			max = 20;
			filtro.setMax(max);
		}

		long count = 0;
		List<LogImportacaoVO> vos = logImportacaoService.findAllVOByFiltro(filtro, getEmpresa());
		count = logImportacaoService.countByFiltro(filtro, getEmpresa());
		table.setCount(count);
		table.setPageSize(max);
		table.setRowList(vos);
	}

	public boolean upload() {
		super.onRender();
		LogSistema logSistema = new LogSistema();
		if (form.isValid()) {
			Usuario usuario = getUsuario();
			String userImportou = usuario.getNome();
			FileItem fileItem = fileField.getFileItem();
			try {

				ParametrosMap map = ParametrosMap.getInstance(getEmpresaId());
				InputStream in = fileItem.getInputStream();

				if (in == null) {
					setMessageError(getMessage("msg.arquivo.invalido.error"));
					return false;
				}

				String importOn = ParametrosMap.getInstance(getEmpresaId()).get(Params.IMPORTACAO_ARQUIVO_ON, "0");
				if (!StringUtils.equals(importOn, "1")) {
					setMessageError("Parametro importacao.arquivo.on está desativado");
					return false;
				}

				byte[] bytes = fileItem.get();
				if (StringUtils.isEmpty(entrada)) {
					setMessageError(getMessage("pastaDeEntrada.error"));
					return false;
				}

				String[] split = StringUtils.split(StringUtils.trim(fileItem.getName()), ".");
				if (split == null || split.length == 0) {
					setMessageError("Não foi possível fazer o parse do arquivo, formato não identificado");
					return false;
				}

				Empresa empresa = getEmpresa();
				if (empresa == null) {
					setMessageError("Empresa não identificada");
					return true;
				}
				String nome = JobImportacaoUtil.normalizeNomeArquivo(split[0]);
				String ext = split[1];
				jobInfo.arquivo = nome;

				nome += "." + ext;
				
				logSistema.setMsg("Importou o arquivo " + nome);
				if (!JobImportacaoUtil.isImportFile(nome, map)) {
					setMessageError("O arquivo <b>" + nome + "</b> não é reconhecido pelo sistema");
					return false;
				}

				jobInfo.arquivo += "_" + empresa.getId() + "." + ext;

				File f = new File(StringUtils.trim(entrada) + "/" + jobInfo.arquivo);
				Log.debug("Importando arquivo: " + f);

				FileUtils.writeByteArrayToFile(f, bytes);

				
				
				String msgLog = "[" + userImportou + "]" + " importou o arquivo ["+ nome + "]";
				LogAuditoria.log(getUsuario(), null, AuditoriaEntidade.IMPORTACAO, AuditoriaAcao.IMPORTAR, msgLog);
				
				jobInfo.temArquivo = true;
				setFlashAttribute("msg", getMessage("msg.importacao.arquivo.salvar.sucess"));
				refresh();

				return true;
			} catch (Exception e) {
				String msgLog = userImportou + " Tentou importar o arquivo [" + fileItem.getName() + "]";
				LogAuditoria.log(getUsuario(), null, AuditoriaEntidade.IMPORTACAO, AuditoriaAcao.IMPORTAR, msgLog, e);
				
				logSistema.setTipo(TipoLogSistema.ERRO);
				logSistema.setException(e.getMessage() + " - " + e);
				logError(e.getMessage(), e);
				return false;
			} finally {
				logSistema.setTipo(TipoLogSistema.INFORMACAO);
				logSistema.setUsuario(getUsuario());
				logSistema.setEmpresa(getEmpresa());
				logSistema.setData(new Date());
				LogSistemaJob.getInstance().addLogSistema(logSistema);
			}
		}
		setMessageError(getMessage("msg.importacao.arquivo.salvar.error"));
		return false;
	}
}