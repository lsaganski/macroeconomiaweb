package br.livetouch.livecom.domain.repository.impl;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Canal;
import br.livetouch.livecom.domain.repository.CanalRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class CanalRepositoryImpl extends StringHibernateRepository<Canal> implements CanalRepository {

	public CanalRepositoryImpl() {
		super(Canal.class);
	}

}