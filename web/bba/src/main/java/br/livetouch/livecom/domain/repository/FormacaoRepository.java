package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Formacao;
import br.livetouch.livecom.domain.Usuario;

@Repository
public interface FormacaoRepository extends net.livetouch.tiger.ddd.repository.Repository<Formacao> {

	List<Formacao> findAllFormacao(Usuario userInfo);

	List<Formacao> findAll(Empresa empresa);
	
}