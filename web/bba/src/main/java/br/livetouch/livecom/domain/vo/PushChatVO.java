package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.List;

import br.livetouch.livecom.push.PushNotificationVO;
import br.livetouch.livecom.utils.JSONUtils;

public class PushChatVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6818120154504380201L;
	
	private Long conversa;
	private List<Long> usuarios;
	private PushNotificationVO notification;

	public PushChatVO() {

	}

	public Long getConversa() {
		return conversa;
	}

	public void setConversa(Long conversa) {
		this.conversa = conversa;
	}

	public List<Long> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Long> usuarios) {
		this.usuarios = usuarios;
	}

	public PushNotificationVO getNotification() {
		return notification;
	}

	public void setNotification(PushNotificationVO notification) {
		this.notification = notification;
	}
	
	public String getMsg() {
		return notification != null ? notification.getMsg() : "";
	}
	
	@Override
	public String toString() {
		return JSONUtils.toJSON(this);
	}
}
