package br.livetouch.livecom.domain;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;

import br.infra.util.Utils;
import br.infra.web.click.HTMLEncode;
import br.livetouch.livecom.domain.enums.TipoLogSistema;
import br.livetouch.livecom.utils.JSONUtils;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.extras.util.ExceptionUtil;


/**
 * Log para erros e mensagens do sistema.
 * 
 * Ex: alterar um usuário salva um log
 * 
 * @author ricardo
 *
 */
@Entity
public class LogSistema extends net.livetouch.tiger.ddd.Entity {

	static final long serialVersionUID = 1L;
	public static final String SESSION_FILTRO_KEY = "LogSistema";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "LOGSISTEMA_SEQ")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private java.util.Date data;

	private String codigo;

	/**
	 * login do usuário logado
	 */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario usuario;

	@Column(length=2000, nullable=false)
	private String msg;
	
	@Column(length=2000, nullable=true)
	private String exception;

	@Enumerated(EnumType.STRING)
	private TipoLogSistema tipo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public java.util.Date getData() {
		return data;
	}

	public void setData(java.util.Date data) {
		this.data = data;
	}

	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDataString() {
		return DateUtils.toString(data, "dd/MM/yyyy HH:mm:ss");
	}

	public String getLogin() {
		Usuario u = getUsuario();
		String login = u != null ? u.getLogin() : null;
		return login;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getMsg() {
		return msg;
	}
	
	public String getMsgEncode() throws UnsupportedEncodingException {
		//		byte[] b = Base64.encodeBase64(msg.getBytes());
		//		String s = new String(b);
		//		String s = URLEncoder.encode(msg);
		//return "&lt Ricardo&gt";

		String s = HTMLEncode.encode(msg);
		return s;
	}

	public void setMsg(String msg) {
		this.msg = Utils.truncate(msg, 2000);
	}	

	public TipoLogSistema getTipo() {
		return tipo;
	}

	public void setTipo(TipoLogSistema tipo) {
		this.tipo = tipo;
	}

	public static LogSistema logInfo(Usuario u, String msg) {
		LogSistema  log = new LogSistema();
		log.setData(new Date());
		log.setUsuario(u);
		log.setMsg(msg);
		log.setTipo(TipoLogSistema.INFORMACAO);
		return log;
	}

	public static LogSistema logError(Usuario u, String msg) {
		LogSistema  log = new LogSistema();
		log.setData(new Date());
		log.setUsuario(u);
		log.setMsg(msg);
		log.setTipo(TipoLogSistema.ERRO);
		return log;
	}
	
	public static LogSistema logError(Usuario u, Exception e) {
		LogSistema  log = new LogSistema();
		log.setData(new Date());
		log.setUsuario(u);
		log.setMsg(ExceptionUtil.getStackTrace(e));
		log.setTipo(TipoLogSistema.ERRO);
		return log;
	}
	
	public String getException() {
		return exception;
	}
	
	public String getExceptionString() {
		String s = HTMLEncode.encode(exception);
		return s;
	}
	public void setException(String exception) {
		this.exception = Utils.truncate(exception, 2000);
	}
	
	/**
	 * Metodo que verifica se a transacao foi originada com ERRO
	 * 
	 * @return
	 */
	public boolean isErro() {
		if(tipo != null && TipoLogSistema.ERRO.equals(tipo)) {
			return true;
		}
		if (StringUtils.isNotEmpty(this.exception)) {
			return true;
		} else {
			return false;
		}
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public String toString() {
		return JSONUtils.toJSON(this);
	}
	
	
	
}
