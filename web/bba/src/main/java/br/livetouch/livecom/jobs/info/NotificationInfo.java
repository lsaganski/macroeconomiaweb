package br.livetouch.livecom.jobs.info;

import java.util.LinkedList;

import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.vo.NotificationJobVO;

/**
 * Os logs adicionados aqui serao processados pelo LivecomJob
 * 
 * @author live
 *
 */
public class NotificationInfo {
	
	protected LinkedList<NotificationJobVO> list;
	protected static NotificationInfo instance = null;
	
	public NotificationInfo() {
		list = new LinkedList<>();
	}
	
	public static NotificationInfo getInstance() {
		if (instance == null) {
			instance = new NotificationInfo();
		}		
		return instance;
	}

	public void addNotification(NotificationJobVO not) {
		list.add(not);
	}

	public void addNotification(Notification n, Boolean isPushAutor) {
		Post p = n.getPost();
		NotificationJobVO not = new NotificationJobVO(p, isPushAutor, p.getUsuario(), n, n.isSendPush());
		list.add(not);
	}
	
	public LinkedList<NotificationJobVO> getList() {
		return list;
	}
	
	public LinkedList<NotificationJobVO> getListAndDelete() {
		LinkedList<NotificationJobVO> list = new LinkedList<>(this.list);
		this.list.removeAll(this.list);
		
		return list;
	}

	public void remove(Notification n) {
		
		for (NotificationJobVO not : list) {
			if(n.equals(not.notification)) {
				list.remove(not);
				return;
			}
		}
		
	}
	
	public Notification remove(Post p) {
		
		for (NotificationJobVO not : list) {
			if(p.equals(not.post)) {
				list.remove(not);
				return not.notification;
			}
		}
		
		return null;
		
	}
	
	
}
