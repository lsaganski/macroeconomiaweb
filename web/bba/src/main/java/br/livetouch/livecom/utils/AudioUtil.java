package br.livetouch.livecom.utils;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * http://stackoverflow.com/questions/3009908/how-do-i-get-a-sound-files-total-time-in-java
 * 
 * @author rlech
 *
 */
public class AudioUtil {

	public static void main(String[] args) throws Exception {
		String url = "https://s3-sa-east-1.amazonaws.com/livecom-livetouch/4ae104ea-639a-499b-9449-572c6fe132cb/filesChat/27297c05-d375-4efa-90e7-d48a26ca871d_livecom1446638142517.3gp";
		
		File f = new File("a.3gp");
		
		getDurationWithMp3Spi(f);
		
		double durationInSeconds = getDurationInSeconds(f, url);
		
		System.out.println(durationInSeconds);
	}

	public static double getDurationInSeconds(File f,String url) {
		try {
			System.out.println(f.exists());
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(f);
			AudioFormat format = audioInputStream.getFormat();
			long frames = audioInputStream.getFrameLength();
			double durationInSeconds = (frames+0.0) / format.getFrameRate();
			return durationInSeconds;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	private static void getDurationWithMp3Spi(File file) throws UnsupportedAudioFileException, IOException {

	    AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(file);
	    System.out.println(fileFormat);
//	    if (fileFormat instanceof TAudioFileFormat) {
//	        Map<?, ?> properties = ((TAudioFileFormat) fileFormat).properties();
//	        String key = "duration";
//	        Long microseconds = (Long) properties.get(key);
//	        int mili = (int) (microseconds / 1000);
//	        int sec = (mili / 1000) % 60;
//	        int min = (mili / 1000) / 60;
//	        System.out.println("time = " + min + ":" + sec);
//	    } else {
//	        throw new UnsupportedAudioFileException();
//	    }

	}
}
