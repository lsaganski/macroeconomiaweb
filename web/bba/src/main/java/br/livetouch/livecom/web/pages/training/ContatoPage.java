package br.livetouch.livecom.web.pages.training;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.EmailContato;
import br.livetouch.livecom.domain.enums.StatusContato;
import br.livetouch.livecom.domain.enums.TipoEmailContato;

@Controller
@Scope("prototype")
public class ContatoPage extends TrainingPage {

	@Override
	public void onInit() {

		TipoEmailContato[] values = TipoEmailContato.values();
//		for (TipoEmailContato tipo : values) {
//			System.out.println(tipo);
//			System.out.println(tipo.toString());
//		}
		addModel("motivos", values);
	}

	@Override
	public void onPost() {

		String nome = getParam("nome");
		String email = getParam("email");
		String mensagem = getParam("mensagem");
		String motivoIdStr = getParam("motivoId");
		Long motivoId = StringUtils.isNotBlank(motivoIdStr) ? new Long(motivoIdStr) : null;

		if(StringUtils.isBlank(nome) || StringUtils.isBlank(email) || StringUtils.isBlank(mensagem) || motivoId == null) {

			setMessageError("Preencha todos os campos.");
			return;
		}

		EmailContato contato = new EmailContato();
		contato.setNome(nome);
		contato.setEmail(email);
		contato.setMensagem(mensagem);
		contato.setStatus(StatusContato.A_PENDENTE);
		contato.setTipo(TipoEmailContato.DUVIDA);
		contato.setData(new Date());

		try {
			emailContatoService.saveOrUpdate(contato);
		}
		catch (Exception e) {
			setMessageError(e);
			return;
		}

		setRedirect(getClass());
		setMessageKey("msg.agradecimento-contato");
	}
}
