package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.Filtro;
import net.livetouch.tiger.ddd.DomainException;

public interface EmpresaService extends Service<Empresa> {

	List<Empresa> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Empresa c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Empresa empresa, Usuario usuario, Usuario userInfo, List<Parametro> parametros) throws DomainException;

	List<Empresa> filterEmpresa(Filtro filtroEmpresa);

	Empresa findDominio(String dominio);

	Empresa findTrial(String codigo);

	Empresa findByName(Empresa empresa);

	List<Parametro> parametrosTrial(String nome, Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo, Empresa e) throws DomainException;

	Empresa findByAdmin(Usuario u);

    void setParametrosDefaultEmpresa(Empresa empresa);
}
