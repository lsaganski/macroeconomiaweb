package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostView;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.AudienciaVO;
import br.livetouch.livecom.domain.vo.ComentarioVO;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.livecom.domain.vo.TagVO;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class PostPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	public Long id;
	private TextField tMode;
	private LongField tId;
	private UsuarioLoginField tUser;
	private Checkbox tAudiencia;
	public int maxRowsComentarios;
	
	private TextField tIdioma;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tId = new LongField("id", true));
		form.add(tUser = new UsuarioLoginField("user_id", true, usuarioService, getEmpresa()));
		
		tAudiencia = new Checkbox("audiencia");
		form.add(tAudiencia);

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		form.add(new TextField("maxRowsComentarios"));

		form.add(tMode = new TextField("mode"));
		
		form.add(tIdioma = new TextField("idioma"));
		
		tMode.setValue("json");

		form.add(new Submit("login"));

		tId.setFocus(true);

		// setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (id != null) {

			Usuario u = tUser.getEntity();
			if (u == null) {
				if(isWsVersion3()) {
					Response r = Response.error("Usuário não encontrado");
					return r;
				}
				return new MensagemResult("NOK", "Usuário não encontrado");
			}

			Post p = postService.get(id);

			if (p != null) {
				
				Idioma i = null;
				String idiomaCodigo = tIdioma.getValue();
				if(StringUtils.isNotEmpty(idiomaCodigo)) {
					i = idiomaService.findByCodigo(idiomaCodigo);
				}
				
				if(!hasPermissao(ParamsPermissao.VISUALIZAR_TODOS_OS_POSTS)) {
					
					Set<Grupo> grupos = new HashSet<Grupo>();
					List<Grupo> subgrupos = grupoService.findSubgruposInGrupos(p.getGrupos());
					grupos.addAll(subgrupos);
					grupos.addAll(p.getGrupos());
					
					if (Collections.disjoint(usuarioService.getGrupos(u), grupos)) {
						boolean visivel = postService.isVisivel(getUsuario(), p);
						if(!visivel) {
							if(isWsVersion3()) {
								Response r = Response.error("Usuário sem permissão para visualizar o post");
								if(grupoService.hasAberto(grupos)) {
									r.groups = GrupoVO.toList(new ArrayList<>(grupos));
								}
								return r;
							}
							if(grupoService.hasAberto(grupos)) {
								return new MensagemResult("NOK", "Usuário sem permissão para visualizar o post", GrupoVO.toList(new ArrayList<>(grupos)));
							} else {
								return new MensagemResult("NOK", "Usuário sem permissão para visualizar o post");
							}
						}
					}
				
				}
				
				// PostView
				PostView postView = postViewService.findByUserPost(u, p);
				if(postView == null) {
					postView = new PostView();
					postView.setUsuario(u);
					postView.setPost(p);
				}
				postView.setData(new Date());
				postView.setLido(true);
				postView.setVisualizado(true);
				postView.incrementContador();
				postView.incrementContadorLido();
				postViewService.saveOrUpdate(postView);

				// Notification
				notificationService.markPostAsRead(p, u);

				// JSON
				PostVO response = postService.toVO(u, p, i);
				response.resumirMural();
				long countLikes = likeService.getCountLikesByPost(p);
				response.likeCount = countLikes;

				boolean audiencia = tAudiencia.isChecked();

				if(audiencia && p.getUsuario().getId().equals(u.getId())) {
					AudienciaVO audienciaVO = notificationService.getCountAudienciaByPost(p);
					if(audienciaVO != null) {
						long total = postService.getCountUsuariosByPost(p);
						audienciaVO.setTotal(total);
						Long iteracao = audienciaVO.getIteracao() != null ? audienciaVO.getIteracao() : 0;
						audienciaVO.setRestante(total - iteracao);
						response.setAudiencia(audienciaVO);
					}
				}
                notificationService.markComentarioAsRead(p.getId(), u.getId());
                
                if(getParametrosMap().isChatOn()) {
                	AkkaHelper.sendNotificationWasRead(u.getId(), p.getId());
                }
                
				if(isWsVersion3()) {
					List<Comentario> list = comentarioService.findAllByUserAndPost(-1, p.getId(), 0, maxRowsComentarios);
					List<ComentarioVO> comentarios = comentarioService.toListVo(u,list);
					// Notification
					response.comentarios = comentarios;
					Response r = Response.ok("OK");
					r.post = response;
					return r;
				}
				
				return response;
			} else {
				if(isWsVersion3()) {
					Response r = Response.error("POST não encontrado");
					return r;
				}
				return new MensagemResult("NOK", "POST não encontrado");
			}
		}

		if(isWsVersion3()) {
			Response r = Response.error("POST não encontrado");
			return r;
		}
		return new MensagemResult("NOK", "POST não encontrado");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("post", PostVO.class);
		x.alias("arquivo", FileVO.class);
		x.alias("grupo", GrupoVO.class);
		x.alias("tags", TagVO.class);
		super.xstream(x);
	}
}
