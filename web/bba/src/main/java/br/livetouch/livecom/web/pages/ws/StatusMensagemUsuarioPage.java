package br.livetouch.livecom.web.pages.ws;

import java.util.List;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.StatusMensagemUsuario;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.StatusMensagemUsuarioVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class StatusMensagemUsuarioPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	
	public Long id;
	private UsuarioLoginField tUser;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		IntegerField tId = null;
		form.add(tId = new IntegerField("id"));
		form.add(tUser = new UsuarioLoginField("user_id", false, usuarioService, getEmpresa()));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));
		
		tId.setFocus(true);
		tMode.setValue("json");

		form.add(new Submit("Enviar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario user = tUser.getEntity();
			Mensagem mensagem = mensagemService.get(id);
			
			if (mensagem == null) {
				if(isWsVersion3()) {
					Response r = Response.error("Mensagem não encontrada");
					return r;
				}
				return new MensagemResult("NOK","Mensagem não encontrada");
			}
			if (user == null) {
				if(isWsVersion3()) {
					Response r = Response.error("Acesso negado.");
					return r;
				}
				return new MensagemResult("NOK","Acesso negado.");
			}
			
			boolean ok = mensagem.isFromTo(user);
			if(!ok) {
				if(isWsVersion3()) {
					Response r = Response.error("Acesso negado.");
					return r;
				}
				return new MensagemResult("NOK","Acesso negado.");
			}

			Set<StatusMensagemUsuario> statusUsuarios = mensagem.getStatusUsuarios();
			List<StatusMensagemUsuarioVO> listVO = StatusMensagemUsuarioVO.toList(statusUsuarios, user);
			
			if(isWsVersion3()) {
				Response r = Response.ok("OK");
				r.statusMensagens = listVO;
				return r;
			}

			return listVO;
		}
		if(isWsVersion3()) {
			Response r = Response.error("Erro ao listar mensagens.");
			return r;
		}
		return new MensagemResult("NOK","Erro ao listar mensagens.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("status", StatusMensagemUsuarioVO.class);
		super.xstream(x);
	}
}
