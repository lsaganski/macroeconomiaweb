package br.livetouch.livecom.web.pages.admin;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.livetouch.hibernate.HibernateUtil;

@Controller
@Scope("prototype")
public class ClearCachePage extends LivecomAdminPage {

	public Date now = new Date();
	
	@Override
	public String getTemplate() {
		return getPath();
	}
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}
	
	@Override
	public void onInit() {
		super.onInit();
		
		HibernateUtil.clearCache();
		System.gc();
	}
}
