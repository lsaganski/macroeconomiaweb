package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.enums.TipoParametro;
import br.livetouch.livecom.domain.vo.ParametroVO;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public interface ParametroRepository extends net.livetouch.tiger.ddd.repository.Repository<Parametro> {

	List<Parametro> findDefaultByNomeNotIn(List<String> nomes);

	
	Parametro findByNome(String nome, Empresa empresa);
	
	List<Parametro> findAllByEmpresa(Long id);

	List<ParametroVO> parametrosAutocomplete(Long empresa, String nome);

	List<Parametro> findAllParametros(Empresa e, Integer page, Integer max, boolean count) throws DomainException;

	long getCount(Empresa e, int page, int max, boolean count) throws DomainException;

	List<Parametro> findAllParametros(Empresa e) throws DomainException;

	long getCount();

	List<Parametro> findAllParametrosByTipo(TipoParametro tParametro, Empresa empresa);

	List<Parametro> findAll(Long empresaId, TipoParametro tipoParametro);

	List<Parametro> findParametrosMobile(Empresa empresa);


    List<Parametro> findMobileDefault();
}