package br.livetouch.livecom.jobs;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.vo.UserToPushVO;

/**
 * Service do Spring para abrir Transactions para o Work.
 * 
 * Spring controla commit/rollback
 * 
 * @author Centrium
 *
 */
@Service
public class WorkDispatcherService {
	@Autowired
	ApplicationContext applicationContext;

	@Autowired
	protected SessionFactory sessionFactory;

	@Autowired
	protected InsertNotificationWork insertNotificationUsuariosWork;

	@Transactional(rollbackFor = Exception.class)
	public void insertNotificationUsuariosWork(Notification n, List<UserToPushVO> listUsers, Post p, boolean insert) {
		Session session = sessionFactory.getCurrentSession();
		insertNotificationUsuariosWork.init(applicationContext, n, listUsers, p, insert);
		session.doWork(insertNotificationUsuariosWork);
	}
}
