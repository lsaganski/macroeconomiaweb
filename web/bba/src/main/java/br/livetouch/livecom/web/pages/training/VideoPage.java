package br.livetouch.livecom.web.pages.training;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Historico;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.PostVO;
import net.livetouch.tiger.ddd.DomainException;

@Controller
@Scope("prototype")
public class VideoPage extends TrainingLogadoPage {

	public Long id;
	public Post post;
	public PostVO postvo;
	
	@Override
	public boolean onSecurityCheck() {
		boolean ok = super.onSecurityCheck();
		if(ok) {
			Usuario usuario = getUsuario();
			if(!usuario.isStatusPagOk()) {
				setRedirect(BillingPage.class,"msgInfo","1");
				return onSecurityCheckNotOk();
			}
		}
		return ok;
	}

	@Override
	public void onInit() {
		super.onInit();
		
		if(id == null) {
			setRedirect(IndexPage.class);
			return;
		}
		
		post = postService.get(id);
		
		if(post == null) {
			setRedirect(IndexPage.class);
			return;
		}
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();

		if(post != null) {
			postvo = postService.toVO(getUsuario(),post);
			
			Historico h = historico();

			post.incrementPostView();

			try {
				postService.saveOrUpdate(getUsuario(),post);
				historicoService.saveOrUpdate(h);
			} catch (DomainException e) {
				logError(e.getMessage(), e);
			}
		}
	}

	private Historico historico() {
		Historico h = historicoService.findByUserPost(getUsuario(),post);

		if(h == null) {
			h = new Historico();
			h.setPost(post);
			h.setUsuario(getUsuario());
		}
		
		h.setData(new Date());
		
		return h;
	}
}
