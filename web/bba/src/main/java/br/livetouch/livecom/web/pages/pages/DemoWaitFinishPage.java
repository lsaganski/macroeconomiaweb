package br.livetouch.livecom.web.pages.pages;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.admin.LivecomAdminPage;

@Controller
@Scope("prototype")
public class DemoWaitFinishPage extends LivecomAdminPage {

	@Override
	public void onGet() {
		super.onGet();

		String cod = getParam("cod");
		String erro = getParam("erro");
		if(StringUtils.contains(cod, "delete_empresa_")) {
			if(StringUtils.isNotEmpty(erro) && !StringUtils.equalsIgnoreCase(erro, "null")) {
				msgError = erro;
			} else {
				msg = "Empresa excluída com sucesso.";
			}
		} else {
			msg = "Fim do job ["+cod+"]";	
		}
		
		
	}
}
