package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Favorito;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.PostCountVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import br.livetouch.spring.SearchInfo;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public interface FavoritoRepository extends net.livetouch.tiger.ddd.repository.Repository<Favorito> {

	Favorito findByUserAndPost(Usuario u, Post post);
	
	Favorito findByUserAndFile(Usuario u, Arquivo file);

	List<Favorito> findAllFavoritosByUserAndPostIds(Usuario user, List<Long> ids);

	List<Favorito> findAllByUser(Usuario userInfo);

	List<Favorito> findAllFavoritosNotificationByUser(Usuario u);

	List<Post> findAllPostsByUser(Usuario userInfo, SearchInfo search);

	List<Favorito> findAllByPosts(Usuario user, List<Long> ids);

	List<PostCountVO> findCountByPosts(List<Long> ids);

	List<RelatorioVisualizacaoVO> reportFavoritos(RelatorioFiltro filtro) throws DomainException;

}