package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Token;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.TokenRepository;
import br.livetouch.livecom.domain.service.TokenService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class TokenServiceImpl  extends LivecomService<Token> implements TokenService {
	@Autowired
	private TokenRepository rep;

	@Override
	public Token get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Token c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Token> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void delete(Token c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public List<Token> findAllByUser(Usuario u) {
		return rep.findAllByUser(u);
	}

	@Override
	public Token findLastToken(Long userId, String token, String so) {
		return rep.findLastToken(userId, token, so);
	}

}