package br.livetouch.livecom.rest.resource;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.FuncaoVO;
import br.livetouch.livecom.domain.vo.FuncoesVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/funcao")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class FuncaoResource extends MainResource {
	protected static final Logger log = Log.getLogger(FuncaoResource.class);
	
	@POST
	public Response create(Funcao funcao) {
		
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			if(StringUtils.isEmpty(funcao.getNome())) {
				return Response.ok(MessageResult.error("Preencha o campo nome.")).build();
			}
			
			//Validar se ja existe um cargo com o mesmo nome
			boolean existe = funcaoService.findByName(funcao, getEmpresa()) != null;
			if(existe) {
				log.debug("Ja existe uma area com este nome");
				return Response.ok(MessageResult.error("Ja existe um cargo com este nome")).build();
			}
			
			funcaoService.saveOrUpdate(funcao, getUserInfo());
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", new FuncaoVO(funcao))).build();
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel efetuar o cadastro do cargo")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@PUT
	public Response update(Funcao funcao) {
			
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			if(funcao != null && funcao.getId() != null) {
				
				//Validar se ja existe um cargo com o mesmo nome
				boolean existe = funcaoService.findByName(funcao, getEmpresa()) != null;
				if(existe) {
					log.debug("Ja existe uma area com este nome");
					return Response.ok(MessageResult.error("Ja existe um cargo com este nome")).build();
				}
				
				funcaoService.saveOrUpdate(funcao, getUserInfo());
				return Response.ok(MessageResult.ok("Cargo atualizo com sucesso", new FuncaoVO(funcao))).build();
			} else {
				return Response.ok(MessageResult.error("Função não foi informada")).build();
			}
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar o cargo")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@GET
	public Response findByNome(@QueryParam("buscaTotal") String buscaTotal, @QueryParam("q") String nome, @QueryParam("page") int page, @QueryParam("maxRows") int maxRows) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		List<Funcao>funcoes = funcaoService.findByNome(nome, page, maxRows, getEmpresa());
		
		FuncoesVO vo = new FuncoesVO();
		if(StringUtils.isNotEmpty(buscaTotal) || StringUtils.equals(buscaTotal, "1")) {
			Long count = funcaoService.countByNome(nome, page, maxRows, getEmpresa());
			vo.total = count;
		}
		vo.funcoes = FuncaoVO.toListVO(funcoes);
		
		return Response.ok().entity(vo).build();
		
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Funcao funcao = funcaoService.get(id);
		if(funcao == null) {
			return Response.ok(MessageResult.error("Função não encontrada")).build();
		}
		FuncaoVO vo = new FuncaoVO(funcao);
		return Response.ok().entity(vo).build();
	}
	
	@GET
	@Path("/combo/{id}")
	public Response findCombo(@PathParam("id") Long id) {
		if(id == null) {
			return Response.ok(MessageResult.error("Funções não encontradas")).build();
		}

		try {
			Usuario usuario = usuarioService.get(id);
			if(usuario == null) {
				return Response.ok(MessageResult.error("Funções não encontradas")).build();
			}
			
			List<Funcao> funcoes = funcaoService.findAll(usuario);
			List<FuncaoVO> vos = FuncaoVO.toListVO(funcoes);
			return Response.ok().entity(vos).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Erro ao buscar combo de cargos")).build();
		}
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Funcao funcao = funcaoService.get(id);
			if(funcao == null) {
				return Response.ok(MessageResult.error("Função não encontrada")).build();
			}
			funcaoService.delete(getUserInfo(), funcao);
			return Response.ok().entity(MessageResult.ok("Função deletada com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error(e.getMessage())).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Cargo  " + id)).build();
		}
	}
	
	@GET
	@Path("/exportar")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response exportar() throws IOException {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Empresa empresa = getEmpresa();
		String csv = funcaoService.csvCargo(empresa);
		
		String charset = ParametrosMap.getInstance(empresa).get(Params.EXPORTACAO_ARQUIVO_CHARSET, "ISO-8859-1");

		String attachament = "attachment; filename=" + URLEncoder.encode("cargos.csv", "UTF-8");
		ResponseBuilder b = Response.status(Status.OK);
		b.encoding(charset);
		b.header("Content-Disposition", attachament);
		b.header("Content-Type", "application/octet-stream"+";charset="+charset);
		b.entity(csv);
		Response r = b.build();
		return r;
	}
	
}
