package br.livetouch.livecom.web.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import net.livetouch.click.page.BorderPage;

@Controller
@Scope("prototype")
public class Error404Page extends BorderPage {
	
	
	@Override
	public String getTemplate() {
		return getPath();
	}
	
	@Override
	public void onGet() {
		super.onGet();
	}
}
