package br.livetouch.livecom.web.pages.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.LivecomRootPage;

@Controller
@Scope("prototype")
public class ParametroPage extends LivecomRootPage {
	
}

