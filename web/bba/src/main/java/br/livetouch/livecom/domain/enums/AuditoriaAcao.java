package br.livetouch.livecom.domain.enums;

public enum AuditoriaAcao {
	INSERIR("Inserir"), 
	EDITAR("Editar"),
	DELETAR("Deletar"),
	IMPORTAR("Importar");

	private final String label;
	AuditoriaAcao(String label){
		this.label = label;
	}

	@Override
	public String toString() {
		return label != null ? label : "?";
	}
}
