package br.livetouch.livecom.web.pages.pages;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.PostVO;

/**
 * 
 */
@Controller
@Scope("prototype")
public class MeusRascunhosPage extends PostComunicadoPage {

	public List<PostVO> posts;
	public boolean meusPosts = true;
	
	@Override
	public void onInit() {
		super.onInit();
		
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();

		int maxSize = 10;
		
		Usuario user = getUsuario();
		List<Post> list = postService.findAllRascunhosByUser(user, page, maxSize);

		posts = postService.toListVo(user, list);
		
		formAberto = false;
	}
}
