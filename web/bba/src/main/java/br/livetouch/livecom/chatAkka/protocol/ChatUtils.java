package br.livetouch.livecom.chatAkka.protocol;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import br.infra.util.Log;
import br.livetouch.livecom.chatAkka.LivecomChatInterface;
import br.livetouch.livecom.chatAkka.actor.AkkaFactory;
import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.chatAkka.router.ChatVO;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.vo.CardButtonVO;
import br.livetouch.livecom.domain.vo.CardVO;
import br.livetouch.livecom.domain.vo.ControleVersaoVO;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.LocationVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;

public class ChatUtils {
	private static Logger log = Log.getLogger("chat");

	private String code;
	private String json;

	public ChatUtils(String code, String json) {
		super();
		this.code = code;
		this.json = json;
	}
	
	/**
	 * {"code":"loginResponse","login":"rlecheta@livetouch.com.br","status":"Ok"}
	 * 
	 * @param user
	 * @param string
	 * @return
	 * @throws JSONException 
	 */
	public static JsonRawMessage getJsonLoginResponse(String login, String status, ControleVersaoVO c) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("code", "loginResponse");
		json.put("login", login);
		if(c != null) {
			JSONObject jsonControle = new JSONObject();
			jsonControle.put("status", c.status);
			jsonControle.put("mensagem", c.mensagem);
			jsonControle.put("errorCode", c.errorCode);
			jsonControle.put("forceUpdate", c.forceUpdate);
			jsonControle.put("link", c.link);
			json.put("controleVersao", jsonControle);
		}
		json.put("status", status);
		return new JsonRawMessage(json.toString());
	}
	
	public static JsonRawMessage getJsonMsg1C(MensagemVO msg, long identifier, long conversationId,boolean ok) {

		try {
			JSONObject json = new JSONObject();
			json.put("code", "msg1C");

			json.put("identifier", identifier);
			json.put("status", ok ? "ok" : "error");
			json.put("cId", conversationId);
			
			if(msg != null) {

				long messageId = msg.getId();

				json.put("date", msg.getData());
				json.put("msgId", messageId);
				if(msg.isGroup()) {
					json.put("gId", msg.getGrupoId());
				}

				List<FileVO> arquivos = msg.getArquivos();
				
				if (arquivos != null && arquivos.size() > 0) {
					// JSONObject arquivosObj = new JSONObject();

					// ArrayList<JSONObject> arquivosArray = new ArrayList<>();
					JSONArray arquivosArray = new JSONArray();

					for (FileVO arquivo : arquivos) {
						JSONObject arquivoObj = new JSONObject();

						arquivoObj.put("id", arquivo.getId());
						arquivoObj.put("nome", arquivo.getFile());
						arquivoObj.put("extensao", arquivo.getExtensao());
						arquivoObj.put("url", arquivo.getUrl());
						arquivoObj.put("length", arquivo.getLength());
						arquivoObj.put("width", arquivo.getWidth());
						arquivoObj.put("height", arquivo.getHeight());

						if (arquivo.getThumbs() != null && arquivo.getThumbs().size() > 0) {
							JSONArray thumbs = new JSONArray();
							for (ThumbVO thumb : arquivo.getThumbs()) {
								JSONObject thumbObj = new JSONObject();

								thumbObj.put("url", thumb.getUrl());
								thumbObj.put("width", thumb.getWidth());
								thumbObj.put("height", thumb.getHeight());

								thumbs.put(thumbObj);
							}

							arquivoObj.put("thumbs", thumbs);
						}

						arquivosArray.put(arquivoObj);
					}

					json.put("arquivos", arquivosArray);
				}
			}
	
			return new JsonRawMessage(json);

		} catch (Exception e) {
			log.error("getJsonServerReceivedMessage: " + e.getMessage(), e);
		}
		
		return null;
	
	}
	
	public static JsonRawMessage getJsonReceiveMessageAdmin(MensagemVO mensagem) {
		return new JsonRawMessage(getJsonMsg(mensagem, true));
	}
	
	
	public static JsonRawMessage getJsonMsgJsonRawMessage(MensagemVO mensagem) {
		return new JsonRawMessage(getJsonMsg(mensagem, false));
	}
	
	public static JSONObject getJsonMsg(MensagemVO mensagem) {
		return getJsonMsg(mensagem, false);
	}

	private static JSONObject getJsonMsg(MensagemVO mensagem, boolean messageAdmin) {
		return getJsonMsg(mensagem, messageAdmin, -1);
	}
	
	public static JSONObject getJsonMsg(MensagemVO mensagem, boolean messageAdmin, int idx) {

		Long conversationID = mensagem.getConversaId();
		Long messageID = mensagem.getId();
		String msg = mensagem.getMsg();
		Long from = mensagem.getFromId();
		String fromUserName = mensagem.getFromNome();
		
		List<FileVO> files = mensagem.getArquivos();

		try {
			JSONObject json = new JSONObject();
			json.put("code", "msg");
			
			if(idx > -1) {
				json.put("i", idx);
			}
			
			if(messageAdmin) {
				json.put("msgAdmin", 1);
			}
            json.put("identifier", mensagem.getIdentifier());
			json.put("cId", conversationID);
			json.put("msgId", messageID);
			if(mensagem.isGroup()) {
				json.put("gId", mensagem.getGrupoId());
			}
			json.put("from", from);
			json.put("fromName", fromUserName);
			json.put("date", mensagem.getData());
			json.put("msg", msg);
			json.put("status", mensagem.getStatusChat());

			if (files != null && files.size() > 0) {
				JSONArray arquivosArray = new JSONArray();

				for (FileVO arquivo : files) {
					JSONObject arquivoObj = new JSONObject();

					arquivoObj.put("id", arquivo.getId());
					arquivoObj.put("nome", arquivo.getFile());
					arquivoObj.put("tipo", arquivo.getTipo());
					arquivoObj.put("extensao", arquivo.getExtensao());
					arquivoObj.put("url", arquivo.getUrl());
					arquivoObj.put("length", arquivo.getLength());
					arquivoObj.put("width", arquivo.getWidth());
					arquivoObj.put("height", arquivo.getHeight());

					List<ThumbVO> thumbs = arquivo.getThumbs();
					if (thumbs != null && thumbs.size() > 0) {
						JSONArray jsonThumbs = new JSONArray();
						for (ThumbVO thumb : thumbs) {
							JSONObject thumbObj = new JSONObject();

							thumbObj.put("url", thumb.getUrl());
							thumbObj.put("width", thumb.getWidth());
							thumbObj.put("height", thumb.getHeight());

							jsonThumbs.put(thumbObj);
						}

						arquivoObj.put("thumbs", jsonThumbs);
					}

					arquivosArray.put(arquivoObj);
				}

				json.put("files", arquivosArray);
			}
			
			if(mensagem.getLocation() != null) {
				LocationVO location = mensagem.getLocation();
				JSONObject locObj = new JSONObject();
				if(location.getLat() != 0 && location.getLng() != 0) {
					locObj.put("lat", location.getLat());
					locObj.put("lng", location.getLng());
					json.put("loc", locObj);
				}
			}
			
			HashSet<CardVO> cards = mensagem.getCards();
			if(cards != null && cards.size() > 0) {
				JSONArray cardsArray = new JSONArray();
				for (CardVO card : cards) {
					JSONObject cardObj = new JSONObject();
					cardObj.put("id", card.getId());
					cardObj.put("imageUrl", card.getImageUrl());
					cardObj.put("title", card.getTitle());
					cardObj.put("subTitle", card.getSubTitle());
					List<CardButtonVO> buttons = card.getButtons();
					if(buttons != null && buttons.size() > 0) {
						JSONArray btnsObj = new JSONArray();
						for (CardButtonVO cardButton : buttons) {
							JSONObject btnObj = new JSONObject();
							btnObj.put("label", cardButton.getLabel());
							btnObj.put("id", cardButton.getId());
							btnObj.put("value", cardButton.getValue());
							btnObj.put("action", cardButton.getAction());
							btnsObj.put(btnObj);
						}
						cardObj.put("buttons", btnsObj);
					}
					cardsArray.put(cardObj);
				}
				json.put("cards", cardsArray);
			}
			
			HashSet<CardButtonVO> buttons = mensagem.getButtons();
			if(buttons != null && buttons.size() > 0) {
				JSONArray btnsObj = new JSONArray();
				for (CardButtonVO cardButton : buttons) {
					JSONObject btnObj = new JSONObject();
					btnObj.put("label", cardButton.getLabel());
					btnObj.put("id", cardButton.getId());
					btnObj.put("value", cardButton.getValue());
					btnObj.put("action", cardButton.getAction());
					btnsObj.put(btnObj);
				}
				json.put("buttons", btnsObj);
			}
			return json;
		} catch (Exception e) {
			log.error("getJsonReceiveMessage: " + e.getMessage(), e);
		}

		return null;
	}

	/*public static JsonRawMessage getJsonMessage(String code, Long conversaId, Long mensagemId,Long from,Long to) {

        try {
        		JSONObject json = new JSONObject();
            json.put("code", code);
            
            json.put("cId", conversaId);
            json.put("msgId", mensagemId);
            json.put("from", from);
            json.put("to", to);

            return new JsonRawMessage(json);

        } catch (JSONException e) {
            log.error("getJsonMessage: " + e.getMessage(),e);
            return null;
        }
    }*/

	public static JsonRawMessage getJsonMessageMsg2A_2C(String code, Long conversaId, Long mensagemId,Long from) {

        try {
        		JSONObject json = new JSONObject();
            json.put("code", code);

            json.put("cId", conversaId);
            json.put("msgId", mensagemId);
            json.put("from", from);

            return new JsonRawMessage(json);

        } catch (JSONException e) {
            log.error("getJsonMessage: " + e.getMessage(),e);
            return null;
        }
    }
	
	/**
	 * Avisa todos usuarios online que possuem conversa com o user.
	 * 
	 * {"code":"userStatus","content":{"fromUserID":1,"online":false,"status":"Disponivel"}}
	 * 
	 * @param online
	 */
	public static void sendUserStatus(LivecomChatInterface livecomInterface, Long userId, boolean online, boolean away, String so) {
		if(userId != null) {
//			GetFriends getFriends = new GetFriends(userId);
			
			// Atualiza data online
			Date dateOnlineChat = livecomInterface.updateLoginChat(userId);
			// Busca amigos que possui conversa
			List<Long> allFriends = livecomInterface.findUsuariosComQuemTemConversa(userId);
			// Remove eu
			allFriends.remove(userId);
			
			// Status: online/offline
			JsonRawMessage jsonStatusChanged = ChatUtils.getJsonUserStatus(userId,online,away,so, null, dateOnlineChat);
			AkkaHelper.sendToOnlineFriends(jsonStatusChanged, allFriends);
		}
	}
	
	// retorna o json para enviar o status do usuario
	public static JsonRawMessage getUserStatusToMe(LivecomChatInterface livecomInterface, Long userId, String so) {
		if(userId != null) {
			UsuarioVO u = livecomInterface.getUsuario(userId);
			if(u != null) {
				Boolean away = Livecom.getInstance().isUsuarioAwayChat(userId);
				Boolean online = Livecom.getInstance().isUsuarioLogadoChat(userId);
				return ChatUtils.getJsonUserStatus(userId, online, away, so,null, u.timestampOnlineChat);
			}
		}
		return null;
		
	}
	
	public static JsonRawMessage getJsonUserStatus(Long userId, boolean online, boolean away,String so,String status, Long dateOnlineChat) {
		try {
			JSONObject json = new JSONObject();
			json.put("code", "userStatus");

			if(dateOnlineChat != null) {
				json.put("dateOnlineChat", dateOnlineChat);
			}
			
			json.put("from", userId);
			json.put("online", online);
			if(away) {
				json.put("away", away);
			}
			
			if(status != null) {
				// Disponivel, Ocupado, etc
				json.put("status", status);
			}
			
			if(so != null) {
				json.put("so", so);
			}
			
			return new JsonRawMessage(json);

		} catch (Exception e) {
			log.error("getJsonUserStatus: " + e.getMessage(), e);
		}

		return null;
	}
	
	public static JsonRawMessage getJsonUserStatus(Long userId, boolean online, boolean away,String so,String status, Date dateOnlineChat) {
		Long dataChat = null;
		if(dateOnlineChat != null) {
			dataChat = dateOnlineChat.getTime();
		}
		return getJsonUserStatus(userId, online, away,so, status, dataChat);
	}

	public static JsonRawMessage getJsonPingOK(Long userId) {
		try {
			JSONObject json = new JSONObject();
			json.put("code", "pingOk");


			json.put("from", userId);
			boolean online = AkkaHelper.isOnline(userId);
			json.put("online", online);
			json.put("chat", Livecom.getInstance().getUserInfoChat(userId));
			
			return new JsonRawMessage(json);

		} catch (Exception e) {
			log.error("getJsonPingOK: " + e.getMessage(), e);
		}

		return null;
	}
	
	/**
	 * {"code":"keepAlive"}
	 * 
	 * @param userId 
	 * @return
	 */
	public static JsonRawMessage getJsonKeepAlive(Long userId) {
		try {
			JSONObject json = new JSONObject();
			json.put("code", "keepAlive");
			return new JsonRawMessage(json);

		} catch (Exception e) {
			log.error("getJsonKeepAlive: " + e.getMessage(), e);
		}

		return null;
	}
	
	public static JsonRawMessage getJsonIncorrectFormat(String originalJson) {
		try {
			JSONObject json = new JSONObject();
			json.put("code", "jsonParserError");

			

			json.put("json", originalJson);

			

			
			return new JsonRawMessage(json);

		} catch (Exception e) {
			log.error("getJsonIncorrectFormat: " + e.getMessage(), e);
		}

		return null;
	}
	
	/**
	 * Atualiza para messageReachDestination ou messageWasRead.
	 * 
	 * Usado para grupos.
	 * 
	 * @param json
	 * @param status
	 * @return
	 */
	public static JsonRawMessage updateCode(String json, String code) {
		try {
			if(!StringUtils.contains(json, "from")) {
				return null;
			}

			JSONObject jsonObj = new JSONObject(json);
			jsonObj.put("code", code);
			return new JsonRawMessage(jsonObj);
		} catch (Exception e) {
			log.error("getFromUserId ["+json+"]: " + e.getMessage(), e);
		}

		return null;
	}

	public String getCode() {
		return code;
	}

	public String getJson() {
		return json;
	}

	@Override
	public String toString() {
		return json;
	}

	public static JsonRawMessage getJsonToast(String msg) {
		try {
			JSONObject json = new JSONObject();
			json.put("code", "toast");
			json.put("msg", msg);
			return new JsonRawMessage(json);

		} catch (Exception e) {
			log.error("getJsonToast: " + e.getMessage(), e);
		}

		return null;
	}

	public static JsonRawMessage getJsonProgress(int percent, long identifier) {
		try {
			JSONObject json = new JSONObject();
			json.put("code", "progress");

			json.put("percent", percent);
			if(identifier > 0) {
				json.put("identifier", identifier);
			}
			
			return new JsonRawMessage(json);

		} catch (Exception e) {
			log.error("getJsonProgress: " + e.getMessage(), e);
		}

		return null;
	}
	
	/**
	 * Retorna o identifier
	 * 
	 * @param json
	 * @return
	 */
	public static Long getIdentifier(String json) {
		try {
			JSONObject jsonObj = new JSONObject(json);
			
			Long identifier = jsonObj.optLong("identifier");
			
			return identifier;
		} catch (JSONException e) {
			log.error("getIdentifier: " + e.getMessage(), e);
			return 0L;
		}
	}
	

	public static String getCode(String json) {
		try {
			if(!StringUtils.startsWith(json, "{")) {
				return json;
			}
			JSONObject jsonObj = new JSONObject(json);
			String code = jsonObj.optString("code");
			if(StringUtils.isEmpty(code)) {
				return "";
			}
			return code;
		} catch (JSONException e) {
			log.error("JSON inválido ["+json+"]" + e.getMessage(), e);
			return "";
		}
	}

	public static void logError(String msg, Throwable e) {
		log.error(msg,e);
	}

	public static JsonRawMessage putInt(String key,int value, JsonRawMessage raw) throws JSONException {
		// Msg de grupo
		JSONObject jObj = new JSONObject(raw.toString());
		// g2A=1, indica que todos do grupo leram.
		jObj.put(key, value);
		raw = new JsonRawMessage(jObj);
		return raw;
	}
	
	public static List<Long> getIdsUsersOnline(LivecomChatInterface livecomInterface,Long from) {
		List<Long> idUsers = livecomInterface.findUsuariosComQuemTemConversa(from);
		List<Long> idsOnline = new ArrayList<>();
		for (Long userId : idUsers) {
			boolean chatOn = Livecom.getInstance().isUsuarioLogadoChat(userId);
			if(chatOn) {
				idsOnline.add(userId);
			}
		}
		return idsOnline;
	}
	
	public static List<Long> getIdsUsersAway(LivecomChatInterface livecomInterface,Long from) {
		List<Long> idUsers = livecomInterface.findUsuariosComQuemTemConversa(from);
		List<Long> idsAway = new ArrayList<>();
		for (Long userId : idUsers) {
			boolean away = Livecom.getInstance().isUsuarioLogadoChatAway(userId);
			if(away) {
				idsAway.add(userId);
			}
		}
		return idsAway;
	}

	public static JsonRawMessage getJsonUsersOnline(List<Long> idsOnline, List<Long> idsAway) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("code", "online");
		json.put("ids", idsOnline);
		json.put("idsAway", idsAway);
		return new JsonRawMessage(json);
	}

	/**
	 * Inicia e para o sync
	 * 
	 * @param start
	 * @return
	 * @throws JSONException
	 */
	public static JsonRawMessage getJsonMsgBatch(boolean start, int qtdeMsgs, String flag) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("code", "msgs");
		json.put("count", qtdeMsgs);
		if(StringUtils.isNotEmpty(flag)) {
			json.put("flag", flag);
		}
		json.put("status", start);
		return new JsonRawMessage(json);
	}

	/**
	 * Faz refresh do chat.
	 * 
	 * @param start
	 * @return
	 * @throws JSONException
	 */
	public static JsonRawMessage getJsonRefreshChat() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("code", "refresh");
		return new JsonRawMessage(json);
	}
	
	/**
	 * {"code":"sendMessage","content":{"identifier":64,"conversationID":1596,"fromUserID":2,"msg":"oi"}}
	 * 
	 * @param time
	 * @param conversaId
	 * @param id
	 * @param msg
	 * @return 
	 * @throws JSONException 
	 */
	public static JsonRawMessage newMsg(long identifier, Long cId, Long from, String msg) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("code", "msg");
		json.put("identifier", identifier);
		json.put("cId", cId);
		json.put("from", from);
		json.put("msg", msg);
		return new JsonRawMessage(json);
	}
	
	public static String getUserAgentSO(String so) {
		if("android".equals(so)) {
			return "android";
		} else if("ios".equals(so)) {
			return "ios";
		}
		return null;
	}

	public static JsonRawMessage getJsonC2C(Long cId, Long fromId) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("code", "c2C");
		json.put("cId", cId);
		json.put("from", fromId);
		json.put("status", "OK");
		return new JsonRawMessage(json);
	}

	public static JsonRawMessage getJsonMsg2A(Long msgId, Long cId, Long from, Long to) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("code", "msg2A");
		json.put("msgId", msgId);
		json.put("cId", cId);
		json.put("from", from);
		json.put("to", to);
		return new JsonRawMessage(json);
	}

	public static JsonRawMessage getJsonMsg2C(Long msgId, Long cId, Long from, Long to) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("code", "msg2C");
		json.put("msgId", msgId);
		json.put("cId", cId);
		json.put("from", from);
		json.put("to", to);
		return new JsonRawMessage(json);
	}
	
	public static JsonRawMessage getJsonMsg2A_2C(MensagemVO msg) throws JSONException {
		JSONObject json = new JSONObject();
		
		String code = "msg" + msg.getStatusChat();
		
		json.put("code", code);
		json.put("status", msg.getStatusChat());
		json.put("msgId", msg.getId());
		json.put("cId", msg.getConversaId());
		json.put("from", msg.getFromId());
		json.put("to", msg.getToId());
		
		return new JsonRawMessage(json);
	}
	
	public static JsonRawMessage getJsonDenied(Long cId, Long gId) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("code", "accessDenied");
		json.put("cId", cId);
		json.put("gId", gId);
		json.put("msg", "Voce não tem mais acesso a esse grupo");
		return new JsonRawMessage(json);
	}

	/**
	 * Envia par todas conexoes deste ator.
	 * @param c
	 * @param raw
	 * @param userActor
	 */
	public static void sendReplicateMessage(ChatVO c, JsonRawMessage raw) {
		long from = c.from;
		final ActorRef webTcpActor = c.raw.actor;
		JsonReplicateRawMessage rawReplicate = new JsonReplicateRawMessage(webTcpActor,from,raw);

		// Pega o ator UserActor (dentro dele tem varias connections para replicar)
		// Ver UserActor.receiveReplicateJson(raw)
		ActorSelection userActor = AkkaFactory.getUserActor(c.from);
		AkkaHelper.tellWithFuture(userActor, rawReplicate, null);
	}
	
	/**
	 * {"code":"c2A","cId":2522,"from":2523}
	 */
	public static JsonRawMessage getJsonC2A(Long fromId, Long cId) {
		try {
			JSONObject json = new JSONObject();
			json.put("code", "c2A");
			json.put("cId", cId);
			json.put("from", fromId);
			return new JsonRawMessage(json);
		} catch (JSONException e) {
			log.error("getJsonC2A: " + e.getMessage(), e);
		}
		return null;
	}

    public static JsonRawMessage getJsonNotificationWasRead(Long postId) {
        try {
            JSONObject json = new JSONObject();
            json.put("code", "nRead");
            json.put("pId", postId);
            return new JsonRawMessage(json);
        } catch (JSONException e) {
            log.error("getJsonNotificationWasRead: " + e.getMessage(), e);
        }
        return null;
    }

    public static JsonRawMessage getJsonNotificationReadAll() {
        try {
            JSONObject json = new JSONObject();
            json.put("code", "nReadAll");
            return new JsonRawMessage(json);
        } catch (JSONException e) {
            log.error("getJsonNotificationReadAll: " + e.getMessage(), e);
        }
        return null;
    }
}
