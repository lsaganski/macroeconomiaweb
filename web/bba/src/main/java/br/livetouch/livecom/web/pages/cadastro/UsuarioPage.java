package br.livetouch.livecom.web.pages.cadastro;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.CidadeTextField;
import br.infra.web.click.ComboCanal;
import br.infra.web.click.ComboComplemento;
import br.infra.web.click.ComboEstado;
import br.infra.web.click.ComboExpediente;
import br.infra.web.click.ComboFormacao;
import br.infra.web.click.ComboFuncao;
import br.infra.web.click.ComboRegiao;
import br.infra.web.click.DateSimpleField;
import br.infra.web.click.EntityField;
import br.infra.web.click.RadioGenero;
import br.infra.web.click.RadioPerfil;
import br.livetouch.livecom.domain.Area;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Cidade;
import br.livetouch.livecom.domain.Diretoria;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Expediente;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.Estado;
import br.livetouch.livecom.domain.enums.OrigemCadastro;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.enums.TipoTemplate;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.service.ExpedienteService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.CamposMap;
import br.livetouch.livecom.domain.vo.GrupoFilter;
import br.livetouch.livecom.utils.LivecomUtil;
import br.livetouch.livecom.utils.UploadHelper;
import br.livetouch.livecom.web.pages.AcessoNegadoPage;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.ImageField;
import net.livetouch.click.control.link.Link;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Checkbox;
import net.sf.click.control.FileField;
import net.sf.click.control.Form;
import net.sf.click.control.Option;
import net.sf.click.control.Select;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class UsuarioPage extends LivecomLogadoPage {

	@Autowired
	protected UsuarioService usuarioService;

	@Autowired
	protected ExpedienteService expedienteService;

	public Form form = new Form();
	public String msgErro;
	public Long id;

	public int page;

	public Usuario usuario;

	private TextField ids;

	private EntityField<Diretoria> tDiretoria;

	private EntityField<Diretoria> tDiretoriaExec;

	private EntityField<Area> tArea;

	private RadioPerfil comboPerfil;

	public List<Perfil> perfis;

	private TextField tGrupos;

	public ComboEstado testado;

	public ComboExpediente tExpediente;
	
	private TextField tHoraAbertura;
	private TextField tHoraFechamento;

	private CidadeTextField tCidade;

	private Estado estado;

	public Estado[] estados = Estado.values();

	private boolean novoUsuario;

	public List<Grupo> grupos;

	public CamposMap campos;
	
	public Expediente expediente;

	/**
	 * Se o web service enviar este id, atualiza a foto com o arquivo.
	 */
	public Long arquivoFotoId;
	public Long arquivoFotoCapaId;
	
	public Checkbox tSendMail;
	public Link desativarUsuario = new Link("desativar", "desativar", this, "desativar");
	public Link ativarUsuario = new Link("ativar", "ativar", this, "ativar");
	public Link convidarUsuario = new Link("convidar", "convidar", this, "convidar");

	private Select tOrigemUsuario;
	
	public boolean chatOn = false;
	
	public String empresa;
	public String url;

	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;

		initGetUsuario();

		campos = getCampos();
		
		empresa = ParametrosMap.getInstance(getEmpresa()).get(Params.PUSH_SERVER_PROJECT, "");
		url = ParametrosMap.getInstance(getEmpresa()).get(Params.PUSH_SERVER_HOST, "");

		if (clear) {
			getContext().removeSessionAttribute(Usuario.SESSION_FILTRO_KEY);
		}
		form();

		if (usuario != null) {
			
			chatOn = Livecom.getInstance().isUsuarioLogadoChat(usuario.getId());
			
			StringBuilder sb = new StringBuilder("");
			grupos = usuarioService.getGrupos(usuario);
			expediente = usuario.getExpediente();
			boolean firstTime = true;
			for (Grupo g : grupos) {
				if (!firstTime) {
					sb.append(",");
				}
				sb.append(" ").append(g.getId());
				firstTime = false;
			}
			sb.append(",");
			ids.setValue(sb.toString());
			form.copyFrom(usuario);
			
			if(arquivoFotoId == null) {
				arquivoFotoId = usuario.getFoto() != null ? usuario.getFoto().getId() : null;
			}
			
			if(arquivoFotoCapaId == null) {
				arquivoFotoCapaId = usuario.getCapa() != null ? usuario.getCapa().getId() : null;
			}
			
			
			if(expediente != null) {
				tExpediente.setValueObject(expediente);
			}

			Cidade cidade = usuario.getCidade();
			if (cidade != null && cidade.getEstado() != null) {
				String estadosigla = usuario.getEstado();
				testado.setValue(estadosigla);
				testado.setValueObject(estadosigla);
			}
		}
	}
	
	@Override
	public boolean onSecurityCheck() {
		boolean b = super.onSecurityCheck();
		if (b) {
			b = hasPermissoes(true, ParamsPermissao.CADASTRAR_USUARIOS, ParamsPermissao.ACESSO_CADASTROS);
			if (!b && !getUsuario().getId().equals(id)) {
				setRedirect(AcessoNegadoPage.class);
			}
		}
		return true;
	}

	protected void initGetUsuario() {
		if (id != null) {
			usuario = usuarioService.get(id);
		} else {
			novoUsuario = true;
		}
	}

	@SuppressWarnings("unchecked")
	public void form() {
		form.add(new IdField());

		// ids dos grupos
		ids = new TextField("ids");
		ids.setAttribute("style", "display:none");
		if (isCampoObrigatorioCadastroAdmin(campos, "grupos")) {
			ids.setAttribute("required","required");
		}
		form.add(ids);

		// nome
		TextField tnome = new TextField("nome");
		tnome.setAttribute("class", "form-control input-sm");
		if (isCampoReadOnly(campos, "nome")) {
			tnome.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "nome", true)) {
			tnome.setAttribute("required","required");
		}
		form.add(tnome);
		
		if (isCampoVisivel(campos, "sobrenome")) {
			TextField tSobrenome = new TextField("sobrenome", "sobrenome");
			tSobrenome.setAttribute("class", "form-control input-sm");
			if (isCampoReadOnly(campos, "sobrenome")) {
				tSobrenome.setReadonly(true);
			}
			if (isCampoObrigatorioCadastroAdmin(campos, "sobrenome")) {
				tSobrenome.setAttribute("required","required");
			}
			form.add(tSobrenome);
		}
		
		TextField tLogin = new TextField("login");
		tLogin.setAttribute("class", "form-control input-sm");
		if(id != null) {
			tLogin.setDisabled(true);
			tnome.setAttribute("autofocus", "");
		}else{
			tLogin.setAttribute("autofocus", "");
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "login", true)) {
			tLogin.setAttribute("required","required");
		}
		form.add(tLogin);

		if (isCampoVisivel(campos, "cargo")) {
			TextField tcargo = new TextField("cargo", "cargo");
			tcargo.setAttribute("class", "form-control input-sm");
			if (isCampoReadOnly(campos, "cargo")) {
				tcargo.setReadonly(true);
			}
			if (isCampoObrigatorioCadastroAdmin(campos, "cargo")) {
				tcargo.setAttribute("required","required");
			}
			form.add(tcargo);
		}
		
		if (isCampoVisivel(campos, "regiao")) {
			ComboRegiao tRegiao = new ComboRegiao();
			if (isCampoReadOnly(campos, "regiao")) {
				tRegiao.setReadonly(true);
			}
			if (isCampoObrigatorioCadastroAdmin(campos, "regiao")) {
				tRegiao.setAttribute("required","required");
			}
			form.add(tRegiao);
		}
		
		// expediente
		tExpediente = new ComboExpediente(expedienteService);
		tExpediente.getOptionList().add(0, new Option("", "- Selecione -"));
		tExpediente.setValue("0");
		if (isCampoReadOnly(campos, "expediente")) {
			tExpediente.setReadonly(true);
		}
		form.add(tExpediente);
		
		// hora Abertura
		tHoraAbertura = new TextField("horaAbertura");
		tHoraAbertura.setAttribute("class", "form-control input-sm inputHorario");
		if (isCampoReadOnly(campos, "horaAbertura")) {
			tHoraAbertura.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "horaAbertura")) {
			tHoraAbertura.setAttribute("required","required");
		}
		form.add(tHoraAbertura);
		
		// hora Fechamento
		tHoraFechamento = new TextField("horaFechamento");
		tHoraFechamento.setAttribute("class", "form-control input-sm inputHorario");
		if (isCampoReadOnly(campos, "horaFechamento")) {
			tHoraFechamento.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "horaFechamento")) {
			tHoraFechamento.setAttribute("required","required");
		}
		form.add(tHoraFechamento);

		// email
		TextField temail = new TextField("email");
		temail.setAttribute("class", "email form-control input-sm");
		if (isCampoReadOnly(campos, "email")) {
			temail.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "email", true)) {
			temail.setAttribute("required","required");
		}
		form.add(temail);

		if (isCampoVisivel(campos, "twitter")) {
			TextField tTwitter = new TextField("twitter", "twitter");
			tTwitter.setAttribute("class", "twitter form-control input-sm");
			if (isCampoReadOnly(campos, "twitter")) {
				tTwitter.setReadonly(true);
			}
			if (isCampoObrigatorioCadastroAdmin(campos, "twitter")) {
				tTwitter.setAttribute("required","required");
			}
			form.add(tTwitter);
		}
		
		// telefoneFixo
		TextField ttelefoneFixo = new TextField("telefoneFixo");
		ttelefoneFixo.setAttribute("class", "form-control input-sm second");
		ttelefoneFixo.setMaxLength(10);
		if (isCampoReadOnly(campos, "telefoneCelular")) {
			ttelefoneFixo.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "telefoneFixo")) {
			ttelefoneFixo.setAttribute("required","required");
		}
		form.add(ttelefoneFixo);

		// dddTelefone
		TextField tDdd = new TextField("dddTelefone");
		tDdd.setMaxLength(2);
		tDdd.setAttribute("class", "form-control input-sm first");
		tDdd.setAttribute("placeholder", "ddd");
		if (isCampoReadOnly(campos, "telefoneFixo")) {
			tDdd.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "telefoneFixo")) {
			tDdd.setAttribute("required","required");
		}
		form.add(tDdd);

		// telefoneCelular
		TextField ttelefoneCelular = new TextField("telefoneCelular");
		ttelefoneCelular.setAttribute("class", "form-control input-sm second");
		ttelefoneCelular.setMaxLength(10);
		if (isCampoReadOnly(campos, "telefoneCelular")) {
			ttelefoneCelular.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "telefoneCelular")) {
			ttelefoneCelular.setAttribute("required","required");
		}
		form.add(ttelefoneCelular);

		// dddCelular
		tDdd = new TextField("dddCelular");
		tDdd.setMaxLength(2);
		tDdd.setAttribute("class", "form-control input-sm first");
		tDdd.setAttribute("placeholder", "ddd");
		if (isCampoReadOnly(campos, "telefoneCelular")) {
			tDdd.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "telefoneCelular")) {
			tDdd.setAttribute("required","required");
		}
		form.add(tDdd);

		// setor
		TextField tsetor = new TextField("setor");
		if (isCampoReadOnly(campos, "setor")) {
			tsetor.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "setor")) {
			tsetor.setAttribute("required","required");
		}
		form.add(tsetor);

		// unidade
		if (isCampoVisivel(campos, "unidade")) {
			TextField tunidade = new TextField("unidade", "unidade");
			tunidade.setAttribute("class", "form-control input-sm");
			if (isCampoReadOnly(campos, "unidade")) {
				tunidade.setReadonly(true);
			}
			if (isCampoObrigatorioCadastroAdmin(campos, "unidade")) {
				tunidade.setAttribute("required","required");
			}
			form.add(tunidade);
		}

		// identificacao
		TextField tidentificacao = new TextField("identificacao");
		if (isCampoReadOnly(campos, "identificacao")) {
			tidentificacao.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "identificacao")) {
			tidentificacao.setAttribute("required","required");
		}
		form.add(tidentificacao);

		// Status
		TextField tstatus = new TextField("status");
		if (isCampoReadOnly(campos, "status")) {
			tstatus.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "status")) {
			tstatus.setAttribute("required","required");
		}
		form.add(tstatus);

		RadioGenero tgenero = new RadioGenero();
		form.add(tgenero);
		
		tOrigemUsuario = new Select("flags");
		tOrigemUsuario.add(new Option("", "Livecom"));
		tOrigemUsuario.add(new Option("ldap", "LDAP"));
		tOrigemUsuario.add(new Option("webservice", "Web Service"));
		form.add(tOrigemUsuario);

		// distribuidor
		TextField tdistribuidor = new TextField("distribuidor");
		if (isCampoReadOnly(campos, "distribuidor")) {
			tdistribuidor.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "distribuidor")) {
			tdistribuidor.setAttribute("required","required");
		}
		form.add(tdistribuidor);

		// canal
		ComboCanal tcanal = new ComboCanal(canalService);
		tcanal.getOptionList().add(0, new Option("", "- Selecione -"));
		tcanal.setValue("0");
		if (isCampoReadOnly(campos, "canal")) {
			tcanal.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "canal")) {
			tcanal.setAttribute("required","required");
		}
		form.add(tcanal);

		// complemento
		ComboComplemento tcomplemento = new ComboComplemento(complementoService, getEmpresa());
		tcomplemento.getOptionList().add(0, new Option("", "- Selecione -"));
		tcomplemento.setValue("0");
		if (isCampoReadOnly(campos, "complemento")) {
			tcomplemento.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "complemento")) {
			tcomplemento.setAttribute("required","required");
		}
		form.add(tcomplemento);

		// funcao
		if (isCampoVisivel(campos, "funcao")) {
			ComboFuncao tfuncao = new ComboFuncao(funcaoService,getUsuario());
			tfuncao.getOptionList().add(0, new Option("", "- Selecione -"));
			tfuncao.setValue("0");
			if (isCampoReadOnly(campos, "funcao")) {
				tfuncao.setReadonly(true);
			}
			if (isCampoObrigatorioCadastroAdmin(campos, "funcao")) {
				tfuncao.setAttribute("required","required");
			}
			form.add(tfuncao);
		}
		
		// estado
		if (isCampoVisivel(campos, "estado")) {
			testado = new ComboEstado("estado");
			if (isCampoReadOnly(campos, "estado")) {
				testado.setReadonly(true);
			}
			if (isCampoObrigatorioCadastroAdmin(campos, "estado")) {
				testado.setAttribute("required","required");
			}
			form.add(testado);
		}

		// cidade
		if (isCampoVisivel(campos, "cidade")) {
			tCidade = new CidadeTextField("cidade", false, cidadeService);
			tCidade.setAttribute("class", "form-control input-sm");
			if (isCampoReadOnly(campos, "cidade")) {
				tCidade.setReadonly(true);
			}
			if (isCampoObrigatorioCadastroAdmin(campos, "cidade")) {
				tCidade.setAttribute("required","required");
			}
			tCidade.setStyle("display", "none");
			form.add(tCidade);
		}

		// dataNasc
		DateSimpleField tDataNasc = new DateSimpleField("dataNasc");
		tDataNasc.setAttribute("class", "data form-control input-sm");
		tDataNasc.setAttribute("placeholder", "dd/mm/aaaa");
		if (isCampoReadOnly(campos, "dataNasc")) {
			tDataNasc.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "dataNasc")) {
			tDataNasc.setAttribute("required","required");
		}
		form.add(tDataNasc);

		// Area
		tArea = new EntityField<Area>("area", areaService);
		tArea.setAttribute("class", "form-control input-sm");
		if (isCampoObrigatorioCadastroAdmin(campos, tArea.getName())) {
			tArea.setAttribute("required","required");
		}
		form.add(tArea);

		// Diretoria
		tDiretoria = new EntityField<Diretoria>("diretoria", diretoriaService);
		tDiretoria.setAttribute("class", "form-control input-sm");
		if (isCampoObrigatorioCadastroAdmin(campos, tDiretoria.getName())) {
			tDiretoria.setAttribute("required","required");
		}
		form.add(tDiretoria);

		// Diretoria Executiva
		tDiretoriaExec = new EntityField<Diretoria>("diretoria_exec", diretoriaService);
		if (isCampoObrigatorioCadastroAdmin(campos, tDiretoriaExec.getName())) {
			tDiretoriaExec.setAttribute("required","required");
		}
		form.add(tDiretoriaExec);

		

		// Grupos
		tGrupos = new TextField("grupos");
		tGrupos.setSize(650);
		form.add(tGrupos);

		// formacao
		ComboFormacao tformacao = new ComboFormacao(formacaoService);
		tformacao.getOptionList().add(0, new Option("", "- Selecione -"));
		tformacao.setValue("0");
		if (isCampoReadOnly(campos, "formacao")) {
			tformacao.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "formacao")) {
			// nao precisa required em combo
			tformacao.setAttribute("required","required");
		}
		form.add(tformacao);

		// PERFILL
		perfis = perfilService.findAll(getEmpresa());
		comboPerfil = new RadioPerfil(perfilService, getEmpresa());
		if (isCampoReadOnly(campos, "perfil")) {
			comboPerfil.setReadonly(true);
		}
		if (isCampoObrigatorioCadastroAdmin(campos, "perfil")) {
			// comboPerfil.setAttribute("required","required");
		}
		form.add(comboPerfil);

		form.add(new FileField("file"));
		form.add(new ImageField("foto"));

		tSendMail = new Checkbox("sendMail", getMessage("padrao.label"));
		tSendMail.setAttribute("class", "bootstrap");
		tSendMail.setAttribute("data-off-color", "danger");
		tSendMail.setAttribute("data-off-text", "Não");
		tSendMail.setAttribute("data-on-text", "Sim");
		form.add(tSendMail);
		
		Submit tsalvar = new Submit("salvar", "Salvar", this, "salvar");
		tsalvar.setAttribute("class", "btn btn-primary min-width hidden");
		form.add(tsalvar);

		tsalvar = new Submit("salvarConvidar", "Salvar e Convidar", this, "salvarConvidar");
		tsalvar.setAttribute("class", "btn btn-outline blue btSalvarConvidar min-width");
		form.add(tsalvar);

		form.add(new Submit("novo", this, "novo"));
		
		// setFormTextWidth(form);

	}

	@Override
	public void onGet() {
		super.onGet();
	}

	private void setGrupoAndGruposSelected() {
		usuario = new Usuario();
		
		List<Long> grupoIds = getListIds("ids");
		if(grupoIds != null && grupoIds.size() > 0) {
			List<Grupo> grupos = grupoService.findAllByIds(grupoIds);
			if(grupos != null && grupos.size() > 0) {
				Set<GrupoUsuarios> gruposUsuario = new HashSet<>();
				for (Grupo grupo : grupos) {
					GrupoUsuarios gsu = new GrupoUsuarios();
					gsu.setUsuario(usuario);
					gsu.setGrupo(grupo);
					gruposUsuario.add(gsu);
				}
				usuario.setGrupos(gruposUsuario);
			}
		}
	}

	public boolean salvar() {
		if (form.isValid()) {
			boolean salvo = execSalvar();

			return salvo;
		} else {
			setGrupoAndGruposSelected();
			msgErro = "Preencha os campos obrigatórios";
		}

		return false;
	}

	protected boolean execSalvar() {
		try {

			if (usuario == null) {
				usuario = id != null ? usuarioService.get(id) : new Usuario();
				usuario.setOrigemCadastro(OrigemCadastro.WEB);
			}

			Usuario userInfo = getUsuario();

			usuario.setEmpresa(getEmpresa());
			
			// copy
			StatusUsuario status = usuario.getStatus();
			form.copyTo(usuario);
			usuario.setStatus(status);

			if(isCampoVisivel(campos, "cidade")) {
				String city = (String) tCidade.getValue();
				if(StringUtils.isNotEmpty(city)) {
					Cidade cidade = cidadeService.get(Long.valueOf(city));
					usuario.setCidade(cidade);
				}
			}

			if(isCampoVisivel(campos, "estado")) {
				usuario.setEstado(testado.getEstado().name());
			}
			
			Area area = usuario.getArea();
			Diretoria diretoria = usuario.getDiretoria();
			if (diretoria == null && area != null) {
				Diretoria d = area.getDiretoria();
				if (d != null) {
					usuario.setDiretoria(d);
				}
			} else if(diretoria == null){
				Diretoria diretoriaExec = tDiretoriaExec.getEntity();
				usuario.setDiretoria(diretoriaExec);
			}

			String perfilUsuario = ParametrosMap.getInstance(1L).get(Params.PERFIL_USER_PADRAO, "Usuário");
			// perfil
			Perfil perfil = comboPerfil.getPerfil();
			usuario.setPermissao(perfil);
			if (usuario.getPermissao() == null) {
				perfil = perfilService.findByNome(perfilUsuario, getEmpresa());
				usuario.setPermissao(perfil);
			}

			Date horaAbertura = null;
			if(tHoraAbertura != null) {
				horaAbertura = DateUtils.newDateByHoraMinutoSeg(tHoraAbertura.getValue());
			}
						
			Date horaFechamento = null; 
			if(tHoraFechamento != null) {
				horaFechamento = DateUtils.newDateByHoraMinutoSeg(tHoraFechamento.getValue());
			}
			
			usuario.setHoraAbertura(horaAbertura);
			usuario.setHoraFechamento(horaFechamento);

			Empresa empresa = userInfo.getEmpresa();
			ParametrosMap params = ParametrosMap.getInstance(empresa);
			boolean validaEmail = params.getBoolean(Params.VALIDA_EMAIL_UNICO_ON, true);
			boolean validaLogin = params.getBoolean(Params.VALIDA_LOGIN_UNICO_ON, true);
			
			if(validaEmail) {
				boolean existe = usuarioService.existsByEmail(usuario, empresa);
				if (existe) {
					setMessageError("Este e-mail já está em uso.");
					return false;
				}
			}
			
			if(validaLogin) {
				boolean existe = usuarioService.existsByLogin(usuario, empresa);
				if (existe) {
					setMessageError("Este login já está em uso.");
					return false;
				}
			}
			
			if(id == null) {
				usuario.setPreCadastro(true);
				usuario.setPrimeiroAcesso(true);
			} else {
				usuario.setPreCadastro(false);
			}
			
			setFoto();
			usuarioServiceSalvar();

			if (usuario.getId() != null) {
				// grupos
				List<Long> grupoIds = getListIds("ids");
				List<Grupo> grupos = grupoService.findAllByIds(grupoIds);

				GrupoFilter filter = new GrupoFilter();
				List<Long> notIds = new ArrayList<Long>();
				notIds.add(-1L);
				filter.notIds = notIds;
				List<GrupoUsuarios> meusGrupos = grupoService.findGrupoUsuariosByUsuario(usuario);
				Set<GrupoUsuarios> gruposToSave = new HashSet<GrupoUsuarios>();
				List<Grupo> gruposToAdd = new ArrayList<Grupo>();
				
				for (GrupoUsuarios gu : meusGrupos) {
					Grupo grupo = gu.getGrupo();
					if(grupo != null) {
						if(grupos.contains(grupo)) {
							gruposToAdd.add(grupo);
						}
					}
				}
				
				for (Grupo g : grupos) {
					if(g != null) {
						if(!gruposToAdd.contains(g)) {
							GrupoUsuarios gu = usuarioService.addGrupoToUserSemSalvar(usuario, g, true);
							gruposToSave.add(gu);
						}
					}
				}
				
				usuarioService.deleteGrupoUsuarios(usuario, gruposToAdd);
				usuarioService.saveGruposToUser(usuario, gruposToSave, gruposToAdd);
			}
			return true;

		} catch (DomainException e) {
			form.setError(e.getMessage());
			msg = e.getMessage();
		} catch (Exception e) {
			logError(e.getMessage(), e);
			form.setError(e.getMessage());
			msg = e.getMessage();
		}

		return false;
	}

	/**
	 * Se o web service enviar este id, atualiza a foto com o arquivo.
	 * 
	 * @throws IOException
	 * @throws DomainMessageException 
	 */
	private void setFoto() throws IOException, DomainMessageException {
		if (arquivoFotoId != null) {
			Arquivo a = arquivoService.get(arquivoFotoId);

			if (a != null) {
				if(!usuario.getArquivos().contains(a)) {
					usuario.setFoto(createThumbsArquivoFoto(a, "fotoProfile"));
				} else {
					usuario.setFoto(a);
				}
			}
		}
		
		if (arquivoFotoCapaId != null) {
			Arquivo a = arquivoService.get(arquivoFotoCapaId);

			if (a != null) {
				if(!a.getUrlThumb().equals(usuario.getUrlFotoCapa())) {
					usuario.setCapa(createThumbsArquivoFoto(a, "capa"));
				} else {
					usuario.setCapa(a);
				}
			}
		}
	}
	
	protected Arquivo createThumbsArquivoFoto(Arquivo a, String tipo) throws IOException, DomainMessageException {
		UploadHelper.log.debug("Arquivo: " + a.getId());
		UploadHelper.log.debug("Gerando thumbs arquivo: " + a.getNome());
		UploadHelper.createThumbs(getParametrosMap(), a, usuario.getChave() + "/" + tipo);
		return a;
	}

	protected void usuarioServiceSalvar() throws DomainException, UnsupportedEncodingException, UnknownHostException {
		boolean cadastroOk = usuario.validarCadastroAtivo(getbuildType());

		if (cadastroOk) {
			
			if (usuario.getEmpresa() == null) {
				usuario.setEmpresa(getEmpresa());
			}

			if (StringUtils.isEmpty(usuario.getSenha())) {
				String senha = LivecomUtil.gerarSenha(getEmpresaId());
				usuario.setSenhaCript(senha);
			}

			usuario.setPerfilAtivo(true);
			usuarioService.saveAudit(getUsuario(), usuario);

			if (novoUsuario) {
				//SendMail
				
				TemplateEmail template = templateEmailService.findByType(getEmpresa(), TipoTemplate.convite);
				
				if(template != null && tSendMail.isChecked()) {
					String assunto = getParametrosMap().get(Params.EMAIL_SUBJECT_CONVITE, "Convite Livecom");
					ArrayList<Long> users = new ArrayList<Long>();
					users.add(usuario.getId());

					usuarioService.convidarUsuarios(getEmpresa(), getUserInfo(), getContextPath(), users, template, assunto);

					setMessageSesssion("Usuário(a) [" + usuario.getLogin() + "] cadastrado(a) com sucesso. ");
				} else {
					setMessageSesssion(
							"Usuário(a) [" + usuario.getLogin()
							+ "] cadastrado(a) com sucesso. <br/>Utilize a seção Convidar usuários para enviar um e-mail com os dados de acesso.",
							"Cadastrar.");
				}
				
			} else {
				setMessageSesssion("Usuário(a) [" + usuario.getLogin() + "] salvo(a) com sucesso.", "Sucesso.");
			}
			
			id = usuario.getId();
			
			if (id != null) {
				if(getUsuario().isPrimeiroAcesso()){
					setRedirect(MuralPage.class);
				} else{
					setRedirect(UsuarioPage.class, "id", id.toString());
				}
				
			} else {
				setRedirect(UsuariosPage.class);
			}
		}
	}

	public boolean novo() {
		setRedirect(UsuarioPage.class);
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		onRenderFindUsers();

		estado = Estado.from("SP");
		if (usuario != null && usuario.getCidade() != null) {
			estado = usuario.getCidade().getEstado();
			tCidade.setValueObject(usuario.getCidade());
		} else {
			estado = Estado.from("SP");
		}
		if (estado != null && testado != null) {
			testado.setValue(estado.name());
			testado.setValueObject(estado.name());
		}

	}

	protected void onRenderFindUsers() {
	}
	
	public boolean desativar() {
		try {
			Long id = desativarUsuario.getValueLong();
			if (id != null) {
				Usuario usuario = usuarioService.get(id);
				usuario.setPerfilAtivo(false);
				usuarioService.saveOrUpdate(getUserInfo(),usuario);
				setRedirect(UsuariosPage.class);
				setFlashAttribute("msg", "Usuário desativado com sucesso");
			}
		} catch (Exception e) {
			logError(e.getMessage(),e);
			this.msgErro = "Erro ao desativar usuario";
		}
		return true;
	}

	public boolean ativar() {
		try {
			Long id = ativarUsuario.getValueLong();
			if (id != null) {
				Usuario usuario = usuarioService.get(id);
				usuario.setPerfilAtivo(true);
				usuarioService.saveOrUpdate(getUserInfo(),usuario);
				setRedirect(UsuariosPage.class);
				setFlashAttribute("msg", "Usuário ativado com sucesso");
			}
		} catch (Exception e) {
			logError(e.getMessage(),e);
			this.msgErro = "Erro ao ativar usuario";
		}
		return true;
	}
}