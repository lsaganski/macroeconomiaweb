package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.CardButton;

public class CardButtonVO implements Serializable{
	private static final long serialVersionUID = -6702688067486576715L;
	private String action;
	private String label;
	private String value;
	private Long id;

	public CardButtonVO(CardButton button) {
		setAction(button.getAction());
		setId(button.getId());
		setLabel(button.getLabel());
		setValue(button.getValue());
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
