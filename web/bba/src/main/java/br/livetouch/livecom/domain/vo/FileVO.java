package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.Tag;

public class FileVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public String dir;
	
	// tipo no mobile, audio, video, etc
	public String tipo;
	
	public String file;
	public String extensao;
	public String contentType;
	public Long length;
	public String lengthHumanReadable;
	public int width;
	public int height;
	public String data;
	
	public String url;
	public String urlThumb;
	
	public List<ThumbVO> thumbs;

	public List<TagVO> tags;

	public UsuarioSimpleVO usuario;

	public MensagemVO mensagem;

	public PostSimpleVO post;
	
	public String descricao;
	public String dimensao;

	public boolean destaque;
	public Integer ordem;

	public void setArquivo(Arquivo a, boolean addMensagemToVO) {
		if (a != null) {
			this.width = a.getWidth();
			this.height = a.getHeight();
			
			this.tipo = a.getTipo();

			setId(a.getId());
			setExtensao(a.getExtensao());
			setDescricao(a.getDescricao());
			setDimensao(a.getDimensao());

			this.destaque = a.isDestaque();
			
			setFile(a.getNome());
			setUrl(a.getUrl());

			setUrlThumb(a.getUrlThumb());	
			this.thumbs = a.getThumbsVO();

			setLength(a.getLength());
			setLengthHumanReadable(a.getHumanReadableLength());

			setData(a.getDataUpdate().toString());

			if (a.getUsuario() != null) {
				if(!org.hibernate.Hibernate.isInitialized(a.getUsuario())) {
					//  SQL performance.
//					System.err.println(" SQL: Usuario nao inicializado ao carregar conversa. Vai fazer SQL aqui.");
				}
				this.usuario = new UsuarioSimpleVO();
				this.usuario.setUsuario(a.getUsuario());
			}

			if (addMensagemToVO) {
				if (a.getMensagem() != null) {
					this.mensagem = new MensagemVO();
					Mensagem msg = a.getMensagem();
					this.mensagem.setMensagem(msg, a.getUsuario());
				}
			}
			if (a.getTags() != null && a.getTags().size() > 0) {
				this.tags = new ArrayList<TagVO>();
				Set<Tag> arquivosTags = a.getTags();
				for (Tag tag : arquivosTags) {
					TagVO t = new TagVO();
					t.setTag(tag);
					this.tags.add(t);
				}
			}
			
			if (a.getPost() != null) {
				this.post = new PostSimpleVO();
				this.post.setPost(a.getPost());
			}

			this.ordem = a.getOrdem();
		}
	}

	public List<TagVO> getTags() {
		return tags;
	}

	public void setTags(List<TagVO> tags) {
		this.tags = tags;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrlThumb() {
		return urlThumb;
	}

	public void setUrlThumb(String urlThumb) {
		this.urlThumb = urlThumb;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExtensao() {
		return extensao;
	}
	
	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}

	public Long getLength() {
		return length;
	}

	public void setLength(Long length) {
		this.length = length;
	}

	public String getLengthHumanReadable() {
		return Utils.sizeToHumanReadable(length);
	}

	public void setLengthHumanReadable(String lengthHumanReadable) {
		this.lengthHumanReadable = lengthHumanReadable;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public MensagemVO getMensagem() {
		return mensagem;
	}

	public void setMensagem(MensagemVO mensagem) {
		this.mensagem = mensagem;
	}

	public List<ThumbVO> getThumbs() {
		return thumbs;
	}

	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	
	public void resumir() {
		this.usuario = null;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public static List<FileVO> fromList(List<Arquivo> arquivos, boolean addMensagemToVO) {
		List<FileVO> vos = new ArrayList<>();
		if(arquivos == null) {
			return vos;
		}
		for (Arquivo arquivo : arquivos) {
			FileVO vo = new FileVO();
			vo.setArquivo(arquivo, addMensagemToVO);
			vos.add(vo);
		}
		return vos;
	}

	public String getDimensao() {
		return dimensao;
	}

	public void setDimensao(String dimensao) {
		this.dimensao = dimensao;
	}

}
