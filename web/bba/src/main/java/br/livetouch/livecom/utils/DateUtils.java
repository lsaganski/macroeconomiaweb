package br.livetouch.livecom.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.Validate;
import org.joda.time.DateTime;
import org.ocpsoft.prettytime.PrettyTime;

import br.livetouch.livecom.domain.Livecom;

public class DateUtils {

	public static final String[] MESES_ABREVIADOS = new String[] { "Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez" };

	public static final String[] MESES_COMPLETOS = new String[] { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };

	public static final int[] ULTIMA_DIA_MES = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	public static final String DATE = "dd/MM/yyyy";

	// nao tem diferen�a entre dia e noite
	public static final String DATE_TIME_AM_PM = "dd/MM/yyyy hh:mm:ss";

	// formato at� 25h
	public static final String DATE_TIME_24h = "dd/MM/yyyy HH:mm:ss";

	public static String getDate() {
		return toString(new Date());
	}

	public static String getDate(String pattern) {
		return getDate(new Date(), pattern);
	}

	public static String getDate(Date date, String pattern) {
		return toString(date, pattern);
	}

	public static String toString(Date date, String pattern) {
		return toString(date, pattern, null);
	}

	public static String toString(Date date, String pattern, Locale locale) {
		if (date == null) {
			return "";
		}

		if (pattern == null) {
			throw new IllegalArgumentException("Null pattern parameter");
		}

		SimpleDateFormat format = locale != null ? new SimpleDateFormat(pattern, locale) : new SimpleDateFormat(pattern);

		return format.format(date);
	}

	public static String toString(Date date) {
		return toString(date, DATE, null);
	}

	public static String toString(Date date, Locale locale) {
		return toString(date, DATE, locale);
	}

	public static Date toDate(String date, SimpleDateFormat format) {
		try {
			return format.parse(date);
		} catch (ParseException e) {
			throw new RuntimeException("Error to parse Date: " + date + " using the pattern: " + format, e);
		}
	}

	public static Date toDate(String date, String pattern, Locale locale) {
		if (date == null) {
			return null;
		}

		if (pattern == null) {
			throw new IllegalArgumentException("Null pattern parameter");
		}

		SimpleDateFormat format = locale != null ? new SimpleDateFormat(pattern, locale) : new SimpleDateFormat(pattern);

		try {
			return format.parse(date);
		} catch (ParseException e) {
			throw new RuntimeException("Error to parse the Date: " + date + " using the pattern: " + pattern, e);
		}
	}

	public static Date toDate(String date, Locale locale) {
		return toDate(date, DATE, locale);
	}

	public static Date toDate(String date, String pattern) {
		return toDate(date, pattern, null);
	}

	public static Date toDate(String date) {
		return toDate(date, DATE, null);
	}

	public static Date toDateTime(String date) {
		return toDate(date, DATE_TIME_24h, null);
	}

	public static Date toDateTime(String date, Locale locale) {
		return toDate(date, DATE_TIME_24h, locale);
	}

	public static int compareTo(Date dateA, Date dateB) {
		return compareTo(dateA, dateB, "yyyyMMdd");
	}

	/**
	 * Compara duas datas
	 * 
	 * // - : menor (this < other) // 0 : igual (this = other) // + : maior
	 * (this > other)
	 * 
	 * @param otherCalendar
	 * @return
	 */
	public static int compareTo(Date dateA, Date dateB, String pattern) {
		Validate.notNull(dateA, "Data 1 vazia para comparar");
		Validate.notNull(dateB, "Data 2 vazia para comparar");

		String sA = toString(dateA, pattern);
		String sB = toString(dateB, pattern);

		// - : menor (this < other)
		// 0 : igual (this = other)
		// + : maior (this > other)

		return sA.compareTo(sB);
	}

	public static boolean equals(Date dateA, Date dateB, String pattern) {
		return compareTo(dateA, dateB, pattern) == 0;
	}

	public static boolean equals(Date dateA, Date dateB) {
		return compareTo(dateA, dateB) == 0;
	}

	/**
	 * Retorna o valor do hor�rio minimo para a data de referencia passada. <BR>
	 * <BR>
	 * Por exemplo se a data for "30/01/2009 as 17h:33m:12s e 299ms" a data
	 * retornada por este metodo ser� "30/01/2009 as 00h:00m:00s e 000ms".
	 * 
	 * @param date
	 *            de referencia.
	 * @return {@link Date} que representa o hor�rio minimo para dia informado.
	 */
	public static Date lowDateTime(Date date) {
		Calendar aux = Calendar.getInstance();
		aux.setTime(date);
		setTimeClearHours(aux); // zera os parametros de hour,min,sec,milisec
		return aux.getTime();
	}

	/**
	 * Retorna o valor do hor�rio maximo para a data de referencia passada. <BR>
	 * <BR>
	 * Por exemplo se a data for "30/01/2009 as 17h:33m:12s e 299ms" a data
	 * retornada por este metodo ser� "30/01/2009 as 23h:59m:59s e 999ms".
	 * 
	 * @param date
	 *            de referencia.
	 * @return {@link Date} que representa o hor�rio maximo para dia informado.
	 */
	public static Date highDateTime(Date date) {
		Calendar aux = Calendar.getInstance();
		aux.setTime(date);
		setTimeClearHours(aux); // zera os parametros de hour,min,sec,milisec
		aux.add(Calendar.DATE, 1); // vai para o dia seguinte
		aux.add(Calendar.MILLISECOND, -1); // reduz 1 milisegundo
		return aux.getTime();
	}

	/**
	 * Zera todas as referencias de hora, minuto, segundo e milesegundo do
	 * {@link Calendar}.
	 * 
	 * @param date
	 *            a ser modificado.
	 */
	public static void setTimeClearHours(Calendar date) {
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
	}

	public static Date setTimeInicioDia(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);

		setTimeInicioDia(c);

		Date d = c.getTime();
		return d;
	}

	public static void setTimeInicioDia(Calendar c) {
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
	}

	public static Date setTimeCurrent(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);

		Calendar now = Calendar.getInstance();
		now.setTime(new Date());

		c.set(Calendar.HOUR, now.get(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, now.get(Calendar.MINUTE));
		c.set(Calendar.SECOND, now.get(Calendar.SECOND));
		c.set(Calendar.MILLISECOND, now.get(Calendar.MILLISECOND));

		Date d = c.getTime();
		return d;
	}

	public static Date setTimeFimDia(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);

		setTimeFimDia(c);

		Date d = c.getTime();
		return d;
	}

	public static void setTimeFimDia(Calendar c) {
		c.set(Calendar.HOUR, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 0);
	}

	public static int getDiaSemana(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);

		int dia = c.get(Calendar.DAY_OF_WEEK);
		return dia;
	}
	
	public static int getDia(Date date) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);

		int dia = c.get(Calendar.DAY_OF_MONTH);
		return dia;
	}

	public static int getMes(Date date) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);

		int dia = c.get(Calendar.MONTH) + 1;
		return dia;
	}

	public static int getAno(Date date) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);

		int dia = c.get(Calendar.YEAR);
		return dia;
	}

	/**
	 * http://www.exampledepot.com/egs/java.util/CompDates.html
	 * 
	 * Retorna a quantidade de horas de diferença entre a data informada e a
	 * data atual
	 * 
	 * @param dataAtual
	 * @param data
	 * @return
	 */
	public static long getDiffInMillis(Date dataInicial, Date dataFinal) {

		Calendar c1 = Calendar.getInstance();
		c1.setTime(dataInicial);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(dataFinal);

		// Determine which is earlier
		// boolean b = c1.after(c2); // false
		// b = c1.before(c2); // true

		// Get difference in milliseconds
		long diffMillis = c2.getTimeInMillis() - c1.getTimeInMillis();

		return diffMillis;
	}

	/**
	 * http://www.exampledepot.com/egs/java.util/CompDates.html
	 * 
	 * Retorna a quantidade de horas de diferença entre a data informada e a
	 * data atual
	 * 
	 * @param dataAtual
	 * @param data
	 * @return
	 */
	public static long getDiffInSeconds(Date dataInicial, Date dataFinal) {
		long diffMillis = getDiffInMillis(dataInicial, dataFinal);
		long diffSecs = diffMillis / (1000);
		return diffSecs;
	}

	public static float getDiffInMinutes(Date dataInicial, Date dataFinal) {
		float diffMillis = getDiffInMillis(dataInicial, dataFinal);
		float diffMins = diffMillis / (60 * 1000);
		return diffMins;
	}

	/**
	 * http://www.exampledepot.com/egs/java.util/CompDates.html
	 * 
	 * Retorna a quantidade de horas de diferença entre a data informada e a
	 * data atual
	 * 
	 * @param dataAtual
	 * @param data
	 * @return
	 */
	public static long getDiffInHours(Date dataInicial, Date dataFinal) {
		long diffMillis = getDiffInMillis(dataInicial, dataFinal);
		// Get difference in hours
		long diffHours = diffMillis / (60 * 60 * 1000);
		return diffHours;
	}

	/**
	 * http://www.exampledepot.com/egs/java.util/CompDates.html
	 * 
	 * Retorna a quantidade de horas de diferença entre a data informada e a
	 * data atual
	 * 
	 * @param dataAtual
	 * @param data
	 * @return
	 */
	public static long getDiffInDays(Date dataInicial, Date dataFinal) {
		long diffMillis = getDiffInMillis(dataInicial, dataFinal);

		// Get difference in days
		long diffDays = diffMillis / (24 * 60 * 60 * 1000);

		return diffDays;
	}

	/**
	 * Faz a soma de 1 dia na data especificada, corrigindo o problema com
	 * horário de verão.<br>
	 * 
	 * Se por acaso o calendar do Java (ao somar 1 dia), continuar no mesmo dia
	 * se for horário de verão (ex: horario de verão perde 1 hora, e volta para
	 * as 23h) O algoritmo força a troca de dia
	 * 
	 * @author Ricardo R. Lecheta
	 * @param data
	 * @return
	 * @since v2.0, 14/1/2009
	 */
	public static Date addDia(Date date, int dias) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);

		int dia1 = c.get(Calendar.DAY_OF_MONTH);
		c.add(Calendar.DATE, dias);
		int dia2 = c.get(Calendar.DAY_OF_MONTH);
		if (dia1 == dia2) {
			// Adiciona novamente o dia porque o Horário de Verão não troca de
			// dia
			// Ex: 10/10/2009, fica para 10/10/2009 - 23:00:00
			c.add(Calendar.DATE, dias);
		}

		// zera as horas
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.HOUR_OF_DAY, 0);

		Date data = c.getTime();
		return data;
	}

	public static String getMesDesc(int mes) {
		return MESES_COMPLETOS[mes - 1];
	}

	public static String getMesDescAbrev(int mes) {
		return MESES_ABREVIADOS[mes - 1];
	}

	public static boolean isMaiorToday(Date d) {
		// Calendar today = Calendar.getInstance();
		// today.setTime(new Date());
		// setTimeClearHours(today);
		//
		// Calendar other = Calendar.getInstance();
		// other.setTime(d);
		//
		// boolean b = isMaior(other.getTime(), today.getTime());
		//
		// return b;

		boolean b = DateUtils.compareTo(d, new Date(), "yyyyMMdd HH:mm:s") > 0;
		return b;
	}

	public static boolean isMenorToday(Date d) {
		Calendar today = Calendar.getInstance();
		today.setTime(new Date());
		setTimeClearHours(today);

		Calendar other = Calendar.getInstance();
		other.setTime(d);

		boolean b = isMenor(other.getTime(), today.getTime());

		return b;
	}

	/*
	 * public static void main(String[] args) { Date d27 =
	 * DateUtils.toDate("27/01/2015"); Date d28 =
	 * DateUtils.toDate("28/01/2015"); Date d29 =
	 * DateUtils.toDate("29/01/2015"); System.out.println(d27 + " - " +
	 * DateUtils.isMaiorToday(d27) + " - " + DateUtils.isMenorToday(d27));
	 * System.out.println(d28 + " - " + DateUtils.isMaiorToday(d28) + " - " +
	 * DateUtils.isMenorToday(d28)); System.out.println(d29 + " - " +
	 * DateUtils.isMaiorToday(d29) + " - " + DateUtils.isMenorToday(d29)); }
	 */

	public static boolean isMaior(Date dateA, Date dateB, String pattern) {
		return compareTo(dateA, dateB, pattern) > 0;
	}

	public static boolean isIgual(Date dateA, Date dateB, String pattern) {
		return compareTo(dateA, dateB, pattern) == 0;
	}

	public static boolean isMenor(Date dateA, Date dateB, String pattern) {
		return compareTo(dateA, dateB, pattern) < 0;
	}

	public static boolean isMaior(Date dateA, Date dateB) {
		return compareTo(dateA, dateB) > 0;
	}

	public static boolean isIgual(Date dateA, Date dateB) {
		return compareTo(dateA, dateB) == 0;
	}

	public static boolean isMenor(Date dateA, Date dateB) {
		return compareTo(dateA, dateB) < 0;
	}

	/**
	 * Retorna o numero de dias uteis no mes que ocorreram antes desta Data
	 * 
	 * @param mes
	 *            Jan = 1
	 * @return
	 */
	public static int getDiasUteisAteDia(Date dateFim) {
		int diaFim = getDia(dateFim);
		int mes = getMes(dateFim);

		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, mes - 1);
		int dias = 0;
		for (int i = 1; i < diaFim; i++) {
			c.set(Calendar.DAY_OF_MONTH, i);
			if (isDiaSemana(c.getTime())) {
				dias++;
			}

		}
		return dias;
	}

	/**
	 * Retorna o numero de dias uteis no mes que ainda tem depois desta Data
	 * 
	 * @param dataInicio
	 * @return
	 */
	public static int getDiasUteisDepoisDia(Date dataInicio) {
		int diaInicio = getDia(dataInicio);
		int mes = getMes(dataInicio);

		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, mes - 1);
		int dias = 0;
		int diasMes = ULTIMA_DIA_MES[mes - 1];
		for (int i = diaInicio + 1; i <= diasMes; i++) {
			c.set(Calendar.DAY_OF_MONTH, i);
			if (isDiaSemana(c.getTime())) {
				dias++;
			}

		}
		return dias;
	}

	/**
	 * Retorna o numero de dias uteis no mes
	 * 
	 * @param mes
	 *            Jan = 1
	 * @return
	 */
	public static int getDiasUteis(int mes) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, mes - 1);
		int dias = 0;
		int diasMes = ULTIMA_DIA_MES[mes - 1];
		for (int i = 1; i <= diasMes; i++) {
			c.set(Calendar.DAY_OF_MONTH, i);
			if (isDiaSemana(c.getTime())) {
				dias++;
			}

		}
		return dias;
	}

	/**
	 * É domingo?
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isDomingo(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dia = c.get(Calendar.DAY_OF_WEEK);

		boolean b = dia == Calendar.SUNDAY;

		return b;
	}

	/**
	 * É sábado?
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isSabado(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dia = c.get(Calendar.DAY_OF_WEEK);

		boolean b = dia == Calendar.SATURDAY;

		return b;
	}

	/**
	 * Verifica se a data é de 2ª-feira a 6ª-feira
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isDiaSemana(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dia = c.get(Calendar.DAY_OF_WEEK);

		switch (dia) {
		case Calendar.MONDAY:
		case Calendar.TUESDAY:
		case Calendar.WEDNESDAY:
		case Calendar.THURSDAY:
		case Calendar.FRIDAY:
			return true;
		}

		return false;
	}

	public static boolean isToday(long time) {
		Calendar c1 = Calendar.getInstance();
		c1.setTime(new Date());

		Calendar c2 = Calendar.getInstance();
		c2.setTime(new Date(time));

		boolean day = c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR);
		boolean month = c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH);
		boolean year = c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR);
		if (day && month && year) {
			return true;
		}

		return false;
	}

	public static boolean isYesterday(long time) {
		// Yesterday
		Calendar c1 = Calendar.getInstance();
		c1.setTime(new Date());
		c1.add(Calendar.DAY_OF_YEAR, -1);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(new Date(time));

		boolean day = c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR);
		boolean month = c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH);
		boolean year = c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR);
		if (day && month && year) {
			return true;
		}

		return false;
	}

	public static boolean isTomorrow(long time) {
		// Yesterday
		Calendar c1 = Calendar.getInstance();
		c1.setTime(new Date());
		c1.add(Calendar.DAY_OF_YEAR, +1);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(new Date(time));

		boolean day = c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR);
		boolean month = c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH);
		boolean year = c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR);
		if (day && month && year) {
			return true;
		}

		return false;
	}
	
	/**
	 * Se for Hoje: 17:15
	 * Se for Ontem: Ontem
	 * Se for 10/Ago: 10/Ago
	 * 
	 * @param dt
	 * @return
	 */
	public static String toDateChat(Date dt) {
		if(dt == null){
			return "";
		}
		String hora = DateUtils.toString(dt, "HH:mm", Livecom.getLocale());;

		long timestamp = dt.getTime();
		if (br.livetouch.livecom.utils.DateUtils.isToday(timestamp)) {
//			return "Hoje";
			return hora;
		} else if (br.livetouch.livecom.utils.DateUtils.isYesterday(timestamp)) {
			return "Ontem" ;
		} else {
			String dataStr = DateUtils.toString(dt, "dd/MMM", Livecom.getLocale());
			return dataStr;
		}
	}
	
	/**
	 * Se for Hoje: 17:15
	 * Se for Ontem: Ontem - 17:15
	 * Se for 10/Ago: 10/Ago - 17:15
	 * 
	 * @param dt
	 * @return
	 */
	public static String toDateChatFull(Date dt) {
		if(dt == null){
			return "";
		}
		String hora = DateUtils.toString(dt, "HH:mm", Livecom.getLocale());;

		long timestamp = dt.getTime();
		if (br.livetouch.livecom.utils.DateUtils.isToday(timestamp)) {
//			return "Hoje";
			return hora;
		} else if (br.livetouch.livecom.utils.DateUtils.isYesterday(timestamp)) {
			return "Ontem - " + hora;
		} else {
			String dataStr = DateUtils.toString(dt, "dd/MMM", Livecom.getLocale());
			return dataStr + " - " + hora;
		}
	}

	public static String toDateStringHojeOntem(Date dt) {
		String dataStr = "Hoje";
		if (dt != null) {
			long timestamp = dt.getTime();

			if (br.livetouch.livecom.utils.DateUtils.isToday(timestamp)) {
				dataStr = "Hoje";
			} else if (br.livetouch.livecom.utils.DateUtils.isYesterday(timestamp)) {
				dataStr = "Ontem";
			} else {
				dataStr = DateUtils.toString(dt, "dd/MMM", Livecom.getLocale());
			}
		}
		return dataStr;
	}
	
	public static String toDateStringChat(Date dt) {
		String diaSemana = null;
        Date dataAtual = DateUtils.toDate(DateUtils.getDate());
        
        int diferencaDias = (int) DateUtils.getDiffInDays(dt, dataAtual);

        switch (diferencaDias) {
            case 0:
                diaSemana = "hoje";
                //Se estiver em um intervalo de 14h, mas ja é ontem
                if(DateUtils.isMenorToday(dt)) {
                	 diaSemana = "ontem";
                }
                return diaSemana;
            case 1:
                diaSemana = "ontem";
                return diaSemana;
        }
        
        if (diferencaDias < 7) {
            int dia = DateUtils.getDiaSemana(dt);
            switch (dia) {
                case 1: {
                    diaSemana = "domingo";
                    break;
                }
                case 2: {
                    diaSemana = "segunda-feira";
                    break;
                }
                case 3: {
                    diaSemana = "terça-feira";
                    break;
                }
                case 4: {
                    diaSemana = "quarta-feira";
                    break;
                }
                case 5: {
                    diaSemana = "quinta-feira";
                    break;
                }
                case 6: {
                    diaSemana = "sexta-feira";
                    break;
                }
                case 7: {
                    diaSemana = "sábado";
                    break;
                }
            }
        } else {
            diaSemana = DateUtils.toString(dt, "dd 'de' MMMM 'de' yyyy", new Locale("pt","BR"));
        }
        return diaSemana;
	}

	public static String toDateStringHojeOntemHora(Date dt) {
		String dataStr = "Hoje";
		if (dt != null) {
			long timestamp = dt.getTime();

			if (br.livetouch.livecom.utils.DateUtils.isToday(timestamp)) {
				dataStr = "Hoje";
			} else if (br.livetouch.livecom.utils.DateUtils.isYesterday(timestamp)) {
				dataStr = "Ontem";
			} else {
				dataStr = net.livetouch.extras.util.DateUtils.toString(dt, "dd MMM 'às' HH'h'mm");
			}
		}
		return dataStr;
	}

	public static int getCurrentYear() {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		return c.get(Calendar.YEAR);
	}

	public static String toDateStringTimeAgo(Date dt) {
		PrettyTime p = new PrettyTime();
		String s = p.format(dt);
		return s;
	}

	public static String toString(Date date, SimpleDateFormat format) {
		if (date == null) {
			return "";
		}

		return format.format(date);
	}

	public static java.sql.Date toSqlDate(Date date) {
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		return sqlDate;
	}
	
	public static java.sql.Time toSqlTime(Date date) {
		java.sql.Time sqlDate = new java.sql.Time(date.getTime());
		return sqlDate;
	}
	
	public static java.sql.Timestamp toSqlTimestamp(Date date) {
		java.sql.Timestamp sqlDate = new java.sql.Timestamp(date.getTime());
		return sqlDate;
	}

	public static Date sameWeekDayOtherYear(Date data, int ano) {

		int anoReferencia = new DateTime(data).getYear();
		Date anterior = new DateTime(data).minusYears(ano - anoReferencia).toDate();

		int dia = new DateTime(data).getDayOfWeek();
		int diaAnterior = new DateTime(anterior).getDayOfWeek();

		int valor = dia - diaAnterior;
		valor = valor < 0 ? valor * -1 : valor;

		Date dataCalculada = new DateTime(anterior).plusDays(valor).toDate();
		return dataCalculada;

	}
	
	/**
	 * 
	 * @param data
	 * @return Data ano anterior a data de referencia
	 */
	public static Date sameWeekDayOtherYear(Date data) {

		Date anterior = new DateTime(data).minusYears(1).toDate();

		int dia = new DateTime(data).getDayOfWeek();
		int diaAnterior = new DateTime(anterior).getDayOfWeek();

		int valor = dia - diaAnterior;
		valor = valor < 0 ? valor * -1 : valor;

		Date dataCalculada = new DateTime(anterior).plusDays(valor).toDate();
		return dataCalculada;

	}

	public static Date toDate(long now) {
		if(now <= 0) {
			return null;
		}
		Date dt = new Date();
		dt.setTime(now);
		return dt;
	}
	
	public static long getTimeMilisecond() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		return calendar.getTimeInMillis();
	}
}
