package br.livetouch.livecom.domain.vo;

/**
 * Esta é a badge do chat usada em /ws/conversas.htm
 * 
 * Existe uma badge desta por conversa.
 * 
 * @author rlech
 *
 */
public class BadgeChatVO {
// TODO essa classe pode ser removida? nao, esta sendo usada no chat, podemos apenas remover a deixar badges direto como inteiro
	public long mensagens;
}
