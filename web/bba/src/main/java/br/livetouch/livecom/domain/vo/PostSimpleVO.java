package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.Post;

public class PostSimpleVO implements Serializable {
	private static final long serialVersionUID = 1L;

	public Long id;

	public String titulo;
	
	public String mensagem;

	public Boolean html;

	public PostSimpleVO() {
		
	}
	
	public void setPost(Post post) {
		if (post != null) {
			this.id = post.getId();
			this.titulo = post.getTitulo();
			this.mensagem = post.getMensagem();
			this.html = post.isHtml();
		}
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	

	
}
