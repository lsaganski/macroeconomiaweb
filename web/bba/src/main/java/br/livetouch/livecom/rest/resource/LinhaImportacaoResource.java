package br.livetouch.livecom.rest.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.service.LinhaImportacaoService;
import br.livetouch.livecom.domain.vo.LinhaImportacaoVO;

@Path("/linhaImportacao")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LinhaImportacaoResource extends MainResource {

	protected static final Logger log = Log.getLogger(LinhaImportacaoResource.class);

	@Context
	private SecurityContext securityContext;

	@Autowired
	protected LinhaImportacaoService linhaImportacaoService;

	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public LinhaImportacaoVO getLinhaImportacao(@QueryParam("id") Long id) {
		LinhaImportacaoVO vo = linhaImportacaoService.findAllVOByLogId(id);
		return vo;
	}

}
