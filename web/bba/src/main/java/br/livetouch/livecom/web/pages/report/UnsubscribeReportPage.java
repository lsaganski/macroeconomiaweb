package br.livetouch.livecom.web.pages.report;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.extras.util.DateUtils;

@Controller
@Scope("prototype")
public class UnsubscribeReportPage extends RelatorioPage {

	public RelatorioFiltro filtro;
	public int page;
	
	public Long empresaId;
	public String urlNewsletter;
	
	@Override
	public void onInit() {
		super.onInit();
		empresaId = getUserInfo().getEmpresa().getId();
		urlNewsletter = ParametrosMap.getInstance(empresaId).get("url.node.newsletter", "http://localhost:3001/api/newsletter");
		
		if (clear) {
			getContext().removeSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			newFilter();
		} else {
			filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			if (filtro != null) {
				if (filtro.getUsuarioId() != null) {
					Usuario usuario = usuarioService.get(filtro.getUsuarioId());
					if (usuario != null) {
						filtro.setUsuario(usuario);
					}
				}
				if (filtro.getPostId() != null) {
					Post post = postService.get(filtro.getPostId());
					if (post != null) {
						filtro.setPost(post);
					}
				}
				if (filtro.getCategoriaId() != null) {
					CategoriaPost categoria = categoriaPostService.get(filtro.getCategoriaId());
					if (categoria != null) {
						filtro.setCategoria(categoria);
					}
				}
			}else{
				newFilter();
			}
		}
	}
	void newFilter(){
		filtro = new RelatorioFiltro();
		filtro.setDataIni(DateUtils.toString(br.livetouch.livecom.utils.DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
		filtro.setDataFim(DateUtils.toString(new Date(), "dd/MM/yyyy"));
	}
}
