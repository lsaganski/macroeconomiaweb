package br.livetouch.livecom.web.pages.admin;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.pages.MuralPage;


@Controller
@Scope("prototype")
public class DataSourcePage extends LivecomAdminPage {

	@Override
	public String getTemplate() {
		return getPath();
	}
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}
	
	public void onGet() {
		
		if (applicationContext != null) {
			Object ds = (Object) applicationContext.getBean("dataSource");
			if (ds != null) {
				addModel("ds", ds);
				
				Field[] fields = ds.getClass().getDeclaredFields();
				
				Map<String, String> map = new TreeMap<>();
				for (Field field : fields) {
					try {
						String value = BeanUtils.getProperty(ds, field.getName());
						map.put(field.getName(), value);
					} catch (Exception e) {
					}
				}
				
				addModel("mapKeys", map.keySet());
				addModel("mapValues", map);
			}
			
			
			
		}
	}
}
