package br.livetouch.livecom.web.pages;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Amizade;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusAmizade;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.CamposMap;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.ImageField;
import net.livetouch.click.control.link.Link;
import net.sf.click.control.FileField;
import net.sf.click.control.Form;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class UsuarioPage extends LivecomLogadoPage {

	@Autowired
	protected UsuarioService usuarioService;

	public Form form = new Form();
	public String msgErro;
	public Long id;

	public int page;

	public Usuario usuario;

	private TextField ids;

	private TextField tGrupos;

	public List<Grupo> grupos;

	public CamposMap campos;

	/**
	 * Se o web service enviar este id, atualiza a foto com o arquivo.
	 */
	public Long arquivoFotoId;
	
	public Link desativarUsuario = new Link("desativar", "desativar", this, "desativar");
	public Link ativarUsuario = new Link("ativar", "ativar", this, "ativar");

	public boolean novoUsuario;
	public boolean chatOn = false;
	public Long notificationId;
	
	public boolean amigo;
	public boolean solicitacao;

	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;

		initGetUsuario();

		campos = getCampos();

		if (clear) {
			getContext().removeSessionAttribute(Usuario.SESSION_FILTRO_KEY);
		}
		form();

		if (usuario != null) {
			
			chatOn = Livecom.getInstance().isUsuarioLogadoChat(usuario.getId());
			
			StringBuilder sb = new StringBuilder("");
			grupos = usuarioService.getGrupos(usuario);
			boolean firstTime = true;
			for (Grupo g : grupos) {
				if (!firstTime) {
					sb.append(",");
				}
				sb.append(" ").append(g.getId());
				firstTime = false;
			}
			sb.append(",");
			ids.setValue(sb.toString());
			
			amizade();
			
			form.copyFrom(usuario);

		}
	}

	private void amizade() {
		Usuario user = getUsuario();
		Amizade amizade = usuarioService.getAmizade(usuario, user);
		
		if(amizade != null) {
			amigo = true;
			StatusAmizade status = amizade.getStatusAmizade();
			if(StatusAmizade.SOLICITADA.equals(status)) {
				if(user.getId().equals(amizade.getAmigo().getId())) {
					solicitacao = true;
				}
			}
		}
	}

	protected void initGetUsuario() {
		if (id != null) {
			usuario = usuarioService.get(id);
		} else {
			novoUsuario = true;
		}
	}

	public void form() {
		form.add(new IdField());

		// ids dos grupos
		ids = new TextField("ids");
		ids.setAttribute("style", "display:none");
		if (isCampoObrigatorioCadastroAdmin(campos, "grupos")) {
			ids.setRequired(true);
		}
		form.add(ids);

		// Grupos
		tGrupos = new TextField("grupos");
		tGrupos.setSize(650);
		form.add(tGrupos);

		form.add(new FileField("file"));
		form.add(new ImageField("foto"));

	}

	@Override
	public void onGet() {
		super.onGet();

	}

	@Override
	public void onRender() {
		super.onRender();

		onRenderFindUsers();
	}

	protected void onRenderFindUsers() {
	}
	
}
