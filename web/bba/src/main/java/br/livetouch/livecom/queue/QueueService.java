package br.livetouch.livecom.queue;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class QueueService {
	//http://www.concretepage.com/java/newsinglethreadexecutor_java
	static final ExecutorService executor = Executors.newSingleThreadExecutor();
	
	private static final QueueService instance = new QueueService();
	
	private QueueService() {
		
	}
	
	public static QueueService getInstance() {
		return instance;
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			Thread t = new Thread("Thread:"+(i+1)) {
				public void run() {
					
					while(true) {
						int random = new Random().nextInt(100);
						final String cmd = getName()+"_"+random;
						
						executor.execute(new Runnable() {
							@Override
							public void run() {
								System.out.println("Exec: " + cmd);
							}
						});
						
						try {
							sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					
				};
			};
			t.start();
		}
	}
	
	public void stop() {
		executor.shutdownNow();
	}
}
