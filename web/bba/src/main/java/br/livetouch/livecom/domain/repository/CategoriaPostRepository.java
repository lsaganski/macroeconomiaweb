package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.CategoriaPostAuditoria;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.vo.CategoriaVO;

@Repository
public interface CategoriaPostRepository extends net.livetouch.tiger.ddd.repository.Repository<CategoriaPost> {

	int MAX = 20;

	CategoriaPost findPadrao(Empresa empresa);

	List<CategoriaPost> findAllDefault(Empresa empresa);

	List<CategoriaPost> findAllFetchChildren(Empresa empresa);

	List<CategoriaPost> findAllParent(Empresa empresa);

	List<CategoriaPost> findAllParentByIdioma(Empresa empresa, List<Idioma> idiomas);

	List<CategoriaPost> findAllChildren(Empresa empresa);

	boolean exists(String nome, Empresa empresa);

	long countPosts(CategoriaPost c, Empresa empresa);

	void saveOrUpdate(CategoriaPostAuditoria a);

	List<CategoriaPost> findAllByNomeLikeWithMax(String nome, int max2, Empresa empresa);

	List<CategoriaPost> findAll(Empresa empresa);

	List<CategoriaVO> findByIdioma(Empresa empresa, Idioma idioma);

	CategoriaPost findByName(CategoriaPost categoria, Empresa empresa);

	List<CategoriaPost> findMenuCategoria(Empresa e);

	CategoriaPost findByCodigo(Empresa empresa, String codigo);

	void delete(List<CategoriaPost> categorias);

	void deleteArquivos(Long categoriaId, List<Long> arquivosIds);

	List<CategoriaPost> findByParent(CategoriaPost pai);
	List<CategoriaPost> findByParentAndIdiomas(CategoriaPost pai, List<Idioma> idiomas);
	List<CategoriaPost> findParentByIdiomas(List<Idioma> idiomas);
	List<CategoriaPost> findByParent(CategoriaVO pai);

	List<CategoriaPost> findByNameAndIdioma(String nome, Idioma idioma, Empresa empresa);

	List<CategoriaPost> findByIdsAndIdioma(Idioma idioma, List<Long> categoriaIds);

	
}