package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Rate;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

public interface RateService extends Service<Rate> {

	List<Rate> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Rate f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Rate f) throws DomainException;

	List<Rate> findAllByUser(Usuario u);

	Rate findByUserPost(Usuario u, Post p);
	
	public int findMedia(Post p);

	Long countByPost(Post p);

}
