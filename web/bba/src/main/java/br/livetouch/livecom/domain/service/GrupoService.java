package br.livetouch.livecom.domain.service;

import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoSubgrupos;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusParticipacao;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.vo.Filtro;
import br.livetouch.livecom.domain.vo.GrupoFilter;
import br.livetouch.livecom.domain.vo.GrupoUsuariosVO;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.UserToPushVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import net.livetouch.tiger.ddd.DomainException;

public interface GrupoService extends Service<Grupo> {

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Usuario userInfo,Grupo g) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo, Grupo g, boolean force) throws DomainException;

	Grupo findByNome(String nome,Usuario userInfo);
	List<Grupo> findAllByNomeLike(String nome,Usuario userInfo);
	Grupo findByCodigo(Grupo grupo,Usuario userInfo);

	long getCountByFilter(Grupo filtro,Usuario userInfo);

	List<GrupoUsuariosVO> findbyFilter(Grupo filtro, Usuario userInfo,int page, int pageSize) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	List<Usuario> findUsuarios(Grupo grupo);
	
	List<UserToPushVO> findUsers(Grupo grupo, Post p, TipoNotificacao tipoNotificacao);
	
	List<Object[]> findUsuariosArrayIdNome(Grupo grupo,Usuario userInfo);

	@Transactional(rollbackFor=Exception.class)
	void removeUsuario(Usuario userInfo, Grupo grupo, Usuario usuario);

	List<GrupoUsuarios> findMeusGrupos(GrupoFilter filter, Usuario user);
	
	List<GrupoVO> findGrupos(GrupoFilter filter, Usuario user);

	List<Grupo> findGruposParticipar(GrupoFilter filter, Usuario user, List<Grupo> grupos);

	List<Grupo> findAllByIds(List<Long> grupoIds);
	
	List<Grupo> findAllByCodigos(List<String> grupoCods);

	long countUsers(Grupo grupo);

	long countSubgrupos(Grupo grupo);

	List<Grupo> getGrupos(Usuario u);

	List<GrupoUsuariosVO> findGrupoUsuariosByUser(Usuario userInfo);
	List<GrupoUsuarios> findGrupoUsuariosByUsuario(Usuario userInfo);

	List<Grupo> findAllWithoutUsers(GrupoFilter filter, Usuario userInfo);

	void associarGrupoUsuario(Grupo grupo, boolean postar,Usuario... users) throws DomainException;

	
	List<Object[]> findIdCodGrupos(Long idEmpresa);

	List<Grupo> findAll(Empresa empresa, Integer page, Integer maxRows);
	List<GrupoVO> findAll(GrupoFilter filter, Usuario u);

	List<GrupoUsuariosVO> findUsuariosNaoAtivos(Usuario userInfo);

	List<GrupoUsuariosVO> findUsuariosByStatusAtivacao(String status, Usuario userInfo);

	List<UsuarioVO> findUsuariosByStatusParticipacao(StatusParticipacao status, Grupo g);

	List<Grupo> findAllDefault(Usuario userInfo);

	Grupo findDefaultByEmpresa(Long empresaId);

	List<Grupo> findAllByUser(Usuario u);

	String csvGrupos(Empresa empresa);

	List<Long> findUsuariosToPush(Grupo g, Post p, Idioma idioma);

	List<Long> findUsuariosReminder(Grupo g, Post p);

	List<Object[]> findIdNomeGrupos(Long empresaId);
	
	boolean isGruposDentroHorario(Usuario u);

	Grupo findByName(Grupo g, Empresa empresa);

	List<Grupo> getGrupos(GrupoFilter filter);

	List<Grupo> findAll(Empresa empresa);

	List<Long> findIdUsuarios(Grupo g);

	/**
	 * Grupos do post
	 */
	List<Grupo> findGruposByPost(Post post);

	List<GrupoVO> toListVO(List<Grupo> list);

	GrupoVO setGrupo(Grupo grupo);

	/**
	 * Qtde de posts deste grupo
	 */
	Long getCountPosts(Grupo g);
	
	List<Long> gruposUsuariosDentroHorario(List<Long> users);

	Long countPostsSomenteEu(Usuario user);

	Grupo findByNomeAndUser(String nome, Usuario user);

	@Transactional(rollbackFor=Exception.class)
	GrupoUsuarios participar(Usuario usuario, Grupo grupo, boolean participar) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void movePostsToAdminGrupo(Usuario usuario, Usuario admin, Grupo grupo) throws DomainException;

	boolean isAdminOrSubadminGrupo(Usuario usuario, Grupo grupo);

	boolean isAdmin(Usuario usuario, Grupo grupo);

	void criarNotificacao(GrupoUsuarios grupoUsuario, TipoNotificacao tipo) throws DomainException;

	/**
	 * Deve retornar true se o usuário está dentro deste grupo.
	 * 
	 * @param grupo
	 * @param user
	 * @return
	 */
	boolean isUsuarioDentroDoGrupo(Grupo grupo, Usuario user);
	
	GrupoUsuarios findByGrupoEUsuario(Grupo g, Usuario u);

	boolean existe(Grupo g, Empresa empresa, Usuario userInfo);

	GrupoSubgrupos getGrupoSubgrupo(Grupo grupo, Grupo subgrupo);

	@Transactional(rollbackFor = Exception.class)
	void saveSubgruposToGrupo(Usuario userInfo, Grupo grupo, List<Grupo> subgrupos) throws DomainException;
	
	@Transactional(rollbackFor = Exception.class)
	void saveOrUpdate(GrupoSubgrupos gs);

	void removeSubgruposToGrupo(Usuario userInfo, Grupo grupo, List<Grupo> subgrupos) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void removeSubgrupo(Usuario userInfo, Grupo grupo, Grupo subgrupo);

	List<Grupo> findSubgruposByGrupo(Grupo grupo);

	List<Grupo> findAllSubgrupos(Filtro filtro, Empresa empresa) throws Exception;

	List<Grupo> findAllSubgruposWithAcesso(Filtro filtro, Usuario userInfo) throws Exception;

	List<Grupo> findSubgruposInGrupos(Set<Grupo> grupos);

	void changePermissaoPostarUsers(Grupo grupo, boolean postar);

	List<Grupo> findGruposVirtuais(GrupoFilter filter, Usuario user, List<Grupo> list);

	List<Usuario> findSubadminsByGrupo(Grupo grupo);

	boolean hasAberto(Set<Grupo> grupos);
	
	Long countAll(GrupoFilter filter, Usuario userInfo);

	Long countFindGrupos(GrupoFilter filter, Usuario userInfo);

}
