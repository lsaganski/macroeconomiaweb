package br.livetouch.livecom.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.domain.enums.GrupoNotification;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import net.livetouch.extras.util.DateUtils;

@Entity
@Table(indexes = {
		@Index(columnList = "tipo", name = "type_notification"),
		@Index(columnList = "grupoNotification", name = "grupo_notification")
})
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Notification extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;
	public static final String KEY = "Notification";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "NOTIFICATION_SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario usuario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id", nullable = true)	
	private Post post;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idioma_id", nullable = true)	
	private Idioma idioma;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "comentario_id", nullable = true)
	private Comentario comentario;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mensagem_id", nullable = true)
	private Mensagem mensagem;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "arquivo_id", nullable = true)
	private Arquivo arquivo;
	
	@OneToMany(mappedBy = "notification", fetch = FetchType.LAZY)
	private Set<NotificationUsuario> notifications;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "grupo_id", nullable = true)
	private Grupo grupo;

	private String titulo;
	private String texto;
	private String textoNotificacao;
	//TODO ver se podemos remover esses campos abaixo
	private String tituloNot;
	private String subTituloNot;
	private boolean visivel;
	//---------------------------//
	
	@Enumerated(EnumType.STRING)
	private TipoNotificacao tipo;
	
	@Enumerated(EnumType.STRING)
	private GrupoNotification grupoNotification;
	private Date data;
	private Date dataPublicacao;
	
	/**
	 * Atualmente uma notifition é publicada quando o seu Post é publicado para controlar o status de agendado/publicado/expirado.
	 * 
	 * Para notificacões que nao possuem Post, ex (Usuario entrou/saiu do grupo), vale este status
	 */
	private Boolean publicado;
	
	private boolean sendPush;
	private Date dataPush;

	private Long pushId;
	
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDataString() {
		return DateUtils.toString(data, "dd/MM/yyyy HH:mm:ss");
	}
	
	public String getDataPublicacaoStringHojeOntem() {
		return br.livetouch.livecom.utils.DateUtils.toDateStringHojeOntem(dataPublicacao);
	}
	
	public String getDataPublicacaoString() {
		return DateUtils.toString(dataPublicacao, "dd/MM/yyyy HH:mm:ss");
	}
	
	public String getDataStringHojeOntem() {
		return br.livetouch.livecom.utils.DateUtils.toDateStringHojeOntem(data);
	}
	
	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public Comentario getComentario() {
		return comentario;
	}

	public void setComentario(Comentario comentario) {
		this.comentario = comentario;
	}

	public Mensagem getMensagem() {
		return mensagem;
	}

	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Date getData() {
		return data;
	}
	
	public Date getDataPublicacao() {
		return dataPublicacao;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public void setDataPublicacao(Date dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}
	
	public boolean isVisivel() {
		return visivel;
	}
	public void setVisivel(boolean visivel) {
		this.visivel = visivel;
	}
	
	public TipoNotificacao getTipo() {
		return tipo;
	}
	public void setTipo(TipoNotificacao tipo) {
		this.tipo = tipo;
	}
	
	public boolean isSendPush() {
		return sendPush;
	}
	
	public void setSendPush(boolean sendPush) {
		this.sendPush = sendPush;
	}
	public void setDataPush(Date dataPush) {
		this.dataPush = dataPush;
	}
	
	public Date getDataPush() {
		return dataPush;
	}
	
	public String getTituloNot() {
		return tituloNot;
	}

	public void setTituloNot(String tituloNot) {
		this.tituloNot = tituloNot;
	}

	public String getSubTituloNot() {
		return subTituloNot;
	}

	public void setSubTituloNot(String subTituloNot) {
		this.subTituloNot = subTituloNot;
	}

	public String getTextoNotificacao() {
		return textoNotificacao;
	}

	public void setTextoNotificacao(String textoNotificacao) {
		this.textoNotificacao = textoNotificacao;
	}
	
	public void setPublicado(Boolean publicado) {
		this.publicado = publicado;
	}
	
	public boolean isPublicado() {
		if(publicado == null) {
			return false;
		}
		return publicado;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public Long getPushId() {
		return pushId;
	}

	public void setPushId(Long pushId) {
		this.pushId = pushId;
	}

	public GrupoNotification getGrupoNotification() {
		return grupoNotification;
	}

	public void setGrupoNotification(GrupoNotification grupoNotification) {
		this.grupoNotification = grupoNotification;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Set<NotificationUsuario> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<NotificationUsuario> notifications) {
		this.notifications = notifications;
	}
	
	@Override
	public String toString() {
		return id + " - " + titulo;
	}

	public Idioma getIdioma() {
		return idioma;
	}

	public void setIdioma(Idioma idioma) {
		this.idioma = idioma;
	}
}
