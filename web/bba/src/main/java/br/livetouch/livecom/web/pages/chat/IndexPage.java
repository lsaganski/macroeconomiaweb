package br.livetouch.livecom.web.pages.chat;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;

@Controller
@Scope("prototype")
public class IndexPage extends LivecomLogadoPage {
	
	@Override
	public String getTemplate() {
		return getPath();
	}
	
}
