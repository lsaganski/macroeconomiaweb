package br.livetouch.livecom.domain.vo;

import java.io.InputStream;
import java.io.Serializable;

public class FileInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	private InputStream in;

	private String nome;

	public FileInfo(){}

	public FileInfo(String fileName, InputStream in) {
		super();
		this.in = in;
		this.nome = fileName;
	}

	public InputStream getIn() {
		return in;
	}

	public void setIn(InputStream in) {
		this.in = in;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String fileName) {
		this.nome = fileName;
	}


}
