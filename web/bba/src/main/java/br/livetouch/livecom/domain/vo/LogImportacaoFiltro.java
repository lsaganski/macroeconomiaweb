package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;

import net.livetouch.extras.util.DateUtils;

public class LogImportacaoFiltro implements Serializable {

	private static final long serialVersionUID = -8800402609370460617L;

	public static final String SESSION_FILTRO_KEY = "logImportacaoFiltro";

	private Date dataInicial;
	private Date dataFinal;
	private int page;
	private int max;
	private String arquivoNome;
	private Long id;

	public LogImportacaoFiltro(Date dataInicial, Date dataFinal, int page, int max, String arquivo) {
		super();
		this.dataInicial = dataInicial;
		this.dataFinal = dataFinal;
		this.page = page;
		this.max = max;
		this.setArquivoNome(arquivo);
	}

	public LogImportacaoFiltro() {

	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public String getDataInicialStringDMY() {
		return DateUtils.toString(dataInicial, "dd/MM/yyyy");
	}

	public String getDataFinalStringDMY() {
		return DateUtils.toString(dataFinal, "dd/MM/yyyy");
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public String getArquivoNome() {
		return arquivoNome;
	}

	public void setArquivoNome(String arquivoNome) {
		this.arquivoNome = arquivoNome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
