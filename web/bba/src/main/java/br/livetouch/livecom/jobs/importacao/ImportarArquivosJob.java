package br.livetouch.livecom.jobs.importacao;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ImportarArquivoResponse;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.service.WorkService;
import br.livetouch.livecom.jobs.SpringJob;
import jxl.read.biff.BiffException;
import net.livetouch.extras.util.ExceptionUtil;
import net.livetouch.tiger.ddd.DomainException;

public class ImportarArquivosJob extends SpringJob {

	protected static final Logger log = Log.getLogger(ImportarArquivosJob.class);

	private static boolean running = false;
	
	@Autowired
	private WorkService workService;

	private JobInfo jobInfo;

	@Override
	protected void execute(Map<String, Object> params) {
		
		Collection<Long> allEmpresa = ParametrosMap.getAllEmpresas();
		if(allEmpresa == null || allEmpresa.size() == 0) {
			return;
		}
		
		for (Long idEmpresa : allEmpresa) {
			ParametrosMap parametrosMap = ParametrosMap.getInstance(idEmpresa);
			String importOn = parametrosMap.get(Params.IMPORTACAO_ARQUIVO_ON, "0");
			
			jobInfo = JobInfo.getInstance(idEmpresa);
			
			boolean importar = StringUtils.equals("1", importOn);
			if (!importar) {
				continue;
			}
			
			if (running) {
				log("Job de importação Arquivo já está executando");
				return;
			}

			jobInfo.lastDate = new Date();

			boolean precisaLogar = false;
			boolean logMoveOld = false;

			running = true;

			boolean error = false;
			try {

				log("Executando Work job: " + new Date());

				String entrada = ParametrosMap.getInstance(idEmpresa).get(Params.PASTAENTRADA);
				if (StringUtils.isEmpty(entrada)) {
					logError("Cadastre o parametro 'pastaEntrada' com o caminho da pasta", null);
					log("Job cancelado.");
					return;
				}
				File dir = new File(entrada);
				if (!dir.exists()) {
					logError("Dir de entrada [" + dir + "] nao encontrado.", null);
					log("Job cancelado.");
					return;
				}

				// Lista arquivos para debug
				File[] files = dir.listFiles();
				log("Diretório de entrada encontrado [" + dir + "], listando arquivos...");
				if (files != null && files.length > 0) {
					for (File f : files) {
						if (f.isDirectory()) {
							log("(*) Dir: " + f);
						} else {
							log("(*) Arquivo: " + f);
							try {
								if(JobImportacaoUtil.isImportFile(f)) {
									precisaLogar |= importar(dir, f);
								}
							} catch(IllegalArgumentException e) {
								logError("Não foi possível importar o arquivo", e);
								JobImportacaoUtil.moveToOldDir(dir, f);
							}
						}
					}
				}
				
				jobInfo.porcentagem = 0.0;
				
			} catch (Exception e) {
				error = true;
				logError("Erro ao importar arquivo: " + e.getMessage(), e);
			} catch (Throwable e) {
				error = true;
				logError("Throwable ao importar arquivo: " + e.getMessage(), e);
			} finally {

				running = false;

				log("Fim job, fechando log: " + new Date() + "\n ---\n");

				boolean logOn = error || logMoveOld || precisaLogar;

				jobInfo.finishLog(logOn);

				log("Fim job: " + new Date() + "\n ---\n");
			}
		}
	}

	/**
	 * @param arquivo
	 * @param files
	 * @throws DomainException
	 * @throws BiffException
	 * @throws IOException 
	 */
	protected boolean importar(File dir, File fileIn) throws DomainException, BiffException, IOException {
		if (fileIn == null) {
			return false;
		}
		if (!fileIn.exists()) {
			return false;
		}

		File f = JobImportacaoUtil.moveToOldDir(dir, fileIn);
		if(!f.exists()) {
			log.error("Erro ao mover aquivo para pasta /old");
			return false;
		}
		jobInfo.arquivo = f.getName();
		log("Arquivo com nome padrão identificado [" + f.getName() + "] ");

		ImportarArquivoResponse response = null;

		try {
			// usuario
			log("Importando [" + f + "]");

			// Importacao com JDBC
//			Session session = sessionFactory.getCurrentSession();
//			ImportacaoArquivoWork work = factoryWork.getImportacaoWork(session, f);
//			session.doWork(work);
//			response = work.getResponse();

			response = workService.execute_importarArquivo(f);

			log("Arquivo [" + f + "] importado com sucesso");
			if(response != null) {
				log("Count OK: " + response.countOk);
				log("Count Inserts: " + response.countInsert);
				log("Count Updates: " + response.countUpdate);
				log("Count Erro: " + response.countError);
				jobInfo.response = response;
				jobInfo.arquivo = "";
			}

			log("Fim importação [response=" + response + "], movendo arquivo...");

		} catch (IOException e) {
			jobInfo.temArquivo = false;
			logError("Erro ao importar arquivo: " + f.getName() + ": " + e.getMessage(), e);
		} catch (Throwable e) {
			jobInfo.temArquivo = false;
			logError("Throwable ao importar arquivo: " + f.getName() + ": " + e.getMessage(), e);
		} finally {
			jobInfo.finishLog(true);
			f.renameTo(new File(f.getParent(), JobImportacaoUtil.getNomeFileSemEmpresa(f)));
		}
		return false;

	}

	private void logError(String string, Throwable e) {
		log.error(string, e);
		jobInfo.log("ERROR: " + string + ", exception: " + ExceptionUtil.getStackTrace(e));
	}

	private void log(String s) {
		boolean showLog = "1".equals(ParametrosMap.getInstance().get(Params.IMPORTACAO_LOG_ON, "0"));
		if(showLog) {
			log.debug(s);
		}
		jobInfo.log(s);
	}

	public static class JobInfo {
		private static final HashMap<Long, JobInfo> mapJobInfo = new HashMap<Long, JobInfo>();
		
		public Date lastDate;
		public String lastMessage;
		public String lastMessageImportacao;
		public double porcentagem;
		public int atual;
		public int fim;
		public String status = "";
		public boolean temArquivo;
		public List<String> list = new ArrayList<String>();
		public List<String> tempList = new ArrayList<String>();
		public ImportarArquivoResponse response;
		public String arquivo;
		private String header;
		private boolean successful;
		private Empresa empresa;

		public void log(String string) {
			tempList.add(string);
			this.lastMessage = string;
		}

		public void finishLog(boolean importou) {
			// Collections.reverse(tempList);
			if (importou) {
				list.addAll(0, tempList);
			}
			tempList.clear();
		}

		public static JobInfo getInstance(Long empresaId) {
			if(empresaId == null) {
				empresaId = ParametrosMap.EMPRESA_LIVECOM;
			}
			JobInfo map = mapJobInfo.get(empresaId);
			if(map == null) {
				map = new JobInfo();
				mapJobInfo.put(empresaId, map);
			}
			return map;
		}
		
		public Date getLastDate() {
			return lastDate;
		}

		public String getLastMessage() {
			return lastMessage;
		}

		public String getLastMessageImportacao() {
			return lastMessageImportacao;
		}

		public List<String> getList() {
			return list;
		}

		public String getLog() {
			String log = "";
			for (String str : list) {
				log += str + "\n";
			}
			return log;
		}
		
		public double getPorcentagem() {
			return porcentagem;
		}

		public void setPorcentagem(double porcentagem) {
			this.porcentagem = porcentagem;
		}

		public int getAtual() {
			return atual;
		}

		public void setAtual(int atual) {
			this.atual = atual;
		}

		public int getFim() {
			return fim;
		}

		public void setFim(int fim) {
			this.fim = fim;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public boolean isTemArquivo() {
			return temArquivo;
		}

		public void setTemArquivo(boolean temArquivo) {
			this.temArquivo = temArquivo;
		}

		public ImportarArquivoResponse getResponse() {
			return response;
		}

		public void setResponse(ImportarArquivoResponse response) {
			this.response = response;
		}

		public String getArquivo() {
			return arquivo;
		}

		public void setArquivo(String arquivo) {
			this.arquivo = arquivo;
		}

		public String getHeader() {
			return header;
		}

		public void setHeader(String header) {
			this.header = header;
		}

		public boolean isSuccessful() {
			return successful;
		}

		public void setSuccessful(boolean successful) {
			this.successful = successful;
		}

		public Empresa getEmpresa() {
			return empresa;
		}

		public void setEmpresa(Empresa empresa) {
			this.empresa = empresa;
		}

	}
}
