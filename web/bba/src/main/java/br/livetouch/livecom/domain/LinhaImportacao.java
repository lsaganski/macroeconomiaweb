package br.livetouch.livecom.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LinhaImportacao extends net.livetouch.tiger.ddd.Entity {

	private static final long serialVersionUID = 8771734275567884350L;

	public static final long TEMPO_EXPIRAR_OTP = 60;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "LINHAIMPORTACAO_SEQ")
	private Long id;

	private String linha;

	@Column(columnDefinition = "clob")
	private String mensagem;
	
	private Integer numeroLinha;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "log_importacao_id", nullable = true)
	private LogImportacao logImportacao;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLinha() {
		return linha;
	}

	public void setLinha(String linha) {
		this.linha = linha;
	}

	public LogImportacao getLogImportacao() {
		return logImportacao;
	}

	public void setLogImportacao(LogImportacao logImportacao) {
		this.logImportacao = logImportacao;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Integer getNumeroLinha() {
		return numeroLinha;
	}

	public void setNumeroLinha(Integer numeroLinha) {
		this.numeroLinha = numeroLinha;
	}

}
