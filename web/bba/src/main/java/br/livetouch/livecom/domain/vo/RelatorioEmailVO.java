package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import br.livetouch.livecom.domain.EmailReport;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.DateUtils;

public class RelatorioEmailVO implements Serializable {

	private static final long serialVersionUID = 1524516747642576158L;
	
	private Long id;
	private String data;
	private String assunto;
	private String template;
	private String status;

	private String usuario;
	private Long usuarioId;

	public RelatorioEmailVO(EmailReport report) {
		this.id = report.getId();
		this.data = DateUtils.toString(report.getData(), "dd/MM/yyyy HH:mm:ss");
		this.template = report.getTipoTemplate() != null ? report.getTipoTemplate().toString() : "Normal";
		this.assunto = StringUtils.isNotEmpty(report.getAssunto()) ? report.getAssunto() : "Sem Assunto";
		this.status = report.isStatus() ? "Enviado" : "Não Enviado";
		
		Usuario user = report.getUsuario();
		this.setUsuario(user != null ? user.getLogin() : "Desconhecido");
		this.setUsuarioId(user != null ? user.getId() : 0);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}
}
