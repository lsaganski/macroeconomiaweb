package br.livetouch.livecom.web.pages.cadastro.permissoes;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.LivecomRootEmpresaPage;


/**
 * 
 * @author ricardo
 *
 */
@Controller
@Scope("prototype")
public class PermissaoPage extends LivecomRootEmpresaPage {

	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
	}
	
}
