package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.utils.DateUtils;

public class TemplateVO implements Serializable{
	private static final long serialVersionUID = 511847560868136130L;
	public Long id;
	public String nome;
	public String nomeHtml;
	public String html;
	public String dateCreated;
	public String dateupdated;
	public String tipo;
	
	public static List<TemplateVO> fromList(List<TemplateEmail> templates) {
		List<TemplateVO> vos = new ArrayList<>();
		if(templates == null) {
			return vos;
		}
		for (TemplateEmail t : templates) {
			vos.add(new TemplateVO(t));
		}
		return vos;
	}
	
	public TemplateVO(TemplateEmail template) {
		this.id = template.getId();
		this.nome = template.getNome();
		this.nomeHtml = template.getNomeHtml();
		this.html = template.getHtml();
		this.dateupdated = template.getDateUpdated() != null ? DateUtils.toString(template.getDateUpdated()) : "";
		this.dateCreated = template.getDateCreated() != null ? DateUtils.toString(template.getDateCreated()) : "";
		this.tipo = template.getTipoTemplate() != null ? template.getTipoTemplate().toString() : "";
	}
}
