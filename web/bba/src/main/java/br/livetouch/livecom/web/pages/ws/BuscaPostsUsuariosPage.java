package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class BuscaPostsUsuariosPage extends MuralPage {

	// deprecated
	// usar o mural
	
}