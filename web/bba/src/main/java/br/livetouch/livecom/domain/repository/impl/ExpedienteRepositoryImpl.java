package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Expediente;
import br.livetouch.livecom.domain.repository.ExpedienteRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
@SuppressWarnings("unchecked")
public class ExpedienteRepositoryImpl extends StringHibernateRepository<Expediente>implements ExpedienteRepository {

	public ExpedienteRepositoryImpl() {
		super(Expediente.class);
	}

	@Override
	public Expediente findByNome(String codigo) {
		List<Expediente> list = createQuery("from Expediente where codigo=?")
		.setString(0, codigo).list();
		Expediente expediente = list.isEmpty() ? null : list.get(0);
		return expediente;
	}

	@Override
	public List<Object[]> findIdNomeExpediente() {
		StringBuffer sb = new StringBuffer("select id,codigo from Expediente e where 1=1");
		Query q = createQuery(sb.toString());
		return q.list();
	}
	
	@Override
	public Expediente findByCodigo(Expediente expediente, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Expediente e where e.codigo = :codigo");
		
		if(expediente.getId() != null) {
			sb.append(" and e.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("codigo", expediente.getCodigo());
		
		if(expediente.getId() != null) {
			q.setParameter("id", expediente.getId());
		}
		
		List<Expediente> list = q.list();
		Expediente e = (Expediente) (list.size() > 0 ? q.list().get(0) : null);
		return e;
	}

}
