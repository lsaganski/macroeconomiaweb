package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Favorito;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostUsuarios;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.GrupoNotification;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.repository.FavoritoRepository;
import br.livetouch.livecom.domain.service.FavoritoService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import br.livetouch.spring.SearchInfo;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class FavoritoServiceImpl implements FavoritoService {
	@Autowired
	private FavoritoRepository rep;

	@Autowired
	protected NotificationService notificationService;
	
	@Autowired
	protected UsuarioService usuarioService;

	@Autowired
	protected PostService postService;
	
	@Autowired
	protected GrupoService grupoService;

	@Override
	public Favorito favoritar(Usuario u, Post post, boolean favorited) throws DomainException {

		Favorito f = findByUserAndPost(u, post);
		if (f == null) {
			f = new Favorito();
			f.setData(new Date());
			f.setUsuario(u);
			f.setPost(post);
		}

		// Favoritar
		f.setFavorito(favorited);
		if (favorited) {
			f.setLidaNotification(false);
		}
		saveOrUpdate(f);

		if(favorited) {
			push(f);
		}

		return f;
	}

	private void push(Favorito f) throws DomainException {
		
		Usuario u = f.getUsuario();
		Post post = f.getPost();
		Usuario usuario = post.getUsuario();
		Empresa e = u.getEmpresa();
		
		
		Boolean isSend = false;
		Set<Grupo> grupos = new HashSet<Grupo>();
		grupos.addAll(post.getGrupos());
		List<Grupo> subgrupos = grupoService.findSubgruposInGrupos(post.getGrupos());
		if(subgrupos != null && subgrupos.size() > 0) {
			grupos.addAll(subgrupos);
		}
		
		if(grupos.isEmpty() && !u.getId().equals(usuario.getId())) {
			isSend = true;
		}
		
		for (Grupo grupo : grupos) {
			GrupoUsuarios grupoUsuario = usuarioService.getGrupoUsuario(usuario, grupo);
			if(grupoUsuario != null && grupoUsuario.isPush()) {
				isSend = true;
				break;
			}
		}
		
		isSend &= Livecom.getInstance().hasPermissao(ParamsPermissao.CURTIR_PUBLICACAO, usuario);
		
		if(isSend == false) {
			return;
		}

		String titulo = post.getTituloDesc();
		
		ParametrosMap params = ParametrosMap.getInstance(e);
		String texto = params.get(Params.MSG_PUSH_FAVORITO_TITULO, "[%s] favoritou seu post [%s]");
		texto = String.format(texto, u.getNome(), post.getTituloDesc());
		if (titulo == null) {
			texto = params.get(Params.MSG_PUSH_FAVORITO, "[%s] favoritou seu post");
			texto = String.format(texto, u.getNome());
		}
		
		String tituloNot = params.get(Params.PUSH_FAVORITO_TITULO_NOTIFICATION, "Post Favoritado");
		String subTituloNot = params.get(Params.PUSH_FAVORITO_SUB_TITULO_NOTIFICATION, "[%user%] favoritou seu post [%titulo%]");
		if(StringUtils.isNotEmpty(subTituloNot)) {
			subTituloNot = StringUtils.replace(subTituloNot, "%user%", u.getNome());
			subTituloNot = StringUtils.replace(subTituloNot, "%titulo%", post.getTituloDesc());
		}
		
		boolean pushOn = params.isPushOn();

		String textoNotificacao = u.getNome() + " favoritou a publicação " + titulo;
		
		// Notification
		Notification n = new Notification();
		n.setData(new Date());
		n.setTipo(TipoNotificacao.FAVORITO);
		n.setGrupoNotification(GrupoNotification.MURAL);
		n.setTitulo(titulo);
		n.setTexto(texto);
		n.setTituloNot(tituloNot);
		n.setSubTituloNot(subTituloNot);
		n.setTextoNotificacao(textoNotificacao);
		n.setPost(post);
		n.setSendPush(pushOn);
		n.setVisivel(true);
		n.setUsuario(u);
		
		PostUsuarios postUsuarios = postService.getPostUsuarios(usuario, post);
		if(postUsuarios != null) {
			n.setSendPush(postUsuarios.isPush());
			n.setVisivel(postUsuarios.isNotification());
		}
		notificationService.save(n);
		
		// salva a notification para ser enviada pelo job
		NotificationUsuario notUsuario = new NotificationUsuario();
		notUsuario.setUsuario(usuario);
		notUsuario.setNotification(n);
		notUsuario.setVisivel(true);
		
		
		PostUsuarios postUser = postService.getPostUsuarios(usuario, post);
		if(postUser != null && !postUser.isNotification()) {
			notUsuario.setLida(true);
		}
		
		notificationService.saveOrUpdate(notUsuario);
	}

	@Override
	public Favorito get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Favorito c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Favorito> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void delete(Favorito c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public Favorito findByUserAndPost(Usuario u, Post post) {
		return rep.findByUserAndPost(u,post);
	}

	@Override
	public List<Favorito> findAllByUser(Usuario userInfo) {
		return rep.findAllByUser(userInfo);
	}

	@Override
	public List<Favorito> findAllFavoritosNotificationByUser(Usuario u) {
		if(u == null) {
			return new ArrayList<Favorito>();
		}
		return rep.findAllFavoritosNotificationByUser(u);
	}

	@Override
	public Favorito findByUserAndFile(Usuario u, Arquivo file) {
		return rep.findByUserAndFile(u, file);
	}

	@Override
	public void clearNotification(Long id) throws DomainException {
		Favorito f = get(id);
		f.setLidaNotification(true);
		saveOrUpdate(f);
	}

	@Override
	public List<Post> findAllPostsByUser(Usuario userInfo, SearchInfo search) {
		return rep.findAllPostsByUser(userInfo,search);
	}

	@Override
	public List<Favorito> findAllByPosts(Usuario user, List<Long> ids) {
		return rep.findAllByPosts(user, ids);
	}
	
	@Override
	public List<RelatorioVisualizacaoVO> reportFavoritos(RelatorioFiltro filtro) throws DomainException {
		return rep.reportFavoritos(filtro);
	}
}
