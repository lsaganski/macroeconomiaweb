package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Application;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.rest.oauth.ConsumerKeyMap;
import net.livetouch.tiger.ddd.DomainException;

public interface ApplicationService extends Service<Application> {

	List<Application> findAll();

	@Transactional(rollbackFor = Exception.class)
	void saveOrUpdate(Application a) throws DomainException;

	@Transactional(rollbackFor = Exception.class)
	void delete(Application a) throws DomainException;

	void generateKeys(Application a) throws DomainException;

	List<Application> findByUsuario(Usuario usuario);

	Boolean exists(Application app);
	
	Application get(Long id, Usuario u);

	@Transactional(rollbackFor = Exception.class)
	void update(Application a) throws DomainException;

	Application find(String consumerKey, String consumerSecret);
	
	public ConsumerKeyMap init();

}
