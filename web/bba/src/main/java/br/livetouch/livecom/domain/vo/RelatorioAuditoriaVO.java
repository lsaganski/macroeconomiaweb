package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.DateUtils;

public class RelatorioAuditoriaVO implements Serializable {

	private static final long serialVersionUID = 1524516747642576158L;
	
	private Long id;
	private String data;
	private String acao;
	private String entidade;
	private String mensagem;

	private String value1;
	private String value2;
	private String value3;
	private String value4;
	private String value5;
	private String value6;
	private String value7;

	private String usuario;
	private Long usuarioId;

	public RelatorioAuditoriaVO(LogAuditoria log) {
		this.id = log.getId();
		this.data = DateUtils.toString(log.getData(), "dd/MM/yyyy HH:mm:ss");
		this.acao = log.getAcao() != null ? log.getAcao().name() : "Desconhecida";
		this.entidade = log.getEntidade() != null ? log.getEntidade().name() : "Desconhecida";
		this.mensagem = StringUtils.isNotEmpty(log.getMensagem()) ? log.getMensagem() : "";

		this.value1 = StringUtils.isNotEmpty(log.getValue1()) ? log.getValue1() : "";
		this.value2 = StringUtils.isNotEmpty(log.getValue2()) ? log.getValue2() : "";
		this.value3 = StringUtils.isNotEmpty(log.getValue3()) ? StringUtils.normalizeSpace(log.getValue3()) : "";
		this.value4 = StringUtils.isNotEmpty(log.getValue4()) ? log.getValue4() : "";
		this.value5 = StringUtils.isNotEmpty(log.getValue5()) ? log.getValue5() : "";
		this.value6 = StringUtils.isNotEmpty(log.getValue6()) ? log.getValue6() : "";
		this.value7 = StringUtils.isNotEmpty(log.getValue7()) ? log.getValue7() : "";

		Usuario user = log.getUsuario();
		this.usuario = user != null ? user.getLogin() : "Desconhecido";
		this.usuarioId = user != null ? user.getId() : 0;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public String getEntidade() {
		return entidade;
	}

	public void setEntidade(String entidade) {
		this.entidade = entidade;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public static List<RelatorioAuditoriaVO> fromList(List<LogAuditoria> audits) {
		List<RelatorioAuditoriaVO> list = new ArrayList<>();
		if(audits != null && audits.size() > 0) {
			for (LogAuditoria log : audits) {
				list.add(new RelatorioAuditoriaVO(log));
			}
		}
		return list;
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	public String getValue3() {
		return value3;
	}

	public void setValue3(String value3) {
		this.value3 = value3;
	}

	public String getValue4() {
		return value4;
	}

	public void setValue4(String value4) {
		this.value4 = value4;
	}

	public String getValue5() {
		return value5;
	}

	public void setValue5(String value5) {
		this.value5 = value5;
	}

	public String getValue6() {
		return value6;
	}

	public void setValue6(String value6) {
		this.value6 = value6;
	}

	public String getValue7() {
		return value7;
	}

	public void setValue7(String value7) {
		this.value7 = value7;
	}

}
