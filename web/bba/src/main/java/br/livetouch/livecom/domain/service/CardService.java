package br.livetouch.livecom.domain.service;

import br.livetouch.livecom.domain.Card;
import br.livetouch.livecom.domain.CardButton;

public interface CardService extends Service<Card>{

	void saveOrUpdate(Card card);
	
	void saveOrUpdate(CardButton cardButton);

}
