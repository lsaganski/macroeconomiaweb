package br.livetouch.livecom.rest.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.infra.util.Utils;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.enums.TipoParametro;
import br.livetouch.livecom.domain.vo.BBAWebviewVO;
import br.livetouch.livecom.domain.vo.ParametroVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import br.livetouch.livecom.rest.request.ConfigMobileRequest;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/parametro")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class ParametroResource extends MainResource {

	protected static final Logger log = Log.getLogger(ParametroResource.class);

	@GET
	public Object findByName(@QueryParam("nome") String nome, @QueryParam("empresa") Long id) throws DomainException {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}

		return parametroService.parametrosAutocomplete(id, nome);

	}
	
	@GET
	@Path("/param")
	public String findValue(@QueryParam("nome") String nome) throws DomainException {

		if(StringUtils.isEmpty(nome)) {
			return "";
		}
		
		List<ParametroVO> parametrosAutocomplete = parametroService.parametrosAutocomplete(null, nome);
		ParametroVO vo = parametrosAutocomplete.size() > 0 ? parametrosAutocomplete.get(0) : null;
		String value = vo != null ? vo.valor : "";
		return value;

	}

	@GET
	@Path("/map/param")
	public Response findParam(@QueryParam("nome") String nome) throws DomainException {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		if(StringUtils.isEmpty(nome)) {
			return Response.ok(MessageResult.error("Parametro de consulta não informado")).build();
		}
		
		ParametrosMap params = ParametrosMap.getInstance(getEmpresa());
		String value = params.get(nome, "");
		
		if(StringUtils.isEmpty(value)) {
			return Response.ok(MessageResult.error(value)).build();
		}
		
		return Response.ok(MessageResult.ok(value)).build();
		
	}

	@GET
	@Path("/map/params/{keys}")
	public Response findParams(@PathParam("keys") String nomes) throws DomainException {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		if(StringUtils.isEmpty(nomes)) {
			return Response.ok(MessageResult.error("Parametro de consulta não informado")).build();
		}
		
		Map<String, String> map = new HashMap<String, String>();
		ParametrosMap params = ParametrosMap.getInstance(getEmpresa());

		List<String> keys = Utils.getListCods(nomes);
		for (String key : keys) {
			map.put(key, params.get(key, ""));
		}
		
		if(map.isEmpty()) {
			return Response.ok(MessageResult.error("Erro ao buscar os parametros informados")).build();
		}
		
		return Response.ok(MessageResult.ok(map)).build();
		
	}
	
	@GET
	@Path("/value")
	public Object findValor(@QueryParam("nome") String nome, @QueryParam("empresa") Long id) throws DomainException {
		
		if(StringUtils.isEmpty(nome)) {
			return "";
		}
		
		if(id == null) {
			return "";
		}
		
		ParametrosMap parametrosMap = ParametrosMap.getInstance(id);
		String valor = parametrosMap.get(nome);
		
		return valor;

	}
	
	@GET
	@Path("/mobile/{empresaId}")
	public Response findAllByEmpresa(@PathParam("empresaId") Long empresaId) {
		List<Parametro> parametros = parametroService.findAll(empresaId, TipoParametro.MOBILE);
		List<ParametroVO> list = ParametroVO.toList(parametros);
		return Response.ok(list).build();
	}
	
	@POST
	@Path("/mobile/{empresaId}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response cadastro(@PathParam("empresaId") Long empresaId, @FormParam("nome") String nome, @FormParam("id") Long id, @FormParam("valor") String valor) {
		Parametro param = null;
		boolean insert = true;
		if(id != null) {
			param = parametroService.get(id);
			if(param == null) {
				return Response.ok(MessageResult.error("Erro parâmetro não encontrado")).build();
			}
			insert = false;
		} else {
			param = new Parametro();
		}
		
		Empresa empresa = empresaService.get(empresaId);
		if(empresa == null) {
			return Response.ok(MessageResult.error("Erro empresa não encontrada")).build();
		}
		
		if(StringUtils.isEmpty(nome)) {
			return Response.ok(MessageResult.error("Nome do paramêtro não informado.")).build();
		}
		
		if(!org.apache.commons.lang3.StringUtils.startsWith(nome, "app.")) {
			nome = "app." + nome;
		}
		
		nome = nome.toLowerCase();
		
		
		Parametro isExist = parametroService.findByNome(nome, getEmpresa());
		if(isExist != null && !isExist.getId().equals(param.getId())) {
			return Response.ok(MessageResult.error("Paramêtro com o nome "+ nome +" já cadastrado.")).build();
		}
		
		param.setNome(nome);
		param.setValor(valor);
		param.setEmpresa(empresa);
		param.setTipoParametro(TipoParametro.MOBILE);
		try {
			parametroService.saveOrUpdate(param);
			if(insert) {
				return Response.ok(MessageResult.ok("Parâmetro cadastrado com sucesso")).build();
			}
			return Response.ok(MessageResult.ok("Parâmetro atualizado com sucesso")).build();
		} catch (DomainException e) {
			return Response.ok(MessageResult.error("Não foi possível cadastrar ou atualizar o parâmetro")).build();
		}
	}
	
	@Path("/config/mobile/{empresaId}")
	@POST
	public Response saveConfigMobile(@PathParam("empresaId") Long empresaId, ConfigMobileRequest config) {
		Empresa empresa = empresaService.get(empresaId);
		if(empresa == null) {
			return Response.ok(MessageResult.error("Erro empresa não encontrada")).build();
		}
		
		List<Parametro> parametros = config.toParametros();
		// salva a lista de parametros
		parametroService.saveOrUpdate(parametros, empresa);
		
		return Response.ok(MessageResult.ok("Configurações atualizadas com sucesso")).build();
	}
	
	@GET
	@Path("/mobile/bba/sobre/{idioma}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response webviewSobre(@PathParam("idioma") String idioma) {
		ParametrosMap params = ParametrosMap.getInstance(getEmpresa());
		
		String url = "";
		if(StringUtils.equalsIgnoreCase(idioma, "pt")) {
			url = params.get(Params.BBA_SOBRE_URL_PT, "");
		} else {
			url = params.get(Params.BBA_SOBRE_URL_EN, "");
		}
		
		BBAWebviewVO response = new BBAWebviewVO();
		response.url = url;
		
		return Response.ok(response).build();
	}
	
	@GET
	@Path("/mobile/bba/termos/{idioma}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response webviewTermos(@PathParam("idioma") String idioma) {
		ParametrosMap params = ParametrosMap.getInstance(getEmpresa());
		
		String url = "";
		if(StringUtils.equalsIgnoreCase(idioma, "pt")) {
			url = params.get(Params.BBA_TERMOS_URL_PT, "");
		} else {
			url = params.get(Params.BBA_TERMOS_URL_EN, "");
		}
		
		BBAWebviewVO response = new BBAWebviewVO();
		response.url = url;
		
		return Response.ok(response).build();
	}

}