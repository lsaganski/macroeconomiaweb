package br.livetouch.livecom.domain.repository.impl;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.CategoriaIdioma;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.repository.CategoriaIdiomaRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class CategoriaIdiomaRepositoryImpl extends StringHibernateRepository<CategoriaIdioma> implements CategoriaIdiomaRepository {

	public CategoriaIdiomaRepositoryImpl() {
		super(CategoriaIdioma.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<CategoriaIdioma> findByCategoria(CategoriaPost c) {
		StringBuffer sb = new StringBuffer("select ci from CategoriaIdioma ci inner join ci.categoria c WHERE 1=1 ");
		
		if(c != null) {
			sb.append(" AND c = :categoria ");
		}
		
		sb.append(" order by ci.idioma.nome ");
		
		Query q = createQuery(sb.toString());
		
		if(c != null) {
			q.setParameter("categoria", c);
		}
		
		Set<CategoriaIdioma> list = new HashSet<CategoriaIdioma>(q.list());
		return list;
	}
	
}