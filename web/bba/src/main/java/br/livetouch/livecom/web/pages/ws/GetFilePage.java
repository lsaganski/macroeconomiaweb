package br.livetouch.livecom.web.pages.ws;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.files.FileManagerFactory;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class GetFilePage extends LivecomLogadoPage {

	public Form form = new Form();
	
	public String dir;
	public String file;
	public String url;
	public String contentType;
	private TextField tMode;
	public String mode;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(new TextField("dir", true));
		form.add(new TextField("file", true));
		form.add(new TextField("contentType","contentType: text/html image/jpg"));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode","mode: url, image"));
		tMode.setValue("url");

		form.add(new Submit("Get File"));

//		setFormTextWidth(form);
	}
	
	@Override
	public void onPost() {
		super.onPost();
		if(form.isValid()) {
			try {
				this.url = FileManagerFactory.getFileManager(getParametrosMap()).getFileUrl(dir, file);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if("url".equals(mode)) {
				setPath(null);
				writeResponseText(url, StringUtils.isNotEmpty(contentType) ? contentType : "text/html");
			}else if("image".equals(mode)) {
//				setPath(null);
//				writeResponseBytes(url, StringUtils.isNotEmpty(contentType) ? contentType : "image/jpg");
			}
		}
	}
	
	@Override
	public String getTemplate() {
		if("image".equals(mode)) {
			return getPath();
		}
		
		return super.getTemplate();
	}
	
	@Override
	public String getPath() {
		return super.getPath();
	}
}
