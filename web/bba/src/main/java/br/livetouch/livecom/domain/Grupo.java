package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.gson.Gson;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.livetouch.livecom.domain.enums.TipoGrupo;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.utils.LivecomUtil;
import net.livetouch.extras.util.DateUtils;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Grupo extends JsonEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5621805878507014639L;

	public static final String SESSION_FILTRO_KEY = "grupo";
	
	public static final String GRUPO_DEFAULT = "Geral";
	
	public static final String THUMB_DEFAULT_GRUPO = "img/grupo.jpg";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "GRUPO_SEQ")
	private Long id;

	private String nome;
	/**
	 * No caso da ambev o nome é:
	 * 
	 * Pdvs do Vendedor xxx cpfVendedor
	 * 
	 * E nome2 é:
	 * 
	 * Meus Pdvs
	 */
	private String nome2;
	
	/**
	 * CPF ou CNPJ
	 */
	private String codigo;

	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "post_grupos", joinColumns = { @JoinColumn(name = "grupo_id") }, inverseJoinColumns = { @JoinColumn(name = "post_id") })
	@XStreamOmitField
	private Set<Post> posts;
	
	private Boolean padrao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Usuario usuario; // dono do grupo
	
	@OneToMany(mappedBy = "grupo", fetch = FetchType.LAZY)
	@XStreamOmitField
	private Set<GrupoUsuarios> usuarios;

	@OneToMany(mappedBy = "grupo", fetch = FetchType.LAZY)
	@XStreamOmitField
	private Set<GrupoSubgrupos> subgrupos;
	
	/**
	 * Default é false;
	 */
	private Boolean grupoVirtual = false;
	
	/**
	 * Default é false;
	 */
	private Boolean todosPostam = true;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_create_id", nullable = true)
	private Usuario userCreate;
	private Date dateCreate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_update_id", nullable = true)
	private Usuario userUpdate;
	private Date dateUpdate;

	private Date horaAbertura;
	private Date horaFechamento;
	
	@ManyToMany(mappedBy="grupos")
    private List<Evento> eventos;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "arquivo_foto_id", nullable = true)
	private Arquivo foto;
	
	@Enumerated(EnumType.STRING)
	private TipoGrupo tipoGrupo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categoria_id", nullable = true)
	private CategoriaPost categoria;
	
	/**
	 * Default é true;
	 */
	private boolean visivel = true;
	
	public Grupo() {
		
	}
	
	public String getUrlFotoCapa() {
		if(foto != null) {
			return foto.getUrlThumb();
		}
		ParametrosMap map = null;
		Empresa empresa = getEmpresa();
		if(empresa != null) {
			map = ParametrosMap.getInstance(empresa.getId());
		} else {
			map = ParametrosMap.getInstance();
		}
		
		String url = map.get(Params.LAYOUT_THUMB_GRUPO, THUMB_DEFAULT_GRUPO);
		
		if (LivecomUtil.isUrl(url)) {
			return url;
		}
		return LivecomUtil.getUrlFotoCapa(map, url);
	}
	
	
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getNome2() {
		return nome2 != null ? nome2 : nome;
	}
	
	public void setNome2(String nome2) {
		this.nome2 = nome2;
	}

	public String getNomeString (UserInfoVO user) {
		if(user == null) {
			return nome;
		}
		if(this.usuario != null && this.usuario.getId().equals(user.getId())) {
			return getNome2();
		}
		return nome;
	}
	
	public String getNomeString (Usuario user) {
		if(user == null) {
			return nome;
		}
		if(this.usuario != null && this.usuario.getId().equals(user.getId())) {
			return getNome2();
		}
		return nome;
	}

	public Set<Post> getPosts() {
		return posts;
	}
	
	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}
	
	public Boolean isPadrao() {
		if(padrao == null) {
			return false;
		}
		return padrao;
	}
	
	public void setPadrao(Boolean padrao) {
		this.padrao = padrao;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getCodigoDesc() {
		return codigo != null ? codigo : id.toString();
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public static List<Long> getIds(Collection<Grupo> list) {
		List<Long> ids = new ArrayList<Long>();
		if(list == null || list.size() == 0) {
			return ids;
		}
		for (net.livetouch.tiger.ddd.Entity e : list) {
			ids.add(e.getId());
		}
		return ids;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Set<GrupoUsuarios> getUsuarios() {
		return usuarios;
	}
	
	public List<GrupoUsuarios> getUsuariosList() {
		return new ArrayList<>(usuarios);
	}
	
	public void setUsuarios(Set<GrupoUsuarios> usuarios) {
		this.usuarios = usuarios;
	}

	public Usuario getUserCreate() {
		return userCreate;
	}
	
	public Usuario getAdmin() {
		return userCreate;
	}

	public void setUserCreate(Usuario userCreate) {
		this.userCreate = userCreate;
	}

	public Date getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Usuario getUserUpdate() {
		return userUpdate;
	}

	public void setUserUpdate(Usuario userUpdate) {
		this.userUpdate = userUpdate;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public List<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(List<Evento> eventos) {
		this.eventos = eventos;
	}

	public Date getHoraAbertura() {
		return horaAbertura;
	}

	public void setHoraAbertura(Date horaAbertura) {
		this.horaAbertura = horaAbertura;
	}

	public Date getHoraFechamento() {
		return horaFechamento;
	}

	public void setHoraFechamento(Date horaFechamento) {
		this.horaFechamento = horaFechamento;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	

	@Override
	public String toString() {
		return toStringDesc();
	}

	public String toStringDesc() {
		if(codigo != null) {
			return id + ":("+codigo+"): " + nome;
		}
		return id + ": " + nome;
	}

	@Deprecated
	public boolean isGrupoDentroHorario()  {
		ParametrosMap map = ParametrosMap.getInstance(getEmpresa().getId());
		String on = map.get(Params.GRUPO_RESTRICAO_HORARIO_ON, "0");
		if("1".equals(on)) {
			Date h = DateUtils.getDateByHourDate(this.getHoraAbertura());
			Date f = DateUtils.getDateByHourDate(this.getHoraFechamento());
			
			on = map.get(Params.HORARIO_GRUPO_DEFAULT_ON, "0");
			boolean useHorarioDefault = "1".equals(on);
			
			if(!useHorarioDefault && (h == null || f == null)) {
				return true;
			}
			
			if(h == null) {
				String hda = map.get(Params.HORARIO_GRUPO_DEFAULT_ABERTURA, "09:00");
				h = DateUtils.newDateByHoraMinutoSeg(hda);
			}
			
			if(f == null) {
				String hda = map.get(Params.HORARIO_GRUPO_DEFAULT_FECHAMENTO, "14:00");
				f = DateUtils.newDateByHoraMinutoSeg(hda);
			}
			
			if(h != null && f != null) {
				long diferencaMinutosAbertura = DateUtils.getDiferencaMinutos(h, new Date());
				long diferencaMinutosFechamento = DateUtils.getDiferencaMinutos(f, new Date());
				boolean isForaHorario = diferencaMinutosAbertura < 0 || diferencaMinutosFechamento > 0;
				if(isForaHorario) {
					return false;
				}
				return true;
			}
		}
		return true;
	}
	
	
	public String toStringAll() {
		return "Grupo [id=" + id + ", nome=" + nome + ", nome2=" + nome2 + ", codigo=" + codigo + ", posts=" + posts + ", padrao=" + padrao + ", usuario=" + usuario + ", userCreate=" + userCreate + ", dateCreate=" + dateCreate + ", userUpdate=" + userUpdate + ", dateUpdate=" + dateUpdate + "]";
	}

	public Arquivo getFoto() {
		return foto;
	}

	public void setFoto(Arquivo foto) {
		this.foto = foto;
	}
	
	public String getUrlFoto() {
		if(foto != null) {
			return foto.getUrl();
		} else {
			return getUrlFotoCapa();
		}
	}

	@Override
	public String toJson() {
		GrupoVO vo = new GrupoVO();
		vo.setGrupo(this);
		return new Gson().toJson(vo);
	}

	public void setTipoGrupo(TipoGrupo tipoGrupo) {
		this.tipoGrupo = tipoGrupo;
	}
	
	public TipoGrupo getTipoGrupo() {
		return tipoGrupo;
	}
	
	public void setVisivel(boolean visivel) {
		this.visivel = visivel;
	}
	
	public boolean isVisivel() {
		return visivel;
	}


	public CategoriaPost getCategoria() {
		return categoria;
	}


	public void setCategoria(CategoriaPost categoria) {
		this.categoria = categoria;
	}

	public boolean isAdmin(Usuario u) {
		if(u != null && userCreate != null) {
			boolean isAdmin = u.equals(userCreate);
			return isAdmin;
		}
		return false;
	}


	public Boolean isGrupoVirtual() {
		return grupoVirtual != null ? grupoVirtual : false;
	}


	public void setGrupoVirtual(Boolean virtual) {
		this.grupoVirtual = virtual;
	}

	public Set<GrupoSubgrupos> getSubgrupos() {
		return subgrupos;
	}

	public void setSubgrupos(Set<GrupoSubgrupos> subgrupos) {
		this.subgrupos = subgrupos;
	}

	public Boolean isTodosPostam() {
		return todosPostam != null ? todosPostam : false;
	}

	public void setTodosPostam(Boolean todosPostam) {
		this.todosPostam = todosPostam;
	}
	
}
