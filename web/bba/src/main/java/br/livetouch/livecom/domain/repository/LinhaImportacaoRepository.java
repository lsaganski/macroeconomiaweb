package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.LinhaImportacao;

@Repository
public interface LinhaImportacaoRepository extends net.livetouch.tiger.ddd.repository.Repository<LinhaImportacao> {

	List<LinhaImportacao> findAllByLogId(Long id);

	List<LinhaImportacao> findAllByIdLog(Long id);
	
	
}