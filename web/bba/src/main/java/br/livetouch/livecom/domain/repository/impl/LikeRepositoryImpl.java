package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Likes;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.LikeRepository;
import br.livetouch.livecom.domain.vo.PostCountVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioLikesVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public class LikeRepositoryImpl extends LivecomRepository<Likes> implements LikeRepository {

	public LikeRepositoryImpl() {
		super(Likes.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Likes findLikeByUserAndPost(Usuario u, Post post) {
		StringBuffer sb = new StringBuffer("from Likes where usuario.id=? and favorito=1 and comentario is null");

		if (post != null) {
			sb.append(" and post.id=? ");
		}
		Query q = createQuery(sb.toString());
		q.setCacheable(true);

		q.setLong(0, u.getId());
		if (post != null) {
			q.setLong(1, post.getId());
		}

		q.setCacheable(true);
		List<Likes> list = q.list();
		Likes f = list.isEmpty() ? null : list.get(0);
		return f;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Likes findLikeByUserAndComentario(Usuario u, Comentario c) {
		Query q = createQuery("from Likes where usuario.id=? and comentario.id=? and favorito=1");
		q.setLong(0, u.getId());
		q.setLong(1, c.getId());

		q.setCacheable(true);
		List<Likes> list = q.list();
		Likes f = list.isEmpty() ? null : list.get(0);
		return f;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Likes> findAllLikesOfPostByUser(Usuario u) {
		Query q = createQuery("from Likes f where f.favorito=1 and f.usuario.id=? and f.post is not null order by f.post.data");
		q.setLong(0, u.getId());

		q.setCacheable(true);
		List<Likes> list = q.list();
		return list;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Likes> findAllLikesByUser(Usuario u) {
		Query q = createQuery("from Likes l where l.usuario.id=?");
		q.setLong(0, u.getId());
		q.setCacheable(true);
		List<Likes> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Likes> findAllLikesByPost(Post post) {
		Query q = createQuery("from Likes f where (f.favorito = 1 and f.comentario is null and f.post is not null) and f.post.id=? order by f.post.data");
		q.setLong(0, post.getId());
		q.setCacheable(true);
		List<Likes> list = q.list();
		return list;
	}
	
	@Override
	public long getCountLikesByPost(Post post) {
		Query q = createQuery("select count(*) from Likes f where (f.favorito = 1 and f.comentario is null and f.post is not null) and f.post.id=?");
		q.setLong(0, post.getId());
		q.setCacheable(true);

		Object obj = q.uniqueResult();
		return new Long(obj.toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Likes> findAllLikesByComentario(Comentario c) {
		Query q = createQuery("from Likes f where f.favorito=1 and f.comentario.id=? and f.comentario is not null order by f.comentario.data");
		q.setLong(0, c.getId());

		q.setCacheable(true);
		List<Likes> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Likes> findAllLikesOfComentarioByPost(Post post) {
		Query q = createQuery("from Likes f where f.favorito=1 and f.post.id=? and f.comentario is not null order by f.comentario.data");
		q.setLong(0, post.getId());

		q.setCacheable(true);
		List<Likes> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Likes> findAllLikesPostByUserAndIds(Usuario user, List<Long> ids) {
		Criteria c = createCriteria();
		c.add(Restrictions.eq("usuario.id", user.getId()));
		c.add(Restrictions.eq("favorito", true));
		c.add(Restrictions.in("post.id", ids));
		c.setCacheable(true);
		List<Likes> list = c.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Likes> findAllLikesComentarioByUserAndIds(Usuario user, List<Long> ids) {
		Criteria c = createCriteria();
		c.add(Restrictions.eq("usuario.id", user.getId()));
		c.add(Restrictions.eq("favorito", true));
		c.add(Restrictions.in("comentario.id", ids));
		c.setCacheable(true);
		List<Likes> list = c.list();
		return list;
	}

	@Override
	public long countByPost(Post p) {
		Criteria c = createCriteria();
		if (p != null) {
			c.add(Restrictions.eq("post.id", p.getId()));
		}
		c.add(Restrictions.eq("favorito", true));
		c.setCacheable(true);
		c.setProjection(Projections.rowCount());
		c.setCacheable(true);
		long count = getCount(c);
		return count;
	}

	@Override
	public Long countByComentario(Comentario comentario) {
		Criteria c = createCriteria();
		c.add(Restrictions.eq("comentario.id", comentario.getId()));
		c.add(Restrictions.eq("favorito", true));
		c.setProjection(Projections.rowCount());
		c.setCacheable(true);
		Long count = getCount(c);
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Likes> findAllLikesNotificationByUser(Usuario user) {
		Query q = null;

		StringBuffer sb = new StringBuffer("select f from Likes f inner join f.post p where 1=1 ");

		if (user != null) {
			// se o post é seu, e o favorito é do outro. Nao deixa ler seu
			// proprio favorito
			sb.append(" and p.usuario.id = :userId and f.usuario.id != :userId ");
		}

		sb.append(" and (f.favorito = 1 and (f.lidaNotification=0 or f.lidaNotification is null)) ");

		sb.append(" order by f.id desc");

		// Query
		q = createQuery(sb.toString());

		if (user != null) {
			q.setParameter("userId", user.getId());
		}

		// if( maxRows > 0) {
		// int firstResult = page * maxRows;
		// q.setFirstResult(firstResult);
		// q.setMaxResults(maxRows);
		// }

		q.setCacheable(true);
		List<Likes> list = q.list();

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Likes> findAllByPosts(Usuario user, List<Long> ids) {

		StringBuffer sql = new StringBuffer("select f from Post p ");

		// Like
		sql.append(" inner join p.likes f");

		// Where
		sql.append(" where 1=1 ");

		if (ids != null && ids.size() > 0) {
			sql.append(" and p.id in (:postsIds)");
		}

		// Like
		sql.append(" and (f.favorito = 1 and f.usuario.id = :user and f.comentario is null)");

		Query q = createQuery(sql.toString());

		q.setParameter("user", user.getId());
		if (ids != null && ids.size() > 0) {
			q.setParameterList("postsIds", ids);
		}

		q.setCacheable(true);
		List<Likes> list = q.list();

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PostCountVO> findCountByPosts(List<Long> ids) {

		StringBuffer sql = new StringBuffer("select new br.livetouch.livecom.domain.vo.PostCountVO(p,count(f.id)) from Post p ");

		// Like
		sql.append(" inner join p.likes f");

		// Where
		sql.append(" where 1=1 ");

		if (ids != null && ids.size() > 0) {
			sql.append(" and p.id in (:postsIds)");
		}

		// Like
		sql.append(" and (f.favorito = 1 and f.comentario is null)");

		// Group by
		sql.append(" group by p");

		Query q = createQuery(sql.toString());

		if (ids != null && ids.size() > 0) {
			q.setParameterList("postsIds", ids);
		}

		q.setCacheable(true);
		List<PostCountVO> list = q.list();

		return list;
	}

	@Override
	public long getCountByFilter(RelatorioFiltro filtro, boolean count, boolean geral) throws DomainException {
		Query query = getQueryFindByFilter(filtro, count, geral);

		Object obj = !geral ? query.uniqueResult() : query.list().size();

		return new Long(obj.toString());
	}

	private Query getQueryFindByFilter(RelatorioFiltro filtro, boolean count, boolean geral) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;
		Usuario usuario = filtro.getUsuario();
		Post post = filtro.getPost();
		CategoriaPost categoria = filtro.getCategoria();
		Long empresa = filtro.getEmpresaId();

		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		if (count) {
			sb.append("select count(distinct l.id) ");
		} else {
			sb.append("select distinct new br.livetouch.livecom.domain.vo.RelatorioLikesVO (p,DATE_FORMAT(l.data,'%d/%m/%Y'),count(l.id),l.usuario) ");
		}

		sb.append(" from Likes l inner join l.post p ");

		sb.append(" where 1=1 ");

		if (dataInicio != null) {
			sb.append(" and l.data >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and l.data <= :dataFim");
		}

		if (usuario != null) {
			sb.append(" and l.usuario = :usuario");
		}

		if (empresa != null) {
			sb.append(" and l.usuario.empresa.id = :empresa");
		}

		if (post != null) {
			sb.append(" and p = :post");
		}
		if (categoria != null) {
			sb.append(" and p.categoria = :categoria");
		}

		sb.append(" and l.favorito = 1");

		if (geral) {
			sb.append(" group by DATE_FORMAT(l.data,'%d/%m/%Y'), p, l.usuario");
		}

		if (!count) {
			sb.append(" order by DATE_FORMAT(l.data,'%d/%m/%Y'), p, l.usuario desc");
		}

		Query q = createQuery(sb.toString());
		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}

		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}

		if (post != null) {
			q.setParameter("post", post);
		}
		if (categoria != null) {
			q.setParameter("categoria", categoria);
		}

		q.setCacheable(true);
		return q;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioLikesVO> findbyFilter(RelatorioFiltro filtro, boolean count) throws DomainException {
		Query query = getQueryFindByFilter(filtro, count, true);

		int page = filtro.getPage();

		if (!count) {
			int firstResult = 0;
			if (page != 0) {
				firstResult = page * filtro.getMax();
			}
			query.setFirstResult(firstResult);
			query.setMaxResults(filtro.getMax());
		}

		query.setCacheable(true);
		List<RelatorioLikesVO> likes = query.list();

		return likes;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Likes> findDetalhesByDate(RelatorioFiltro filtro, boolean count) {

		Query query = getQueryDetalhesByDate(filtro, count);

		if (!count) {
			int firstResult = 0;
			if (filtro.getPage() != 0) {
				firstResult = (filtro.getPage() * filtro.getMax());
			}
			query.setFirstResult(firstResult);
			query.setMaxResults(filtro.getMax());
		}

		query.setCacheable(true);
		List<Likes> likes = query.list();

		return likes;
	}

	private Query getQueryDetalhesByDate(RelatorioFiltro filtro, boolean count) {
		Date dataInicio = null;
		Date dataFim = null;
		dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
		dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());

		Usuario usuario = filtro.getUsuario();
		Post post = filtro.getPost();
		StringBuffer sb = new StringBuffer();
		if (count) {
			sb.append("select count(distinct id) ");
		}

		sb.append(" from Likes ");

		sb.append(" where 1=1 ");

		if (dataInicio != null) {
			sb.append(" and data >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and data <= :dataFim");
		}
		if (usuario != null) {
			sb.append(" and usuario = :usuario");
		}

		sb.append(" and favorito = 1");

		if (post != null) {
			sb.append(" and post = :post");
		}

		if (!count) {
			sb.append(" order by data desc");
		}

		Query q = createQuery(sb.toString());
		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}
		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}

		if (post != null) {
			q.setParameter("post", post);
		}

		q.setCacheable(true);
		return q;
	}

	@Override
	public long getCountDetalhesByDate(RelatorioFiltro filtro, boolean count) {
		Query query = getQueryDetalhesByDate(filtro, count);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioLikesVO> findbyFilterConsolidado(RelatorioFiltro filtro) throws DomainException {
		Date dataInicio = null;
		Date dataFim = null;
		Usuario usuario = filtro.getUsuario();
		Post post = filtro.getPost();
		CategoriaPost categoria = filtro.getCategoria();
		Long empresa = filtro.getEmpresaId();

		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		sb.append("select distinct new br.livetouch.livecom.domain.vo.RelatorioLikesVO (max(l.data),count(l.id),p) ");

		sb.append(" from Likes l inner join l.post p ");

		sb.append(" where 1=1 ");

		if (dataInicio != null) {
			sb.append(" and l.data >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and l.data <= :dataFim");
		}

		if (usuario != null) {
			sb.append(" and l.usuario = :usuario");
		}

		if (empresa != null) {
			sb.append(" and l.usuario.empresa.id = :empresa");
		}

		if (post != null) {
			sb.append(" and p = :post");
		}
		if (categoria != null) {
			sb.append(" and p.categoria = :categoria");
		}

		sb.append(" and l.favorito = 1");

		// sb.append(" group by DATE_FORMAT(l.data,'%d/%m/%Y'), p");
		sb.append(" group by p");
		sb.append(" order by max(l.data) desc");

		Query q = createQuery(sb.toString());
		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}

		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}

		if (post != null) {
			q.setParameter("post", post);
		}
		if (categoria != null) {
			q.setParameter("categoria", categoria);
		}

		q.setCacheable(true);
		return (List<RelatorioLikesVO>) q.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioVisualizacaoVO> reportLikes(RelatorioFiltro filtro) throws DomainException {
		Query query = queryReportLikes(filtro);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		query.setCacheable(true);
		List<RelatorioVisualizacaoVO> likes = query.list();

		return likes;
	}

	public Query queryReportLikes(RelatorioFiltro filtro) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;

		if (StringUtils.isNotEmpty(filtro.getDataIni())) {
			filtro.setDataInicial(DateUtils.toDate(filtro.getDataIni()));
		}
		if (StringUtils.isNotEmpty(filtro.getDataFim())) {
			filtro.setDataFinal(DateUtils.toDate(filtro.getDataFim()));
		}

		Long empresa = filtro.getEmpresaId();
		
		Usuario usuario = filtro.getUsuario();
		Post post = filtro.getPost();
		CategoriaPost categoria = filtro.getCategoria();
		
		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		sb.append("select new br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO(DATE_FORMAT(p.dataPublicacao,'%d/%m/%Y %H:%m:%s'),count(p.id),p,l) ");

		sb.append(" from Likes l inner join l.post p");
		
		sb.append(" where 1=1 ");
		
		sb.append(" and l.favorito = 1");

		if (dataInicio != null) {
			sb.append(" and p.dataPublicacao >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and p.dataPublicacao <= :dataFim");
		}

		if (usuario != null) {
			sb.append(" and l.usuario = :usuario ");
		}

		if (post != null) {
			sb.append(" and p = :post");
		}

		if (empresa != null) {
			sb.append(" and p.usuario.empresa.id = :empresa");
		}
		
		if (categoria != null) {
			sb.append(" and p.categoria = :categoria");
		}

		sb.append(" group by p");
		
		if(filtro.isAgrupar()) {
			sb.append(" , l");
		}
		
		sb.append(" order by p.dataPublicacao desc");

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}

		if (post != null) {
			q.setParameter("post", post);
		}
		
		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		if (categoria != null) {
			q.setParameter("categoria", categoria);
		}
		
		return q;

	}
}