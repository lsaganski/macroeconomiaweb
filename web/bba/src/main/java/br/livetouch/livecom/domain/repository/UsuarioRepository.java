package br.livetouch.livecom.domain.repository;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import br.livetouch.livecom.domain.Amizade;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.UsuarioAcessoMural;
import br.livetouch.livecom.domain.UsuarioAuditoria;
import br.livetouch.livecom.domain.enums.StatusAmizade;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVO;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioFuncionaisVO;
import br.livetouch.livecom.domain.vo.StatusAtivacaoVO;
import br.livetouch.livecom.domain.vo.UsuarioAutocompleteFiltro;
import br.livetouch.livecom.domain.vo.UsuarioStatusCountVO;
import net.livetouch.tiger.ddd.DomainException;
import net.livetouch.tiger.ddd.repository.Repository;

public interface UsuarioRepository extends Repository<Usuario>{

	int MAX = 20;
	
	Usuario findByLogin(String login, Empresa empresa);
	
	Usuario findByEmail(String email);

	List<Usuario> findByFilter(Usuario userInfo,Usuario filtro, int page, int pageSize, Empresa empresa);
	List<Usuario> findByFiltro(UsuarioAutocompleteFiltro filtro, Empresa empresa);

	long getCountByFilter(Usuario userInfo, Usuario filtro);
	long getCountByNomeLike(UsuarioAutocompleteFiltro filtro, Empresa empresa);

	List<StatusAtivacaoVO> findStatusAtivacao(Empresa empresa);
	UsuarioAcessoMural findUsuarioAcessoMural(Usuario u, Post post);
	Usuario findByLoginFetch(String login, Empresa empresa, boolean isMobile);
	GrupoUsuarios getGrupoUsuario(Usuario usuario, Grupo grupo);
	Amizade getAmizade(Usuario usuario, Usuario amigo);
	List<Usuario> findUsuariosByGrupo(Grupo grupo, int page, int maxSize);
	List<Usuario> findAll(Usuario user, int page, int max);
	List<Usuario> findAllByStatusAtivacaoByGrupo(Grupo g, int page, int maxRows, String status);
	List<Usuario> findAllAtivosByGrupo(Grupo g, int page, int maxRows);
	List<Usuario> findAllEnviadoConviteByGrupo(Grupo g, int page, int maxRows);
	List<String> findLogins();
	List<Object[]> findLoginsTipoImportacao();
	List<Grupo> getGruposCriadosPorMim(Usuario user);
	List<Grupo> getGrupos(Usuario u, boolean postar);
	List<Grupo> getGrupos(Usuario u);
	List<RelatorioAcessosVersaoVO> findAcessosByVersao(RelatorioFiltro filtro) throws DomainException;
	List<Object[]> findIdLoginsUsuario();
	List<Usuario> findByNomeLikeLogin(UsuarioAutocompleteFiltro filtro, Empresa empresa);
	List<Usuario> findAll(Empresa empresa);
	List<String> findLoginsByIds(List<Long> ids);
	Long cont(Empresa empresa);
	List<RelatorioFuncionaisVO> getFuncionais(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException;
	long getCountByFilter(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException;
	List<RelatorioAcessosVO> findbyFilter(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException;
	long getCountVersaoByFilter(RelatorioFiltro filtro) throws DomainException;
	
	void deleteGrupoUsuarios(Usuario u);
	void deleteGrupoUsuarios(Usuario u, List<Grupo> grupos);
	void deleteGrupoUsuarios(Grupo g);
	public void saveOrUpdateUsuarioAcesso(UsuarioAcessoMural u);
	void saveOrUpdate(GrupoUsuarios gu);
	void saveOrUpdate(Amizade a);
	void deleteGrupoUsuarios(Grupo g, Usuario u);
	void deleteGrupoUsuarios(GrupoUsuarios grupoUsuarios);
	void deleteAmizade(Usuario u, Usuario amigo);
	void deleteAcessoMural(Usuario u);
	void deleteAuditoria(Usuario u);
	void saveOrUpdate(UsuarioAuditoria a);
	void deleteAllUsersByTipoImportacao();
	
	boolean isUsuarioDentroDoGrupo(Usuario user, Grupo grupo);
	void insertGrupoUsuario(Long userId, Long grupoId) throws SQLException;

	Long insertUsuarioImportacao(String login,String senha,String dataNasc,Long permissaoId,Long grupoId) throws SQLException;
	
	boolean isUsuarioDentroHorario(Usuario u);
	List<Long> usuariosDentroHorarioExpediente(List<Long> idsToFilter);
	
	void setNullFuncaoByCargos(List<Funcao> funcoes);
	List<Usuario> findByCargos(List<Funcao> funcoes);

	boolean existsByLogin(Usuario u, Empresa empresa);
	boolean existsByEmail(Usuario u, Empresa empresa);

	List<Long> findAllIds(Empresa empresa);

	List<Usuario> findByStatus(Grupo g, StatusUsuario status, Empresa empresa, int page, int maxRows);

	Long getCountFindByStatus(Grupo g, StatusUsuario statusAtivacao, Empresa empresa);

	UsuarioStatusCountVO getCountUsuariosStatus(Empresa empresa);

	List<Usuario> findNovos(Empresa empresa);

	List<Usuario> findAtivos(Empresa empresa);

	List<Usuario> findInativos(Empresa empresa);

	List<Usuario> findAllNaoAtivosByGrupo(Grupo g, int page, int maxRows);

	List<Amizade> findUsuariosByStatusAmizade(StatusAmizade status, Usuario u);

	List<Long> findUsuariosToPush(Post p, List<Long> amigos, Idioma idioma);

	List<Amizade> findUsuariosByStatusAmizade(StatusAmizade aprovada, Usuario usuario, String nome);

	Date updateDataLastChat(Long userId);

	void updateAllDataSenha(Long empresaId);
	
}