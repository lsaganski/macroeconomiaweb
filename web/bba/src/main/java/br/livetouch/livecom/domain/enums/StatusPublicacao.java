package br.livetouch.livecom.domain.enums;

/**
 * @author Ricardo Lecheta
 * @created 23/02/2013
 */
public enum StatusPublicacao {
	EMPTY,
	PUBLICADO,
	AGENDADO,
	EXPIRADO;
//	RASCUNHO;
}
