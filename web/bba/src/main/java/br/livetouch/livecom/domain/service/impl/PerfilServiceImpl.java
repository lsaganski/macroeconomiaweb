package br.livetouch.livecom.domain.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.internal.util.SerializationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.MenuPerfil;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.PerfilPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.PerfilRepository;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.MenuPerfilService;
import br.livetouch.livecom.domain.service.PerfilService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class PerfilServiceImpl extends LivecomService<Perfil> implements PerfilService {
	@Autowired
	private PerfilRepository rep;

	@Autowired
	private EmpresaService empresaService;

	@Autowired
	private MenuPerfilService menuPerfilService;

	@Override
	public Perfil get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Perfil c) throws DomainException {
		rep.saveOrUpdate(c);
	}
	
	@Override
	public void saveOrUpdate(PerfilPermissao p) {
		rep.saveOrUpdate(p);
	}

	@Override
	public List<Perfil> findAll() {
		return rep.findAll(true);
	}
	
	@Override
	public void saveOrUpdate(Perfil c, Empresa e) throws DomainException {
		c.setEmpresa(e);
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Perfil> findAll(Empresa e) {
		return rep.findAll(e);
	}

	@Override
	public void delete(Perfil m, Usuario user) throws DomainException {
		try {
			rep.delete(m);
		}catch (DataIntegrityViolationException e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if(root == null) {
				root = e;
			}
			log.error("Erro ao excluir o perfil [" + m + "]: " + root.getMessage(), root);
			
			LogSistema.logError(user, e);
			
			throw e;
		} catch (Exception e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if(root == null) {
				root = e;
			}
			String msg = "Erro ao excluir o perfil [" + m + "]: " + root.getMessage();
			log.error(msg, root);
			LogSistema.logError(user, e);
			
			throw e;
		} 
	}

	@Override
	public List<Object[]> findIdNomePerfil() {
		return rep.findIdNomePerfil();
	}

	@Override
	public Perfil findByNome(String nome, Empresa e) {
		return rep.findByNome(nome, e);
	}

	@Override
	public Perfil createPerfil(String nome, Empresa e) throws DomainException {
		Empresa livetouch = empresaService.get(1L);
		Perfil p = rep.findByNome(nome, livetouch);
		if(p != null) {
			Perfil perfil = (Perfil) SerializationHelper.clone((Serializable) p); 
			perfil.setNome(perfil.getNome() + " " + e.getNome());
			perfil.setId(null);
			
			saveOrUpdate(perfil, e);
			
			updateMenusDefault(perfil, p);
			
			return perfil;
		}
		
		return null;
		
	}
	
	@Override
	public void generatePerfis(Empresa e) throws DomainException {
		List<Perfil> perfis = rep.findDefaults();
		
		for (Perfil perfil : perfis) {
			Perfil p = (Perfil) SerializationHelper.clone((Serializable) perfil); 
			p.setId(null);
			saveOrUpdate(p, e);
			generatePerfisPermissaoDefault(p, p.getPermissoes());
		}
		
	}
	
	@Override
	public void generatePerfisPermissaoDefault(Perfil p, Set<PerfilPermissao> permissoes) throws DomainException {
		
		if(permissoes == null || p == null) {
			return;
		}
		
		removePermissoes(p);
		
		for (PerfilPermissao permissao : permissoes) {
			PerfilPermissao per = (PerfilPermissao) SerializationHelper.clone((Serializable) permissao); 
			per.setId(null);
			per.setPerfil(p);
			saveOrUpdate(per);
		}
		
	}
	
	@Override
	public Perfil setMenusDefault(Perfil perfil, String nome, Empresa e) throws DomainException {
		Empresa livetouch = empresaService.get(1L);
		Perfil p = rep.findByNome(nome, livetouch);
		if(p != null) {
			
			saveOrUpdate(perfil, e);
			
			updateMenusDefault(perfil, p);
			
			return perfil;
		}
		
		return null;
		
	}

	private void updateMenusDefault(Perfil perfil, Perfil p) throws DomainException {
		List<MenuPerfil> menus = menuPerfilService.findByPerfil(p, null);
		List<MenuPerfil> menusPerfil = new ArrayList<MenuPerfil>();
		for (MenuPerfil m : menus) {
			MenuPerfil menuPerfil = new MenuPerfil();
			menuPerfil.setPerfil(perfil);
			menuPerfil.setMenu(m.getMenu());
			menuPerfil.setOrdem(m.getOrdem());
			menusPerfil.add(menuPerfil);
		}
		
		List<MenuPerfil> menusRemove = new ArrayList<MenuPerfil>();
		for (MenuPerfil m : perfil.getMenus()) {
			menusRemove.add(m);
		}
		
		menuPerfilService.removeIn(menusRemove);
		menuPerfilService.saveList(menusPerfil);
	}

	@Override
	public Perfil findDefault() {
		Perfil p = rep.get(3L);
		if(p != null) {
			return p;
		}
		List<Perfil> list = rep.findAll();
		return list.size() > 0 ? list.get(0) : null;
	}

	@Override
	public Perfil findByNomeValid(Perfil perfil, Empresa e) {
		return rep.findByNomeValid(perfil, e);
	}

	@Override
	public void removePermissoes(Perfil perfil) {
		rep.removePermissoes(perfil);
	}

	@Override
	public void savePermissoes(Perfil perfil, Set<PerfilPermissao> permissoes) {
		for (PerfilPermissao p : permissoes) {
			p.setPerfil(perfil);
			saveOrUpdate(p);
		}
		
	}
	
	
	

}
