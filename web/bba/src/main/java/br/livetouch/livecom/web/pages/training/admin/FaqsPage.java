package br.livetouch.livecom.web.pages.training.admin;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Faq;
import net.livetouch.click.control.link.Link;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.extras.control.LinkDecorator;

/**
 * 
 */
@Controller
@Scope("prototype")
public class FaqsPage extends TrainingAdminPage {

	public PaginacaoTable table = new PaginacaoTable();
	public ActionLink editLink = new ActionLink("editar", this, "editar");
	public Link deleteLink = new Link("deletar", this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public Faq device;

	public List<Faq> Faqs;

	@Override
	public void onInit() {
		super.onInit();

		table();

		if (id != null) {
			device = faqService.get(id);
		} else {
			device = new Faq();
		}
	}

	private void table() {
		Column c = new Column("id");
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("enunciado", "titlo");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);

		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes");
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean novo() {
		setRedirect(FaqsPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		device = faqService.get(id);
		setRedirect(FaqPage.class, "id", String.valueOf(id));

		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Faq e = faqService.get(id);
			faqService.delete(e);
			setRedirect(getClass());
			setFlashAttribute("msg", "Faq deletado com sucesso.");
		} catch (DomainException e) {
			logError(e.getMessage(), e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(), e);
			this.msgErro = "Faq possui relacionamentos. Não foi possível excluir.";
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		Faqs = faqService.findAll();

		// Count(*)
		int pageSize = 100;

		table.setPageSize(pageSize);
		table.setRowList(Faqs);
	}
}
