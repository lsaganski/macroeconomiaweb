package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.List;

public class DiretoriasVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public List<DiretoriaVO> diretorias;
	public Long total;
	
}
