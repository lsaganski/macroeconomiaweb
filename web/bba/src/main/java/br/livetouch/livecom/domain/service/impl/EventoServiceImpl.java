package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Evento;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.repository.EventoRepository;
import br.livetouch.livecom.domain.service.EventoService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class EventoServiceImpl implements EventoService {
	@Autowired
	private EventoRepository rep;

	@Override
	public Evento get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Evento c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Evento> findAll() {
		return rep.findAll();
	}

	@Override
	public void delete(Evento c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public List<Evento> findByAtivo(List<Grupo> grupos) {
		return rep.findByAtivo(grupos);
	}

	@Override
	public List<Evento> findAllOrderBy() {
		return rep.findAllOrderBy("data", false);
	}
}
