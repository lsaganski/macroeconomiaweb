package br.livetouch.livecom.web.pages.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.extras.control.IntegerField;

@Controller
@Scope("prototype")
public class YuriPage extends LivecomLogadoPage{

	public String msg = "Oi Yuri";
	
	// http://localhost:8080/livecom/pages/yuri.htm?a=1&b=2
	public int a;
	public int b;
	
	public Form form = new Form();

	private IntegerField tA;
	private IntegerField tB;
	
	@Override
	public void onInit() {
		super.onInit();
		
		form.setMethod("post");
		
		form.add(tA = new IntegerField("a"));
		form.add(tB = new IntegerField("b"));
		form.add(new Submit("somar"));
		form.add(new Submit("somar2",this,"somar2"));
	}
	
	@Override
	public void onGet() {
		super.onGet();
	}
	
	@Override
	public void onPost() {
		super.onPost();
		
		a = tA.getInteger();
		b = tB.getInteger();
		
		msg = "Soma por POST é " + (a+b);
	}
	
	public boolean somar2() {
		
		msg = "Soma por POST somar2() é " + (a+b);
		
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
