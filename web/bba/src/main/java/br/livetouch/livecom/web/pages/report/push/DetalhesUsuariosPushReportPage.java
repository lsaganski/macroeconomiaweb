package br.livetouch.livecom.web.pages.report.push;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.web.pages.report.RelatorioPage;

@Controller
@Scope("prototype")
public class DetalhesUsuariosPushReportPage extends RelatorioPage {

	public String login;
	public String empresa;
	public String url;
	public Long id;

	@Override
	public void onInit() {
		super.onInit();
		
		empresa = ParametrosMap.getInstance(getEmpresa()).get(Params.PUSH_SERVER_PROJECT, "");
		url = ParametrosMap.getInstance(getEmpresa()).get(Params.PUSH_SERVER_HOST, "");

	}
}
