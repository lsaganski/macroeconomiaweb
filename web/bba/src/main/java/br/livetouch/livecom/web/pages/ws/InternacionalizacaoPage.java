package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.click.control.Form;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class InternacionalizacaoPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tSigla;
	private TextField tMode;
	
	@Override	
	public void onInit() {
		super.onInit();
		
		form.setMethod("post");
		form.add(tSigla = new TextField("sigla", true));
		form.add(tMode = new TextField("mode"));
		tMode.setValue("json");
		setFormTextWidth(form);
		
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			String sigla= tSigla.getValue();
			if (sigla.equals("pt")){
				getContext().setLocale(java.util.Locale.forLanguageTag("pt"));
			}else if (sigla.equals("es")){
				getContext().setLocale(java.util.Locale.forLanguageTag("es"));
			}
			return new MensagemResult("OK","Idioma trocado para: "+sigla);
		}else{
			return new MensagemResult("NOK","Erro no webservice");
		}

	}

	
	@Override
	protected boolean isWsTokenOn() {
		return false;
	}

	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}