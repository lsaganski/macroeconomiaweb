package br.livetouch.livecom.rest.resource;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Application;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.ApplicationService;
import br.livetouch.livecom.rest.constraint.ApplicationUnique;
import br.livetouch.livecom.rest.domain.ApplicationVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import br.livetouch.livecom.rest.domain.UserPrincipal;
import net.livetouch.tiger.ddd.DomainException;

@Path("/app")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
@RolesAllowed({"admin"})
public class ApplicationResource {

	protected static final Logger log = Log.getLogger(ApplicationResource.class);
	
	@Autowired
	private ApplicationService applicationService;
	
	@Context
	private SecurityContext securityContext;
	
	@POST
	public MessageResult post(@ApplicationUnique @Valid Application app) {
		try {
			app.setUsuario(getUser());
			applicationService.generateKeys(app);
			applicationService.saveOrUpdate(app);
		} catch (DomainException e) {
			log.debug("Erro ao cadastrar uma aplicação oAuth " + e.getMessage(), e);
			return MessageResult.error("Desculpe, occoreu um erro ao tentar realizar o cadastro da aplicação");
		}
		return MessageResult.ok("Aplicação cadastrada com sucesso");
	}
	
	@PUT
	public MessageResult put(@ApplicationUnique @Valid Application app) {
		try {
			applicationService.update(app);
		} catch (DomainException e) {
			log.debug("Erro ao atualizar a aplicação oauth  " + app.getId() + " : " + e.getMessage(), e);
			return MessageResult.error("Desculpe, occoreu um erro ao tentar atualizar a aplicação");
		}
		return MessageResult.ok("Aplicação atualizada com sucesso", app);
	}
	
	@DELETE
	@Path("/{id}")
	public MessageResult delete(@PathParam("id") Long id) {
		
		Application app = applicationService.get(id);
		try {
			if(app == null) {
				return MessageResult.error("Aplicação não foi localizada");
			}
			applicationService.delete(app);
			return MessageResult.ok("Aplicação deletada com sucessso");
		} catch (DomainException e) {
			log.debug("Erro ao deletar a aplicação oauth  " + app.getId() + " : " + e.getMessage(), e);
			return MessageResult.error("Desculpe, occoreu um erro ao tentar deletar está aplicação");
		}
		
	}
	
	@GET
	@Path("/{id}")
	public MessageResult get(@PathParam("id") Long id) {
		
		Application app = applicationService.get(id, getUser());
		
		if(app == null) {
			throw new WebApplicationException(404);
		}
		return MessageResult.ok(new ApplicationVO(app));
	}
	
	@GET
	public List<ApplicationVO> getAll() {
		return ApplicationVO.createByList(applicationService.findByUsuario(getUser()));
	}
	
	protected Usuario getUser() {
		UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
		return userPrincipal.getUsuario();
	}
}
