package br.livetouch.livecom.domain.vo;

/**
 * Guarda o id, login e badge de um usuário.
 * 
 * A badge pode ser a soma de post, like, favorito, comentario, mensagem.
 * 
 * Esta badge é o número enviado pro iPhone por Push.
 * 
 * @author rlech
 *
 */
public class PushBadgeVO {
	private long userId;
	private String login;
	// count da badge que vai pro user no push
	private long count;
	private String nome;
	private String email;

	public PushBadgeVO(Long userId, String login, Long count, String nome, String email) {
		super();
		this.setNome(nome);
		this.setEmail(email);
		this.userId = userId != null ? userId : 0L;
		this.login = login;
		this.count = count != null ? count : 0L;
	}

	public Long getCount() {
		return count;
	}

	public String getLogin() {
		return login;
	}

	public Long getUserId() {
		return userId;
	}
	
	/**
	 * Incrementa o total das badges
	 * 
	 * @param count
	 */
	public void increment(long count) {
		this.count += count;
	}

	@Override
	public String toString() {
		return "PushBadgeVO [userId=" + userId + ", login=" + login + ", count=" + count + "]";
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
