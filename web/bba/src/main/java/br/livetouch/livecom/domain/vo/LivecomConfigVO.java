package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.List;

public class LivecomConfigVO implements Serializable{
	private static final long serialVersionUID = -7935645968542724016L;
	public List<ParametroVO> parametros;
	public List<MenuMobileVO> menuMobile;
	public String serverUrl;
	public String socketIp;
	public String socketPort;
	public String menuList;
	public String chatOn;
	public String dynamicMenuOn;
	public String navBarOptiosMenuOn;
	public String showAudienciaPost;
	public String loginBtnColor;
	public String loginAutomatico;
	public String navbarColor;
	public String navbarBadgeBackgroundColor;
	public String navbarBadgeTextColor;
	public String menuBackgroundColor;
	public String menuCellBackgroundColor;
	public String menuCellSelectedBackgroundColor;
	public String menuCellTextColor;
	public String menuCellSelectedTextTolor;
	public String menuBadgeTextColor;
	public String showLabelGroupsPost;
	public String showBtnCurtir;
	public String pushAlertviewColor;
	public String showButtonNewPost;
	public String statusPost;
	public String showWhatsNews;
	public String lockLogin;
}
