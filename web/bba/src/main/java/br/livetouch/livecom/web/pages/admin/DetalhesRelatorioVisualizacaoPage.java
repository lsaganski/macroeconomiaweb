package br.livetouch.livecom.web.pages.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoFiltro;
import br.livetouch.livecom.web.pages.report.RelatorioPage;

@Controller
@Scope("prototype")
public class DetalhesRelatorioVisualizacaoPage extends RelatorioPage {

	public int page;
	public Long id;
	
	public Post post;

	@Override
	public void onInit() {
		super.onInit();
		
		if(id != null) {
			post = postService.get(id);
		}
	}

	@Override
	public void onPost() {
		super.onPost();
		RelatorioVisualizacaoFiltro filtro = (RelatorioVisualizacaoFiltro) getContext().getSessionAttribute(RelatorioVisualizacaoFiltro.SESSION_FILTRO_KEY);
		Post post = postService.get(id);
		if (post != null) {
			String csv;
			if (filtro != null) {
				Usuario usuario = null;
				if (filtro.getUsuarioId() != null) {
					usuario = usuarioService.get(filtro.getUsuarioId());
				}
				if (usuario != null) {
					csv = reportService.csvDetalhesVisualizacao(usuario, post);
				} else {
					csv = reportService.csvDetalhesVisualizacao(null, post);
				}
			} else {
				csv = reportService.csvDetalhesVisualizacao(null, post);
			}
			download(csv, "detalhes-relatorio-visualizacao.csv");
		}
	}
}
