package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.chatAkka.DateUtils;
import br.livetouch.livecom.domain.Feriado;

public class FeriadoVO implements Serializable{
	private static final long serialVersionUID = 511847560768136130L;
	public Long id;
	public String nome;
	public String codigo;
	public String codigoDesc;
	public String data;
	
	
	public static List<FeriadoVO> fromList(List<Feriado> feriados) {
		List<FeriadoVO> vos = new ArrayList<>();
		if(feriados == null) {
			return vos;
		}
		for (Feriado a : feriados) {
			vos.add(new FeriadoVO(a, true));
		}
		return vos;
	}
	
	public FeriadoVO(Feriado feriado, Boolean isFindDirExc) {
		setData(feriado, isFindDirExc);
	}

	private void setData(Feriado feriado, Boolean isFindDirExc) {
		this.id = feriado.getId();
		this.nome = feriado.getNome();
		this.codigo = feriado.getCodigo();
		this.data = DateUtils.toString(feriado.getData(), "dd/MM/yyyy");
		this.codigoDesc = codigo != null ? codigo : id.toString();
	}

	public FeriadoVO(Feriado feriado) {
		setData(feriado, false);
	}

	public static FeriadoVO from(Feriado feriado) {
		return new FeriadoVO(feriado);
	}
}
