package br.livetouch.livecom.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Menu extends JsonEntity{
	
	private static final long serialVersionUID = 8771454278547884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "MENU_SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent", nullable = true)
	private Menu parent;
	
	private String label;
	private String link;
	private String icone;

	private String descricao;
	private boolean mobile;
	private String parametro;

	private int ordem;

	private Boolean linkExterno;
	private Boolean divider = false;
	
	@OneToMany(mappedBy = "menu", fetch = FetchType.LAZY)
	private Set<MenuPerfil> perfis;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;
    private boolean menuDefault;

    public Menu() {
		
	}
	
	public Menu(Menu menu) {
		this.label = menu.getLabel();
		this.link = menu.getLink();
		this.icone = menu.getIcone();
		this.descricao = menu.getDescricao();
		this.ordem = menu.getOrdem();
		this.linkExterno = menu.isLinkExterno();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getIcone() {
		return icone;
	}

	public void setIcone(String icone) {
		this.icone = icone;
	}

	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	public Menu getParent() {
		return parent;
	}

	public void setParent(Menu parent) {
		this.parent = parent;
	}
	
	@Override
	public String toJson() {
		return "";
	}

	public Set<MenuPerfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(Set<MenuPerfil> perfis) {
		this.perfis = perfis;
	}

	public Boolean isLinkExterno() {
		if(linkExterno == null) {
			return false;
		}
		return linkExterno;
	}

	public void setLinkExterno(Boolean linkExterno) {
		this.linkExterno = linkExterno;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public boolean isMobile() {
		return mobile;
	}

	public void setMobile(boolean mobile) {
		this.mobile = mobile;
	}

	public String getParametro() {
		return parametro;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

    public void setDefault(boolean aDefault) {
        this.menuDefault = aDefault;
    }

    public boolean isMenuDefault() {
        return menuDefault;
    }

	public Boolean isDivider() {
		return divider;
	}

	public void setDivider(Boolean divider) {
		this.divider = divider;
	}
}
