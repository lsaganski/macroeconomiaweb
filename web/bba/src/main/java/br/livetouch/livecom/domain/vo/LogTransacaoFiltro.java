package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusTransacao;
import br.livetouch.livecom.utils.DateUtils;

/**
 * Filtro com todos os campos de LogSistema, mas a dataInicial e dataFinal
 * 
 * @author ricardo
 *
 */
public class LogTransacaoFiltro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String SESSION_FILTRO_KEY = "LogTransacaoFiltro";

	private Date dataInicial;
	private Date dataFinal;
	private String requestPath;
	private Long id;
	private StatusTransacao status;
	private int max;
	private Usuario usuario;
	private Long usuarioId;
	
	private Empresa empresa;
	
	public Date getDataInicial() {
		return dataInicial;
	}
	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}
	public Date getDataFinal() {
		return dataFinal;
	}
	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}
	public String getRequestPath() {
		return requestPath;
	}
	public void setRequestPath(String requestPath) {
		this.requestPath = requestPath;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public StatusTransacao getStatus() {
		return status;
	}
	public void setStatus(StatusTransacao status) {
		this.status = status;
	}
	public int getMax() {
		return max;
	}
	public void setMax(int max) {
		this.max = max;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Long getUsuarioId() {
		return usuarioId;
	}
	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getDataInicioStringDMY() {
		return DateUtils.toString(dataInicial, "dd/MM/yyyy");
	}

	public String getDataFimStringDMY() {
		return DateUtils.toString(dataFinal, "dd/MM/yyyy");
	}
	
}
