package br.livetouch.livecom.web.pages.training.admin;


import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboCategoriaPostChildren;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.web.pages.training.TrainingUtil;
import net.livetouch.click.control.IdField;
import net.sf.click.control.Form;
import net.sf.click.control.HiddenField;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class CategPage extends CategsPage {

	public Form form = new Form();
	public String msgErro;
	public Long id;

	public int page;

	public CategoriaPost categoriaPost;
	private TextField tCategoriaPath;
	private HiddenField tCategoriaPostId;

	@Override
	public void onInit() {
		super.onInit();

		form();
		
		if (id != null) {
			categoriaPost = categoriaPostService.get(id);
		} else {
			categoriaPost = new CategoriaPost();
		}
		form.copyFrom(categoriaPost);
	
		TrainingUtil.setTrainingStyle(form);
	}
	
	public void form(){
		form.add(new IdField());

		TextField t = new TextField("nome");
		t.setAttribute("autocomplete", "off");
		t.setFocus(true);
		form.add(t);

		t = new IntegerField("ordem");
		t.setAttribute("autocomplete", "off");
		t.setFocus(true);
		form.add(t);
		
		form.add(new ComboCategoriaPostChildren("categoriaParent",categoriaPostService, getEmpresa()));
		
		tCategoriaPostId = new HiddenField("categoria_post_id", Long.class);
		form.add(tCategoriaPostId);
		
		Submit tsalvar = new Submit("salvar", this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);
		
		if (id != null) {
			Submit tDeletar = new Submit("deletar", this, "deletar");
			tDeletar.setAttribute("class", "botao btSalvar");
			form.add(tDeletar);
		}
		Submit cancelar = new Submit("cancelar",this,"cancelar");
		cancelar.setAttribute("class", "botao");
		form.add(cancelar);

		setFormTextWidth(form);
	}
	
	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				categoriaPost = id != null ? categoriaPostService.get(id) : new CategoriaPost();
				form.copyTo(categoriaPost);
				
				Long id = (Long) tCategoriaPostId.getValueObject();
				if(id != null && id > 0) {
					CategoriaPost parent = categoriaPostService.get(id);
					categoriaPost.setCategoriaParent(parent);
				}

				categoriaPostService.saveOrUpdate(getUsuario(),categoriaPost);

				setFlashAttribute("msg",  "CategoriaPost ["+categoriaPost.getNome()+"] salvo com sucesso.");
				setRedirect(CategsPage.class);

				return false;

			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean cancelar() {
		setRedirect(CategsPage.class);
		return true;
	}

	public boolean deletar() {
		try {
			try {
				categoriaPost = id != null ? categoriaPostService.get(id) : new CategoriaPost();

				categoriaPostService.delete(getUsuario(),categoriaPost);

				setFlashAttribute("msg",  "CategoriaPost ["+categoriaPost.getNome()+"] deletado com sucesso.");
				setRedirect(CategsPage.class);

				return false;

			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = "Entidade possui relacionamentos. Não foi possível excluir.";
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();
		
		if(categ != null) {
			tCategoriaPostId.setValueObject(categ.getId());
			tCategoriaPath.setValue(categ.getPath());
		}
	}
}
