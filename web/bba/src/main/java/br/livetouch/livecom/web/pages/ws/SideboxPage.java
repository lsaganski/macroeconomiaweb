package br.livetouch.livecom.web.pages.ws;


import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.vo.UserInfoVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class SideboxPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private TextField tSidebox;

	public Long id;
	
	@Override
	public void onInit() {
		super.onInit();

		form.add(tSidebox = new TextField("sidebox_tab"));
		tSidebox.setFocus(true);
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		form.add(new Submit("consultar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			
			UserInfoVO userInfo = getUserInfoVO();
			
			String tab = tSidebox.getValue();
			if(StringUtils.isNotEmpty(tab)) {
				if(userInfo != null) {
					userInfo.setSideboxTab(tab);
					return new MensagemResult("OK","Sidebox alterado.");
				}
				else{
					return new MensagemResult("OK","Usuario não logado (null).");
				}
				
			}else{
				return new MensagemResult("NOK","Sidebox em branco.");
			}
		}
		return new MensagemResult("NOK","Sidebox não validado.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("result", MensagemResult.class);
		super.xstream(x);
	}
}
