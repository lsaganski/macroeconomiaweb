package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.PermissaoCategoria;

public class PermissaoCategoriaVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Long id;
	public String nome;
	public String codigo;

	public PermissaoCategoriaVO() {
		
	}

	public PermissaoCategoriaVO(PermissaoCategoria p) {
		id = p.getId();
		nome = p.getNome();
		codigo = p.getCodigo();
	}

	public static List<PermissaoCategoriaVO> fromList(List<PermissaoCategoria> categorias) {
		List<PermissaoCategoriaVO> vos = new ArrayList<>();
		if(categorias == null) {
			return vos;
		}
		for (PermissaoCategoria p : categorias) {
			PermissaoCategoriaVO categoriasVO = new PermissaoCategoriaVO(p);
			vos.add(categoriasVO);
		}
		return vos;
	}

}
