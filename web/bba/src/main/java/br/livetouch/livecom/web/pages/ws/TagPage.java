package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.vo.TagVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Juillian Lee 
 * 
 */
@Controller
@Scope("prototype")
public class TagPage extends WebServiceXmlJsonPage {
	public Form form = new Form();
	private TextField tNome;
	
	private Tag tag;
	
	public Long id;

	protected void form() {
		form.add(tNome = new TextField("nome", true));
		form.add(new TextField("wsVersion"));
		form.add(new Submit("Enviar"));
		
	}
	
	@Override
	public void onInit() {
		super.onInit();
		form();
		
		if (id != null) {
			tag = tagService.get(id);
		} else {
			tag = new Tag();
		}
	}

	@Override
	protected Object execute() throws Exception {
		if(tag == null) {
			tag = new Tag();
		}
		form.copyTo(tag);
		if(getUsuario() != null){
			tagService.saveOrUpdate(getUsuario(), tag);
		} else {
			if(isWsVersion3()) {
				Response r = Response.error("Usuario null");
				return r;
			}
			return new MensagemResult("NOK","Usuario nulo.");
		}
		TagVO tagVO = new TagVO();
		tagVO.setTag(tag);
		
		if(isWsVersion3()) {
			Response r = Response.ok("OK");
			r.tag = tagVO;
			return r;
		}
		
		return tagVO;
			
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("tag", TagVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
