package br.livetouch.livecom.web.pages.admin;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.files.FTPFileManager;
import br.livetouch.livecom.web.pages.LivecomRootPage;
import net.sf.click.extras.tree.Tree;
import net.sf.click.extras.tree.TreeNode;

@Controller
@Scope("prototype")
public class FtpTreePage extends LivecomRootPage {
	public static final String TREE_NODES_SESSION_KEY = "ftpTreeNodes";

	protected Tree tree;

	public String entrada;

	public boolean clear;

	@Override
	public void onInit() {
		super.onInit();

		entrada = ParametrosMap.getInstance(getEmpresaId()).get(Params.FILE_MANAGER_FTP_WORKPLACE);

		try {
			tree = buildTree();
		} catch (IOException e) {
			e.printStackTrace();
		}
		addControl(tree);

		if (clear) {
			clearSession();
		}
	}

	@Override
	public void onGet() {
		String selectId = getContext().getRequestParameter(Tree.SELECT_TREE_NODE_PARAM);
		String expandId = getContext().getRequestParameter(Tree.EXPAND_TREE_NODE_PARAM);
		if (selectId == null && expandId == null) {
			return;
		}

		TreeNode node = null;
		if (selectId != null) {
			node = tree.find(selectId);
		} else {
			node = tree.find(expandId);
		}
		if (!node.hasChildren()) {
			try {
				baixarArquivo(node);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void baixarArquivo(TreeNode node) throws IOException {
		int level = node.getLevel();
		String entrada = ParametrosMap.getInstance(getEmpresaId()).get(Params.FILE_MANAGER_FTP_WORKPLACE);
		if (StringUtils.isEmpty(entrada)) {
			setMessageError(getMessage("pastaDeEntrada.error"));
		}
		String caminho = entrada;
		String aux = "";
		String nomeArquivo = node.getValue().toString();
		for (int i = 0; i < level; i++) {
			TreeNode parent = node.getParent();
			String p = parent.getValue().toString();
			if (StringUtils.equals(p, "root")) {
				break;
			}
			aux = p + "/" + aux;
			node = parent;
		}
		caminho += "/" + aux;

		FTPFileManager ftp = new FTPFileManager(getParametrosMap());
		byte[] bytes = ftp.getBytesFromFile(caminho, nomeArquivo);
		if (bytes != null) {
			downloadFile(bytes, nomeArquivo);
		}
	}

	protected Tree createTree() {
		return new Tree("tree");
	}

	protected Tree buildTree() throws IOException {
		tree = createTree();

		TreeNode existingRootNode = loadNodesFromSession();

		if (existingRootNode != null) {
			tree.setRootNode(existingRootNode);
			return tree;
		}

		FTPFileManager ftp = new FTPFileManager(getParametrosMap());
		TreeNode root = ftp.getTree();
		tree.setRootNode(root);
		tree.expand(root);
		storeNodesInSession(root);

		return tree;
	}

	protected String getSessionKey() {
		return TREE_NODES_SESSION_KEY;
	}

	protected void storeNodesInSession(TreeNode rootNode) {
		if (tree.getRootNode() == null) {
			return;
		}
		getContext().getSession().setAttribute(getSessionKey(), rootNode);
	}

	protected void clearSession() {
		if (tree.getRootNode() == null) {
			return;
		}
		getContext().getSession().setAttribute(getSessionKey(), null);
	}

	protected TreeNode loadNodesFromSession() {
		return (TreeNode) getContext().getSession().getAttribute(getSessionKey());
	}
}