package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.StatusMensagemUsuarioVO;
import br.livetouch.livecom.domain.vo.ThumbVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class MensagemPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	
	public Long id;
	private UsuarioLoginField tUser;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		IntegerField tId = null;
		form.add(tId = new IntegerField("id"));
		form.add(tUser = new UsuarioLoginField("user_id", false, usuarioService, getEmpresa()));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));
		
		tId.setFocus(true);
		tMode.setValue("json");

		form.add(new Submit("Enviar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario user = tUser.getEntity();
			Mensagem mensagem = mensagemService.get(id);
			
			if (mensagem == null) {
				return new MensagemResult("NOK","Mensagem não encontrada");
			}
			if (user == null) {
				return new MensagemResult("NOK","Acesso negado.");
			}
			
			boolean ok = mensagem.isFromTo(user);
			if(!ok) {
				return new MensagemResult("NOK","Acesso negado.");
			}

			MensagemVO vo = new MensagemVO();
			vo.setMensagem(mensagem, user);
			return vo;
		}
		return new MensagemResult("NOK","Erro ao listar mensagens.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("conversa", ConversaVO.class);
		x.alias("msg", MensagemVO.class);
		x.alias("arquivo", FileVO.class);
		x.alias("status", StatusMensagemUsuarioVO.class);
		x.alias("arquivo", ThumbVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
