package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.QueryParam;

import br.livetouch.livecom.domain.enums.OrigemCadastro;
import br.livetouch.livecom.domain.enums.StatusUsuario;

/**
 * Filtro com todos os campos de LogSistema, mas a dataInicial e dataFinal
 * 
 * @author ricardo
 *
 */
public class UsuarioAutocompleteFiltro implements Serializable {

	
	private static final long serialVersionUID = -7242370332782831326L;

	public static final String SESSION_FILTRO_KEY = "usuarioFiltro";

	@QueryParam("page")
	private int page;
	@QueryParam("max")
	private int max;
	@QueryParam("maxRows")
	private int maxRows;
	@QueryParam("login")
	private String login;
	@QueryParam("nome")
	private String nome;
	@QueryParam("q")
	private String q;
	@QueryParam("grupoId")
	private Long grupoId;
	
	private List<Long> usuariosIds;
	
	private List<Long> notUsersIds;
	private List<Long> gruposIds;
	
	@QueryParam("ativo")
	private boolean ativo;
	@QueryParam("buscaTotal")
	private boolean buscaTotal;
	@QueryParam("coluna")
	private String coluna;
	@QueryParam("ordenacao")
	private String ordenacao;
	@QueryParam("cargoId")
	private Long cargoId;
	@QueryParam("acesso")
	private String acesso;
	@QueryParam("origem")
	private String origem;
	@QueryParam("postar")
	private boolean podePostar;
	
	@QueryParam("statusAtivacao")
	private Integer statusAtivacao;
	
	private StatusUsuario statusUsuario;
	private OrigemCadastro origemCadastro;
	
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(int maxRows) {
		this.maxRows = maxRows;
	}

	public String getQ() {
		return q;
	}

	public void setQ(String q) {
		this.q = q;
	}

	public Long getGrupoId() {
		return grupoId;
	}

	public void setGrupoId(Long grupoId) {
		this.grupoId = grupoId;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isBuscaTotal() {
		return buscaTotal;
	}

	public void setBuscaTotal(boolean buscaTotal) {
		this.buscaTotal = buscaTotal;
	}

	public String getColuna() {
		return coluna;
	}

	public void setColuna(String coluna) {
		this.coluna = coluna;
	}

	public String getOrdenacao() {
		return ordenacao;
	}

	public void setOrdenacao(String ordenacao) {
		this.ordenacao = ordenacao;
	}

	public Long getCargoId() {
		return cargoId;
	}

	public void setCargoId(Long cargoId) {
		this.cargoId = cargoId;
	}

	public String getAcesso() {
		return acesso;
	}

	public void setAcesso(String acesso) {
		this.acesso = acesso;
	}

	public List<Long> getNotUsersIds() {
		return notUsersIds;
	}

	public void setNotUsersIds(List<Long> notUsersIds) {
		this.notUsersIds = notUsersIds;
	}

	public List<Long> getGruposIds() {
		return gruposIds;
	}

	public void setGruposIds(List<Long> gruposIds) {
		this.gruposIds = gruposIds;
	}

	public Integer getStatusAtivacao() {
		return statusAtivacao;
	}

	public void setStatusAtivacao(Integer statusAtivacao) {
		this.statusAtivacao = statusAtivacao;
	}

	public StatusUsuario getStatusUsuario() {
		return statusUsuario;
	}

	public void setStatusUsuario(StatusUsuario statusUsuario) {
		this.statusUsuario = statusUsuario;
	}

	public List<Long> getUsuariosIds() {
		return usuariosIds;
	}

	public void setUsuariosIds(List<Long> usuariosIds) {
		this.usuariosIds = usuariosIds;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public OrigemCadastro getOrigemCadastro() {
		return origemCadastro;
	}

	public void setOrigemCadastro(OrigemCadastro origemCadastro) {
		this.origemCadastro = origemCadastro;
	}

	public boolean isPodePostar() {
		return podePostar;
	}

	public void setPodePostar(boolean podePostar) {
		this.podePostar = podePostar;
	}

}
