package br.livetouch.livecom.web.pages.admin;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.StatusMensagemUsuario;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.RelatorioConversaFiltro;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
import net.livetouch.click.table.PaginacaoTable;
import net.sf.click.Context;
import net.sf.click.control.Column;
import net.sf.click.control.Decorator;



@Controller
@Scope("prototype")
public class DetalhesStatusRelatorioConversaPage extends RelatorioPage {

	public Long mensagemId;
	public Mensagem mensagem;
	public RelatorioConversaFiltro filtro;
	public List<MensagemVO> vos;
	public PaginacaoTable table = new PaginacaoTable();
	public int page;

	@Override
	public void onGet() {
		filtro = (RelatorioConversaFiltro) getContext().getSessionAttribute(RelatorioConversaFiltro.SESSION_FILTRO_KEY);
		if(filtro != null) {
			filtro.setPage(page);
		}
	}
	
	@Override
	public void onInit() {
		
		if(mensagemId != null) {
			filtro = (RelatorioConversaFiltro) getContext().getSessionAttribute(RelatorioConversaFiltro.SESSION_FILTRO_KEY);
			if(filtro != null) {
				filtro.setPage(page);
			}
			
			mensagem = mensagemService.get(mensagemId);
		}
		
		table();
	}
	
	@Override
	public String getContentType() {
		return super.getContentType();
	}
	
	@Override
	public String getTemplate() {
		return super.getTemplate();
	}
	
	private void table() {
		Column c = new Column("dataEntregue", getMessage("data.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);
		
		c = new Column("dataLida", getMessage("data.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);
		
		c = new Column("mensagem.msg", getMessage("coluna.mensagem.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("usuario.nome", getMessage("to.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);
		
		c = new Column(getMessage("status.conversa.label"));
		c.setDecorator(new Decorator() {
			
			@Override
			public String render(Object object, Context context) {
				StatusMensagemUsuario smu = (StatusMensagemUsuario) object;
				if(smu.isLida()) {
					//icon lida
					return "<i class='icones lida'></i>";
				} else if (smu.isEntregue()) {
					//icon entregue
					return "<i class='icones entregue'></i>";
				} else {				
					//icon enviada
					return "<i class='icones enviada'></i>";
				}
				
			}
		});
		c.setAttribute("align", "center");
		table.addColumn(c);
		
	}
	
	@Override
	public void onRender() {
		super.onRender();

			List<StatusMensagemUsuario> mensagens = null;
			mensagens = statusMensagemUsuarioService.findAllByMensagem(mensagem);
//			long count = mensagemService.getCountDetalhesByConversa(filtro);
//			table.setCount(count);
			table.setPageSize(50);
			
			table.setRowList(mensagens);
		}
		
}
