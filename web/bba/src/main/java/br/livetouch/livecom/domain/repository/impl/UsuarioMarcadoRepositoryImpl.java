package br.livetouch.livecom.domain.repository.impl;


import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.UsuarioMarcado;
import br.livetouch.livecom.domain.repository.UsuarioMarcadoRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class UsuarioMarcadoRepositoryImpl extends StringHibernateRepository<UsuarioMarcado> implements UsuarioMarcadoRepository {

	public UsuarioMarcadoRepositoryImpl() {
		super(UsuarioMarcado.class);
	}

	@Override
	public UsuarioMarcado find(Comentario comentario, Usuario usuario) {
		Query query = createQuery("FROM UsuarioMarcado u WHERE u.usuario.id = :usuarioId AND u.comentario.id = :comentarioId");
		query.setParameter("usuarioId", usuario.getId());
		query.setParameter("comentarioId", comentario.getId());
		query.setCacheable(true);
		return (UsuarioMarcado) (query.list().size() > 0 ? query.list().get(0) : null);
	}

	@Override
	public void delete(List<Long> idsMarcados, Comentario comentario) {
		Query query = createQuery("DELETE FROM UsuarioMarcado u WHERE u.usuario.id not in(:idsUsuario) AND u.comentario.id = :comentarioId");
		query.setParameterList("idsUsuario", idsMarcados);
		query.setParameter("comentarioId", comentario.getId());
		query.executeUpdate();
	}

}