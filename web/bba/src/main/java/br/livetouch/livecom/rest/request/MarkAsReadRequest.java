package br.livetouch.livecom.rest.request;

import java.io.Serializable;
import java.util.List;

import br.livetouch.livecom.utils.JSONUtils;

public class MarkAsReadRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	public List<Long> cIds;

	public Long from;
	
	@Override
	public String toString() {
		String json = JSONUtils.toJSON(this);
		return json;
	}
}
