package br.livetouch.livecom.domain.vo;

import java.io.File;
import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.livetouch.livecom.domain.ArquivoThumb;

public class ThumbVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;

	public String url;
	private Integer cod;
	public Integer width;
	public Integer height;

	@XStreamOmitField
	private File file;

	public ThumbVO() {
	}
	
	public ThumbVO(Integer cod , Integer width, Integer height) {
		super();
		this.cod = cod;
		this.width = width;
		this.height = height;
	}


	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
	public int getCod() {
		return cod;
	}
	public void setCod(int id) {
		this.cod = id;
	}

	public File getFile() {
		return file;
	}

	public void setArquivoThumb(ArquivoThumb t) {
//		this.cod = t.getCod();
		this.id = t.getId();
		this.width = t.getWidth();
		this.height = t.getHeight();
		this.url = t.getUrl();
	}

	@Override
	public String toString() {
		return "ThumbVO [width=" + width + ", height=" + height + ", url=" + url + "]";
	}

}
