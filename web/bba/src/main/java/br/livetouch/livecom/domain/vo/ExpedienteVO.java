package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Expediente;

public class ExpedienteVO implements Serializable{
	private static final long serialVersionUID = 511847560768136130L;
	public Long id;
	public String codigo;
	
	public boolean segunda;
	public boolean terca;
	public boolean quarta;
	public boolean quinta;
	public boolean sexta;
	public boolean sabado;
	public boolean domingo;
	
	public ExpedienteVO(Expediente expediente) {
		this.id = expediente.getId();
		this.codigo = expediente.getCodigo();
		this.segunda = expediente.isSegunda();
		this.terca = expediente.isTerca();
		this.quarta = expediente.isQuarta();
		this.quinta = expediente.isQuinta();
		this.sexta = expediente.isSexta();
		this.sabado = expediente.isSabado();
		this.domingo = expediente.isDomingo();
	}
	
	public static List<ExpedienteVO> fromList(List<Expediente> expedientes) {
		List<ExpedienteVO> vos = new ArrayList<>();
		if(expedientes == null) {
			return vos;
		}
		for (Expediente e : expedientes) {
			vos.add(new ExpedienteVO(e));
		}
		return vos;
	}

}
