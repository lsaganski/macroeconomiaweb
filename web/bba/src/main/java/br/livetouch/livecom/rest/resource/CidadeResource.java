package br.livetouch.livecom.rest.resource;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Cidade;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.vo.CidadeVO;
import br.livetouch.livecom.domain.vo.CidadesVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/cidade")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class CidadeResource extends MainResource {
	protected static final Logger log = Log.getLogger(CidadeResource.class);
	
	@POST
	public Response create(Cidade cidade) {
		
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Cidade.isFormValid(cidade, false);
			
			//Validar se ja existe uma area com o mesmo nome
			boolean existe = cidadeService.findByName(cidade, getEmpresa()) != null;
			if(existe) {
				log.debug("Ja existe uma cidade com este nome");
				return Response.ok(MessageResult.error("Ja existe uma cidade com este nome")).build();
			}
			
			cidadeService.saveOrUpdate(cidade);
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", CidadeVO.from(cidade))).build();
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel efetuar o cadastro da cidade")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@PUT
	public Response update(Cidade cidade) {
		
		try {
			Cidade.isFormValid(cidade, true);
			
			//Validar se ja existe uma area com o mesmo nome
			boolean existe = cidadeService.findByName(cidade, getEmpresa()) != null;
			if(existe) {
				log.debug("Ja existe uma cidade com este nome");
				return Response.ok(MessageResult.error("Ja existe uma cidade com este nome")).build();
			}
			
			cidadeService.saveOrUpdate(cidade);
			return Response.ok(MessageResult.ok("Cidade atualiza com sucesso com sucesso", CidadeVO.from(cidade))).build();
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar a cidade")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
//	@GET
//	public Response findAll() {
//		List<Cidade> cidades = cidadeService.findAll();
//		List<CidadeVO> vos = CidadeVO.fromList(cidades);
//		return Response.ok().entity(vos).build();
//	}

	@GET
	public Response findByNome(@QueryParam("buscaTotal") String buscaTotal, @QueryParam("q") String nome, @QueryParam("page") int page, @QueryParam("maxRows") int maxRows) {
//		List<Cidade> cidades = cidadeService.findAll();
//		List<CidadeVO> vos = CidadeVO.fromList(cidades);
		
		List<Cidade> cidades = cidadeService.findByNome(nome, null, page, maxRows);
		CidadesVO vo = new CidadesVO();
		if(StringUtils.isNotEmpty(buscaTotal) || StringUtils.equals(buscaTotal, "1")) {
			Long count = cidadeService.countByNome(nome, null, page, maxRows);
			vo.total = count;
		}
		vo.cidades = cidades;
		
		return Response.ok().entity(vo).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		Cidade cidade = cidadeService.get(id);
		if(cidade == null) {
			return Response.ok(MessageResult.error("Cidade não encontrada")).build();
		}
		CidadeVO vo = new CidadeVO(cidade);
		return Response.ok().entity(vo).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			Cidade cidade = cidadeService.get(id);
			if(cidade == null) {
				return Response.ok(MessageResult.error("Cidade não encontrada")).build();
			}
			cidadeService.delete(cidade);
			return Response.ok().entity(MessageResult.ok("Cidade deletada com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir a Cidade  " + id)).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir a Cidade  " + id)).build();
		}
	}
	
	@GET
	@Path("/exportar")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response exportar() throws IOException {
		
		Empresa empresa = getEmpresa();
		String csv = cidadeService.csvCidade();
		
		String charset = ParametrosMap.getInstance(empresa).get(Params.EXPORTACAO_ARQUIVO_CHARSET, "ISO-8859-1");

		String attachament = "attachment; filename=" + URLEncoder.encode("cidades.csv", "UTF-8");
		ResponseBuilder b = Response.status(Status.OK);
		b.encoding(charset);
		b.header("Content-Disposition", attachament);
		b.header("Content-Type", "application/octet-stream"+";charset="+charset);
		b.entity(csv);
		Response r = b.build();
		return r;
	}
	
//	@GET
//	@Path("/filtro")
//	public Response findByName(@BeanParam FiltroArea filtro) {
//		List<Area> areas = areaService.filterArea(filtro, getEmpresa());
//		List<AreaVO> vo = AreaVO.fromList(areas);
//		return Response.status(Status.OK).entity(vo).build();
//	}
}
