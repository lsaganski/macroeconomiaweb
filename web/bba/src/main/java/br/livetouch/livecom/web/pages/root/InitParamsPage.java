package br.livetouch.livecom.web.pages.root;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.LivecomRootPage;

@Controller
@Scope("prototype")
public class InitParamsPage extends LivecomRootPage {

	public Date now = new Date();
	
	@Override
	public String getTemplate() {
		return getPath();
	}
	
	@Override
	public void onInit() {
		super.onInit();
		log("Parametros reiniciados");
		parametroService.init();
	}
}
