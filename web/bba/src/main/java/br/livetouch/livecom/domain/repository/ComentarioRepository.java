package br.livetouch.livecom.domain.repository;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.PostCountVO;
import br.livetouch.livecom.domain.vo.UserToPushVO;

@Repository
public interface ComentarioRepository extends net.livetouch.tiger.ddd.repository.Repository<Comentario> {

	List<Comentario> findAllByUserAndPost(long userId, long postId, int page, int maxRows);

	List<Comentario> findAllComentariosNotificationByUser(Usuario user);
	
	List<Long> findAllIdsByUser(Usuario user);

	/**
	 * Qtde de comentarios de cada post.
	 * 
	 * @param ids
	 * @return
	 */
	List<PostCountVO> findCountByPosts(List<Long> ids);

	Long countByPost(Post p);

	List<Usuario> findAllUsuariosQueComentaramPost(Post p, Usuario u);
	List<UserToPushVO> findAllUsersQueComentaramPost(Post p, Usuario u);

	/**
	 * Retorna q qtde de badges (comentarios nao lidos deste post).
	 * 
	 * @param ids
	 * @return
	 */
	List<PostCountVO> findBadgesByPosts(Usuario userInfo, List<Long> ids);

	List<Long> findAllByPost(Post p);

	List<Comentario> findByPost(Post p);

	List<Comentario> findAllByDate(Date data);

}