package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogImportacao;
import br.livetouch.livecom.domain.repository.LogImportacaoRepository;
import br.livetouch.livecom.domain.vo.LogImportacaoFiltro;
import br.livetouch.spring.StringHibernateRepository;
import net.livetouch.extras.util.DateUtils;

@Repository
public class LogImportacaoRepositoryImpl extends StringHibernateRepository<LogImportacao> implements LogImportacaoRepository {

	public LogImportacaoRepositoryImpl() {
		super(LogImportacao.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> findAllTiposCsv(Empresa empresa) {
		StringBuffer sb = new StringBuffer("select distinct arquivoNome from LogImportacao l where l.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setParameter("empresa", empresa);
		List<String> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LogImportacao> findAllByFiltro(LogImportacaoFiltro filtro, Empresa empresa) {
		Date inicial = null;
		Date dFinal = null;
			if (filtro.getDataInicial() != null) {
				inicial = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dFinal = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}

		StringBuffer sb = new StringBuffer("from LogImportacao p where 1=1 ");
		String arquivo = filtro.getArquivoNome();
		if (inicial != null) {
			sb.append(" and p.dataInicio >= :dataIni ");
		}
		if (dFinal != null) {
			sb.append(" and p.dataFim <= :dataFim ");
		}
		if (!StringUtils.isEmpty(arquivo) && !StringUtils.equals(arquivo, "- Todos -")) {
			sb.append(" and p.arquivoNome like :arquivo ");
		}
		if (empresa != null) {
			sb.append(" and p.empresa = :empresa ");
		}
		sb.append(" order by p.id desc");
		Query q = createQuery(sb.toString());

		if (inicial != null) {
			q.setParameter("dataIni", inicial);
		}
		if (dFinal != null) {
			q.setParameter("dataFim", dFinal);
		}
		if (!StringUtils.isEmpty(arquivo) && !StringUtils.equals(arquivo, "- Todos -")) {
			q.setString("arquivo", "%" + arquivo + "%");
		}
		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		q.setFirstResult(firstResult);
		q.setMaxResults(filtro.getMax());

		List<LogImportacao> list = q.list();
		return list;
	}

	@Override
	public long countByFiltro(LogImportacaoFiltro filtro, Empresa empresa) {
		Date inicial = null;
		Date dFinal = null;
			if (filtro.getDataInicial() != null) {
				inicial = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dFinal = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}

		StringBuffer sb = new StringBuffer("select count(*) from LogImportacao p where 1=1 ");
		String arquivo = filtro.getArquivoNome();
		if (inicial != null) {
			sb.append(" and p.dataInicio >= :dataIni ");
		}
		if (dFinal != null) {
			sb.append(" and p.dataFim <= :dataFim ");
		}
		if (!StringUtils.isEmpty(arquivo) && !StringUtils.equals(arquivo, "- Todos -")) {
			sb.append(" and p.arquivoNome like :arquivo ");
		}
		if (empresa != null) {
			sb.append(" and p.empresa = :empresa ");
		}
		Query q = createQuery(sb.toString());

		if (inicial != null) {
			q.setParameter("dataIni", inicial);
		}
		if (dFinal != null) {
			q.setParameter("dataFim", dFinal);
		}
		if (!StringUtils.isEmpty(arquivo) && !StringUtils.equals(arquivo, "- Todos -")) {
			q.setString("arquivo", "%" + arquivo + "%");
		}
		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}

		Object obj = q.uniqueResult();
		Long count = new Long(obj.toString());
		
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LogImportacao> findAll(Empresa empresa) {
		Query query = createQuery("from LogImportacao where empresa = :empresa order by dataInicio DESC");
		query.setParameter("empresa", empresa);
		return query.list();
	}

}