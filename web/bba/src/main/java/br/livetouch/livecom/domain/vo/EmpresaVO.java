package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Usuario;

public class EmpresaVO implements Serializable {
	private static final long serialVersionUID = -7138687327324169730L;
	
	public Long id;
	public String nome;
	public String dominio;
	public String trial;
	public String admin;
	public Long adminId;
	
	public EmpresaVO(Empresa empresa) {
		this.id = empresa.getId();
		this.nome = empresa.getNome();
		this.dominio = empresa.getDominio();
		this.trial = StringUtils.isNotEmpty(empresa.getTrialCode()) ? empresa.getTrialCode() : "";
		Usuario adm = empresa.getAdmin();
		if(adm != null) {
			this.admin = adm.getNome();
			this.adminId = adm.getId();
		}
	}
	
	public static List<EmpresaVO> fromList(List<Empresa> empresas) {
		ArrayList<EmpresaVO> list = new ArrayList<EmpresaVO>();
		if(empresas == null) {
			return list;
		}
		
		for (Empresa e : empresas) {
			list.add(new EmpresaVO(e));
		}
		return list;
	}
	
}
