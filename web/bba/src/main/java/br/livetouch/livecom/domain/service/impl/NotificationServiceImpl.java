package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.repository.NotificationRepository;
import br.livetouch.livecom.domain.repository.NotificationSearch;
import br.livetouch.livecom.domain.repository.NotificationUsuarioRepository;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.vo.AudienciaVO;
import br.livetouch.livecom.domain.vo.NotificationBadge;
import br.livetouch.livecom.domain.vo.NotificationVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioNotificationsVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoFiltro;
import br.livetouch.livecom.domain.vo.UserToPushVO;
import net.livetouch.tiger.ddd.DomainException;
import net.livetouch.tiger.ddd.Entity;


@Service
public class NotificationServiceImpl extends LivecomService<Notification> implements NotificationService {
	@Autowired
	private NotificationRepository rep;

	@Autowired
	private NotificationUsuarioRepository repUsuario;

	@Override
	public Notification get(Long id) {
		return rep.get(id);
	}

	@Override
	public List<Notification> findAllToSendPush() {
		return repUsuario.findAllToSendPush();
	}

	@Override
	public List<Long> findIdUsersFrom(Notification parent) {
		return rep.findIdUsersFrom(parent);
	}

	@Override
	public List<UserToPushVO> findUsersFrom(Notification parent) {
		return rep.findUsersFrom(parent);
	}

	@Override
	public void save(Notification n) throws DomainException {
		rep.saveOrUpdate(n);
	}


	@Override
	public List<NotificationUsuario> findAll(NotificationSearch search) {
		return rep.findAll(search);
	}

	@Override
	public List<NotificationUsuario> findAllAgrupada(NotificationSearch search) {
		return rep.findAllAgrupada(search);
	}

	@Override
	public List<Usuario> findNaoLidasByNotification(Notification n, NotificationSearch search) {
		return rep.findNaoLidasByNotification(n, search);
	}

	@Override
	public List<NotificationUsuario> findSolicitacoes(NotificationSearch search) {
		return rep.findSolicitacoes(search);
	}

	@Override
	public void delete(Notification n) throws DomainException {
		rep.deleteFilhas(n);
		rep.delete(n);
	}

	@Override
	public void deleteFilhas(Notification n) throws DomainException {
		rep.deleteFilhas(n);
	}

	@Override
	public void read(NotificationUsuario n) throws DomainException {
		n.setDataLida(new Date());
		n.setLida(true);
		saveOrUpdate(n);
	}

	@Override
	public void readAgrupada(NotificationUsuario notUser, Post p, TipoNotificacao tipo) throws DomainException {

		if(p == null) {
			read(notUser);
			return;
		}

		rep.markNotificationsAsRead(notUser, p, tipo);
	}

	@Override
	public Notification findByPost(Post p, TipoNotificacao tipo) {
		return rep.findByPost(p, tipo);
	}

	@Override
	public int markAllAsRead(Usuario u) {
		return rep.markAllAsRead(u);
	}

	@Override
	public List<NotificationUsuario> findPostVisivel(Usuario u) {
		return rep.findPostVisivel(u);
	}

	@Override
	public int markAsRead(NotificationUsuario notificationUsuario) {
		notificationUsuario.setDataLida(new Date());
		notificationUsuario.setLida(true);
		repUsuario.saveOrUpdate(notificationUsuario);
		return 1;
	}

	@Override
	public List<RelatorioNotificationsVO> reportNotifications(RelatorioFiltro filtro) throws DomainException {
		return rep.reportNotifications(filtro);
	}

	@Override
	public List<RelatorioNotificationsVO> reportNotificationsByType(RelatorioFiltro filtro) throws DomainException {
		return rep.reportNotificationsByType(filtro);
	}

	@Override
	public List<RelatorioNotificationsVO> reportNotificationsByTypeDetalhes(RelatorioFiltro filtro) throws DomainException {
		return rep.reportNotificationsByTypeDetalhes(filtro);
	}

	@Override
	public int markPostAsRead(Post p, Usuario u) {
		return rep.markPostAsRead(p, u);
	}

	@Override
	public int markGrupoAsRead(Grupo g, Usuario u) {
		return rep.markGrupoAsRead(g, u);
	}

	@Override
	public int markSolicitacaoAsRead(Grupo g, Usuario u) {
		return rep.markSolicitacaoAsRead(g, u);
	}

	@Override
	public int markSolicitacaoAmizadeAsRead(Usuario u, Usuario amigo) {
		return rep.markSolicitacaoAmizadeAsRead(u, amigo);
	}

	@Override
	public int markComentarioAsRead(Long p, Long u) {
		return rep.markComentarioAsRead(p, u);
	}

	@Override
	public void delete(List<Notification> notifications) {
		rep.delete(notifications);
	}

	@Override
	public NotificationBadge findBadges(Usuario u) {
		return rep.findBadges(u);
	}

	/**
	 * user[0] = Long id
	 * user[1] = String login
	 * user[2] = Long badge
	 */
	@Override
	public List<Object[]> findBadgesToPush(TipoNotificacao tipo, List<Long> ids) {
		if(tipo != null) {
			return rep.findBadgesToPush(tipo, ids); 
		} else {
			return rep.findUsersWithoutBadgesToPush(ids); 
		}
	}
	
	/**
	 * user[0] = Long id
	 * user[1] = String login
	 * user[2] = Long badge = 0
	 */
	@Override
	public List<Object[]> findUsersWithoutBadgesToPush(List<Long> ids) {
		return rep.findUsersWithoutBadgesToPush(ids);
	}

	@Override
	public List<Notification> findViewsByFilter(RelatorioVisualizacaoFiltro filtro) {
		return rep.findViewsByFilter(filtro);
	}

	@Override
	public List<NotificationVO> findAllGroupUserByPostDate(Usuario usuario, Post post) {
		return rep.findAllGroupUserByPost(usuario, post);
	}

	@Override
	public AudienciaVO getCountAudienciaByPost(Post post) {
		return rep.getCountAudienciaByPost(post);
	}

	@Override
	public void deleteBy(Object obj) {
		if( obj == null) {
			return;
		}
		List<Long> ids = null;

		Long id = ((Entity)obj).getId();
		List<Long> idsUsersNot = new ArrayList<>();
		if(obj instanceof Post) {
			ids = queryIds("select id from Notification n where n.post.id=?", false, id);
		} else if(obj instanceof Comentario) {
			ids = queryIds("select id from Notification n where n.comentario.id=?", false, id);
		} else if(obj instanceof Mensagem) {
			ids = queryIds("select id from Notification n where n.mensagem.id=?", false, id);
		} else if(obj instanceof Arquivo) {
			ids = queryIds("select id from Notification n where n.arquivo.id=?", false, id);
		} else if(obj instanceof Usuario) {
			ids = queryIds("select id from Notification n where n.usuario.id=?", false, id);
			idsUsersNot = queryIds("select id from NotificationUsuario n where n.usuario.id=?", false, id);
		} else if(obj instanceof Idioma) {
			ids = queryIds("select id from Notification n where n.idioma.id=?", false, id);
		}

		// deleta as notifications do usuario
		executeIn("delete from NotificationUsuario n where n.id in (:ids)","ids", idsUsersNot);
		
		// deleta as notificaoes filhas da notificacoes criadas pelo usuario
		executeIn("delete from NotificationUsuario n where n.notification.id in (:ids)","ids", ids);

		// deleta as notification criadas pelo usuario
		executeIn("delete from Notification n where n.id in (:ids)","ids", ids);
	}

	@Override
	public void deleteBy(Class<? extends Entity> cls, List<Long> idsToDelete) {
		if(idsToDelete == null || idsToDelete.isEmpty()) {
			return;
		}
		
		List<Long> ids = null;

		if(Post.class.equals(cls)) {
			ids = queryIdsIn("select n.id from Notification n where n.post.id in (:ids)",false,"ids", idsToDelete);
		} else if(Comentario.class.equals(cls)) {
			ids = queryIdsIn("select n.id from Notification n where n.comentario.id in (:ids)",false,"ids", idsToDelete);
		} else if(Mensagem.class.equals(cls)) {
			ids = queryIdsIn("select n.id from Notification n where n.mensagem.id in (:ids)",false,"ids", idsToDelete);
		} else if(Arquivo.class.equals(cls)) {
			ids = queryIdsIn("select n.id from Notification n where n.arquivo.id in (:ids)",false,"ids", idsToDelete);
		} else if(Usuario.class.equals(cls)) {
			ids = queryIdsIn("select n.id from Notification n where n.from.id in (:ids)",false,"ids", idsToDelete);
			ids.addAll(queryIdsIn("select id from Notification n where n.to.id in (:ids)",false,"ids", idsToDelete));
		}

//		executeIn("update Notification n set n.comentario=null,n.from=null,n.to=null,n.post=null,n.mensagem=null,n.arquivo=null where n.id in (:ids)","ids", ids);

		// deleta filhos
		executeIn("delete from NotificationUsuario notUser where notUser.notification.id in(:ids)", "ids", ids);
		
		// deleta notifications
		executeIn("delete from Notification n where n.id in (:ids)","ids", ids);

	}

	@Override
	public List<Notification> findAll() {
		return rep.findAll();
	}

	@Override
	public List<Notification> findByGrupo(Grupo g) {
		return rep.findByGrupo(g);
	}

	@Override
	public List<NotificationUsuario> findNotificationsFilhas(Notification notification) {
		return rep.findNotificationsFilhas(notification);
	}

	@Override
	public void saveOrUpdate(NotificationUsuario notification) {
		repUsuario.saveOrUpdate(notification);
	}

	@Override
	public int updateSendPushNotification(Long id, List<Long> userIds) {
		return repUsuario.updateSendPushNotification(id, userIds);
	}

	@Override
	public NotificationUsuario getNotificationUsuario(Long id) {
		return repUsuario.get(id);
	}

	@Override
	public void markAllAsRead(Long id) {
		repUsuario.markAllAsRead(id);
	}

}
