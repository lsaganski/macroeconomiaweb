package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.impl.ArquivoRepositoryImpl.TipoBuscaArquivo;
import br.livetouch.livecom.domain.vo.ArquivoFiltro;

@Repository
public interface ArquivoRepository extends net.livetouch.tiger.ddd.repository.Repository<Arquivo> {

	void deleteFrom(Post p);

	void deleteFrom(Mensagem c);

	void deleteFrom(Comentario c);

	List<Arquivo> findAllByUser(Usuario usuario, String nome,List<Tag> tags, TipoBuscaArquivo tipoBusca, int page, int pageSize);
	
	List<Arquivo> findAllByTipo(Usuario usuario, String[] tipoArquivo, TipoBuscaArquivo tipoBusca, int page, int pageSize, String nome);

	List<Arquivo> findAllByUser(ArquivoFiltro arquivoFiltro);

	List<Long> findIdsArquivoRefs(Arquivo a);

	List<Arquivo> findAll(Empresa e);

	List<Arquivo> findAllByTag(Tag tag);

	List<Long> findIdsByComentarios(Comentario c);

	List<Long> findIdsByUser(Usuario u);

	void updateFileDestaquePost(Post post);

	void deleteNotInCategorias(List<Long> arquivosIds, CategoriaPost categoria);

	ArquivoThumb find(Long id);

	List<Long> getIdsArquivosNotIn(Post p, List<Long> arquivoIds);

}