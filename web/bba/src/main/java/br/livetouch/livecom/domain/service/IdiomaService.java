package br.livetouch.livecom.domain.service;


import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Idioma;
import net.livetouch.tiger.ddd.DomainException;

public interface IdiomaService extends br.livetouch.livecom.domain.service.Service<Idioma> {

	List<Idioma> findAll();
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Idioma i) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Idioma i) throws DomainException;
	
	List<Idioma> findByCodigos(String[] idioma);

	Idioma findByCodigo(String idioma);
	
	Idioma findByCodigo(Idioma idioma);

	String getNomeByIdioma(CategoriaPost categoria, Idioma idioma);

}