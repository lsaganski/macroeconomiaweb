package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.impl.ArquivoRepositoryImpl.TipoBuscaArquivo;
import br.livetouch.livecom.domain.vo.ArquivoFiltro;
import br.livetouch.livecom.domain.vo.FileVO;
import net.livetouch.tiger.ddd.DomainException;

public interface ArquivoService extends Service<Arquivo> {

	List<Arquivo> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Arquivo c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(ArquivoThumb t);
	
	@Transactional(rollbackFor=Exception.class)
	void delete(ArquivoThumb t);

	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo, Arquivo c, boolean deleteArquivosAmazon) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Arquivo a) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo, List<Long> arquivosIds, boolean deleteArquivosAmazon) throws DomainException;

	List<Arquivo> findAllByIds(List<Long> arquivoIds);
	
	List<Arquivo> findAllByUser(Usuario usuario, String nome,List<Tag> tags, TipoBuscaArquivo tipoBusca, int page, int pageSize);
	
	List<Arquivo> findAllByTipo(Usuario usuario, String[] tipoArquivo, TipoBuscaArquivo tipoBusca, int page, int pageSize, String nome);
	
	List<FileVO> toListVo(Usuario user, List<Arquivo> arquivos, String string);
	
	FileVO toListVO(Usuario user, Arquivo arquivo);

	Arquivo duplicar(Arquivo arquivo, Long destaqueId) throws DomainException;

	ArquivoThumb find(Long id);

	List<Arquivo> findAllByUser(ArquivoFiltro arquivoFiltro);

	List<Arquivo> findAll(Empresa e);

	List<Arquivo> findAllByTag(Tag tag);

	List<Long> findIdsByComentarios(Comentario c);

	List<Long> findIdsByUser(Usuario u);

	void updateFileDestaquePost(Post post);

	void deleteNotInCategorias(List<Long> arquivosIds, CategoriaPost categoria);

	void setDestaque(Arquivo old, Long destaqueId, Arquivo ar);

	void setDestaque(Long destaqueId, Arquivo ar);

	List<Long> getIdsArquivosNotIn(Post p, List<Long> arquivoIds);

}
