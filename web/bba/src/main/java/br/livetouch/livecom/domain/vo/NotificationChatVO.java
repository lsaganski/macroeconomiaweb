package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.ConversaNotification;

public class NotificationChatVO implements Serializable {

	private static final long serialVersionUID = -2361258693360752472L;
	private Boolean sendPush;
	private Boolean sound;
	private Boolean notification;

	public NotificationChatVO(ConversaNotification c) {
		if(c != null) {
			this.sendPush = c.getSendPush() != null ? c.getSendPush() : true;
			this.sound = c.getSound() != null ? c.getSound() : true;
			this.notification = c.getNotification() != null ? c.getNotification() : true;
		} else {
			this.sendPush = true;
			this.sound = true;
			this.notification = true;
		}
	}

	public Boolean getSendPush() {
		return sendPush;
	}

	public void setSendPush(Boolean sendPush) {
		this.sendPush = sendPush;
	}

	public Boolean getSound() {
		return sound;
	}

	public void setSound(Boolean sound) {
		this.sound = sound;
	}

	public Boolean getNotification() {
		return notification;
	}

	public void setNotification(Boolean notification) {
		this.notification = notification;
	}

}
