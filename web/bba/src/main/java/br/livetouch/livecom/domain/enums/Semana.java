package br.livetouch.livecom.domain.enums;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;

public enum Semana {
	SEG(0,"segunda"),
	TER(1,"terca"),
	QUA(2,"quarta"),
	QUI(3,"quinta"),
	SEX(4,"sexta"),
	SAB(5,"sabado"),
	DOM(6, "domingo");
	
	private static final Locale LOCALE_PT_BR = new Locale("pt", "BR");
	private int ordinal;
	private String dia;
	
	Semana(int ordinal, String dia) {
		this.setOrdinal(ordinal);
		this.setDia(dia);
	}

	public int getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(int ordinal) {
		this.ordinal = ordinal;
	}
	
	public String getDia() {
		return dia;
	}
	
	public void setDia(String dia) {
		this.dia = dia;
	}

	public static Semana from(String s) {
		try {
			s = StringUtils.upperCase(s);
			s = StringUtils.replace(s, "SÁB", "SAB");
			Semana dia = Semana.valueOf(s);
			return dia;
		} catch (Exception e) {
			return Semana.SEG;
		}
	}

	public static Semana today() {
		LocalDate date = new LocalDate();
		String hoje = date.dayOfWeek().getAsShortText(LOCALE_PT_BR);
		return from(hoje);
	}
	
}
