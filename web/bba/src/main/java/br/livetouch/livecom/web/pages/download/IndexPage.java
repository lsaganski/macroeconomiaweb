package br.livetouch.livecom.web.pages.download;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.web.pages.admin.LivecomPage;

@Controller
@Scope("prototype")
public class IndexPage extends LivecomPage {
	
	public String buildApk;
	public String buildBeta;
	public String buildIOS;
	public String empresa;
	
	@Override
	public boolean onSecurityCheck() {
		boolean b = validateSecurity();
		return b;
	}
	
	@Override
	public String getTemplate() {
		return super.getPath();
	}
	
	@Override
	public void onRender() {
		super.onRender();
		ParametrosMap p = ParametrosMap.getInstance(getEmpresa());
		buildBeta = p.getByEmpresa(Params.BUILD_BETA_ANDROID_URL);
		buildApk = p.getByEmpresa(Params.BUILD_APP_ANDROID_URL);
		buildIOS = p.getByEmpresa(Params.BUILD_APP_IOS_URL);
		
		empresa = p.getByEmpresa(Params.WEB_TEMPLATE_TITLE);
	}
}
