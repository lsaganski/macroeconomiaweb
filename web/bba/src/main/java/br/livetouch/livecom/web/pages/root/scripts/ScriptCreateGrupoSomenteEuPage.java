package br.livetouch.livecom.web.pages.root.scripts;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.LongOperation.LongOperationRunner;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.pages.WaitPage;
import br.livetouch.livecom.web.pages.root.RootPage;
import net.sf.click.control.ActionLink;

@Controller
@Scope("prototype")
public class ScriptCreateGrupoSomenteEuPage extends RootPage {

	public ActionLink link = new ActionLink("link","FIX",this,"exec");

	public boolean exec() {
		executeLongOperation(WaitPage.COD_SOMENTE_EU, new LongOperationRunner() {
			@Override
			public void run(Usuario userInfo) throws Exception {
//				scriptsService.createGrupoSomenteEu(userInfo);
			}
		});

		return false;
	}
}
