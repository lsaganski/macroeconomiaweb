package br.livetouch.livecom.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.domain.enums.StatusLogImportacao;
import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LogImportacao extends net.livetouch.tiger.ddd.Entity {

	private static final long serialVersionUID = 8771734275567884350L;

	public static final long TEMPO_EXPIRAR_OTP = 60;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "LOGIMPORTACAO_SEQ")
	private Long id;

	private String arquivoNome;
	private String header;

	@Enumerated(EnumType.STRING)
	private StatusLogImportacao status;

	private Date dataInicio;
	private Date dataFim;

	private int linhasOk;
	private int linhasNok;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;

	public LogImportacao() {

	}

	public LogImportacao(JobInfo job) {
		this.dataInicio = job.getLastDate() != null ? job.getLastDate() : new Date();
		this.dataFim = new Date();
		this.arquivoNome = job.getArquivo().replaceAll("[^a-zA-Z.]", "");
		this.linhasOk = job.getResponse().countOk;
		this.linhasNok = job.getResponse().countError;
		this.header = job.getHeader();
		if (job.isSuccessful()) {
			this.status = StatusLogImportacao.SUCESSO;
		} else {
			this.status = StatusLogImportacao.ERRO;
		}
		this.empresa = job.getEmpresa();
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getArquivoNome() {
		return arquivoNome;
	}

	public void setArquivoNome(String arquivoNome) {
		this.arquivoNome = arquivoNome;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public int getLinhasOk() {
		return linhasOk;
	}

	public void setLinhasOk(int linhasOk) {
		this.linhasOk = linhasOk;
	}

	public int getLinhasNok() {
		return linhasNok;
	}

	public void setLinhasNok(int linhasNok) {
		this.linhasNok = linhasNok;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

}
