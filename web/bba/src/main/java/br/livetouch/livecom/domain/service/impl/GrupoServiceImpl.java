
package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.chat.webSocket.WebSocketChat;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoAuditoria;
import br.livetouch.livecom.domain.GrupoSubgrupos;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.enums.GrupoNotification;
import br.livetouch.livecom.domain.enums.StatusParticipacao;
import br.livetouch.livecom.domain.enums.TipoGrupo;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.repository.GrupoRepository;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.Filtro;
import br.livetouch.livecom.domain.vo.GrupoFilter;
import br.livetouch.livecom.domain.vo.GrupoUsuariosVO;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.UserToPushVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.push.PushNotificationVO;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.hibernate.HibernateUtil;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class GrupoServiceImpl extends LivecomService<Grupo> implements GrupoService {
	
	@Autowired
	protected NotificationService notificationService;

	@Autowired
	private GrupoRepository rep;

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private PostService postService;

	@Override
	public Grupo get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Usuario userInfo, Grupo grupo) throws DomainException {
		boolean insert = grupo.getId() == null;
		try {
			Validate.notNull(userInfo);
			Validate.notNull(userInfo.getEmpresa());
			Validate.notNull(grupo);
			Validate.notEmpty(grupo.getNome());
			if (insert) {
				boolean existe = rep.exists(grupo, userInfo.getEmpresa());
				if (existe) {
					throw new DomainMessageException("validate.grupo.ja.existe");
				}
			}
			
			if(userInfo != null) {
				grupo.setUsuario(userInfo);
			}

			if (grupo.getEmpresa() == null) {
				grupo.setEmpresa(userInfo.getEmpresa());
			}

			if (grupo.getDateCreate() == null) {
				grupo.setDateCreate(new Date());
				grupo.setUserCreate(userInfo);
			}
			grupo.setDateUpdate(new Date());
			if (userInfo != null) {
				grupo.setUserUpdate(userInfo);
			}
			rep.saveOrUpdate(grupo);
			
			auditSave(userInfo, grupo, insert);

		} catch (DomainMessageException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected void auditSave(Usuario userInfo, Grupo grupo, boolean insert) {
		String msg = null;
		if(insert) {
			msg = "Grupo " + grupo.getNome() + " cadastrado";
		} else {
			msg = "Grupo " + grupo.getNome() + " editado";
		}
		
		AuditoriaAcao acao = insert ? AuditoriaAcao.INSERIR : AuditoriaAcao.EDITAR;
		LogAuditoria.log(userInfo, grupo, AuditoriaEntidade.GRUPO, acao, msg);
	}

	/**
(1) Regras:

(r1) Não permite excluir o Grupo padrão.
(r2) Não permite excluir o grupo caso tenha posts associados.
(r3) O grupo somente pode ser excluído pelo seu admin.
(r4) O grupo somente pode ser excluído pelo seu admin ou por qualquer admin do sistema.

(2) Cascade ao excluir:

(c1) Ao excluir o grupo remove os Grupos Adicionais dos Usuários e Posts.
(c2) Ao excluir o Grupo Origem de algum usuário, atualiza o Grupo Origem com o Grupo Padrão.
(c3) Ao excluir o grupo, move os Posts para o grupo Padrão.
(c4) Ao excluir o grupo, deleta os Posts que possuem apenas este como grupo.
	 */
	@Override
	public void delete(Usuario userInfo, Grupo g, boolean force) throws DomainException {
		if (g == null) {
			throw new DomainException("Grupo nulo para deletar");
		}

		Grupo grupoPadrao = findDefaultByEmpresa(g.getEmpresa().getId());
		
		boolean r1 = true;
		boolean r2 = ParametrosMap.getInstance(g.getEmpresa()).getBoolean(Params.DELETAR_GRUPO_R2_ON,true);
		boolean r3 = ParametrosMap.getInstance(g.getEmpresa()).getBoolean(Params.DELETAR_GRUPO_R3_ON,true);
		boolean r4 = ParametrosMap.getInstance(g.getEmpresa()).getBoolean(Params.DELETAR_GRUPO_R4_ON,false);
		boolean r5 = ParametrosMap.getInstance(g.getEmpresa()).getBoolean(Params.DELETAR_GRUPO_R5_ON,true);
		
		boolean c1 = ParametrosMap.getInstance(g.getEmpresa()).getBoolean(Params.DELETAR_GRUPO_C1_ON,false);
		boolean c2 = ParametrosMap.getInstance(g.getEmpresa()).getBoolean(Params.DELETAR_GRUPO_C2_ON,false);
		boolean c3 = ParametrosMap.getInstance(g.getEmpresa()).getBoolean(Params.DELETAR_GRUPO_C3_ON,false);
		boolean c4 = ParametrosMap.getInstance(g.getEmpresa()).getBoolean(Params.DELETAR_GRUPO_C4_ON,false);
		boolean c5 = ParametrosMap.getInstance(g.getEmpresa()).getBoolean(Params.DELETAR_GRUPO_C5_ON,false);

		if(!force) {
			// (1) Aplica regras
			if(userInfo != null) {
				
				if(r1) {
					if(g.isPadrao()) {
						throw new DomainException("O Grupo padrão não pode ser excluído.");
					}
				}

				if(r2) {
					List<Post> posts = postService.findPostsByGrupo(g,false);
					if(posts != null && posts.size() > 0) {
						throw new DomainException("Este grupo possui comunicados e não pode ser excluído.");
					}
				}
				
				if(r3 && !r4) {
					/**
					 * O R4 é o R3 e R4 juntos pois faz "OU"
					 */
					boolean owner = userInfo.isAdmin(g);
					if (!owner) {
						throw new DomainException("Somente o dono do Grupo pode excluí-lo.");
					}
				}

				if(r4) {
					boolean owner = userInfo.isAdmin(g);
					if (!owner) {
						throw new DomainException("Permissão negada. Somente o dono do Grupo pode excluí-lo.");
					}
				}

				if(r5) {
					List<Notification> list = notificationService.findByGrupo(g);
					if(!list.isEmpty()) {
						throw new DomainException("Existem notificações deste grupo e não é possivel excluí-lo.");
					}
				}
			}
		}
		
		LogAuditoria auditoria = null;
		
		try {
			
			// Auditoria
			if(userInfo != null) {
				String msg = "Grupo " + g.getNome() + " deletado";
				auditoria = LogAuditoria.log(userInfo, g, AuditoriaEntidade.GRUPO, AuditoriaAcao.DELETAR, msg, false);
			}
				
			if(!force) {
				
				if(c3 || c4) {
					// (c3) Ao excluir o grupo, move os Posts para o grupo Padrão.
					// move posts deste grupo para outro grupo
					List<Post> posts = postService.findPostsByGrupo(g,false);
					for (Post post : posts) {
						
						if(c4) {
							boolean qtde = post.getGrupos().size() == 1;
							if(qtde) {
								// Apaga o post para ele não ficar orfao
								postService.delete(userInfo, post,force);
							}
						} else {
							// (c3)
							// Dessassocia este grupo
							post.getGrupos().remove(g);
							// Associa com grupo padrao
							post.getGrupos().add(grupoPadrao);
							// Salva post
							postService.saveOrUpdate(userInfo, post);
						}
					}
				}
			}

			if(!c1) {
				// Se nao for para dar cascade, mas o admin está tentando excluir o grupo, e só tem ele.
				Usuario adminGrupo = g.getAdmin();
				List<GrupoUsuarios> usuarios = g.getUsuariosList();
				if(usuarios.size() == 1) {
					GrupoUsuarios gu = usuarios.get(0);
					if(adminGrupo.equals(gu.getUsuario())){
						// Apaga relacionamentos..
						c1 = true;
					}
				}
			}
			
			// Aplica C1 e C2 por ultimo, para fazer as outras regras de cascade.
			Long grupoId = g.getId();
			if(c1) {

				executeNative("delete from evento_grupos where grupos = ?", grupoId);
				
				// (c1) Ao excluir o grupo remove os Grupos Adicionais dos Usuários e Posts.
				execute("delete from GrupoUsuarios where grupo.id = ?", grupoId);

				// Relacionamento mensagem.
				execute("delete from Location l where l.id in (select m.location.id from Mensagem m)");
				executeNative("delete from status_mensagem_usuario where conversa_id in (select id from mensagem_conversa where grupo_id = ?)", grupoId);
//				executeNative("delete from mensagem where conversa_id in (select id from mensagem_conversa where grupo_id = ?)", g.getId());
				execute("delete from MensagemConversa where grupo.id = ?", grupoId);
				
				// Relacionamento com Posts
				executeNative("delete from post_grupos where grupo_id = ?", grupoId);
			}
			
			if(c2) {
				// troca grupo origem
				if(grupoPadrao != null) {
					executeReassociate("Usuario","grupo", grupoPadrao.getId(), grupoId);
				}
			}
			
			if(c5) {
				// delete notificações
				List<Long> ids = new ArrayList<Long>();
				ids.add(grupoId);
				List<Long> idsNotifications = queryIdsIn("select n.id from Notification n where n.grupo.id in (:ids)",false,"ids", ids);
				executeIn("delete from NotificationUsuario notUser where notUser.notification.id in(:ids)", "ids", idsNotifications);
				execute("delete from Notification n where n.grupo.id = ?", grupoId);
			}
			
			
			execute("delete from GrupoSubgrupos gs where gs.grupo.id = ?", grupoId);
			
			// delete
			rep.delete(g);
			
			// OK
			if(auditoria != null) {
				auditoria.finish();
			}
			
		} catch (Exception e) {
			throw new DomainException("Não foi possível excluir o grupo [" + g + "]", e);
		}

		HibernateUtil.clearCache();
	}

	protected void auditDelete(Usuario userInfo, Grupo c) {
		StringBuffer sb = new StringBuffer("Grupo Deletado: " + c.toStringAll());
		// audit
		String msgAudit = StringUtils.abbreviate(sb.toString(), 1000);
		GrupoAuditoria a = GrupoAuditoria.audit(c, userInfo, msgAudit, 2);
		rep.saveOrUpdate(a);
	}

	@Override
	public List<Grupo> findAll(Empresa empresa) {
		return rep.findAll(empresa, null, null);
	}
	
	@Override
	public List<Grupo> findAll(Empresa empresa, Integer page, Integer maxRows) {
		return rep.findAll(empresa, page, maxRows);
	}

	@Override
	public List<GrupoVO> findAll(GrupoFilter filter, Usuario u) {
		return rep.findAll(filter, u);
	}

	@Override
	public List<Grupo> findAllDefault(Usuario userInfo) {
		return rep.findAllDefault(userInfo);
	}

	@Override
	public Grupo findByNome(String nome, Usuario userInfo) {
		return rep.findByNome(nome, userInfo);
	}

	@Override
	public Grupo findByNomeAndUser(String nome, Usuario user) {
		return rep.findByNomeAndUser(nome, user);
	}

	@Override
	public List<Grupo> findAllByNomeLike(String nome, Usuario userInfo) {
		return rep.findAllByNomeLike(nome, userInfo);
	}

	@Override
	public Grupo findByCodigo(Grupo grupo, Usuario userInfo) {
		return rep.findByCodigo(grupo, userInfo);
	}

	@Override
	public long getCountByFilter(Grupo filtro, Usuario userInfo) {
		return rep.getCountByFilter(filtro, userInfo);
	}

	@Override
	public List<GrupoUsuariosVO> findbyFilter(Grupo filtro, Usuario userInfo, int page, int pageSize) throws DomainException {
		return rep.findbyFilter(filtro, userInfo, page, pageSize);
	}

	@Override
	public List<Usuario> findUsuarios(Grupo grupo) {
		return rep.findUsuarios(grupo);
	}

	@Override
	public List<UserToPushVO> findUsers(Grupo grupo, Post p, TipoNotificacao tipoNotificacao) {
		return rep.findUsers(grupo, p, tipoNotificacao);
	}

	@Override
	public List<Object[]> findUsuariosArrayIdNome(Grupo grupo, Usuario userInfo) {
		return rep.findUsuariosArrayIdNome(grupo, userInfo);
	}

	@Override
	public void removeUsuario(Usuario userInfo, Grupo grupo, Usuario usuario) {
		rep.removeUsuario(userInfo, grupo, usuario);
	}

	@Override
	public List<GrupoUsuarios> findMeusGrupos(GrupoFilter filter, Usuario user) {
		return rep.findMeusGrupos(filter, user);
	}

	@Override
	public List<GrupoVO> findGrupos(GrupoFilter filter, Usuario user) {
		return rep.findGrupos(filter, user);
	}

	@Override
	public List<Grupo> findGruposParticipar(GrupoFilter filter, Usuario user, List<Grupo> grupos) {
		return rep.findGruposParticipar(filter, user, grupos);
	}

	@Override
	public List<Grupo> findGruposVirtuais(GrupoFilter filter, Usuario user, List<Grupo> grupos) {
		return rep.findGruposVirtuais(filter, user, grupos);
	}

	@Override
	public List<Usuario> findSubadminsByGrupo(Grupo grupo) {
		return rep.findSubadminsByGrupo(grupo);
	}

	@Override
	public Long countPostsSomenteEu(Usuario user) {
		return rep.countPostsSomenteEu(user);
	}

	@Override
	public List<Grupo> findAllByIds(List<Long> ids) {
		if (ids == null || ids.isEmpty()) {
			return new ArrayList<Grupo>();
		}
		return rep.findAllByKeys(ids);
	}

	@Override
	public List<Grupo> findAllByCodigos(List<String> codigos) {
		if (codigos == null || codigos.isEmpty()) {
			return new ArrayList<Grupo>();
		}
		return rep.findAllByCodigos(codigos);
	}

	@Override
	public List<GrupoUsuariosVO> findUsuariosNaoAtivos(Usuario userInfo) {
		return rep.findGrupoUsuariosNaoAtivos(userInfo);
	}

	@Override
	public List<GrupoUsuariosVO> findUsuariosByStatusAtivacao(String status, Usuario userInfo) {
		return rep.findGrupoUsuariosByStatusAtivacao(status, userInfo);
	}

	@Override
	public List<UsuarioVO> findUsuariosByStatusParticipacao(StatusParticipacao status, Grupo grupo) {
		List<GrupoUsuarios> grupoUsuarios = rep.findUsuariosByStatusParticipacao(status, grupo);
		
		List<UsuarioVO> users = new ArrayList<UsuarioVO>();
		for (GrupoUsuarios gu : grupoUsuarios) {
			UsuarioVO vo = new UsuarioVO();
			vo.setUsuario(gu.getUsuario());
			users.add(vo);
		}
		
		return users;
	}

	@Override
	public long countUsers(Grupo grupo) {
		return rep.countUsers(grupo);
	}

	@Override
	public long countSubgrupos(Grupo grupo) {
		return rep.countSubgrupos(grupo);
	}

	@Override
	public List<Grupo> getGrupos(Usuario u) {
		return rep.getGrupos(u);
	}
	
	@Override
	public List<Grupo> getGrupos(GrupoFilter filter) {
		return rep.getGrupos(filter);
	}

	@Override
	public List<GrupoUsuariosVO> findGrupoUsuariosByUser(Usuario user) {
		return rep.findGrupoUsuariosByUser(user);
	}

	@Override
	public List<GrupoUsuarios> findGrupoUsuariosByUsuario(Usuario user) {
		return rep.findGrupoUsuariosByUsuario(user);
	}

	@Override
	public List<Grupo> findAllWithoutUsers(GrupoFilter filter, Usuario userInfo) {
		return rep.findAllWithoutUsers(filter, userInfo);
	}

	@Override
	public void associarGrupoUsuario(Grupo grupo, boolean postar, Usuario... users) throws DomainException {
		if (users != null) {
			for (Usuario usuario : users) {

				GrupoUsuarios gu = usuarioService.getGrupoUsuario(usuario, grupo);

				if (gu == null) {

					gu = new GrupoUsuarios();
					gu.setUsuario(usuario);
					gu.setGrupo(grupo);
				}

				gu.setPostar(postar);

				usuarioService.saveOrUpdate(gu);
			}
		}
	}

	@Override
	public List<Object[]> findIdCodGrupos(Long idEmpresa) {
		return rep.findIdCodGrupos(idEmpresa);
	}

	@Override
	public Grupo findDefaultByEmpresa(Long empresaId) {
		return rep.findDefaultByEmpresa(empresaId);
	}

	@Override
	public List<Grupo> findAllByUser(Usuario u) {
		return rep.findAllByUser(u);
	}

	@Override
	public String csvGrupos(Empresa empresa) {
		List<Grupo> findAll = findAll(empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("CODIGO").add("NOME");
		for (Grupo g : findAll) {
			body.add(g.getCodigoDesc()).add(g.getNome() != null ? g.getNome() : "");
			body.end();
		}
		return generator.toString();
	}

	@Override
	public List<Long> findUsuariosToPush(Grupo g, Post p, Idioma idioma) {
		return rep.findUsuariosToPush(g, p, idioma);
	}

	@Override
	public List<Long> findUsuariosReminder(Grupo g, Post p) {
		return rep.findUsuariosReminder(g, p);
	}

	@Override
	public List<Object[]> findIdNomeGrupos(Long empresaId) {
		return rep.findIdNomeGrupos(empresaId);
	}

	@Override
	public boolean isGruposDentroHorario(Usuario u) {
		return rep.isGruposDentroHorario(u);
	}

	@Override
	public Grupo findByName(Grupo g, Empresa empresa) {
		return rep.findByName(g, empresa);
	}

	@Override
	public List<Long> findIdUsuarios(Grupo g) {
		return rep.findIdUsuarios(g);
	}

	@Override
	public List<Grupo> findGruposByPost(Post post) {
		return rep.findGruposByPost(post);
	}

	@Override
	public List<GrupoVO> toListVO(List<Grupo> grupos) {
		List<GrupoVO> list = new ArrayList<GrupoVO>();
		if(grupos != null) {
			for (Grupo g : grupos) {
				if(g != null) {
					GrupoVO gvo = setGrupo(g);
					list.add(gvo);
				}
			}
		}
		return list;
	}

	@Override
	public GrupoVO setGrupo(Grupo g) {
		GrupoVO gvo = new GrupoVO();
		if(g != null) {
			gvo.setGrupo(g);
			gvo.countPosts = getCountPosts(g); 
			gvo.havePost = gvo.countPosts > 0;
		}
		return gvo;
	}

	@Override
	public Long getCountPosts(Grupo g) {
		return rep.getCountPostsByGrupo(g);
	}

	@Override
	public List<Long> gruposUsuariosDentroHorario(List<Long> users) {
		return rep.gruposUsuariosDentroHorario(users);
	}

	@Override
	public GrupoUsuarios participar(Usuario usuario, Grupo grupo, boolean participar) throws DomainException {

		try {
			TipoGrupo tipo = grupo.getTipoGrupo();
			GrupoUsuarios grupoUsuario = usuarioService.getGrupoUsuario(usuario, grupo);
			if(grupoUsuario != null && !participar) {
				return sair(usuario, grupo, tipo, grupoUsuario);
			}

			// participar
			if(participar) {
				grupoUsuario = new GrupoUsuarios();
				grupoUsuario.setGrupo(grupo);
				grupoUsuario.setUsuario(usuario);
				grupoUsuario.setPostar(true);
				grupoUsuario.setFavorito(false);
				grupoUsuario.setMostraMural(true);
				grupoUsuario.setPush(true);
				
				grupoUsuario = solicitacao(grupoUsuario);
				
				if(tipo.equals(TipoGrupo.ABERTO)) {
					criarNotificacao(grupoUsuario, TipoNotificacao.GRUPO_PARTICIPAR);
				} else if(tipo.equals(TipoGrupo.ABERTO_FECHADO)) {
					criarNotificacao(grupoUsuario, TipoNotificacao.GRUPO_SOLICITAR_PARTICIPAR);
				}
				
				usuarioService.saveOrUpdate(grupoUsuario);
			}
			
			return grupoUsuario;
		} catch (Exception e) {
			throw new DomainException("Erro ao participar do grupo: ", e);
		}
	}

	private GrupoUsuarios sair(Usuario usuario, Grupo grupo, TipoGrupo tipo, GrupoUsuarios grupoUsuario) throws DomainException {
		
//		Usuario admin = grupo.getAdmin();
		
		// deixar de participar
		if(tipo.equals(TipoGrupo.ABERTO_FECHADO)) {
			notificationService.markSolicitacaoAsRead(grupo, usuario);
			notificationService.markSolicitacaoAsRead(grupo, grupo.getAdmin());
		}
		usuarioService.deleteGrupoUsuarios(grupo, usuario);
		criarNotificacao(grupoUsuario, TipoNotificacao.GRUPO_SAIR);
		
//		movePostsToAdminGrupo(usuario, admin, grupo);
		
		return null;
	}
	
	@Override
	public void movePostsToAdminGrupo(Usuario usuario, Usuario admin, Grupo grupo) {
		List<Post> posts = postService.findPostsByGrupo(grupo, false);
		rep.movePostsToAdminGrupo(usuario, admin, grupo, posts);
	}

	@Override
	public void criarNotificacao(GrupoUsuarios grupoUsuario, TipoNotificacao tipo) throws DomainException {
		Grupo grupo = grupoUsuario.getGrupo();
		Usuario admin = grupo.getAdmin();
		Usuario user = grupoUsuario.getUsuario();
		
		Notification n = new Notification();
		n.setData(new Date());
		n.setGrupo(grupo);
		n.setSendPush(false);
		n.setPublicado(true);
		n.setVisivel(true);
		
		Usuario from = null;
		Usuario to = null;
		String titulo = null;
		String texto = null;
		String textoNot = null;
		TipoNotificacao tipoNotificacao = null;
		switch (tipo) {
			case GRUPO_PARTICIPAR: {
				tipoNotificacao = TipoNotificacao.GRUPO_PARTICIPAR;
				titulo = "Novo participante no grupo.";
				texto = "Usuário " + user.getNome() + " entrou no grupo " + grupo.getNome() + ".";
				textoNot = "Usuário " + user.getNome() + " entrou no grupo " + grupo.getNome() + ".";
				from = user;
				to = admin;
				break;
			}
			case GRUPO_SOLICITAR_PARTICIPAR: {
				tipoNotificacao = TipoNotificacao.GRUPO_SOLICITAR_PARTICIPAR;
				titulo = "Novo participante no grupo.";
				texto = "Usuário " + user.getNome() + " solicitou entrar no grupo " + grupo.getNome() + ".";
				textoNot = "Usuário " + user.getNome() + " solicitou entrar no grupo " + grupo.getNome() + ".";
				from = user;
				to = admin;
				break;
			}
			case GRUPO_SAIR: {
				tipoNotificacao = TipoNotificacao.GRUPO_SAIR;
				titulo = "Usuário saiu do grupo.";
				texto = "Usuário " + user.getNome() + " saiu do grupo " + grupo.getNome() + ".";
				textoNot = "Usuário " + user.getNome() + " saiu do grupo " + grupo.getNome() + ".";
				from = user;
				to = admin;
				break;
			}
			case GRUPO_USUARIO_ADICIONADO: {
				tipoNotificacao = TipoNotificacao.GRUPO_USUARIO_ADICIONADO;
				titulo = "Adicionado ao grupo.";
				texto = "Você foi adicionado ao grupo " + grupo.getNome() + " pelo administrador.";
				textoNot = "Você foi adicionado ao grupo " + grupo.getNome() + " pelo administrador.";
				from = admin;
				to = user;
				break;
			}
			case GRUPO_USUARIO_REMOVIDO: {
				tipoNotificacao = TipoNotificacao.GRUPO_USUARIO_REMOVIDO;
				titulo = "Removido do grupo.";
				texto = "Você foi removido do grupo " + grupo.getNome() + " pelo administrador.";
				textoNot = "Você foi removido do grupo " + grupo.getNome() + " pelo administrador.";
				from = admin;
				to = user;
				break;
			}
			case GRUPO_USUARIO_ACEITO: {
				tipoNotificacao = TipoNotificacao.GRUPO_USUARIO_ACEITO;
				titulo = "Solicitação de participação aceita.";
				texto = "O Administrador do grupo " + grupo.getNome() + " aprovou sua participação.";
				textoNot = "O Administrador do grupo " + grupo.getNome() + " aprovou sua participação.";
				from = admin;
				to = user;
				break;
			}
			case GRUPO_USUARIO_RECUSADO: {
				tipoNotificacao = TipoNotificacao.GRUPO_USUARIO_RECUSADO;
				titulo = "Solicitação de participação recusada.";
				texto = "O Administrador do grupo " + grupo.getNome() + " recusou sua participação.";
				textoNot = "O Administrador do grupo " + grupo.getNome() + " recusou sua participação.";
				from = admin;
				to = user;
				break;
			}
			default:
				break;
		}
		n.setUsuario(from);
		n.setTipo(tipoNotificacao);
		n.setTitulo(titulo);
		n.setTexto(texto);
		n.setTextoNotificacao(textoNot);
		n.setGrupoNotification(GrupoNotification.GRUPO);
		notificationService.save(n);
		
		// salva a notification para ser enviada pelo job
		NotificationUsuario notUsuario = new NotificationUsuario();
		notUsuario.setUsuario(to);
		notUsuario.setVisivel(true);
		notUsuario.setNotification(n);
		notificationService.saveOrUpdate(notUsuario);
		
		// envia para o socket da web para atualizar as badges
		PushNotificationVO not = PushNotificationVO.createNotificacaoParticipacaoGrupo(n, to);
		WebSocketChat.sendMsg(to.getId(), not);
	}

	private GrupoUsuarios solicitacao(GrupoUsuarios grupoUsuario) {

		Grupo grupo = grupoUsuario.getGrupo();
		
		TipoGrupo tipo = grupo.getTipoGrupo();
		if(tipo != null) {
			switch(tipo) {
				case ABERTO:
					grupoUsuario.setStatusParticipacao(StatusParticipacao.APROVADA);
					return grupoUsuario;
				case ABERTO_FECHADO:
					grupoUsuario.setStatusParticipacao(StatusParticipacao.SOLICITADA);
					return grupoUsuario;
				default: 
					grupoUsuario.setStatusParticipacao(StatusParticipacao.RECUSADA);
					return grupoUsuario;
			}
		}

		grupoUsuario.setStatusParticipacao(StatusParticipacao.RECUSADA);
		return grupoUsuario;		
	}

	@Override
	public boolean isAdminOrSubadminGrupo(Usuario usuario, Grupo grupo) {
//		Long usuarioId = usuario.getId();
//		Long adminId = grupo.getAdmin().getId();
		List<GrupoUsuarios> admins = rep.isAdmin(usuario, grupo);
		return !admins.isEmpty();
	}

	@Override
	public boolean isAdmin(Usuario usuario, Grupo grupo) {
		Long usuarioId = usuario.getId();
		Long adminId = grupo.getAdmin().getId();
		return usuarioId.equals(adminId);
	}

	@Override
	public boolean isUsuarioDentroDoGrupo(Grupo grupo, Usuario user) {
		return usuarioService.isUsuarioDentroDoGrupo(user, grupo);
	}
	
	@Override
	public GrupoUsuarios findByGrupoEUsuario(Grupo g, Usuario u) {
		return rep.findByGrupoEUsuario(g, u);
	}

	@Override
	public boolean existe(Grupo g, Empresa empresa, Usuario userInfo) {
		
		boolean codigo = false;
		if(StringUtils.isNotEmpty(g.getCodigo())) {
			codigo = findByCodigo(g, userInfo) != null;
		}
		
		return findByName(g, empresa) != null || codigo;
	}

	@Override
	public GrupoSubgrupos getGrupoSubgrupo(Grupo grupo, Grupo subgrupo) {
		return rep.getGrupoSubgrupo(grupo, subgrupo);
	}

	@Override
	public void saveSubgruposToGrupo(Usuario userInfo, Grupo grupo, List<Grupo> subgrupos) throws DomainException {
		if (subgrupos != null) {
			for (Grupo subgrupo : subgrupos) {
				GrupoSubgrupos gs = new GrupoSubgrupos();
				gs.setGrupo(grupo);
				gs.setSubgrupo(subgrupo);
				rep.saveOrUpdate(gs);
				String msg = "Subgrupo ["+subgrupo.getNome()+"] adicionado ao grupo ["+grupo.getNome()+"]";
				LogAuditoria.log(userInfo, grupo, AuditoriaEntidade.GRUPO, AuditoriaAcao.INSERIR, msg);
			}
		}
		
	}
	
	@Override
	public void removeSubgruposToGrupo(Usuario userInfo, Grupo grupo, List<Grupo> subgrupos) throws DomainException {
		for (Grupo subgrupo : subgrupos) {
			
			removeSubgrupo(userInfo, grupo, subgrupo);
			String msg = "Subgrupo ["+subgrupo.getNome()+"] removido do grupo ["+grupo.getNome()+"] com sucesso";
			LogAuditoria.log(userInfo, grupo, AuditoriaEntidade.GRUPO, AuditoriaAcao.DELETAR, msg);
		}
	}
	
	@Override
	public void saveOrUpdate(GrupoSubgrupos gs) {
		rep.saveOrUpdate(gs);
	}

	@Override
	public void removeSubgrupo(Usuario userInfo, Grupo grupo, Grupo subgrupo) {
		rep.removeSubgrupo(userInfo, grupo, subgrupo);
	}

	@Override
	public List<Grupo> findSubgruposByGrupo(Grupo grupo) {
		return rep.findSubgruposByGrupo(grupo);
	}

	@Override
	public List<Grupo> findAllSubgrupos(Filtro filtro, Empresa empresa) throws Exception {
		return rep.findAllSubgrupos(filtro, empresa);
	}

	@Override
	public List<Grupo> findAllSubgruposWithAcesso(Filtro filtro, Usuario userInfo) throws Exception {
		return rep.findAllSubgruposWithAcesso(filtro, userInfo);
	}

	@Override
	public List<Grupo> findSubgruposInGrupos(Set<Grupo> grupos) {
		return rep.findSubgruposInGrupos(grupos);
	}
	
	@Override
	public void changePermissaoPostarUsers(Grupo grupo, boolean postar) {
		rep.changePermissaoPostarUsers(grupo, postar);
	}

	@Override
	public boolean hasAberto(Set<Grupo> grupos) {
		for (Grupo grupo : grupos) {
			TipoGrupo tipo = grupo.getTipoGrupo();
			if(tipo != null && (tipo.equals(TipoGrupo.ABERTO) || tipo.equals(TipoGrupo.ABERTO_FECHADO))) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Long countAll(GrupoFilter filter, Usuario userInfo) {
		return rep.countAll(filter, userInfo);
	}

	@Override
	public Long countFindGrupos(GrupoFilter filter, Usuario userInfo) {
		return rep.countFindGrupos(filter, userInfo);
	}
	
}
