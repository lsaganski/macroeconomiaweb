package br.livetouch.livecom.domain.vo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

public class LinhaArquivoVO {
	protected static Logger log = Logger.getLogger(LinhaArquivoVO.class);

	public String string;
	public int row;

	public List<? extends LinhaArquivoVO> read(InputStream in) throws IOException {

		List<LinhaArquivoVO> list = new ArrayList<LinhaArquivoVO>();

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(in, "ISO-8859-1"));
			String line = null;

			int row = 0;
			while ((line = reader.readLine()) != null) {
				row++;
				String[] split = StringUtils.split(line, ";");
				if (split == null || split.length == 0) {
					log.debug("Linha [" + row + "] inválida");
					break;
				}

				LinhaArquivoVO vo = null;
				try {

					vo = parser(line, split, row);

					if (vo != null) {
						list.add(vo);
					}

				} catch (ArrayIndexOutOfBoundsException e) {
					throw new RuntimeException("Arquivo inválido, erro ao ler linha [" + row + "]. Verifique se a quantidade de colunas está ok. Erro na coluna: [" + e.getMessage() + "]");
				} catch (Exception e) {
					throw new RuntimeException("Arquivo inválido, erro ao ler linha [" + row + "]: " + e.getMessage());
				}
			}

			log.debug("Fim da leitura do arquivo");

		} catch (Throwable e) {
			e.printStackTrace();
			log.error("Erro linha [" + row + "]: " + e.getMessage(), e);
			throw new IOException("Erro de memória ao ler o arquivo: " + e.getClass() + " - " + e.getMessage());
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}

			IOUtils.closeQuietly(in);
		}

		return list;
	}

	protected LinhaArquivoVO parser(String line, String[] split, int row) {
		return null;
	}

	protected String get(String[] split, int col) {
		if (col == 0) {
			throw new RuntimeException("A coluna do split começa em zero.");
		}
		return StringUtils.trim(split[col - 1]);
	}

	protected String getInt(String[] split, int col) {
		return String.valueOf(NumberUtils.toInt(get(split, col), 0));
	}

	protected String getInt(String[] split, int col, int length) {
		String sInt = String.valueOf(NumberUtils.toInt(get(split, col), 0));
		return StringUtils.leftPad(sInt, length, "0");
	}

	protected float getFloat(String[] split, int col) {
		String replace = StringUtils.replace(get(split, col), ".", "");
		replace = StringUtils.replace(replace, ",", ".");
		float f = NumberUtils.toFloat(replace, 0);
		return f;
	};

	protected String getFloatString(String[] split, int i) {
		float f = getFloat(split, i);
		return String.valueOf(f);
	};

	@Override
	public String toString() {
		return "LinhaApoVO [row=" + row + ", string=" + string + "]";
	}
}
