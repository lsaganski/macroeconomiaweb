package br.livetouch.livecom.web.pages.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;

import br.infra.web.click.ComboCategoriaPost;
import br.infra.web.click.ComboVisibilidade;
import br.infra.web.click.GrupoCheckList;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Chapeu;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostIdioma;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.CategoriaVO;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import net.livetouch.click.control.wysiwyg.RichTextArea;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class PostComunicadoPage extends LivecomLogadoPage {

	public Form form = new Form();

	public String msg;

	public Long id;

	public Post post;

	public List<Tag> tags;
	public List<CategoriaVO> categorias;

	public boolean formAberto = true;
	public boolean isShowThumbCrop = true;

	public List<Grupo> gruposDefault;
	public List<CategoriaPost> categoriasDefault;

	public String dataPublicacao;

	public Chapeu chapeu;
	
	public ComboVisibilidade comboVisibilidade;

	// @Override
	public void onInit() {
		super.onInit();
		
		web_layout_sideBox = "1";

		TextField tTitulo = new TextField("titulo", getMessage("titulo.label"));
		tTitulo.setFocus(true);
		form.add(tTitulo);
		form.add(new TextField("tags", getMessage("tags.label")));

		RichTextArea messageTextArea = new RichTextArea("mensagem", 300, 100);
		// messageTextArea.setRequired(true);
		messageTextArea.setLabel(getMessage("mensagem.label") + ":");
		messageTextArea.setCols(45);
		messageTextArea.setRows(8);
		form.add(messageTextArea);

		form.add(new TextField("urlSite", getMessage("url.site.label")));
		form.add(new TextField("urlImagem", getMessage("url.imagem.label")));
		form.add(new TextField("urlVideo", getMessage("url.video.label")));

		// Destaque
		form.add(new TextField("destaqueDescricaoUrl", getMessage("url.destaque.descricao.label")));
		form.add(new TextField("destaqueTituloUrl", getMessage("url.destaque.titulo.label")));
		form.add(new IntegerField("destaqueTipo", getMessage("destaque.tipo.label")));

		form.add(new ComboCategoriaPost(categoriaPostService, getEmpresa()));
		form.add(new GrupoCheckList(grupoService, getEmpresa()));
		
		form.add(comboVisibilidade = new ComboVisibilidade());

		form.add(new Submit("publicar", getMessage("publicar.label"), this, "publicar"));

		// setFormTextWidth(form);
	}

	@Override
	public void onGet() {
		super.onGet();

		if (id != null) {
			post = postService.get(id);
			Set<PostIdioma> idiomas = post.getIdiomas();
			if(!CollectionUtils.isEmpty(idiomas)) {
				PostIdioma idioma = idiomas.iterator().next();
				categorias = CategoriaVO.createTree(new ArrayList<>(post.getCategorias()), idioma.getIdioma());
			} else {
				categorias = CategoriaVO.createTree(new ArrayList<>(post.getCategorias()), null);
			}
		}

		if (post != null) {
			form.copyFrom(post);

			tags = tagService.getTagsAndSave(post.getTags(), getEmpresa());

			chapeu = post.getChapeu();
			
		}
	}

	@Override
	public void onRender() {
		super.onRender();

		Usuario user = getUsuario();
		Usuario usuario = usuarioService.get(user.getId());

		gruposDefault = grupoService.findAllDefault(usuario);
		categoriasDefault = categoriaPostService.findAllDefault(getEmpresa());

	}
	
	@Override
	public boolean onSecurityCheck() {
		if(super.onSecurityCheck()) {
			if (id != null) {
				post = postService.get(id);
			}
			
			if(post != null) {
				if(post.getUsuario().getId() != getUsuario().getId() && !hasPermissao(ParamsPermissao.ACESSO_CADASTROS) && !hasPermissao(ParamsPermissao.EDITAR_PUBLICACAO)) {
					setFlashAttribute("ERRO", "Você não tem permissão para editar este post!");
					setRedirect(MuralPage.class);
				}
			}
			return true;
		}
		return false;
	}
}
