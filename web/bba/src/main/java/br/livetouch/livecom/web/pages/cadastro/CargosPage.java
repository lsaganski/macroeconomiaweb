package br.livetouch.livecom.web.pages.cadastro;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Funcao;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LinkDecorator;

/**
 * 
 */
@Controller
@Scope("prototype")
public class CargosPage extends CadastrosPage {
	
	public boolean bootstrap_on = true;
	
	public boolean escondeChat = true;
	public PaginacaoTable table = new PaginacaoTable();
	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public Funcao funcao;

	public List<Funcao> funcaos;
	
	@Override
	public void onInit() {
		super.onInit();

		form();
		
		table();
	}
	
	private void table() {
		Column c = new Column("id",getMessage("codigo.label"));
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("nome", getMessage("nome.label"));
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes",getMessage("detalhes.label"));
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);
		
		table.addStyleClass("table");

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	public void form(){
		form.add(new IdField());

		TextField tNome = new TextField("nome", getMessage("nome.label"));
		tNome.setAttribute("autocomplete", "off");
		tNome.setAttribute("rel", "Adicionar função");
		tNome.setAttribute("class", "input mascara");
		form.add(tNome);
		
		TextField tCodigo = new TextField("codigo", getMessage("codigo.label"));
		tCodigo.setAttribute("class", "input");
		form.add(tCodigo);
		
		if (id != null) {
			funcao = funcaoService.get(id);
		} else {
			funcao = new Funcao();
		}

		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip botao export");
		form.add(tExportar);
		
		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao salvar");
		form.add(tsalvar);
		
		Submit cancelar = new Submit("novo", getMessage("novo.label"), this,"novo");
		cancelar.setAttribute("class", "botao salvar");
		form.add(cancelar);

		form.copyFrom(funcao);
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				funcao = id != null ? funcaoService.get(id) : new Funcao();
				form.copyTo(funcao);

				funcaoService.saveOrUpdate(funcao,getUsuario());

				setMessageSesssion(getMessage("msg.funcao.salvar.sucess", funcao.getNome()), getMessage("msg.funcao.salvar.label.sucess"));
				
				setRedirect(CargosPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(CargosPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		funcao = funcaoService.get(id);
		form.copyFrom(funcao);
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Funcao c = funcaoService.get(id);
			if(id.equals(1L) && "livecom".equalsIgnoreCase(c.getNome())) {
				throw new DomainException(getMessage("msg.funcao.livecom.excluir.error"));
			}
			funcaoService.delete(getUsuario(), c);
			setRedirect(CargosPage.class);
			setMessageSesssion(getMessage("msg.funcao.excluir.sucess"), getMessage("msg.funcao.excluir.label.sucess"));
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.funcao.excluir.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		funcaos = funcaoService.findAll(getUsuario());

		// Count(*)
		int pageSize = 100;
		long count = funcaos.size();

		createPaginator(page, count, pageSize);

		table.setPageSize(pageSize);
		table.setRowList(funcaos);
	}
	public boolean exportar(){
		String csv = funcaoService.csvCargo(getEmpresa());
		download(csv, "cargos.csv");
		return true;
	}
}
