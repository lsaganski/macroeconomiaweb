package br.livetouch.livecom.web.pages.report;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextArea;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.DateField;
import net.sf.click.extras.control.IntegerField;

@Controller
@Scope("prototype")
public class LoginReportPage extends RelatorioPage {
	
	public RelatorioFiltro filtro;
	public PaginacaoTable table = new PaginacaoTable();
	public int page;
	private IntegerField tMax;
	public Form form = new Form();
	public TextArea textarea = new TextArea();
	public DateField tDataInicio = new DateField("dataInicial", getMessage("dataInicio.label"));
	public DateField tDataFim = new DateField("dataFinal", getMessage("dataFim.label"));
	public String dataInicio;
	public String dataFim;
	private TextField tUsuario;
	
	
	@Override
	public void onInit() {
		super.onInit();
		
		bootstrap_on = true;
		escondeChat = true;

		if(clear) {
			getContext().removeSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		}

		table();

		form();
	}
	
	public void form() {
		
		tDataInicio.setAttribute("class", "input data datepicker");
		tDataFim.setAttribute("class", "input data datepicker");
		tDataInicio.setFormatPattern("dd/MM/yyyy");
		tDataFim.setFormatPattern("dd/MM/yyyy");

		tDataInicio.setValue(DateUtils.toString(DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
		tDataFim.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		
		tDataInicio.setMaxLength(10);
		tDataFim.setMaxLength(10);

		form.add(tDataInicio);
		form.add(tDataFim);

		tUsuario = new TextField("usuario");
		tUsuario.setAttribute("class", "input");
		tUsuario.setId("usuario");
		form.add(tUsuario);
		
		Submit tfiltrar = new Submit("filtrar", getMessage("filtrar.label"), this, "filtrar");
		tfiltrar.setAttribute("class", "btn btn-primary min-width");
		form.add(tfiltrar);
		
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "btn btn-outline blue min-width");
		form.add(tExportar);
		
		tMax = new IntegerField("max");
		tMax.setAttribute("class", "input");
		form.add(tMax);
	}

	private void table() {
		Column c = new Column("data", getMessage("data.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("Web");
		c.setAttribute("align", "left");
		table.addColumn(c);
		
		c = new Column("Android");
		c.setAttribute("align", "left");
		table.addColumn(c);
		
		c = new Column("IOS");
		c.setAttribute("align", "left");
		table.addColumn(c);
		
		c = new Column("Total");
		c.setAttribute("align", "left");
		table.addColumn(c);
				
		c = new Column(getMessage("detalhes.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);
	}

	@Override
	public void onGet() {
		super.onGet();
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro != null) {
			filtro.setPage(0);
			form.copyFrom(filtro);
			filtro.setPage(page);
		}
	}
	
	public boolean exportar() throws DomainException {
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new RelatorioFiltro();
			getContext().setSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);
		filtro.setExpandir(true);
		
		String csv = reportService.csvLoginReport(filtro);
		download(csv, "login-report.csv");
		
		return true;
	}
	
	@Override
	public String getContentType() {
		return super.getContentType();
	}
	
	@Override
	public String getTemplate() {
		return super.getTemplate();
	}
	
}
