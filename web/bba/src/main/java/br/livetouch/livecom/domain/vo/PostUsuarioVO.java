package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.PostUsuarios;

public class PostUsuarioVO implements Serializable {

	private static final long serialVersionUID = 1L;
	public Long id;
	public Boolean push;
	public Boolean notification;
	public Long usuarioId;
	public Long postId;
	
	public PostUsuarioVO(PostUsuarios pu) {
		super();
		if(pu != null) {
			this.id = pu.getId();
			this.usuarioId = pu.getUsuario().getId();
			this.postId = pu.getPost().getId();
			this.push = pu.isPush();
			this.notification = pu.isNotification();
		}
	}
	
	public PostUsuarioVO() {	}
	
}