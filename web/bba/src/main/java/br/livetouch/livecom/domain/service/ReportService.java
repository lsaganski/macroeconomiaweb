package br.livetouch.livecom.domain.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.QuantidadeVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioAudienciaVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioLoginVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoFiltro;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import net.livetouch.tiger.ddd.DomainException;

public interface ReportService {

	List<RelatorioLoginVO> loginsResumido(RelatorioFiltro filtro) throws DomainException;
	
	HashMap<String, HashMap<String, Long>> buildMapLogin(List<RelatorioLoginVO> logins);
	
	long getCountloginsResumido(RelatorioFiltro filtro) throws DomainException;

	List<RelatorioLoginVO> reportVersao(RelatorioFiltro filtro) throws DomainException;

	List<RelatorioLoginVO> loginsExpandido(RelatorioFiltro filtro) throws DomainException;

	String csvVisualizacao(RelatorioVisualizacaoFiltro filtro);

	List<RelatorioVisualizacaoVO> relVisualizacao(RelatorioVisualizacaoFiltro filtro);

	String csvDetalhesVisualizacao(Usuario usuario, Post post);

	List<RelatorioLoginVO> loginsAgrupado(RelatorioFiltro filtro) throws DomainException;

	String csvLoginReport(RelatorioFiltro filtro) throws DomainException;

	String csvDetalhesLoginReport(RelatorioFiltro filtro) throws DomainException;

	String csvDetalhesAuditoriaPostReport(List<LogAuditoria> audits) throws DomainException;

	List<RelatorioVisualizacaoVO> relVisualizacaoConsolidado(RelatorioVisualizacaoFiltro filtro);

	String csvFavoritos(RelatorioFiltro filtro) throws DomainException;

	String csvDetalhesFavoritos(RelatorioFiltro filtro) throws DomainException;

	List<RelatorioAudienciaVO> findAudiencia(RelatorioFiltro filtro) throws DomainException;

	long getCountAudiencia(RelatorioFiltro filtro) throws DomainException;

	List<RelatorioAudienciaVO> audienciaDetalhes(RelatorioFiltro filtro) throws DomainException;

	Map<String, List<QuantidadeVersaoVO>> reportVersaoGrafico(RelatorioFiltro filtro) throws DomainException;

}
