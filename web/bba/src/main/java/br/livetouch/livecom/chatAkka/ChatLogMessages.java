package br.livetouch.livecom.chatAkka;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class ChatLogMessages {
	private static ChatLogMessages instance = new ChatLogMessages();
	public List<String> list = new ArrayList<String>();

	public void log(String string) {
		if(!StringUtils.contains(string, "typing")) {
			list.add(string);
		}
	}

	public static ChatLogMessages getInstance() {
		return instance;
	}
	
	public JSONObject getLogAfterIndex(Integer index) throws JSONException {
		if(list.size() > index) {
			String log = "";
			for (int i = index; list.size() > i; i++) {
				log = list.get(i) + "\n" + log;
			}
			JSONObject json = new JSONObject();
			json.put("msg", log);
			json.put("index", list.size());
			return json;
		}
		
		return null;
	}
	
	public Integer sizeLog() {
		return list.size();
	}
	
	public void clear() {
		list.clear();
	}
}