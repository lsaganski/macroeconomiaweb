package br.livetouch.livecom.web.filter;

import java.util.List;

public class ListUtils {
    public ListUtils() {
    }

    public static boolean isEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }

    public static boolean isNotEmpty(List<?> list) {
        return list != null && list.size() > 0;
    }
}
