package br.livetouch.livecom.web.pages.root;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.LivecomRootPage;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class DeletarUsuarioPage extends LivecomRootPage {
	public String msg;
	public Form form = new Form();
	public TextField tLogin;

	@Override
	public void onInit() {
		super.onInit();
		
		form.setMethod("post");
		form.add(tLogin = new TextField("login", true));
		form.setAttribute("class","selecionaProjeto");
		
		Submit bot = new Submit("deletar");
		bot.setAttribute("class", "botao botao-lg danger");
		form.add(bot);
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onPost() {
		if(form.isValid()) {
			String login = tLogin.getValue();
			try {
				Usuario usuario = usuarioService.findByLogin(login);
				usuarioService.delete(getUserInfo(), usuario);
				log("Usuario [" + login + "] deletado com sucesso");
				setFlashAttribute("msg", "Usuario [" + login + "] deletado com sucesso");
			} catch (DomainException e) {
				logError(e.getMessage(), e);
				setFlashAttribute("msgErro", "Não foi possivel remover o usuario");
			}
		}
		setFlashAttribute("msgErro", "Login não informado");
	}

}

