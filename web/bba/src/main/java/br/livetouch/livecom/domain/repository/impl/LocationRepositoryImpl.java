package br.livetouch.livecom.domain.repository.impl;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Location;
import br.livetouch.livecom.domain.repository.LocationRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class LocationRepositoryImpl extends StringHibernateRepository<Location> implements LocationRepository {

	public LocationRepositoryImpl() {
		super(Location.class);
	}

}