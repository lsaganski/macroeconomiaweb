package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Palestrante;
import br.livetouch.livecom.domain.repository.PalestranteRepository;
import br.livetouch.livecom.domain.service.PalestranteService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class PalestranteServiceImpl implements PalestranteService {
	@Autowired
	private PalestranteRepository rep;

	@Override
	public Palestrante get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Palestrante c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Palestrante> findAll() {
		return rep.findAll();
	}

	@Override
	public void delete(Palestrante c) throws DomainException {
		rep.delete(c);
	}
}
