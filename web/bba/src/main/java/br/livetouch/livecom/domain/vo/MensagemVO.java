package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Card;
import br.livetouch.livecom.domain.CardButton;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.Location;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.JSONUtils;

public class MensagemVO {
	private Long id;
	private Long conversaId;
	private String titulo;
	private String msg;
	
	private Long fromId;
	private String from;
	private String fromNome;
	private String fromUrlFoto;
	private String fromUrlFotoThumb;
	private boolean fromChatOn;
	
	private Long toId;
	private String to;
	private String toNome;
	private String toUrlFoto;
	private String toUrlFotoThumb;
	private boolean toChatOn;
	
	private Long postId;
	
	// Grupo
	private Long grupoId;
	private String nomeGrupo;
	private FileVO fotoGrupo;

	// 2 check cinza
	private Integer entregue;
	// 2 check azuk
	private Integer lida;
	
	private List<FileVO> arquivos;
	private BadgeChatVO badge;
	private Long identifier;
	
	// Se é admin do grupo
	private boolean isAdmin;
	
	// se a msg é do tipo admin (fulano saiu do grupo)
	private int msgAdmin;
	
	private String dataWeb;
	private boolean isGroup;
	private int grupoQtdeUsers;
	private String dataEntregue;
	private String dataLida;
	
	private long data;
	private String dataString;
	
	private NotificationChatVO notifications;
	
	private LocationVO location;
	private boolean isCallbackCheck1Cinza;
	public int getGrupoQtdeUsers() {
		return grupoQtdeUsers;
	}

	public void setGrupoQtdeUsers(int grupoQtdeUsers) {
		this.grupoQtdeUsers = grupoQtdeUsers;
	}

	public boolean isCallbackCheck1Cinza() {
		return isCallbackCheck1Cinza;
	}

	public void setCallbackCheck1Cinza(boolean callbackCheck1Cinza) {
		this.isCallbackCheck1Cinza = callbackCheck1Cinza;
	}

	public boolean isCallbackCheck2Azul() {
		return isCallbackCheck2Azul;
	}

	public void setCallbackCheck2Azul(boolean callbackCheck2Azul) {
		this.isCallbackCheck2Azul = callbackCheck2Azul;
	}

	public boolean isCallbackCheck2Cinza() {
		return isCallbackCheck2Cinza;
	}

	public void setCallbackCheck2Cinza(boolean callbackCheck2Cinza) {
		this.isCallbackCheck2Cinza = callbackCheck2Cinza;
	}

	public BadgeChatVO getBadge() {
		return badge;
	}

	public void setFromChatOn(boolean fromChatOn) {
		this.fromChatOn = fromChatOn;
	}

	public void setToChatOn(boolean toChatOn) {
		this.toChatOn = toChatOn;
	}

	public void setLida(Integer lida) {
		this.lida = lida;
	}

	public void setLocation(LocationVO location) {
		this.location = location;
	}

	private boolean isCallbackCheck2Azul;
	private boolean isCallbackCheck2Cinza;
	private String dataPush;
	private Boolean fromChatAway;
	private Boolean toChatAway;
	private HashSet<CardVO> cards;
	private HashSet<CardButtonVO> buttons;
	
	public MensagemVO () {
			
	}

	public void setMensagem(Mensagem msg, Usuario userLogado) {
		if (msg == null) {
			return;
		}
		this.id = msg.getId();


		MensagemConversa c = msg.getConversa();
		this.conversaId = c.getId();

		if(c.getPost() != null){
			this.postId = c.getPost().getId();	
		}

		if(c.getFrom() != null){
			Usuario from = msg.getFrom();
			this.fromId = from.getId();
			this.from = from.getLogin();
			this.fromNome = from.getNome();
			this.fromUrlFoto = from.getUrlFoto();
			this.fromUrlFotoThumb = from.getUrlThumb();
			this.fromChatOn = Livecom.getInstance().isUsuarioLogadoChat(fromId);
			this.setFromChatAway(Livecom.getInstance().isUsuarioAwayChat(fromId));
		}
		if(c.getTo() != null) {
			Usuario uTo = msg.getTo();
			this.toId = uTo.getId();
			this.to = uTo.getLogin();
			this.toNome = uTo.getNome();
			this.toUrlFoto = uTo.getUrlFoto();
			this.toUrlFotoThumb = uTo.getUrlThumb();
			this.toChatOn = Livecom.getInstance().isUsuarioLogadoChat(toId);
			this.setToChatAway(Livecom.getInstance().isUsuarioAwayChat(toId));
		}

		if (c.getGrupo() != null) {
			this.isGroup = true;

			this.grupoId = c.getGrupo().getId();				
			this.nomeGrupo = c.getGrupo().getNome();
			this.isAdmin = userLogado == null ? false : c.isAdminGrupo(userLogado);
			if (c.getGrupo().getFoto() != null) {
				this.fotoGrupo = new FileVO();
				this.fotoGrupo.setArquivo(c.getGrupo().getFoto(), false);
				this.fotoGrupo.resumir();
			}
			Set<GrupoUsuarios> users = c.getGrupo().getUsuarios();
			if(users != null) {
				this.grupoQtdeUsers = users.size();
			}
		}
		
		this.msg = msg.getMsg();

		this.data = msg.getDataCreated().getTime();
		this.dataString = msg.getDataString();
		
		this.setDataLida(msg.getDataLidaString());
		this.setDataEntregue(msg.getDataEntregueString());
		this.setData(msg.getDataCreated().getTime());
		this.identifier = msg.getIdentifier();
		this.setDataWeb(msg.getDataWeb());
		isCallbackCheck1Cinza = msg.isCallbackCheck1Cinza();
		isCallbackCheck2Azul = msg.isCallbackCheck2Azul();
		isCallbackCheck2Cinza = msg.isCallbackCheck2Cinza();
		this.setDataPush(msg.getDataPushStringHojeOntem());
		
		
		if(msg.isMsgAdmin()) {
			this.msgAdmin = 1;
		}
		
		// arquivos
		Set<Arquivo> list = msg.getArquivos();
		if(list != null && list.size() > 0) {
			arquivos = new ArrayList<FileVO>();
			for (Arquivo a : list) {
				FileVO f = new FileVO();
				f.setArquivo(a, false);
				arquivos.add(f);
			}
		}
		
		Set<Card> cards = msg.getCards();
		if(cards != null && cards.size() > 0) {
			this.setCards(new HashSet<CardVO>());
			for (Card card : cards) {
				this.getCards().add(new CardVO(card));
			}
		}
		
		Set<CardButton> buttons = msg.getButtons();
		if(buttons != null && buttons.size() > 0) {
			this.setButtons(new HashSet<CardButtonVO>());
			for (CardButton cardButton : buttons) {
				this.buttons.add(new CardButtonVO(cardButton));
			}
		}
		
		Location location = msg.getLocation();
		if(location != null) {
			/**
			 * Location de msg por chat.
			 */
			if(location.getLat() != null && location.getLng() != null) {
				if(!Double.isNaN(location.getLat()) && !Double.isNaN(location.getLng())) {
					this.location = new LocationVO(location.getLat(), location.getLng());
				}
			}
		}

		this.lida = msg.isLida() ? 1 : 0;

		this.entregue = msg.isEntregue() ? 1 : 0;
	}
	
	public void setMensagem(MensagemConversa c, Usuario userLogado) {
		Mensagem msg = c.getLastMensagem();
		this.conversaId = c.getId(); 

		if(c.getPost() != null){
			this.postId = c.getPost().getId();	
		}

		if(c.getFrom() != null){
			Usuario from = msg != null ? msg.getFrom() : c.getFrom();
			this.fromId = from.getId();
			this.from = from.getLogin();
			this.fromNome = from.getNome();
			this.fromUrlFoto = from.getUrlFoto();
			this.fromUrlFotoThumb = from.getUrlThumb();
			this.fromChatOn = Livecom.getInstance().isUsuarioLogadoChat(fromId);
		}
		if(c.getTo() != null) {
			Usuario uTo = msg != null ? msg.getTo() : c.getTo();
			this.toId = uTo.getId();
			this.to = uTo.getLogin();
			this.toNome = uTo.getNome();
			this.toUrlFoto = uTo.getUrlFoto();
			this.toUrlFotoThumb = uTo.getUrlThumb();
			this.toChatOn = Livecom.getInstance().isUsuarioLogadoChat(toId);
		}

		if (c.getGrupo() != null) {
			this.isGroup = true;

			this.grupoId = c.getGrupo().getId();				
			this.nomeGrupo = c.getGrupo().getNome();
			this.isAdmin = userLogado == null ? false : c.isAdminGrupo(userLogado);
			if (c.getGrupo().getFoto() != null) {
				this.fotoGrupo = new FileVO();
				this.fotoGrupo.setArquivo(c.getGrupo().getFoto(), false);
				this.fotoGrupo.resumir();
			}
			Set<GrupoUsuarios> users = c.getGrupo().getUsuarios();
			if(users != null) {
				this.grupoQtdeUsers = users.size();
			}
		}
		
		if(msg != null) {
			this.id = msg.getId();
			this.msg = msg.getMsg();

			this.data = msg.getDataCreated().getTime();
			this.dataString = msg.getDataString();
			
			this.setDataLida(msg.getDataLidaString());
			this.setDataEntregue(msg.getDataEntregueString());
			this.setData(msg.getDataCreated().getTime());
			this.identifier = msg.getIdentifier();
			this.setDataWeb(msg.getDataWeb());
			isCallbackCheck1Cinza = msg.isCallbackCheck1Cinza();
			isCallbackCheck2Azul = msg.isCallbackCheck2Azul();
			isCallbackCheck2Cinza = msg.isCallbackCheck2Cinza();
			this.setDataPush(msg.getDataPushStringHojeOntem());
			
			
			if(msg.isMsgAdmin()) {
				this.msgAdmin = 1;
			}
			
			// arquivos
			Set<Arquivo> list = msg.getArquivos();
			if(list != null && list.size() > 0) {
				arquivos = new ArrayList<FileVO>();
				for (Arquivo a : list) {
					FileVO f = new FileVO();
					f.setArquivo(a, false);
					arquivos.add(f);
				}
			}
			
			Location location = msg.getLocation();
			if(location != null) {
				/**
				 * Location de msg por chat.
				 */
				if(location.getLat() != null && location.getLng() != null) {
					if(!Double.isNaN(location.getLat()) && !Double.isNaN(location.getLng())) {
						this.location = new LocationVO(location.getLat(), location.getLng());
					}
				}
			}

			this.lida = msg.isLida() ? 1 : 0;

			this.entregue = msg.isEntregue() ? 1 : 0;
		}
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public Long getFromId() {
		return fromId;
	}
	
	public boolean isFromChatOn() {
		return fromChatOn;
	}
	
	public boolean isToChatOn() {
		return toChatOn;
	}

	public void setFromId(Long fromId) {
		this.fromId = fromId;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getFromUrlFoto() {
		return fromUrlFoto;
	}

	public void setFromUrlFoto(String fromUrlFoto) {
		this.fromUrlFoto = fromUrlFoto;
	}

	public Long getToId() {
		return toId;
	}

	public void setToId(Long toId) {
		this.toId = toId;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getToUrlFoto() {
		return toUrlFoto;
	}

	public void setToUrlFoto(String toUrlFoto) {
		this.toUrlFoto = toUrlFoto;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public List<FileVO> getArquivos() {
		return arquivos;
	}
	public void setArquivos(ArrayList<FileVO> arquivos) {
		this.arquivos = arquivos;
	}

	public int getLida() {
		return lida;
	}
	public Integer getEntregue() {
		return entregue;
	}

	public void setLida(int lida) {
		this.lida = lida;
	}
	public void setEntregue(Integer entregue) {
		this.entregue = entregue;
	}
	public Long getConversaId() {
		return conversaId;
	}

	public void setConversaId(Long conversaId) {
		this.conversaId = conversaId;
	}

	public void setArquivos(List<FileVO> arquivos) {
		this.arquivos = arquivos;
	}

	public String getFromNome() {
		return fromNome;
	}

	public void setFromNome(String fromNome) {
		this.fromNome = fromNome;
	}

	public String getToNome() {
		return toNome;
	}

	public void setToNome(String toNome) {
		this.toNome = toNome;
	}

	public String getToUrlFotoThumb() {
		if(StringUtils.isEmpty(toUrlFotoThumb)) {
			return toUrlFoto;
		}
		return toUrlFotoThumb;
	}

	public void setToUrlFotoThumb(String toUrlFotoThumb) {
		this.toUrlFotoThumb = toUrlFotoThumb;
	}

	public String getFromUrlFotoThumb() {
		if(StringUtils.isEmpty(fromUrlFotoThumb)) {
			return fromUrlFoto;
		}
		return fromUrlFotoThumb;
	}

	public void setFromUrlFotoThumb(String fromUrlFotoThumb) {
		this.fromUrlFotoThumb = fromUrlFotoThumb;
	}

	public Long getGrupoId() {
		return grupoId;
	}

	public void setGrupoId(Long grupoId) {
		this.grupoId = grupoId;
	}
	
	

	public String getNomeGrupo() {
		return nomeGrupo;
	}

	public void setNomeGrupo(String nomeGrupo) {
		this.nomeGrupo = nomeGrupo;
	}

	public FileVO getFotoGrupo() {
		return fotoGrupo;
	}

	public void setFotoGrupo(FileVO fotoGrupo) {
		this.fotoGrupo = fotoGrupo;
	}

	public void setBadge(BadgeChatVO b) {
		this.badge = b;
	}
	public BadgeChatVO getBadge(BadgeChatVO b) {
		return this.badge;
	}

	public Long getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}
	
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	public boolean isAdmin() {
		return isAdmin;
	}

	public int getMsgAdmin() {
		return msgAdmin;
	}
	
	public void setMsgAdmin(int msgAdmin) {
		this.msgAdmin = msgAdmin;
	}

	public String getDataWeb() {
		return dataWeb;
	}

	public void setDataWeb(String dataWeb) {
		this.dataWeb = dataWeb;
	}
	
	public boolean isGroup() {
		return isGroup;
	}

	public void setGroup(boolean isGroup) {
		this.isGroup = isGroup;
	}

	public int getCountUsers() {
		return grupoQtdeUsers;
	}

	public void setCountUsers(int countUsers) {
		this.grupoQtdeUsers = countUsers;
	}

	public LocationVO getLocation() {
		return location;
	}

	public boolean isEntregue() {
		return entregue == 1;
	}

	public boolean isLida() {
		return lida == 1;
	}
	
	@Override
	public String toString() {
		return JSONUtils.toJSON(this);
	}

	public static List<MensagemVO> create(List<Mensagem> msgs, Usuario user) {
		List<MensagemVO> list = new ArrayList<>();
		if(msgs != null) {
			for (Mensagem msg : msgs) {
				MensagemVO vo = new MensagemVO();
				vo.setMensagem(msg, user);
				list.add(vo);
			}
		}
		return list;
	}

	public NotificationChatVO getNotifications() {
		return notifications;
	}

	public void setNotifications(NotificationChatVO notifications) {
		this.notifications = notifications;
	}

	public String getDataEntregue() {
		return dataEntregue;
	}

	public void setDataEntregue(String dataEntregue) {
		this.dataEntregue = dataEntregue;
	}

	public String getDataLida() {
		return dataLida;
	}

	public void setDataLida(String dataLida) {
		this.dataLida = dataLida;
	}

	public long getData() {
		return data;
	}
	
	public void setData(long data) {
		this.data = data;
	}

	public String getDataPush() {
		return dataPush;
	}

	public void setDataPush(String dataPush) {
		this.dataPush = dataPush;
	}

	public String getDataString() {
		return dataString;
	}

	public void setDataString(String dataString) {
		this.dataString = dataString;
	}
	
	public long getAmigoId(Long idUserSender) {
		if(fromId != null && idUserSender != null) {
			Long id = idUserSender == fromId.longValue() ? toId : fromId;
			if(id == null) {
				return 0;
			}
	        return id;
		}
		return 0;
    }
	
	public String getStatusChat() {
		if(isLida()) {
			return "2A";
		} else if(isEntregue()) {
			return "2C";
		}
		return "1C";
	}

	public Boolean getFromChatAway() {
		return fromChatAway;
	}

	public void setFromChatAway(Boolean fromChatAway) {
		this.fromChatAway = fromChatAway;
	}

	public Boolean getToChatAway() {
		return toChatAway;
	}

	public void setToChatAway(Boolean toChatAway) {
		this.toChatAway = toChatAway;
	}

	public HashSet<CardVO> getCards() {
		return cards;
	}

	public void setCards(HashSet<CardVO> cards) {
		this.cards = cards;
	}

	public HashSet<CardButtonVO> getButtons() {
		return buttons;
	}

	public void setButtons(HashSet<CardButtonVO> buttons) {
		this.buttons = buttons;
	}

	public boolean isFrom(Long userId) {
        if (userId == null || fromId == null) {
            return false;
        }
        boolean ok = fromId.longValue() == userId.longValue();
        return ok;
    }
}