package br.livetouch.livecom.web.pages;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.click.Page;

@Controller
@Scope("prototype")
public class NowPage extends Page {

	public Date now = new Date();
}
