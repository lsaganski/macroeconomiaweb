package br.livetouch.livecom.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class LoginCount {

	private static LoginCount instance;
	private Map<String, LoginCountVO> map = new HashMap<String,LoginCountVO>();
	
	static class LoginCountVO {
		Integer count;
		Date date;
	}
	
	private LoginCount() {
		
	}
	
	public static LoginCount getInstance() {
		if(instance == null) {
			instance = new LoginCount();
		}
		return instance;
	}
	
	public int increment(String login) {
		LoginCountVO vo = map.get(login);
		if(vo == null) {
			vo = new LoginCountVO();
			vo.count = 0;
			vo.date = new Date();
		}
		vo.count++;
		map.put(login,vo);
		return vo.count;
	}
	
	public int get(String login) {
		LoginCountVO vo = map.get(login);
		if(vo == null) {
			return 0;
		}
		return vo.count;
	}

	public void clear(String login) {
		map.remove(login);
	}
	
	public long isBloqueado(String login) {
		LoginCountVO vo = map.get(login);
		
		return (long) DateUtils.getDiffInMinutes(vo.date, new Date());
	}
	
} 
