package br.livetouch.livecom.rest.resource;


import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import br.infra.util.Log;
import br.infra.util.TempExceptionUtils;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusParticipacao;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.PerfilService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.domain.vo.UsuarioAutocompleteFiltro;
import br.livetouch.livecom.domain.vo.UsuarioStatusCountVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import br.livetouch.livecom.rest.domain.UserPrincipal;
import br.livetouch.livecom.utils.UploadHelper;
import net.livetouch.extras.util.CripUtil;
import net.livetouch.tiger.ddd.DomainException;

@Path("/usuario")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class UsuarioResource extends MainResource{

	protected static final Logger log = Log.getLogger(UsuarioResource.class);
	
	@Context
	private SecurityContext securityContext;
	
	@Autowired
	protected UsuarioService usuarioService;
	
	@Autowired
	PerfilService permissaoService;
	
	@Autowired
	protected GrupoService grupoService;
	
	@GET
	@RolesAllowed({"admin", "user"})
	@Path("/perfil")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public UsuarioVO getPerfil() {
		Usuario usuario = usuarioService.get(getUser().getId());
		if(usuario == null) {
			throw new NotFoundException("Usuário não localizado");
		}
		UsuarioVO usuarioVO = new UsuarioVO();
		usuarioVO.setUsuario(usuario, usuarioService, grupoService);
		return usuarioVO;
	}
	
	protected Usuario getUser() {
		UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
		return userPrincipal.getUsuario();
	}
	
	@POST
	@Path("/cadastro")
	public MessageResult cadastroNutrabem(
			@FormParam("nome") String nome,
			@FormParam("login") String login,
			@FormParam("email") String email,
			@FormParam("senha") String senha,
			@FormParam("grupo_origem") String grupoOrigem,
			@FormParam("grupos") String grupos)
	{
		
		Usuario usuario = new Usuario();
		usuario.setNome(nome);
		usuario.setLogin(login);
		usuario.setEmail(email);
		usuario.setSenha(senha);
		
		
		if(StringUtils.isEmpty(nome) && StringUtils.isEmpty(login) && StringUtils.isEmpty(senha) && StringUtils.isEmpty(grupoOrigem)) {
			return MessageResult.error("Formulário inválido");
		}
		
		if(StringUtils.isEmpty(nome)) {
			return MessageResult.error("Preencha o campo nome");
		}
		
		if(StringUtils.isEmpty(login)) {
			return MessageResult.error("Preencha o campo login");
		}
		
		if(StringUtils.isEmpty(senha)) {
			return MessageResult.error("Preencha o campo senha");
		}
		
		if(StringUtils.isEmpty(grupoOrigem)) {
			return MessageResult.error("Preencha o campo grupo_origem");
		}
		
		Usuario usuarioExist = usuarioService.findByLogin(login);
		if(usuarioExist != null) {
			return MessageResult.error("Usuário já possui cadastrado");
		}

		long idGrupoOrigem = Long.parseLong(grupoOrigem);
		Grupo go = grupoService.get(idGrupoOrigem);
		if(go != null) {
			try {
				usuarioService.addGrupo(getUserInfo(),usuario,go);
			} catch (DomainException e) {
				MessageResult.error("Erro ao salvar usuário.");
			}
		} else {
			return MessageResult.error("Grupo " + grupoOrigem + " não encontrado");
		}

		Perfil permissao = permissaoService.get(2L);
		if(permissao == null) {
			return MessageResult.error("Permissão 2 não encontrada");
		}
		usuario.setPermissao(permissao);
		usuario.setPerfilAtivo(true);
		//Alterações para nutrabem 
		usuario.setAtivo(true);
		Empresa empresa = getEmpresa() != null ? getEmpresa() : empresaService.get(1L);
		usuario.setEmpresa(empresa);
		
		try {
			usuarioService.saveOrUpdate(getUserInfo(),usuario);
		} catch (DomainException e) {
			return MessageResult.error("Erro ao salvar usuário.");
		}
		
		String[] split = StringUtils.split(grupos, ",");
		if (split != null) {
			List<Long> ids = new ArrayList<Long>();
			for (String id : split) {
				id = StringUtils.trim(id);
				if (NumberUtils.isNumber(id)) {
					Long lId = new Long(id);
					if(go.getId() != lId) {
						ids.add(lId);
					}
				}
			}
			if(ids.size() > 0) {
				List<Grupo> listGrupos = grupoService.findAllByIds(ids);
				if(listGrupos.size() > 0) {
					usuarioService.saveGruposToUser(usuario, listGrupos);
					for (Grupo grupo2 : listGrupos) {
						try {
							GrupoUsuarios grupoUsuario = usuarioService.getGrupoUsuario(usuario, grupo2);
							grupoUsuario.setStatusParticipacao(StatusParticipacao.APROVADA);
							grupoService.criarNotificacao(grupoUsuario, TipoNotificacao.GRUPO_USUARIO_ACEITO);
						} catch (DomainException e) {
							return MessageResult.error("Não foi possivel aprovar o cadastro no grupo padrão.");
						}
					}
					
				}
			}
		}
		
		UsuarioVO usuarioVO = new UsuarioVO();
		usuarioVO.setUsuario(usuario, usuarioService, grupoService);
		return MessageResult.ok("Usuário cadastrado com sucesso", usuarioVO);
	}
	
	@POST
	@Path("/cadastro-bot")
	public MessageResult cadastroBot(
			@FormParam("nome") String nome,
			@FormParam("login") String login,
			@FormParam("email") String email)
	{
		
		Usuario usuario = new Usuario();
		usuario.setNome(nome);
		usuario.setLogin(login);
		usuario.setEmail(email);
		usuario.setEmpresa(getEmpresa());
		
		if(StringUtils.isEmpty(nome) && StringUtils.isEmpty(login)) {
			return MessageResult.error("Formulário inválido");
		}
		
		if(StringUtils.isEmpty(nome)) {
			return MessageResult.error("Preencha o campo nome");
		}
		
		if(StringUtils.isEmpty(login)) {
			return MessageResult.error("Preencha o campo login");
		}
		
		ParametrosMap params = ParametrosMap.getInstance(getEmpresa());
		String senha = params.get(Params.SENHA_PADRAO, "abc123");
		usuario.setSenhaCript(senha);
		
		Long idGrupoOrigem = params.getLong(Params.GRUPO_CHAT_BOT, 1L);
		
		Usuario usuarioExist = usuarioService.findByLogin(login);
		if(usuarioExist != null) {
			return MessageResult.error("Usuário já possui cadastrado");
		}

		Grupo go = grupoService.get(idGrupoOrigem);
		if(go != null) {
			try {
				usuarioService.addGrupo(getUserInfo(),usuario,go);
			} catch (DomainException e) {
				MessageResult.error("Erro ao salvar usuário.");
			}
		} else {
			return MessageResult.error("Grupo " + idGrupoOrigem + " não encontrado");
		}

		Perfil permissao = permissaoService.get(2L);
		if(permissao == null) {
			return MessageResult.error("Permissão 2 não encontrada");
		}
		usuario.setPermissao(permissao);
		usuario.setPerfilAtivo(true);
		//Alterações para nutrabem 
		usuario.setAtivo(true);
		Empresa empresa = getEmpresa() != null ? getEmpresa() : empresaService.get(1L);
		usuario.setEmpresa(empresa);
		
		try {
			usuarioService.saveOrUpdate(getUserInfo(),usuario);
		} catch (DomainException e) {
			return MessageResult.error("Erro ao salvar usuário.");
		}
		
		UsuarioVO usuarioVO = new UsuarioVO();
		usuarioVO.setUsuario(usuario, usuarioService, grupoService);
		return MessageResult.ok("Usuário cadastrado com sucesso", usuarioVO);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public List<UsuarioVO> getUsuarios(@BeanParam UsuarioAutocompleteFiltro filtro) {
		List<Usuario> usuarios = usuarioService.findByNomeLikeLogin(filtro, getEmpresa());
		List<UsuarioVO> list = UsuarioVO.toList(usuarios, usuarioService, grupoService);
		return list;
	}
	
	@GET
	@Path("/exportar")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response exportar() throws IOException {
		
		Empresa empresa = getEmpresa();
		String csv = usuarioService.csvUsuarios(empresa);
		
		String charset = ParametrosMap.getInstance(empresa).get(Params.EXPORTACAO_ARQUIVO_CHARSET, "ISO-8859-1");

		String attachament = "attachment; filename=" + URLEncoder.encode("usuarios.csv", "UTF-8");
		ResponseBuilder b = Response.status(Status.OK);
		b.encoding(charset);
		b.header("Content-Disposition", attachament);
		b.header("Content-Type", "application/octet-stream"+";charset="+charset);
		b.entity(csv);
		Response r = b.build();
		return r;
	}
	
	@GET
	@Path("/email")
	public Response email(@QueryParam("email") String email) {
		Usuario user = usuarioService.findByEmail(email);
		
		if(user == null) {
			return Response.ok(MessageResult.ok("Email válido")).build();
		} 
		
		return Response.ok(MessageResult.error("E-mail ja cadastrado!")).build();
	}
	
	
	@POST
	@Path("/senha")
	public Response alterarSenha(@QueryParam("id") Long id, @QueryParam("senha") String senha, @QueryParam("sendMail") boolean sendMail) throws DomainException {
		Usuario usuario = usuarioService.get(id);
		
		if(usuario == null) {
			return Response.ok(MessageResult.error("Usuario não encontrado")).build();
		} 
		
		if(StringUtils.isEmpty(senha)) {
			usuarioService.recuperarSenha(getEmpresa(), getUserInfo(), usuario);
		} else {
			String cript = CripUtil.criptToMD5(senha);
			usuario.setSenha(cript);
			usuarioService.saveOrUpdate(getUserInfo(), usuario);
			if(sendMail) {
				usuarioService.alterarSenha(getEmpresa(), getUserInfo(), usuario, senha);
			}
		}
		
		return Response.ok(MessageResult.ok("Senha alterada com sucesso!")).build();
	}

	@POST
	@Path("/idioma")
	public Response idioma(@QueryParam("id") Long id, @QueryParam("idioma") String idioma) throws DomainException {
		
		Usuario usuario = usuarioService.get(id);
		
		if(usuario == null) {
			return Response.ok(MessageResult.error("Usuario não encontrado")).build();
		} 

		if(StringUtils.isEmpty(idioma)) {
			return Response.ok(MessageResult.error("Idioma não encontrado")).build();
		} 
		
		Idioma i = idiomaService.findByCodigo(idioma);
		
		usuario.setIdioma(i);
		UserInfoVO.update(usuario);
		usuarioService.saveOrUpdate(usuario);
		
		return Response.ok(MessageResult.ok("Idioma alterado com sucesso!")).build();
	}
	
	@POST
	@Path("/uploadImagem")
	public Response uploadImagem(@QueryParam("fotoId") Long fotoId, @QueryParam("usuarioId") Long usuarioId, @QueryParam("tipo") String tipo) throws DomainMessageException {
		
		try {
			if(usuarioId == null || fotoId == null) {
				return Response.ok(MessageResult.error("Não foi possivel efetuar o upload da imagem")).build();
			}

			if(StringUtils.isEmpty(tipo)) {
				return Response.ok(MessageResult.error("Não foi possivel efetuar o upload da imagem")).build();
			}
			
			Usuario usuario = usuarioService.get(usuarioId);
			Arquivo a = arquivoService.get(fotoId);
			
			UploadHelper.log.debug("Arquivo: " + a.getId());
			UploadHelper.log.debug("Gerando thumbs arquivo: " + a.getNome());
			
			if(StringUtils.equalsIgnoreCase(tipo, "capa")) {
				usuario.setCapa(a);
			} else {
				usuario.setFoto(a);
			}
			
			usuarioService.saveOrUpdate(getUserInfo(),usuario);
			
			return Response.ok(MessageResult.ok("Upload ok")).build();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel efetuar o upload da imagem")).build();
		}
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			Usuario usuario = usuarioService.get(id);
			if(usuario == null) {
				return Response.ok(MessageResult.error("Usuario não encontrado")).build();
			}
			usuarioService.delete(getUserInfo(), usuario);
			return Response.ok().entity(MessageResult.ok("Usuario deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error(e.getMessage())).build();
		} catch (DataIntegrityViolationException e) {
			log.error(e.getMessage(), e);
			if(TempExceptionUtils.isErrorContraint(e)) {
				return Response.ok(MessageResult.error("Não foi possível excluir o Usuario " + id + " porque ele possui relacionamentos.")).build();
			}
			return Response.ok(MessageResult.error("Não foi possível excluir o Usuario  " + id + " porque ele possui relacionamentos.")).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Usuario  " + id)).build();
		}
	}
	
	@GET
	@Path("ativacao/{id}")
	public Response ativacao(@PathParam("id") Long id) {
		try {
			Usuario usuario = usuarioService.get(id);
			if(usuario == null) {
				return Response.ok(MessageResult.error("Usuario não encontrado")).build();
			}
			
			boolean ativar = usuario.isPerfilAtivo();
			
			usuario.setPerfilAtivo(!ativar);			
			usuarioService.saveOrUpdate(getUserInfo(),usuario);
			UsuarioVO vo = new UsuarioVO();
			vo.setUsuario(usuario);
			return Response.ok().entity(MessageResult.ok("Ativação alterada com sucesso", vo)).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Usuario  " + id)).build();
		}
	}
	
	@Path("/countStatus")
	@GET
	public Response getCountStatus(@QueryParam("id") String empresaId) {
		Empresa empresa = null;
		if(StringUtils.isNumeric(empresaId)) {
			empresa = empresaService.get(Long.parseLong(empresaId));
		} else {
			empresa = getEmpresa();
		}
		
		if(empresa == null) {
			return Response.ok(MessageResult.error("Favor informe o id da empresa")).build();
		}
		UsuarioStatusCountVO vo = usuarioService.getCountUsuariosStatus(empresa);
		return Response.ok().entity(MessageResult.ok(vo)).build();
	}
	
	@Path("/status")
	@GET
	public Response findUsuariosStatus(@QueryParam("status") String status, @QueryParam("id") String empresaId) {
		Empresa empresa = null;
		if(StringUtils.isNumeric(empresaId)) {
			empresa = empresaService.get(Long.parseLong(empresaId));
		} else {
			empresa = getEmpresa();
		}
		
		if(empresa == null) {
			return Response.ok(MessageResult.error("Favor informe o id da empresa")).build();
		}
		List<Usuario> usuarios = new ArrayList<>();
		if(StringUtils.isNotEmpty(status) && !StringUtils.equalsIgnoreCase(status, "todos")) {
			if(StringUtils.equalsIgnoreCase(status, "novo")) {
				usuarios = usuarioService.findNovos(empresa);
			} else if(StringUtils.equalsIgnoreCase(status, "inativo")) {
				usuarios = usuarioService.findInativos(empresa);
			} else if(StringUtils.equalsIgnoreCase(status, "ativo")) {
				usuarios = usuarioService.findAtivos(empresa);
			}
		} else {
			usuarios = usuarioService.findAll(empresa);
		}
		if(usuarios.size() > 0) {
			List<UsuarioVO> users = UsuarioVO.toList(usuarios, usuarioService, grupoService);
			return Response.ok().entity(MessageResult.ok(users)).build();
		}
		return Response.ok().entity(MessageResult.ok(usuarios)).build();
	}
	
}
	
