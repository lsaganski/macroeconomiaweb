package br.livetouch.livecom.domain.service.impl;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.CategoriaIdioma;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.PostIdioma;
import br.livetouch.livecom.domain.repository.IdiomaRepository;
import br.livetouch.livecom.domain.repository.PostIdiomaRepository;
import br.livetouch.livecom.domain.service.IdiomaService;
import br.livetouch.livecom.domain.service.NotificationService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class IdiomaServiceImpl extends LivecomService<Idioma> implements IdiomaService {
	
	@Autowired
	private IdiomaRepository rep;

	@Autowired
	private PostIdiomaRepository postIdiomaRep;

	@Autowired
	private NotificationService notificationService;

	@Override
	public Idioma get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Idioma i) throws DomainException {
		rep.saveOrUpdate(i);
	}

	@Override
	public void delete(Idioma i) throws DomainException {
		
		try {
			execute("delete from PostIdioma where idioma.id = ?", i.getId());
			notificationService.deleteBy(i);
		} catch (Exception e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if (root == null) {
				root = e;
			}
	
			throw new DomainException("Não foi possível excluir o idioma [" + i + "]");
		}
		
		
		Set<PostIdioma> posts = postIdiomaRep.findByIdioma(i);
		for (PostIdioma postIdioma : posts) {
			postIdiomaRep.delete(postIdioma);
		}
		rep.delete(i);
	}
	
	@Override
	public List<Idioma> findAll() {
		return rep.findAll();
	}

	@Override
	public List<Idioma> findByCodigos(String[] idiomas) {
		return rep.findByCodigos(idiomas);
	}

	@Override
	public Idioma findByCodigo(String idioma) {
		Idioma i = new Idioma();
		i.setCodigo(idioma);
		return rep.findByCodigo(i);
	}

	@Override
	public Idioma findByCodigo(Idioma idioma) {
		return rep.findByCodigo(idioma);
	}

	@Override
	public String getNomeByIdioma(CategoriaPost categoria, Idioma idioma) {
		Set<CategoriaIdioma> idiomas = categoria.getIdiomas();
		for (CategoriaIdioma categoriaIdioma : idiomas) {
			
			if(categoriaIdioma.getIdioma().equals(idioma)) {
				return categoriaIdioma.getNome();
			}
			
		}
		
		return categoria.getNome();
	}

}
