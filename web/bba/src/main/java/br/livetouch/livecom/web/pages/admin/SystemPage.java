package br.livetouch.livecom.web.pages.admin;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.livecom.web.pages.pages.MuralPage;

/**
 * 
 */
@Controller
@Scope("prototype")
public class SystemPage extends LivecomAdminPage {

	public String memoria;
	public RuntimeMXBean mxBean;
	public List<String> mxBeanInputArguments;
	public Date now = new Date();
	
	public Date start;
	public String uptime;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
		
		memoria = "Total: " + Utils.sizeToHumanReadable(Runtime.getRuntime().totalMemory()) + ", Free: " + Utils.sizeToHumanReadable(Runtime.getRuntime().freeMemory()) + ", Utilizada: " + Utils.sizeToHumanReadable((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()));
		
		Properties properties = System.getProperties();
		List<String> list = new ArrayList<String>();
		Set<Object> keys = properties.keySet();
		for (Object key : keys) {
			String s = properties.getProperty(key.toString());
			list.add("<b>"+key+"</b> : " + s);
		}
		addModel("system_properties", list);
		
		try {
			mxBean = ManagementFactory.getRuntimeMXBean();
			if(mxBean != null) {
				start = DateUtils.toDate(mxBean.getStartTime());
				uptime = Utils.getDurationStringMillis(mxBean.getUptime());
				mxBeanInputArguments = mxBean.getInputArguments();
			}
		} catch (Exception e) {
			logError(e.getMessage(), e);
		}
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
