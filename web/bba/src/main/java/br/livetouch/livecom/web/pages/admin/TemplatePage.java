package br.livetouch.livecom.web.pages.admin;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.Utils;
import br.infra.web.click.ComboTipoTemplate;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.enums.TipoTemplate;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.livetouch.click.control.IdField;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.FileField;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextArea;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class TemplatePage extends LivecomAdminPage {

	public Form form = new Form();
	public String msgErro;
	public Long id;

	public int page;
	private TextField tNome;
	private TextArea tHtml;
	private FileField fileField;
	public ComboTipoTemplate comboTipo;
	
	public TemplateEmail templateEmail;
	
	public String tipo;
	
	public ActionLink deleteLink = new ActionLink("deletar", this, "deletar");
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.ENVIAR_EMAILS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();

		form();
		
		if (id != null) {
			templateEmail = templateEmailService.get(id);
		} else {
			templateEmail = new TemplateEmail();
		}
		
		//Provisório
		tipo = "emailMarketing";
		if(StringUtils.isNotEmpty(tipo)) {
			List<TipoTemplate> list = new ArrayList<TipoTemplate>();
			list.add(TipoTemplate.valueOf(tipo));
			comboTipo.setValues(list);
		}
		
		form.copyFrom(templateEmail);
	}
	
	public void form(){
		form.add(new IdField());

		tNome = new TextField("nome");
		tNome.setFocus(true);
		tNome.setAttribute("autocomplete", "off");
		tNome.setAttribute("class", "inputCLean");
		tNome.setId("nome");
		form.add(tNome);
		
		tHtml = new TextArea("html");
		tHtml.setAttribute("class", "inputCLean form-control");
		form.add(tHtml);
		
		fileField = new FileField("fileField");
		form.add(fileField);
		
		form.add(comboTipo = new ComboTipoTemplate());
		
		Submit tsalvar = new Submit("salvar", this, "salvar");
		tsalvar.setAttribute("class", "btn btn-primary min-width");
		form.add(tsalvar);
		
		if (id != null) {
			deleteLink.setValueObject(id);
			deleteLink.setId("form_deletar");
			deleteLink.setLabel("<i class='fa fa-trash'></i> Deletar");
			form.add(deleteLink);
		}
		
		Submit cancelar = new Submit("cancelar",this,"cancelar");
		cancelar.setAttribute("class", "btn btn-default min-width");
		form.add(cancelar);

	}
	
	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				
				if(id != null) {
					templateEmail = templateEmailService.get(id);
					templateEmail.setDateUpdated(new Date());
				} else {
					templateEmail = new TemplateEmail();
					templateEmail.setDateCreated(new Date());
					templateEmail.setEmpresa(getEmpresa());
				}
				
				form.copyTo(templateEmail);
				
				Long empresaId = getUserInfoVO().getEmpresaId();
				Empresa empresa = getEmpresa();
				if(empresaId != null) {
					empresa = empresaService.get(empresaId);
				}
				TemplateEmail template = templateEmailService.existe(empresa, templateEmail);
				if(template != null) {
					setFlashAttribute("msg",  "Ja existe um template com o tipo [" + template.getTipoTemplate().name() + "], por favor altere ou edite o template ["+templateEmail.getNome()+"].");
					setRedirect(TemplatesPage.class);
					return false;
				}
				
				String html = Utils.readFileItem(fileField.getFileItem());
				if(StringUtils.isNotEmpty(html)) {
					templateEmail.setNomeHtml(fileField.getFileItem().getName());
				}
				
				templateEmailService.saveOrUpdate(templateEmail, empresa);

				setFlashAttribute("msg",  "Template ["+templateEmail.getNome()+"] salvo com sucesso.");
				setRedirect(TemplatesPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean cancelar() {
		setRedirect(TemplatesPage.class);
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			TemplateEmail e = templateEmailService.get(id);
			templateEmailService.delete(e);
			setRedirect(TemplatesPage.class);
			setFlashAttribute("msg", "Template deletado com sucesso.");
		} catch (DomainException e) {
			logError(e.getMessage(), e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(), e);
			this.msgErro = "Esse template possui relacionamentos. Não foi possível excluir.";
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
