package br.livetouch.livecom.web.servlet;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Ricardo Lecheta
 *
 */
public class FotoServlet extends HttpServlet {
	private static final long serialVersionUID = 1072329623420157062L;
	public static final Upload upload = new Upload();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			String uri = req.getRequestURI();
			int idx = uri.lastIndexOf("/");
			String proposta = uri.substring(idx+1);

			ServletInputStream in = req.getInputStream();
			DataInputStream data = new DataInputStream(in);

			int size = data.readInt();

			for (int i = 0; i < size; i++) {
				String nome = data.readUTF();

				int length = data.readInt();
				byte[] bytes = new byte[length];
				data.readFully(bytes);

				String tmpDir = System.getProperty("java.io.tmpdir");
				File file = new File(tmpDir,proposta+"_"+nome+".png");
				FileOutputStream fout = new FileOutputStream(file);
				fout.write(bytes);
				fout.flush();
				fout.close();

				upload.add(proposta, file);
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		System.out.println("OK");
	}

	public static class Upload {
		private final Map<String, List<File>> map = new HashMap<String, List<File>>();
		public List<File> getMap(String proposta, boolean remove) {
			List<File> list = remove ? map.remove(proposta) : map.get(proposta);
			return list;
		}
		public List<File> getMap(String proposta) {
			return getMap(proposta, false);
		}
		public void add(String proposta, File file) {
			List<File> files = getMap(proposta);
			if(files == null) {
				files = new ArrayList<File>();
				map.put(proposta, files);
			}
			for (File file2 : files) {
				if(file2.getName().equals(file.getName())) {
					files.remove(file2);
					break;
				}
			}
			files.add(file);
		}
	}
}