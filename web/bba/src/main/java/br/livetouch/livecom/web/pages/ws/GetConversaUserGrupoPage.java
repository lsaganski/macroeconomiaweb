package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.ConversaNotification;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.NotificationChatVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class GetConversaUserGrupoPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	
	public ConversaVO conversaVo;
	private UsuarioLoginField tUserId;
	private LongField tGrupoId;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		form.add(tUserId = new UsuarioLoginField("user_logado_id", false, usuarioService, getEmpresa()));
		form.add(tGrupoId = new LongField("grupo_id", false));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));
		
		tUserId.setFocus(true);
		tMode.setValue("json");

		form.add(new Submit("Enviar"));

		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario user = tUserId.getEntity();
			Grupo grupo = tGrupoId.getLong() != null ? grupoService.get(tGrupoId.getLong()) : null;
			
			if(user == null) {
				return new MensagemResult("NOK","Usuário inválido.");
			}
			if(grupo == null) {
				return new MensagemResult("NOK","Grupo inválido.");
			}
			
			boolean participa = grupoService.isUsuarioDentroDoGrupo(grupo, user);
			if (!participa) {
				return new MensagemResult("NOK","Usuário não tem permissão para visualizar essa conversa.");
			}

			/**
			 * Busca a conversa ou cria uma nova entre estes dois users
			 */
			MensagemConversa conversa = mensagemService.getMensagemConversaByUserGrupo(user,grupo, true);
			ConversaNotification notifications = conversaNotificationService.findByConversationUser(conversa.getId(), user.getId());
			if(isWsVersion3()) {
				Response r = Response.ok("OK");
				MensagemVO mensagemVO = new MensagemVO();
				mensagemVO.setMensagem(conversa, user);
				mensagemVO.setNotifications(new NotificationChatVO(notifications));
				r.msg = mensagemVO;
				return r;
			}
			ConversaVO vo = new ConversaVO();
			vo.setConversa(user, conversa);
			return vo;
		}
		return new MensagemResult("NOK","Erro ao encontrar a conversa.");
	}

	
	@Override
	protected void xstream(XStream x) {
		x.alias("conversa", ConversaVO.class);
		x.alias("msg", MensagemVO.class);
		x.alias("arquivo", FileVO.class);
		super.xstream(x);
	}
}
