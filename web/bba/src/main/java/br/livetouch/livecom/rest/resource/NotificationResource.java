package br.livetouch.livecom.rest.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.AudienciaVO;
import br.livetouch.livecom.domain.vo.NotificationVO;
import br.livetouch.livecom.domain.vo.RelatorioNotificationsVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoFiltro;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import br.livetouch.livecom.rest.domain.MessageResult;

@Path("/v1/notification")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class NotificationResource extends MainResource {

	protected static final Logger log = Log.getLogger(NotificationResource.class);

	@GET
	@Path("/relatorioVisualizao")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public List<RelatorioVisualizacaoVO> relatorioVisualizacao(@BeanParam RelatorioVisualizacaoFiltro filtro) {

		setAttributeSession(RelatorioVisualizacaoFiltro.SESSION_FILTRO_KEY, filtro);

		filtro.setEmpresaId(getEmpresa().getId());

		List<RelatorioVisualizacaoVO> list;
		if (StringUtils.equals(filtro.getModo(), "consolidado")) {
			list = reportService.relVisualizacaoConsolidado(filtro);
		} else {
			list = reportService.relVisualizacao(filtro);
		}
		return list;
	}

	@GET
	@Path("/detalhesVisualizao")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public List<NotificationVO> detalhesVisualizacao(@QueryParam("id") Long id) {
		RelatorioVisualizacaoFiltro filtro = (RelatorioVisualizacaoFiltro) getSession()
				.getAttribute(RelatorioVisualizacaoFiltro.SESSION_FILTRO_KEY);
		List<NotificationVO> list = new ArrayList<NotificationVO>();
		Post post = postService.get(id);
		if (post != null) {
			if (filtro != null) {
				Usuario usuario = null;
				if (filtro.getUsuarioId() != null) {
					usuario = usuarioService.get(filtro.getUsuarioId());
				}
				if (usuario != null) {
					list = notificationService.findAllGroupUserByPostDate(usuario, post);
				} else {
					list = notificationService.findAllGroupUserByPostDate(null, post);
				}
			} else {
				list = notificationService.findAllGroupUserByPostDate(null, post);
			}
		}
		return list;
	}
	
	@GET
	@Path("/detalhesNotification")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Object detalhesNotification(@QueryParam("id") Long id) {
		List<NotificationVO> list = new ArrayList<NotificationVO>();
		Notification notification = notificationService.get(id);
		if (notification != null) {
			RelatorioNotificationsVO vo = new RelatorioNotificationsVO(notification);
			List<NotificationUsuario> notifications = notificationService.findNotificationsFilhas(notification);
			for (NotificationUsuario n : notifications) {
				NotificationVO item = new NotificationVO(n);
				list.add(item);
			}
			vo.setNotifications(list);
			return vo;
		}
		return Response.ok(MessageResult.error("Notificação não encontrada")).build();
	}

	@GET
	@Path("/quantidade/{id}")
	public Response quantidadeVisualizacoes(@PathParam("id") Long id) {
		Post post = postService.get(id);

		if (post == null) {
			return Response.ok(MessageResult.error("Post não encontrado")).build();
		}

		AudienciaVO audienciaVO = notificationService.getCountAudienciaByPost(post);
		if (audienciaVO != null) {
			long total = postService.getCountUsuariosByPost(post);
			audienciaVO.setTotal(total);
			Long iteracao = audienciaVO.getIteracao() != null ? audienciaVO.getIteracao() : 0;
			audienciaVO.setRestante(total - iteracao);
		}
		return Response.ok(audienciaVO).build();
	}
	
	@POST
	@Path("/mark/sent/{id}")
	public Response markAsEnviada(@PathParam("id") Long id) {
		NotificationUsuario notificationUsuario = notificationService.getNotificationUsuario(id);
		if(notificationUsuario != null) {
			notificationService.markAsRead(notificationUsuario);
			return Response.ok(MessageResult.ok("Notificação marcada como enviada")).build();
		} else {
			return Response.ok(MessageResult.error("Notificação não encontrada")).build();
		}
	}
	
	@POST
	@Path("/markAll/sent/{id}")
	public Response markAllAsEnviada(@PathParam("id") Long id) {
		try {
			notificationService.markAllAsRead(id);
			return Response.ok(MessageResult.ok("Notificações marcadas como enviada")).build();
		} catch(Exception e) {
			return Response.ok(MessageResult.ok("Erro ao marcar as notificações como enviada")).build();
		}
	}
}
