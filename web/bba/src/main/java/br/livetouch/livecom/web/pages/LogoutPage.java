package br.livetouch.livecom.web.pages;

import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.web.pages.admin.LivecomPage;

@Controller
@Scope("prototype")
public class LogoutPage extends LivecomPage {
	
	@Override
	public boolean onSecurityCheck() {
		return true;
	}

	@Override
	public void onInit() {

		UserInfoVO userInfo = getUserInfoVO();
		Livecom.getInstance().removeUserInfo(userInfo,"web");

		HttpSession session = getContext().getSession();
		session.invalidate();

		setRedirect(LogonPage.class);
		
		removeCookie(COOKIE_LOGIN);
	}
}
