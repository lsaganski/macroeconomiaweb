package br.livetouch.livecom.web.pages.ws;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.security.UserAgentUtil;
import br.livetouch.pushserver.lib.DeviceFirebaseVO;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class LogoutPage extends WebServiceXmlJsonPage {
	
	public Form form = new Form();

	public String msg;

	private UsuarioLoginField tUser;
	//private TextField tUser;
	private TextField tMode;

	private Checkbox checkValidar;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tUser = new UsuarioLoginField("user", true,usuarioService, getEmpresa()));
		tUser.setFocus(true);
		
		form.add(new TextField("registrationId"));

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		form.add(tMode = new TextField("mode"));
		form.add(checkValidar = new Checkbox("validar"));
		
		checkValidar.setChecked(true);

		tMode.setValue("json");

		form.add(new Submit("logout",this,"logout"));

		setFormTextWidth(form);
	}

	public boolean logout() {
		return true;
	}

	@Override
	protected Object execute() throws Exception {

		if(form.isValid()) {
			String login = this.getParam("user");

			log("Chamando logout para ["+login+"]");
			
			Usuario usuario = tUser.getEntity();
			if (usuario == null) {
				return new MensagemResult("ERRO","Usuario inválido");
			}
			
			UserInfoVO usuarioLogado = Livecom.getInstance().getUserInfo(usuario.getLogin());
			if (usuarioLogado == null) {
				return new MensagemResult("ERRO","Usuario não logado");
			}
			
			log("Logout user ["+login+"]");
			
			//Unregister
			String registrationId = getParam("registrationId");

			if(StringUtils.isNotEmpty(registrationId)) {
				String cod = usuario.getLogin();
				DeviceFirebaseVO d = new DeviceFirebaseVO();
				d.setRegistrationId(registrationId);
				d.setUserCode(cod);
				
				pushLivecomService.unregister(usuario.getEmpresa(), d);
			}
			
			String so = UserAgentUtil.getUserAgentSO(getContext());
			Livecom.getInstance().removeUserInfo(usuarioLogado, so);

			UserInfoVO.removeHttpSession(getContext());
			
			if(isWsVersion3()) {
				return Response.ok("Logout efetuado com sucesso.");
			} else {
				return new MensagemResult("OK","Logout efetuado com sucesso.");
			}
		}

		logError("Form invalid logout: " + getFormErrorMensagemResult(form));

		return new MensagemResult("NOK","Usuario nao encontrado");
	}
	
    @Override
    protected boolean isWsTokenOn() {
        return false;
    }
    
    @Override
    protected boolean isWsCryptOn() {
    	return false;
    }
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
