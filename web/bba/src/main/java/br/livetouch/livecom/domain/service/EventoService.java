package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Evento;
import br.livetouch.livecom.domain.Grupo;
import net.livetouch.tiger.ddd.DomainException;

public interface EventoService extends Service<Evento> {

	List<Evento> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Evento f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Evento f) throws DomainException;

	List<Evento> findByAtivo(List<Grupo> grupos);

	List<Evento> findAllOrderBy();

}
