package br.livetouch.livecom.chatAkka.actor;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.actor.UntypedActorContext;
import br.infra.util.Log;
import br.livetouch.livecom.chatAkka.AkkaChatServer;
import br.livetouch.livecom.chatAkka.LivecomChatInterface;
import br.livetouch.livecom.chatAkka.protocol.AssociateWithUser;
import br.livetouch.livecom.chatAkka.protocol.ChatUtils;
import br.livetouch.livecom.chatAkka.protocol.JsonRawMessage;
import br.livetouch.livecom.chatAkka.protocol.JsonReplicateRawMessage;
import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.domain.Livecom;

public class UserActor extends UntypedActor {

	private static Logger log = Log.getLogger("UserActor");

	/**
	 * Map [so: android/ios/web : ActorRef]
	 * Caso o usuario se logue em mais de um dispositivo (android, ios, browser)
	 */
	public Map<String,ActorRef> connectionMap = new HashMap<String, ActorRef>();

	private Long userId;

	public static ActorRef create(UntypedActorContext context, String name) {
		ActorRef ref = context.actorOf(Props.create(UserActor.class), name);
		return ref;
	}

	@Override
	public void preStart() throws Exception {
		
	}

	protected LivecomChatInterface getLivecomInterface() {
		LivecomChatInterface livecomInterface = AkkaChatServer.getLivecomInterface();
		if(livecomInterface == null) {
			throw new IllegalArgumentException("SpringError: LivecomChatInterface is null.");
		}
		return livecomInterface;
	}

	public static class AddConnection {
		public ActorRef connection;
		public Long userId;
		public String so;

		public AddConnection(ActorRef connection, Long userId, String so) {
			this.connection = connection;
			this.userId = userId;
			this.so = so;
		}

		@Override
		public String toString() {
			return "AddConnection [connection=" + connection + "]";
		}
	}

	public static class RemoveConnection {
		/**
		 * Se encerrou o TCP passa esse parâmetro
		 */
		public ActorRef connection;
		public String so;

		public RemoveConnection(ActorRef connection, String so) {
			this.connection = connection;
			this.so = so;
		}

		@Override
		public String toString() {
			return "RemoveConnection [connection=" + connection + "]";
		}

	}
	
	public static class Stop {
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof AddConnection) {
			receiveAddConnection((AddConnection)msg);
		} else if (msg instanceof RemoveConnection) {
			receiveRemoveConnection((RemoveConnection) msg);
		} else if (msg instanceof JsonRawMessage) { 
			receiveJson((JsonRawMessage) msg);
		} else if (msg instanceof JsonReplicateRawMessage) { 
			receiveReplicateJson((JsonReplicateRawMessage) msg);
		} else if (msg instanceof Stop) {
			receiveStop();
		}
	}

	private void receiveAddConnection(AddConnection msg) {
		userId = msg.userId;
		String so = msg.so;
		
		ActorRef connection = msg.connection;

		log("AddConnection actor ["+connection+"] userId ["+userId+"], so ["+so+"]");
		
		connectionMap.put(so,connection);

		connection.tell(new AssociateWithUser(getSelf(), userId, so), getSelf());
	}

	private void receiveRemoveConnection(RemoveConnection msg) {
		String so = msg.so;
		
		if(so != null) {
			stopConnection(so);

			if (connectionMap.size() == 0) {
				stop();
			}
		}
	}

	private void receiveJson(JsonRawMessage msg) {
		Set<String> keys = connectionMap.keySet();
		for (String so : keys) {
			ActorRef actor = connectionMap.get(so);
			AkkaHelper.tellTcpJson(getSelf(), actor, msg);
		}
	}
	
	/**
	 * Manda somente para as outras conexoes do usuario.
	 * 
	 * @param msg
	 */
	private void receiveReplicateJson(JsonReplicateRawMessage msg) {
		String sender = msg.sender.toString();
		Set<String> keys = connectionMap.keySet();
		for (String key: keys) {
			// Outra conexao
			ActorRef actor = connectionMap.get(key);
			if(!StringUtils.equals(actor.toString(), sender)) {
				AkkaHelper.tellTcpJson(getSelf(), actor, msg.raw);
			}
		}
	}

	private void receiveStop() {
		Set<String> keys = connectionMap.keySet();

		for (String so : keys) {
			stopConnection(so);
		}

		stop();
	}

	/**
	 * Manda Stop para TcpConnectionActor ou WebConnectionActor
	 * 
	 * @param key
	 */
	private void stopConnection(String so) {
		ActorRef tcpWebRef = connectionMap.remove(so);
		if(tcpWebRef != null) {
			tcpWebRef.tell(new Stop(), getSelf());
			// Remove a session do UserInfo
			Livecom.getInstance().removeSessionChat(userId, tcpWebRef);
		}

	}

	/**
	 * Destroi este ator
	 */
	public void stop() {
		log("stop() actor " + getSelf());
		getContext().stop(getSelf());

		ChatUtils.sendUserStatus(getLivecomInterface(),userId,false,false,null);
	}
	
	private void log(String string) {
		int max = 200;
		if(string.length() > max) {
			string = string.substring(0,max);
		}
		if(userId != null) {
			log.debug("("+userId+"/"+connectionMap.keySet()+"): " + string);
		}
		else {
			log.debug(string);
		}
	}
}
