package br.livetouch.livecom.rest.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Path("/hello")
@Api(value="/user", description = "Operations about user")
public class HelloResource {

	@GET
	@ApiOperation(value = "Create user", notes = "This can only be done by the logged in user.")
	public Response hello() {
		return Response.ok().build();
	}
	
}
