package br.livetouch.livecom.domain.service;


import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.StatusPost;
import net.livetouch.tiger.ddd.DomainException;

public interface StatusPostService extends br.livetouch.livecom.domain.service.Service<StatusPost> {

	List<StatusPost> findAll(Empresa e);
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(StatusPost s, Empresa empresa) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(StatusPost s) throws DomainException;
	
	StatusPost findByCodigo(String status, Empresa empresa);

	StatusPost findByCodigo(StatusPost status, Empresa empresa);

}