package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.StatusPost;

public class StatusPostVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public Long id;
	public String nome;
	public String codigo;
	public String cor;
	public Long count;
	
	public StatusPostVO() {
		
	}

	public StatusPostVO(StatusPost status) {
		this.id = status.getId();
		this.nome = status.getNome();
		this.codigo = status.getCodigo();
		this.cor = status.getCor();
		
		if(status.getPosts() != null) {
			this.count = Long.valueOf(status.getPosts().size());
		}
	}
	
	public static List<StatusPostVO> fromList(List<StatusPost> status) {
		List<StatusPostVO> vos = new ArrayList<>();
		if(status == null) {
			return vos;
		}
		for (StatusPost s : status) {
			vos.add(new StatusPostVO(s));
		}
		return vos;
	}

	

}
