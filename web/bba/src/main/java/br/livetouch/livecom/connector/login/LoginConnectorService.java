package br.livetouch.livecom.connector.login;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class LoginConnectorService implements LoginConnector {
	protected static final Logger log = Log.getLogger(LoginConnector.class);
	
	@Autowired
	DefaultLoginConnector defaultLoginConnector;

	@Autowired
	HttpLoginConnector httpLoginConnector;

	@Autowired
	LdapLoginConnector ldapLoginConnector;

	public boolean validate(Usuario user, String pwd) throws DomainException {
		
		Empresa e = user.getEmpresa();
		ParametrosMap params = ParametrosMap.getInstance(e);
		
		boolean ok = false;

		boolean loginLDAP = user.isLoginLDAP();

		if(loginLDAP) {
			/**
			 * Ex: Usuário LDAP
			 */
			String connector = params.get("login.connector.2","ldap");
			LoginConnector c = getLoginConnector(user, connector);

			ok = c.validate(user, pwd);
			
		} else {
			/**
			 * Padrao Livecom
			 */
			String connector = params.get("login.connector","default");
			LoginConnector c = getLoginConnector(user, connector);

			ok = c.validate(user, pwd);
		}
		
		return ok;
	}
	
	private LoginConnector getLoginConnector(Usuario user, String connector) {
		if("http".equals(connector)) {
			return httpLoginConnector;
		} else if("ldap".equals(connector)) {
			return ldapLoginConnector;
		}
		return defaultLoginConnector;
	}
}
