package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.StatusPost;
import br.livetouch.livecom.domain.repository.StatusPostRepository;
import br.livetouch.livecom.domain.service.StatusPostService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class StatusPostServiceImpl implements StatusPostService {
	@Autowired
	private StatusPostRepository rep;

	@Override
	public StatusPost get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(StatusPost s, Empresa empresa) throws DomainException {
		if(empresa != null) {
			s.setEmpresa(empresa);
		}
		rep.saveOrUpdate(s);
	}

	@Override
	public void delete(StatusPost s) throws DomainException {
		rep.delete(s);
	}
	
	@Override
	public List<StatusPost> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	@Override
	public StatusPost findByCodigo(StatusPost status, Empresa empresa) {
		return rep.findByCodigo(status, empresa);
	}

	@Override
	public StatusPost findByCodigo(String status, Empresa empresa) {
		StatusPost statusPost = new StatusPost();
		statusPost.setCodigo(status);
		return rep.findByCodigo(statusPost, empresa);
	}

}
