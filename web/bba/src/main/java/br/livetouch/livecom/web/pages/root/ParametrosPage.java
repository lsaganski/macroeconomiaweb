package br.livetouch.livecom.web.pages.root;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboTipoParametro;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.web.pages.LivecomRootPage;
import br.livetouch.livecom.web.pages.LogoutPage;
import net.livetouch.click.control.IdField;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.FieldSet;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class ParametrosPage extends LivecomRootPage {

	
	public List<Parametro> parametros;
	public String nome;
	public String valor;
	public ComboTipoParametro tTipo;

	public Long id;
	public Long empresaId;
	public String nomeEmpresa;
	
	public Form form = new Form();
	
	public ActionLink deleteCustomer = new ActionLink("delete", getMessage("excluir.label"), this, "onDeleteClick");
	public ActionLink editarParam = new ActionLink("editar", "Editar", this, "onEditarClick");
	
	    
	@Override
	public void onInit() {
		super.onInit();
        form();
        bootstrap_on = true;
        escondeChat = true;
        
        if(empresaId != null) {
        	Empresa empresa = empresaService.get(empresaId);
        	if(empresa != null) {
        		nomeEmpresa = empresa.getNome();
        	}
        } else {
        	setRedirect(EmpresasPage.class);
        	return;
        }
	}

	public void form() {
		
		FieldSet fieldSet = new FieldSet(getMessage("menu.parametros"));
		form.add(fieldSet);
		
		form.add(new IdField());
		
		TextField tnome  = new TextField("nome", getMessage("nome.label"), true);
		tnome.setAttribute("class", "input");
		form.add(tnome);
		
		TextField tValor  = new TextField("valor", getMessage("valor.label"));
		tValor.setAttribute("class", "input");
		form.add(tValor);

		tTipo = new ComboTipoParametro();
		tTipo.setAttribute("class", "input");
		form.add(tTipo);
	
		Submit tSalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tSalvar.setAttribute("class", "btn btn-primary min-width hidden");
		form.add(tSalvar);
	}
	
	public boolean cancelar() {
		refresh();
		refreshTable();
		return false;
	}

	public boolean salvar() {
		if(form.isValid()) {
			Parametro parametro = id != null ? parametroService.get(id) : new Parametro();
			form.copyTo(parametro);
			if(parametro.getId() == null) {
				Empresa empresa = empresaService.get(empresaId);
				parametro.setEmpresa(empresa);
			}
			
			try {
				parametroService.saveOrUpdate(parametro);
				setRedirect(ParametrosPage.class, "empresaId", empresaId.toString());
				return true;
			} catch (DomainException e) {
				form.setError(e.getMessage());
			}
		}
		return false;
	}

	public boolean onDeleteClick() throws DomainException {
		if(StringUtils.isNotEmpty(deleteCustomer.getValue())&& StringUtils.isNumeric(deleteCustomer.getValue())) {
			long id = deleteCustomer.getValueLong();
		    Parametro parametro =  parametroService.get(id);
		    if (parametro != null) {
		    	parametroService.delete(parametro); 
		    }
		    refreshTable();
		}
	     return true;
	}
	
	public boolean onEditarClick() throws DomainException {
		 long id = editarParam.getValueLong();
	     Parametro parametro =  parametroService.get(id);
	     form.copyFrom(parametro);
	     return true;
	}

	private void refreshTable() {
		try {
			if(getUserInfoVO() == null) {
				setRedirect(LogoutPage.class);
				return;
			}
			
			if(empresaId == null) {
				setRedirect(EmpresasPage.class);
				return;
			}
			
			Empresa e = empresaService.get(empresaId);
			parametros = parametroService.findAllParametros(e);
		} catch (DomainException e) {
			form.setError(e.getMessage());
			return;
		}

	}
	
	@Override
	public void onRender() {
		super.onRender();
		refreshTable();
	}
}

