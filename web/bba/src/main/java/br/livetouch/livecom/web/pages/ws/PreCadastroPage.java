package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.ComboFuncao;
import br.infra.web.click.DateField;
import br.infra.web.click.RadioGenero;
import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.livecom.domain.vo.TagVO;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class PreCadastroPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	public Long id;
	private TextField tMode;
	private TextField tNome;
	private TextField tEmail;
	private TextField tFixo;
	private TextField tCelular;
	private ComboFuncao tCargo;
	private DateField tNascimento;
	private RadioGenero tGenero;
	private LongField tId;
	private UsuarioLoginField tUser;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tId = new LongField("id", true));
		form.add(tUser = new UsuarioLoginField("login", true, usuarioService, getEmpresa()));
		
		form.add(tNome = new TextField("nome"));
		form.add(tEmail = new TextField("email"));
		form.add(tFixo = new TextField("telefoneFixo"));
		form.add(tCelular = new TextField("telefoneCelular"));
		form.add(tNascimento = new DateField("dataNasc"));
		form.add(tCargo = new ComboFuncao(funcaoService, getUserInfo()));
		form.add(tGenero = new RadioGenero());

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));

		form.add(tMode = new TextField("mode"));
		tMode.setValue("json");

		form.add(new Submit("cadastrar"));

	}

	@Override
	protected Object execute() throws DomainException {
		if (id != null) {
			try {
				Usuario u = tUser.getEntity();
				if (u == null) {
					return new MensagemResult("NOK", "Usuário não encontrado");
				}
				
				form.copyTo(u);
				u.setPreCadastro(false);
				u.setPrimeiroAcesso(true);
				usuarioService.saveOrUpdate(getUserInfo(),u);
	
				if (isWsVersion3()) {
					return new MensagemResult("OK", "Dados atualizados com sucesso");
				}
	
			} catch (DomainException e) {
				return new MensagemResult("NOK", e.getMessage());
			}

			return new MensagemResult("OK", "Dados atualizados com sucesso");

		}
		
		return new MensagemResult("NOK", "Usuario não encontrado");
		
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("post", PostVO.class);
		x.alias("arquivo", FileVO.class);
		x.alias("grupo", GrupoVO.class);
		x.alias("tags", TagVO.class);
		super.xstream(x);
	}
}
