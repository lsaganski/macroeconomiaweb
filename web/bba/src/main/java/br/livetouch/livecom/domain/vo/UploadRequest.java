package br.livetouch.livecom.domain.vo;

import java.util.List;

import org.apache.commons.fileupload.FileItem;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;

public class UploadRequest {

	public Empresa empresa;
	
	// id para update no arquivo
	public Long id;
	public List<Long> thumbIds;
	
	// info da request
	public Usuario usuario;
	public String dir;
	public String descricao;
	public String url;
	
	// Arquivo
	public String fileBase64;
	public String fileName;
	public FileItem fileItem;
	public String fileUrl;
	public String dimensao;
	public Boolean destaque;
	
	// FKs do arquivo
	public Post post;
	public Comentario comentario;
	public Mensagem mensagem;
	public String tagsString;
	public Long destaqueAtual;


	// crop
    public Integer eixoX;
    public Integer eixoY;
    public Integer width;
    public Integer height;

    public Integer angle;

    public Integer ordem;
    public boolean resize;

}