package br.livetouch.livecom.jobs;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.LogTransacao;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.LogAuditoriaService;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.email.EmailFactory;
import br.livetouch.livecom.email.EmailManager;
import br.livetouch.livecom.jobs.info.LogAuditoriaJob;
import br.livetouch.livecom.jobs.info.LogSistemaJob;
import br.livetouch.livecom.jobs.info.LogTransacaoJob;

@Service
public class LivecomJob extends SpringJob {

	protected static final Logger log = Log.getLogger(LivecomJob.class);
	private static boolean running = false;

	@Autowired
	protected LogService logService;
	
	@Autowired
	protected PostService postService;
	
	@Autowired
	protected LogAuditoriaService auditoriaSistemaService;
	
	@Autowired
	protected EmpresaService empresaService;
	
	@Autowired
	protected UsuarioService usuarioService;
	private Empresa empresa;
	
	@Override
	protected void execute(Map<String, Object> params) throws Exception {
		saveLogsTransacao();
	}
	
	private void saveLogsTransacao() {
		if (running) {
			log("LivecomJob already running.");
			return;
		}
		
		running = true;
		
		try {
			if(postService != null) {
				int count = postService.publicar();
				if(count > 0) {
					log.debug("LivecomJob: [" + count + "] posts publicados");
				}
				
				count = postService.expirar();
				if(count > 0) {
				}
				log.debug("LivecomJob: [" + count + "] posts expirados");
				
				// log de transacao
				saveLogTransacao();

				// log sistema
				saveLogSistema();
				
				// auditoria sistema
				saveAuditoria();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(), e);
		} finally {
			running = false;
		}
	}

	private void saveLogSistema() throws IOException, EmailException {
		List<LogSistema> systemLogs = LogSistemaJob.getInstance().getListAndDelete();			
		if(systemLogs != null && systemLogs.size() > 0) {
			log.debug("Salvando auditoria: " + systemLogs.size());
			for (LogSistema log : systemLogs) {
				logService.saveOrUpdate(log, log.getEmpresa());
				
				ParametrosMap params = ParametrosMap.getInstance();
				if(params.getBoolean(Params.LOG_SEND_EMAIL_ON, false) && log.isErro()) {
					sendMail(log, params);
				}
				
			}
		}
	}

	private void saveAuditoria() {
		List<LogAuditoria> audits = LogAuditoriaJob.getInstance().getListToSave();			
		if(audits != null && audits.size() > 0) {
			log.debug("Salvando auditoria: " + audits.size());
			for (LogAuditoria audit : audits) {
				if(audit.getEmpresa() != null){
					empresa = empresaService.get(audit.getEmpresa().getId());
					Usuario u = usuarioService.get(audit.getUsuario().getId());
					audit.setEmpresa(empresa);
					audit.setUsuario(u);
					if(u != null && empresa != null){
						auditoriaSistemaService.saveOrUpdate(audit);
					}
				}
			}
		}
	}

	private void saveLogTransacao() {
		List<LogTransacao> logs = LogTransacaoJob.getInstance().getListAndDelete();			
		if(logs != null && logs.size() > 0) {
			log.debug("Salvando logs: " + logs.size());
			
			for (LogTransacao logTransacao : logs) {
				Empresa empresa = logTransacao.getEmpresa();
				logService.saveOrUpdate(logTransacao, empresa);
			}
		}
	}

	private void sendMail(LogSistema log, ParametrosMap params) throws IOException, EmailException {
		String to = params.get(Params.LOG_SEND_EMAIL_TO, "juillianlee@livetouch.com.br");
		String subject = params.get(Params.LOG_SEND_EMAIL_SUBJECT, "Exception Livecom");
		EmailManager emailManager = EmailFactory.getEmailManager();
		emailManager.sendEmail(to, subject, log.getMsg() + "\n\n" + log.getExceptionString(), false);
	}
	
	private void log(String string) {
		log.debug(string);
	}
}
