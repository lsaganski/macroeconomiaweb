package br.livetouch.livecom.domain.repository.impl;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.repository.ArquivoThumbRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class ArquivoThumbRepositoryImpl extends StringHibernateRepository<ArquivoThumb> implements ArquivoThumbRepository {

	public ArquivoThumbRepositoryImpl() {
		super(ArquivoThumb.class);
	}

}