package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.CampoUsersVO;
import br.livetouch.livecom.domain.vo.CamposMap;
import br.livetouch.livecom.domain.vo.ProfileVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ProfilePage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private UsuarioLoginField tUser;
	public CamposMap campos;
	
	
	@Override
	public void onInit() {
		super.onInit();
		
		campos = getCampos();

		form.setMethod("post");
		form.add(tUser = new UsuarioLoginField("user_id", true, usuarioService,getEmpresa()));
		
		TextField tJsonFormat = null;
		form.add(tJsonFormat = new TextField("json.format"));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		TextField tMode = null;
		form.add(tMode = new TextField("mode"));
		tMode.setValue("json");
		
		tJsonFormat.setValue("1");

		tUser.setFocus(true);

		form.add(new Submit("consultar"));
		
		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			
//			String buildType = getbuildType();
			
			Usuario user = tUser.getEntity();
			
			String buildType = getContext().getRequest().getParameter("buildType");
			ProfileVO p = ProfileVO.create(user, usuarioService, buildType, campos);
			
			return p;
		}
		
		return new MensagemResult("NOK","Erro ao buscar os dados.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("profile", ProfileVO.class);
		x.alias("campo", CampoUsersVO.class);
		super.xstream(x);
	}
}
