package br.livetouch.livecom.web.listener;

import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import br.infra.util.Log;
import br.livetouch.livecom.chatAkka.AkkaChatServer;
import br.livetouch.livecom.domain.CategoriasMap;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.service.ApplicationService;
import br.livetouch.livecom.domain.service.CampoService;
import br.livetouch.livecom.domain.service.CategoriaPostService;
import br.livetouch.livecom.domain.service.ParametroService;
import br.livetouch.livecom.domain.vo.CamposMapCache;
import br.livetouch.livecom.jobs.SpringJob;
import br.livetouch.livecom.rest.oauth.ConsumerKeyMap;

/**
 * @author Ricardo Lecheta
 */
@Controller
public class StartServerListener implements ServletContextListener {
	private static Logger log = Logger.getLogger(StartServerListener.class);

	@Override
	public void contextInitialized(final ServletContextEvent servletContext) {
		final WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext.getServletContext());
		log.debug("contextInitialized");
		parametros(appContext);

		ParametrosMap params = ParametrosMap.getInstance();
		if (params != null && params.isChatOn()) {
			Livecom.getInstance().setChatOn(true);
			AkkaChatServer.start(appContext);
		} else {
			log.debug("Chat não foi inicializado. Para iniciar ele configure o parametro chat_on");
		}

		log.debug("contextInitialized OK");
	}

	protected void parametros(final WebApplicationContext appContext) {
		SpringJob aux = new SpringJob() {
			@Override
			protected void execute(Map<String, Object> params) {
				ParametroService parametroService = (ParametroService) appContext
						.getBean(appContext
								.getBeanNamesForType(ParametroService.class)[0]);
				ParametrosMap parametros = parametroService.init();
				log.debug("Parametros carregados: " + parametros);

				CampoService campoService = (CampoService) appContext
						.getBean(appContext
								.getBeanNamesForType(CampoService.class)[0]);
				CamposMapCache campos = campoService.init(null);
				log.debug("Campos carregados: " + campos);
				
				boolean subcategorias = parametros.getBoolean(Params.SUBCATEGORIAS_ON, false);
				if(subcategorias) {
					CategoriaPostService categoriaService = (CategoriaPostService) appContext
							.getBean(appContext
									.getBeanNamesForType(CategoriaPostService.class)[0]);
					CategoriasMap categorias = categoriaService.init();
					log.debug("Categorias carregadas: " + categorias);
				}
				
				// OAUTH
				ApplicationService applicationService = (ApplicationService) appContext
						.getBean(appContext
								.getBeanNamesForType(ApplicationService.class)[0]);
				ConsumerKeyMap keys = applicationService.init();
				log.debug("Keys carregadas: " + keys.size());

			}
		};
		aux.setApplicationContext(appContext);
		try {
			aux.executeInternal(null);
		} catch (JobExecutionException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent context) {
		try {
			log.debug("contextDestroyed");

			AkkaChatServer.stop();

		} catch (Exception e) {
			Log.e(e.getMessage(), e);
		}
	}
}
