package br.livetouch.livecom.connector.login;

import java.io.IOException;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import br.infra.util.HttpHelper;
import br.infra.util.Log;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.exception.SenhaInvalidaException;
import net.livetouch.tiger.ddd.DomainException;

/**

 http://localhost:8080/livecom/rest/v1/loginFake?id=2&senha=123
 
{
  "status": "OK",
  "message": "Login OK"
}
 * 
 * @author rlech
 *
 */
@Service
public class HttpLoginConnector implements LoginConnector {
	protected static final Logger log = Log.getLogger(LoginConnector.class);

	public boolean validate(Usuario user, String pwd) throws SenhaInvalidaException {
		ParametrosMap params = ParametrosMap.getInstance(user.getEmpresa());
		String url = params.get("login.connector.http.url","http://localhost:8080/livecom/rest/v1/loginFake");
		if(url == null) {
			throw new SenhaInvalidaException("Conector de login não configurado.");
		}
		
		HashMap<String, String> httpParams = new HashMap<String, String>();
		try {
			String idParam = params.get("login.connector.http.url.params.id","id");
			String loginParam = params.get("login.connector.http.url.params.login","login");
			String senhaParam = params.get("login.connector.http.url.params.senha","senha");
			
			httpParams.put(idParam, String.valueOf(user.getId()));
			httpParams.put(loginParam, user.getLogin());
			httpParams.put(senhaParam, pwd);

			String json = HttpHelper.doPost(url,httpParams);

			JSONObject jObj = new JSONObject(json);
			
			String status = jObj.optString("status");
			String message = jObj.optString("message");
			
			if("OK".equalsIgnoreCase(status)) {
				return true;
			}
			
			throw new SenhaInvalidaException(message);
			
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} catch (JSONException e) {
			log.error(e.getMessage(), e);
		}
		
		return false;
	}
	
	public static void main(String[] args) throws DomainException {
		Usuario user = new Usuario();
		user.setId(2L);
		HttpLoginConnector http = new HttpLoginConnector();
		boolean ok = http.validate(user, "ricardo");
		System.out.println(ok);
	}

}
