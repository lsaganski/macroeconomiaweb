package br.livetouch.livecom.domain.vo;


public class CacheVO {

	private String key;
	private String creationTime;
	private String hitCount;
	private String value;
	private String version;
	private String descricao;
	private String size;
	
	private Long id;
	
	public CacheVO(String key, String creationTime, String hitCount, String value, String version, Long id, String descricao, String size) {
		super();
		this.key = key;
		this.creationTime = creationTime;
		this.hitCount = hitCount;
		this.value = value;
		this.version = version;
		this.id = id;
		this.descricao = descricao;
		this.size = size;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getHitCount() {
		return hitCount;
	}

	public void setHitCount(String hitCount) {
		this.hitCount = hitCount;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

}