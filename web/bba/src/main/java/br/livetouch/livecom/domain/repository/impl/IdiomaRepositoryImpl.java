package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.repository.IdiomaRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class IdiomaRepositoryImpl extends StringHibernateRepository<Idioma> implements IdiomaRepository {

	public IdiomaRepositoryImpl() {
		super(Idioma.class);
	}
	
	@Override
	public Idioma findByCodigo(Idioma idioma) {
		StringBuffer sb = new StringBuffer("from Idioma where 1=1 ");
		
		if(StringUtils.isNotEmpty(idioma.getCodigo())) {
			sb.append(" and codigo like :codigo ");
		}
		
		if(idioma.getId() != null) {
			sb.append(" and id != :id");
		}

		Query q = createQuery(sb.toString());
		
		if(StringUtils.isNotEmpty(idioma.getCodigo())) {
			q.setParameter("codigo", idioma.getCodigo());
		}
		
		if(idioma.getId() != null) {
			q.setParameter("id", idioma.getId());
		}
		
		Idioma i = (Idioma) (q.list().isEmpty() ? null : q.list().get(0));
		return i;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Idioma> findByCodigos(String[] idiomas) {
		StringBuffer sb = new StringBuffer("from Idioma where 1=1 ");
		
		if(idiomas.length > 0) {
			sb.append(" and codigo in :codigos ");
		}
		
		Query q = createQuery(sb.toString());
		
		if(idiomas.length > 0) {
			q.setParameterList("codigos", idiomas);
		}
		
		return q.list();
	}

}