package br.livetouch.livecom.web.pages.training;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.CategTree;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.web.pages.HomePage;
import br.livetouch.livecom.web.pages.admin.LivecomPage;
import net.sf.click.control.Form;
import net.sf.click.extras.tree.Tree;
import net.sf.click.extras.tree.TreeNode;

@Controller
@Scope("prototype")
public class TrainingPage extends LivecomPage {
	
	public static final String TEMPLATE = TrainingLogadoPage.TEMPLATE;

	public List<CategoriaPost> categsParent;
	public CategTree tree;
	public TreeNode treeNode;
	public CategoriaPost categoriaPost;
	
	@Override
	public boolean onSecurityCheck() {
		if(!isBuildBJJ()) {
			// livecom
			setRedirect(HomePage.class);
			return onSecurityCheckNotOk();
		}
		return super.onSecurityCheck();
	}
	
	protected void redirectLogin() {
		br.livetouch.livecom.web.pages.training.LoginPage.redirect(this);
	}

	@Override
	public String getTemplate() {
		return TEMPLATE;
	}
	
	@Override
	public void onInit() {
		super.onInit();
		
		buildTree();
	}
	
	public void buildTree() {
		categsParent = categoriaPostService.findAllParent(getUsuario().getEmpresa());

		// Create the tree and tree model and add it to the page
		tree = new CategTree("tree", categoriaPostService, getEmpresa());
		tree.build(getContext());
		tree.setJavascriptEnabled(true);

		String selectId = getContext().getRequestParameter(Tree.SELECT_TREE_NODE_PARAM);
		String expandId = getContext().getRequestParameter(Tree.EXPAND_TREE_NODE_PARAM);
		if (selectId == null && expandId == null) {
			return;
		}

		TreeNode node = null;
		if (selectId != null) {
			node = tree.find(selectId);
		} else {
			node = tree.find(expandId);
		}
		treeNode = node;
		
		long id = NumberUtils.toLong(treeNode.getId(),0);
		categoriaPost = categoriaPostService.get(id);
	}
	
	protected void setTrainingStyle(Form form) {
		TrainingUtil.setTrainingStyle(form);
	}
}
