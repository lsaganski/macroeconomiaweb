package br.livetouch.livecom.web.pages.training;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Faq;

/**
 * 
 */
@Controller
@Scope("prototype")
public class FaqPage extends TrainingPage {

	@Override
	public void onInit() {
		super.onInit();
		
		List<Faq> perguntas = faqService.findAll();

		addModel("perguntas", perguntas);
	}
}
