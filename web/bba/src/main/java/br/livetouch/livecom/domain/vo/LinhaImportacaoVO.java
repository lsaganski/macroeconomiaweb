package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.livetouch.livecom.domain.LinhaImportacao;

public class LinhaImportacaoVO implements Serializable {

	private static final long serialVersionUID = 325325401699685573L;
	private String[] headers;
	private List<String[]> linhas;
	private List<String> linhaErro = new ArrayList<String>();

	public LinhaImportacaoVO(String header, List<LinhaImportacao> linhas) {
		header = String.format("LINHA;%s", header);
		this.setHeaders(parseHeader(header));
		this.setLinhas(parseLinhas(linhas));
	}

	private List<String[]> parseLinhas(List<LinhaImportacao> list) {
		List<String[]> retorno = new ArrayList<>();
		for (LinhaImportacao l : list) {
			String linha = String.format("%s;%s", l.getNumeroLinha(), l.getLinha()) ;
			String[] colunas = linha.split(";");
			linhaErro.add(l.getMensagem());
			retorno.add(colunas);
		}
		return retorno;
	}

	private String[] parseHeader(String header) {
		String[] split = StringUtils.split(header, ";");
		return split;
	}

	public List<String[]> getLinhas() {
		return linhas;
	}

	public void setLinhas(List<String[]> linhas) {
		this.linhas = linhas;
	}

	public String[] getHeaders() {
		return headers;
	}

	public void setHeaders(String[] headers) {
		this.headers = headers;
	}

	public List<String> getLinhaErro() {
		return linhaErro;
	}

	public void setLinhaErro(List<String> linhaErro) {
		this.linhaErro = linhaErro;
	}

}