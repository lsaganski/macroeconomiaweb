package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.utils.DateUtils;

public class RelatorioEnvioPostVO implements Serializable {

	private static final long serialVersionUID = 1524516747642576158L;
	
	private Long id;
	private Long idEntity;
	private String data;
	private String dataUpdate;
	private String dataPublicacao;
	private String acao;
	private String user;
	private String userUpdate;
	private String mensagem;
	private String titulo;
	private String tituloOriginal;
	private String conteudoOriginal;
	private String categorias;
	private String grupos;
	private String categoriasUpdate;
	private String gruposUpdate;

	public RelatorioEnvioPostVO(LogAuditoria update, LogAuditoria first) {
		this.id = update.getId();
		this.idEntity = update.getIdEntity();
		this.acao = update.getAcao().name();
		this.data = DateUtils.toString(update.getData(), "dd/MM/yyyy");
		if(first != null) {
			this.titulo = StringUtils.defaultString(update.getValue2());
			this.mensagem = StringUtils.defaultString(update.getValue3());
			this.dataUpdate = StringUtils.defaultString(update.getValue5());
			this.setDataPublicacao(StringUtils.defaultString(first.getValue4()));
			this.setTituloOriginal(StringUtils.defaultString(first.getValue2()));
			this.setConteudoOriginal(StringUtils.defaultString(first.getValue3()));
			this.categorias = StringUtils.defaultString(first.getValue6());
			this.grupos = StringUtils.defaultString(first.getValue7());
			this.user = first.getUsuario() != null ? first.getUsuario().getLogin() : StringUtils.defaultString(first.getValue8());
			this.categoriasUpdate = StringUtils.defaultString(update.getValue9());
			this.gruposUpdate = StringUtils.defaultString(update.getValue10());
			this.userUpdate = StringUtils.defaultString(update.getValue11());
		} else {
			this.dataPublicacao = StringUtils.defaultString(update.getValue4());
			this.setTituloOriginal(StringUtils.defaultString(update.getValue2()));
			this.setConteudoOriginal(StringUtils.defaultString(update.getValue3()));
			this.categorias = StringUtils.defaultString(update.getValue6());
			this.grupos = StringUtils.defaultString(update.getValue7());
			this.user = update.getUsuario() != null ? update.getUsuario().getLogin() : StringUtils.defaultString(update.getValue8());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}


	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDataUpdate() {
		return dataUpdate;
	}

	public void setDataUpdate(String dataUpdate) {
		this.dataUpdate = dataUpdate;
	}

	public String getDataPublicacao() {
		return dataPublicacao;
	}

	public void setDataPublicacao(String dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}

	public String getTituloOriginal() {
		return tituloOriginal;
	}

	public void setTituloOriginal(String tituloOriginal) {
		this.tituloOriginal = tituloOriginal;
	}

	public String getConteudoOriginal() {
		return conteudoOriginal;
	}

	public void setConteudoOriginal(String conteudoOriginal) {
		this.conteudoOriginal = conteudoOriginal;
	}

	public String getCategorias() {
		return categorias;
	}

	public void setCategorias(String categorias) {
		this.categorias = categorias;
	}

	public String getGrupos() {
		return grupos;
	}

	public void setGrupos(String grupos) {
		this.grupos = grupos;
	}

	public String getCategoriasUpdate() {
		return categoriasUpdate;
	}

	public void setCategoriasUpdate(String categoriasUpdate) {
		this.categoriasUpdate = categoriasUpdate;
	}

	public String getGruposUpdate() {
		return gruposUpdate;
	}

	public void setGruposUpdate(String gruposUpdate) {
		this.gruposUpdate = gruposUpdate;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUserUpdate() {
		return userUpdate;
	}

	public void setUserUpdate(String userUpdate) {
		this.userUpdate = userUpdate;
	}

	public Long getIdEntity() {
		return idEntity;
	}

	public void setIdEntity(Long idEntity) {
		this.idEntity = idEntity;
	}


}