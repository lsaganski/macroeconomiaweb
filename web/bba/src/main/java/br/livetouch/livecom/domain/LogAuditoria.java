package br.livetouch.livecom.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.enums.StatusTransacao;
import br.livetouch.livecom.jobs.info.LogAuditoriaJob;
import net.livetouch.extras.util.ExceptionUtil;

@Entity
public class LogAuditoria extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = -3984719551274634366L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "LOGAUDITORIA_SEQ")
	private Long id;
	
	private Date data;
	
	private AuditoriaAcao acao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario usuario;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;
	
	private AuditoriaEntidade entidade;
	
	@Column(columnDefinition = "clob")
	private String value1;
	
	@Column(columnDefinition = "clob")
	private String value2;
	
	@Column(columnDefinition = "clob")
	private String value3;
	
	@Column(columnDefinition = "clob")
	private String value4;
	
	@Column(columnDefinition = "clob")
	private String value5;

	@Column(columnDefinition = "clob")
	private String value6;

	@Column(columnDefinition = "clob")
	private String value7;

	@Column(columnDefinition = "clob")
	private String value8;

	@Column(columnDefinition = "clob")
	private String value9;

	@Column(columnDefinition = "clob")
	private String value10;

	@Column(columnDefinition = "clob")
	private String value11;

	@Column(columnDefinition = "clob")
	private String exception;
	
	@Column(columnDefinition = "clob")
	private String mensagem;
	
	private StatusTransacao status;

	private Long idEntity;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public AuditoriaAcao getAcao() {
		return acao;
	}
	
	public void setAcao(AuditoriaAcao acao) {
		this.acao = acao;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public AuditoriaEntidade getEntidade() {
		return entidade;
	}

	public void setEntidade(AuditoriaEntidade entidade) {
		this.entidade = entidade;
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getException() {
		return exception;
	}
	
	public void setException(String exception) {
		this.exception = exception;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public static LogAuditoria log(Usuario userInfo, JsonEntity jsonEntity, AuditoriaEntidade entidade, AuditoriaAcao acao, String mensagem) {
		return log(userInfo, jsonEntity, entidade, acao, mensagem, null, true);
	}
	
	public static LogAuditoria log(Usuario userInfo, JsonEntity jsonEntity, AuditoriaEntidade entidade, AuditoriaAcao acao, String mensagem, Throwable exception) {
		return log(userInfo, jsonEntity, entidade, acao, mensagem, exception, true);
	}

	public static LogAuditoria log(Usuario userInfo, JsonEntity jsonEntity, AuditoriaEntidade entidade, AuditoriaAcao acao, String mensagem, boolean autoSave) {
		return log(userInfo, jsonEntity, entidade, acao, mensagem, null, autoSave);
	}
	
	public static LogAuditoria log(Usuario userInfo, JsonEntity jsonEntity, AuditoriaEntidade entidade, AuditoriaAcao acao, String mensagem, Throwable exception, boolean autoSave) {
		if(userInfo == null) {
			return null;
		}
		
		Empresa empresa = userInfo.getEmpresa();
		if(empresa == null || empresa.getId() == null) {
			return null;
		}
		
		// Json como novo valor
		LogAuditoria log = new LogAuditoria();
		
		if(jsonEntity != null) {
			log.setValue1(jsonEntity.toJson());
			log.setIdEntity(jsonEntity.getId());
			log.setValue2(jsonEntity.getValue2());
			log.setValue3(jsonEntity.getValue3());
			log.setValue4(jsonEntity.getValue4());
			log.setValue8(jsonEntity.getValue8());
			log.setValue9(jsonEntity.getValue9());
			log.setValue10(jsonEntity.getValue10());
			log.setValue11(jsonEntity.getValue11());
			log.setValue5(jsonEntity.getValue5());
			log.setValue6(jsonEntity.getValue6());
			log.setValue7(jsonEntity.getValue7());
		}
		log.setData(new Date());
		log.setEmpresa(empresa);
		log.setUsuario(userInfo);
		log.setAcao(acao);
		log.setEntidade(entidade);
		log.setMensagem(mensagem);
		log.setStatus(StatusTransacao.OK);
		if(exception != null) {
			log.setStatus(StatusTransacao.ERRO);
			log.setException(ExceptionUtil.getStackTrace(exception));
		}
		
		if(autoSave) {
			log.finish();
		}
		
		return log;
	}

	public void setIdEntity(Long idEntity) {
		this.idEntity = idEntity;
	}
	
	public Long getIdEntity() {
		return idEntity;
	}

	public void finish() {
		// Buffer para LivecomJob
		LogAuditoriaJob.getInstance().add(this);
	}

	public StatusTransacao getStatus() {
		return status;
	}

	public void setStatus(StatusTransacao status) {
		this.status = status;
	}

	public String getValue3() {
		return value3;
	}

	public void setValue3(String value3) {
		this.value3 = value3;
	}

	public String getValue4() {
		return value4;
	}

	public void setValue4(String value4) {
		this.value4 = value4;
	}

	public String getValue5() {
		return value5;
	}

	public void setValue5(String value5) {
		this.value5 = value5;
	}

	public String getValue6() {
		return value6;
	}

	public void setValue6(String value6) {
		this.value6 = value6;
	}

	public String getValue7() {
		return value7;
	}

	public void setValue7(String value7) {
		this.value7 = value7;
	}

	public String getValue8() {
		return value8;
	}

	public void setValue8(String value8) {
		this.value8 = value8;
	}

	public String getValue9() {
		return value9;
	}

	public void setValue9(String value9) {
		this.value9 = value9;
	}

	public String getValue10() {
		return value10;
	}

	public void setValue10(String value10) {
		this.value10 = value10;
	}

	public String getValue11() {
		return value11;
	}

	public void setValue11(String value11) {
		this.value11 = value11;
	}

}