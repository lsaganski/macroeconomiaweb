package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.gson.Gson;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.livetouch.livecom.domain.vo.TagVO;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Tag extends JsonEntity{
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "TAG_SEQ")
	private Long id;

	private String nome;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tag_parent_id", nullable = true)
	private Tag tagParent;

	@OneToMany(mappedBy = "tagParent", fetch=FetchType.LAZY)
	@OrderBy(value = "id")
	@XStreamOmitField
	private Set<Tag> tags;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_create_id", nullable = true)
	private Usuario userCreate;
	private Date dateCreate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_update_id", nullable = true)
	private Usuario userUpdate;
	private Date dateUpdate;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public static List<Long> getIds(List<? extends net.livetouch.tiger.ddd.Entity> list) {
		if(list == null || list.size() == 0) {
			return null;
		}
		List<Long> ids = new ArrayList<Long>();
		for (net.livetouch.tiger.ddd.Entity e : list) {
			ids.add(e.getId());
		}
		return ids;
	}
	
	public Tag getTagParent() {
		return tagParent;
	}
	public Set<Tag> getTags() {
		return tags;
	}
	public void setTagParent(Tag tagParent) {
		this.tagParent = tagParent;
	}
	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}
	
	public boolean hasChildren() {
		return getTags().size() > 0;
	}
	
	public boolean hasParent() {
		return tagParent != null;
	}
	
	
	
	public Usuario getUserCreate() {
		return userCreate;
	}

	public void setUserCreate(Usuario userCreate) {
		this.userCreate = userCreate;
	}

	public Date getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Usuario getUserUpdate() {
		return userUpdate;
	}

	public void setUserUpdate(Usuario userUpdate) {
		this.userUpdate = userUpdate;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public String toStringAll() {
		return "Tag [id=" + id + ", nome=" + nome + ", tagParent=" + tagParent + ", tags=" + tags + ", userCreate=" + userCreate + ", dateCreate=" + dateCreate + ", userUpdate=" + userUpdate + ", dateUpdate=" + dateUpdate + "]";
	}

	@Override
	public String toString() {
		return id+": " +nome;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public String getCodigoDesc() {
//		return codigo != null ? codigo : id.toString();
		return id.toString();
	}

	@Override
	public String toJson() {
		TagVO vo = new TagVO();
		vo.setTag(this);
		return new Gson().toJson(vo);
	}
	
}
