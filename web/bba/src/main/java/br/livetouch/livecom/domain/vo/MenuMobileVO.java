package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Menu;

public class MenuMobileVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Long id;
	public String label;
	// Ex: "mural,notificações,meus_dados,usuarios,grupos,chat,configurations,sair"
	public String codigo;
	public String icone;
	public String descricao;
	public String parametro;
	public int ordem;
	public boolean menuDefault;
	public boolean divider;
	
	
	public MenuMobileVO(Menu m) {
		id = m.getId();
		label = m.getLabel();
		codigo = m.getLink();
		icone = m.getIcone();
		descricao = m.getDescricao();
		parametro = m.getParametro();
		ordem = m.getOrdem();
		menuDefault = m.isMenuDefault();
		divider = m.isDivider();
	}


	public static List<MenuMobileVO> createByList(List<Menu> menus) {
		List<MenuMobileVO> vos = new ArrayList<>();
		for (Menu menu : menus) {
			vos.add(new MenuMobileVO(menu));
		}
		return vos;
	}

}
