package br.livetouch.livecom.domain.vo;

import java.util.List;

import br.livetouch.spring.SearchInfo;

public class TagFilter extends SearchInfo {

	public String nome;
	public List<Long> notIds;
}