package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Historico;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;

@Repository
public interface HistoricoRepository extends net.livetouch.tiger.ddd.repository.Repository<Historico> {

	List<Historico> findAllByUser(Usuario u);

	List<Post> findAllPostsByUser(Usuario u, int page, int max);

	Historico findByUserPost(Usuario u, Post p);
	
}