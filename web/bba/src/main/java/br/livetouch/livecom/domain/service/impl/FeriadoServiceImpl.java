package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Feriado;
import br.livetouch.livecom.domain.repository.FeriadoRepository;
import br.livetouch.livecom.domain.service.FeriadoService;
import br.livetouch.livecom.domain.service.FormacaoService;
import br.livetouch.livecom.utils.DateUtils;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class FeriadoServiceImpl implements FeriadoService {
	protected static final Logger log = Log.getLogger(FormacaoService.class);

	@Autowired
	private FeriadoRepository rep;

	@Override
	public Feriado get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Feriado f, Empresa empresa) throws DomainException {
		f.setEmpresa(empresa);
		rep.saveOrUpdate(f);
	}

	@Override
	public List<Feriado> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	@Override
	public void delete(Feriado f) throws DomainException {
		rep.delete(f);
	}

	@Override
	public String csvFeriado(Empresa empresa) {
		List<Feriado> findAll = findAll(empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("CODIGO").add("DATA").add("NOME");
		for (Feriado a : findAll) {
			body.add(a.getCodigoDesc()).add(DateUtils.toString(a.getData())).add(a.getNome() != null ? a.getNome() : "");
			body.end();
		}
		return generator.toString();
	}

}
