package br.livetouch.livecom.domain.vo;

import java.io.File;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.Params;

public class UploadResponse {

	// Arquivo para salvar no banco.
	public Arquivo arquivo;
	public ArquivoThumb thumb;

	// Arquivo salvo no filesystem
	public File file;
	public String parametro;
}