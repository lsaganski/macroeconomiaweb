package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.List;

public class FuncoesVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public List<FuncaoVO> funcoes;
	public Long total;
	
}
