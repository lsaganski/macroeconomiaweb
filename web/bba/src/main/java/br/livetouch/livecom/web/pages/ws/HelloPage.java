package br.livetouch.livecom.web.pages.ws;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.HelloVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class HelloPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private TextField tLogin;
	
	public String login;
	public String senha;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tLogin = new TextField("login"));
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		tLogin.setFocus(true);
		
		form.add(new Submit("enviar"));

		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			
//			String login = tLogin.getValue();
//			String senha = tSenha.getValue();
			
			if(StringUtils.isEmpty(login)) {
				return new MensagemResult("NOK","Informe o login.");
			}
			
			Usuario usuario = usuarioService.login(login, senha);
			if(usuario == null) {
				return new MensagemResult("NOK","Login inválido.");
			}
			
			HelloVO h = new HelloVO();
			h.setNome(usuario.getNome());
			h.setCidade(usuario.getCidade().getNome());
			
			return h;
			
		}
		
		return new MensagemResult("NOK","Erro ao consultar o web service.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("hello", HelloVO.class);
		super.xstream(x);
	}
}
