package br.livetouch.livecom.rest.resource;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.vo.TagVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import br.livetouch.livecom.web.pages.ws.MensagemResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/tag")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class TagResource extends MainResource {
	protected static final Logger log = Log.getLogger(TagResource.class);
	
	@POST
	public Response create(Tag tag) {
		
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			//Validar se ja existe uma tag com o mesmo nome
			boolean existe = tagService.findByName(tag, getEmpresa()) != null;
			if(existe) {
				log.debug("Ja existe uma tag com este nome");
				return Response.ok(MessageResult.error("Ja existe uma tag com este nome")).build();
			}
			
			tagService.saveOrUpdate(getUserInfo(), tag);
			TagVO tagVO = new TagVO();
			tagVO.setTag(tag);
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", tagVO)).build();
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel efetuar o cadastro da tag")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@PUT
	public Response update(Tag tag) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			if(tag != null && tag.getId() != null) {
				
				//Validar se ja existe uma tag com o mesmo nome
				boolean existe = tagService.findByName(tag, getEmpresa()) != null;
				if(existe) {
					log.debug("Ja existe uma tag com este nome");
					return Response.ok(MessageResult.error("Ja existe uma tag com este nome")).build();
				}
				
				tagService.saveOrUpdate(getUserInfo(), tag);
			} else {
				log.debug("Não foi possivel atualizar o cadastro da tag");
				return Response.ok(MensagemResult.erro("Não foi possivel atualizar o cadastro da tag")).build();
			}
			
			TagVO tagVO = new TagVO();
			tagVO.setTag(tag);
			return Response.ok(MessageResult.ok("Tag atualiza com sucesso com sucesso", tagVO)).build();
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar a tag")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@GET
	public Response findAll() {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		List<Tag> tags = tagService.findAll(getEmpresa());
		List<TagVO> vos = TagVO.createByList(tags);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Tag tag = tagService.get(id);
		if(tag == null) {
			return Response.ok(MessageResult.error("Tag não encontrada")).build();
		}
		TagVO vo = new TagVO();
		vo.setTag(tag);
		return Response.ok().entity(vo).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Tag tag = tagService.get(id);
			if(tag == null) {
				return Response.ok(MessageResult.error("Tag não encontrada")).build();
			}
			tagService.delete(getUserInfo(), tag, false);
			return Response.ok().entity(MessageResult.ok("Tag deletada com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error(e.getMessage())).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir a Tag  " + id)).build();
		}
	}
	
	@GET
	@Path("/exportar")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response exportar() throws IOException {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Empresa empresa = getEmpresa();
		String csv = tagService.csvTag(empresa);
		
		String charset = ParametrosMap.getInstance(empresa).get(Params.EXPORTACAO_ARQUIVO_CHARSET, "ISO-8859-1");

		String attachament = "attachment; filename=" + URLEncoder.encode("tags.csv", "UTF-8");
		ResponseBuilder b = Response.status(Status.OK);
		b.encoding(charset);
		b.header("Content-Disposition", attachament);
		b.header("Content-Type", "application/octet-stream"+";charset="+charset);
		b.entity(csv);
		Response r = b.build();
		return r;
	}
	
//	@GET
//	@Path("/filtro")
//	public Response findByName(@BeanParam FiltroArea filtro) {
//		List<Area> areas = areaService.filterArea(filtro, getEmpresa());
//		List<AreaVO> vo = AreaVO.fromList(areas);
//		return Response.status(Status.OK).entity(vo).build();
//	}
}
