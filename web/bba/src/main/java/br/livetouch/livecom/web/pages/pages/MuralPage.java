package br.livetouch.livecom.web.pages.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboVisibilidade;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoGrupo;
import br.livetouch.livecom.domain.vo.CategoriaVO;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import net.sf.click.control.Form;

@Controller
@Scope("prototype")
public class MuralPage extends LivecomLogadoPage {
	public String clear;
	public Long tag;
	public Long categoria;
	public Long user_post_id;
	public Long grupo;
	
	public String filterNameSideBox;
	
	public List<CategoriaPost> categorias;
	public List<CategoriaVO> categoriasVO;
	public Usuario usuario;
	public Form form = new Form();
	

	public boolean showForm = true;
	public boolean isShowThumbCrop = true;
	

	public List<Grupo> gruposDefault;
	public List<CategoriaPost> categoriasDefault;
	public List<Tag> tags = new ArrayList<>();
	
	@Override
	public void onRender() {
		super.onRender();
		
		form.add(new ComboVisibilidade());
		this.web_layout_sideBox = ParametrosMap.getInstance(getEmpresa()).get("web.layout.sideBox", "0");
		
		Usuario user = getUsuarioMural();
		usuario = usuarioService.get(user.getId());
		categorias = categoriaPostService.findAll(getEmpresa());
		categoriasVO = CategoriaVO.create(categorias, getIdioma());
		categoriasVO = !categoriasVO.isEmpty() ? categoriasVO : null; 
		Map<Long, Grupo> gruposDefaultMap = new HashMap<>();
		
		Set<Grupo> userDefault = new HashSet<Grupo>();
		userDefault.addAll(grupoService.findAllDefault(usuario));
		List<Grupo> subgrupos = grupoService.findSubgruposInGrupos(userDefault);
		if(subgrupos != null && subgrupos.size() > 0) {
			userDefault.addAll(subgrupos);
		}
		
		gruposDefault = new ArrayList<>();

		if(grupo == null) {
			for (Grupo gUser : userDefault) {
				if (gUser.isPadrao()) {
					gruposDefaultMap.put(gUser.getId(), gUser);
				}
			}
		}
		
		gruposDefault = new ArrayList<Grupo>(gruposDefaultMap.values());
		categoriasDefault = categoriaPostService.findAllDefault(getEmpresa());
		categoriasDefault = !categoriasDefault.isEmpty() ? categoriasDefault : null; 
		
		if(tag != null) {
			Tag t = tagService.get(tag);
			if(t != null) {
				filterNameSideBox = t.getNome();
			}
			tags.add(t);
		} else if(categoria != null) {
			CategoriaPost c = categoriaPostService.get(categoria);
			if(c != null) {
				c.translate(usuario.getIdioma());
				filterNameSideBox = c.getNome();
			}
			categoriasDefault = new ArrayList<>();
			categoriasDefault.add(c);
		} else if(user_post_id != null) {
			Usuario u = usuarioService.get(user_post_id);
			if(u != null) {
				filterNameSideBox = u.getNome();
			}
		} else if(grupo != null) {
			Grupo g = grupoService.get(grupo);
			if(g != null) {
				GrupoUsuarios gu = usuarioService.getGrupoUsuario(getUsuario(), g);
				boolean tipoGrupoOn = getParametrosMap().getBoolean(Params.CADASTRO_GRUPO_TIPO_ON, false);
				if(tipoGrupoOn && !g.isGrupoVirtual()) {
					TipoGrupo tipoGrupo = g.getTipoGrupo();
					boolean isPrivado = tipoGrupo.equals(TipoGrupo.PRIVADO) || tipoGrupo.equals(TipoGrupo.PRIVADO_OPCIONAL);
					if(gu == null && isPrivado && !hasPermissao(ParamsPermissao.VISUALIZAR_TODOS_OS_GRUPOS)) {
						setRedirect(MuralPage.class);
						return;
					}
				}
				filterNameSideBox = g.getNome();
				if(g.isGrupoVirtual() && g.isAdmin(getUsuario())) {
					gruposDefaultMap.put(g.getId(), g);
				} else {
					for (Grupo gUser : userDefault) {
						if (gUser.isPadrao()) {
							gruposDefaultMap.put(gUser.getId(), gUser);
						}
					}
				}
				
				if(g.getCategoria() != null) {
					categoriasDefault = new ArrayList<>();
					categoriasDefault.add(g.getCategoria());
				}
			}
			gruposDefault = new ArrayList<Grupo>(gruposDefaultMap.values());
		}
		
		if(StringUtils.equals(clear, "true")) {
			getUserInfoVO().setSideboxTab(null);
		}
	}

	private void changePrimeiroAcesso() {
		Usuario userInfo = getUserInfo();
		if(userInfo != null && userInfo.isPrimeiroAcesso()) {
			userInfo.setPrimeiroAcesso(false);
			usuarioService.saveOrUpdate(userInfo);
			UserInfoVO userInfoVO = getUserInfoVO();
			userInfoVO.setPrimeiroAcesso(false);
			UserInfoVO.setHttpSession(getContext(),userInfoVO);
		}
	}

	protected Usuario getUsuarioMural() {
		return getUsuario();
	}
	
	@Override
	public String getTemplate() {
		ParametrosMap param = ParametrosMap.getInstance(getEmpresa());
		String on = param.get("border.muralv2.on", "0");
		if("1".equals(on)) {
			return "borders/mural.htm";
		}
		return super.getTemplate();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		changePrimeiroAcesso();
	}
}
