package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.MenuPerfil;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.repository.MenuPerfilRepository;
import br.livetouch.livecom.domain.service.MenuPerfilService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class MenuPerfilServiceImpl extends LivecomService<MenuPerfil> implements MenuPerfilService {
	@Autowired
	private MenuPerfilRepository rep;

	@Override
	public MenuPerfil get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(MenuPerfil c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public void delete(MenuPerfil c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public void saveList(List<MenuPerfil> menus) throws DomainException {
		for (MenuPerfil menuPerfil : menus) {
			rep.saveOrUpdate(menuPerfil);
		}
	}

	@Override
	public void removeIn(List<MenuPerfil> menus) throws DomainException {
		rep.removeIn(menus);
	}

	@Override
	public void removeNotIn(List<MenuPerfil> menus, Perfil perfil) throws DomainException {
		rep.removeNotIn(menus, perfil);
	}
	
	@Override
	public List<MenuPerfil> findPaiByPerfil(Perfil perfil, Empresa empresa, boolean in) {
		return rep.findPaiByPerfil(perfil, empresa, in);
	}

	@Override
	public List<MenuPerfil> findFilhoByPerfil(Perfil perfil, Empresa empresa, boolean in) {
		return rep.findFilhoByPerfil(perfil, empresa, in);
	}

	@Override
	public List<MenuPerfil> findByPerfil(Perfil p, Empresa empresa) {
		return rep.findByPerfil(p, empresa);
	}

}
