package br.livetouch.livecom.domain.exception;

import br.infra.util.MessageUtil;
import net.livetouch.tiger.ddd.DomainException;

public class DataInvalidaException extends DomainException {
	
	private static final long serialVersionUID = 1L;

	public DataInvalidaException(String msg) {
		super(msg);
	}

	@Override
	public String getMessage() {	

		String s = MessageUtil.getString("exception.data.invalida");

		return s;
	}

}
