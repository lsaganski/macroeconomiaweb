package br.livetouch.livecom.jobs;


import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import br.livetouch.spring.SpringHelper;

/**
 * 
 */
public abstract class SpringJob extends QuartzJobBean {
	protected static Logger log = Logger.getLogger(SpringJob.class);
	
	@Autowired ApplicationContext applicationContext;

	public void executeInternal(JobExecutionContext context) throws JobExecutionException {

		Map<String, Object> params = context != null ? context.getMergedJobDataMap() : new HashMap<String,Object>();

		SpringHelper helper = new SpringHelper();
		try {
			
			SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);

			helper.openSession(applicationContext);

			try {
				execute(params);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			} finally {
				
			}
		}
		catch (RuntimeException e) {

			log.error(getClass().getName() + ": falha ao executar o job, nao foi possivel criar a sessao: " + e.getMessage());

			log.error(e.getMessage(),e);
		}
		finally {
			try {
				helper.closeSession(applicationContext);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
//			log.debug(getClass().getName() + ": job finalizado as " + new Date());
		}
	}

	protected abstract void execute(Map<String, Object> params) throws Exception;

	public void setApplicationContext(WebApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
	
	public Object getBean(Class<?> cls) {
		String[] beansArray = applicationContext.getBeanNamesForType(cls);
		if(beansArray == null || beansArray.length == 0) {
			throw new RuntimeException("SpringJob: Spring nao foi inicializado, nao foi possivel obter bean: " + cls);
		}
		Object o =  applicationContext.getBean(beansArray[0]);
		return o;
	}
}
