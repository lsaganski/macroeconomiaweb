package br.livetouch.livecom.rest.domain;

import java.security.Principal;

import br.livetouch.livecom.domain.Usuario;

public class UserPrincipal implements Principal {
	private Usuario usuario;
	
	public UserPrincipal(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Long getId() {
		return usuario.getId();
	}
	
	@Override
	public String getName() {
		return usuario.getNome();
	}
	
	public String getPermisao() {
		return usuario.getPermissao().getNome();
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
