package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.DateUtils;

public class RelatorioNotificationsVO implements Serializable {
	
	private static final long serialVersionUID = 1524516347642576158L;
	private Long id;
	private String data;
	private String titulo;
	private String texto;
	private String dataPush;
	private Long idAutor;
	private String nomeAutor;
	private String loginAutor;
	private int totalUsers;
	private String tipo;
	private Long postId;
	private List<NotificationVO> notifications;
	private boolean sendPush;
	
	public RelatorioNotificationsVO(Notification notification) {
		this.id = notification.getId();
		this.titulo = notification.getTitulo();
		this.texto = notification.getTexto();
		this.data = DateUtils.toString(notification.getData(), "dd/MM/yyyy HH:mm");
		this.dataPush = DateUtils.toString(notification.getDataPush(), "dd/MM/yyyy HH:mm");
		Usuario u = notification.getUsuario();
		this.idAutor = u.getId();
		this.nomeAutor = u.getNome();
		this.loginAutor = u.getLogin();
		this.sendPush = notification.isSendPush();
		this.postId = notification.getPost() != null ? notification.getPost().getId() : null;
		this.setTipo(notification.getTipo().toString());
		Set<NotificationUsuario> notifications = notification.getNotifications();
		if(notifications != null) {
			this.totalUsers = notifications.size();
		}
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getData() {
		return data;
	}


	public void setData(String data) {
		this.data = data;
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public String getTexto() {
		return texto;
	}


	public void setTexto(String texto) {
		this.texto = texto;
	}


	public String getDataPush() {
		return dataPush;
	}


	public void setDataPush(String dataPush) {
		this.dataPush = dataPush;
	}


	public Long getIdAutor() {
		return idAutor;
	}


	public void setIdAutor(Long idAutor) {
		this.idAutor = idAutor;
	}


	public String getNomeAutor() {
		return nomeAutor;
	}


	public void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}


	public String getLoginAutor() {
		return loginAutor;
	}


	public void setLoginAutor(String loginAutor) {
		this.loginAutor = loginAutor;
	}


	public int getTotalUsers() {
		return totalUsers;
	}


	public void setTotalUsers(int totalUsers) {
		this.totalUsers = totalUsers;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public void setNotifications(List<NotificationVO> list) {
		this.notifications = list;
	}
	
	public List<NotificationVO> getNotifications() {
		return notifications;
	}

	public boolean isSendPush() {
		return sendPush;
	}

	public void setSendPush(boolean sendPush) {
		this.sendPush = sendPush;
	}
}
