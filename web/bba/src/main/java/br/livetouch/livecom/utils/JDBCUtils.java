package br.livetouch.livecom.utils;

import java.sql.Connection;
import java.sql.SQLException;

public class JDBCUtils {

	public static void printConnectionInfo(Connection c) {
		try {
			System.out.println("Auto commit: " + c.getAutoCommit());
			switch (c.getTransactionIsolation()) {
			case Connection.TRANSACTION_NONE:
				System.out.println("Transaction Isolation: TRANSACTION_NONE");
				break;
			case Connection.TRANSACTION_READ_UNCOMMITTED:
				System.out.println("Transaction Isolation: TRANSACTION_READ_UNCOMMITTED");
				break;
			case Connection.TRANSACTION_READ_COMMITTED:
				System.out.println("Transaction Isolation: TRANSACTION_READ_COMMITTED");
				break;
			case Connection.TRANSACTION_REPEATABLE_READ:
				System.out.println("Transaction Isolation: TRANSACTION_REPEATABLE_READ");
				break;
			case Connection.TRANSACTION_SERIALIZABLE:
				System.out.println("Transaction Isolation: TRANSACTION_SERIALIZABLE");
				break;
			default:
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
