package br.livetouch.livecom.web.pages.cadastro;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

//import br.livetouch.livecom.domain.Idioma;
//import br.livetouch.livecom.domain.service.IdiomaService;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class IdiomasPage extends CadastrosPage {

//	@Autowired
//	IdiomaService idiomaService;
//
//	public Form form = new Form();
//	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
//	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
//	public String msgErro;
//	public Long id;
//	
//	public boolean escondeChat = true;
//
//	public int page;
////	public int lastPage;
//
//	public List<Idioma> idiomas;
//
//	public Idioma idioma;
//
//
//	@Override
//	public void onInit() {
//		super.onInit();
//		
//		form();
//	}
//	
//	
//	public void form(){
//		form.add(new IdField());
//
//		TextField tNome = new TextField("nome", getMessage("nome.label"));
//		tNome.setAttribute("autocomplete", "off");
//		tNome.setAttribute("rel", getMessage("idiomas.adicionar.label"));
//		tNome.setAttribute("class", "mascara");
//		form.add(tNome);
//
//		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
//		tExportar.setAttribute("class", "tooltip botao export");
//		form.add(tExportar);
//		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
//		tsalvar.setAttribute("class", "botao salvar btSalvar");
//		form.add(tsalvar);
//		
//		form.add(new Submit("novo", getMessage("novo.label"), this, "novo"));
//
//	}
//
//	@Override
//	public void onGet() {
//		super.onGet();
//		
//		if (id != null) {
//			idioma = idiomaService.get(id);
//		} else {
//			idioma = new Idioma();
//		}
//		
//		if(idioma != null) {
//			form.copyFrom(idioma);
//		}
//	}
//
//	public boolean salvar() {
//		if (form.isValid()) {
//			try {
//				idioma = id != null ? idiomaService.get(id) : new Idioma();
//				form.copyTo(idioma);
//
//				idiomaService.saveOrUpdate(getUsuario(),idioma);
//
//				setMessageSesssion(getMessage("msg.idiomas.salvar.sucess", idioma.getNome()),getMessage("msg.idiomas.salvar.label.sucess"));
//				setRedirect(IdiomasPage.class);
//
//				return false;
//
//			} catch (DomainException e) {
//				form.setError(e.getMessage());
//			} catch (Exception e) {
//				logError(e.getMessage(),e);
//				form.setError(e.getMessage());
//			}
//		}
//		return true;
//	}
//
//	public boolean novo() {
//		setRedirect(IdiomasPage.class);
//		return true;
//	}
//
//	public boolean editar() {
//		Long id = editLink.getValueLong();
//		idioma = idiomaService.get(id);
//		form.copyFrom(idioma);
//		
//		return true;
//	}
//
//	public boolean deletar() {
//		try {
//			Long id = deleteLink.getValueLong();
//			Idioma idioma = idiomaService.get(id);
//			idiomaService.delete(getUsuario(),idioma, false);
//			setRedirect(IdiomasPage.class);
//			setMessageSesssion(getMessage("msg.idiomas.excluir.sucess"),getMessage("msg.idiomas.excluir.label.sucess"));
//		} catch (DomainException e) {
//			logError(e.getMessage(),e);
//			this.msgErro = e.getMessage();
//		} catch (ConstraintViolationException e) {
//			logError(e.getMessage(),e);
//			this.msgErro = getMessage("msg.idiomas.excluir.error");
//		} catch (Exception e) {
//			logError(e.getMessage(),e);
//			this.msgErro = getMessage("msg.idiomas.excluir.error");
//		}
//		return true;
//	}
//
//	@Override
//	public void onRender() {
//		super.onRender();
//
//		idiomas = idiomaService.findAll(getEmpresa());
//
//		// Count(*)
//		int pageSize = 10;
//		long count = idiomas.size();
//
//		createPaginator(page, count, pageSize);
//	}
//	
//	public boolean exportar(){
//		String csv = idiomaService.csvIdioma(getEmpresa());
//		download(csv, "idiomas.csv");
//		return true;
//	}
}
