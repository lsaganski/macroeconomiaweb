package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

public class ConfiguracoesVO implements Serializable{
	private static final long serialVersionUID = 2546920667628177621L;
	public Boolean pushOn;
	public Boolean mostraMuralOn;
	public Long userId;
	public Long grupoId;
}
