package br.livetouch.livecom.web.pages.admin;


import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Evento;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Table;
import net.sf.click.extras.control.LinkDecorator;

/**
 * 
 */
@Controller
@Scope("prototype")
public class EventosPage extends LivecomAdminPage {

	public Table table = new Table();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public Link agendaLink = new Link("agenda", getMessage("agenda.label"), this, "agenda");
	public String msgErro;
	public Long id;

	public int page;

	public Evento evento;

	public List<Evento> Eventos;
	
	@Override
	public void onInit() {
		super.onInit();

		table();

		if (id != null) {
			evento = eventoService.get(id);
		} else {
			evento = new Evento();
		}
	}
	
	private void table() {
		Column c = null;
		
		c = new Column("nome", getMessage("nome.label"));
		c.setWidth("200px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("data", getMessage("data.label"));
		c.setFormat("{0,date,dd/MM/yyyy}"); 
		c.setWidth("20px");
		c.setTextAlign("center");
		table.addColumn(c);

		ActionLink[] links = new ActionLink[] { editLink, deleteLink, agendaLink };
		c = new Column("detalhes", getMessage("detalhes.label"));
		c.setTextAlign("center");
		c.setWidth("100px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean novo() {
		setRedirect(EventosPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		evento = eventoService.get(id);
		setRedirect(EventoPage.class,"id",String.valueOf(id));
		
		return true;
	}
	
	public boolean agenda() {
		Long id = agendaLink.getValueLong();
		setRedirect(AgendasPage.class,"evento",String.valueOf(id));
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Evento e = eventoService.get(id);
			eventoService.delete(e);
			setRedirect(getClass());
			setFlashAttribute("msg", getMessage("msg.evento.excluir.sucess"));
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.evento.excluir.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		Eventos = eventoService.findAllOrderBy();

		// Count(*)
		int pageSize = 100;

		table.setPageSize(pageSize);
		table.setRowList(Eventos);
	}
}
