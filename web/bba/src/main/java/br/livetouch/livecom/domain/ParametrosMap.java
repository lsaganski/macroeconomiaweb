package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

public class ParametrosMap {

	private static final HashMap<Long, ParametrosMap> mapInstance = new HashMap<Long, ParametrosMap>();

	public static final Long EMPRESA_LIVECOM = 1L;

	private HashMap<String, Parametro> hash;
	private Long empresaId;

	
	public static ParametrosMap getInstance() {
		return getInstance(EMPRESA_LIVECOM);
	}
	
	public static ParametrosMap getInstance(Empresa e) {
		return getInstance(e != null ? e.getId() : null);
	}
	
	public static ParametrosMap getInstance(Long empresaId) {
		ParametrosMap map = mapInstance.get(empresaId);
		if(map == null) {
			map = new ParametrosMap();
			map.empresaId = EMPRESA_LIVECOM;
			map.hash = new LinkedHashMap<String, Parametro>();
			mapInstance.put(empresaId, map);
		}
		return map;
	}

	public static ParametrosMap setParametros(List<Parametro> list) {
		for (Parametro p : list) {
			Long empresaId = EMPRESA_LIVECOM;
			if(p.getEmpresa() != null) {
				empresaId = p.getEmpresa().getId();
			}
			ParametrosMap map = getInstance(empresaId);
			map.hash.put(p.getNome(), p);
		}
		return getInstance();
	}
	
	private Parametro getParametro(String key) {
		return hash.get(key);
	}

	public String get(String key) {
		Parametro p = getParametro(key);
		if(empresaId != EMPRESA_LIVECOM || p == null) {
			p = ParametrosMap.getInstance(EMPRESA_LIVECOM).getParametro(key);
		}
		return p != null ? p.getValor() : null;
	}

	public String get(String key, String defaultValue) {
		String s = get(key);
		return StringUtils.isNotEmpty(s) ? s : defaultValue;
	}
	
	public Long getLong(String key, long defaultValue) {
		String s = get(key);
		return StringUtils.isNotEmpty(s) ? NumberUtils.toLong(s) : defaultValue;
	}
	
	public int getInt(String key, int defaultValue) {
		String s = get(key);
		return StringUtils.isNotEmpty(s) ? NumberUtils.toInt(s) : defaultValue;
	}

	public boolean getBoolean(String key) {
		return getBoolean(key, false);
	}
	
	public boolean getBoolean(String key, boolean defaultValue) {
		String s = get(key);
		return StringUtils.isNotEmpty(s) ? "1".equals(s) : defaultValue;
	}

	public boolean isChatOn() {
		String chat_on = get(Params.CHAT_ON, "1");
		return "yes".equals(chat_on) || "1".equals(chat_on);
	}
	
	public List<Parametro> getAll() {
		return new ArrayList<Parametro>(hash.values());
	}
	
	public void put(String key,String value) {
		if(hash != null) {
			Parametro p = getParametro(key);
			if(p != null) {
				p.setValor(value);
			} else {
				p = new Parametro();
				p.setNome(key);
				p.setValor(value);
				hash.put(key, p);
			}
		}
	}

	public String getBuildType() {
		String s = get(Params.BUILDTYPE);
		if(StringUtils.isEmpty(s)) {
			return "livecom";
		}
		return s;
	}

	public boolean isBuildBJJ() {
		return "bjj".equals(getBuildType());
	}
	
	public boolean isPushOn() {
		String on = get(Params.PUSH_ON,"1");
		return "1".equals(on);
	}

	public boolean isSilentOn() {
		String on = get(Params.PROJETO_PUSH_SILENT_ON,"0");
		return "1".equals(on);
	}

	public boolean isDevModeOn() {
		String devModeOn = get(Params.DEVMODE_ON,"0");
		return "1".equals(devModeOn);
	}

	public static Set<Long> getAllEmpresas() {
		if(mapInstance == null) {
			return null;
		}
		return mapInstance.keySet();
	}
	
	public static void clear() {
		if(mapInstance != null) {
			mapInstance.clear();
		}
	}

	public String getByEmpresa(String key) {
		Parametro parametro = getParametro(key);
		if(parametro != null) {
			return parametro.getValor();
		}
		return null;
	}
	
	public HashMap<String, Parametro> getMap() {
		return hash;
	}
	
	@Override
	public String toString() {
		if(getMap() == null) {
			return super.toString();
		}
		return getMap().toString();
	}
}
