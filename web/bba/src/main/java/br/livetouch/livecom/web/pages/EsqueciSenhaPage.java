package br.livetouch.livecom.web.pages;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.EmailReportService;
import br.livetouch.livecom.web.pages.admin.LivecomPage;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

/**
 * Alterar senha do usuario
 * 
 * @author ricardo
 *
 */
@Controller
@Scope("prototype")
public class EsqueciSenhaPage extends LivecomPage {

	public Form form = new Form();
	
	private UsuarioLoginField tLogin;
	
	@Autowired
	protected EmailReportService emailReportService;

	@Override
	public void onInit() {
		super.onInit();
		
		logoutUserInfo();

		form.add(tLogin = new UsuarioLoginField("login","login",true , usuarioService, getEmpresa()));
		
		tLogin.setFocus(true);
		
		Submit btEnviar = new Submit("enviar", "Enviar", this, "enviar");
		btEnviar.setAttribute("class", "btn btn-circle green");
		form.add(btEnviar);
	}
	
	@Override
	public String getTemplate() {
		return super.getPath();
	}

	public boolean enviar() throws DomainException {
		try {
			if(form.isValid()) {
				Usuario u = tLogin.getEntity();
				if(u == null) {
					msg = "A nova senha foi enviada para o e-mail informado";
				} else {
					if(u.getEmpresa() == null || u.getEmpresa() != getEmpresa()) {
						msg = "A nova senha foi enviada para o e-mail informado";
					} else {
						if(u.isLoginLivecom()) {
							usuarioService.recuperarSenha(getEmpresa(), getUsuario(), u);
							msg = "A nova senha foi enviada para o e-mail informado";
						} else {
							ParametrosMap map = getParametrosMap();
							msg = map.get(Params.LOGIN_CONNECTOR_RECUPERAR_SENHA, "Entre em contato com a equipe de TI e solicite uma nova senha.");
							if(StringUtils.isEmpty(msg)) {
								msg = "Entre em contato com a equipe de TI e solicite uma nova senha.";
							}
						}
					}
				}
			}
		} catch (Exception e) {
			form.setError(e.getMessage());
		}

		return true;
	}

}
