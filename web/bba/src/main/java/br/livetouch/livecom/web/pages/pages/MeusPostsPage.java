package br.livetouch.livecom.web.pages.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 
 */
@Controller
@Scope("prototype")
public class MeusPostsPage extends PostComunicadoPage {

	public boolean meusPosts = true;
	
	@Override
	public void onInit() {
		super.onInit();
		
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();

		formAberto = false;
	}
}
