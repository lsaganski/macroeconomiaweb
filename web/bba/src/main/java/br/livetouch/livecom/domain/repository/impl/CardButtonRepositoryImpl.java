package br.livetouch.livecom.domain.repository.impl;


import br.livetouch.livecom.domain.CardButton;
import br.livetouch.livecom.domain.repository.CardButtonRepository;

@org.springframework.stereotype.Repository
public class CardButtonRepositoryImpl extends LivecomRepository<CardButton> implements CardButtonRepository{
	public CardButtonRepositoryImpl() {
		super(CardButton.class);
	}
}
