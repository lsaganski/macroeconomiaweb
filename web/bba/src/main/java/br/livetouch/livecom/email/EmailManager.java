package br.livetouch.livecom.email;

import java.io.IOException;

import org.apache.commons.mail.EmailException;

public interface EmailManager {

	public boolean sendEmail(String to,String subject, String body, boolean html) throws IOException, EmailException;
	
	public boolean sendEmail(String from,String to,String subject, String body, boolean html) throws IOException, EmailException;
	
	public boolean sendEmail(String from,String[] to,String subject, String body, boolean html, String[] bcc) throws IOException, EmailException;

}
