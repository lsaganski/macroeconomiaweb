package br.livetouch.livecom.rest.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.vo.ArquivoFiltro;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/arquivo")
public class ArquivoResource extends MainResource{
	
	protected static final Logger log = Log.getLogger(ArquivoResource.class);
	
	@GET
	public Response meusArquivos(
			@QueryParam("texto") String texto,
			@QueryParam("tags") String tags,
			@QueryParam("todos") Boolean todos,
			@QueryParam("grupos") String grupos,
			@QueryParam("ordem") String ordem,
			@QueryParam("start") String start,
			@QueryParam("end") String end,
			@QueryParam("page") Integer page,
			@QueryParam("max") Integer max,
			@QueryParam("userId") Long userId,
			@QueryParam("extensao") String extensao){
		
		if(!hasPermissao(ParamsPermissao.ARQUIVOS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		ArquivoFiltro arquivoFiltro = new ArquivoFiltro();
		arquivoFiltro.texto = texto;
		arquivoFiltro.start = start;
		arquivoFiltro.end = end;
		arquivoFiltro.todos = todos != null ? todos : true;
		arquivoFiltro.usuario = usuarioService.get(userId);
		arquivoFiltro.pageSize = max;
		arquivoFiltro.extensao = extensao;
		
		String[] split = StringUtils.split(grupos, ",");
		if(split != null) {
			List<Long> grupoIds = new ArrayList<>();
			for (String id : split) {
				grupoIds.add(Long.parseLong(StringUtils.trim(id)));
			}
			if(grupoIds.size() > 0) {
				arquivoFiltro.grupos = grupoService.findAllByIds(grupoIds);
			}
		}
		
		split = StringUtils.split(tags, ",");
		if(split != null) {
			List<Long> tagsId = new ArrayList<>();
			for (String id : split) {
				tagsId.add(Long.parseLong(StringUtils.trim(id)));
			}
			if(tagsId.size() > 0) {
				arquivoFiltro.tags = tagService.findAllByIds(tagsId, getEmpresa());
			}
		}
		
		arquivoFiltro.ordem = ordem;
		arquivoFiltro.page = page;
		List<Arquivo> arquivos = arquivoService.findAllByUser(arquivoFiltro);
		List<FileVO> vos = FileVO.fromList(arquivos, false);
		return Response.ok(vos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ARQUIVOS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Arquivo arquivo = arquivoService.get(id);
		if(arquivo == null) {
			return Response.ok(MessageResult.error("Arquivo não encontrado")).build();
		}
		FileVO vo = new FileVO();
		vo.setArquivo(arquivo, false);
		return Response.ok().entity(vo).build();
	}
	
	@PUT
	public Response update(Arquivo arquivo) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ARQUIVOS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			if(arquivo == null) {
				return Response.ok(MessageResult.error("Arquivo invalido")).build();
			}
			
			if(StringUtils.isEmpty(arquivo.getNome())) {
				return Response.ok(MessageResult.error("Nome de arquivo invalido")).build();
			}
			
			FileVO fileVO = new FileVO();
			Arquivo arq = arquivoService.get(arquivo.getId());
			arq.setNome(arquivo.getNome());
			arq.setDescricao(arquivo.getDescricao());
			arquivoService.saveOrUpdate(arq);
			fileVO.setArquivo(arq, false);
			return Response.ok(MessageResult.ok("Arquivo atualizado com sucesso", fileVO)).build();
		}  catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar o arquivo")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ARQUIVOS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Arquivo arquivo = arquivoService.get(id);
			if(arquivo == null) {
				return Response.ok(MessageResult.error("Arquivo não encontrado")).build();
			}
			
			if(arquivo.getPost() != null) {
				return Response.ok(MessageResult.error("Arquivo possui relacionamentos e não pode ser excluido")).build();
			}
			
			if(!arquivo.getUsuario().getId().equals(getUserInfo().getId()) && !getUserInfo().isAdmin()) {
				return Response.ok(MessageResult.error("Não possui permissão para remover este Arquivo")).build();
			}
			
			arquivoService.delete(arquivo);
			return Response.ok().entity(MessageResult.ok("Arquivo deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Arquivo  " + id)).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Arquivo  " + id)).build();
		}
	}
}
