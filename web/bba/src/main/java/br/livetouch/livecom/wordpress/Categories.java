package br.livetouch.livecom.wordpress;

public class Categories {
	private Long id;
	private String slug;
	private String title;
	private String description;
	private Integer parent;
	private Integer post_count;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getParent() {
		return parent;
	}
	public void setParent(Integer parent) {
		this.parent = parent;
	}
	public Integer getPost_count() {
		return post_count;
	}
	public void setPost_count(Integer post_count) {
		this.post_count = post_count;
	}
}
