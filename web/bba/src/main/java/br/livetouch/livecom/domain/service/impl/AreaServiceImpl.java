package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Area;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.repository.AreaRepository;
import br.livetouch.livecom.domain.service.AreaService;
import br.livetouch.livecom.domain.vo.FiltroArea;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class AreaServiceImpl extends LivecomService<Area> implements AreaService {
	@Autowired
	private AreaRepository rep;

	@Override
	public Area get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Usuario userInfo, Area area, Empresa empresa) throws DomainException {
		try {
			area.setEmpresa(empresa);
			rep.saveOrUpdate(area);
			
			auditSave(userInfo, area);
		} catch(Exception e) {
			throw new DomainException("Não foi possivel efetuar o cadastro da area", e);
		}
	}

	protected void auditSave(Usuario userInfo, Area area) {
		boolean insert = area.getId() == null;
		AuditoriaAcao acao = insert ? AuditoriaAcao.INSERIR : AuditoriaAcao.EDITAR;
		String msg = null;
		if(userInfo != null) {
			msg = "Área " + area.getNome() + " cadastrada";
			if(!insert) {
				msg = "Área " + area.getNome() + " editada";
			}
			
		}
		LogAuditoria.log(userInfo, area, AuditoriaEntidade.AREA, acao, msg);
	}

	@Override
	public List<Area> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	@Override
	public void delete(Usuario userInfo, Area a) throws DomainException {
		LogAuditoria auditoria = null;
		
		try {
			
			if(userInfo != null) {
				String msg  = "Área "+ a.getNome() +" deletada";
				auditoria = LogAuditoria.log(userInfo, a, AuditoriaEntidade.AREA, AuditoriaAcao.DELETAR, msg, false);
			}
			
			Empresa empresa = userInfo.getEmpresa();
			
			// Nao pode excluir area com usuarios.
			boolean r1 = ParametrosMap.getInstance(empresa).getBoolean(Params.DELETAR_AREA_R1_ON,true);
			if(r1) {
				List<Long> userIds = queryIds("select u.id from Usuario u where u.area.id = ?", false, a.getId());
				if (userIds.size() > 0) {
					throw new DomainException("Não foi possivel excluir a área, pois existem " + userIds.size() + " usuários associados.");
				}
			}

			// Delete
			rep.delete(a);
			
			// OK
			if(auditoria != null) {
				auditoria.finish();
			}
		} catch (DomainException e) {
			throw e;
		} catch (Exception e) {
			throw new DomainException("Não foi possível excluir a área [" + a + "]", e);
		}
	}

	@Override
	public List<Object[]> findIdCodAreas(Long idEmpresa) {
		return rep.findIdCodGrupos(idEmpresa);
	}

	@Override
	public List<Area> filterArea(FiltroArea filtro, Empresa empresa) {
		return rep.filterArea(filtro, empresa);
	}

	@Override
	public Area findDefaultByEmpresa(Long empresaId) {
		return rep.findDefaultByEmpresa(empresaId);
	}

	@Override
	public String csvArea(Empresa empresa) {
		List<Area> findAll = findAll(empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("CODIGO").add("NOME").add("DIRETORIA");
		for (Area a : findAll) {
			body.add(a.getCodigoDesc()).add(a.getNome()).add(a.getDiretoria() != null ? a.getDiretoria().getCodigoDesc() : "");
			body.end();
		}
		return generator.toString();
	}

	@Override
	public List<Object[]> findIdNomeAreas(Long empresaId) {
		return rep.findIdNomeAreas(empresaId);
	}

	@Override
	public Area findByName(Area area, Empresa empresa) {
		return rep.findByName(area, empresa);
	}
	
	public Area findByCodigoValid(Area area, Empresa empresa) {
		return rep.findByCodigoValid(area, empresa);
	}
	
}
