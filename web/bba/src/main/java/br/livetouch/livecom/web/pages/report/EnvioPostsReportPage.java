package br.livetouch.livecom.web.pages.report;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboAcao;
import br.infra.web.click.ComboTipoEntidade;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

@Controller
@Scope("prototype")
public class EnvioPostsReportPage extends RelatorioPage {

	public Form form = new Form();
	public RelatorioFiltro filtro;
	public int page;
	public ComboTipoEntidade comboEntidade;
	public ComboAcao comboAcao;
	public ActionLink exportar = new ActionLink(this, "exportar");
	
	@Override
	public void onInit() {
		super.onInit();

		if (clear) {
			getContext().removeSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			filtro = new RelatorioFiltro();
			filtro.setDataIni(DateUtils.toString(br.livetouch.livecom.utils.DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
			filtro.setDataFim(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		} else {
			filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			if (filtro != null) {
				if (filtro.getUsuarioId() != null) {
					Usuario usuario = usuarioService.get(filtro.getUsuarioId());
					if (usuario != null) {
						filtro.setUsuario(usuario);
					}
				}
			}
		}
		
		form();
	}
	
	public void form() {
		
		form.add(comboEntidade = new ComboTipoEntidade());
		form.add(comboAcao= new ComboAcao());
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "btn btn-outline blue min-width");
		form.add(tExportar);
		
	}
	
	public boolean exportar() throws DomainException {
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new RelatorioFiltro();
			getContext().setSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);
		filtro.setExpandir(true);
		
		String csv = logAuditoriaService.csvEnvioPostReport(filtro);
		if(csv != null) {
			download(csv, "envioPost-report.csv");
		} else {
			setFlashAttribute("msg", "Não existem dados para serem importados");
		}
		setRedirect(EnvioPostsReportPage.class);
		return true;
	}

}
