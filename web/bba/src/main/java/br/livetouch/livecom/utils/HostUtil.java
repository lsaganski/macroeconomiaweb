package br.livetouch.livecom.utils;

import java.net.Inet4Address;
import java.net.UnknownHostException;

import org.apache.commons.lang.StringUtils;

public class HostUtil {

	public static boolean isLocalhost() {
		
		boolean isLocalHost = "true".equals(System.getProperty("force.push.local"));
		if(isLocalHost) {
			return false;
		}
		
		try {
			String hostName = Inet4Address.getLocalHost().getHostName().toLowerCase();
			if (StringUtils.contains(hostName, "live-all-series")) {
				return true;
			}else if (StringUtils.contains(hostName, "usuario")) {
				return true;
			} else if (StringUtils.contains(hostName, "mac") || StringUtils.contains(hostName, "desktop")) {
				return true;
			}else if (StringUtils.contains(hostName, "ricardo") || StringUtils.contains(hostName, "lecheta")) {
				return true;
			} else if ("felipe".equals(hostName)) {
				return true;
			} else if ("desktop-fk5aobl".equals(hostName)) {
				return true;
			} else if (hostName.contains("augusto")) {
				return true;
			} else if("vostro-5470".equals(hostName)) {
				return true;
			}else if("livetouch-01".equals(hostName)) {
				return true;
			}
		} catch (UnknownHostException e) {
			
		}
		return false;
	}
	
	public static String getHostFromUrl(String url){
		return url.replaceAll("^(http://www\\.|http://|www\\.|https://www\\.|https://)","");
	}
}
