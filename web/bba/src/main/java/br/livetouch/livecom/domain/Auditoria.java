package br.livetouch.livecom.domain;

import net.livetouch.tiger.ddd.Entity;

public abstract class Auditoria extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7713878862799778069L;

	public abstract Long getRefId();
}
