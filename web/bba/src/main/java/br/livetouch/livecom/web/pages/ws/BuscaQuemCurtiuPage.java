package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Likes;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.vo.LikeVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class BuscaQuemCurtiuPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private LongField tPostId;
	private TextField tMode;
	private LongField tComentarioId;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tPostId = new LongField("post_id"));
		form.add(tComentarioId = new LongField("comentario_id"));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		form.add(new Submit("buscar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			long postId = getLong(tPostId.getLong());
			long comentarioId = getLong(tComentarioId.getLong());

			if(postId <= 0 && comentarioId <= 0) {
				return new MensagemResult("ERROR","Informe o id do post ou id do comentário.");
			}
			
			if(postId > 0 && comentarioId > 0) {
				return new MensagemResult("ERROR","Informe Apenas o id do post ou id do comentário.");
			}

			if(postId > 0) {
				Post post = postService.get(postId);
				if(post == null) {
					return new MensagemResult("ERROR","Post inválido.");
				}

				List<Likes> likes = likeService.findAllLikesByPost(post);
				List<LikeVO> likesVO = LikeVO.toListVO(likes);
				
				if(isWsVersion3()) {
					Response r = Response.ok("OK");
					r.likes = likesVO;
					return r;
				}
				
				return likesVO;
			}
			
			if(comentarioId > 0) {
				Comentario c = comentarioService.get(comentarioId);
				if(c == null) {
					return new MensagemResult("ERROR","Comentário inválido.");
				}

				List<Likes> likes = likeService.findAllLikesByComentario(c);
				List<LikeVO> likesVO = LikeVO.toListVO(likes);
				long postComentId = c.getPost().getId();
				for (LikeVO likeVO : likesVO) {
					likeVO.postId = postComentId;
				}
				
				if(isWsVersion3()) {
					Response r = Response.ok("OK");
					r.likes = likesVO;
					return r;
				}
				
				return likesVO;
			}
			
		}
		
		return new MensagemResult("NOK","Categorias não encontradas");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("user", UsuarioVO.class);
		x.alias("like", LikeVO.class);
		super.xstream(x);
	}
}
