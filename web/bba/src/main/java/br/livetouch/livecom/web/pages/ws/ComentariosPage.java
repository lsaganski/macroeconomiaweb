package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.ComentarioVO;
import br.livetouch.livecom.domain.vo.FileVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ComentariosPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tUserId;
	private TextField tPostId;
	private TextField tMode;
	private List<ComentarioVO> comentarios;
	
	public int maxRows;
	public int page;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tUserId = new TextField("user_id"));
		form.add(tPostId = new TextField("post_id"));
		form.add(new IntegerField("page"));
		form.add(new IntegerField("maxRows"));
		
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		tUserId.setFocus(true);

		form.add(new Submit("Consultar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			String userIdText = tUserId.getValue();
			String postIdText = tPostId.getValue();
			long userId = -1;
			long postId = -1;

			if(StringUtils.isEmpty(userIdText)) {
				userIdText = getParam("user_logado_id");
			}

			if(StringUtils.isNotEmpty(userIdText)) {
				if(NumberUtils.isNumber(userIdText)) {
					userId = new Long(userIdText);
				}
				else {
					if(isWsVersion3()) {
						return Response.error("Usuário inválido.");
					}
					return new MensagemResult("ERROR","Usuário inválido.");
				}
			}

			Post post = null;
			if(StringUtils.isNotEmpty(postIdText) ) {
				if(NumberUtils.isNumber(postIdText)) {
					postId = new Long(postIdText);
					post = postService.get(postId);
				}
				else {
					if(isWsVersion3()) {
						return Response.error("Post inválido.");
					}
					return new MensagemResult("ERROR","Post inválido.");
				}
			}

			Usuario user = usuarioService.get(userId);
			if(user == null) {
				if(isWsVersion3()) {
					return Response.error("Usuario inválido.");
				}
				return new MensagemResult("ERROR","Usuário inválido.");
			}
			
			Perfil permissao = user.getPermissao();
			if(permissao != null) {
				boolean isVisualizarComentario = Livecom.getInstance().hasPermissao(ParamsPermissao.VIZUALIZAR_COMENTARIOS, user);
				if(!isVisualizarComentario) {
					if(isWsVersion3()) {
						Response r = Response.error("Ação não autorizada");
						r.comentarios = new ArrayList<>();
						return r;
					}
					return new MensagemResult("ERROR","Ação não autorizada");
				}
			}
			
			if(StringUtils.isEmpty(userIdText) && StringUtils.isEmpty(postIdText)) {
				if(isWsVersion3()) {
					return Response.error("Informe usuário ou post.");
				}
				return new MensagemResult("ERROR","Informe usuário ou post.");
			}

			List<Comentario> list = comentarioService.findAllByUserAndPost(-1, postId,page, maxRows);
			
			comentarios = comentarioService.toListVo(user,list);
			
			// Notification
			
			notificationService.markComentarioAsRead(postId, userId);
			if(post != null) {
				notificationService.markPostAsRead(post, user);
			}
			
			if(isWsVersion3()) {
				Response r = Response.ok("OK");
				r.comentarios = comentarios;
				return r;
			}
			
			return comentarios;
		}
		
		return new MensagemResult("NOK","Erro ao buscar comentarios.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("comentario", ComentarioVO.class);
		x.alias("arquivo", FileVO.class);
		super.xstream(x);
	}
	
}
