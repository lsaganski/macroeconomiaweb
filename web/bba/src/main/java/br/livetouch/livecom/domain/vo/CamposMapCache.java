package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.Campo;
import br.livetouch.livecom.domain.Empresa;

public class CamposMapCache {

	private static final HashMap<Long, CamposMapCache> mapInstance = new HashMap<Long, CamposMapCache>();

	public static final Long EMPRESA_LIVECOM = 1L;

	private HashMap<String, Campo> hash;
	private Long empresaId;

	
	public static CamposMapCache getInstance() {
		return getInstance(EMPRESA_LIVECOM);
	}
	
	public static CamposMapCache getInstance(Empresa e) {
		return getInstance(e != null ? e.getId() : null);
	}
	
	public static CamposMapCache getInstance(Long empresaId) {
		CamposMapCache map = mapInstance.get(empresaId);
		if(map == null) {
			map = new CamposMapCache();
			map.empresaId = EMPRESA_LIVECOM;
			map.hash = new LinkedHashMap<String, Campo>();
			mapInstance.put(empresaId, map);
		}
		return map;
	}

	public static CamposMapCache setCampos(List<Campo> list) {
		for (Campo c : list) {
			Long empresaId = EMPRESA_LIVECOM;
			if(c.getEmpresa() != null) {
				empresaId = c.getEmpresa().getId();
			}
			CamposMapCache map = getInstance(empresaId);
			map.hash.put(c.getNome(), c);
		}
		return getInstance();
	}
	
	private Campo getCampo(String key) {
		return hash.get(key);
	}

	public Campo get(String key) {
		Campo c = getCampo(key);
		if(empresaId != EMPRESA_LIVECOM || c == null) {
			c = CamposMapCache.getInstance(EMPRESA_LIVECOM).getCampo(key);
		}
		return c != null ? c : null;
	}

	public List<Campo> getAll() {
		return new ArrayList<Campo>(hash.values());
	}
	
	public void put(String key,Campo value) {
		if(hash != null) {
			hash.put(key, value);
		}
	}

	public static Set<Long> getAllEmpresas() {
		if(mapInstance == null) {
			return null;
		}
		return mapInstance.keySet();
	}
	
	public static void clear() {
		if(mapInstance != null) {
			mapInstance.clear();
		}
	}

	public Campo getByEmpresa(String key) {
		Campo campo = getCampo(key);
		if(campo != null) {
			return campo;
		}
		return null;
	}
	
	public HashMap<String, Campo> getMap() {
		return hash;
	}
}
