package br.livetouch.livecom.domain.repository.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoAuditoria;
import br.livetouch.livecom.domain.GrupoSubgrupos;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.Semana;
import br.livetouch.livecom.domain.enums.StatusParticipacao;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.enums.TipoGrupo;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.repository.GrupoRepository;
import br.livetouch.livecom.domain.repository.UsuarioRepository;
import br.livetouch.livecom.domain.vo.Filtro;
import br.livetouch.livecom.domain.vo.GrupoFilter;
import br.livetouch.livecom.domain.vo.GrupoUsuariosVO;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.UserToPushVO;

@Repository
@SuppressWarnings("unchecked")
public class GrupoRepositoryImpl extends LivecomRepository<Grupo> implements GrupoRepository {

	@Autowired
	UsuarioRepository usuarioRep;

	public GrupoRepositoryImpl() {
		super(Grupo.class);
	}

	@Override
	public List<Grupo> findAllDefault(Usuario userInfo) {
		StringBuffer sql = new StringBuffer("select distinct g from Grupo g where 1=1 ");

		sql.append(" and g.empresa.id=:empresaId ");

		sql.append(" and g.padrao=1 ");

		Query q = createQuery(sql.toString());
		q.setCacheable(true);

		q.setParameter("empresaId", userInfo.getEmpresa().getId());

		return q.list();
	}

	@Override
	public List<Grupo> findAll(Empresa empresa, Integer page, Integer maxRows) {
		StringBuffer sql = new StringBuffer("from Grupo g where 1=1 ");

		sql.append(" and g.empresa.id=:empresaId ");

		Query q = createQuery(sql.toString());
		q.setCacheable(true);
		q.setParameter("empresaId", empresa.getId());
		
		
		if(page != null && maxRows != null) {
			setMaxResults(q, page, maxRows);
		}

		return q.list();
	}

	@Override
	public List<GrupoVO> findAll(GrupoFilter filter, Usuario userInfo) {
		Query query = getQueryAll(filter, userInfo, false);		
		
		int firstResult = 0;
		int page = filter.page;
		int max = filter.maxRows;
		
		if (page != 0) {
			firstResult = page * max;
			
		}
		query.setFirstResult(firstResult);
		
		if(max != 0) {
			query.setMaxResults(max);
		}
		
		query.setCacheable(true);
		List<GrupoVO> list = query.list();
		
		return list;
	}
	
	@Override
	public Long countAll(GrupoFilter filter, Usuario userInfo) {
		Query query = getQueryAll(filter, userInfo, true);	
		Long count = getCount(query);
		return count;
	}

	@Override
	public Grupo findByNome(String nome, Usuario userInfo) {
		StringBuffer sql = new StringBuffer("from Grupo where nome =  ?");

		sql.append(" and g.empresa.id=:empresaId ");

		Query q = createQuery(sql.toString());

		q.setString(0, nome);
		q.setParameter("empresaId", userInfo.getEmpresa().getId());

		q.setCacheable(true);
		Grupo g = (Grupo) q.uniqueResult();
		if (g != null) {
			deatach(g);
		}
		return g;
	}
	
	@Override
	public Grupo findByNomeAndUser(String nome, Usuario user) {
		StringBuffer sql = new StringBuffer("select gu.grupo from GrupoUsuarios gu where 1=1");

		if(StringUtils.isNotEmpty(nome)) {
			sql.append(" and nome = :nome");
		}
		
		if(user != null) {
			sql.append(" and gu.grupo.empresa.id = :empresaId ");
			sql.append(" and gu.usuario = :user ");
		}

		Query q = createQuery(sql.toString());

		if(StringUtils.isNotEmpty(nome)) {
			q.setString("nome", nome);
		}
		
		if(user != null) {
			q.setParameter("empresaId", user.getEmpresa().getId());
			q.setParameter("user", user);
		}

		q.setCacheable(true);
		Grupo g = (Grupo) q.uniqueResult();
		if (g != null) {
			deatach(g);
		}
		return g;
	}

	@Override
	public List<Grupo> findAllByNomeLike(String nome, Usuario userInfo) {
		StringBuffer sql = new StringBuffer("from Grupo g where upper(g.nome) like :nome");

		sql.append(" and g.empresa.id=:empresaId ");

		Query q = createQuery(sql.toString());
		q.setString("nome", "%" + nome + "%");
		q.setParameter("empresaId", userInfo.getEmpresa().getId());

		q.setCacheable(true);
		List<Grupo> list = q.list();
		return list;
	}

	@Override
	public Grupo findByCodigo(Grupo g, Usuario userInfo) {

		if (g == null) {
			return null;
		}

		StringBuffer sql = new StringBuffer("from Grupo g where 1=1 ");

		if (!StringUtils.isEmpty(g.getCodigo())) {
			sql.append(" and g.codigo = :cod");
		}

		if (g.getId() != null) {
			sql.append(" and g.id != :id");
		}

		sql.append(" and g.empresa.id=:empresaId ");

		Query q = createQuery(sql.toString());

		if (g.getCodigo() != null) {
			q.setParameter("cod", g.getCodigo());
		}

		if (g.getId() != null) {
			q.setParameter("id", g.getId());
		}

		q.setParameter("empresaId", userInfo.getEmpresa().getId());

		q.setCacheable(true);
		List<Grupo> list = q.list();
		Grupo grupo = list.size() > 0 ? (Grupo) list.get(0) : null;

		return grupo;
	}

	@Override
	public long getCountByFilter(Grupo filtro, Usuario userInfo) {
		Query query = getQueryFindByFilter(filtro, userInfo, true);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	private Query getQueryFindByFilter(Grupo filtro, Usuario userInfo, boolean count) {

		List<TipoGrupo> tipos = tipos();
		String countUsuarios = "select count(distinct gu.usuario.id) from GrupoUsuarios gu where gu.grupo.id = g.id";
		
		StringBuffer sb = new StringBuffer();
		if (count) {
			sb.append("select count(g.id) ");
		} else {
			sb.append("select distinct new br.livetouch.livecom.domain.vo.GrupoUsuariosVO (g,("+countUsuarios+")) ");
		}

		sb.append(" from Grupo g ");

		sb.append(" where 1=1 ");
		
		if(userInfo != null) {
			sb.append(" and (g.id in (select gu.grupo.id from GrupoUsuarios gu ");
			sb.append(" where (gu.usuario.id = :userId or gu.grupo.userCreate.id = :userId) ");
			sb.append(" or (gu.usuario.id = :userId and gu.grupo.todosPostam = 1) ");
			sb.append(" or gu.grupo.tipoGrupo in (:tipos))) ");
		}

		sb.append(" and g.empresa.id=:empresaId ");

		if (filtro != null && StringUtils.isNotEmpty(filtro.getNome())) {
			sb.append(" and upper(g.nome) like :nome ");
		}

		if (!count) {
			sb.append(" group by g");
		}

		sb.append(" order by g ");

		Query q = createQuery(sb.toString());
		if (filtro != null && StringUtils.isNotEmpty(filtro.getNome())) {
			q.setParameter("nome", "%" + filtro.getNome().toUpperCase() + "%");
		}
		
		if(userInfo != null) {
			q.setParameter("userId", userInfo.getId());
		}
		
		if(!tipos.isEmpty()) {
			q.setParameterList("tipos", tipos);
		}

		q.setParameter("empresaId", userInfo.getEmpresa().getId());

		q.setCacheable(true);
		return q;
	}

	@Override
	public List<GrupoUsuariosVO> findbyFilter(Grupo filtro, Usuario userInfo, int page, int pageSize) {
		Query query = getQueryFindByFilter(filtro, userInfo, false);

		if (page != -1) {
			int firstResult = 0;
			if (page != 0) {
				firstResult = page * pageSize;
			}
			query.setFirstResult(firstResult);
			query.setMaxResults(pageSize);
		}

		query.setCacheable(true);
		List<GrupoUsuariosVO> logs = query.list();

		return logs;
	}

	@Override
	public List<Object[]> findUsuariosArrayIdNome(Grupo grupo, Usuario userInfo) {
		StringBuffer sb = new StringBuffer("select gu.usuario.id,gu.usuario.login from GrupoUsuarios gu ");

		sb.append(" where 1=1 ");

		if (grupo != null) {
			sb.append(" and gu.grupo.id=? ");
		}

		sb.append(" and gu.grupo.empresa.id=:empresaId ");

		Query q = createQuery(sb.toString());
		if (grupo != null) {
			q.setLong(0, grupo.getId());
		}

		q.setParameter("empresaId", userInfo.getEmpresa().getId());

		q.setCacheable(true);
		List<Object[]> list = q.list();

		return list;
	}

	@Override
	public List<Usuario> findUsuarios(Grupo grupo) {
		StringBuffer sb = new StringBuffer("select gu.usuario from GrupoUsuarios gu where gu.grupo.id=?");

		sb.append(" and gu.grupo.empresa.id=:empresaId and gu.usuario.perfilAtivo = 1 ");
		sb.append(" group by gu.usuario order by gu.usuario.nome");

		Query q = createQuery(sb.toString());
		q.setLong(0, grupo.getId());
		q.setParameter("empresaId", grupo.getEmpresa().getId());
		/**
		 * Não ativar cache pois pode dar proxy load
		 * q.setCacheable(true); 
		 */
		 
		List<Usuario> list = q.list();
		return list;
	}

	@Override
	public List<UserToPushVO> findUsers(Grupo grupo, Post p, TipoNotificacao tipoNotificacao) {
		
		Long postId = p.getId();

		StringBuffer sb = new StringBuffer();

		sb.append("select u.id as id,u.login as login,u.email as email, COALESCE(pu.push,1) as push, COALESCE(pu.notification,1) as notification ");
		sb.append(" from grupo_usuarios gu ");
		sb.append(" inner join usuario u on gu.usuario_id = u.id ");
		sb.append(" inner join grupo g on gu.grupo_id = g.id ");
		sb.append(" inner join perfil p on u.permissao_id = p.id");
		sb.append(" left join post_usuarios pu on pu.post_id = :post and pu.usuario_id = gu.usuario_id ");
		sb.append(" where 1=1 ");
		sb.append(" and g.id = :grupo ");
		sb.append(" and g.empresa_id=:empresaId and u.perfil_ativo = 1 ");
		sb.append(" and (gu.push is null  or gu.push = 1) ");
		sb.append(" and (pu.push = 1 or pu.push is null)");
		if(tipoNotificacao == TipoNotificacao.COMENTARIO) {
			sb.append(" and p.id in (select pp.perfil_id from perfil_permissao pp inner join permissao per on per.id = pp.permissao_id ");
			sb.append(" where per.codigo = :permissao and pp.perfil_id = p.id and pp.ligado = 1)");
		}
		sb.append(" group by u.id, pu.usuario_id order by u.nome");
		
		Query q = getSession().createSQLQuery(sb.toString());

		if(tipoNotificacao == TipoNotificacao.COMENTARIO) {
			q.setParameter("permissao", ParamsPermissao.VIZUALIZAR_COMENTARIOS);
		}
		
		q.setParameter("grupo", grupo.getId());
		q.setParameter("empresaId", grupo.getEmpresa().getId());
		q.setParameter("post", postId);

		List<Object[]> array = q.list();

		List<UserToPushVO> list = UserToPushVO.hibernate(array);
		
		return list;
	}

	@Override
	public void removeUsuario(Usuario userInfo, Grupo grupo, Usuario usuario) {
		Query q = createQuery("delete from GrupoUsuarios gu where gu.grupo.id=? and gu.usuario.id=?");
		q.setLong(0, grupo.getId());
		q.setLong(1, usuario.getId());
		q.setCacheable(true);
		q.executeUpdate();
	}

	@Override
	public List<GrupoUsuarios> findMeusGrupos(GrupoFilter filter, Usuario userInfo) {
		Query query = getQueryFindMeusGrupos(filter, userInfo, false);		
		
		int firstResult = 0;
		int page = filter.page;
		int max = filter.maxRows;
		
		if (page != 0) {
			firstResult = page * max;
			
		}
		query.setFirstResult(firstResult);
		
		if(max != 0) {
			query.setMaxResults(max);
		}

		query.setCacheable(true);
		List<GrupoUsuarios> list = query.list();

		return list;
	}

	@Override
	public List<GrupoVO> findGrupos(GrupoFilter filter, Usuario userInfo) {
		Query query = getQueryGrupos(filter, userInfo, false);		
		
		int firstResult = 0;
		int page = filter.page;
		int max = filter.maxRows;
		
		if (page != 0) {
			firstResult = page * max;
			
		}
		query.setFirstResult(firstResult);
		
		if(max != 0) {
			query.setMaxResults(max);
		}
		
		query.setCacheable(true);
		List<GrupoVO> list = query.list();
		
		return list;
	}
	
	@Override
	public Long countFindGrupos(GrupoFilter filter, Usuario userInfo) {
		Query query = getQueryGrupos(filter, userInfo, true);	
		Long count = getCount(query);
		return count;
	}

	@Override
	public long getCountMeusGrupos(GrupoFilter filter, Usuario user) {
		Query query = getQueryFindMeusGrupos(filter, user, true);
		query.setCacheable(true);
		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	private Query getQueryFindMeusGrupos(GrupoFilter filter, Usuario userInfo, boolean count) {
		StringBuffer sb = new StringBuffer();
		
		TipoGrupo tipo = null;
		if(StringUtils.isNotEmpty(filter.tipo)) {
			tipo = TipoGrupo.valueOf(filter.tipo);
		}
		
		List<StatusParticipacao> status = status();

		if (count) {
			sb.append("select count(gu.grupo.id) ");
		} else {
			sb.append("select distinct gu ");
		}

		sb.append(" from GrupoUsuarios gu ");

		sb.append(" where 1=1 ");

		Long userId = userInfo.getId();
		Long grupoId = filter.id;
		
		if(grupoId != null) {
			sb.append(" and gu.grupo.id = :grupoId ");
		}

		sb.append(" and gu.grupo.empresa.id=:empresaId ");

		if (StringUtils.isNotEmpty(filter.nome)) {
			sb.append(" and (gu.grupo.nome like :nome)");
		}

		if (filter.gruposQueCriei) {
			// Grupos criados por mim
			sb.append(" and gu.grupo.userCreate.id=:userId ");
		} else {
			// Permissao
			String filtroPodePostar = filter.podePostar ? "gu.postar=1" : "";
			sb.append(" and (");
			sb.append(" (gu.usuario.id=:userId and ");
			if(filter.podePostar) {
				sb.append(filtroPodePostar);
				sb.append(" and");
			}
			sb.append(" gu.statusParticipacao in (:status))");
			sb.append(" or (gu.usuario.id=:userId and (");
			if(filter.podePostar) {
				sb.append(filtroPodePostar);
				sb.append(" or");
			}
			sb.append(" gu.grupo.todosPostam = 1)))");
		}

		if (filter.notIds != null && filter.notIds.size() > 0) {
			sb.append(" and gu.grupo.id not in (:gruposNotIds) ");
		}

		if (filter.cods != null && filter.cods.size() > 0) {
			sb.append(" and gu.grupo.codigo in (:cods) ");
		}
		
		if(tipo != null) {
			sb.append(" and gu.grupo.tipoGrupo = :tipo ");
		}
		
		if(filter.fisicos) {
			sb.append(" and gu.grupo.grupoVirtual = 0 ");
		}
		
		if (count) {
			sb.append(" group by gu.grupo.id ");
		} else {
			sb.append(" group by gu ");
			sb.append(" order by gu.favorito desc, gu.grupo.nome ");
		}

		Query query = createQuery(sb.toString());

		query.setParameter("empresaId", userInfo.getEmpresa().getId());
		
		if (!filter.gruposQueCriei) {
			query.setParameterList("status", status);
		}

		if (grupoId != null) {
			query.setParameter("grupoId", grupoId);
		}

		if (userId != null) {
			query.setParameter("userId", userId);
		}

		if (StringUtils.isNotEmpty(filter.nome)) {
			query.setParameter("nome", "%" + filter.nome + "%");
		}

		if (filter.notIds != null && filter.notIds.size() > 0) {
			query.setParameterList("gruposNotIds", filter.notIds);
		}
		
		if (filter.cods != null && filter.cods.size() > 0) {
			query.setParameterList("cods", filter.cods);
		}
		
		if(tipo != null) {
			query.setParameter("tipo", tipo);
		}
		
		query.setCacheable(true);
		return query;
	}
	
	private Query getQueryGrupos(GrupoFilter filter, Usuario userInfo, boolean count) {
		StringBuffer sb = new StringBuffer();
		
		TipoGrupo tipo = null;
		if(StringUtils.isNotEmpty(filter.tipo)) {
			tipo = TipoGrupo.valueOf(filter.tipo);
		}
		
		List<StatusParticipacao> status = status();
		List<TipoGrupo> tipos = tipos();

		String queryCountUsers = "(select count(distinct id) from GrupoUsuarios where grupo = g)"; 
		String queryCountSubgrupos = "(select count(distinct id) from GrupoSubgrupos where grupo = g)"; 
		
		if (count) {
			sb.append(" select count(g.id) ");
		} else {
			sb.append(" select distinct new br.livetouch.livecom.domain.vo.GrupoVO(g, gu, "+queryCountUsers+", "+queryCountSubgrupos+") ");
		}

		sb.append(" from Grupo g left join g.usuarios gu ");
		sb.append(" ON (gu.usuario.id = :userId)");

		sb.append(" left join g.subgrupos gs ");
		
		sb.append(" where 1=1 ");

		Long userId = userInfo.getId();
		Long grupoId = filter.id;
		
		if(grupoId != null) {
			sb.append(" and g.id = :grupoId ");
		}

		sb.append(" and g.empresa.id=:empresaId ");

		if (StringUtils.isNotEmpty(filter.nome)) {
			sb.append(" and (g.nome like :nome)");
		}

		if (filter.gruposQueCriei) {
			// Grupos criados por mim
			sb.append(" and g.userCreate.id=:userId ");
		} else {
			// Permissao
			sb.append(" and (");
			if(filter.podePostar) {
				sb.append(" (gu.usuario.id=:userId and (");
				sb.append(" gu.postar=1 ");
				sb.append(" or gu.statusParticipacao in (:status))");
				sb.append(" or g.todosPostam = 1 and gs.subgrupo.id in (select grupo.id from GrupoUsuarios where usuario.id = :userId))");
			} else {
				sb.append(" (gu.usuario.id=:userId and (");
				sb.append(" gu.postar=1 ");
				sb.append(" or gu.statusParticipacao in (:status)");
				sb.append(" or g.todosPostam = 1)");
				sb.append(" or (g.tipoGrupo in (:tipos)))");
				sb.append(" or gs.subgrupo.id in (select grupo.id from GrupoUsuarios where usuario.id = :userId) ");
			}
			sb.append(" )");
		}

		if (filter.notIds != null && filter.notIds.size() > 0) {
			sb.append(" and g.id not in (:gruposNotIds) ");
		}

		if (filter.cods != null && filter.cods.size() > 0) {
			sb.append(" and g.codigo in (:cods) ");
		}
		
		if(tipo != null) {
			sb.append(" and g.tipoGrupo = :tipo ");
		}
		
		if(filter.fisicos) {
			sb.append(" and g.grupoVirtual = 0 ");
		}

		if(filter.padrao) {
			sb.append(" and g.padrao = 1 ");
		}
		
		if (!count) {
			sb.append(" order by gu.favorito desc, g.nome ");
		}

		Query query = createQuery(sb.toString());

		query.setParameter("empresaId", userInfo.getEmpresa().getId());
		
		if (!filter.gruposQueCriei) {
			query.setParameterList("status", status);
			if(!filter.podePostar) {
				query.setParameterList("tipos", tipos);
			}
		}

		if (grupoId != null) {
			query.setParameter("grupoId", grupoId);
		}

		if (userId != null) {
			query.setParameter("userId", userId);
		}

		if (StringUtils.isNotEmpty(filter.nome)) {
			query.setParameter("nome", "%" + filter.nome + "%");
		}

		if (filter.notIds != null && filter.notIds.size() > 0) {
			query.setParameterList("gruposNotIds", filter.notIds);
		}
		
		if (filter.cods != null && filter.cods.size() > 0) {
			query.setParameterList("cods", filter.cods);
		}
		
		if(tipo != null) {
			query.setParameter("tipo", tipo);
		}
		
		query.setCacheable(true);
		return query;
	}
	
	private Query getQueryAll(GrupoFilter filter, Usuario userInfo, boolean count) {
		StringBuffer sb = new StringBuffer();
		
		TipoGrupo tipo = null;
		if(StringUtils.isNotEmpty(filter.tipo)) {
			tipo = TipoGrupo.valueOf(filter.tipo);
		}
		
		String queryCountUsers = "(select count(distinct id) from GrupoUsuarios where grupo = g)"; 
		String queryCountSubgrupos = "(select count(distinct id) from GrupoSubgrupos where grupo = g)"; 
		
		if (count) {
			sb.append(" select count(g.id) ");
		} else {
			sb.append(" select distinct new br.livetouch.livecom.domain.vo.GrupoVO(g, gu, "+queryCountUsers+", "+queryCountSubgrupos+") ");
		}

		sb.append(" from Grupo g left join g.usuarios gu ");
		sb.append(" ON (gu.usuario.id = :userId)");

		sb.append(" left join g.subgrupos gs ");
		
		sb.append(" where 1=1 ");

		Long userId = userInfo.getId();
		Long grupoId = filter.id;
		
		if(grupoId != null) {
			sb.append(" and g.id = :grupoId ");
		}

		sb.append(" and g.empresa.id=:empresaId ");

		if (StringUtils.isNotEmpty(filter.nome)) {
			sb.append(" and (g.nome like :nome)");
		}

		if (filter.gruposQueCriei) {
			// Grupos criados por mim
			sb.append(" and g.userCreate.id=:userId ");
		}

		if (filter.notIds != null && filter.notIds.size() > 0) {
			sb.append(" and g.id not in (:gruposNotIds) ");
		}

		if (filter.cods != null && filter.cods.size() > 0) {
			sb.append(" and g.codigo in (:cods) ");
		}
		
		if(tipo != null) {
			sb.append(" and g.tipoGrupo = :tipo ");
		}
		
		if(filter.fisicos) {
			sb.append(" and g.grupoVirtual = 0 ");
		}
		
		if (!count) {
			sb.append(" order by gu.favorito desc, g.nome ");
		}

		Query query = createQuery(sb.toString());

		query.setParameter("empresaId", userInfo.getEmpresa().getId());
		
		if (grupoId != null) {
			query.setParameter("grupoId", grupoId);
		}

		if (userId != null) {
			query.setParameter("userId", userId);
		}

		if (StringUtils.isNotEmpty(filter.nome)) {
			query.setParameter("nome", "%" + filter.nome + "%");
		}

		if (filter.notIds != null && filter.notIds.size() > 0) {
			query.setParameterList("gruposNotIds", filter.notIds);
		}
		
		if (filter.cods != null && filter.cods.size() > 0) {
			query.setParameterList("cods", filter.cods);
		}
		
		if(tipo != null) {
			query.setParameter("tipo", tipo);
		}
		
		query.setCacheable(true);
		return query;
	}	
	
	@Override
	public List<Grupo> findGruposParticipar(GrupoFilter filter, Usuario userInfo, List<Grupo> grupos) {
		Query query = getQueryFindGruposParticipar(filter, userInfo, false, grupos);		
		int firstResult = 0;
		int page = filter.page;
		int max = filter.maxRows;
		
		if (page != 0) {
			firstResult = page * max;
			
		}
		query.setFirstResult(firstResult);
		
		if(max != 0) {
			query.setMaxResults(max);
		}

		query.setCacheable(true);
		List<Grupo> list = query.list();

		return list;
	}

	@Override
	public List<Grupo> findGruposVirtuais(GrupoFilter filter, Usuario userInfo, List<Grupo> grupos) {
		Query query = getQueryFindGruposVirtuais(filter, userInfo, false, grupos);		
		int firstResult = 0;
		int page = filter.page;
		int max = filter.maxRows;
		
		if (page != 0) {
			firstResult = page * max;
			
		}
		query.setFirstResult(firstResult);
		
		if(max != 0) {
			query.setMaxResults(max);
		}
		
		query.setCacheable(true);
		List<Grupo> list = query.list();
		
		return list;
	}

	@Override
	public List<Usuario> findSubadminsByGrupo(Grupo grupo) {
		if(grupo == null) {
			return new ArrayList<Usuario>();
		}

		Query q = createQuery("select distinct gu.usuario from GrupoUsuarios gu where gu.grupo.id = :grupo and gu.admin = 1 ");
		q.setLong("grupo", grupo.getId());
		q.setCacheable(true);
		List<Usuario> list = q.list();
		return list;
	}

	private Query getQueryFindGruposParticipar(GrupoFilter filter, Usuario userInfo, boolean count, List<Grupo> grupos) {
		StringBuffer sb = new StringBuffer();
		
		TipoGrupo tipo = null;
		if(StringUtils.isNotEmpty(filter.tipo)) {
			tipo = TipoGrupo.valueOf(filter.tipo);
		}
		
		List<TipoGrupo> tipos = tipos();

		if (count) {
			sb.append("select count(g.id) ");
		} else {
			sb.append("select distinct g ");
		}

		sb.append(" from Grupo g ");

		sb.append(" where 1=1 ");

		Long userId = userInfo.getId();
		Long grupoId = filter.id;
		
		if(grupoId != null) {
			sb.append(" and g.id = :grupoId ");
		}

		sb.append(" and g.empresa.id=:empresaId ");

		if (StringUtils.isNotEmpty(filter.nome)) {
			sb.append(" and (g.nome like :nome)");
		}

		if (filter.gruposQueCriei) {
			// Grupos criados por mim
			sb.append(" and g.userCreate.id=:userId ");
		} else {
			sb.append(" and g.tipoGrupo in (:tipos) ");
		}
		
		if(!grupos.isEmpty()) {
			sb.append(" and g not in (:grupos) ");
		}
		
		if (filter.notIds != null && filter.notIds.size() > 0) {
			sb.append(" and g.id not in (:gruposNotIds) ");
		}

		if (filter.cods != null && filter.cods.size() > 0) {
			sb.append(" and g.codigo in (:cods) ");
		}
		
		if(tipo != null) {
			sb.append(" and g.tipoGrupo = :tipo ");
		}

		if(filter.fisicos) {
			sb.append(" and g.grupoVirtual = 0 ");
		}
		
		if (!count) {
			sb.append(" group by g ");
			sb.append(" order by g.nome ");
		}

		Query query = createQuery(sb.toString());

		query.setParameter("empresaId", userInfo.getEmpresa().getId());
		

		if (!filter.gruposQueCriei) {
			query.setParameterList("tipos", tipos);
		} else {
			query.setParameter("userId", userId);
		}
		
		if(!grupos.isEmpty()) {
			query.setParameterList("grupos", grupos);
		}
		
		if (grupoId != null) {
			query.setParameter("grupoId", grupoId);
		}

		if (StringUtils.isNotEmpty(filter.nome)) {
			query.setParameter("nome", "%" + filter.nome + "%");
		}

		if (filter.notIds != null && filter.notIds.size() > 0) {
			query.setParameterList("gruposNotIds", filter.notIds);
		}
		
		if (filter.cods != null && filter.cods.size() > 0) {
			query.setParameterList("cods", filter.cods);
		}
		
		if(tipo != null) {
			query.setParameter("tipo", tipo);
		}

		query.setCacheable(true);
		return query;
	}

	private Query getQueryFindGruposVirtuais(GrupoFilter filter, Usuario userInfo, boolean count, List<Grupo> grupos) {
		StringBuffer sb = new StringBuffer();
		
		if (count) {
			sb.append("select count(g.id) ");
		} else {
			sb.append("select distinct g ");
		}
		
		sb.append(" from GrupoSubgrupos gs inner join gs.grupo g ");
		
		sb.append(" where 1=1 ");
		
		Long userId = userInfo.getId();
		Long grupoId = filter.id;
		
		if(grupoId != null) {
			sb.append(" and g.id = :grupoId ");
		}
		
		sb.append(" and g.empresa.id=:empresaId ");
		
		if (StringUtils.isNotEmpty(filter.nome)) {
			sb.append(" and (g.nome like :nome)");
		}
		
		if (filter.gruposQueCriei) {
			// Grupos criados por mim
			sb.append(" and g.userCreate.id=:userId ");
		} else {
			sb.append(" and gs.subgrupo.id in (select grupo.id from GrupoUsuarios where usuario.id = :userId) ");
		}
		
		sb.append(" and g.todosPostam = 1 ");
		sb.append(" and g.grupoVirtual = 1 ");
		
		if(!grupos.isEmpty()) {
			sb.append(" and g not in (:grupos) ");
		}
		
		if (filter.notIds != null && filter.notIds.size() > 0) {
			sb.append(" and g.id not in (:gruposNotIds) ");
		}
		
		if (filter.cods != null && filter.cods.size() > 0) {
			sb.append(" and g.codigo in (:cods) ");
		}
		
		if (!count) {
			sb.append(" group by g ");
			sb.append(" order by g.nome ");
		}
		
		Query query = createQuery(sb.toString());
		
		query.setParameter("empresaId", userInfo.getEmpresa().getId());
		
		
		query.setParameter("userId", userId);
		
		if(!grupos.isEmpty()) {
			query.setParameterList("grupos", grupos);
		}
		
		if (grupoId != null) {
			query.setParameter("grupoId", grupoId);
		}
		
		if (StringUtils.isNotEmpty(filter.nome)) {
			query.setParameter("nome", "%" + filter.nome + "%");
		}
		
		if (filter.notIds != null && filter.notIds.size() > 0) {
			query.setParameterList("gruposNotIds", filter.notIds);
		}
		
		if (filter.cods != null && filter.cods.size() > 0) {
			query.setParameterList("cods", filter.cods);
		}
		
		query.setCacheable(true);
		return query;
	}

	private List<TipoGrupo> tipos() {
		List<TipoGrupo> tipos = new ArrayList<TipoGrupo>();
		tipos.add(TipoGrupo.ABERTO);
		tipos.add(TipoGrupo.ABERTO_FECHADO);
		return tipos;
	}

	private List<StatusParticipacao> status() {
		List<StatusParticipacao> status = new ArrayList<StatusParticipacao>();
		status.add(StatusParticipacao.APROVADA);
		status.add(StatusParticipacao.SOLICITADA);
		return status;
	}

	@Override
	public List<GrupoUsuariosVO> findGrupoUsuariosNaoAtivos(Usuario userInfo) {
		StringBuffer sb = new StringBuffer("select distinct new br.livetouch.livecom.domain.vo.GrupoUsuariosVO (u.grupo,count(distinct u.id)) from Usuario u inner join u.grupos gu where (u.status = ? or u.status is null) ");

		sb.append(" and u.empresa.id =  :empresaId ");

		sb.append(" group by u.grupo");

		Query q = createQuery(sb.toString());
		q.setCacheable(true);
		q.setParameter(0, StatusUsuario.NOVO);
		q.setParameter("empresaId", userInfo.getEmpresa().getId());

		List<GrupoUsuariosVO> list = q.list();
		list = GrupoUsuariosVO.fixListEmpty(list);
		return list;
	}

	@Override
	public List<GrupoUsuariosVO> findGrupoUsuariosAtivos(Usuario userInfo) {
		StringBuffer sb = new StringBuffer("select distinct new br.livetouch.livecom.domain.vo.GrupoUsuariosVO (u.grupo,count(distinct u.id)) from Usuario u inner join u.grupos gu where (u.status is not null and u.status = ?) ");

		sb.append(" and gu.grupo.empresa.id=:empresaId ");

		sb.append(" group by u.grupo");

		Query q = createQuery(sb.toString());
		q.setCacheable(true);
		q.setParameter(0, StatusUsuario.ATIVO);
		q.setParameter("empresaId", userInfo.getEmpresa().getId());

		List<GrupoUsuariosVO> list = q.list();
		list = GrupoUsuariosVO.fixListEmpty(list);
		return list;
	}

	@Override
	public List<GrupoUsuariosVO> findGrupoUsuariosEnviadoConvite(Usuario userInfo) {
		StringBuffer sb = new StringBuffer("select distinct new br.livetouch.livecom.domain.vo.GrupoUsuariosVO (u.grupo,count(distinct u.id)) from Usuario u inner join u.grupos gu where (u.status is not null and (u.status = ? or u.status = ? or u.status = ?)) ");

		sb.append(" and gu.grupo.empresa.id=:empresaId ");

		sb.append(" group by u.grupo");

		Query q = createQuery(sb.toString());
		q.setParameter(0, StatusUsuario.ENVIADO_CONVITE);
		q.setParameter(1, StatusUsuario.ENVIADO_CONVITE_JA_LOGOU);
		q.setParameter(2, StatusUsuario.ENVIADO_CONVITE_JA_LOGOU_MOBILE);

		q.setParameter("empresaId", userInfo.getEmpresa().getId());

		q.setCacheable(true);
		List<GrupoUsuariosVO> list = q.list();
		list = GrupoUsuariosVO.fixListEmpty(list);
		return list;
	}

	/**
	 * "1", "Novo - Cadastro recente, ainda não foi enviado e-mail." "2",
	 * "Enviado Convite - Ainda não fizeram login no sistema" "3",
	 * "Ativo - Já fizeram login no sistema"
	 * 
	 * @see br.livetouch.livecom.domain.repository.GrupoRepository#findGrupoUsuariosByStatusAtivacao(java.lang.String)
	 */
	@Override
	public List<GrupoUsuariosVO> findGrupoUsuariosByStatusAtivacao(String status, Usuario userInfo) {
		if ("2".equals(status)) {
			return findGrupoUsuariosEnviadoConvite(userInfo);
		} else if ("3".equals(status)) {
			return findGrupoUsuariosAtivos(userInfo);
		}

		return findGrupoUsuariosNaoAtivos(userInfo);

	}

	public java.util.List<GrupoUsuariosVO> findGrupoUsuariosByUser(Usuario user) {
		if (user == null) {
			return new ArrayList<GrupoUsuariosVO>();
		}
		StringBuffer sb = new StringBuffer("select distinct new br.livetouch.livecom.domain.vo.GrupoUsuariosVO (gu.grupo,count(gu.usuario.id)) from GrupoUsuarios gu where gu.grupo.id in (select gu2.grupo.id from GrupoUsuarios gu2 where gu2.usuario.id=?) ");

		sb.append(" group by gu.grupo");

		Query q = createQuery(sb.toString());
		q.setParameter(0, user.getId());

		q.setCacheable(true);

		List<GrupoUsuariosVO> list = q.list();
		list = GrupoUsuariosVO.fixListEmpty(list);
		return list;
	}

	public java.util.List<GrupoUsuarios> findGrupoUsuariosByUsuario(Usuario user) {
		if (user == null) {
			return new ArrayList<GrupoUsuarios>();
		}
		StringBuffer sb = new StringBuffer("from GrupoUsuarios gu where gu.usuario.id=? ");

		Query q = createQuery(sb.toString());
		q.setParameter(0, user.getId());

		q.setCacheable(true);

		List<GrupoUsuarios> list = q.list();
		return list;
	}

	@Override
	public Long countUsers(Grupo g) {
		StringBuffer sb = new StringBuffer();
		sb.append("select count(distinct gu.usuario.id) from GrupoUsuarios gu where gu.grupo.id=?");
		Query q = createQuery(sb.toString());
		q.setParameter(0, g.getId());
		q.setCacheable(true);
		Long count = (Long) q.uniqueResult();
		return count;
	}

	@Override
	public Long countUsersSubgrupos(Grupo g) {
		StringBuffer sb = new StringBuffer();
		sb.append("select count(distinct gu.usuario.id) from GrupoUsuarios gu where 1=1 ");
		sb.append("and gu.grupo.id in (select gs.subgrupo.id from GrupoSubgrupos gs where gs.grupo.id=:grupo) ");
		Query q = createQuery(sb.toString());
		q.setParameter("grupo", g.getId());
		q.setCacheable(true);
		Long count = (Long) q.uniqueResult();
		return count;
	}

	@Override
	public Long countSubgrupos(Grupo g) {
		StringBuffer sb = new StringBuffer();
		sb.append("select count(distinct gs.subgrupo.id) from GrupoSubgrupos gs where gs.grupo.id=?");
		Query q = createQuery(sb.toString());
		q.setParameter(0, g.getId());
		q.setCacheable(true);
		Long count = (Long) q.uniqueResult();
		return count;
	}

	@Override
	public List<Grupo> findAllByKeys(List<? extends Serializable> ids) {
		return createCriteria().add(Restrictions.in("id", ids)).setCacheable(true).list();
		// return super.findAllByKeys(ids);
	}

	@Override
	public List<Grupo> getGrupos(Usuario u) {
		Query q = createQuery("select distinct gu.grupo from GrupoUsuarios gu where gu.usuario.id=?");
		q.setLong(0, u.getId());
		q.setCacheable(true);
		List<Grupo> list = q.list();
		return list;
	}
	
	@Override
	public List<Grupo> getGrupos(GrupoFilter filter) {
		Usuario u = filter.usuario;
		Query q = createQuery("select gu.grupo from GrupoUsuarios gu where gu.usuario.id=?");
		q.setLong(0, u.getId());
		q.setMaxResults(filter.maxRows);;
		q.setCacheable(true);
		List<Grupo> list = q.list();
		return list;
	}

	@Override
	public boolean exists(Grupo g, Empresa e) {
		Query q = createQuery("select count(*) from Grupo where nome = ? and empresa = ?");
		q.setParameter(0, g.getNome());
		q.setParameter(1, e);
		q.setCacheable(true);
		long count = getCount(q);
		return count > 0;
	}

	@Override
	public void saveOrUpdate(GrupoAuditoria a) {
		getSession().save(a);
	}

	@Override
	public List<Grupo> findAllWithoutUsers(int page, int pageSize) {
		Query q = createQuery("select g from Grupo g where g.id not in (select distinct gu.usuario.id from GrupoUsuarios gu)");
		q.setFirstResult(page);
		q.setMaxResults(pageSize);
		q.setCacheable(true);
		List<Grupo> list = q.list();
		return list;
	}

	@Override
	public List<Grupo> findAllWithoutUsers(GrupoFilter filter, Usuario userInfo) {
		StringBuffer sb = new StringBuffer("select g from Grupo g where g.id not in (select distinct gu.grupo.id from GrupoUsuarios gu) ");

		if (StringUtils.isNotEmpty(filter.nome)) {
			sb.append(" and (g.nome like :nome)");
		}

		Query q = createQuery(sb.toString());

		if (StringUtils.isNotEmpty(filter.nome)) {
			q.setParameter("nome", "%" + filter.nome + "%");
		}

		q.setFirstResult(filter.page);
		q.setMaxResults(filter.maxRows);
		q.setCacheable(true);
		List<Grupo> list = q.list();
		return list;
	}

	@Override
	public List<Object[]> findIdCodGrupos(Long idEmpresa) {
		Query q = createQuery("select id,codigo from Grupo where empresa.id = :idEmpresa");
		q.setLong("idEmpresa", idEmpresa);
		q.setCacheable(true);
		return q.list();
	}

	@Override
	public Grupo findDefaultByEmpresa(Long empresaId) {
		List<Grupo> list = query("from Grupo where padrao=1 and empresa.id=?",true, empresaId);
		return list.isEmpty() ? null : list.get(0);
	}

	@Override
	public List<Grupo> findAllByUser(Usuario u) {
		List<Grupo> list = query("from Grupo where usuario.id = ?", true,u.getId());
		return list;
	}

	@Override
	public List<Long> findUsuariosDentroHorarioGrupoPadrao(Grupo grupo, Post p) {
		Long empresaId = p.getUsuario().getEmpresa().getId();
		String on = ParametrosMap.getInstance(empresaId).get(Params.GRUPO_RESTRICAO_HORARIO_ON, "0");
		String restricaoUserOn = ParametrosMap.getInstance(empresaId).get(Params.USUARIO_RESTRICAO_HORARIO_ON, "0");
		
		Semana dia = Semana.today();
		String diaHoje = dia.getDia();
		
		StringBuffer sb = new StringBuffer("select gu.usuario.id from GrupoUsuarios gu left join gu.usuario.expediente e where gu.grupo.id=?");

		sb.append(" and gu.grupo.empresa.id=:empresaId ");

		sb.append(" and gu.usuario.perfilAtivo = :ativo ");

		if(on.equals("1")) {
			sb.append(" and ((gu.usuario.grupo.horaAbertura is null and gu.usuario.grupo.horaFechamento is null) ");
			sb.append(" or (CONVERT_DATE_TO_TIME(gu.usuario.grupo.horaAbertura) <= CONVERT_DATE_TO_TIME(:data) and CONVERT_DATE_TO_TIME(gu.usuario.grupo.horaFechamento) >= CONVERT_DATE_TO_TIME(:data))");
			sb.append(" or (CONVERT_DATE_TO_TIME(gu.usuario.grupo.horaAbertura) = CONVERT_DATE_TO_TIME(gu.usuario.grupo.horaFechamento)))");
		}
		
		if(restricaoUserOn.equals("1")) {
			sb.append(" and ((gu.usuario.horaAbertura is null and gu.usuario.horaFechamento is null) ");
			sb.append(" or (CONVERT_DATE_TO_TIME(gu.usuario.horaAbertura) <= CONVERT_DATE_TO_TIME(:data) and CONVERT_DATE_TO_TIME(gu.usuario.horaFechamento) >= CONVERT_DATE_TO_TIME(:data))");
			sb.append(" or (CONVERT_DATE_TO_TIME(gu.usuario.horaAbertura) = CONVERT_DATE_TO_TIME(gu.usuario.horaFechamento))) ");
			sb.append(" and (e is null or e."+diaHoje+"=1) ");
		}
		
		sb.append(" and gu.usuario.grupo in (:grupos)");

		Query q = createQuery(sb.toString());
		q.setLong(0, grupo.getId());
		q.setParameter("empresaId", empresaId);
		q.setParameter("ativo", true);
		
		if(on.equals("1")) {
			q.setTime("data", p.getDataPublicacao());
		}
		
		q.setParameterList("grupos", p.getGrupos());

		q.setCacheable(true);
		List<Long> list = q.list();
		return list;
	}
	
	@Override
	public List<Long> findUsuariosToPush(Grupo grupo, Post p, Idioma idioma) {
		Long empresaId = p.getUsuario().getEmpresa().getId();
		String on = ParametrosMap.getInstance(empresaId).get(Params.GRUPO_RESTRICAO_HORARIO_ON, "0");
		String restricaoUserOn = ParametrosMap.getInstance(empresaId).get(Params.USUARIO_RESTRICAO_HORARIO_ON, "0");
		
		Semana dia = Semana.today();
		String diaHoje = dia.getDia();
		
		StringBuffer sb = new StringBuffer("select gu.usuario.id from GrupoUsuarios gu left join gu.usuario.expediente e where gu.grupo.id=?");

		sb.append(" and gu.grupo.empresa.id=:empresaId ");

		sb.append(" and gu.usuario.perfilAtivo = :ativo ");
		sb.append(" and gu.push = 1");
		
		if(idioma != null) {
			sb.append(" and gu.usuario.idioma = :idioma ");
		}

		if(on.equals("1")) {
			sb.append(" and ((gu.grupo.horaAbertura is null and gu.grupo.horaFechamento is null) ");
			sb.append(" or (CONVERT_DATE_TO_TIME(gu.grupo.horaAbertura) <= CONVERT_DATE_TO_TIME(:data) and CONVERT_DATE_TO_TIME(gu.grupo.horaFechamento) >= CONVERT_DATE_TO_TIME(:data))");
			sb.append(" or (CONVERT_DATE_TO_TIME(gu.grupo.horaAbertura) = CONVERT_DATE_TO_TIME(gu.grupo.horaFechamento)))");
		}
		
		if(restricaoUserOn.equals("1")) {
			sb.append(" and ((gu.usuario.horaAbertura is null and gu.usuario.horaFechamento is null) ");
			sb.append(" or (CONVERT_DATE_TO_TIME(gu.usuario.horaAbertura) <= CONVERT_DATE_TO_TIME(:data) and CONVERT_DATE_TO_TIME(gu.usuario.horaFechamento) >= CONVERT_DATE_TO_TIME(:data))");
			sb.append(" or (CONVERT_DATE_TO_TIME(gu.usuario.horaAbertura) = CONVERT_DATE_TO_TIME(gu.usuario.horaFechamento))) ");
			sb.append(" and (e is null or e."+diaHoje+"=1) ");
		}
		
//		sb.append(" and gu.grupo in (:grupos)");

		Query q = createQuery(sb.toString());
		q.setLong(0, grupo.getId());
		q.setParameter("empresaId", empresaId);
		q.setParameter("ativo", true);

		if(idioma != null) {
			q.setParameter("idioma", idioma);
		}
		
		if(on.equals("1") || restricaoUserOn.equals("1")) {
			q.setTime("data", p.getDataPublicacao());
		}
		
//		q.setParameterList("grupos", p.getGrupos());

		q.setCacheable(true);
		List<Long> list = q.list();
		return list;
	}

	@Override
	public List<Long> findUsuariosReminder(Grupo grupo, Post p) {
		StringBuffer sb = new StringBuffer();
		
		sb.append("select distinct gu.usuario.id from Grupo g inner join g.usuarios gu where g = :grupo ");
		sb.append("and (gu.admin = 1 or g.userCreate = gu.usuario.id) ");

		Query q = createQuery(sb.toString());

		q.setParameter("grupo", grupo);
		
		List<Long> list = q.list();
		return list;
	}

	@Override
	public List<Object[]> findIdNomeGrupos(Long empresaId) {
		Query q = createQuery("select id,nome from Grupo where empresa.id = :empresaId");
		q.setLong("empresaId", empresaId);
		q.setCacheable(true);
		return q.list();
	}

	@Override
	public boolean isGrupoPadraoDentroHorario(Usuario u) {
		
		if(u == null) {
			return false;
		}
		
		StringBuffer sb = new StringBuffer("select gu.usuario.id from GrupoUsuarios gu ");

		sb.append(" where gu.usuario = :usuario ");
		sb.append(" and ((gu.usuario.grupo.horaAbertura is null and gu.usuario.grupo.horaFechamento is null) ");
		sb.append("or (CONVERT_DATE_TO_TIME(gu.usuario.grupo.horaAbertura) <= CONVERT_DATE_TO_TIME(:data) and CONVERT_DATE_TO_TIME(gu.usuario.grupo.horaFechamento) >= CONVERT_DATE_TO_TIME(:data))");
		sb.append("or (CONVERT_DATE_TO_TIME(gu.usuario.grupo.horaAbertura) = CONVERT_DATE_TO_TIME(gu.usuario.grupo.horaFechamento)))");

		Query q = createQuery(sb.toString());
		
		q.setTime("data", new Date());
		q.setParameter("usuario", u);

		return !q.list().isEmpty();
	}

	@Override
	public boolean isGruposDentroHorario(Usuario u) {
		if(u == null) {
			return false;
		}
		
		StringBuffer sb = new StringBuffer("select gu.usuario.id from GrupoUsuarios gu ");

		sb.append(" where gu.usuario = :usuario ");
		sb.append(" and ((gu.grupo.horaAbertura is null and gu.grupo.horaFechamento is null) ");
		sb.append("or (CONVERT_DATE_TO_TIME(gu.grupo.horaAbertura) <= CONVERT_DATE_TO_TIME(:data) and CONVERT_DATE_TO_TIME(gu.grupo.horaFechamento) >= CONVERT_DATE_TO_TIME(:data))");
		sb.append("or (CONVERT_DATE_TO_TIME(gu.grupo.horaAbertura) = CONVERT_DATE_TO_TIME(gu.grupo.horaFechamento)))");

		Query q = createQuery(sb.toString());
		
		q.setTime("data", new Date());
		q.setParameter("usuario", u);

		q.setCacheable(true);
		return !q.list().isEmpty();
	}
	
	@Override
	public List<Long> grupoPadraoUsuariosDentroHorario(List<Long> users) {
		
		StringBuffer sb = new StringBuffer("select gu.usuario.id from GrupoUsuarios gu ");

		sb.append(" where gu.usuario.id in (:users) ");
		sb.append(" and ((gu.usuario.grupo.horaAbertura is null and gu.usuario.grupo.horaFechamento is null) ");
		sb.append("or (CONVERT_DATE_TO_TIME(gu.usuario.grupo.horaAbertura) <= CONVERT_DATE_TO_TIME(:data) and CONVERT_DATE_TO_TIME(gu.usuario.grupo.horaFechamento) >= CONVERT_DATE_TO_TIME(:data))");
		sb.append("or (CONVERT_DATE_TO_TIME(gu.usuario.grupo.horaAbertura) = CONVERT_DATE_TO_TIME(gu.usuario.grupo.horaFechamento)))");

		Query q = createQuery(sb.toString());
		
		q.setTime("data", new Date());
		q.setParameter("users", users);

		return q.list();
	}
	
	@Override
	public List<Long> gruposUsuariosDentroHorario(List<Long> users) {

		StringBuffer sb = new StringBuffer("select gu.usuario.id from GrupoUsuarios gu ");

		sb.append(" where gu.usuario.id in (:users) and gu.usuario.receivePush = 1");
		sb.append(" and ((gu.grupo.horaAbertura is null and gu.grupo.horaFechamento is null) ");
		sb.append("or (CONVERT_DATE_TO_TIME(gu.grupo.horaAbertura) <= CONVERT_DATE_TO_TIME(:data) and CONVERT_DATE_TO_TIME(gu.grupo.horaFechamento) >= CONVERT_DATE_TO_TIME(:data))");
		sb.append("or (CONVERT_DATE_TO_TIME(gu.grupo.horaAbertura) = CONVERT_DATE_TO_TIME(gu.grupo.horaFechamento)))");

		Query q = createQuery(sb.toString());
		
		q.setTime("data", new Date());
		q.setParameterList("users", users);

		q.setCacheable(true);
		return q.list();
	}
	
	@Override
	public Grupo findByName(Grupo grupo, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Grupo g where g.nome = :nome");
		sb.append(" and g.empresa = :empresa");
		
		if(grupo.getId() != null) {
			sb.append(" and g.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("empresa", empresa);
		q.setParameter("nome", grupo.getNome());
		
		if(grupo.getId() != null) {
			q.setParameter("id", grupo.getId());
		}
		
		q.setCacheable(true);
		List<Grupo> list = q.list();
		Grupo g = (Grupo) (list.size() > 0 ? q.list().get(0) : null);
		return g;
	}
	
	@Override
	public List<Long> findIdUsuarios(Grupo grupo) {
		StringBuffer sb = new StringBuffer("select gu.usuario.id from GrupoUsuarios gu where gu.grupo.id=?");

		sb.append(" and gu.grupo.empresa.id=:empresaId ");

		Query q = createQuery(sb.toString());
		q.setLong(0, grupo.getId());
		q.setParameter("empresaId", grupo.getEmpresa().getId());

		q.setCacheable(true);
		List<Long> list = q.list();
		return list;
	}

	@Override
	public List<Grupo> findGruposByPost(Post post) {
		StringBuffer sb = new StringBuffer("select g from Post p inner join p.grupos g where p.id=:postId");
		
		Query q = createQuery(sb.toString());
		q.setParameter("postId", post.getId());
		q.setCacheable(true);
		List<Grupo> list = q.list();
		return list;
	}

	@Override
	public Long getCountPostsByGrupo(Grupo g) {
		StringBuffer sb = new StringBuffer("select count(p.id) from Post p inner join p.grupos g where g.id=:grupoId");
		
		Query q = createQuery(sb.toString());
		q.setParameter("grupoId", g.getId());
		q.setCacheable(true);
		Long count = getCount(q);
		return count;
	}

	@Override
	public Long countPostsSomenteEu(Usuario user) {
		StringBuffer sb = new StringBuffer("select count(p.id) from Post p left join p.grupos g where 1=1 ");
		
		if(user != null) {
			sb.append(" and (g is null and p.usuario = :user)");
		}
		
		Query q = createQuery(sb.toString());
		q.setParameter("user", user);
		q.setCacheable(true);
		Long count = getCount(q);
		return count;
	}
	
	@Override
	public List<GrupoUsuarios> findUsuariosByStatusParticipacao(StatusParticipacao status, Grupo g) {
		StringBuffer sb = new StringBuffer("select gu from GrupoUsuarios gu where 1=1 ");
		
		if(g != null) {
			sb.append(" and gu.grupo = :grupo");
		}

		if(status != null) {
			sb.append(" and gu.statusParticipacao = :status");
		}
		
		sb.append(" order by gu.usuario.nome");
		
		Query q = createQuery(sb.toString());
		
		if(g != null) {
			q.setParameter("grupo", g);
		}

		if(status != null) {
			q.setParameter("status", status);
		}
		
		q.setCacheable(true);
		List<GrupoUsuarios> list = q.list();
		return list;
	}
	
	@Override
	public void movePostsToAdminGrupo(Usuario usuario, Usuario admin, Grupo grupo, List<Post> posts) {
		
		if(posts.isEmpty()) {
			return;
		}
		
		StringBuffer sb = new StringBuffer("update Post p set p.usuarioUpdate = :admin, p.usuario = :admin where 1=1 ");
		
		sb.append("and (p.usuario = :usuario or p.usuarioUpdate = :usuario) ");

		sb.append("and p in (:posts) ");
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("admin", admin);
		q.setParameter("usuario", usuario);
		
		q.setParameterList("posts", posts);

		q.executeUpdate();
	}
	
	@Override
	public GrupoUsuarios findByGrupoEUsuario(Grupo g, Usuario u) {
		Query q = createQuery("from GrupoUsuarios gu where gu.grupo = :grupo and gu.usuario = :usuario");
		q.setParameter("grupo", g);
		q.setParameter("usuario", u);
		q.setCacheable(true);
		GrupoUsuarios gu = !q.list().isEmpty() ? (GrupoUsuarios) q.list().get(0) : null;
		return gu;
	}

	@Override
	public List<Grupo> findAllByCodigos(List<String> codigos) {
		StringBuffer sb = new StringBuffer("select distinct g from Grupo g where 1=1 ");
		
		sb.append(" and g.codigo in (:codigos) ");
		
		Query q = createQuery(sb.toString());
		
		q.setParameterList("codigos", codigos);
		
		q.setCacheable(true);
		List<Grupo> list = q.list();
		return list;
	}
	
	@Override
	public GrupoSubgrupos getGrupoSubgrupo(Grupo grupo, Grupo subgrupo) {
		Query q = createQuery("select gs from GrupoSubgrupos gs where gs.grupo.id=? and gs.subgrupo.id=?");
		q.setLong(0, grupo.getId());
		q.setLong(1, subgrupo.getId());
		q.setCacheable(true);
		List<GrupoSubgrupos> list = q.list();
		return list.size() > 0 ? list.get(0) : null;
	}
	
	@Override
	public void saveOrUpdate(GrupoSubgrupos gs) {
		getSession().saveOrUpdate(gs);
	}
	
	@Override
	public void removeSubgrupo(Usuario userInfo, Grupo grupo, Grupo subgrupo) {
		Query q = createQuery("delete from GrupoSubgrupos gs where gs.grupo.id=? and gs.subgrupo.id=?");
		q.setLong(0, grupo.getId());
		q.setLong(1, subgrupo.getId());
		q.setCacheable(true);
		q.executeUpdate();
	}

	@Override
	public List<Grupo> findSubgruposByGrupo(Grupo grupo) {
		
		if(grupo == null) {
			return new ArrayList<Grupo>();
		}
		
		Query q = createQuery("select distinct gs.subgrupo from GrupoSubgrupos gs where gs.grupo.id = :grupo ");
		q.setLong("grupo", grupo.getId());
		q.setCacheable(true);
		List<Grupo> list = q.list();
		return list;
	}

	@Override
	public List<Grupo> findAllSubgrupos(Filtro filtro, Empresa empresa) {
		
		if(empresa == null) {
			return new ArrayList<Grupo>();
		}
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("select distinct g from Grupo g where 1=1 ");
		sb.append(" and g.grupoVirtual = :virtual ");
		sb.append(" and g.empresa = :empresa ");
		
		if(StringUtils.isNotEmpty(filtro.nome)) {
			sb.append(" and g.nome like :nome ");
		}

		if(!filtro.ids.isEmpty()) {
			sb.append(" and g.id not in (:ids) ");
		}
		
		sb.append(" order by g.nome");
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("empresa", empresa);
		
		if(StringUtils.isNotEmpty(filtro.nome)) {
			q.setParameter("nome", "%" + filtro.nome + "%");
		}
		
		if(!filtro.ids.isEmpty()) {
			q.setParameterList("ids", filtro.ids);
		}
		
		q.setParameter("virtual", false);
		q.setCacheable(true);
		List<Grupo> list = q.list();
		return list;
		
	}

	@Override
	public List<Grupo> findAllSubgruposWithAcesso(Filtro filtro, Usuario user) {

		if(user == null) {
			return new ArrayList<Grupo>();
		}
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("select distinct g from Grupo g where 1=1 ");
		sb.append(" and g.grupoVirtual = :virtual ");
		sb.append(" and g.empresa = :empresa ");
		sb.append(" and g.id in (select grupo.id from GrupoUsuarios where usuario = :user and (statusParticipacao = :aprovada or status_participacao is null))");
		
		if(StringUtils.isNotEmpty(filtro.nome)) {
			sb.append(" and g.nome like :nome ");
		}
		
		if(!filtro.ids.isEmpty()) {
			sb.append(" and g.id not in (:ids) ");
		}
		
		sb.append(" order by g.nome");
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("user", user);
		q.setParameter("empresa", user.getEmpresa());
		q.setParameter("aprovada", StatusParticipacao.APROVADA);
		q.setParameter("virtual", false);
		
		if(StringUtils.isNotEmpty(filtro.nome)) {
			q.setParameter("nome", "%" + filtro.nome + "%");
		}
		
		if(!filtro.ids.isEmpty()) {
			q.setParameterList("ids", filtro.ids);
		}
		
		q.setCacheable(true);
		List<Grupo> list = q.list();
		return list;
		
	}

	@Override
	public List<Grupo> findSubgruposInGrupos(Set<Grupo> grupos) {

		if(grupos == null || grupos.isEmpty()) {
			return new ArrayList<Grupo>();
		}
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("select distinct gs.subgrupo from GrupoSubgrupos gs where 1=1 ");
		sb.append(" and gs.grupo in (:grupos)");
		
		Query q = createQuery(sb.toString());
		
		q.setParameterList("grupos", grupos);
		q.setCacheable(true);
		List<Grupo> list = q.list();
		return list;
	}
	
	@Override
	public void changePermissaoPostarUsers(Grupo grupo, boolean postar) {
		Query q = createQuery("update GrupoUsuarios gu set gu.postar = :postar where gu.grupo=:grupo and gu.usuario != :admin");
		q.setParameter("grupo", grupo);
		q.setParameter("admin", grupo.getAdmin());
		q.setParameter("postar", postar);
		q.executeUpdate();
	}
	
	@Override
	public List<GrupoUsuarios> isAdmin(Usuario usuario, Grupo grupo) {
		StringBuffer sb = new StringBuffer();

		sb.append("select gu from Grupo g left join g.usuarios gu where g = :grupo and gu.usuario = :usuario ");
		sb.append("and (gu.admin = 1 or gu.grupo.userCreate = :usuario) ");
		sb.append("or (g.userCreate = :usuario and g = :grupo)");

		Query q = createQuery(sb.toString());

		q.setParameter("grupo", grupo);
		q.setParameter("usuario", usuario);
		q.setCacheable(true);
		return q.list();
	}

}