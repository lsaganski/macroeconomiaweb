//package br.livetouch.livecom.domain.service.impl;
//
//import java.io.File;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import br.infra.util.Log;
//import br.infra.util.Utils;
//import br.livetouch.livecom.domain.Grupo;
//import br.livetouch.livecom.domain.ImportarArquivoResponse;
//import br.livetouch.livecom.domain.ParametrosMap;
//import br.livetouch.livecom.domain.Permissao;
//import br.livetouch.livecom.domain.Usuario;
//import br.livetouch.livecom.domain.service.GrupoService;
//import br.livetouch.livecom.domain.service.ImportarArquivoUsuarioService;
//import br.livetouch.livecom.domain.service.PermissaoService;
//import br.livetouch.livecom.domain.service.UsuarioService;
//import br.livetouch.livecom.domain.vo.LinhaUsuarioVO;
//import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;
//import jxl.read.biff.BiffException;
//import net.livetouch.tiger.ddd.DomainException;
//
///**
// * 87682 mil - 45min
// * 
// * @author ricardo
// * 
// * @deprecated Usar work
// *
// */
//@Service
//public class ImportarArquivoUsuarioServiceImpl implements ImportarArquivoUsuarioService {
//	protected static final Logger log = Log.getLogger("importacao_usuario");
//	protected static final Logger logError = Log.getLogger("importacao_usuario_error");
//
//	@Autowired
//	protected GrupoService grupoService;
//
//	@Autowired
//	protected PermissaoService permissaoService;
//	
//	@Autowired
//	protected UsuarioService usuarioService;
//	
//	@Autowired
//	protected SessionFactory sessionFactory;
//	
//	private File dirIn;
//	private boolean modeSearch = false;
//	private Long idGrupo;
//	private Long idPermissao;
//
//	@SuppressWarnings("unchecked")
//	@Override
//	public ImportarArquivoResponse importar(InputStream in) throws DomainException, BiffException, IOException {
//		Session session = sessionFactory.getCurrentSession();
////		Transaction tx = session.beginTransaction();
////		tx.commit();
//		
//		long timeA = System.currentTimeMillis();
//		
//		if (in == null) {
//			throw new DomainException("InputStream inválida.");
//		}
//		List<LinhaUsuarioVO> linhasArquivoUsuario = (List<LinhaUsuarioVO>) new LinhaUsuarioVO().read(in);
//		int totalRows = linhasArquivoUsuario.size();
//		log("Importando arquivo linhas: " + totalRows);
//
//		int countOk = 0;
//		int countError = 0;
//		ImportarArquivoResponse response = new ImportarArquivoResponse();
//		List<Object> list = new ArrayList<Object>();
//
//		idPermissao = new Long(ParametrosMap.getInstance().get("importacao.permissao_id", "3"));
//		idGrupo = new Long(ParametrosMap.getInstance().get("importacao.grupo_id", "5"));
//
//		Grupo grupo = grupoService.get(idGrupo);
//		if (grupo == null) {
//			log.debug("Grupo não encontrado no banco de dados, id [" + idGrupo + "]");
//			throw new DomainException("Grupo não encontrado no banco de dados, id [" + idGrupo + "]");
//		}
//		Permissao permissao = permissaoService.get(idPermissao);
//		if (permissao == null) {
//			log.debug("Permissão não encontrada no banco de dados, id [" + idPermissao + "]");
//			throw new DomainException("Permissão não encontrada no banco de dados, id [" + idPermissao + "]");
//		}
//
//		// Salva map com todos os logins
//		// Os logins que que nao estiverem no csv
//		// E estiverem no map, serao excluidos no final.
//		List<String> logins = usuarioService.findLogins();
//		Map<String, String> mapLogins = new HashMap<String,String>();
//		for (String login : logins) {
//			mapLogins.put(login, login);
//		}
//		
//		for (int i = 0; i < totalRows; i++) {
//			int row = i + 1;
//			try {
//
//				LinhaUsuarioVO linha = linhasArquivoUsuario.get(i);
//				log("Linha [" + row + "]: " + linha);
//
//				/**
//				 * Email Keite
//				 * Com zeros à esquerda. Total de 9 dígitos.
//				String login = StringUtils.leftPad(linha.getLogin(), 9, "0");
//				 */
//				
//				String login = linha.getLogin();
//				String dataNasc = linha.getDataNascimento();
//
//				if(modeSearch) {
//					Long id = usuarioService.findLogin(login);
//					
//					// Somente busca
//					if (id != null) {
//						countOk++;
//						mapLogins.remove(login);
//					} else {
//						writeRow("NotFound ["+row+"] user not found ["+login+"]");
//						countError++;
//						log.debug("Linha ["+row+"] user not found ["+login+"]");
//						break;
//					}
//				} else {
//
//					// Usuario u = usuarioService.findByLogin(login);
//					Long id = usuarioService.findLogin(login);
//					
//					boolean naoEstaNoGrupo = true;
//					if (id == null) {
//						
//						String dataNascString = StringUtils.replace(dataNasc, "/", "");
//						id = usuarioService.insertUsuarioImportacao(login, dataNascString, dataNascString, idPermissao, idGrupo);
//						
//						/*u = new Usuario();
//
//						// Somente faz insert pois nao tem dados para atualizar
//						u.setLogin(login);
//						u.setTipoSistema(TipoSistema.IMPORTACAO);
//
//						Date dt = DateUtils.toDate(dataNasc, sfCsv);
//						//u.setSenhaCript(DateUtils.toString(dt, sfToSaveSenha));
//						u.setSenhaCript();
//						u.setDataNasc(dt);
//						u.setNome(login);
//						u.setPermissao(permissao);
//						u.setGrupo(grupo);
//
//						session.save(u);*/
//					} else {
//
//						log.debug("update ("+row+") " + login);
//						//naoEstaNoGrupo = false;
//						
//						boolean dentro = usuarioService.isUsuarioDentroDoGrupo(id,grupo);
//						
//						if(dentro) {
//							naoEstaNoGrupo = false;
//						}
//					}
//
//					// Se precisasse atualizar, colocar o saveOrUpdate aqui
//					countOk++;
//					
//					if(naoEstaNoGrupo) {
//						usuarioService.insertGrupoUsuario(id, idGrupo);
//					}
//
//					// Remove do map
//					mapLogins.remove(login);
//
//					if(countOk % 50 == 0) {
//						session.flush();
//						session.clear();
//
////						writeRow(row);
//					}
//				}
//
//			} catch (Throwable e) {
//				countError++;
//				log.error("Erro usuario linha [" + row + "]: " + e.getMessage(), e);
//				
//				writeRow(row);
//			}
//		}
//
//		if(countError == 0) {
//			writeRow("OK:"+totalRows);
//		} else {
//			if(countError == 0) {
//				writeRow("Total:"+totalRows+", CountOK:"+countOk+", CountErro:"+countError);
//			}
//		}
//
//		if(!modeSearch) {
//			session.flush();
//			session.clear();
//		}
//
//		if(modeSearch) {
//			log.debug("Logins não encontrados");
//			log.debug("> " + mapLogins.keySet());
//		}
//		
//		long timeB = System.currentTimeMillis();
//		log.debug("TimeB min: " + (timeB-timeA)/1000/60);
//
//		// Todos logins que nao estavam no csv serao excluidos
//		boolean local = Utils.isLocalMachine();
//		if(!local) {
//			String deleteLoginsQueNaoEstavamNoCsv = ParametrosMap.getInstance().get("importacao.deleteLoginsQueNaoEstavamNoCsv","1");
//			log.debug("deleteLoginsQueNaoEstavamNoCsv: " + deleteLoginsQueNaoEstavamNoCsv);
//			boolean delete = "1".equals(deleteLoginsQueNaoEstavamNoCsv);
//			if(delete) {
//				deleteLoginsQueNaoEstavamNoCsv(response,mapLogins);
//			}
//		}
//
//		session.flush();
//		session.clear();
//
//		response.countOk = countOk;
//		response.countError = countError;
//		response.list = list;
//
//		long timeC = System.currentTimeMillis();
//		log("Fim importação usuarios countOk[" + countOk + "], error (" + countError + ") , usuarios.size(): " + list.size() + " Time min: " + ((timeC-timeA)/1000/60));
//		log.debug("TimeC min: " + (timeC-timeA)/1000/60);
//		
//		return response;
//	}
//
//	private void writeRow(int i) {
//		writeRow(String.valueOf(i));
//	}
//	
//	private void writeRow(String i) {
//		if(dirIn == null) {
//			logError.error("ahah");
//			String entrada = ParametrosMap.getInstance().get("pastaEntrada");
//			dirIn = new File(entrada);
//			if (!dirIn.exists()) {
//				logError.error("Dir de entrada [" + dirIn + "] nao encontrado.");
//				return;
//			}
//		}
//
//		File f = new File(dirIn,"import_usuarios_debug.txt");
//		try {
//			FileUtils.writeByteArrayToFile(f,String.valueOf(i).getBytes());
//		} catch (IOException e) {
//			logError.error(e.getMessage(), e);
//		}		
//	}
//
//	protected void deleteLoginsQueNaoEstavamNoCsv(ImportarArquivoResponse response, Map<String, String> mapLogins) {
//		Set<String> loginsToDelete = mapLogins.keySet();
//		
//		int count = 0;
//		int countError = 0;
//		
//		if(loginsToDelete.size() > 0) {
//			
//			int total = loginsToDelete.size();
//			
//			log.debug("Deletando " + total + " usuários que não estavam no csv");
//			log.debug("Keys: ["+loginsToDelete+"]");
//			
//			usuarioService.setLogSistemaOn(false);
//			
//			for (String login : loginsToDelete) {
//				// Deleta usuario que nao estava no csv
//				Usuario u = usuarioService.findByLogin(login);
//				
//				if(!u.getCanDelete() || !u.isTipoSistemaImportacao()) {
//					continue;
//				}
//				
//				try {
//					log.debug("Deletando user " + u.toStringIdDesc());
//					usuarioService.delete(null, u);
//					log.debug("User deletado com sucesso");
//					
//					count++;
//					
//					log.debug("Count["+count+"], Total ["+total+"]");
//					
//				} catch (DomainException e) {
//					countError++;
//					log.error("Não foi possível excluir usuário [] ao importar: " + e.getMessage(), e);
//				}
//			}
//		}
//		
//		response.countDeleteOk = count;
//		response.countDeleteError = countError;
//	}
//
//	private void log(String string, Long idEmpresa) {
//		JobInfo.getInstance().lastMessageImportacao = string;
//		log.debug(string);
//	}
//
//	@Override
//	public Usuario get(Long id) {
//		return null;
//	}
//}
