package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.ConversaNotification;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.LocationVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.NotificationChatVO;
import br.livetouch.livecom.domain.vo.StatusMensagemUsuarioVO;
import br.livetouch.livecom.domain.vo.ThumbVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ConversaPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	
	public ConversaVO conversaVo;
	public Long id;
	private MensagemConversa conversa;
	private UsuarioLoginField tUser;
	
	public int maxRows;
	public int page;
	
	/**
	 * TODO Zerar msgs nao entregues de callback (ex: check2cinza)
	 */
	public int reload;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		IntegerField tId = null;
		form.add(tId = new IntegerField("id", true));
		form.add(tUser = new UsuarioLoginField("user_id", false, usuarioService, getEmpresa()));
		form.add(new IntegerField("page"));
		form.add(new IntegerField("maxRows"));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(new TextField("reload"));
		
		form.add(tMode = new TextField("mode"));
		
		tId.setFocus(true);
		tMode.setValue("json");

		form.add(new Submit("Enviar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			boolean admin = false;
			
			Usuario user = tUser.getEntity();
			conversa = mensagemService.getMensagemConversa(id);
			
			if (conversa == null) {
				return new MensagemResult("NOK","Conversa não encontrada");
			}
			
			if(user == null) {
				return new MensagemResult("NOK","Usuario não encontrado");
			}
			
			Long userId = user.getId();
			if (conversa.isGrupo()) {
				Grupo grupo = conversa.getGrupo();
				
				admin = conversa.isAdminGrupo(user);
				
				boolean participa = grupoService.isUsuarioDentroDoGrupo(grupo, user);
				
				if (!participa) {
					return new MensagemResult("NOK","Usuário não tem permissão para visualizar essa conversa.");
				}
				
			} else {
				if (!conversa.isFromTo(user)) {
					return new MensagemResult("NOK","Usuário não tem permissão para visualizar essa conversa");
				}
			}
			
			if(maxRows == 0) {
				maxRows = 100;
			}

			// Traz as últimas conversas
			List<Mensagem> mensagens = mensagemService.getMensagensByConversa(conversa, user, page, maxRows, false);

			Mensagem.sortByIdLastFirst(mensagens);
			
			ConversaNotification notifications = conversaNotificationService.findByConversationUser(conversa.getId(), userId);
			
			ConversaVO vo = new ConversaVO();
			vo.setConversa(user,conversa, mensagens);
			vo.setNotifications(new NotificationChatVO(notifications));
			vo.setAdmin(admin);
			
			if(isWsVersion3()) {
				Response r = Response.ok("OK");
				r.conversa = vo;
				return r;
			}
			
			return vo;
		}
		return new MensagemResult("NOK","Erro ao listar mensagens.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("conversa", ConversaVO.class);
		x.alias("msg", MensagemVO.class);
		x.alias("arquivo", FileVO.class);
		x.alias("statusMensagemUsuario", StatusMensagemUsuarioVO.class);
		x.alias("thumb", ThumbVO.class);
		x.alias("loc", LocationVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
