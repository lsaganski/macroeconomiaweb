package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.StatusPost;

@Repository
public interface StatusPostRepository extends net.livetouch.tiger.ddd.repository.Repository<StatusPost> {

	List<StatusPost> findAll(Empresa empresa);

	StatusPost findByCodigo(StatusPost status, Empresa empresa);

}