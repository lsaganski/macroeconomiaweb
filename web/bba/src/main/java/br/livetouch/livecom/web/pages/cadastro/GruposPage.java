package br.livetouch.livecom.web.pages.cadastro;

import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LinkDecorator;


@Controller
@Scope("prototype")
public class GruposPage extends LivecomLogadoPage {

	public PaginacaoTable table = new PaginacaoTable();
	public ActionLink detalhesLink = new ActionLink("detalhes", getMessage("detalhes.label"), this, "detalhes");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public Form form = new Form();
	public String msgErro;
	public Long id;
	private Grupo filtro;
	
	public boolean escondeChat = true;
	
	public String aba;

	@Override
	public void onInit() {
		super.onInit();
		
		if(clear) {
			getContext().removeSessionAttribute(Grupo.SESSION_FILTRO_KEY);
		}

		table();

		form();
	}

	private void table() {
		Column c = new Column("id",getMessage("codigo.label"));
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("nome", getMessage("nome.label"));
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);

		ActionLink[] links = new ActionLink[] { detalhesLink, deleteLink };
		c = new Column("detalhes",getMessage("coluna.detalhes.label"));
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	public void form() {
		form.setMethod("post");

		form.add(new IdField());

		TextField tNome = new TextField("nome", getMessage("nome.label"), true);
		tNome.setAttribute("class", "placeholder input borda lupa");
		/*tNome.setAttribute("rel", getMessage("buscar.grupo.label"));*/
		tNome.setAttribute("placeholder", getMessage("buscar.grupo.label"));
		tNome.setAttribute("autocomplete", "off");
		tNome.setFocus(false);
		form.add(tNome);
		
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip botao export");
		form.add(tExportar);

//		GrupoPermissaoPickList grupo = new GrupoPermissaoPickList(permissaoService);
//		grupo.setSize(650);
//		form.add(grupo);

		form.add(new Submit("Consultar", getMessage("consultar.label"), this, "filtrar"));
//		form.add(new Submit("Novo", this, "novo"));
	}

	@Override
	public void onGet() {
		super.onGet();

		if(id != null) {
			Grupo grupo = grupoService.get(id);
			if(grupo != null) {
				form.copyFrom(grupo);
			}
		}
	}

	public boolean filtrar() {
		if(form.isValid()) {
			filtro = (Grupo) getContext().getSessionAttribute(Grupo.SESSION_FILTRO_KEY);
			if(filtro == null) {
				filtro = new Grupo();
				getContext().setSessionAttribute(Grupo.SESSION_FILTRO_KEY, filtro);
			}
			form.copyTo(filtro);
			return true;
		
		}
		return false;
	}

	public boolean novo() {
		setRedirect(GruposPage.class);
		return false;
	}	

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Grupo grupo = grupoService.get(id);
			String nome = grupo.getNome();
			grupoService.delete(getUsuario(),grupo, false);
			setRedirect(GruposPage.class);
			setMessageSesssion("Grupo "+nome+" deletado com sucesso.", "Grupo excluído.");
		} catch (DataIntegrityViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.grupo.excluir.error");
		} catch (DomainException e) {
			this.msgErro = e.getMessage();
		} catch (Exception e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.grupo.excluir.error");
		} 
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();
	}
	
	public boolean exportar(){
		String csv = grupoService.csvGrupos(getEmpresa());
		download(csv, "grupos.csv");
		return true;
	}
}
