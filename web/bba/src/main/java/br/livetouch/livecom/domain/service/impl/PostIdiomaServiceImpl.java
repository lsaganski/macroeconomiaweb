package br.livetouch.livecom.domain.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostIdioma;
import br.livetouch.livecom.domain.repository.PostIdiomaRepository;
import br.livetouch.livecom.domain.service.PostIdiomaService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class PostIdiomaServiceImpl implements PostIdiomaService {
	@Autowired
	private PostIdiomaRepository rep;

	@Override
	public PostIdioma get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(PostIdioma ci) throws DomainException {
		rep.saveOrUpdate(ci);
	}

	@Override
	public void delete(PostIdioma ci) throws DomainException {
		rep.delete(ci);
	}
	
	@Override
	public List<PostIdioma> findAll() {
		return rep.findAll();
	}

	@Override
	public Set<PostIdioma> findByPost(Post p) {
		return rep.findByPost(p);
	}

}
