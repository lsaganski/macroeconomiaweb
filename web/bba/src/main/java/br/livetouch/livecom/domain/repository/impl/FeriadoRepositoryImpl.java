package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Feriado;
import br.livetouch.livecom.domain.repository.FeriadoRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class FeriadoRepositoryImpl extends StringHibernateRepository<Feriado> implements FeriadoRepository {

	public FeriadoRepositoryImpl() {
		super(Feriado.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Feriado> findAll(Empresa empresa) {
		StringBuffer sql = new StringBuffer("from Feriado f where f.empresa=:empresa ");

		Query q = createQuery(sql.toString());

		q.setParameter("empresa", empresa);

		return q.list();
	}

}