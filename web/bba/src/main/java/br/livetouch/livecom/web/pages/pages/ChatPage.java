package br.livetouch.livecom.web.pages.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;

@Controller
@Scope("prototype")
public class ChatPage extends LivecomLogadoPage {
	public Long user;
	public Long grupo;
	
	@Override
	public void onInit() {
		super.onInit();
	}
	
	@Override
	public String getTemplate() {
		return getPath();
	}
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.ENVIAR_MENSAGEM)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

}
