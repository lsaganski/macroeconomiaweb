package br.livetouch.livecom.jobs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.kinesis.model.InvalidArgumentException;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.domain.service.TemplateEmailService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.sender.Sender;
import br.livetouch.livecom.sender.SenderFactory;
import br.livetouch.livecom.sender.SenderMarketing;
import br.livetouch.livecom.web.pages.admin.ConvidarUsuariosPage;
import br.livetouch.pushserver.lib.PushNotification;
import br.livetouch.pushserver.lib.UsuarioToPush;

@Service
public class EmailMarketingJob extends SpringJob {

	protected static final Logger log = Log.getLogger(ConvidarUsuariosPage.class);

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private GrupoService grupoService;
	
	@Autowired
	protected LogService logService;

	@Autowired
	protected TemplateEmailService templateService;

	@Autowired
	protected EmpresaService empresaService;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void execute(Map<String, Object> params) throws Exception {
		log.debug("EmailMarketingJob.execute()");

		int count = 0;

		if (params != null) {
			List<Long> grupos_ids = (List<Long>) params.get("grupos_ids");
			List<Long> usuarios_ids = (List<Long>) params.get("usuarios_ids");
			String status = (String) params.get("status");
			String subject = (String) params.get("subject");
			Long templateId = (Long) params.get("templateId");
			Usuario userInfo = (Usuario) params.get("userInfo");
			
			log("status: " + status);
			log(">> " + grupos_ids);
			log(">> " + usuarios_ids);
			
			Empresa empresa = (Empresa) params.get("empresa");
			List<Usuario> usuarios = new ArrayList<>();
			usuarios.addAll(getUsuariosGrupos(grupos_ids,userInfo,status));
			usuarios.addAll(getUsuarios(usuarios_ids,userInfo));
			
			count = usuarios.size();
			
			if(usuarios.size() > 0) {
				log("enviando e-mail para " + count + " usuários");
				sendEmail(empresa, usuarios, subject, templateId);
			}
			
		}
		log.debug("EmailMarketingJob, push_count: " + count);
	}
	
	private List<Usuario> getUsuarios(List<Long> ids, Usuario userInfo) {
		log.debug("Busca usuarios para enviar e-mail ids: " + ids);
		List<Usuario> list = new ArrayList<>();
		if(ids != null) {
			List<Usuario> usuarios = usuarioService.findAllByIds(ids);
			if(usuarios != null && usuarios.size() > 0) {
				list.addAll(usuarios);
			}
		}
		return list;
	}

	private List<Usuario> getUsuariosGrupos(List<Long> ids, Usuario userInfo,String status) {
		log.debug("enviarEmailParaGrupos(): " + ids + ", status: " + status);
		List<Usuario> list = new ArrayList<>();
		if(ids != null) {
			List<Grupo> grupos = grupoService.findAllByIds(ids);
			for (Grupo g : grupos) {
				List<Usuario> usuarios = usuarioService.findAllByStatusAtivacaoByGrupo(g,0,0, status);
				if(usuarios != null && usuarios.size() > 0) {
					list.addAll(usuarios);
				}
			}
		}
		return list;
	}
	
	private int sendEmail(Empresa empresa, List<Usuario> usuarios, String subject, Long templateEmail) throws InvalidArgumentException {
		if(usuarios != null && usuarios.size() > 0) {
			Sender sender = SenderFactory.getSender(applicationContext, SenderMarketing.class);
			List<UsuarioToPush> users = new ArrayList<>();
			for (Usuario u : usuarios) {
				UsuarioToPush up = new UsuarioToPush();
				up.setCodigo(u.getLogin());
				up.setEmail(u.getEmail());
				up.setNome(u.getNome());
				up.setIdLivecom(u.getId());
				Map<String, Object> params = new HashMap<>();
				params.put("nome", u.getNome());
				params.put("email", u.getEmail());
				up.setParams(params);
				
				users.add(up);
			}
			PushNotification not = new PushNotification();
			not.setIsEmail(true);
			not.setTitulo(subject);
			not.setUsuarios(users);
			sender.send(empresa, not, templateEmail);
			return usuarios.size();
		}
		return 0;
	}
	
	protected void log(String msg) {
		log.debug(msg);
	}
}
