package br.livetouch.livecom.domain.repository.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.ConversaNotification;
import br.livetouch.livecom.domain.repository.ConversaNotificationRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class ConversaNotificationRepositoryImpl extends StringHibernateRepository<ConversaNotification> implements ConversaNotificationRepository {

	public ConversaNotificationRepositoryImpl() {
		super(ConversaNotification.class);
	}

	@Override
	public ConversaNotification findByConversationUser(Long conversationId, Long userId) {
		StringBuffer sb = new StringBuffer();
		sb.append(" FROM ConversaNotification c WHERE 1=1");
		sb.append(" AND c.usuario.id = :userId AND c.conversa.id = :conversationId");
		Query q = createQuery(sb.toString());
		q.setParameter("userId", userId);
		q.setParameter("conversationId", conversationId);
		q.setCacheable(true);
		return (ConversaNotification) (q.list().size() > 0 ? q.list().get(0) : null);
	}

}