package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.TagAuditoria;
import br.livetouch.livecom.domain.repository.TagRepository;
import br.livetouch.livecom.domain.vo.TagFilter;

@Repository
public class TagRepositoryImpl extends LivecomRepository<Tag> implements TagRepository {

	public TagRepositoryImpl() {
		super(Tag.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> findAllByFilter(TagFilter filter, Empresa empresa) {
		StringBuffer sb = new StringBuffer();

		sb.append("from Tag t ");

		sb.append(" where 1=1 ");

		if (StringUtils.isNotEmpty(filter.nome)) {
			sb.append(" and (t.nome like :nome)");
		}

		if (filter.notIds != null && filter.notIds.size() > 0) {
			sb.append(" and t.id not in (:notIds) ");
		}
		
		sb.append(" AND t.empresa = :empresa");
		
		sb.append(" order by trim(t.nome) ASC ");
		
		Query query = createQuery(sb.toString());
		if (StringUtils.isNotEmpty(filter.nome)) {
			query.setParameter("nome", "%" + filter.nome + "%");
		}
		if(filter.notIds != null && filter.notIds.size() > 0) {
			query.setParameterList("notIds", filter.notIds);
		}
		query.setParameter("empresa", empresa);

		query.setCacheable(true);
		
		if (filter.page > 0) {
			int firstResult = filter.page * filter.maxRows;
			query.setFirstResult(firstResult);
			query.setMaxResults(filter.maxRows);
		}
		
		query.setMaxResults(filter.maxRows);
		
		List<Tag> tags = query.list();

		return tags;
	}

	@Override
	public boolean exists(String nome, Empresa empresa) {
		StringBuffer sb = new StringBuffer("select count(*) from Tag t where t.nome = ? AND t.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setParameter(0, nome);
		q.setParameter("empresa", empresa);
		long count = getCount(q);
		return count > 0;
	}
	
	@Override
	public void saveOrUpdate(TagAuditoria a) {
		getSession().saveOrUpdate(a);
	}

	@Override
	public void deleteAll(Empresa empresa) {
		Query q = createQuery("DELETE FROM Tag t WHERE t.empresa = :empresa");
		q.setParameter("empresa", empresa);
		q.executeUpdate();
	}

	@Override
	public List<Tag> findAll(Empresa empresa) {
		if(empresa != null) {
			return query("from Tag t where t.empresa.id=? order by t.nome",false,empresa.getId());
		}
		return query("from Tag order by nome",false);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> findAllByKeys(List<Long> ids, Empresa empresa) {
		StringBuffer sb = new StringBuffer("FROM Tag t WHERE t.empresa = :empresa AND t.id IN(:ids)");
		Query q = createQuery(sb.toString());
		q.setParameter("empresa", empresa);
		q.setParameterList("ids", ids);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> findByNome(String nome, Empresa empresa) {
		Query q = createQuery("FROM Tag t WHERE t.nome = :nome AND t.empresa.id = :empresaId");
		q.setParameter("nome", nome);
		q.setParameter("empresaId", empresa.getId());
		return q.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Tag findByName(Tag tag, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Tag t where t.nome = :nome");
		sb.append(" and t.empresa = :empresa");
		
		if(tag.getId() != null) {
			sb.append(" and t.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("empresa", empresa);
		q.setParameter("nome", tag.getNome());
		
		if(tag.getId() != null) {
			q.setParameter("id", tag.getId());
		}
		
		List<Tag> list = q.list();
		Tag t = (Tag) (list.size() > 0 ? q.list().get(0) : null);
		return t;
	}

}