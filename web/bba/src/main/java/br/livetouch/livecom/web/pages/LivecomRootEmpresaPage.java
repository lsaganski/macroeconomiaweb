package br.livetouch.livecom.web.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import br.livetouch.livecom.web.pages.pages.MuralPage;

@Controller
@Scope("prototype")
public class LivecomRootEmpresaPage extends LivecomLogadoPage {
	
	@Override
	public void onInit() {
		super.onInit();
		showMenuRootEmpresa = getUserInfoVO().isRootEmpresa();
		escondeChat = true;
		bootstrap_on = true;
	}

	public boolean onSecurityCheck() {
		Usuario u = getUserInfo();
		boolean logado = u != null;
		if (logado) {
			if(u.isRootEmpresa() || u.isRoot()) {
				return true;
			}
		}
		setRedirect(MuralPage.class);
		return false;
	}
}
