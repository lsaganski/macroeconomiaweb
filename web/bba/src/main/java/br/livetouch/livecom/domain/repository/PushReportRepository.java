package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PushReport;
import br.livetouch.livecom.domain.vo.PushReportFiltro;

@Repository
public interface PushReportRepository extends net.livetouch.tiger.ddd.repository.Repository<PushReport> {

	List<PushReport> findReportByFilter(PushReportFiltro filtro, Empresa empresa);

	List<PushReport> findByPost(List<Post> posts);

	void delete(List<PushReport> pushReports);

}