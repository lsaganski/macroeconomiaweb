package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Cidade;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.enums.Estado;
import net.livetouch.tiger.ddd.DomainException;

public interface CidadeService extends Service<Cidade> {

	List<Cidade> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Cidade f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Cidade f) throws DomainException;

	List<Cidade> findAllByEstado(Estado estado);

	String csvCidade();

	List<Cidade> findByNome(String nome, Estado estado, int page, int maxRows);

	Long countByNome(String nome, Estado estado, int page, int maxRows);

	Cidade findByName(Cidade cidade, Empresa empresa);

}
