package br.livetouch.livecom.rest.resource;

import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioAuditoriaVO;
import br.livetouch.livecom.domain.vo.RelatorioEnvioPostVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.web.pages.ws.Response;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/report/auditorias")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class AuditoriaResource extends MainResource {

	protected static final Logger log = Log.getLogger(AuditoriaResource.class);
	
	@GET
	public List<RelatorioAuditoriaVO> getAuditorias(@BeanParam RelatorioFiltro filtro) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return null;
		}

		setAttributeSession(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		filtro.setEmpresaId(getEmpresa().getId());

		if (filtro.getUsuarioId() != null) {
			Usuario usuario = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(usuario);
			filtro.setUsuarioNome(usuario.getNome());
			filtro.setUsuarioLogin(usuario.getLogin());
		}
		List<LogAuditoria> audits = auditoriaService.reportAuditoria(filtro);
		return RelatorioAuditoriaVO.fromList(audits);
	}
	
	@GET
	@Path("/post")
	public Response getEnvioPosts(@BeanParam RelatorioFiltro filtro) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return Response.error("Ação não autorizada");
		}
		
		setAttributeSession(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		filtro.setEmpresaId(getEmpresa().getId());

		if (filtro.getUsuarioId() != null) {
			Usuario usuario = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(usuario);
			filtro.setUsuarioNome(usuario.getNome());
			filtro.setUsuarioLogin(usuario.getLogin());
		}
		List<RelatorioEnvioPostVO> audits = auditoriaService.envioPosts(filtro);
		Long count = auditoriaService.countEnvioPosts(filtro);
		Response r = Response.ok("OK");
		r.relatorioPosts = audits;
		r.count = count;

		return r;
	}
}