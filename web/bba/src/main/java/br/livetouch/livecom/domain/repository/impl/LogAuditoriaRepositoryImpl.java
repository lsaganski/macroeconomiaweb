package br.livetouch.livecom.domain.repository.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.repository.LogAuditoriaRepository;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.spring.StringHibernateRepository;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public class LogAuditoriaRepositoryImpl extends StringHibernateRepository<LogAuditoria> implements LogAuditoriaRepository {
	
	public LogAuditoriaRepositoryImpl() {
		super(LogAuditoria.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LogAuditoria> reportAuditoria(RelatorioFiltro filtro) throws DomainException {
		Query query = queryReportAuditoria(filtro);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		List<LogAuditoria> audits = query.list();

		return audits;
	}

	public Query queryReportAuditoria(RelatorioFiltro filtro) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;
		
		AuditoriaEntidade entidade = filtro.getTipoEntidadeEnum();
		AuditoriaAcao acao = filtro.getAcaoEnum();

		if (StringUtils.isNotEmpty(filtro.getDataIni())) {
			filtro.setDataInicial(DateUtils.toDate(filtro.getDataIni()));
		}
		if (StringUtils.isNotEmpty(filtro.getDataFim())) {
			filtro.setDataFinal(DateUtils.toDate(filtro.getDataFim()));
		}

		Long empresa = filtro.getEmpresaId();
		
		Usuario usuario = filtro.getUsuario();
		
		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer(" from LogAuditoria a ");
		sb.append(" where 1=1 ");
		
		if (dataInicio != null) {
			sb.append(" and a.data >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and a.data <= :dataFim");
		}

		if (usuario != null) {
			sb.append(" and a.usuario = :usuario");
		}
		
		if (entidade != null) {
			sb.append(" and a.entidade = :entidade");
		}
		
		if (acao != null) {
			sb.append(" and a.acao = :acao");
		}

		if (empresa != null) {
			sb.append(" and a.empresa.id = :empresa");
		}
		
		sb.append(" order by a.data desc");

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}
		
		if (entidade != null) {
			q.setParameter("entidade", entidade);
		}
		
		if (acao != null) {
			q.setParameter("acao", acao);
		}

		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		return q;

	}

	private Query queryEnvioPosts(RelatorioFiltro filtro, boolean count) throws DomainException {
		Date dataInicio = null;
		Date dataFim = null;
		
		AuditoriaEntidade entidade = AuditoriaEntidade.POST;

		if (StringUtils.isNotEmpty(filtro.getDataIni())) {
			filtro.setDataInicial(DateUtils.toDate(filtro.getDataIni()));
		}
		if (StringUtils.isNotEmpty(filtro.getDataFim())) {
			filtro.setDataFinal(DateUtils.toDate(filtro.getDataFim()));
		}

		Long empresa = filtro.getEmpresaId();
		
		Usuario usuario = filtro.getUsuario();
		
		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();

		if(count) {
			sb.append("select count(a) from LogAuditoria a ");
		} else {
			sb.append("select max(a) from LogAuditoria a ");
		}

		sb.append(" where 1=1 ");
		
		if (dataInicio != null) {
			sb.append(" and a.data >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and a.data <= :dataFim");
		}

		if (usuario != null) {
			sb.append(" and a.usuario = :usuario");
		}
		
		if (entidade != null) {
			sb.append(" and a.entidade = :entidade");
		}
		
		if(filtro.isPrioritario()) {
			sb.append(" and a.idEntity in (select id from Post where prioritario = 1)");
		}

		if (empresa != null) {
			sb.append(" and a.empresa.id = :empresa");
		}
		sb.append(" AND a.idEntity is not null");

		if(!count) {
			sb.append(" group by a.idEntity ");
			sb.append(" order by min(a.data) desc");
		}

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}
		
		if (entidade != null) {
			q.setParameter("entidade", entidade);
		}
		
		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		return q;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LogAuditoria> envioPosts(RelatorioFiltro filtro) throws DomainException {

		Query q = queryEnvioPosts(filtro, false);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		q.setFirstResult(firstResult);
		q.setMaxResults(filtro.getMax());

		List<LogAuditoria> audits = q.list();

		return audits;
	}

	@Override
	public Long countEnvioPosts(RelatorioFiltro filtro) throws DomainException {

		Query query = queryEnvioPosts(filtro, true);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	@Override
	public LogAuditoria findFirstReport(LogAuditoria a) {
		StringBuffer sb = new StringBuffer("FROM LogAuditoria l WHERE 1=1 ");

		if(a.getIdEntity() != null) {
			sb.append(" and l.idEntity = :idEntity ");
		}

		if(a.getId() != null) {
			sb.append(" and l.id != :idLog ");
		}

		if(a.getEntidade() != null) {
			sb.append(" and l.entidade = :entidade ");
		}

		sb.append(" and l.acao =:acao");

		Query q = createQuery(sb.toString());

		if(a.getId() != null) {
			q.setParameter("idLog", a.getId());
		}

		if(a.getIdEntity() != null) {
			q.setParameter("idEntity", a.getIdEntity());
		}

		if(a.getEntidade() != null) {
			q.setParameter("entidade", a.getEntidade());
		}

		q.setParameter("acao", AuditoriaAcao.INSERIR);
		q.setMaxResults(1);
		q.setCacheable(true);
		return (LogAuditoria) (q.list().size() > 0 ? q.list().get(0) : null);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<LogAuditoria> getAuditoriasByEntity(Long id, AuditoriaEntidade entidade) {

		if(id == null) {
			return new ArrayList<>();
		}

		StringBuffer sb = new StringBuffer("FROM LogAuditoria l WHERE 1=1 ");

		sb.append("and l.idEntity = :idEntity ");

		if(entidade != null) {
			sb.append("and l.entidade = :entidade ");
		}

		sb.append("order by l.data desc");

		Query q = createQuery(sb.toString());

		q.setParameter("idEntity", id);

		if(entidade != null) {
			q.setParameter("entidade", entidade);
		}

		q.setCacheable(true);
		return q.list();
	}
}