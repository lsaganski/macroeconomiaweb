package br.livetouch.livecom.web.pages.root;


import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.web.pages.LivecomRootPage;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LinkDecorator;

/**
 * 
 */
@Controller
@Scope("prototype")
public class PermissoesPage extends LivecomRootPage {

	public PaginacaoTable table = new PaginacaoTable();
	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public Perfil permissao;

	public List<Perfil> permissaos;
	
	@Override
	public void onInit() {
		super.onInit();

		form();
		
		table();

		if (id != null) {
			permissao = perfilService.get(id);
		} else {
			permissao = new Perfil();
		}
		form.copyFrom(permissao);
	}
	
	public void form(){
		form.add(new IdField());

		TextField t = new TextField("nome", getMessage("nome.label"), true);
		t.setAttribute("autocomplete", "off");
		t.setAttribute("class", "input");
		t.setFocus(true);
		form.add(t);
		
		t = new TextField("descricao", getMessage("descricao.label"), true);
		t.setAttribute("autocomplete", "off");
		t.setAttribute("class", "input");
		form.add(t);
		
		Checkbox c = new Checkbox("padrao", getMessage("permissao.cadastro.label"));
		c.setAttribute("autocomplete", "off");
		form.add(c);
		
		c = new Checkbox("cadastrarTabelas", getMessage("permissao.cadastro.tabela"));
		form.add(c);
		
		c = new Checkbox("cadastrarUsuarios", getMessage("permissao.cadastro.usuario"));
		form.add(c);
		
		c =  new Checkbox("publicar", getMessage("publicar.label"));
		c.setAttribute("autocomplete", "off");
		form.add(c);
		
		c =  new Checkbox("editarPostagem", getMessage("editar.postagem.label"));
		c.setAttribute("autocomplete", "off");
		form.add(c);
		
		c =  new Checkbox("excluirPostagem", getMessage("excluir.postagem.label"));
		c.setAttribute("autocomplete", "off");
		form.add(c);
		
		c =  new Checkbox("comentar", getMessage("comentar.label"));
		c.setAttribute("autocomplete", "off");
		form.add(c);
		
		c =  new Checkbox("curtir", getMessage("curtir.label"));
		c.setAttribute("autocomplete", "off");
		form.add(c);
		
		c =  new Checkbox("arquivos", getMessage("menu.admin.arquivos"));
		t.setAttribute("autocomplete", "off");
		form.add(c);
		
		c =  new Checkbox("enviarMensagem", getMessage("enviar.mensagem.label"));
		t.setAttribute("autocomplete", "off");
		form.add(c);
		
		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao salvar");
		form.add(tsalvar);
		
		Submit cancelar = new Submit("novo", getMessage("cancelar.label"), this,"novo");
		cancelar.setAttribute("class", "botao cancelar");
		form.add(cancelar);

		//setFormTextWidth(form);
	}
	
	private void table() {
		Column c = new Column("id");
		c.setWidth("35px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("nome", getMessage("nome.label"));
		c.setWidth("35px");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		c = new Column("descricao", getMessage("descricao.label"));
		c.setHeaderStyle("text-align", "center");
//		c.setAttribute("nowrap", "true");
		c.setTextAlign("center");
		table.addColumn(c);
		
		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes", getMessage("detalhes.label"));
		c.setTextAlign("center");
		c.setWidth("120px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				permissao = id != null ? perfilService.get(id) : new Perfil();
				form.copyTo(permissao);

				perfilService.saveOrUpdate(permissao);

				setFlashAttribute("msg",  getMessage("msg.permissao.salvar.sucess", permissao.getNome()));
				setRedirect(getClass());

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(getClass());
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		permissao = perfilService.get(id);
		form.copyFrom(permissao);
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Perfil e = perfilService.get(id);
			if(id.equals(1L) && "livecom".equalsIgnoreCase(e.getNome())) {
				throw new DomainException(getMessage("msg.permissao.livecom.excluir.error"));
			}
			perfilService.delete(e, getUserInfo());
			setRedirect(getClass());
			setFlashAttribute("msg", getMessage("msg.permissao.excluir.sucess"));
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.permissao.excluir.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		permissaos = perfilService.findAll();

		// Count(*)
		int pageSize = 100;

		table.setPageSize(pageSize);
		table.setRowList(permissaos);
	}
}
