package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PushReport;
import br.livetouch.livecom.domain.repository.PushReportRepository;
import br.livetouch.livecom.domain.service.PushReportService;
import br.livetouch.livecom.domain.vo.PushReportFiltro;
import br.livetouch.livecom.domain.vo.PushReportVO;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;

@Service
public class PushReportServiceImpl implements PushReportService {
	@Autowired
	private PushReportRepository rep;

	@Override
	public PushReport get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(PushReport p) {
		rep.saveOrUpdate(p);
	}

	@Override
	public void delete(PushReport p) {
		rep.delete(p);
	}

	@Override
	public List<PushReport> findAll() {
		return rep.findAll();
	}

	@Override
	public List<PushReportVO> findReportByFilterVO(PushReportFiltro filtro, Empresa empresa) {
		List<PushReportVO> vos = new ArrayList<PushReportVO>();
		List<PushReport> list = findReportByFilter(filtro, empresa);
		for (PushReport pushReport : list) {
			PushReportVO vo = new PushReportVO(pushReport);
			vos.add(vo);
		}

		return vos;
	}

	@Override
	public List<PushReport> findReportByFilter(PushReportFiltro filtro, Empresa empresa) {
		return rep.findReportByFilter(filtro, empresa);
	}

	@Override
	public String csvPushReport(PushReportFiltro filtro, Empresa empresa) {
		List<PushReportVO> reports = findReportByFilterVO(filtro, empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("Id").add("Data").add("Post").add("Tipo").add("Status");
		for (PushReportVO a : reports) {
			body.add(a.getId().toString()).add(a.getData()).add(a.getPostTitulo()).add(a.getTipo()).add(String.valueOf(a.isStatus()));
			body.end();
		}
		return generator.toString();
	}

	@Override
	public List<PushReport> findByPost(List<Post> posts) {
		return rep.findByPost(posts);
	}

	@Override
	public void delete(List<PushReport> pushReports) {
		rep.delete(pushReports);
	}
}
