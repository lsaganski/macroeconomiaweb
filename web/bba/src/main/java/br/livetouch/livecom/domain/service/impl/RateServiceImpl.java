package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Rate;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.RateRepository;
import br.livetouch.livecom.domain.service.RateService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class RateServiceImpl implements RateService {
	@Autowired
	private RateRepository rep;

	@Override
	public Rate get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Rate c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Rate> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void delete(Rate c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public List<Rate> findAllByUser(Usuario u) {
		return rep.findAllByUser(u);
	}

	@Override
	public Rate findByUserPost(Usuario u, Post p) {
		return rep.findByUserPost(u,p);
	}
	
	@Override
	public int findMedia(Post p) {
		return rep.findMedia(p);
	}

	@Override
	public Long countByPost(Post p) {
		return rep.countByPost(p);
	}
}
