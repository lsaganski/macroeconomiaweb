package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.UsuarioMarcado;
import net.livetouch.extras.util.DateUtils;

public class ComentarioVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public long userId;
	public long postId;
	public String msg;
	public String nome;
	private String data;
	
	
	private String urlArquivo;
	private long timestamp;
	private String dataStr;
	private ArrayList<FileVO> arquivos;
	private String urlFoto;
	private String urlFotoThumb;
	public Long likeCount;
	public String like;
	public boolean lidaNotification;
	public List<UsuarioVO> usuariosMarcado;

	public void setComentario(Comentario c) {
		this.id = c.getId();
		
		if(c.getUsuario() != null) {
			this.userId = c.getUsuario().getId();
			this.nome = c.getUsuario().getNome();
			this.urlFoto = c.getUsuario().getUrlFoto();
			this.urlFotoThumb = c.getUsuario().getUrlThumb();
			
			Empresa e = c.getUsuario().getEmpresa();
			if(ParametrosMap.getInstance(e).getBoolean("marcacao.usuarios.on",false)) {
				List<UsuarioMarcado> usm = c.getUsuariosMarcado();
				if(usm != null && usm.size() > 0) {
					usuariosMarcado = new ArrayList<>();
					for (UsuarioMarcado usuarioMarcado : usm) {
						Usuario usuario = usuarioMarcado.getUsuario();
						if(usuario != null) {
							UsuarioVO usuarioVO = new UsuarioVO();
							usuarioVO.setUsuario(usuario);
							usuariosMarcado.add(usuarioVO);
						}
					}
				}
			}
		}
		
		this.postId = c.getPost().getId();
		this.msg = c.getComentario();
		this.lidaNotification = c.getLidaNotification();

		if(c.getData() != null) {
			this.data = DateUtils.toString(c.getData(),"dd/MM/yyyy HH:mm:Ss");
		}
		
		Date dt = c.getData();
		if(dt != null) {
			this.timestamp = dt.getTime();
			
			if(br.livetouch.livecom.utils.DateUtils.isToday(this.timestamp)) {
				this.setDataStr("Hoje");
			} else if(br.livetouch.livecom.utils.DateUtils.isYesterday(this.timestamp)) {
				this.setDataStr("Ontem");
			} else {
				this.setDataStr(net.livetouch.extras.util.DateUtils.toString(dt,"dd/MMM"));
			}
		}
		
		Set<Arquivo> list = c.getArquivos();
		if(list != null && list.size() > 0) {
			arquivos = new ArrayList<FileVO>();
			for (Arquivo a : list) {
				FileVO f = new FileVO();
				f.setArquivo(a, true);
				arquivos.add(f);
			}
		}
		
		
	}

	public Long getId() {
		return id;
	}

	public long getUserId() {
		return userId;
	}

	public long getPostId() {
		return postId;
	}

	public String getMsg() {
		return msg;
	}

	
	public String getNome() {
		return nome;
	}
	
	public String getData() {
		return data;
	}
	
	public String getUrlArquivo() {
		return urlArquivo;
	}
	
	public ArrayList<FileVO> getArquivos() {
		return arquivos;
	}
	public void setArquivos(ArrayList<FileVO> arquivos) {
		this.arquivos = arquivos;
	}

	@Override
	public String toString() {
		return "ComentarioVO [id=" + id + ", userId=" + userId + ", postId=" + postId + "]";
	}

	public String getDataStr() {
		return dataStr;
	}

	public void setDataStr(String dataStr) {
		this.dataStr = dataStr;
	}

	public String getUrlFoto() {
		return urlFoto;
	}

	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}

	public String getUrlFotoThumb() {
		return urlFotoThumb;
	}

	public void setUrlFotoThumb(String urlFotoThumb) {
		this.urlFotoThumb = urlFotoThumb;
	}
}
