package br.livetouch.livecom.domain.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.ComentarioVO;
import br.livetouch.livecom.domain.vo.UserToPushVO;
import net.livetouch.tiger.ddd.DomainException;

public interface ComentarioService extends Service<Comentario> {

	List<Comentario> findAll();
	
	@Transactional(rollbackFor=Exception.class)
	Comentario comentar(Long id,Usuario u, Post post, String texto, List<Arquivo> arquivos, String usuariosMarcados) throws DomainException, IOException;

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Comentario c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario usuario, Comentario c,boolean deleteArquivosAmazon) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario usuario, List<Long> comentarioIds,boolean deleteArquivosAmazon) throws DomainException;

	List<Comentario> findAllByUserAndPost(long userId, long postId, int page, int maxRows);
	
	List<ComentarioVO> toListVo(Usuario usuario, List<Comentario> list);

	List<Comentario> findAllComentariosNotificationByUser(Usuario user);

	@Transactional(rollbackFor=Exception.class)
	void clearNotification(Long id) throws DomainException;

	List<Long> findAllIdsByUser(Usuario u);

	List<Usuario> findAllUsuariosQueComentaramPost(Post p, Usuario u);

	List<UserToPushVO> findAllUsersQueComentaramPost(Post p, Usuario u);

	List<UserToPushVO> getUsuariosPushComentario(Usuario user, Post post);

	List<Long> findAllByPost(Post p);
	
	List<Comentario> findByPost(Post p);
	
	List<Comentario> findAllByDate(Date data);
	
}
