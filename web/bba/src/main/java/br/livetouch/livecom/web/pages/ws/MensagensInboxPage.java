package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.EntityField;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class MensagensInboxPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private EntityField<Usuario> tUserFrom;
	private EntityField<Usuario> tUserTo;
	private TextField tMode;
	
	public List<MensagemVO> list;
	private TextField tTipo;
	
	public int maxRows;
	public int page;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		form.add(tUserFrom = new EntityField<Usuario>("user_from_id", false, usuarioService));
		form.add(tUserTo = new EntityField<Usuario>("user_to_id", false, usuarioService));
		form.add(tTipo = new TextField("tipo","1=inbox, 2=outbox", true));
		form.add(new IntegerField("page"));
		form.add(new IntegerField("maxRows"));
		
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));
		
		tTipo.setValue("1");

		tMode.setValue("json");

		tUserFrom.setFocus(true);

		form.add(new Submit("Enviar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario userFrom = tUserFrom.getEntity();
			Usuario userTo = tUserTo.getEntity();

			boolean inbox = "1".equals(tTipo.getValue());

			if(inbox){
				
				List<Mensagem> mensagens = mensagemService.findAllByUser(userFrom, userTo, Mensagem.TIPO_ENTRADA, page, maxRows);
				
				list = new ArrayList<MensagemVO>();
				for (Mensagem msg : mensagens) {
					MensagemVO vo  = new MensagemVO();
					vo.setMensagem(msg, userFrom);
					list.add(vo);
				}
			} else {
				List<Mensagem> mensagens = mensagemService.findAllByUser(userFrom, userTo, Mensagem.TIPO_ENVIADA, page, maxRows);
				
				list = new ArrayList<MensagemVO>();
				for (Mensagem msg : mensagens) {
					MensagemVO vo  = new MensagemVO();
					vo.setMensagem(msg, userFrom);
					list.add(vo);
				}
			}
			return list;
		}

		return new MensagemResult("NOK","Erro ao listar mensagens.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("msg", MensagemVO.class);
		x.alias("arquivo", FileVO.class);
		super.xstream(x);
	}
}
