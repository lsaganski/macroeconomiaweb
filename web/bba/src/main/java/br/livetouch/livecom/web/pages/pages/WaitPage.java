package br.livetouch.livecom.web.pages.pages;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.LongOperation;
import br.livetouch.livecom.web.pages.admin.LivecomAdminPage;

@Controller
@Scope("prototype")
public class WaitPage extends LivecomAdminPage {

	public static final String COD_SOMENTE_EU = "SOMENTE_EU";
	public int refresh = 2;
	public int count;
	public String cod;

	@Override
	public void onGet() {
		super.onGet();

		String erro = LongOperation.getErro(cod);
		boolean running = LongOperation.isRunning(cod);
		boolean finished = LongOperation.isFinished(cod);

		if(running) {
			System.out.println("running");
		} else if(finished) {
			
			Map<String, String> params = new HashMap<String, String>();
			params.put("erro", erro);
			params.put("cod", cod);
			
			setRedirect(DemoWaitFinishPage.class,params);
			
			msg = "FIM ! " + cod;
		}
		
		count++;
	}

	
}
