package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.Usuario;

@Repository
public interface FuncaoRepository extends net.livetouch.tiger.ddd.repository.Repository<Funcao> {

	List<Object[]> findIdNomeFuncao(Long idEmpresa);

	List<Funcao> findAll(Usuario userInfo);

	Funcao findByNome(String nome, Long idEmpresa);

	Funcao findDefaultByEmpresa(Long empresaId);

	List<Funcao> findAll(Empresa empresa);

	List<Funcao> findByNome(String nome, int page, int maxRows, Empresa empresa);

	Long countByNome(String nome, int page, int maxRows, Empresa empresa);

	Funcao findByName(Funcao funcao, Empresa empresa);

	List<Object[]> findIdCodFuncao(Long idEmpresa);

	void delete(List<Funcao> funcoes);

}