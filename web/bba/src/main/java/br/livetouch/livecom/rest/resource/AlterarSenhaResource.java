package br.livetouch.livecom.rest.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.AlterarSenhaVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/alterarsenha")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class AlterarSenhaResource extends MainResource {

	protected static final Logger log = Log.getLogger(AlterarSenhaResource.class);
	
	@Autowired
	UsuarioService usuarioService;
	
	@Context
	private SecurityContext securityContext;
	
	@GET
	public Response createGet(@QueryParam("usuarioId") Long id,@QueryParam("senhaAtual") String senhaAtual, @QueryParam("senhaNova") String senhaNova, @QueryParam("confirmaSenha") String confirmaSenha) throws DomainException {
		Usuario usuario = usuarioService.get(id);
		AlterarSenhaVO alterar = new AlterarSenhaVO();
		alterar.setSenhaAtual(senhaAtual);
		alterar.setSenhaNova(senhaNova);
		alterar.setConfirmaSenha(confirmaSenha);
		usuarioService.alterarSenha(usuario, alterar);
		return Response.ok(MessageResult.ok("Alteração realizada com sucesso")).build();
	}
	
	@POST
	public Response create(AlterarSenhaVO alterar) throws DomainException {
		Usuario usuario = usuarioService.get(alterar.getUsuarioId());
		usuarioService.alterarSenha(usuario, alterar);
		return Response.ok(MessageResult.ok("Cadastro realizado com sucesso")).build();
	}

}
