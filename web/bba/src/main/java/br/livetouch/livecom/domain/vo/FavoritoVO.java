package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.Favorito;

public class FavoritoVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public long userId;
	public long postId;
	public long fileId;
	public String favorito;
	public boolean lidaNotification;

	public void setFavorito(Favorito f) {
		this.userId 	= f.getUsuario().getId();
		if (f.getPost() != null) {
			this.postId = f.getPost().getId();
		}
		if (f.getArquivo() != null) {
			this.fileId = f.getArquivo().getId();
		}
		this.favorito 	= f.isFavorito()?"1":"0";
		this.lidaNotification = f.getLidaNotification();
	}

	@Override
	public String toString() {
		return "FavoritoVO [userId=" + userId + ", postId=" + postId + ", fileId=" + fileId + ", favorito=" + favorito + "]";
	}
}
