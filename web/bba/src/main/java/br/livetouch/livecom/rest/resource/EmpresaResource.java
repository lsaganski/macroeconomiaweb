package br.livetouch.livecom.rest.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.vo.EmpresaVO;
import br.livetouch.livecom.domain.vo.Filtro;
import br.livetouch.livecom.domain.vo.TrialVO;
import br.livetouch.livecom.email.EmailUtils;
import br.livetouch.livecom.rest.domain.MessageResult;
import br.livetouch.livecom.sender.Sender;
import br.livetouch.livecom.sender.SenderConvidarTrial;
import br.livetouch.livecom.sender.SenderFactory;
import br.livetouch.livecom.utils.LivecomUtil;
import br.livetouch.livecom.web.pages.admin.LivecomPage;
import br.livetouch.pushserver.lib.PushNotification;
import br.livetouch.pushserver.lib.UsuarioToPush;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/empresa")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class EmpresaResource extends MainResource {
	protected static final Logger log = Log.getLogger(LivecomPage.class);
	
	@GET
	@Path("/filtro")
	public Response findByName(@BeanParam Filtro filtroEmpresa) {
		List<Empresa> empresas = empresaService.filterEmpresa(filtroEmpresa);
		List<EmpresaVO> list = EmpresaVO.fromList(empresas);
		return Response.ok(list).build();
	}
	
	@GET
	public Response findAll() {
		List<Empresa> empresas = empresaService.findAll();
		List<EmpresaVO> list = EmpresaVO.fromList(empresas);
		return Response.ok(list).build();
	}
	
	@GET
	@Path("/trial")
	public Response trial(@QueryParam("codigo") String codigo) {
		Empresa empresa = empresaService.findTrial(codigo);
		
		if(empresa == null) {
			return Response.ok(MessageResult.error("Código de ativação inválido")).build();
		}
		
		TrialVO trialVO = new TrialVO(empresa);
		
		return Response.ok().entity(trialVO).build();
	}
	
	@POST
	@Path("/trial/send/{id}")
	public Response sendTrial(@PathParam("id") Long id) {
		Empresa empresa = empresaService.get(id);
		
		if(empresa == null) {
			return Response.ok(MessageResult.error("Empresa não encontrada")).build();
		}
		
		Usuario usuario = empresa.getAdmin();
		
		try {
			PushNotification not = getTrialPushNotification(usuario, empresa);
			
			Sender sender = SenderFactory.getSender(applicationContext, SenderConvidarTrial.class);
			sender.sendTrial(empresa, not);
		} catch(Exception e) {
			return Response.ok(MessageResult.error("Erro ao enviar codigo trial")).build();
		}

		return Response.ok().entity(MessageResult.ok("Email com o codigo trial enviado com sucesso")).build();
	}
	
	private PushNotification getTrialPushNotification(Usuario usuario, Empresa empresa) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dominio", empresa.getDominio());
		params.put("login", usuario.getEmail());
		params.put("senha", usuario.getSenha());
		params.put("codigoTrial", empresa.getTrialCode());
		
		UsuarioToPush usuarioToPush = new UsuarioToPush();
		usuarioToPush.setEmail(usuario.getEmail());
		usuarioToPush.setCodigo(usuario.getLogin());
		usuarioToPush.setNome(usuario.getNome());
		usuarioToPush.setIdLivecom(usuario.getId());
		usuarioToPush.setParams(params);
		
		PushNotification not = new PushNotification();
		not.setIsEmail(true);
		not.setUsuario(usuarioToPush);
		return not;
	}

	@POST
	public Response create(Empresa empresa) {

		try {
			//Validar se ja existe uma empresa com o mesmo nome
			boolean existe = empresaService.findByName(empresa) != null;
			if(existe) {
				log.debug("Ja existe uma empresa com este nome");
				return Response.ok(MessageResult.error("Ja existe uma empresa com este nome")).build();
			}

			existe = empresaService.findDominio(empresa.getDominio()) != null;
			if(existe) {
				log.debug("Ja existe uma empresa com este dominio");
				return Response.ok(MessageResult.error("Ja existe uma empresa com este dominio")).build();
			}
			
			String trialCode = LivecomUtil.createRandomPassword(6);
			empresa.setTrialCode(trialCode);
			empresa.setCodigo(empresa.getNome());
			Usuario usuario = empresa.getAdmin();
			empresa.setAdmin(null);
			
			existe = usuarioService.findByEmail(usuario.getEmail()) != null;
			if(existe) {
				log.debug("Ja existe um usuario com este email");
				return Response.ok(MessageResult.error("Ja existe um usuario com este email")).build();
			}
			
			Response response = execSalvar(empresa, usuario);
			
			if (usuario != null && usuario.getStatus().equals(StatusUsuario.NOVO) && response.getStatus() == 200) {
				usuarioService.createPassword(usuario);
				usuario.setStatus(StatusUsuario.ENVIADO_CONVITE);
				usuarioService.saveAudit(null, usuario);
				
				TrialVO trialVO = new TrialVO(empresa, usuario);
				
				PushNotification not = getTrialPushNotification(usuario, empresa);
				
				Sender sender = SenderFactory.getSender(applicationContext, SenderConvidarTrial.class);
				sender.sendTrial(empresa, not);
				return Response.ok().entity(trialVO).build();
			}
			
			log.debug("Não foi possivel efetuar o cadastro da empresa");
			return Response.ok(MessageResult.error("Não foi possivel efetuar o cadastro da empresa")).build();
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel efetuar o cadastro da empresa")).build();
		} catch (Throwable e) {
			e.printStackTrace();
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	public Response execSalvar(Empresa empresa, Usuario usuario) {
			try {

				String nome = usuario.getNome();
				String email = usuario.getEmail();
				String fixo = usuario.getTelefoneFixo();
				usuario.setTelefoneFixo(StringUtils.replace(fixo, "-", ""));
				if(!EmailUtils.isEmailValido(email)){
					log.debug("Email inválido: "+email+" verifique se o domínio está correto.");
					return Response.ok(MessageResult.error("Email inválido: "+email+" verifique se o domínio está correto.")).build();
				}
				
				List<Parametro> parametros = empresaService.parametrosTrial(nome, empresa);
				
				usuario.setStatus(StatusUsuario.NOVO);
				usuario.setEmpresa(empresa);

				usuario.setEmail(email);
				usuario.setLogin(email);
				usuario.setNome(nome);
				usuario.setPerfilAtivo(true);

				empresaService.saveOrUpdate(empresa, usuario, null, parametros);

				return Response.ok().entity(MessageResult.ok("Cadastro realizado com sucesso")).build();

			} catch (Exception e) {
				log.debug("Erro ao criar a empresa: "+ e.getMessage(), e);
				return Response.ok(MessageResult.error("Erro ao criar a empresa: "+ e.getMessage())).build();
			}
		}

}
