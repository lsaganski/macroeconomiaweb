package br.livetouch.livecom.web.pages.oauth;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.rest.domain.UserPrincipal;
import br.livetouch.livecom.rest.oauth.LivecomOAuthProvider;
import br.livetouch.livecom.rest.oauth.LivecomToken;
import br.livetouch.livecom.rest.oauth.RequestTokenMap;
import br.livetouch.livecom.rest.oauth.VerifierTokenMap;
import br.livetouch.livecom.web.pages.LogonPage;
import br.livetouch.livecom.web.pages.admin.LivecomPage;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.HiddenField;
import net.sf.click.control.PasswordField;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class AuthorizePage extends LivecomPage{
	private static Logger log = Logger.getLogger(LivecomOAuthProvider.class);
	
	public Form form = new Form();
	private TextField tLogin;
	private PasswordField tSenha;
	private HiddenField tToken;
	
	@Autowired
	private LivecomOAuthProvider provider;
	
	@Override
	public void onInit() {
		super.onInit();
		
		
		String oauthToken = getParam("oauth_token");
		
		tLogin = new TextField("login", true);
		form.add(tLogin);
		tLogin.setFocus(true);
		form.add(tSenha = new PasswordField("senha", true));
		
		if(StringUtils.isEmpty(oauthToken) && tToken == null) {
			setRedirect(LogonPage.class);
			return;
		}
		
		LivecomToken requestToken = provider.getRequestToken(oauthToken);
		if(requestToken == null) {
			setRedirect(LogonPage.class);
			return;
		}
		form.add(tToken = new HiddenField("oauth_token", oauthToken));
		Submit btlogon = new Submit("logon", "Entrar");
		btlogon.setAttribute("class", "botao");
		form.add(btlogon);
	}

	@Override
	public String getTemplate() {
		return super.getTemplate();
	}
	
	@Override
	public void onPost() {
		super.onPost();
		
		if(form.isValid()) {
			String login = tLogin.getValue();
			String senha = tSenha.getValue();
			String token = tToken.getValue();
			
			try {
				Usuario usuario = usuarioService.login(login, senha);
				
				final LivecomToken requestToken = provider.getRequestToken(token);
				if(requestToken == null) {
					tToken.setError("Desculpe, mas o token " + token + " não foi localizado");
					return;
				}
				String uri = authorize(token, requestToken,usuario);
				log.debug("Redirecionando o usuário para o callback " + uri);
				getContext().getResponse().sendRedirect(uri);
				return;
			} catch (DomainException e) {
				form.setError(e.getMessage());
				return;
			} catch (URISyntaxException e) {
				form.setError("Desculpe, ocorreu um erro interno no servidor.");
				return;
			} catch (IOException e) {
				form.setError("Desculpe, não foi possível redicionar para o callback da aplicação");
				return;
			}
		}
		form.setError("Campo login e senha são obrigatórios");
	}	
	
	private String authorize(String token, final LivecomToken requestToken, Usuario usuario) throws URISyntaxException {
		UserPrincipal principal = new UserPrincipal(usuario);
		Set<String> roles = new HashSet<>();
		roles.add(usuario.getPermissao().getNome().toLowerCase());
		roles.add("logado");
		// Cria o código verificador
		requestToken.authorize(principal, roles);
		RequestTokenMap.getInstance().put(requestToken.getToken(), requestToken);
		String verifier = LivecomToken.newUUIDString();
		VerifierTokenMap.getInstance().put(requestToken.getToken(), verifier);
		String callbackUrl = requestToken.getCallbackUrl();
		callbackUrl += String.format("?oauth_verifier=%s&oauth_token=%s",verifier,token);
		return callbackUrl;
	}
}
