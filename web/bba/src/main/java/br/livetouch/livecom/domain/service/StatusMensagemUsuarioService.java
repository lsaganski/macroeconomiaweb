
package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.StatusMensagemUsuario;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

public interface StatusMensagemUsuarioService extends Service<StatusMensagemUsuario> {

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Usuario usuarioSistema,StatusMensagemUsuario g) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario usuarioSistema, StatusMensagemUsuario g) throws DomainException;
	
	List<StatusMensagemUsuario> findAllByUserConversa(Usuario u, MensagemConversa c);

	List<StatusMensagemUsuario> findAllByConversa(MensagemConversa c);
	
	List<StatusMensagemUsuario> findAllByMensagem(Mensagem m);

	/**
	 * @param userId
	 * @param conversaId
	 * @param status entregue=1, lida=2
	 * @return
	 */
	int updateStatusConversaGrupo(Long userId, Long conversaId, int status);

	/**
	 * status: 1 - recebida, 2 lida
	 */
	int updateStatusMensagemGrupo(Long userId, Long mensagemId, Integer status);

	void delete(Usuario u, Grupo grupo);

	StatusMensagemUsuario findByMsgUsuario(Mensagem msg, Usuario u);
	
}
