package br.livetouch.livecom.web.pages.training;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.vo.BuscaPost;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.spring.SearchInfo;

@Controller
@Scope("prototype")
public class IndexPage extends TrainingPage {

	public List<PostVO> posts;
//	public Usuario usuario;
	public PostFilter filterSession;

	@Override
	public void onInit() {
		super.onInit();
		
		filterSession = PostFilter.get(getContext());
		filterSession.init(getContext());
	}

	@Override
	public void onGet() {
		super.onGet();
	}
	
	@Override
	public void onPost() {
		super.onPost();
	}

	@Override
	public void onRender() {
		super.onRender();

//		Usuario user = getUserInfo();
//		usuario = usuarioService.get(user.getId());

		List<Post> list = null;

		Long categId = filterSession.categId;
		String filter = filterSession.filter;
		String search = filterSession.search;
		String tags =	filterSession.tags;
		String sort = filterSession.sort;

		SearchInfo searchInfo = new SearchInfo(page,10);
		if(StringUtils.isNotEmpty(sort)) {
			searchInfo.sort = sort;
		}

		if(StringUtils.equalsIgnoreCase("favorites", filter)) {
			filterSession.clear();
			list = favoritoService.findAllPostsByUser(getUsuario(),searchInfo);
		}
		else if(StringUtils.equalsIgnoreCase("playlist", filter)) {
			filterSession.clear();
			list = playlistService.findAllPostsByUser(getUsuario(),page,10);
		} else if(StringUtils.equalsIgnoreCase("history", filter)) {
			filterSession.clear();
			list = historicoService.findAllPostsByUser(getUsuario(),page,10);
		} else if(StringUtils.isNotEmpty(search) || StringUtils.isNotEmpty(tags)) {
			BuscaPost b = new BuscaPost();
			b.texto = search;

			b.tags = new ArrayList<Tag>();
			if(StringUtils.isNotEmpty(search)) {
				b.tags.addAll(tagService.getTags(search, getEmpresa()));
			}
			
			if(StringUtils.isNotEmpty(tags)) {
				b.tags.addAll(tagService.getTags(tags, getEmpresa()));
			}

			filterSession.filter = search;
			list = postService.findAllPostsByUserAndTitle(b,searchInfo.page, searchInfo.maxRows);
		} else if(categId != null) {
			categoriaPost = categoriaPostService.get(categId);

			BuscaPost b = new BuscaPost();
			b.categoria = categoriaPost;
			b.categoriaIds = categoriaPost.getChildrenIds();
			list = postService.findAllPostsByUserAndTitle(b,searchInfo.page, searchInfo.maxRows);
		} else {
			list = postService.findAllPostsByUserAndTitle(null,0,10);
		}

		posts = postService.toListVo(getUsuario(), list);
		
		
	}
}
