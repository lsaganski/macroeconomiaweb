package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.vo.PostVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class PostsAutoCompletePage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	
	public boolean get;
	public List<PostVO> posts;
	private TextField tNaoAtivo;
	
	@Override
	public void onInit() {
		form.setMethod("post");

		TextField tQ = new TextField("q");
		form.add(tQ);
		tQ.setFocus(true);
		form.add(new TextField("grupoId"));
		form.add(new TextField("not_users_ids"));
		form.add(tNaoAtivo = new TextField("naoAtivo"));
		form.add(new TextField("page"));
		form.add(new TextField("maxRows"));

		tNaoAtivo.setValue("-1");
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		form.add(new Submit("buscar"));

//		setFormTextWidth(form);
	}

	@Override
	public void onGet() {
		super.onGet();
		
		get = true;
	}

	@Override
	public void onPost() {
		String nome = getContext().getRequestParameter("q");

		List<Post> list = postService.findAllByTituloLike(nome);
		List<PostVO> vos = new ArrayList<PostVO>();
		for (Post post : list) {
			PostVO e = postService.setPost(post, getUsuario());
			vos.add(e);
		}
		posts = vos;
	}

	@Override
	protected boolean isHtml() {
		return super.isHtml();
	}
	
	@Override
	public String getTemplate() {
		return super.getTemplate();
	}
	
	@Override
	public String getContentType() {
		return super.getContentType();
	}

	@Override
	protected Object execute() throws Exception {
		if(posts == null) {
			posts = new ArrayList<PostVO>();
		}
		return posts;
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("post", PostVO.class);
		super.xstream(x);
	}
}
