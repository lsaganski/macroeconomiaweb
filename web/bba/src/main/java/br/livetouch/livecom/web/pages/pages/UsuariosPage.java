package br.livetouch.livecom.web.pages.pages;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboGrupo;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.UsuarioAutocompleteFiltro;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.HiddenField;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class UsuariosPage extends LivecomLogadoPage {

	@Autowired
	public UsuarioService usuarioService;

	public Form form = new Form();
	public String msgErro;
	public int page;
	public String action;
	
	public List<Usuario> usuarios;
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	protected Usuario filtro;
	
	public boolean escondeChat = true;

	@Override
	public void onInit() {
		super.onInit();
		
		bootstrap_on = true;
		
		form();
	}

	public void form(){
		
		form.add(new HiddenField("ids",String.class));
		
		TextField tNome = new TextField("nome", getMessage("nome.label"), true);
		tNome.setAttribute("class", "placeholder input borda lupa");
		tNome.setAttribute("autocomplete", "off");
		tNome.setAttribute("placeholder", getMessage("buscar.usuario.label"));
		tNome.setFocus(false);
		form.add(tNome);

		ComboGrupo tGrupo = new ComboGrupo(grupoService,getUsuario());
		form.add(tGrupo);
		
		Submit tExportar = new Submit("exportar", "Exportar", this, "exportar");
		tExportar.setAttribute("class", "tooltip botao export");
		tExportar.setAttribute("title", getMessage("adicionar.usuario.label"));
		form.add(tExportar);
		
		Submit tconsultar = new Submit("consultar", getMessage("consultar.label"), this,"consultar");
		tconsultar.setAttribute("class", "botaoFundo");
		form.add(tconsultar);
		
//		setFormTextWidth(form);
	}
	
	public boolean filtrar() {
		if(form.isValid()) {
			filtro = (Usuario) getContext().getSessionAttribute(Usuario.SESSION_FILTRO_KEY);
			if(filtro == null) {
				filtro = new Usuario();
				getContext().setSessionAttribute(Usuario.SESSION_FILTRO_KEY, filtro);
			}
			form.copyTo(filtro);
			return true;
		
		}
		return false;
	}

	@Override
	public void onGet() {
		super.onGet();
	}
	
	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			if (id != null) {
				Usuario usuario = usuarioService.get(id);
				usuarioService.delete(getUsuario(), usuario);
				setRedirect(UsuariosPage.class);
				setFlashAttribute("msg", getMessage("msg.usuario.deletado.success"));
			}
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.usuario.deletar.error");
		}
		return true;
	}

	public boolean consultar() {
		if (form.isValid()) {
			try {
				
				return true;

			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return false;
	}

	@Override
	public void onRender() {
		super.onRender();

		try {
			
			usuarios = getUsuarios();

		} catch (Exception e) {
			logError(e.getMessage(), e);
			setFlashAttribute("msg", "Erro: " + e.getMessage());
			return;
		}
		
		
	}

	public List<Usuario> getUsuarios() {
		int maxSize = 24;
		
		UsuarioAutocompleteFiltro filtro = new UsuarioAutocompleteFiltro();
		filtro.setPage(page);
		filtro.setMaxRows(maxSize);
		List<Usuario> list = usuarioService.findByFiltro(filtro, getEmpresa());
		
		usuarios = new ArrayList<Usuario>();
		for (Usuario u : list) {
			usuarios.add(u);
		}
		return usuarios;
	}
	
	public boolean exportar(){
		String csv = usuarioService.csvUsuarios(getEmpresa());
		download(csv, "usuarios.csv");
		return true;
	}
	
}
