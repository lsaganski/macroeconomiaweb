package br.livetouch.livecom.domain.vo;

import br.livetouch.livecom.domain.enums.Plataforma;

public class QuantidadeVersaoVO {

	private Long quantidade;
	private String deviceAppVersion;
	private Plataforma plataforma;
	
	public QuantidadeVersaoVO(){
		
	}
	
	public QuantidadeVersaoVO(Long quantidade, String deviceAppVersion, Plataforma plataforma) {
		super();
		this.quantidade = quantidade;
		this.deviceAppVersion = deviceAppVersion;
		this.plataforma = plataforma;
	}

	public Long getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}
	public String getDeviceAppVersion() {
		return deviceAppVersion;
	}
	public void setDeviceAppVersion(String deviceAppVersion) {
		this.deviceAppVersion = deviceAppVersion;
	}

	public Plataforma getPlataforma() {
		return plataforma;
	}

	public void setPlataforma(Plataforma plataforma) {
		this.plataforma = plataforma;
	}


	
}
