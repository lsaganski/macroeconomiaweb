package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Menu;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

public interface MenuService extends Service<Menu> {

	List<Menu> findAll(Empresa e);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Menu c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Menu c, Usuario user) throws DomainException;

	List<Menu> findByLabel(String label, Empresa e);

	List<Menu> findPaiByLabel(String label, Empresa empresa);

	List<Menu> findByPerfil(Perfil p, Empresa e);

	List<Menu> findMenusNotInPerfil(Perfil perfil, Empresa empresa);

	List<Menu> findPaiByPerfil(Perfil perfil, Empresa empresa, boolean in);

	List<Menu> findFilhoByPerfil(Perfil perfil, Empresa empresa, boolean in);

	List<Menu> findFilhos(Menu menu);

	List<Menu> paisByPerfil(Perfil perfil, Empresa empresa);

	List<Menu> filhosByPerfil(Perfil perfil, Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	void deleteFromEmpresa(Empresa empresa) throws DomainException;

	List<Menu> findAllDefault();

	List<Menu> findAllMobile(Empresa empresa) throws DomainException;

	List<Menu> findMenuMobileDefault();

	@Transactional(rollbackFor=Exception.class)
	void restoreMenu(Empresa empresa) throws DomainException;

	void delete(Empresa empresa);

	void cloneDefaultMenu(Empresa empresa) throws DomainException;

    void removeDefaultMenuMobile(Empresa empresa);

	void generateMenus(Empresa empresa) throws DomainException;
}