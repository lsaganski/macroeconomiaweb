package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.PostDestaque;
import br.livetouch.livecom.domain.repository.PostDestaqueRepository;
import br.livetouch.livecom.domain.vo.FiltroLink;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class PostDestaqueRepositoryImpl extends StringHibernateRepository<PostDestaque> implements PostDestaqueRepository {

	public PostDestaqueRepositoryImpl() {
		super(PostDestaque.class);
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public List<PostDestaque> findAllLinks(FiltroLink filtro) {
		StringBuffer sb = new StringBuffer("SELECT p FROM PostDestaque p ");
		sb.append(" left join p.post.grupos g");
		sb.append(" WHERE p.urlSite is not null");
		sb.append(" AND (g.id in (select gu.grupo.id from GrupoUsuarios gu where gu.usuario.id =:userId) ");
		sb.append(" OR (g is null and p.post.usuario.id = :userId)) ");
		if(StringUtils.isNotEmpty(filtro.titulo)) {
			sb.append(" AND (p.tituloUrl like :titulo OR p.post.titulo like :titulo OR p.post.usuario.nome like :titulo  ");
			sb.append(" OR p.urlSite like :titulo ) ");
		}
		
		if(StringUtils.isNotEmpty(filtro.site)) {
			sb.append(" AND p.urlSite like :site");
		}
		
		sb.append(" order by p desc");
		Query q = createQuery(sb.toString());
		q.setParameter("userId", filtro.userId);
		if(StringUtils.isNotEmpty(filtro.titulo)) {
			q.setParameter("titulo", "%" + filtro.titulo + "%");
		}
		
		if(StringUtils.isNotEmpty(filtro.site)) {
			q.setParameter("site", "%" + filtro.site + "%");
		}
		
		q.setCacheable(true);
		
		return q.list();
	}
}