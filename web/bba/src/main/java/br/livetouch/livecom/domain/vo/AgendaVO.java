package br.livetouch.livecom.domain.vo;

import br.livetouch.livecom.domain.Agenda;
import net.livetouch.extras.util.DateUtils;

public class AgendaVO {
	public Long id;
	public String titulo;
	public String descricao;
	public String dataInicio;
	public String dataFim;
	public String horaInicio;
	public String horaFim;
	public PalestranteVO palestrante;
	

	public void setAgenda(Agenda agenda) {
		if (agenda == null) {
			return;
		}
		this.setId(agenda.getId());
		this.setDataInicio(DateUtils.getDate(agenda.getDataInicio(), "dd/MM/yyyy"));
		this.setDataFim(DateUtils.getDate(agenda.getDataFim(), "dd/MM/yyyy"));
		this.setHoraInicio(agenda.getHoraInicio());
		this.setHoraFim(agenda.getHoraFim());
		this.setDescricao(agenda.getDescricao());
		this.setTitulo(agenda.getTitulo());
		this.setPalestrante(getPalestranteVO(agenda));
	}

	private PalestranteVO getPalestranteVO(Agenda agenda) {
		
		PalestranteVO palestranteVo = new PalestranteVO(agenda.getPalestrante());
		
		return palestranteVo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}

	public PalestranteVO getPalestrante() {
		return palestrante;
	}

	public void setPalestrante(PalestranteVO palestrante) {
		this.palestrante = palestrante;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
}