package br.livetouch.livecom.domain.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Playlist;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.PlaylistRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class PlaylistRepositoryImpl extends StringHibernateRepository<Playlist> implements PlaylistRepository {

	public PlaylistRepositoryImpl() {
		super(Playlist.class);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Playlist> findAllByUser(Usuario u) {
		StringBuffer sb = new StringBuffer("from Playlist h where h.usuario.id=?");

		Query q = createQuery(sb.toString());

		q.setLong(0, u.getId());
		List<Playlist> list = q.list();
		return list;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Post> findAllPostsByUser(Usuario u, int page, int maxRows) {
		if(u == null) {
			return new ArrayList<Post>();
		}
		
		StringBuffer sb = new StringBuffer("select p from Playlist h inner join h.post p where 1=1 ");
		
		if(u != null) {
			sb.append(" and h.usuario.id=? ");
		}

		Query q = createQuery(sb.toString());
		
		if( maxRows > 0) {
			int firstResult =  page * maxRows;
			q.setFirstResult(firstResult);
			q.setMaxResults(maxRows);
		}

		if(u != null) {
			q.setLong(0, u.getId());
		}

		List<Post> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Playlist findByUserPost(Usuario u, Post p) {
		if(u == null) {
			return null;
		}
		
		StringBuffer sb = new StringBuffer("from Playlist h where 1=1 ");
		
		if(u != null) {
			sb.append(" and h.usuario.id = :userId");
		}

		if(p != null) {
			sb.append(" and h.post.id = :postId");
		}

		Query q = createQuery(sb.toString());
		
		if(u != null) {
			q.setLong("userId", u.getId());
		}
		if(p != null) {
			q.setLong("postId", p.getId());
		}
		q.setCacheable(true);
		List<Playlist> list = q.list();
		Playlist pl = list.size() > 0 ? list.get(0) : null;
		return pl;
	}
}