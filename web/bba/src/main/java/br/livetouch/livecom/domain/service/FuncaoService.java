package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

public interface FuncaoService extends Service<Funcao> {

	List<Funcao> findAll(Usuario userInfo);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Funcao f,Usuario userInfo) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo, Funcao f) throws DomainException;

	List<Object[]> findIdNomeFuncao(Long idEmpresa);

	Funcao findByNome(String nome, Long idEmpresa);

	Funcao findDefaultByEmpresa(Long empresaId);

	String csvCargo(Empresa empresa);

	List<Funcao> findAll(Empresa empresa);

	List<Funcao> findByNome(String nome, int page, int maxRows, Empresa empresa);

	Long countByNome(String nome, int page, int maxRows, Empresa empresa);

	Funcao findByName(Funcao funcao, Empresa empresa);

	List<Object[]> findIdCodFuncao(Long idEmpresa);

}
