package br.livetouch.livecom.jobs;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.vo.PushChatVO;
import br.livetouch.livecom.push.PushLivecomService;
import br.livetouch.livecom.push.PushNotificationVO;

/**
 * Job para registrar no Push
 * 
 * @author rlech
 *
 */
@Service
public class PushChatJob extends SpringJob {
	
	@Autowired
	private PushLivecomService pushLivecomService;

	protected static final Logger log = Log.getLogger(PushChatJob.class);

	public static Queue<PushChatVO> fila = new ConcurrentLinkedQueue<PushChatVO>();

	@Override
	protected void execute(Map<String, Object> params) throws Exception {
		log("PushChatJob.execute(): " + fila);
		
		Iterator<PushChatVO> it = fila.iterator();
		while(it.hasNext())
		{
		    PushChatVO pushChat = it.next();
			if(pushLivecomService != null && pushChat != null) {
				log(">> Livecom Chat enviando Push: " + pushChat);
				Long pushId = pushChat.getConversa();
				List<Long> usuarios = pushChat.getUsuarios();
				PushNotificationVO notification = pushChat.getNotification();
				notification.setVoip(true);

				if(pushId != null) {
					String json = pushLivecomService.push(pushId, usuarios, notification);
					log("<< Livecom Push " + json);
					it.remove();
				} else {
					System.err.println(String.format("error %s, %s, %s", pushId, usuarios, notification));
					System.err.println(String.format("Chat %s", pushChat));
				}
			}
		}
	}
	
	private void log(String string) {
		log.debug(string);
	}

	public static void add(PushChatVO vo) {
		fila.add(vo);
	}

	
}
