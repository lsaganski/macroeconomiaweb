package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Permissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.PermissaoRepository;
import br.livetouch.livecom.domain.service.PermissaoService;
import br.livetouch.livecom.domain.vo.PerfilPermissaoVO;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class PermissaoServiceImpl implements PermissaoService {
	@Autowired
	private PermissaoRepository rep;

	@Override
	public Permissao get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Permissao c, Empresa e) throws DomainException {
		c.setEmpresa(e);
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Permissao> findAll(Empresa e) {
		return rep.findAll(e);
	}

	@Override
	public void delete(Permissao p, Usuario user) throws DomainException {
		try {
			rep.delete(p);
		}catch (DataIntegrityViolationException e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if(root == null) {
				root = e;
			}
			LogSistema.logError(user, e);
			
			throw e;
		} catch (Exception e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if(root == null) {
				root = e;
			}
			LogSistema.logError(user, e);
			
			throw e;
		} 
	}


	@Override
	public List<Permissao> findByNome(String nome, Empresa empresa) {
		return rep.findByNome(nome, empresa);
	}
	
	public Permissao findByNomeValid(Permissao permissao, Empresa empresa){
		return rep.findByNomeValid(permissao, empresa);
	}

	@Override
	public Permissao findByCodigoValid(Permissao permissao, Empresa empresa) {
		return rep.findByCodigoValid(permissao, empresa);
	}

	@Override
	public Permissao findByCodigo(String codigo, Empresa empresa) {
		Permissao permissao = new Permissao();
		permissao.setCodigo(codigo);
		return rep.findByCodigoValid(permissao, empresa);
	}

	@Override
	public List<PerfilPermissaoVO> findByPerfil(Perfil perfil) {
		return rep.findByPerfil(perfil);
	}	

}
