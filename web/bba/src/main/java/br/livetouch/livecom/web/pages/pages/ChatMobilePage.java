package br.livetouch.livecom.web.pages.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.admin.LivecomPage;

@Controller
@Scope("prototype")
public class ChatMobilePage extends LivecomPage {
	public Long user;
	public Long grupo;
	public Long idLogado;
	public Usuario userAcesso;
	public String formenviar;
	public String header;
	
	@Override
	public void onInit() {
	}

    @Override
    public void onGet() {
        if(idLogado != null) {
            userAcesso = usuarioService.get(idLogado);
        }
    }

    @Override
	public String getTemplate() {
		return getPath();
	}

}
