package br.livetouch.livecom.web.filter;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import br.infra.util.ServletUtils;
import br.livetouch.livecom.domain.LogTransacao;

public class LogTransacaoFilter implements Filter { 
	
	private static Logger log = Logger.getLogger(LogTransacaoFilter.class);
	private static final boolean LOG = false;
	  
    @Override 
    public void init(FilterConfig filterConfig) throws ServletException {
    } 
  
    @Override 
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
    	
    	LogTransacao log = new LogTransacao();
		log.setDataInicio(new Date());
//		log.setEmpresa(getEmpresa());// FIXME EMPRESA: Log transacao

        try {
        	HttpServletRequest httpRequest = (HttpServletRequest) request;
        	
        	String url = ServletUtils.getRequestUrl(request);
        	String path = ServletUtils.getRequestPath(request);
        	String host = ServletUtils.getHost(httpRequest);
        	
//        	log.debug("Url: " + url);

        	// continua
			chain.doFilter(request, response);
			
			if(log != null) {
				String xml = "?";
				log.setXml(xml);
			}
			
		} catch (Exception e) {
			log.setException(e);
			
			chain.doFilter(request, response);
		} finally {
//			saveLogTransacao(log);
		}
    } 
  
    @Override 
    public void destroy() { 
    }
} 
