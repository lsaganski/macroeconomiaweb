/*
package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.GrupoMensagemUsuarios;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class GrupoMensagemUsuariosRepositoryImpl extends StringHibernateRepository<GrupoMensagemUsuarios> implements GrupoMensagemUsuariosRepository {

	public GrupoMensagemUsuariosRepositoryImpl() {
		super(GrupoMensagemUsuarios.class);
	}

	@Override
	public GrupoMensagemUsuarios findByGrupoEUsuario(GrupoMensagem g, Usuario u) {
			Query q = createQuery("from GrupoMensagemUsuarios gu where gu.grupo = ? and gu.usuario = ?");
			q.setParameter(0, g);
			q.setParameter(1, u);
			q.setCacheable(true);
			GrupoMensagemUsuarios gu = (GrupoMensagemUsuarios) q.uniqueResult();
			if (gu != null) {
				deatach(gu);
			}
			return gu;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GrupoMensagemUsuarios> adminsByGrupo(GrupoMensagem g) {
		Query q = createQuery("from GrupoMensagemUsuarios gu where gu.grupo = ? and admin_grupo = 1");
		q.setParameter(0, g);
		q.setCacheable(true);
		List<GrupoMensagemUsuarios> list = q.list();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GrupoMensagemUsuarios> usersByGrupo(GrupoMensagem g) {
		Query q = createQuery("from GrupoMensagemUsuarios gu where gu.grupo = ? order by gu.usuario.nome ");
		q.setParameter(0, g);
		q.setCacheable(true);
		List<GrupoMensagemUsuarios> list = q.list();
		return list;
	}

	@Override
	public int getUltimaInsercao(GrupoMensagem g) {
		Query q = createQuery("select ordem from GrupoMensagemUsuarios gu where gu.grupo = ? order by ordem desc");
		q.setParameter(0, g);
		q.setCacheable(true);
		int ordem = (int) q.list().get(0);
		return ordem;
	}

	@Override
	public GrupoMensagemUsuarios getByOrdemGrupo(GrupoMensagem g, int ordem) {
		Query q = createQuery("from GrupoMensagemUsuarios gu where gu.grupo = ? and ordem = ?");
		q.setParameter(0, g);
		q.setParameter(1, ordem);
		q.setCacheable(true);
		return (GrupoMensagemUsuarios) q.list().get(0);
	}
	
	

}*/