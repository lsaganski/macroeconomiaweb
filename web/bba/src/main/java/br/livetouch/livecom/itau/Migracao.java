package br.livetouch.livecom.itau;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.jdbc.support.JdbcUtils;

public class Migracao {

	public static void main(String args[]) throws Exception {
		Connection connection = null;
		Connection connectionPostgres = null;
		
		ResultSet rs = null;
		Statement stmt = null;
		
		PreparedStatement stmtInsert = null;
		int BATCH_SIZE = 1000;

		try {
			
			//DRIVER MYSQL
			System.out.println("Testando conexao... Mysql");
			String driver = "com.mysql.jdbc.Driver";
			Class.forName(driver);
			String url = "jdbc:mysql://localhost/macroeconomia";
			System.out.println(url);
			connection = DriverManager.getConnection(url, "root", "root");
			System.out.println("Conn Mysql OK: " + connection);
			
			if (connection == null) {
				throw new SQLException("Sem conexao!");
			}
			
			//DRIVER POSTGRES
			System.out.println("Testando conexao... Postgres");
			String driverPostgres = "org.postgresql.Driver";
			Class.forName(driverPostgres);
			String urlPostgres = "jdbc:postgresql://localhost:5432/macroeconomia";
			System.out.println(urlPostgres);
			connectionPostgres = DriverManager.getConnection(urlPostgres, "postgres", "root");
			System.out.println("Conn Postgres OK: " + connectionPostgres);
			
			if (connectionPostgres == null) {
				throw new SQLException("Postgres sem conexao!");
			}

			//POSTS BBA
			String sql = "SELECT l.name as linguagem, c.name as categoria, r.id as post_id, r.date as data, r.title as titulo, r.description as resumo, r.content as conteudo " + 
					"FROM reports r " + 
					"INNER JOIN channels c " + 
					"ON c.id = r.channel_id " + 
					"INNER JOIN channels_languages cl " + 
					"ON c.id = cl.channel_id " + 
					"INNER JOIN languages l " + 
					"ON l.id = cl.language_id " + 
					"WHERE r.published = true " + 
					"ORDER BY r.id DESC";
			stmt = connectionPostgres.createStatement();
			rs = stmt.executeQuery(sql);

			if (rs == null) {
				throw new SQLException("Sem dados!");
			}
			
			//ZERA TABELA TEMPORARIA MYSQL
			connection.createStatement().execute("truncate table posts_temp");

			//INSERT TABELA TEMPORARIA MYSQL
			int countTemp = 0;
			stmtInsert = connection.prepareStatement("INSERT INTO posts_temp (linguagem, categoria, post_id, data, titulo, resumo, conteudo) values (?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			while (rs.next()) {
				int i = 1;
				try {
					countTemp++;
					stmtInsert.setString(i++, rs.getString("linguagem"));
					stmtInsert.setString(i++, rs.getString("categoria"));
					stmtInsert.setLong(i++, rs.getLong("post_id"));
					stmtInsert.setDate(i++, rs.getDate("data"));
					stmtInsert.setString(i++, rs.getString("titulo"));
					stmtInsert.setString(i++, rs.getString("resumo"));
					stmtInsert.setString(i++, rs.getString("conteudo"));
					stmtInsert.addBatch();
					if (countTemp > BATCH_SIZE) {
						stmtInsert.executeBatch();
						stmtInsert.clearBatch();
						countTemp = 0;
					}
				} catch(Exception e) {
					System.out.println("Erro: " + e.getMessage());
				}
			}
			
		} catch (Exception e) {
			System.out.println("Erro: " + e.getMessage());
			e.printStackTrace();
		} finally {
			
			stmtInsert.executeBatch();
			stmtInsert.clearBatch();
			
			JdbcUtils.closeResultSet(rs);
			JdbcUtils.closeStatement(stmt);
			JdbcUtils.closeStatement(stmtInsert);
			JdbcUtils.closeConnection(connection);
			
			JdbcUtils.closeConnection(connectionPostgres);
		}
	}
}