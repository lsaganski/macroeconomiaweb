package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Token;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

public interface TokenService extends Service<Token> {

	List<Token> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Token t) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Token t) throws DomainException;

	List<Token> findAllByUser(Usuario u);

	Token findLastToken(Long userId, String token, String so);
	
}