package br.livetouch.livecom.web.pages.training;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusPagamento;
import br.livetouch.payment.MessageResponseException;
import br.livetouch.payment.PaymentResponse;
import br.livetouch.payment.PaymentService;


/**
 * @author Ricardo Lecheta
 * @created 06/03/2013
 */
@Controller
@Scope("prototype")
public class BillingPage extends TrainingLogadoPage {

	public String msgInfo;
	
	@Override
	public void onInit() {
		super.onInit();

		if(StringUtils.isNotEmpty(msgInfo)) {
			msgInfo = "Você precisa fazer o pagamento para assistir os vídeos.";
		}
	}

	@Override
	public void onPost() {
		super.onPost();

		boolean localhost = false;
		String host = localhost ? "localhost:8080": "54.207.75.112";
		PaymentService s = new PaymentService(host);

		Usuario userInfo = getUsuario();

		if(!userInfo.isValidNomeSobrenome()) {
			setMessageError("O servidor de pagamento precisa do nome completo para efetuar a compra. Entre na tela de Meus Dados, atualize seu nome e tente novamente.");
			return;
		}
		
		String email = userInfo.getEmail();
		String nome = userInfo.getNome();

		String projetoId = "schubertbjj";
		String title = "Schubert BJJ";
		String buyCode = getUsuario()+"_billing4";
		String amount = getParam("valor");
		if(StringUtils.isEmpty(amount)) {
			amount = "1";
		}

		try {
			PaymentResponse p = s.buy(projetoId, email, nome, email,buyCode, title, amount);
			if(p != null && StringUtils.isNotEmpty(p.getUrl_payment())) {
				setRedirect(p.getUrl_payment());
				userInfo.setStatusPag(StatusPagamento.PAGO);
				usuarioService.saveOrUpdate(userInfo,userInfo);
			} else {
				setMessageError("Ocorreu um eror no pagamento, contate o administrador do sistema.");
			}
		} catch (MessageResponseException e) {
			logError(e.getMessage(), e);
			setMessageError("O servidor de pagameneto retornou um erro: " + e.getMessage());
		} catch (Exception e) {
			logError(e.getMessage(), e);
			setMessageError("Ocorreu um erro ao processar o pagamento: " + e.getMessage());
		}
	}
}
