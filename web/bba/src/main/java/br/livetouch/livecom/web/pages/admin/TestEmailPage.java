package br.livetouch.livecom.web.pages.admin;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.service.EmailReportService;
import br.livetouch.livecom.email.EmailFactory;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextArea;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class TestEmailPage extends LivecomAdminPage {
	
	@Autowired
	protected EmailReportService emailReportService;

	public String msg;
	public Form form = new Form();
	private TextField tAssunto;
	private TextField tEmail;
	private TextArea tMsg;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.ENVIAR_EMAILS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();
		
		form.add(tAssunto = getTextField("assunto"));
		form.add(tMsg = getTextArea("msg"));
		form.add(tEmail = getTextField("email"));
		
		Submit field = new Submit("enviar", this, "enviar");
		field.setAttribute("class", "btn btn-primary min-width");
		form.add(field);

		
		//setFormTextWidth(form);
	}
	
	public boolean enviar() throws DomainException {
		try {
			if(form.isValid()) {
				boolean ok = EmailFactory.getEmailManager().sendEmail(tEmail.getValue(),tAssunto.getValue(), tMsg.getValue(),true);
				
				msg = ok? "Email enviado com sucesso" : "Erro ao enviar email";
				return ok;
			}
			setMessageError("Preencha os campos");
			return false;
		} catch (Exception e) {
			logError(e.getMessage(),e);
			setMessageError(e, "Não foi possivel enviar o email");
		}
		return false;
	}
	
	private TextField getTextField(String nome){
		TextField text = new TextField(nome, true);
		text.addStyleClass("input-sm form-control");
		return text;
	}
	
	private TextArea getTextArea(String nome){
		TextArea text = new TextArea(nome, true);
		text.addStyleClass("input-sm form-control");
		return text;
	}
	
	
	
}
