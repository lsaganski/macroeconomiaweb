package br.livetouch.livecom.domain;

import java.util.WeakHashMap;

public class RequestMap {
	
	private static final RequestMap instance = new RequestMap();
	private WeakHashMap<String, String> requestToken;
	
	
	private RequestMap() {
		requestToken = new WeakHashMap<String, String>();
	}
	
	public static RequestMap getInstance() {
		return instance;
	}
	
	public String get(String token) {
		return requestToken.get(token);
	}
	
	public void put(String token, String livecomToken) {
		requestToken.put(token, livecomToken);
	}
	
	public void remove(String token) {
		requestToken.remove(token);
	}

}
