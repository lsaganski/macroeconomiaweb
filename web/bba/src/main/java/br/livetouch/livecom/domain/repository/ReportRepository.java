package br.livetouch.livecom.domain.repository;

import java.util.List;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.QuantidadeVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioAudienciaVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioLoginVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoFiltro;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import net.livetouch.tiger.ddd.DomainException;
import net.livetouch.tiger.ddd.repository.Repository;

public interface ReportRepository extends Repository<Usuario> {

	List<RelatorioLoginVO> loginsResumido(RelatorioFiltro filtro) throws DomainException;

	long getCountloginsResumido(RelatorioFiltro filtro) throws DomainException;

	List<RelatorioLoginVO> loginsExpandido(RelatorioFiltro filtro) throws DomainException;

	List<RelatorioLoginVO> reportVersao(RelatorioFiltro filtro) throws DomainException;

	List<QuantidadeVersaoVO> reportVersaoGrafico(RelatorioFiltro filtro) throws DomainException;

	List<RelatorioLoginVO> loginsAgrupado(RelatorioFiltro filtro) throws DomainException;

	List<RelatorioVisualizacaoVO> relVisualizacaoConsolidado(RelatorioVisualizacaoFiltro filtro);

	List<RelatorioAudienciaVO> findAudiencia(RelatorioFiltro filtro) throws DomainException;

	long getCountAudiencia(RelatorioFiltro filtro) throws DomainException;
	
	List<RelatorioAudienciaVO> audienciaDetalhes(RelatorioFiltro filtro) throws DomainException;

}