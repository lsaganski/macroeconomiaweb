package br.livetouch.livecom.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;

public class CorsFilter implements Filter {
	

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    	boolean CORS_ON = ParametrosMap.getInstance().getBoolean(Params.SECURITY_CORS_ON, true);

    	if(!CORS_ON) {
    		return;
    	}
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    	
    	String ORIGIN = ParametrosMap.getInstance().get(Params.SECURITY_CORS_ORIGIN, "*");
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setHeader("Access-Control-Allow-Origin", ORIGIN);
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS");
        response.setHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
