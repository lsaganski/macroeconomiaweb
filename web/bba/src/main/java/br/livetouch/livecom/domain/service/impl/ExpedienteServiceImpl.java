package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Expediente;
import br.livetouch.livecom.domain.repository.ExpedienteRepository;
import br.livetouch.livecom.domain.service.ExpedienteService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ExpedienteServiceImpl implements ExpedienteService {
	@Autowired
	private ExpedienteRepository rep;

	@Override
	public Expediente get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Expediente e) throws DomainException {
		rep.saveOrUpdate(e);
	}

	@Override
	public List<Expediente> findAll() {
		return rep.findAll();
	}

	@Override
	public void delete(Expediente e) throws DomainException {
		rep.delete(e);
	}

	@Override
	public List<Object[]> findIdNomeExpediente() {
		return rep.findIdNomeExpediente();
	}

	@Override
	public Expediente findByCodigo(Expediente expediente, Empresa empresa) {
		return rep.findByCodigo(expediente, empresa);
	}

}
