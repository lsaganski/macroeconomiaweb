package br.livetouch.livecom.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.google.gson.Gson;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.enums.TipoPush;
import br.livetouch.livecom.push.PushNotificationVO;

@Entity
public class PushReport extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "PUSHREPORT_SEQ")
	private Long id;
	private boolean status;
	private Date dataEnvio;
	
	@Column(length = 2000)
	private String erro;
	
	@Column(length = 2000)
	private String mensagem;
	
	private TipoPush tipoPush;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id", nullable = true)
	private Post post;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "comentario_id", nullable = true)
	private Comentario comentario;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Date getDataEnvio() {
		return dataEnvio;
	}

	public void setDataEnvio(Date dataEnvio) {
		this.dataEnvio = dataEnvio;
	}

	public TipoPush getTipoPush() {
		return tipoPush;
	}

	public void setTipoPush(TipoPush tipoPush) {
		this.tipoPush = tipoPush;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = Utils.truncate(mensagem, 2000);
	}

	public Comentario getComentario() {
		return comentario;
	}

	public void setComentario(Comentario comentario) {
		this.comentario = comentario;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}
	
	public PushNotificationVO getPushNotificationVO() {
		if(this.mensagem != null) {
			return new Gson().fromJson(this.mensagem, PushNotificationVO.class);
		}
		return null;
	}
}
