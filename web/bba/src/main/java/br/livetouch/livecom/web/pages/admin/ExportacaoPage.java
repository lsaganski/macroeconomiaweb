package br.livetouch.livecom.web.pages.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.sf.click.control.ActionLink;

@Controller
@Scope("prototype")
public class ExportacaoPage extends LivecomLogadoPage {	
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.EXPORTACAO)) {
			return true;
		}
		
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}
	
	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
		
		addControl(new ActionLink("exportarUsuario", this, "exportarUsuario"));
		
		addControl(new ActionLink("exportarGrupos", this, "exportarGrupos"));
		
		addControl(new ActionLink("exportarCategoria", this, "exportarCategoria"));
		
		addControl(new ActionLink("exportarTags", this, "exportarTags"));
		
		addControl(new ActionLink("exportarCanal", this, "exportarCanal"));
		
		addControl(new ActionLink("exportarCidade", this, "exportarCidade"));
		
		addControl(new ActionLink("exportarComplemento", this, "exportarComplemento"));
		
		addControl(new ActionLink("exportarFormacao", this, "exportarFormacao"));
		
		addControl(new ActionLink("exportarCargo", this, "exportarCargo"));
		
		addControl(new ActionLink("exportarDiretoria", this, "exportarDiretoria"));
		
		addControl(new ActionLink("exportarArea", this, "exportarArea"));
	}
	
	public boolean exportarGrupos(){
		String csv = grupoService.csvGrupos(getEmpresa());
		download(csv, "grupos.csv");
		return true;
	}
	
	public boolean exportarUsuario(){
		String csv = usuarioService.csvUsuarios(getEmpresa());
		download(csv, "usuarios.csv");
		return true;
	}
	
	public boolean exportarCategoria(){
		String csv = categoriaPostService.csvCategoria(getEmpresa());
		download(csv, "categorias.csv");
		return true;
	}
	
	public boolean exportarTags(){
		String csv = tagService.csvTag(getEmpresa());
		download(csv, "tags.csv");
		return true;
	}
	
	public boolean exportarCanal(){
		String csv = canalService.csvCanal();
		download(csv, "canais.csv");
		return true;
	}
	
	public boolean exportarCidade() {
		String csv = cidadeService.csvCidade();
		download(csv, "cidade.csv");
		return true;
	}
	
	public boolean exportarComplemento() {
		String csv = complementoService.csvComplemento(getEmpresa());
		download(csv, "complemento.csv");
		return true;
	}
	
	public boolean exportarFormacao() {
		String csv = formacaoService.csvFormacao(getEmpresa());
		download(csv, "formacao.csv");
		return true;
	}
	
	public boolean exportarCargo() {
		String csv = funcaoService.csvCargo(getEmpresa());
		download(csv, "cargo.csv");
		return true;
	}
	
	public boolean exportarDiretoria() {
		String csv = diretoriaService.csvDiretoria(getEmpresa());
		download(csv, "diretoria.csv");
		return true;
	}
	
	public boolean exportarArea() {
		String csv = areaService.csvArea(getEmpresa());
		download(csv, "area.csv");
		return true;
	}
	
}