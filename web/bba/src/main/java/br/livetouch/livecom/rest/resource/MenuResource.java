package br.livetouch.livecom.rest.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;

import br.infra.util.Log;
import br.infra.util.TempExceptionUtils;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Menu;
import br.livetouch.livecom.domain.MenuPerfil;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.vo.MenuMobileVO;
import br.livetouch.livecom.domain.vo.MenuPerfilVO;
import br.livetouch.livecom.domain.vo.MenuVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import br.livetouch.livecom.rest.request.MenuMobileRequest;
import io.swagger.annotations.ApiOperation;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/menu")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class MenuResource extends MainResource {
	protected static final Logger log = Log.getLogger(MenuResource.class);

	@POST
	public Response create(Menu menu) {
		if (menu == null) {
			return Response.status(Status.BAD_REQUEST).entity(MessageResult.error("Erro ao criar menu")).build();
		}

		try {
			fixLinkMenu(menu);
//			menu.setEmpresa(getEmpresa());
			menuService.saveOrUpdate(menu);
			MenuVO menuVO = new MenuVO();
			menuVO.setMenu(menu);
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", menuVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel realizar o cadastro do menu " + e.getMessage(), e);
			return Response.status(Status.BAD_REQUEST).build();
		}
	}

	@PUT
	public Response update(Menu menu) {
		if (menu == null || menu.getId() == null) {
			return Response.status(Status.BAD_REQUEST).entity(MessageResult.error("Menu inexistente")).build();
		}

		try {
			fixLinkMenu(menu);
//			menu.setEmpresa(getEmpresa());
			menuService.saveOrUpdate(menu);
			MenuVO menuVO = new MenuVO();
			menuVO.setMenu(menu);
			return Response.ok(MessageResult.ok("Menu atualizado com sucesso", menuVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel atualizar o cadastro do menu " + e.getMessage(), e);
			return Response.status(Status.BAD_REQUEST).build();
		}
	}

	@GET
	public Response findAll() {
		boolean in = false;
		List<Menu> menus = menuService.findPaiByPerfil(null, getEmpresa(), in);
		List<Menu> menusFilho = menuService.findFilhoByPerfil(null, getEmpresa(), in);

		List<MenuVO> vos = new ArrayList<MenuVO>();
		for (Menu menu : menus) {
			MenuVO menuVO = new MenuVO(menu);
			for (Menu filho : menusFilho) {
				if (filho.getParent().getId().equals(menu.getId())) {
					MenuVO filhoVO = new MenuVO(filho);
					menuVO.addFilho(filhoVO);
				}
			}
			vos.add(menuVO);
		}

		return Response.ok().entity(vos).build();
	}

	@GET
	@Path("/empresa/{id}")
	public Response findByEmpresa(@PathParam("id") Long id) {

		if (id == null) {
			return Response.ok(MessageResult.error("Menus não encontrado para a empresa informada")).build();
		}

		Empresa empresa = empresaService.get(id);

		if (empresa == null) {
			return Response.ok(MessageResult.error("Menus não encontrado para a empresa informada")).build();
		}

		boolean in = false;
		List<Menu> menus = menuService.findPaiByPerfil(null, empresa, in);
		List<Menu> menusFilho = menuService.findFilhoByPerfil(null, empresa, in);

		List<MenuVO> vos = new ArrayList<MenuVO>();
		for (Menu menu : menus) {
			MenuVO menuVO = new MenuVO(menu);
			for (Menu filho : menusFilho) {
				if (filho.getParent().getId().equals(menu.getId())) {
					MenuVO filhoVO = new MenuVO(filho);
					menuVO.addFilho(filhoVO);
				}
			}
			vos.add(menuVO);
		}

		return Response.ok().entity(vos).build();
	}

	@POST
	@Path("/ordem")
	@ApiOperation(value = "salva/atualiza ordem dos menus.", notes = "WS salvar ordem.")
	public Response ordem(List<MenuVO> vos) {
		try {
			for (MenuVO vo : vos) {
				Menu menu = menuService.get(vo.getId());
				menu.setOrdem(vo.getOrdem());
				if(vo.getParent() != null) {
					Menu parent = menuService.get(vo.getParent().getId());
					menu.setParent(parent);
				}
				menuService.saveOrUpdate(menu);
			}
			Response response = Response
					.ok(MessageResult.ok("Ordem de [" + vos.size() + "] menu(s) atualizada com sucesso.")).build();
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			String erro = "Não foi possível salvar a ordem dos menus.";
			log.error(erro, e);
			return Response.ok(MessageResult.error(erro)).build();
		}
	}

	@POST
	@Path("/perfil/ordem/{id}")
	@ApiOperation(value = "salva/atualiza ordem dos menus.", notes = "WS salvar ordem.")
	public Response ordemPerfil(List<MenuPerfilVO> menus, @PathParam("id") Long perfilId) {
		try {
			
			if(perfilId == null) {
				String erro = "Não foi possível salvar a ordem dos menus para este perfil.";
				log.error(erro);
				return Response.ok(MessageResult.error(erro)).build();
			}
			
			Perfil perfil = perfilService.get(perfilId);
			
			if(perfil == null) {
				String erro = "Não foi possível salvar a ordem dos menus para este perfil.";
				log.error(erro);
				return Response.ok(MessageResult.error(erro)).build();
			}
					
			//delete not in
			List<MenuPerfil> listMenus = new ArrayList<MenuPerfil>();

			for (MenuPerfilVO vo : menus) {
				if(vo.id != null) {
					MenuPerfil menu = menuPerfilService.get(vo.id);
					menu.setOrdem(vo.ordem);
					menuPerfilService.saveOrUpdate(menu);
					listMenus.add(menu);
				} else {
					MenuPerfil menu = new MenuPerfil();
					Menu m = menuService.get(vo.menu.getId());
					menu.setMenu(m);
					menu.setPerfil(perfil);
					menu.setOrdem(vo.ordem);
					menuPerfilService.saveOrUpdate(menu);
					listMenus.add(menu);
				}
				
			}
			
			menuPerfilService.removeNotIn(listMenus, perfil);
			
			Response response = Response
					.ok(MessageResult
							.ok("Ordem de [" + menus.size() + "] menus(s) atualizado com sucesso para este perfil."))
					.build();
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			String erro = "Não foi possível salvar a ordem dos menus para este perfil.";
			log.error(erro, e);
			return Response.ok(MessageResult.error(erro)).build();
		}
	}

	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		Menu menu = menuService.get(id);
		if (menu == null) {
			return Response.status(Status.NOT_FOUND).entity(MessageResult.error("Menu não encontrado")).build();
		}
		MenuVO menuVO = new MenuVO();
		menuVO.setMenu(menu);
		return Response.ok().entity(menuVO).build();
	}

	@GET
	@Path("/perfil/{id}")
	public Response findByPerfil(@PathParam("id") Long id) {
		Perfil perfil = perfilService.get(id);
		if (perfil == null) {
			return Response.status(Status.NOT_FOUND).entity(MessageResult.error("Perfil não encontrado")).build();
		}

		List<Menu> menus = menuService.findByPerfil(perfil, getEmpresa());
		List<MenuVO> vos = MenuVO.fromList(menus);
		return Response.ok().entity(vos).build();
	}

	@GET
	@Path("/perfil/menus/{id}")
	public Response findMenusPerfil(@PathParam("id") Long id) {

		try {
			Perfil perfil = perfilService.get(id);
			if (perfil == null) {
				return Response.status(Status.NOT_FOUND).entity(MessageResult.error("Perfil não encontrado")).build();
			}
			
			MenuPerfilVO mp = new MenuPerfilVO();

			boolean menuIn = true;
			// List<Menu> menus = menuService.findPaiByPerfil(perfil,
			// getEmpresa(), menuIn);
			// List<Menu> menusFilho = menuService.findFilhoByPerfil(perfil,
			// getEmpresa(), menuIn);
			List<MenuPerfil> menus = menuPerfilService.findPaiByPerfil(perfil, perfil.getEmpresa(), menuIn);
			List<MenuPerfil> menusFilho = menuPerfilService.findFilhoByPerfil(perfil, perfil.getEmpresa(), menuIn);

			List<MenuPerfilVO> in = new ArrayList<MenuPerfilVO>();
			for (MenuPerfil menuPerfil : menus) {
				MenuPerfilVO menuPerfilVO = new MenuPerfilVO(menuPerfil);
				for (MenuPerfil filho : menusFilho) {
					if (filho.getMenu().getParent().getId().equals(menuPerfil.getMenu().getId())) {
						MenuPerfilVO filhoVO = new MenuPerfilVO(filho);
						menuPerfilVO.addFilho(filhoVO);
					}
				}
				in.add(menuPerfilVO);
			}

			menuIn = false;
			// List<Menu> menusNotIn = menuService.findPaiByPerfil(perfil,
			// getEmpresa(), menuIn);
			// List<Menu> menusFilhoNotIn =
			// menuService.findFilhoByPerfil(perfil, getEmpresa(), menuIn);
			List<Menu> menusNotIn = menuService.findPaiByPerfil(perfil, perfil.getEmpresa(), menuIn);
			List<Menu> menusFilhoNotIn = menuService.findFilhoByPerfil(perfil, perfil.getEmpresa(), menuIn);

			List<MenuPerfilVO> notIn = new ArrayList<MenuPerfilVO>();
			for (Menu menuPerfil : menusNotIn) {
				MenuPerfilVO menuPerfilVO = new MenuPerfilVO(menuPerfil);
				for (Menu filho : menusFilhoNotIn) {
					MenuPerfilVO filhoVO = new MenuPerfilVO(filho);
					if (filho.getParent().getId().equals(menuPerfil.getId())) {
						menuPerfilVO.addFilhoNotIn(filhoVO);
					} else {
						mp.addFilhoNotIn(filhoVO);
					}
				}
				
				if(menuPerfilVO.filhosNotIn != null) {
					notIn.add(menuPerfilVO);
				}
			}

			mp.in = in;
			mp.notIn = notIn;

			return Response.ok().entity(mp).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error(e.getMessage())).build();
		}
	}

	@GET
	@Path("/find")
	public Response findByLabel(@QueryParam("label") String label) {
		List<Menu> menus = menuService.findByLabel(label, getEmpresa());
		if (menus.isEmpty()) {
			return Response.status(Status.NOT_FOUND).entity(MessageResult.error("Menu não encontrado")).build();
		}
		List<MenuVO> vos = MenuVO.fromList(menus);
		return Response.ok().entity(vos).build();
	}

	@GET
	@Path("/find/pai")
	public Response findByPaiLabel(@QueryParam("label") String label) {
		List<Menu> menus = menuService.findPaiByLabel(label, getEmpresa());
		if (menus.isEmpty()) {
			return Response.status(Status.NOT_FOUND).entity(MessageResult.error("Menu não encontrado")).build();
		}
		List<MenuVO> vos = MenuVO.fromList(menus);
		return Response.ok().entity(vos).build();
	}

	@PUT
	@Path("/perfil/menus")
	public Response menus(List<MenuPerfil> menus) {
		if (menus == null || menus.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(MessageResult.error("Perfil inexistente")).build();
		}

		try {
			MenuPerfil menuPerfil = menus.get(0);
			Perfil p = menuPerfil.getPerfil();

			List<MenuPerfil> menusOld = menuPerfilService.findByPerfil(p, getEmpresa());
			Map<Long, MenuPerfil> map = new HashMap<Long, MenuPerfil>();

			if (!menusOld.isEmpty()) {
				for (MenuPerfil mp : menus) {
					if (menusOld.contains(mp)) {
						menusOld.remove(mp);
					} else {
						map.put(mp.getId(), mp);
					}
				}

				menus = new ArrayList<MenuPerfil>();
				for (MenuPerfil m : map.values()) {
					MenuPerfil mp = new MenuPerfil();
					mp.setMenu(m.getMenu());
					mp.setPerfil(p);
					mp.setOrdem(m.getOrdem());
					menus.add(mp);
				}

				menuPerfilService.removeIn(menusOld);
				menuPerfilService.saveList(menus);
				return Response.ok(MessageResult.ok("Menus salvos no perfil com sucesso")).build();
			} else {
				menuPerfilService.saveList(menus);
				return Response.ok(MessageResult.ok("Menus salvos no perfil com sucesso")).build();
			}

		} catch (DomainException e) {
			log.debug("Não foi possivel cadastrar os menus no perfil " + e.getMessage(), e);
			return Response.status(Status.BAD_REQUEST).build();
		}
	}

	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		Menu menu = menuService.get(id);
		if (menu == null) {
			return Response.status(Status.NOT_FOUND).entity(MessageResult.error("Menu não encontrado")).build();
		}
		try {

			List<Menu> filhos = menuService.findFilhos(menu);
			if (!filhos.isEmpty()) {
				return Response.status(Status.BAD_REQUEST)
						.entity(MessageResult.error("Menu possui menus filho associados e não pode ser removido"))
						.build();
			}

			menuService.delete(menu, getUserInfo());
			return Response.ok().entity(MessageResult.ok("Menu deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error("Não foi possível excluir o menu id: " + id + " ERRO:" + e.getMessage(), e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(MessageResult.error("Não foi possível excluir o menu  " + id)).build();
		} catch (DataIntegrityViolationException e) {
			log.error(e.getMessage(), e);
			if (TempExceptionUtils.isErrorContraint(e)) {
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(MessageResult
								.error("Não foi possível excluir o menu " + id + " porque ele possui relacionamentos."))
						.build();
			}
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(MessageResult
							.error("Não foi possível excluir o menu  " + id + " porque ele possui relacionamentos."))
					.build();
		} catch (Exception e) {
			log.error("Não foi possível excluir o menu id: " + id + " ERRO:" + e.getMessage(), e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(MessageResult.error("Não foi possível excluir o menu  " + id)).build();
		}
	}

	@Path("/mobile")
	@GET
	public Response findAllMenuMobile() {
        List<Menu> menus = null;
        try {
            menus = menuService.findAllMobile(getEmpresa());
            if(menus != null && menus.size() > 0) {
                List<MenuMobileVO> vos = MenuMobileVO.createByList(menus);
                return Response.ok(vos).build();
            }
            return Response.ok().build();
        } catch (DomainException e) {
            log.error("Não foi possível buscar o menu desta empresa, ERRO:" + e.getMessage(), e);
            return Response.status(Status.INTERNAL_SERVER_ERROR)
                    .entity(MessageResult.error("Não foi possível buscar o menu desta empresa")).build();
        }

	}

	@Path("/mobile")
	@POST
	public Response saveMenuMobile(MenuMobileRequest menuRequest) {
		try {
			String id = menuRequest.id;
			Menu menu = null;
			boolean insert = true;
			if(StringUtils.isNotEmpty(id) && StringUtils.isNumeric(id)) {
				menu = menuService.get(Long.parseLong(id));
				insert = false;
				if(menu == null) {
					throw new DomainException("Menu " + id + " não foi encontrado");
				}
			}

			menu = menuRequest.toMobile(menu);

			if(StringUtils.isEmpty(menuRequest.label)) {
				throw new DomainException("Campo label não pode ser vázio");
			}

			if(StringUtils.isEmpty(menuRequest.codigo)) {
				throw new DomainException("Campo codigo não pode ser vázio");
			}


			if(StringUtils.isEmpty(menuRequest.ordem) && insert) {
				throw new DomainException("Campo ordem não foi informado");
			}
			if(insert) {
				menu.setOrdem(Integer.parseInt(menuRequest.ordem));
			}

			menu.setEmpresa(getEmpresa());

			if(menu.isMenuDefault()) {
			    menuService.removeDefaultMenuMobile(getEmpresa());
            }

			menuService.saveOrUpdate(menu);
			MenuMobileVO menuVO = new MenuMobileVO(menu);
			return Response.ok(MessageResult.ok("Menu criado com sucesso", menuVO)).build();
		} catch(DomainException e) {
			return Response.ok(MessageResult.error(e.getMessage())).build();
		}
	}

	@Path("/mobile/{id}")
	@GET
	public Response getMenuMobileById(@PathParam("id") Long id) {
		try  {
			Menu menu = menuService.get(id);
			if(menu == null) {
				throw new DomainException("Menu com o id " + id + " não encontrado");
			}

			MenuMobileVO vo = new MenuMobileVO(menu);
			return Response.ok(MessageResult.ok("Menu encontrado", vo)).build();
		} catch(DomainException e) {
			return Response.ok(MessageResult.error(e.getMessage())).build();
		}

	}

	@Path("/mobile/menuPadrao")
	@POST
	public Response menuPadrao() {
		try  {
			Empresa empresa = getEmpresa();
			menuService.restoreMenu(empresa);
			return Response.ok(MessageResult.ok("Menu restaurado com sucesso")).build();
		} catch(DomainException e) {
			return Response.ok(MessageResult.error(e.getMessage())).build();
		}

	}


	private void fixLinkMenu(Menu menu) {
		String link = menu.getLink();
		if (StringUtils.isNotEmpty(link)) {
			boolean barra = StringUtils.startsWith(link, "/");
			if (barra) {
				link = link.replaceFirst("/", "");
				menu.setLink(link);
			}
		}
	}

	@GET
	@Path("/default/empresa/{id}")
	public Response menuDefault(@PathParam("id") Long id) {
		try {

			if (id == null) {
				return Response.ok(MessageResult.error("Menus não encontrado para a empresa informada")).build();
			}

			Empresa empresa = empresaService.get(id);

			if (empresa == null) {
				return Response.ok(MessageResult.error("Menus não encontrado para a empresa informada")).build();
			}

			List<Menu> menusLivecom = menuService.findAllDefault();
			if(menusLivecom.isEmpty()) {
				log.debug("Não existem menus default cadastrados");
				return Response.ok(MessageResult.error("Não existem menus default cadastrados")).build();
			}

			menuService.deleteFromEmpresa(empresa);

			Map<String, Menu> map = new HashMap<String, Menu>();
			for (Menu menu : menusLivecom) {

				Menu m = new Menu(menu);
				m.setEmpresa(empresa);

				if(menu.getParent() == null) {
					map.put(m.getLabel(), m);
				} else {
					Menu parent = map.get(menu.getParent().getLabel());
					m.setParent(parent);
				}

				menuService.saveOrUpdate(m);
			}

			return Response.ok(MessageResult.ok("Menus restaurados")).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel restaurar os menus: " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel restaurar os menus: " + e.getMessage())).build();
		}
	}




}
