package br.livetouch.livecom.push;

import java.io.Serializable;
import java.util.Date;

import com.thoughtworks.xstream.XStream;

import br.infra.util.XStreamUtil;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoPush;
import br.livetouch.livecom.domain.vo.NotificationBadge;
import br.livetouch.pushserver.lib.NotificationVO;
import net.livetouch.extras.util.DateUtils;

public class PushNotificationVO implements Serializable {
	private static final long serialVersionUID = -6818120047504380201L;

	public static final String KEY = "PushNotificationVO";
	
	private String tipo;
	private Long userId;
	private Long empresaId;
	private Long arquivoId;
	private Long postId;
	private Long pushId;
	private Long comentarioId;
	private Long conversaId;
	private Long mensagemId;
	private String title;
	private String msg;
	private String data;
	private long timestamp;
	private String tituloOriginal;
	private String dataAgendamento;
	private String titleNot;
	private String subTitleNot;
	private Boolean voip;

	private NotificationBadge badges;
	
	private Boolean processando;
	private Long msgId;

	private String sound;

	public static PushNotificationVO create(Notification n) {
		Post p = n.getPost();
		Usuario user = p.getUsuario();

		PushNotificationVO pushVO = new PushNotificationVO();
		long pushId = p.getId();
		pushVO.setUserId(user.getId());
		pushVO.setPostId(pushId);
		pushVO.setEmpresaId(user.getEmpresa().getId());
		pushVO.setTipo(TipoPush.newPost.toString());
		String titulo = ParametrosMap.getInstance(user.getEmpresa()).get(Params.MSG_PUSH_NEWPOST,"$userPost postou um novo comunicado $titulo");
		titulo = titulo.replace("$userPost", user.getNome());
		titulo = titulo.replace("$titulo", p.getTituloDesc());
		pushVO.setTitle(titulo);
		pushVO.setTituloOriginal(p.getTituloDesc());
		pushVO.setDataAgendamento(p.getDataPublicacao());
		return pushVO;
	}

	public static PushNotificationVO createNotificacaoParticipacaoGrupo(Notification n, Usuario user) {
		PushNotificationVO pushVO = new PushNotificationVO();
		long pushId = 1L;
		pushVO.setUserId(user.getId());
		pushVO.setPostId(pushId);
		pushVO.setEmpresaId(user.getEmpresa().getId());
		pushVO.setTitle(n.getTextoNotificacao());
		pushVO.setTituloOriginal(n.getTitulo());
		pushVO.setDataAgendamento(n.getDataPublicacao());
		return pushVO;
	}

	public static PushNotificationVO createNotificacaoAmizade(Notification n, Usuario user) {
		PushNotificationVO pushVO = new PushNotificationVO();
		long pushId = 1L;
		pushVO.setUserId(user.getId());
		pushVO.setPostId(pushId);
		pushVO.setEmpresaId(user.getEmpresa().getId());
		pushVO.setTitle(n.getTextoNotificacao());
		pushVO.setTituloOriginal(n.getTitulo());
		pushVO.setDataAgendamento(n.getDataPublicacao());
		return pushVO;
	}

	public PushNotificationVO() {
		
	}

	public Long getArquivoId() {
		return arquivoId;
	}

	public void setArquivoId(Long arquivoId) {
		this.arquivoId = arquivoId;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public Long getComentarioId() {
		return comentarioId;
	}

	public void setComentarioId(Long comentarioId) {
		this.comentarioId = comentarioId;
	}

	public Long getMensagemId() {
		return mensagemId;
	}

	public void setMensagemId(Long mensagemId) {
		this.mensagemId = mensagemId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public void setBadges(NotificationBadge badges) {
		this.badges = badges;
	}
	
	public Long getEmpresaId() {
		return empresaId;
	}
	
	public void setEmpresaId(Long empresaId) {
		this.empresaId = empresaId;
	}

	@Override
	public String toString() {
		return toJson();
	}

	public String toJson() {
		// Nao mexer nesse JSON pois esse json é enviado por push pro mobile
		XStream x = XStreamUtil.getXStream(true);
		x.alias("notificacao", getClass());
		String json = x.toXML(this);
		return json;
	}
	
	public NotificationVO toFirebaseJson() {
		NotificationVO notification = new NotificationVO();
		notification.setBody(this.subTitleNot);
		notification.setText(this.titleNot);
		notification.setTitle(this.titleNot);
		notification.setSound(this.sound);
		return notification;
	}

	public NotificationBadge getBadges() {
		return badges;
	}

	public void setConversaId(Long conversaId) {
		this.conversaId = conversaId;
	}

	public Long getConversaId() {
		return conversaId;
	}
	public String getData() {
		return data;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setData(Date date) {
		this.data = DateUtils.toString(date,DateUtils.DATE_TIME_24h);
		this.timestamp = date.getTime();
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getPushId() {
		return pushId;
	}

	public void setPushId(Long pushId) {
		this.pushId = pushId;
	}

	public String getTituloOriginal() {
		return tituloOriginal;
	}

	public void setTituloOriginal(String tituloOriginal) {
		this.tituloOriginal = tituloOriginal;
	}

	public String getDataAgendamento() {
		return dataAgendamento;
	}

	public void setDataAgendamento(Date agendamento) {
		if(agendamento != null) {
			this.dataAgendamento = DateUtils.toString(agendamento,DateUtils.DATE_TIME_24h);
			this.timestamp = agendamento.getTime();
		}
	}

	public String getTitleNot() {
		return titleNot;
	}

	public void setTitleNot(String tituloNot) {
		this.titleNot = tituloNot;
	}

	public String getSubTitleNot() {
		return subTitleNot;
	}

	public void setSubTitleNot(String subTituloNot) {
		this.subTitleNot = subTituloNot;
	}

	public Boolean isProcessando() {
		if(processando == null) {
			return false;
		}
		return processando;
	}

	public void setProcessando(Boolean processando) {
		this.processando = processando;
	}

	public Long getMsgId() {
		return msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	public Boolean getVoip() {
		return voip;
	}

	public void setVoip(Boolean voip) {
		this.voip = voip;
	}
	
	public boolean isVoip() {
		return voip != null ? voip : false;
	}

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}

}
