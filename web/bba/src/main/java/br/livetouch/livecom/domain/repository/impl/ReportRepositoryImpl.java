package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.Plataforma;
import br.livetouch.livecom.domain.repository.ReportRepository;
import br.livetouch.livecom.domain.vo.QuantidadeVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioAudienciaVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioLoginVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoFiltro;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import br.livetouch.spring.StringHibernateRepository;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public class ReportRepositoryImpl extends StringHibernateRepository<Usuario> implements ReportRepository {

	public ReportRepositoryImpl() {
		super(Usuario.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioLoginVO> loginsResumido(RelatorioFiltro filtro) throws DomainException {
		Query query = queryLoginsResumido(filtro, false);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		query.setCacheable(true);
		List<RelatorioLoginVO> logins = query.list();

		return logins;
	}

	public Query queryLoginsResumido(RelatorioFiltro filtro, boolean count) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;

		if (StringUtils.isNotEmpty(filtro.getDataIni())) {
			filtro.setDataInicial(DateUtils.toDate(filtro.getDataIni()));
		}
		if (StringUtils.isNotEmpty(filtro.getDataFim())) {
			filtro.setDataFinal(DateUtils.toDate(filtro.getDataFim()));
		}

		Usuario usuario = filtro.getUsuario();
		String login = usuario != null ? usuario.getLogin() : null;
		Long empresa = filtro.getEmpresaId();

		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		if (count) {
			sb.append("select count(distinct l.login) ");
		} else {
			sb.append("select new br.livetouch.livecom.domain.vo.RelatorioLoginVO (STR_TO_DATE(DATE_FORMAT(l.data,'%d/%m/%Y'), '%d/%m/%Y'),l.plataforma,count(l.plataforma)) ");
		}

		sb.append(" from LoginReport l");

		sb.append(" where 1=1 ");

		if (dataInicio != null) {
			sb.append(" and l.data >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and l.data <= :dataFim");
		}

		if (StringUtils.isNotEmpty(login)) {
			sb.append(" and l.login = :login");
		}

		if (empresa != null) {
			sb.append(" and l.empresa.id = :empresa");
		}

		sb.append(" and l.status = 1");

		sb.append(" group by STR_TO_DATE(DATE_FORMAT(l.data,'%d/%m/%Y'), '%d/%m/%Y'), plataforma");
		sb.append(" order by STR_TO_DATE(DATE_FORMAT(l.data,'%d/%m/%Y'), '%d/%m/%Y') desc");

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (StringUtils.isNotEmpty(login)) {
			q.setParameter("login", login);
		}

		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}

		q.setCacheable(true);

		return q;

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioLoginVO> reportVersao(RelatorioFiltro filtro) throws DomainException {

		Query query = queryReportVersao(filtro, false);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		query.setCacheable(true);
		List<RelatorioLoginVO> logins = query.list();

		return logins;

	}
	
	public Query queryReportVersao(RelatorioFiltro filtro, boolean count) throws DomainException {

		Usuario usuario = filtro.getUsuario();
		Long empresa = filtro.getEmpresaId();
		String version = filtro.getVersion();
		String versionCode = filtro.getVersionCode();
		String os = StringUtils.upperCase(filtro.getOs());

		StringBuffer sb = new StringBuffer();
		sb.append("select new br.livetouch.livecom.domain.vo.RelatorioLoginVO (u, l) ");

		sb.append(" from LoginReport l right join l.usuario u ");

		sb.append(" where 1=1 ");

		if (usuario != null) {
			sb.append(" and u = :usuario");
		}

		if (StringUtils.isNotEmpty(versionCode)) {
			sb.append(" and u.deviceVersionCode = :versionCode");
		}

		if (StringUtils.isNotEmpty(version)) {
			sb.append(" and u.deviceAppVersion = :version");
		}
		
		if (StringUtils.isNotEmpty(os)) {
			sb.append(" and l.plataforma = :plataforma");
		}

		if (empresa != null) {
			sb.append(" and u.empresa.id = :empresa");
		}

		sb.append(" group by u.login ");
		sb.append(" order by u.login");

		Query q = createQuery(sb.toString());

		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}

		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		if (StringUtils.isNotEmpty(versionCode)) {
			q.setParameter("versionCode", versionCode);
		}

		if (StringUtils.isNotEmpty(version)) {
			q.setParameter("version", version);
		}
		
		if (StringUtils.isNotEmpty(os)) {
			q.setParameter("plataforma", Plataforma.valueOf(os));
		}

		q.setCacheable(true);

		return q;

	}
	
	@SuppressWarnings("unchecked")
	public List<QuantidadeVersaoVO> reportVersaoGrafico(RelatorioFiltro filtro) throws DomainException {
		Long empresa = filtro.getEmpresaId();
		String os = StringUtils.upperCase(filtro.getOs());
		
		StringBuffer sb = new StringBuffer();
		
		if(!filtro.isVersionCodeGrafico()){
			sb.append("SELECT new br.livetouch.livecom.domain.vo.QuantidadeVersaoVO(count(distinct u), u.deviceAppVersion, l.plataforma) from LoginReport l right join l.usuario as u WHERE 1 = 1");
		} else {
			sb.append("SELECT new br.livetouch.livecom.domain.vo.QuantidadeVersaoVO(count(distinct u), u.deviceVersionCode, l.plataforma) from LoginReport l right join l.usuario as u WHERE 1 = 1");
		}
		
		if(empresa != null) {
			sb.append(" AND u.empresa.id = :empresa");
		}
		
		if(StringUtils.isNotEmpty(os)) {
			sb.append(" AND l.plataforma = :plataforma ");
		} else {
			os = "WEB";
			sb.append(" AND l.plataforma != :plataforma ");
		}
		
		if(!filtro.isVersionCodeGrafico()){
			sb.append(" AND u.deviceAppVersion IS NOT NULL ");
		} else {
			sb.append(" AND u.deviceVersionCode IS NOT NULL ");
		}
		
		if(!filtro.isVersionCodeGrafico()){
			sb.append(" GROUP BY u.deviceAppVersion, l.plataforma");
		} else {
			sb.append(" GROUP BY u.deviceVersionCode, l.plataforma");
		}
		
		Query q = createQuery(sb.toString());
		
		if(empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		q.setParameter("plataforma", Plataforma.valueOf(os));
		
		List<QuantidadeVersaoVO> quantidadeVersao = q.list();
		return quantidadeVersao;

	}
	
	@Override
	public long getCountloginsResumido(RelatorioFiltro filtro) throws DomainException {
		Query query = queryLoginsResumido(filtro, true);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioLoginVO> loginsExpandido(RelatorioFiltro filtro) throws DomainException {

		Query query = queryLoginsExpandido(filtro, false);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		query.setCacheable(true);
		List<RelatorioLoginVO> logins = query.list();

		return logins;

	}
	
	public Query queryLoginsExpandido(RelatorioFiltro filtro, boolean count) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;

		if (StringUtils.isNotEmpty(filtro.getDataIni())) {
			filtro.setDataInicial(DateUtils.toDate(filtro.getDataIni()));
		}
		if (StringUtils.isNotEmpty(filtro.getDataFim())) {
			filtro.setDataFinal(DateUtils.toDate(filtro.getDataFim()));
		}

		Usuario usuario = filtro.getUsuario();
		String login = usuario != null ? usuario.getLogin() : "";
		Long empresa = filtro.getEmpresaId();

		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		sb.append("select new br.livetouch.livecom.domain.vo.RelatorioLoginVO (data, login, usuario.id, plataforma) ");

		sb.append(" from LoginReport ");

		sb.append(" where 1=1 ");

		if (dataInicio != null) {
			sb.append(" and data >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and data <= :dataFim");
		}

		if (StringUtils.isNotEmpty(login)) {
			sb.append(" and login = :login");
		}

		if (empresa != null) {
			sb.append(" and empresa.id = :empresa");
		}

		sb.append(" and status = 1");

		sb.append(" order by data desc");

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (StringUtils.isNotEmpty(login)) {
			q.setParameter("login", login);
		}

		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}

		q.setCacheable(true);

		return q;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioLoginVO> loginsAgrupado(RelatorioFiltro filtro) throws DomainException {

		Query query = queryLoginsAgrupado(filtro, false);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		query.setCacheable(true);
		List<RelatorioLoginVO> logins = query.list();

		return logins;

	}

	public Query queryLoginsAgrupado(RelatorioFiltro filtro, boolean count) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;

		if (StringUtils.isNotEmpty(filtro.getDataIni())) {
			filtro.setDataInicial(DateUtils.toDate(filtro.getDataIni()));
		}
		if (StringUtils.isNotEmpty(filtro.getDataFim())) {
			filtro.setDataFinal(DateUtils.toDate(filtro.getDataFim()));
		}

		Usuario usuario = filtro.getUsuario();
		String login = usuario != null ? usuario.getLogin() : "";
		Long empresa = filtro.getEmpresaId();

		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		sb.append("select new br.livetouch.livecom.domain.vo.RelatorioLoginVO (STR_TO_DATE(DATE_FORMAT(data,'%d/%m/%Y'), '%d/%m/%Y'), login, usuario.id, plataforma, count(login)) ");

		sb.append(" from LoginReport ");

		sb.append(" where 1=1 ");

		if (dataInicio != null) {
			sb.append(" and data >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and data <= :dataFim");
		}

		if (StringUtils.isNotEmpty(login)) {
			sb.append(" and login = :login");
		}

		if (empresa != null) {
			sb.append(" and empresa.id = :empresa");
		}

		sb.append(" and status = 1");

		sb.append(" group by login, usuario.id, plataforma, STR_TO_DATE(DATE_FORMAT(data,'%d/%m/%Y'), '%d/%m/%Y')");
		sb.append(" order by login");

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (StringUtils.isNotEmpty(login)) {
			q.setParameter("login", login);
		}

		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}

		q.setCacheable(true);

		return q;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioVisualizacaoVO> relVisualizacaoConsolidado(RelatorioVisualizacaoFiltro filtro) {
		Date dataInicio = null;
		Date dataFim = null;

		Long empresa = filtro.getEmpresaId();

		if (StringUtils.isNotEmpty(filtro.getDataInicialString())) {
			dataInicio = DateUtils.toDate(filtro.getDataInicialString());
		}
		if (StringUtils.isNotEmpty(filtro.getDataFinalString())) {
			dataFim = DateUtils.toDate(filtro.getDataFinalString());
		}

		if (filtro.getDataInicial() != null) {
			dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
		}
		if (filtro.getDataFinal() != null) {
			dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
		}

		StringBuffer sb = new StringBuffer();
		sb.append("select distinct new br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO (DATE_FORMAT(p.dataPublicacao,'%d/%m/%Y %H:%m:%s'), p, SUM(coalesce(pv.contador,0)), SUM(coalesce(pv.contadorLido,0))) ");

		sb.append(" from PostView pv inner join pv.post p ");

		sb.append(" where 1=1 ");

		if (dataInicio != null) {
			sb.append(" and p.dataPublicacao >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and p.dataPublicacao <= :dataFim");
		}
		if (filtro.getUsuarioId() != null) {
			sb.append(" and p.usuario.id= :usuario ");
		}
		if (filtro.getPostId() != null) {
			sb.append(" and p.id= :post ");
		}

		if (empresa != null) {
			sb.append(" and p.usuario.empresa.id = :empresa ");
		}

		if (filtro.getCategoriaId() != null) {
			sb.append(" and p.categoria.id= :categoria ");
		}
		
		sb.append(" group by p, p.dataPublicacao");
		sb.append(" order by p.dataPublicacao desc");

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}
		if (filtro.getUsuarioId() != null) {
			q.setLong("usuario", filtro.getUsuarioId());
		}
		if (filtro.getPostId() != null) {
			q.setLong("post", filtro.getPostId());
		}
		if (filtro.getCategoriaId() != null) {
			q.setLong("categoria", filtro.getCategoriaId());
		}

		if (empresa != null) {
			q.setLong("empresa", empresa);
		}
		
		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		q.setFirstResult(firstResult);
		q.setMaxResults(filtro.getMax());

		List<RelatorioVisualizacaoVO> list = q.list();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioAudienciaVO> findAudiencia(RelatorioFiltro filtro) throws DomainException {
		Query query = queryAudiencia(filtro, false);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		query.setCacheable(true);
		List<RelatorioAudienciaVO> audiencia = query.list();

		return audiencia;
	}

	public Query queryAudiencia(RelatorioFiltro filtro, boolean count) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;

		Long empresa = filtro.getEmpresaId();

		if (StringUtils.isNotEmpty(filtro.getDataIni())) {
			filtro.setDataInicial(DateUtils.toDate(filtro.getDataIni()));
		}
		if (StringUtils.isNotEmpty(filtro.getDataFim())) {
			filtro.setDataFinal(DateUtils.toDate(filtro.getDataFim()));
		}

		if (filtro.getDataInicial() != null) {
			dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
		}
		if (filtro.getDataFinal() != null) {
			dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
		}

		StringBuffer sb = new StringBuffer();
		sb.append("select new br.livetouch.livecom.domain.vo.RelatorioAudienciaVO(DATE_FORMAT(pv.post.dataPublicacao,'%d/%m/%Y'), pv.post, sum(cast(pv.contador as int) + cast(pv.contadorLido as int))) ");

		sb.append(" from PostView pv ");

		sb.append(" where 1=1 ");

		if (dataInicio != null) {
			sb.append(" and pv.post.dataPublicacao >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and pv.post.dataPublicacao <= :dataFim");
		}
		
		if (filtro.getUsuarioId() != null) {
			sb.append(" and pv.post.usuario.id= :usuario ");
		}
		
		if (filtro.getPostId() != null) {
			sb.append(" and pv.post.id= :post ");
		}

		if (empresa != null) {
			sb.append(" and pv.post.usuario.empresa.id = :empresa ");
		}

		if (filtro.getCategoriaId() != null) {
			sb.append(" and pv.post.categoria.id= :categoria ");
		}

		sb.append(" group by pv.post, pv.post.dataPublicacao");
		sb.append(" order by pv.post.dataPublicacao desc");

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}
		if (filtro.getUsuarioId() != null) {
			q.setLong("usuario", filtro.getUsuarioId());
		}
		if (filtro.getPostId() != null) {
			q.setLong("post", filtro.getPostId());
		}
		if (filtro.getCategoriaId() != null) {
			q.setLong("categoria", filtro.getCategoriaId());
		}

		if (empresa != null) {
			q.setLong("empresa", empresa);
		}
		
		return q;
	}

	@Override
	public long getCountAudiencia(RelatorioFiltro filtro) throws DomainException {
		Query query = queryAudiencia(filtro, true);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioAudienciaVO> audienciaDetalhes(RelatorioFiltro filtro) throws DomainException {

		Query query = queryAudienciaDetalhes(filtro);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		if (filtro.getMax() != 0) {
			query.setFirstResult(firstResult);
			query.setMaxResults(filtro.getMax());
		}

		query.setCacheable(false);
		List<RelatorioAudienciaVO> audiencia = query.list();

		return audiencia;

	}
	
	public Query queryAudienciaDetalhes(RelatorioFiltro filtro) throws DomainException {

		Long post = filtro.getPostId();
		
		//true = mostra só os que interagiram
		boolean agrupar = filtro.isAgrupar();

		StringBuffer sb = new StringBuffer();
		
		sb.append("select p.id as postId, p.titulo as titulo, u.nome as usuario, ");
		sb.append("u.id as usuarioId, COALESCE(pv.lido, 0) as lida, COALESCE(pv.visualizado, 0) as visualizado, ");
		sb.append("count(distinct l.id) as countLikes, count(distinct f.id) as countFavoritos, ");
		sb.append("count(distinct c.id) as countComentarios ");
		sb.append("from post p ");
		sb.append("inner join post_grupos pg on pg.post_id = p.id ");
		sb.append("inner join grupo_usuarios gu on pg.grupo_id = gu.grupo_id ");
		sb.append("inner join usuario u on gu.usuario_id = u.id ");
		sb.append("left join post_view pv on pv.post_id = p.id and pv.usuario_id = u.id ");
		sb.append("left join comentario c on c.post_id = p.id and c.usuario_id = u.id ");
		sb.append("left join favorito f on f.post_id = p.id and f.favorito = 1 and f.usuario_id = u.id ");
		sb.append("left join likes l on l.post_id = p.id and l.comentario_id is null and l.favorito = 1 and l.usuario_id = u.id ");
		sb.append("where 1=1 ");
		sb.append("and p.id = :post and (u.id in(l.usuario_id) OR u.id in(f.usuario_id) OR u.id in(c.usuario_id) OR u.id in(pv.usuario_id) ");
		
		if(!agrupar) {
			sb.append("OR u.id in(gu.usuario_id)");
		}
		
		sb.append(")");
		sb.append("group by p.id, p.titulo, u.id, u.nome, pv.lido ");
		sb.append("order by countLikes desc, countFavoritos desc, countComentarios desc, lida desc, visualizado desc, u.nome");
		
		Query q = getSession().createSQLQuery(sb.toString())
		.addScalar("postId",StandardBasicTypes.LONG)
		.addScalar("titulo",StandardBasicTypes.STRING)
		.addScalar("usuario",StandardBasicTypes.STRING)
		.addScalar("usuarioId",StandardBasicTypes.LONG)
		.addScalar("lida",StandardBasicTypes.BOOLEAN)
		.addScalar("visualizado",StandardBasicTypes.BOOLEAN)
		.addScalar("countLikes",StandardBasicTypes.LONG)
		.addScalar("countFavoritos",StandardBasicTypes.LONG)
		.addScalar("countComentarios",StandardBasicTypes.LONG)
        .setResultTransformer(new AliasToBeanResultTransformer(RelatorioAudienciaVO.class));

		if (post != null) {
			q.setParameter("post", post);
		}

		return q;

	}
	
}