package br.livetouch.livecom.web.pages.root;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.vo.EmpresaVO;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.web.pages.LivecomRootPage;

/**
 * 
 */
@Controller
@Scope("prototype")
public class EmpresasPage extends LivecomRootPage {

	public Long id;
	public boolean select;

	public Empresa empresa;

	public List<Empresa> empresas;

	@Override
	public void onInit() {
		super.onInit();
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();

		empresas = empresaService.findAll();

		if (id != null) {
			empresa = empresaService.get(id);

			if(select) {
				if (empresa != null) {
					UserInfoVO userInfoVO = getUserInfoVO();
					if (userInfoVO != null) {
                        Empresa empresaROOT = empresaService.get(empresa.getId());
                        EmpresaVO empresaVO = new EmpresaVO(empresaROOT);
                        userInfoVO.setEmpresaRoot(empresaVO);
                        userInfoVO.setEmpresaId(this.empresa.getId());
					}
					setRedirect(ParametrosPage.class);
					setFlashAttribute("msg", "Empresa [" + empresa.getNome() + "] selecionada com sucesso.");
				}
			}
		}
	}
}
