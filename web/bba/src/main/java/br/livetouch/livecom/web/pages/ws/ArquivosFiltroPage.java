package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.impl.ArquivoRepositoryImpl.TipoBuscaArquivo;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.utils.FileExtensionUtils;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ArquivosFiltroPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tUserId;
	private TextField tTipoArquivo;
	private TextField tNome;
	private TextField tMode;
	private TextField tModoBusca;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tUserId = new TextField("user_id", true));
		form.add(tTipoArquivo = new TextField("tipo arquivo"));
		form.add(tNome= new TextField("texto","Texto"));
		
		form.add(new TextField("maxRows"));
		form.add(new TextField("page"));
		
		form.add(tModoBusca = new TextField("modoBusca", "Modo busca (todos ou meusArquivos)	", true) );
		tModoBusca.setValue("todos");
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		tUserId.setFocus(true);

		form.add(new Submit("consultar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			String userIdText = tUserId.getValue();
			String tipoArquivo = tTipoArquivo.getValue();
			String nome = tNome.getValue();
			String modoBusca = tModoBusca.getValue();
			long userId = -1;

			if(StringUtils.isNotEmpty(userIdText)) {
				if(NumberUtils.isNumber(userIdText)) {
					userId = new Long(userIdText);
				}
				else {
					if(isWsVersion3()) {
						Response r = Response.error("Usuário incorreto.");
						return r;
					}
					return new MensagemResult("ERROR","Usuário incorreto.");
				}
			} else {
				if(isWsVersion3()) {
					Response r = Response.error("Informe usuário");
					return r;
				}
				return new MensagemResult("ERROR","Informe usuário");
			}
			
			Usuario u = usuarioService.get(userId);
			
			if(u == null) {
				if(isWsVersion3()) {
					Response r = Response.error("Usuário inválido.");
					return r;
				}
				return new MensagemResult("ERROR","Usuário inválido.");
			}

			
			int maxRows = NumberUtils.toInt(getContext().getRequestParameter("maxRows"),20);

			TipoBuscaArquivo tipo;
			if (modoBusca.equalsIgnoreCase("todos")) {
				tipo = TipoBuscaArquivo.TODOS_ARQUIVOS_QUE_USUARIO_POSSUI_ACESSO;
			} else if (modoBusca.equalsIgnoreCase("meusArquivos")) {
				tipo = TipoBuscaArquivo.POSTADOS_PELO_USUARIO;
			} else {
				if(isWsVersion3()) {
					Response r = Response.error("Tipo de busca inválido");
					return r;
				}
				return new MensagemResult("ERROR","Tipo de busca inválido");
			}
			
			String[] tiposArquivo = tipoArquivo.split(",");
			String[] tipos = FileExtensionUtils.getExtensionsByTipo(tiposArquivo);
			
			List<Arquivo> list = arquivoService.findAllByTipo(u, tipos, tipo, page, maxRows,nome);
			
			List<FileVO> listVo = arquivoService.toListVo(u, list, null);
			if(isWsVersion3()) {
				Response r = Response.ok("OK");
				r.listFiles = listVo;
				return r;
			}
			
			return listVo;
		}
		
		if(isWsVersion3()) {
			Response r = Response.error("Preencha o id do usuario");
			return r;
		}
		return new MensagemResult("NOK","Preencha o id do usuario");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("arquivos", FileVO.class);
		x.alias("thumb", ThumbVO.class);
		x.alias("thumb", ArquivoThumb.class);
		super.xstream(x);
	}
}
