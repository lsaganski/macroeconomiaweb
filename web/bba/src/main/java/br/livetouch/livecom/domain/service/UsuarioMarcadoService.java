package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.UsuarioMarcado;
import net.livetouch.tiger.ddd.DomainException;

public interface UsuarioMarcadoService extends Service<UsuarioMarcado> {

	List<UsuarioMarcado> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(UsuarioMarcado f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(UsuarioMarcado f) throws DomainException;

	UsuarioMarcado find(Comentario comentario, Usuario usuario);

	void delete(List<Long> idsMarcados, Comentario c);

}
