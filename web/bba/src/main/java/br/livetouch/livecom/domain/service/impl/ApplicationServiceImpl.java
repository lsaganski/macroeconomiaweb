package br.livetouch.livecom.domain.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Application;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.ApplicationRepository;
import br.livetouch.livecom.domain.service.ApplicationService;
import br.livetouch.livecom.rest.MyApplication;
import br.livetouch.livecom.rest.oauth.ConsumerKeyMap;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ApplicationServiceImpl implements ApplicationService {
	@Autowired
	private ApplicationRepository rep;

	@Override
	public Application get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Application a) throws DomainException {
		rep.saveOrUpdate(a);
		
		init();
	}

	@Override
	public List<Application> findAll() {
		return rep.findAll();
	}

	@Override
	public void delete(Application a) throws DomainException {
		rep.delete(a);
		
		init();
	}

	@Override
	public void generateKeys(Application app) throws DomainException {
		String consumerKey = generateConsumerKey();
		String consumerSecret = generateConsumerSecret();
		app.setConsumerKey(consumerKey);
		app.setConsumerSecret(consumerSecret);
	}
	
	private String generateConsumerKey() {
		UUID uuid = UUID.randomUUID();
		String key = uuid.toString();
		List<Application> list = rep.findBy("consumerKey", key);
		if (list.size() > 0) {
			return generateConsumerKey();
		}
		return key;
	}
	
	private String generateConsumerSecret() {
		UUID uuid = UUID.randomUUID();
		String key = uuid.toString();
		List<Application> list = rep.findBy("consumerSecret", key);
		if (list.size() > 0) {
			return generateConsumerSecret();
		}
		return key;
	}
	

	@Override
	public List<Application> findByUsuario(Usuario usuario) {
		return rep.findByUsuario(usuario);
	}

	@Override
	public Boolean exists(Application app) {
		return rep.exists(app);
	}

	@Override
	public Application get(Long id, Usuario u) {
		
		if(u == null) {
			return null;
		}
		
		return rep.get(id, u);
	}

	@Override
	public void update(Application a) {
		
		Application app = this.get(a.getId());
		
		app.setNome(a.getNome());
		app.setDescricao(a.getDescricao());
		app.setWebsite(a.getWebsite());
		app.setCallbackUrl(a.getCallbackUrl());
		
		rep.update(app);
	}

	@Override
	public Application find(String consumerKey, String consumerSecret) {
		return rep.find(consumerKey, consumerSecret);
	}

	@Override
	public ConsumerKeyMap init() {
		List<Application> list = findAll();
		ConsumerKeyMap keys = ConsumerKeyMap.getInstance();
		
		keys.clear();
		for (Application app : list) {
			keys.put(app.getNome(), app.getConsumerKey(), app.getConsumerSecret());
		}
		
		String consumerKey = ParametrosMap.getInstance().get(Params.LIVECOM_CONSUMER_KEY, MyApplication.CONSUMER_KEY);
		String consumerSecret = ParametrosMap.getInstance().get(Params.LIVECOM_CONSUMER_SECRET, MyApplication.CONSUMER_SECRET);
		String name = ParametrosMap.getInstance().get(Params.LIVECOM_CONSUMER_NAME, MyApplication.NAME);
		keys.put(name, consumerKey, consumerSecret);
		
		return keys;
	}
}
