package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.CampoUsersVO;
import br.livetouch.livecom.domain.vo.CamposMap;
import br.livetouch.livecom.domain.vo.ProfileVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class UsuarioPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private UsuarioLoginField tUser;
	private TextField tMode;
	public CamposMap campos;

	@Override
	public void onInit() {
		super.onInit();
		
		campos = getCampos();

		form.setMethod("post");
		form.add(tUser = new UsuarioLoginField("user","login", true, usuarioService, getEmpresa()));
		
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));
		
		tMode.setValue("json");
		
		tUser.setFocus(true);

		form.add(new Submit("login"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			try {
				Usuario u = tUser.getEntity();
				
				if(u != null) {
					UsuarioVO userVO = new UsuarioVO();
					userVO.setUsuario(u, usuarioService, grupoService);
					// Profile
					String buildType = getContext().getRequest().getParameter("buildType");
					ProfileVO p = ProfileVO.create(u, usuarioService, buildType, campos);
					userVO.profile = p;
					
					if(isWsVersion3()) {
						Response r = Response.ok("OK");
						r.user = userVO;
						return r;
					}
					
					return userVO;
				}
			} catch (Exception e) {
				logError(e.getMessage(), e);
				return new MensagemResult("NOK","Erro ao buscar usuário.");
			}
		}
		
		return new MensagemResult("NOK","Usuario não encontrado");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("user", UsuarioVO.class);
		x.alias("campo", CampoUsersVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
	
	@Override
	protected boolean isValidaHorario() {
		return super.isValidaHorario();
	}
}
