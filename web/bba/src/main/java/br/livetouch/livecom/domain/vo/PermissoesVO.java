package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.PerfilPermissao;

public class PermissoesVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Map<String, Boolean> permissoes;
	
	public void setMapPermissoes(Set<PerfilPermissao> permissao) {
		
		if(permissoes == null) {
			permissoes = new HashMap<String, Boolean>();
		}
		
		for (PerfilPermissao p : permissao) {
			if(p.getPermissao() != null) {
				permissoes.put(p.getPermissao().getCodigo(), p.isLigado());
			}
		}
	}
	
	public boolean isPublicar() {
		return isPermissao(ParamsPermissao.PUBLICAR);
	}

	public boolean isComentar() {
		return isPermissao(ParamsPermissao.COMENTAR);
	}

	public boolean isCurtir() {
		return isPermissao(ParamsPermissao.CURTIR_PUBLICACAO);
	}

	public boolean isEnviarMensagem() {
		return isPermissao(ParamsPermissao.ENVIAR_MENSAGEM);
	}

	public boolean isExcluirPostagem() {
		return isPermissao(ParamsPermissao.EXCLUIR_PUBLICACAO);
	}

	public boolean isEditarPostagem() {
		return isPermissao(ParamsPermissao.EDITAR_PUBLICACAO);
	}

	public boolean isCadastrarUsuarios() {
		return isPermissao(ParamsPermissao.CADASTRAR_USUARIOS);
	}

	public boolean isArquivos() {
		return isPermissao(ParamsPermissao.ARQUIVOS);
	}

	public boolean isPadrao() {
		return isPermissao(ParamsPermissao.PADRAO);
	}

	public boolean isCadastrarTabelas() {
		return isPermissao(ParamsPermissao.CADASTRAR_USUARIOS);
	}

	public boolean isCodigo() {
		return isPermissao(ParamsPermissao.CADASTRAR_CODIGOS);
	}

	public boolean isVisualizarComentario() {
		return isPermissao(ParamsPermissao.VIZUALIZAR_COMENTARIOS);
	}

	public boolean isVisualizarRelatorio() {
		return isPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS);
	}

	public boolean isVisualizarConteudo() {
		return isPermissao(ParamsPermissao.VIZUALIZAR_CONTEUDO);
	}

	public Map<String, Boolean> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Map<String, Boolean> permissoes) {
		this.permissoes = permissoes;
	}
	
	public boolean isPermissao(String key) {
		if(permissoes == null) {
			return false;
		}
		
		return permissoes.get(key) != null ? permissoes.get(key) : false;
	}
}
