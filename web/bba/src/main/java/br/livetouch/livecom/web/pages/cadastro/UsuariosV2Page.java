package br.livetouch.livecom.web.pages.cadastro;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.admin.LivecomAdminPage;


@Controller
@Scope("prototype")
public class UsuariosV2Page extends LivecomAdminPage {

	public boolean escondeChat = true;
	@Override
	public void onInit() {
		super.onInit();
	}


	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
