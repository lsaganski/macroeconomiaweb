package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.PerfilPermissao;
import br.livetouch.livecom.domain.repository.PerfilRepository;

@Repository
@SuppressWarnings("unchecked")
public class PerfilRepositoryImpl  extends LivecomRepository<Perfil> implements PerfilRepository {

	public PerfilRepositoryImpl() {
		super(Perfil.class);
	}
	
	@Override
	public void saveOrUpdate(PerfilPermissao p) {
		getSession().saveOrUpdate(p);
	}

	@Override
	public Perfil findByNome(String login, Empresa e) {
		List<Perfil> list = createQuery("from Perfil where nome=? and empresa.id = ?")
		.setString(0, login).setLong(1, e.getId()).list();
		Perfil Permissao = list.isEmpty() ? null : list.get(0);
		return Permissao;
	}
	
	@Override
	public Perfil findByNomeValid(Perfil perfil, Empresa e) {
		StringBuffer sb = new StringBuffer("select distinct p from Perfil p where p.nome = :nome");
		
		if(perfil.getId() != null) {
			sb.append(" and p.id != :id");
		}

		if(e != null) {
			sb.append(" and p.empresa = :empresa");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("nome", perfil.getNome());
		
		if(perfil.getId() != null) {
			q.setParameter("id", perfil.getId());
		}
		
		if(e != null) {
			q.setParameter("empresa", e);
		}
		
		List<Perfil> list = q.list();
		Perfil d = (Perfil) (list.size() > 0 ? q.list().get(0) : null);
		return d;
	}

	@Override
	public List<Object[]> findIdNomePerfil() {
		StringBuffer sb = new StringBuffer("select id,nome from Perfil p where 1=1");
		Query q = createQuery(sb.toString());
		return q.list();
	}
	
	@Override
	public List<Perfil> findAll(Empresa empresa) {
		StringBuffer sb = new StringBuffer("FROM Perfil p WHERE p.empresa.id = :empresaId");
		Query q = createQuery(sb.toString());
		q.setParameter("empresaId", empresa.getId());
		q.setCacheable(true);
		return q.list();
	}
	
	@Override
	public void delete(Perfil p) {
		//Remover Perfis associados a MenuPerfil
		execute("delete from MenuPerfil mp where mp.perfil.id = ?)", p.getId());
		execute("delete from PerfilPermissao pp where pp.perfil.id = ?)", p.getId());
		execute("delete from Perfil p where p.id = ?)", p.getId());
	}
	
	@Override
	public List<Perfil> findDefaults() {
		StringBuffer sb = new StringBuffer("FROM Perfil p WHERE p.empresa is null");
		Query q = createQuery(sb.toString());
		return q.list();
	}
	
	@Override
	public void removePermissoes(Perfil p) {
		execute("delete from PerfilPermissao pp where pp.perfil.id = ?)", p.getId());
	}

}
