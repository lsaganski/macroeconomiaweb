package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.gson.Gson;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.livetouch.livecom.domain.vo.CategoriaVO;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CategoriaPost extends JsonEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1424214675602321861L;

	public static final String KEY = "CategoriaPost";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "CATEGORIAPOST_SEQ")
	private Long id;

	private String nome;

	private Boolean padrao;

	private Boolean menu;
	private Boolean likeable;
	private Boolean hideData;
	private String urlIcone;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categoria_parent_id", nullable = true)
	private CategoriaPost categoriaParent;

	@OneToMany(mappedBy = "categoriaParent", fetch = FetchType.LAZY)
	@OrderBy(value = "id")
	@XStreamOmitField
	private Set<CategoriaPost> categorias;
	
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "post_categorias", joinColumns = { @JoinColumn(name = "categoria_id") }, inverseJoinColumns = { @JoinColumn(name = "post_id") })
	@XStreamOmitField
	private Set<Post> posts;
	
	private Integer ordem;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_create_id", nullable = true)
	private Usuario userCreate;
	private Date dateCreate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_update_id", nullable = true)
	private Usuario userUpdate;
	private Date dateUpdate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;
	
	@OneToMany(mappedBy = "categoria", fetch = FetchType.LAZY)
	@XStreamOmitField
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Arquivo> arquivos;
	
	private String codigo;
	
	@Column(length=12)
	private String cor;
	
	@OneToMany(mappedBy = "categoria", fetch = FetchType.LAZY)
	private Set<CategoriaIdioma> idiomas;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}

	public String toStringAll() {
		return "CategoriaPost [id=" + id + ", nome=" + nome + ", posts=" + posts + ", padrao=" + padrao + ", categoriaParent=" + categoriaParent + ", categorias=" + categorias + ", ordem=" + ordem + ", userCreate=" + userCreate + ", dateCreate=" + dateCreate + ", userUpdate=" + userUpdate
				+ ", dateUpdate=" + dateUpdate + "] ";
	}
	
	@Override
	public String toString() {
		return "id=" + id + ", nome=" + nome;
	}

	public Boolean isPadrao() {
		if (padrao == null) {
			return false;
		}
		return padrao;
	}

	public Boolean getPadrao() {
		if (padrao == null) {
			return false;
		}
		return padrao;
	}

	public void setPadrao(Boolean padrao) {
		this.padrao = padrao;
	}

	public CategoriaPost getCategoriaParent() {
		return categoriaParent;
	}

	public void setCategoriaParent(CategoriaPost categoriaParent) {
		this.categoriaParent = categoriaParent;
	}

	public Set<CategoriaPost> getCategorias() {
		return categorias;
	}

	public void setCategorias(Set<CategoriaPost> categorias) {
		this.categorias = categorias;
	}

	public boolean hasChildren() {
		return getCategorias().size() > 0;
	}

	public boolean hasParent() {
		return categoriaParent != null;
	}

	public String getPath() {
		String path = getNome();
		if (categoriaParent != null) {
			path = categoriaParent.getPath() + "/" + path;
		}
		return path;
	}

	public List<Long> getParentIds() {
		List<Long> parentIds = new ArrayList<Long>();
		addRecursiveCategParent(parentIds, this);
		return parentIds;
	}

	public List<Long> getChildrenIds() {
		List<Long> parentIds = new ArrayList<Long>();
		addRecursiveChildrenParent(parentIds, this);
		return parentIds;
	}
	
	private void addRecursiveCategParent(List<Long> ids, CategoriaPost c) {
		if (c != null) {
			CategoriaPost parent = c.getCategoriaParent();
			if (parent != null) {
				ids.add(parent.getId());
				addRecursiveCategParent(ids, parent);
			}
		}
	}

	private void addRecursiveChildrenParent(List<Long> ids, CategoriaPost c) {
		if (c != null) {
			Set<CategoriaPost> children = c.getCategorias();
			if (children != null) {
				for (CategoriaPost child : children) {
					ids.add(child.getId());
					addRecursiveChildrenParent(ids, child);
				}
			}
		}
	}

	public Integer getOrdem() {
		if (ordem == null) {
			return 0;
		}
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public Date getDateCreate() {
		return dateCreate;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}
	
	public Usuario getUserCreate() {
		return userCreate;
	}
	public Usuario getUserUpdate() {
		return userUpdate;
	}
	
	public void setUserCreate(Usuario userCreate) {
		this.userCreate = userCreate;
	}
	public void setUserUpdate(Usuario userUpdate) {
		this.userUpdate = userUpdate;
	}

	public String getCor() {
		if(cor == null) {
			return "0x000000";
		}
		return cor;
	}
	
	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getCodigoDesc() {
		return codigo != null ? codigo : id.toString();
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public String toJson() {
		CategoriaVO vo = new CategoriaVO(this);
		return new Gson().toJson(vo);
	}

	public Boolean isMenu() {
		if (menu == null) {
			return false;
		}
		return menu;
	}

	public void setMenu(Boolean menu) {
		this.menu = menu;
	}

	public String getUrlIcone() {
		return urlIcone;
	}

	public void setUrlIcone(String urlIcone) {
		this.urlIcone = urlIcone;
	}

	public String toStringIdDesc() {
		return id + ": " + nome;
	}

	public Boolean isLikeable() {
		if (likeable == null) {
			return true;
		}
		return likeable;
	}

	public void setLikeable(Boolean likeable) {
		this.likeable = likeable;
	}

	public Boolean isHideData() {
		if (hideData == null) {
			return false;
		}
		return hideData;
	}

	public void setHideData(Boolean hideData) {
		this.hideData = hideData;
	}

	public Set<Arquivo> getArquivos() {
		return arquivos;
	}

	public void setArquivos(Set<Arquivo> arquivos) {
		this.arquivos = arquivos;
	}

	public Set<CategoriaIdioma> getIdiomas() {
		return idiomas;
	}

	public void setIdiomas(Set<CategoriaIdioma> idiomas) {
		this.idiomas = idiomas;
	}
	
	public void translate() {
		if(this.idiomas != null && !this.idiomas.isEmpty()) {
			translate(this.idiomas.iterator().next());
		}
	}
	
	public void translate(CategoriaIdioma idioma) {
		this.nome = idioma.getNome();
	}
	
	public void translate(Idioma idioma) {
		
		if(idioma == null) {
			return;
		}
		
		if(this.idiomas != null && !this.idiomas.isEmpty()) {
			Iterator<CategoriaIdioma> it = this.idiomas.iterator();
			while(it.hasNext()) {
				CategoriaIdioma ci = it.next();
				if(ci.getIdioma().getId().equals(idioma.getId())) {
					this.nome = ci.getNome();
					return;
				}
			}
		}
	}

}
