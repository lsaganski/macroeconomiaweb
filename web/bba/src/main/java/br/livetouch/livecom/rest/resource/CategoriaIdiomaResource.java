package br.livetouch.livecom.rest.resource;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.CategoriaIdioma;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.vo.CategoriaIdiomaVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/categoriaIdioma")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class CategoriaIdiomaResource extends MainResource {
	protected static final Logger log = Log.getLogger(CategoriaIdiomaResource.class);
	
	@POST
	public Response createIdioma(CategoriaIdioma categoriaIdioma) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		if(categoriaIdioma == null) {
			return Response.ok(MessageResult.error("Erro ao cadastrar idioma para a categoria")).build();
		}
		
		CategoriaIdioma.isFormValid(categoriaIdioma, false);
		
		categoriaIdiomaService.saveOrUpdate(categoriaIdioma);
		return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", new CategoriaIdiomaVO(categoriaIdioma))).build();
	}

	@POST
	@Path("/categorias")
	public Response createIdioma(List<CategoriaIdioma> categoriaIdiomas) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		if(categoriaIdiomas == null || categoriaIdiomas.isEmpty()) {
			return Response.ok(MessageResult.error("Erro ao cadastrar idioma para a categoria")).build();
		}
		
		CategoriaIdioma ci = categoriaIdiomas.get(0);
		if(ci == null) {
			return Response.ok(MessageResult.error("Erro ao cadastrar idioma para a categoria, categoria não informada")).build();
		}
		
		CategoriaPost categoria = ci.getCategoria();
		if(categoria == null) {
			return Response.ok(MessageResult.error("Erro ao cadastrar idioma para a categoria, categoria não informada")).build();
		}
		
		categoriaPostService.saveOrUpdate(getUserInfo(), categoria);
		
		List<CategoriaIdiomaVO> vos = new ArrayList<CategoriaIdiomaVO>();
		
		for (CategoriaIdioma categoriaIdioma : categoriaIdiomas) {
			categoriaIdioma.setCategoria(categoria);
			categoriaIdiomaService.saveOrUpdate(categoriaIdioma);
			vos.add(new CategoriaIdiomaVO(categoriaIdioma));
		}
			
		return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", vos)).build();
	}
	
	@PUT
	public Response updateIdioma(CategoriaIdioma categoriaIdioma) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		if(categoriaIdioma == null) {
			return Response.ok(MessageResult.error("Erro ao editar idioma para a categoria")).build();
		}
		
		CategoriaIdioma.isFormValid(categoriaIdioma, true);
		
		categoriaIdiomaService.saveOrUpdate(categoriaIdioma);
		return Response.ok(MessageResult.ok("Idioma atualizado com sucesso", new CategoriaIdiomaVO(categoriaIdioma))).build();
	}
	
	@GET
	public Response findAll() {
		
		List<CategoriaIdioma> idiomas = categoriaIdiomaService.findAll();
		List<CategoriaIdiomaVO> vos = CategoriaIdiomaVO.fromList(new HashSet<CategoriaIdioma>(idiomas));
		
		return Response.ok().entity(vos).build();
	}

	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		CategoriaIdioma idioma = categoriaIdiomaService.get(id);
		if(idioma == null) {
			return Response.ok(MessageResult.error("Idioma não encontrado")).build();
		}
		CategoriaIdiomaVO vo = new CategoriaIdiomaVO(idioma);
		return Response.ok().entity(vo).build();
	}
	
	@GET
	@Path("/categoria/{id}")
	public Response findByCategoria(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		CategoriaPost categ = categoriaPostService.get(id);
		if(categ == null) {
			return Response.ok(MessageResult.error("Categoria não encontrada")).build();
		}
		
		Set<CategoriaIdioma> idiomas = categoriaIdiomaService.findByCategoria(categ);
		if(idiomas == null) {
			return Response.ok(MessageResult.error("Idioma não encontrado")).build();
		}
		List<CategoriaIdiomaVO> vos = CategoriaIdiomaVO.fromList(idiomas);
		return Response.ok().entity(vos).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			CategoriaIdioma idioma = categoriaIdiomaService.get(id);
			if(idioma == null) {
				return Response.ok(MessageResult.error("Idioma não encontrado")).build();
			}
			categoriaIdiomaService.delete(idioma);
			return Response.ok().entity(MessageResult.ok("Idioma deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error(e.getMessage())).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Idioma  " + id)).build();
		}
	}
	
}
