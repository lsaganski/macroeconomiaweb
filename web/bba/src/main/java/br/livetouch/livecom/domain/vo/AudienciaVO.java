package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

public class AudienciaVO implements Serializable {

	private static final long serialVersionUID = 1524516347542576158L;
	
	private Long total;
	private Long iteracao;
	private Long restante;
	private Long postId;
	
	public AudienciaVO() {
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getIteracao() {
		return iteracao;
	}

	public void setIteracao(Long iteracao) {
		this.iteracao = iteracao;
	}

	public Long getRestante() {
		return restante;
	}

	public void setRestante(Long restante) {
		this.restante = restante;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}
	
	
}
