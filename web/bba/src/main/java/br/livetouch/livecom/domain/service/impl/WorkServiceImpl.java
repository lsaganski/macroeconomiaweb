package br.livetouch.livecom.domain.service.impl;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.ImportarArquivoResponse;
import br.livetouch.livecom.domain.service.WorkService;
import br.livetouch.livecom.jobs.importacao.ImportacaoArquivoWork;
import br.livetouch.livecom.jobs.importacao.ImportarArquivoFactory;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class WorkServiceImpl implements WorkService {
	protected static final Logger log = Log.getLogger(WorkService.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private ImportarArquivoFactory factoryWork;
	

	@Override
	public ImportarArquivoResponse execute_importarArquivo(File f) throws DomainException, IOException {
		Session session = null;
		try {
			
			session = sessionFactory.openSession();
			ImportacaoArquivoWork work = factoryWork.getImportacaoWork(session, f);
			session.doWork(work);
			ImportarArquivoResponse response = work.getResponse();
			return response;
		} catch (HibernateException e) {
			String msgError = "Erro ao importar arquivo: " + f.getName() +" - " + e.getMessage();
			throw new DomainException(msgError, e);
		} finally {
			if(session != null) {
				session.close();
			}
		}
	}
}
