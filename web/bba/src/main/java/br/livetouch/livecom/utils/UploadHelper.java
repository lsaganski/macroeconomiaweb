package br.livetouch.livecom.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Rotation;
import org.jcodec.api.FrameGrab;
import org.jcodec.api.JCodecException;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.AWTUtil;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.infra.util.Utils;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.BBAWebview;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.service.ParametroService;
import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.domain.vo.UploadResponse;
import br.livetouch.livecom.files.FileManager;
import br.livetouch.livecom.files.FileManagerFactory;
import net.coobird.thumbnailator.Thumbnails;
import net.livetouch.tiger.ddd.DomainException;

/**
 * @author Ricardo Lecheta
 * 
 */
public class UploadHelper {
	/**
	 * old
	 */
	public static final int _584 = 584;

	public static final Logger log = Log.getLogger("livecom_upload");

	/**
	 * 245 - > 2 imagens no mural 584 - 1 imagem no mural 960 - full screen
	 */
	static int thumbsSize[] = new int[] { 245, 960 };
	
	public static Arquivo createThumbs(ParametrosMap map, Arquivo arquivo, String dir) throws IOException, FileNotFoundException, DomainMessageException {
		return createThumbs(map, arquivo, null, dir, new Date().getTime());
	}

	public static Arquivo createThumbs(ParametrosMap map, Arquivo arquivo, UploadResponse info, String dir, long timestamp) throws IOException, FileNotFoundException, DomainMessageException {

		String url = arquivo.getUrl();
		String nome = arquivo.getId() + "-" + arquivo.getNome();
		boolean isImage = FileExtensionUtils.isImage(nome);
		boolean isVideo = FileExtensionUtils.isVideo(nome);

		// Thumbs
		List<ThumbVO> thumbsVo = null;
		if (isImage && StringUtils.isNotEmpty(url)) {
			File tempFile = info != null && info.arquivo != null ? info.file : downloadFileToTempDir(url);
			thumbsVo = createThumbs(map, dir, tempFile, nome, timestamp);
		} else if (isVideo && StringUtils.isNotEmpty(url)) {
			File tempFile = info != null && info.arquivo != null && info.file != null ? info.file : downloadFileToTempDir(url);
			
			try {
				File frameVideo = createThumbsVideo(tempFile);
				thumbsVo = createThumbs(map, dir, frameVideo, frameVideo.getName(), timestamp);
			} catch (JCodecException e) {
				e.printStackTrace();
			}
			
		}

		List<ArquivoThumb> list = ArquivoThumb.toList(thumbsVo);
		arquivo.setThumbs(new HashSet<ArquivoThumb>(list));

		if (thumbsVo != null && thumbsVo.size() > 0) {
			ThumbVO thumbVO = thumbsVo.get(0);
			arquivo.setUrlThumb(thumbVO.getUrl());
		} else {
			// nao criou thumb fica o proprio arquivo
			arquivo.setUrlThumb(url);
		}

		return arquivo;
	}

	private static File createThumbsVideo(File file) throws IOException, JCodecException {
		
		String tempDir = System.getProperty("java.io.tmpdir");
		File fileThumb = new File(tempDir, "thumb_video" + "_" + FilenameUtils.getBaseName(file.getName()) + ".jpg");
		
		int frameNumber = 42;
		Picture picture = FrameGrab.getFrameFromFile(file, frameNumber);

		//for JDK (jcodec-javase)
		BufferedImage bufferedImage = AWTUtil.toBufferedImage(picture);
		String name = fileThumb.getName();
		ImageIO.write(bufferedImage, FileExtensionUtils.getExtensao(name), fileThumb);
		
		return fileThumb;
	}

	public static Arquivo createThumbsLambda(ParametrosMap map, Arquivo arquivo, UploadResponse info, String dir, Usuario usuario, long timestamp) throws IOException, FileNotFoundException, DomainMessageException {
		
		String url = arquivo.getUrl();
		String nome = arquivo.getNome();
		boolean isImage = FileExtensionUtils.isImage(nome);
		
		// Thumbs
		List<ThumbVO> thumbsVo = null;
		if (isImage && StringUtils.isNotEmpty(url)) {
			thumbsVo = createThumbsLambda(map, dir, nome, usuario, arquivo, timestamp);
		}
		
		List<ArquivoThumb> list = ArquivoThumb.toList(thumbsVo);
		arquivo.setThumbs(new HashSet<ArquivoThumb>(list));
		
		if (thumbsVo != null && thumbsVo.size() > 0) {
			ThumbVO thumbVO = thumbsVo.get(0);
			arquivo.setUrlThumb(thumbVO.getUrl());
		} else {
			// nao criou thumb fica o proprio arquivo
			arquivo.setUrlThumb(url);
		}
		
		return arquivo;
	}
	
	public static UploadResponse uploadFile(Usuario usuario, Arquivo a, String dir, long timestamp) throws IOException, FileNotFoundException, DomainMessageException {
		
		File file;
		if(a != null) {
			file = downloadFileToTempDir(a.getUrl());
		} else {
			return null;
		}
		
		byte[] bytes = IOUtils.toByteArray(new FileInputStream(file));
		long size = file.length();
		
		return uploadFile(usuario, a.getNome(), bytes, size, dir, timestamp);
		
	}

	public static UploadResponse uploadFile(Usuario usuario, String fileName, byte[] bytes, long fileSize, String dir, long timestamp) throws IOException, FileNotFoundException, DomainMessageException {
		return uploadFile(usuario, fileName, bytes, fileSize, dir, false, timestamp);
	}

	public static UploadResponse uploadFile(Usuario usuario, String fileName, byte[] bytes, long fileSize, String dir, boolean isLambda, long timestamp) throws IOException, FileNotFoundException, DomainMessageException {
		log.debug("> uploadFile ["+usuario.getLogin()+"]: " + fileName + ", size: " + (fileSize/1024) + " kb");
		UploadResponse info = new UploadResponse();

		String nome = FileExtensionUtils.getFileNameFixed(fileName);
		nome = br.infra.util.StringUtils.removeAcentos(nome);
		nome = nome.replaceAll("[^\\w{ASCII}^.]", "");

		String ext = FileExtensionUtils.getExtensao(nome);
		if (StringUtils.isEmpty(ext)) {
			throw new DomainMessageException("Extensão do arquivo não reconhecida [" + nome + "]");
		}

		String contentType = FileExtensionUtils.getMimeType(nome);

		Empresa empresa = usuario.getEmpresa();
		ParametrosMap map = null;
		if(empresa != null) {
			map = ParametrosMap.getInstance(empresa.getId());
		} else {
			map = ParametrosMap.getInstance(1L);
		}
		
		Arquivo a = null;
		if (bytes != null && bytes.length > 0) {

			boolean isImage = FileExtensionUtils.isImage(nome);

			String tempDir = System.getProperty("java.io.tmpdir");
			File tempFile = new File(tempDir, nome);

			// upload s3
			if (dir == null || dir.equals("/") || dir.equals("$dir")) {
				dir = usuario.getChave();
			} else {
				if(isLambda && isImage) {
					dir = dir + "/" + usuario.getChave() + "/" + timestamp;
				} else {
					dir = usuario.getChave() + "/" + dir + "/" + timestamp;
				}
			}

			// Amazon
			String nomeS3 = getFileName(nome);

			FileManager fileManager = FileManagerFactory.getFileManager(map);
			log.debug("uploadFile to S3 ["+usuario.getLogin()+"]: " + fileName + ", size: " + (fileSize/1024) + " kb");
			
			String url = "";
			if(isLambda && isImage) {
				fileManager.putFileLambda(dir, nomeS3, contentType, bytes);
				url = fileManager.getFileLambdaUrl(dir, nomeS3);
			} else {
				fileManager.putFile(dir, nomeS3, contentType, bytes);
				url = fileManager.getFileUrl(dir, nomeS3);
			}
			log.debug("uploadFile S3: " + url);

			// Size
			int width = 0;
			int height = 0;
			if (isImage) {
				IOUtils.write(bytes, new FileOutputStream(tempFile));
				if (tempFile.exists()) {
					info.file = tempFile;
				}
				try {
					BufferedImage bimg = ImageIO.read(tempFile);
					width = bimg.getWidth();
					height = bimg.getHeight();
				} catch (Exception e) {
					log.error("Erro ao ler dimensões da imagem: " + e.getMessage(), e);
				}
			}

			// Arquivo
			a = new Arquivo();
			a.setWidth(width);
			a.setHeight(height);
			a.setNome(nome);
			a.setUsuario(usuario);
			a.setMimeType(contentType);
			a.setLength(fileSize);
			a.setUrl(url);
			a.setUrlThumb(url);
			a.setDataCreate(new Date());
			a.setDataUpdate(new Date());

			info.arquivo = a;
			
			log.debug("< uploadFile: arquivo: " + a.getNome() + ", w/h: " + a.getWidth()+"/"+a.getHeight());
		}

		return info;
	}

	public static UploadResponse uploadFileThumb(Usuario usuario, ArquivoThumb thumb, String fileName, byte[] bytes, long fileSize, String dir, boolean isLambda, long timestamp) throws IOException, FileNotFoundException, DomainMessageException {
		log.debug("> uploadFile ["+usuario.getLogin()+"]: " + fileName + ", size: " + (fileSize/1024) + " kb");
		UploadResponse info = new UploadResponse();
		
		String nome = FileExtensionUtils.getFileNameFixed(fileName);
		nome = br.infra.util.StringUtils.removeAcentos(nome);
		nome = nome.replaceAll("[^\\w{ASCII}^.]", "");
		
		String ext = FileExtensionUtils.getExtensao(nome);
		if (StringUtils.isEmpty(ext)) {
			throw new DomainMessageException("Extensão do arquivo não reconhecida [" + nome + "]");
		}
		
		String contentType = FileExtensionUtils.getMimeType(nome);
		
		Empresa empresa = usuario.getEmpresa();
		ParametrosMap map = null;
		if(empresa != null) {
			map = ParametrosMap.getInstance(empresa.getId());
		} else {
			map = ParametrosMap.getInstance(1L);
		}
		
		Arquivo a = null;
		if (bytes != null && bytes.length > 0) {
			
			boolean isImage = FileExtensionUtils.isImage(nome);
			
			String tempDir = System.getProperty("java.io.tmpdir");
			File tempFile = new File(tempDir, nome);
			
			// upload s3
			if(isLambda && isImage) {
				dir = thumb.getCod() + "/" + dir + "/" + usuario.getChave() + "/" + timestamp;
			} else {
				dir = usuario.getChave() + "/" + dir + "/" + timestamp;
			}
			
			// Amazon
			String nomeS3 = getFileName(nome);
			
			FileManager fileManager = FileManagerFactory.getFileManager(map);
			log.debug("uploadFile to S3 ["+usuario.getLogin()+"]: " + fileName + ", size: " + (fileSize/1024) + " kb");
			
			String url = "";
			if(isLambda && isImage) {
				fileManager.putFileThumb(dir, nomeS3, contentType, bytes);
				url = fileManager.getThumbLambdaURL(dir, nomeS3);
			} else {
				fileManager.putFile(dir, nomeS3, contentType, bytes);
				url = fileManager.getFileUrl(dir, nomeS3);
			}
			log.debug("uploadFile S3: " + url);
			
			if (isImage) {
				IOUtils.write(bytes, new FileOutputStream(tempFile));
				if (tempFile.exists()) {
					info.file = tempFile;
				}
			}
			
			//Thumb
			thumb.setUrl(url);
			
			info.thumb = thumb;
			
			// Arquivo
			a = thumb.getArquivo();
			a.setUrlThumb(url);
			a.setDataUpdate(new Date());
			
			info.arquivo = a;
			
			log.debug("< uploadFile: arquivo: " + a.getNome() + ", w/h: " + a.getWidth()+"/"+a.getHeight());
		}
		
		return info;
	}

	private static String getFileName(String nome) {
		/**
		 * UUID random = UUID.randomUUID();
			String nomeS3 = random + "_" + nome;
		 */
		// Antigamente retornava um nome randomico
		// Vamos apenas salvar na pasta do usuario.
		return nome;
	}

	public static List<ThumbVO> createThumbsFromUrl(ParametrosMap map, String dir, String url, String nome) throws IOException {
		log.debug("Download url [" + url + "] + to file...");
		File file = downloadFileToTempDir(url);
		log.debug("File: " + file);

		List<ThumbVO> thumbs = createThumbs(map, dir, file, nome, new Date().getTime());

		return thumbs;
	}

	public static ThumbVO createThumbFotoUsuario(ParametrosMap map, String dir, String url, String nome) throws IOException {
		log.debug("Creating thumbs usuario...");
		File file = downloadFileToTempDir(url);
		BufferedImage bimg = ImageIO.read(file);
		int width = bimg.getWidth();
		int height = bimg.getHeight();
		int thumbSize = 55;

		ThumbVO t = resizeThumbDown(nome, file, width, height, thumbSize);

		String contentType = FileExtensionUtils.getMimeType(file.getName());

		// Upload thumbs do Amazon
		FileManager fileManager = FileManagerFactory.getFileManager(map);

		if (t != null) {
			log.debug("Upload Thumb w/h: " + t.getWidth() + "/" + t.getHeight() + " ... ");

			File f = t.getFile();
			byte[] bytesThumb = IOUtils.toByteArray(new FileInputStream(f));
			String thumbName = f.getName();
			fileManager.putFile(dir, thumbName, contentType, bytesThumb);
			String urlThumb = fileManager.getFileUrl(dir, thumbName);
			t.setUrl(urlThumb);
			t.setCod(thumbSize);
			t.setWidth(thumbSize);
			t.setHeight(thumbSize);

			log.debug("Upload OK, URL w/h: " + t.getWidth() + "/" + t.getHeight() + " [" + urlThumb + "] ");
			return t;
		}

		return null;
	}

	/**
	 * Cria as thumbs de 245, 584, 960
	 * 
	 * @param bytesThumb
	 * @param file
	 * @param thumbsSize
	 * @return
	 * @throws IOException
	 */
	private static List<ThumbVO> createThumbs(ParametrosMap map, String dir, File file, String nome, long timestamp) throws IOException {
		log.debug("Creating thumbs files...");
		List<ThumbVO> thumbs = new ArrayList<ThumbVO>();

		BufferedImage bimg = ImageIO.read(file);
		int width = bimg.getWidth();
		int height = bimg.getHeight();

		ThumbVO tMural = resizeThumbMural(nome, file, width, height);
		if(tMural != null) {
			thumbs.add(tMural);
		}

		// Create thumbs to file
		for (int thumbSize : thumbsSize) {
			try {
				ThumbVO t = resizeThumbDown(nome, file, width, height, thumbSize);
				if(t != null) {
					thumbs.add(t);
				}

			} catch (Exception e) {
				log.error("Erro ao gerar o thumb: " + e.getMessage(), e);
			}
		}

		String contentType = FileExtensionUtils.getMimeType(file.getName());

		// Upload thumbs do Amazon
		FileManager fileManager = FileManagerFactory.getFileManager(map);
		try {

			dir = dir + "/" + timestamp;
			for (ThumbVO thumb : thumbs) {
				log.debug("Upload Thumb w/h: " + thumb.getWidth() + "/" + thumb.getHeight() + " ... ");

				File f = thumb.getFile();
				byte[] bytesThumb = IOUtils.toByteArray(new FileInputStream(f));
				
				String thumbName = getFileName(f.getName());
				
				fileManager.putFile(dir, thumbName, contentType, bytesThumb);
				String urlThumb = fileManager.getFileUrl(dir, thumbName);
				thumb.setUrl(urlThumb);

				log.debug("Upload OK, URL w/h: " + thumb.getWidth() + "/" + thumb.getHeight() + " [" + urlThumb + "] ");

				// deleta a thumb
				FileUtils.deleteQuietly(f);
			}
		} finally {
			fileManager.close();		
		}

		log.debug("Criou " + thumbs.size() + " thumbs");

		return thumbs;
	}
	
	private static List<ThumbVO> createThumbsLambda(ParametrosMap map, String dir, String nome, Usuario u, Arquivo file, long timestamp) throws IOException {
		log.debug("Creating thumbs files...");
		List<ThumbVO> thumbs = new ArrayList<ThumbVO>();

		// Create thumbs to file
		String bucketChatResize = map.get(Params.FILE_MANAGER_AMAZON_BUCKET_CHAT_RESIZE,"livecom-chat-resize");
		String endpoint = map.get(Params.FILE_MANAGER_AMAZON_SERVER_LAMBDA,"https://s3-us-west-1.amazonaws.com");
		boolean garantirResize = map.getBoolean(Params.AMAZON_LAMBDA_CHAT_RESIZE_GARANTIR_ON,true);

		int tries = map.getInt(Params.FILE_AMAZON_TRIES_GET, 10);
		String baseUrl = endpoint + "/" + bucketChatResize + "/"; 
		for (int thumbSize : thumbsSize) {
			ThumbVO t = null;
			try {
				String url = baseUrl + thumbSize + "/" + dir + "/" + u.getChave() + "/" + timestamp + "/" + nome;
				File tempFile = null;
				
				if(garantirResize) {
					for(int i = 0; tempFile == null && i < tries; i++) {
						boolean exist = checkExists(url);
						if(exist){
							tempFile = downloadFileToTempDir(url);
						} else {
							log.debug("Thumb não gerada ainda");
						}
					}
				}
				
				if(tempFile != null) {
					BufferedImage bimg = ImageIO.read(tempFile);
					int w = bimg.getWidth();
					int h = bimg.getHeight();

					t = new ThumbVO(thumbSize, (int) w, (int) h);
					t.setUrl(url);
					FileUtils.deleteQuietly(tempFile);
				} else {
					int width = file.getWidth();
					int height = file.getHeight();
					
					if (width >= thumbSize || height >= thumbSize) {
						if (width <= height) {
							height = Utils.calculateProportion(thumbSize, width, height);
							width = thumbSize;
						} else {
							width = Utils.calculateProportion(thumbSize, height, width);
							height = thumbSize;
						}
					}
					

					t = new ThumbVO(thumbSize, width, height);
					t.setUrl(url);
				}

				if(t != null) {
					log.debug("Thumb w/h: " + t.getWidth() + "/" + t.getHeight() + " ... ");
					thumbs.add(t);
				}

			} catch (Exception e) {
				log.error("Erro ao gerar o thumb: " + e.getMessage(), e);
			}
		}

		log.debug("Criou " + thumbs.size() + " thumbs");

		return thumbs;
	}

	/**
	 * Cria as thumbs de empresa 150px x 37px
	 * 
	 * @param dir
	 * @param file
	 * @param nome
	 * @return
	 * @throws IOException
	 */
	public static ThumbVO createThumbCustom(ParametrosMap map, Arquivo a, int width, int height) throws IOException {
		log.debug("Creating thumbs files...");
		File file = downloadFileToTempDir(a.getUrl());
		String ext = FileExtensionUtils.getExtensao(file.getName());
		String contentType = FileExtensionUtils.getMimeType(ext);
		
		BufferedImage bimg = ImageIO.read(file);
		bimg = Scalr.resize(bimg, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, width, height, Scalr.OP_ANTIALIAS);
		
		width = bimg.getWidth();
		height = bimg.getHeight();
		ImageIO.write(bimg, ext, file);
		
		ThumbVO t = new ThumbVO(width,  width,  height);
		t.setFile(file);
		if(t != null) {
			log.debug("Upload Thumb w/h: " + t.getWidth() + "/" + t.getHeight() + " ... ");
			// Upload thumbs do Amazon

			File f = t.getFile();
			byte[] bytesThumb = IOUtils.toByteArray(new FileInputStream(f));
			String thumbName = getFileName(f.getName());
			
			FileManager fileManager = FileManagerFactory.getFileManager(map);
			fileManager.putFile("logos", thumbName, contentType, bytesThumb);
			String urlThumb = fileManager.getFileUrl("logos", thumbName);
			
			t.setUrl(urlThumb);

			log.debug("Upload OK, URL w/h: " + t.getWidth() + "/" + t.getHeight() + " [" + urlThumb + "] ");

			// deleta a thumb
			FileUtils.deleteQuietly(f);
		}

		log.debug("Criou thumbs");

		return t;
	}
	
	/**
	 * Faz resize down pelo menor (largura ou altura)
	 * 
	 * @return
	 */
	public static ThumbVO resizeThumbDown(String nome, File file, int width, int height, int thumbSize) {
		
		ThumbVO t = null;
		try {
			String tempDir = System.getProperty("java.io.tmpdir");
			File fileThumb = new File(tempDir, "thumb_" + thumbSize + "_" + nome);

			if (width >= thumbSize || height >= thumbSize) {
				if (width <= height) {

					Thumbnails.of(file).width(thumbSize).toFile(fileThumb);

					BufferedImage bimg = ImageIO.read(fileThumb);
					int w = bimg.getWidth();
					int h = bimg.getHeight();
					
					t = new ThumbVO(thumbSize, (int) w, (int) h);
					t.setFile(fileThumb);
					log.debug("Thumb w/h: " + w + "/" + h + ", file: " + fileThumb);
					// return t.getUrl();
				} else {
					Thumbnails.of(file).height(thumbSize).toFile(fileThumb);

					BufferedImage bimg = ImageIO.read(fileThumb);
					int w = bimg.getWidth();
					int h = bimg.getHeight();

					t = new ThumbVO(thumbSize, (int) w, (int) h);
					t.setFile(fileThumb);
					log.debug("Thumb w/h: " + w + "/" + h + ", file: " + fileThumb);
					// return t.getUrl();
				}
			}

		} catch (Exception e) {
			log.error("Erro ao gerar o thumb: " + e.getMessage(), e);
		}
		return t;
	}

	/**
	 * Faz resize down pelo menor (largura ou altura)
	 * 
	 * @return
	 */
	public static File resizeThumbDown(File file, ArquivoThumb thumb, String nome) {
		
		try {
			
			 int width = thumb.getWidth();
			 int height = thumb.getHeight(); 
			 int thumbSize = thumb.getCod();
			
			String tempDir = System.getProperty("java.io.tmpdir");
			File fileThumb = new File(tempDir, "thumb_" + thumbSize + "_" + nome);
			
			if (width >= thumbSize || height >= thumbSize) {
				if (width <= height) {
					
					Thumbnails.of(file).width(thumbSize).toFile(fileThumb);
					return fileThumb;
				} else {
					Thumbnails.of(file).height(thumbSize).toFile(fileThumb);
					return fileThumb;
				}
			}
			
		} catch (Exception e) {
			log.error("Erro ao gerar o thumb: " + e.getMessage(), e);
		}
		return file;
	}
	
	/**
	 * Faz resize down pelo menor (largura ou altura)
	 * 
	 * @return
	 */
	public static File resize(File file, int width, int height, String nome) {
		
		try {
			
			String tempDir = System.getProperty("java.io.tmpdir");
			File resizeFile = new File(tempDir, nome);
			
			Thumbnails.of(file).width(width).height(height).toFile(resizeFile);
			return resizeFile;
			
		} catch (Exception e) {
			log.error("Erro ao gerar o resize: " + e.getMessage(), e);
		}
		
		return file;
	}

	/**
	 * Rotaciona a imagem pelo angulo informado
	 * 
	 * @return
	 */
	public static File rotate(File file, Rotation angle, String nome) {
		
		try {
			
			String tempDir = System.getProperty("java.io.tmpdir");
			File rotateFile = new File(tempDir, nome);
			
			BufferedImage bimg = ImageIO.read(file);
			
			bimg = Scalr.rotate(bimg, angle);
			
			BufferedImage newImage = new BufferedImage(bimg.getWidth(), bimg.getHeight(), BufferedImage.TYPE_INT_RGB);
			newImage.getGraphics().drawImage(bimg,0,0,null);
			bimg = newImage;
			
			ImageIO.write(bimg, FileExtensionUtils.getExtensao(nome), rotateFile);
			
			return rotateFile;
			
		} catch (Exception e) {
			log.error("Erro ao gerar o resize: " + e.getMessage(), e);
		}
		
		return file;
	}

	/**
	 * O thumb para casos de apenas 1 imagem no post não vai ter tamanho fixo
	 * 
	 * Ele escalar a imagem para baixo ou para cima até chegar em qualquer um dos casos a seguir:
	 * 
	 * A - Largura menor que 580px e altura igual a 325px
	 * B - Largura igual a 580px e altura menor que 325 px.
	 * 
	 * @param nome
	 * @param file
	 * @param width
	 * @param height
	 * @throws IOException 
	 */
	public static ThumbVO resizeThumbMural(String nome, File file, int width, int height) throws IOException {
		ThumbVO t = null;

		String tempDir = System.getProperty("java.io.tmpdir");
		
		// Regra A: Largura menor que 580px e altura igual a 325px
		int h = 325;
		int percent = (int)((float)h * 100 / height);
		int w = (int)((float)percent / 100 * width);
		
		boolean a = w < 580 && h == 325;
		
		String nomeSemExtensao = FileExtensionUtils.getNome(nome);
		String extensao = FileExtensionUtils.getExtensao(nome);

		if(a) {
			File fileThumb = new File(tempDir, "thumb_" +nomeSemExtensao + "_" + w+"_"+h+"."+extensao);
			Thumbnails.of(file).height(h).toFile(fileThumb);
			
			BufferedImage bimg = ImageIO.read(fileThumb);
			w = bimg.getWidth();
			h = bimg.getHeight();

			t = new ThumbVO(h,  w,  h);
			t.setFile(fileThumb);
			log.debug("Thumb w/h: " + w + "/" + h + ", file: " + fileThumb);
			
			return t;
		}

		// Regra B: Largura igual a 580px e altura menor que 325 px.
		w = 580;
		percent = (int)((float)w * 100 / width);
		h = (int)((float)percent / 100 * height);

		boolean b = w == 580 && h < 325;

		if(b) {
			File fileThumb = new File(tempDir, "thumb_" +nomeSemExtensao + "_" + w+"_"+h+"."+extensao);
			Thumbnails.of(file).width( w).toFile(fileThumb);
			
			BufferedImage bimg = ImageIO.read(fileThumb);
			w = bimg.getWidth();
			h = bimg.getHeight();

			t = new ThumbVO(h,  w,  h);
			t.setFile(fileThumb);
			log.debug("Thumb w/h: " + w + "/" + h + ", file: " + fileThumb);

			return t;
		}

		log.error("Erro para gerar a thumb do mural: " + nome + " - " + file);
		log.error("Gerando thumb com 584: " + nome + " - " + file);
		
		t = resizeThumbDown(nome, file, width, height, 584);
		
		log.error("Thumb com 584: " + t);

		return t;
	}
	
	/**
	 * Recebe uma URl como parâmetro, faz download e upload na Amazon.
	 * @param isLambda 
	 */
	public static UploadResponse uploadURL(Usuario usuario, String url, String dirAmazon, boolean isLambda, long timestamp) throws DomainException, IOException {
		if(StringUtils.isEmpty(url)) {
			throw new DomainException("Informe a URL para baixar o arquivo.");
		}
		
		// Download
		File f = downloadFileToTempDir(url);

		if(f == null || !f.exists()) {
			throw new DomainMessageException("Erro ao salvar a URL em arquivo.");
		}

		UploadResponse info = null;

		// Lê os dados do arquivo para Bytes
		String nome = f.getName();
		byte[] bytes = IOUtils.toByteArray(new FileInputStream(f));
		long fileSize = f.length();

		// Upload Amazon
		info = UploadHelper.uploadFile(usuario,nome,bytes,fileSize, dirAmazon, isLambda, timestamp);
		
		return info;
	}
	
	/**
	 * Faz upload do Base64 para a Amazon
	 */
	public static UploadResponse uploadBase64(Usuario usuario, String filename, String base64, String dirAmazon, boolean isLambda, long timestamp) throws DomainException, IOException {

		if(StringUtils.isEmpty(base64) || StringUtils.isEmpty(filename)) {
			throw new DomainException("Informe os dados do arquivo para upload.");
		}
		
//		File fDebug = LivecomUtil.getTmpFile("base64.txt");
//		FileUtils.writeStringToFile(fDebug, base64);
//		System.out.println(fDebug);

		// Decode: Converte o Base64 para array de bytes
		byte[] bytes = Base64.decodeBase64(base64);
//		byte[] bytes = java.util.Base64.getDecoder().decode(base64);
		
		File f = LivecomUtil.getTmpFile(filename);
//		System.out.println(f);
		IOUtils.write(bytes, new FileOutputStream(f));

		if(f == null || !f.exists()) {
			throw new DomainMessageException("Erro ao gravar o arquivo.");
		}

		UploadResponse info = null;

		// Lê os dados do arquivo para Bytes
		bytes = IOUtils.toByteArray(new FileInputStream(f));
		long fileSize = f.length();

		// Upload Amazon
		info = UploadHelper.uploadFile(usuario,filename,bytes,fileSize, dirAmazon, isLambda, timestamp);
		
		return info;
	}
	
	public static File downloadFileToTempDir(String url) throws IOException {
		String[] schemes = { "http", "https" }; // DEFAULT schemes = "http",
												// "https", "ftp"
		UrlValidator urlValidator = new UrlValidator(schemes);
		if (urlValidator.isValid(url)) {
			int idx = url.lastIndexOf("/");
			String nome = url.substring(idx + 1);
			String tempDir = System.getProperty("java.io.tmpdir");
			File fileUrl = new File(tempDir, nome);
			FileUtils.copyURLToFile(new URL(url), fileUrl);
			if (fileUrl != null && fileUrl.exists()) {
				return fileUrl;
			}
		}
		return null;
	}
	
	public static final boolean checkExists(String url) throws IOException {
		
		// Cria a URL
		URL u = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) u.openConnection();
		conn.setReadTimeout(5000);
		conn.setConnectTimeout(5000);

		// Configura a requisição para get
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		conn.setDoOutput(false);
		conn.connect();
		
		if(conn.getResponseCode() == 200) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Faz upload do MultiPart para a Amazon
	 * @param type 
	 */
	public static UploadResponse uploadMultipart(Usuario usuario, FormDataMultiPart multiPart, String dirAmazon) throws DomainException, IOException {

		if(usuario == null) {
			throw new DomainException("Usuario não informado.");
		}

		if(multiPart == null || multiPart.getFields() == null) {
			throw new DomainException("Informe os dados do arquivo para upload.");
		}
		
		if(StringUtils.isEmpty(dirAmazon)) {
			throw new DomainException("Diretório não informado");
		}
		
		ParametrosMap parametrosMap = ParametrosMap.getInstance(usuario.getEmpresa());

		UploadResponse info = null;
		Set<String> keys = multiPart.getFields().keySet();
		
		BBAWebview type = null;
		String idioma = "pt";
		for (String key : keys) {
			if(StringUtils.equalsIgnoreCase(key, "type")) {
				FormDataBodyPart field = multiPart.getField(key);
				type = BBAWebview.valueOf(field.getValue());
				continue;
			}
			
			if(StringUtils.equalsIgnoreCase(key, "idioma")) {
				FormDataBodyPart field = multiPart.getField(key);
				idioma = field.getValue();
				continue;
			}
			
		}
		
		for (String key : keys) {
			
			if(StringUtils.equalsIgnoreCase(key, "type") || StringUtils.equalsIgnoreCase(key, "idioma")) {
				continue;
			}
			
			// Obtem a InputStream para ler o arquivo
			FormDataBodyPart field = multiPart.getField(key);;
			InputStream in = field.getValueAs(InputStream.class);
			try {
				// Salva o arquivo
				String fileName = field.getFormDataContentDisposition().getFileName();
				info = UploadHelper.uploadFile(usuario,fileName, IOUtils.toByteArray(in), 0, dirAmazon, false, new Date().getTime());
				if(info != null) {
					if(type.equals(BBAWebview.sobre)) {
						if(StringUtils.equalsIgnoreCase(idioma, "pt")) {
							parametrosMap.put(Params.BBA_SOBRE_URL_PT, info.arquivo.getUrl());
							info.parametro = Params.BBA_SOBRE_URL_PT;
						} else {
							parametrosMap.put(Params.BBA_SOBRE_URL_EN, info.arquivo.getUrl());
							info.parametro = Params.BBA_SOBRE_URL_EN;
						}
					} else {
						if(StringUtils.equalsIgnoreCase(idioma, "pt")) {
							parametrosMap.put(Params.BBA_TERMOS_URL_PT, info.arquivo.getUrl());
							info.parametro = Params.BBA_TERMOS_URL_PT;
						} else {
							parametrosMap.put(Params.BBA_TERMOS_URL_EN, info.arquivo.getUrl());
							info.parametro = Params.BBA_TERMOS_URL_EN;
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
				throw new DomainMessageException("Erro ao gravar o arquivo.");
			}
		}
		
		
		return info;
	}

}
