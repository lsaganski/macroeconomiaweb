package br.livetouch.livecom.domain.vo;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Chapeu;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostDestaque;
import br.livetouch.livecom.domain.StatusPost;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;

public class PostVO implements Serializable {
	private static final long serialVersionUID = 1L;

	public Long id;

	public String titulo;

	public String tags;
	public List<TagVO> tagsList;

	public String mensagem;
	
	public boolean rascunho;

	private AudienciaVO audiencia;
	public CategoriaVO categoria;
	public List<CategoriaVO> categorias;
	public Long usuarioId;
	public Long usuarioUpdateId;
	public String usuarioNomeUpdate;
	public String usuarioLogin;
	public String usuarioNome;
	public Perfil usuarioPermissao;

	public String urlFotoUsuario;
	public String urlFotoUsuarioThumb;

	public String urlImagem;
	public String urlSite;
	public String urlVideo;

	public String dataStr;
	public Long timestamp;

	public String status;
	public String dataPubStr;
	public int statusPub;
	public String statusPubStr;
	public String dataExpStr;
	public String dataEdit;
	
	public Long timestampExp;
	public Long timestampPub;
	public Long timestampEdit;

	public String favorito = "0";
	public String like = "0";
	public String rate;
	public String rateMedia;

	@XStreamOmitField
	public transient Post post;

	public Integer playlist;

	public List<GrupoVO> grupos;

	public List<FileVO> arquivos;

	public PostDestaqueVO destaque;

	public String checkDestaque;
	public ChapeuVO chapeu;
	public String resumo;

	public String visibilidade;
	
	public String dataPushStr;
	public Long dataPushTimestamp;

	public Long likeCount;
	public Long commentCount;
	public Long postViewCount;
	public Long rateCount;
	
	public Boolean sendPush = true;
	public Boolean sendNotification = true;

	public Boolean likeable = true;
	
	public StatusPostVO statusPost;

	public Boolean prioritario = false;

	/**
	 * Count de comentarios nao lidos deste post, tabela notifications.
	 */
	public Long badgeComentario;

	public List<ComentarioVO> comentarios;

	public Boolean webview = false;
	public Boolean html = false;

	public PostTaskVO postTask;
	
	public List<PostIdiomaVO> idiomas;

	public PostVO() {

	}

	public List<FileVO> getArquivos() {
		return arquivos;
	}

	public void setArquivos(ArrayList<FileVO> arquivos) {
		this.arquivos = arquivos;
	}

	public Post getPost() {
		return post;
	}

	public PostDestaque getPostDestaque() {
		return post.getPostDestaque();
	}

	public Long getId() {
		return id;
	}

	public String getTitulo() {
		return titulo;
	}

	public List<TagVO> getTagsList() {
		return tagsList;
	}

	public String getTags() {
		return tags;
	}
	
	public List<ComentarioVO> getComentarios() {
		return comentarios;
	}

	public String getMensagem() {
		return mensagem;
	}

	public CategoriaVO getCategoria() {
		return categoria;
	}

	public List<GrupoVO> getGrupos() {
		return grupos;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public String getUsuarioLogin() {
		return usuarioLogin;
	}

	public String getUsuarioNome() {
		return usuarioNome;
	}

	public String getUrlFotoUsuario() {
		return urlFotoUsuario;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public String getUrlSite() {
		return urlSite;
	}

	public String getUrlVideo() {
		return urlVideo;
	}

	public String getDataStr() {
		return dataStr;
	}

	public long getTimestamp() {
		if (timestamp == null) {
			timestamp = 0L;
		}
		return timestamp;
	}

	public String getFavorito() {
		return favorito;
	}

	public long getLikeCount() {
		return likeCount != null ? likeCount : 0L;
	}

	public String getLike() {
		return like;
	}

	public Long getCommentCount() {
		return commentCount != null ? commentCount : 0L;
	}

	public String getRate() {
		return rate;
	}

	public String getRateMedia() {
		return rateMedia;
	}

	public void resumir() {
		this.tags = null;
		this.tagsList = null;

		this.mensagem = null;

		this.categoria = null;
		this.usuarioLogin = null;
		this.usuarioNome = null;
		this.usuarioId = null;

		// this.urlFotoUsuario=null;

		this.urlImagem = null;
		this.urlSite = null;
		this.urlVideo = null;

		this.dataStr = null;
		// this. timestamp=null;

		this.favorito = null;
		this.like = null;

		this.post = null;

		this.likeCount = null;
		this.commentCount = null;

		this.grupos = null;

		this.arquivos = null;
	}

	public void resumirMural() {
		// this.tags = null;
		// this.tagsList = null;

		this.urlFotoUsuario = null;

		this.postViewCount = null;
		this.rateCount = null;
		this.rate = null;
		this.rateMedia = null;
		this.playlist = null;
	}

	public void setDestaque(PostDestaque postDestaque) {
		if (postDestaque != null && postDestaque.isOk()) {
			this.destaque = new PostDestaqueVO();
			this.destaque.setDestaque(postDestaque);
		}
	}

	public PostDestaqueVO getDestaque() {
		return destaque;
	}

	public Perfil getUsuarioPermissao() {
		return usuarioPermissao;
	}

	public Long getPostViewCount() {
		return postViewCount != null ? postViewCount : 0L;
	}

	public Long getRateCount() {
		return rateCount != null ? rateCount : 0L;
	}

	public Integer getPlaylist() {
		return playlist;
	}

	public String getStatus() {
		return status;
	}

	public String getDataPubStr() {
		return dataPubStr;
	}

	public Long getTimestampPub() {
		return timestampPub;
	}

	public ChapeuVO getChapeu() {
		return chapeu;
	}

	public String getCheckDestaque() {
		return checkDestaque;
	}

	public String getResumo() {
		return resumo;
	}

	public String getUrlFotoUsuarioThumb() {
		return urlFotoUsuarioThumb;
	}
	
	public String getDataPushStr() {
		return dataPushStr;
	}
	
	public Long getDataPushTimestamp() {
		return dataPushTimestamp;
	}
	
	public Long getTimestampExp() {
		return timestampExp;
	}

	@Override
	public String toString() {
		return "PostVO [id=" + id + ", titulo=" + titulo + ", mensagem=" + mensagem + ", usuarioLogin=" + usuarioLogin + "]";
	}

	public String getDataExpStr() {
		return dataExpStr;
	}

	public void setDataExpStr(String dataExpStr) {
		this.dataExpStr = dataExpStr;
	}

	public AudienciaVO getAudiencia() {
		return audiencia;
	}

	public void setAudiencia(AudienciaVO audiencia) {
		this.audiencia = audiencia;
	}

	public boolean isRascunho() {
		return rascunho;
	}

	public void setRascunho(boolean rascunho) {
		this.rascunho = rascunho;
	}

	public Long getBadgeComentario() {
		return badgeComentario != null ? badgeComentario : 0L;
	}
	
	public int getStatusPub() {
		return statusPub;
	}
	
	public String getStatusPubStr() {
		return statusPubStr;
	}
	
	public void setPost(Post post, Usuario user) {
		this.setPost(post, user, user.getIdioma());
	}

	public void setPost(Post post, Usuario user, Idioma idioma) {
		if(post == null) {
			return;
		}

		idioma = idioma != null ? idioma : user.getIdioma();
		this.post = post;
		
		this.id = post.getId();
		
		this.rascunho = post.isRascunho();
		
		this.tags = post.getTags();
		this.tagsList = new ArrayList<TagVO>();
		Set<Tag> postTags = post.getTagsList();
		if(postTags != null) {
			for (Tag tag : postTags) {
				TagVO t = new TagVO();
				t.setTag(tag);
				this.tagsList.add(t);
			}
		}
		
		// Categ
		this.categorias = new ArrayList<CategoriaVO>();
		CategoriaVO mobile = null;
		
		List<CategoriaPost> categorias = post.getCategorias();
		if (categorias != null) {
			for (CategoriaPost categ : categorias) {
				this.categoria = new CategoriaVO(categ, idioma);
				
				//garantir ordem mobile
				if(mobile == null) {
					mobile = this.categoria;
				}
				
				CategoriaVO.setParents(categ, this.categoria, idioma, this.categoria.nome);
				this.likeable = categ.isLikeable();
				this.categorias.add(this.categoria);
			}
			this.categoria = mobile;
		}
		
		// Status
		StatusPost statusPost = post.getStatusPost();
		if (statusPost != null) {
			this.statusPost = new StatusPostVO(statusPost);
		}
		
		Chapeu c = post.getChapeu();
		if(c != null) {
			this.chapeu = new ChapeuVO(c);
		}
		
		Usuario u = post.getUsuario();
		if (u != null) {
			this.usuarioLogin = u.getLogin();
			this.usuarioNome = StringUtils.isNotEmpty(u.getSobrenome()) ? u.getNome() + " " + u.getSobrenome() : u.getNome();
			this.usuarioId = u.getId();
			this.urlFotoUsuario = u.getUrlFoto();
			this.urlFotoUsuarioThumb = u.getUrlThumb();

			// org.hibernate.Hibernate.initialize(u.getPermissao());
			// this.usuarioPermissao = u.getPermissao();
		}
		
		Usuario userUpdate = post.getUsuarioUpdate();
		if (userUpdate != null) {
			this.usuarioUpdateId  = userUpdate.getId();
			this.usuarioNomeUpdate = userUpdate.getNome();
		}
		
		this.urlImagem = post.getUrlImagem();
		this.urlSite = post.getUrlSite();
		this.urlVideo = post.getUrlVideo();

		if (post.getData() != null) {
			this.dataStr = post.getDataStringHojeOntem();
			this.timestamp = post.getData().getTime();
		}
		if(post.getDataUpdated() != null){
			this.dataEdit = post.getDataStringHojeOntem();
			this.timestampEdit = post.getDataUpdated().getTime();
		}
		if (post.getDataPublicacao() != null) {
			this.dataPubStr = post.getDataPublicacaoStringHojeOntem();
			this.timestampPub = post.getDataPublicacao().getTime();
		}

		if(post.getStatusPublicacao() != null) {
			this.statusPub = post.getStatusPublicacao().ordinal();
			this.statusPubStr = StringUtils.lowerCase(post.getStatusPublicacao().name());
		}

		if(post.getDataPush() != null) {
			this.dataPushStr = post.getDataPushString();
			this.dataPushTimestamp = post.getDataPush().getTime();
		}

		this.checkDestaque = post.isDestaque() ? "1" : "0";
		
		if(post.getVisibilidade() != null) {
			this.visibilidade = post.getVisibilidade().getDesc2();
//			GrupoVO grupoVO = new GrupoVO();
//			grupoVO.setNome("ok");
//			this.grupos.add(grupoVO);
		} else {
			this.visibilidade = "";
		}

		this.status = post.getStatus();
		if (post.getDataExpiracao() != null) {
			this.timestampExp = post.getDataExpiracao().getTime();
			this.setDataExpStr(post.getDataExpiracaoStringHojeOntem());
		}

		this.prioritario = post.isPrioritario();
		this.webview = post.isWebview();
		this.html = post.isHtml();
		
		this.idiomas = PostIdiomaVO.fromList(post.getIdiomas());
		
		post.translate(idioma);
		this.titulo = post.getTitulo();
		this.mensagem = post.getMensagem();
		this.resumo = post.getResumo();
		
	}
	
	
}
