package br.livetouch.livecom.rest.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.LinkConhecido;
import br.livetouch.livecom.domain.service.LinkConhecidoService;
import br.livetouch.livecom.domain.vo.LinkConhecidoVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/linkconhecido")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LinkConhecidoResource extends MainResource {
	protected static final Logger log = Log.getLogger(LinkConhecido.class);

	@Context
	private SecurityContext securityContext;

	@Autowired
	LinkConhecidoService linkService;
	
	
	@GET
	public Response findAll(){
		List<LinkConhecido> links = linkService.findAll();
		List<LinkConhecidoVO> vos = LinkConhecidoVO.fromList(links);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) throws DomainException{
		LinkConhecido link = linkService.get(id);
		linkService.valid(link);
		
		return Response.ok().entity(new LinkConhecidoVO(link)).build();
	}
	
	@POST
	public Response create(LinkConhecido link) throws DomainException {
		linkService.valid(link);
		Response messageResult = existe(link);
		if(messageResult != null){
			return messageResult;
		}
		
		
		linkService.saveOrUpdate(link, getEmpresa());
		LinkConhecidoVO linkVO = new LinkConhecidoVO(link);
		
		
		return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", linkVO)).build();
	}
	
	@PUT
	public Response update (LinkConhecido link) throws DomainException{
		Response messageResult = existe(link);
		if(messageResult != null){
			return messageResult;
		}
		
		LinkConhecido linkOld = linkService.get(link.getId());
		if(linkOld == null){
			throw new DomainException("Link não existe");
		}
		
		linkOld.setNome(link.getNome());
		linkOld.setDominio(link.getDominio());
		
		linkService.saveOrUpdate(linkOld, getEmpresa());
		
		return Response.ok(MessageResult.ok("Link atualizado com sucesso com sucesso", new LinkConhecidoVO(linkOld))).build();
	}
	
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) throws DomainException{
		LinkConhecido link = linkService.get(id);
		if(link == null){
			throw new DomainException("Link não existe");
		}
		
		linkService.delete(link);
		return Response.ok(MessageResult.ok("Link deletado com sucesso")).build();
		
	}
	
	public Response existe(LinkConhecido link) {
		
		if(linkService.findByName(link) != null) {
			log.debug("Ja existe uma link com este nome");
			return Response.ok(MessageResult.error("Ja existe uma link com este nome")).build();
		}
		
		if(linkService.findByDominio(link) != null){
			log.debug("Ja existe uma link com este dominio");
			return Response.ok(MessageResult.error("Ja existe uma link com este dominio")).build();
		}

		return null;
	}
	
	
}
