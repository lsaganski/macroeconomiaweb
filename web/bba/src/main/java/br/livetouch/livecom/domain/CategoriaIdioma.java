package br.livetouch.livecom.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import net.livetouch.tiger.ddd.DomainException;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name="categoria_idioma")
public class CategoriaIdioma extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = -547576754442312371L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "CATEGORIAIDIOMA_SEQ")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categoria_id", nullable = true)
	private CategoriaPost categoria;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idioma_id", nullable = true)
	private Idioma idioma;
	
	private String nome;
	
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CategoriaPost getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaPost categoria) {
		this.categoria = categoria;
	}

	public Idioma getIdioma() {
		return idioma;
	}

	public void setIdioma(Idioma idioma) {
		this.idioma = idioma;
	}
	
	public static void isFormValid(CategoriaIdioma categoriaIdioma, boolean isUpdate) throws DomainException {
		if(categoriaIdioma == null) {
			throw new DomainException("Parametros inválidos");
		}
		
		if(StringUtils.isEmpty(categoriaIdioma.nome)) {
			throw new DomainException("Campo nome não pode ser vazio");
		}
		
		if(isUpdate && categoriaIdioma.getId() == null) {
			throw new DomainException("Idioma não localizado");
		}
		
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
