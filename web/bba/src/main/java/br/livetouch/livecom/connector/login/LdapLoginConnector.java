package br.livetouch.livecom.connector.login;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.exception.SenhaInvalidaException;
import br.livetouch.livecom.ldap.LDAPUser;
import br.livetouch.livecom.ldap.LDAPUtil;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class LdapLoginConnector implements LoginConnector {
	protected static final Logger log = Log.getLogger(LoginConnector.class);

	public boolean validate(Usuario user, String pwd) throws DomainException {
		Empresa empresa = user.getEmpresa();
		ParametrosMap params = ParametrosMap.getInstance(empresa);
		log.debug("LdapLoginConnector: params: " + params);
		LDAPUtil l = new LDAPUtil(params);
		String login = user.getLogin();
		log.debug("LdapLoginConnector : " + login + " / empresa: " + empresa);
		log.debug(">>> LdapLoginConnector: findUser LDAPUtil : " + login);
		LDAPUser r = l.findUser(login);
		if(r == null) {
			log.error(" << LdapLoginConnector user ["+login+"] not found");
			throw new SenhaInvalidaException("Usuário não encontrado.");
		}
		
		String userDN = r.getUserDN();
		log.debug("<< LdapLoginConnector : " + userDN);
		log.debug(">>> LdapLoginConnector: loginByDN " + userDN);
		r = l.loginByDN(userDN,login, pwd);
		if(r == null) {
			log.error(" << LdapLoginConnector user ["+login+"] senha incorreta");
			throw new SenhaInvalidaException("Não foi possível efetuar o login, confirme o usuário e senha.");
		}
		log.debug("<< LdapLoginConnector user "+login+" logado com sucesso");
		return true;
	}

}
