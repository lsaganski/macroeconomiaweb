package br.livetouch.livecom.jobs;

import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdScheduler;
import org.springframework.context.ApplicationContext;

import br.infra.util.Log;

/**
 * Classe utils para executar Jobs
 * 
 * Eles não podem ter triggers, pois a trigger será cancelada.
 * 
 * @author rlech
 *
 */
public class JobUtil {
	private static final Logger log = Log.getLogger(JobUtil.class);
	
	public static void executeJob(final ApplicationContext applicationContext, final Class<? extends Job> cls, final Map<String, Object> params) {
		String threadName = cls.getSimpleName();
		executeJob(applicationContext, cls, threadName, params);
	}
	
	/**
	 * Executa um Job do Quartz
	 * 
	 * @param jobName
	 */
	public static void executeJob(final ApplicationContext applicationContext, final Class<? extends Job> cls,String threadName, final Map<String, Object> params) {
		final String jobClass = cls.getSimpleName();
		log("Iniciando Job: " + jobClass);
		/**
		 * Nao pode ser thread, porque senao os jobs podem ser criados com o mesmo nome na mesma hora.
		 */
		try {
			JobDetail job = (JobDetail) applicationContext.getBean(jobClass);

			Trigger trigger = TriggerBuilder.newTrigger().startNow().build();

			StdScheduler scheduler = (StdScheduler) applicationContext.getBean("SchedulerFactory");

			if (scheduler.checkExists(job.getKey())) {
				try {
					scheduler.deleteJob(job.getKey());
				} catch (Exception e) {
					log.error("scheduler.deleteJob: " + e.getMessage(), e);
				}
			}
			
			// Passa parametros para o job
			JobDataMap jobDataMap = job.getJobDataMap();
			if(params != null) {
				Set<String> keys = params.keySet();
				for (String key : keys) {
					Object value = params.get(key);
					jobDataMap.put(key, value);
				}
			}

			// executa
			scheduler.scheduleJob(job, trigger);
			
			log.debug("JOB [" + jobClass + "] disparado com sucesso");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Erro ao executar o JOB [" + jobClass + "] " + e.getMessage(), e);
		}
	}

	private static void log(String string) {
		log.debug(string);
	}
}
