package br.livetouch.livecom.rest.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/site")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class SiteResource extends MainResource{

	@GET
	@Path("/dominioDisponivel")
	public Response createGet(@QueryParam("dominio") String dominio) throws DomainException {

		Boolean disponivel = empresaService.findDominio(dominio) == null ? true : false;
		
		if(disponivel){
			return Response.ok(MessageResult.ok("Dominio disponivel.")).build();
		} else {
			return Response.ok(MessageResult.error("Dominio já sendo utilizado")).build();
		}
		
	}
	
}
