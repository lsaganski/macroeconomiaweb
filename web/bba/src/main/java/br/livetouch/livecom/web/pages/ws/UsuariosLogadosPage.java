package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class UsuariosLogadosPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));
		
		tMode.setValue("json");

		form.add(new Submit("consultar"));

		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Empresa empresa = getEmpresa();
			List<UserInfoVO> users = Livecom.getInstance().getUsuarios(empresa);

			return users;
		}
		
		return new MensagemResult("NOK","Erro ao fazer busca.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("user", UserInfoVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return false;
	}
}
