package br.livetouch.livecom.web.pages.admin;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.ldap.LDAPUser;
import br.livetouch.livecom.ldap.LDAPUtil;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class LdapLoginPage extends LivecomAdminPage {

	public String msg;
	public Form form = new Form();
	public TextField tLogin;
	public TextField tSenha;
	public String userAttributes;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();

		TextField login = new TextField("email", true);
		login.addStyleClass("input-sm form-control");
		form.add(tLogin = login);
		
		TextField senha = new TextField("senha", true);
		senha.addStyleClass("input-sm form-control");
		form.add(tSenha = senha);
		
		Submit field = new Submit("Buscar", this, "buscar");
		field.setAttribute("class", "btn btn-primary");
		form.add(field);
		
		//setFormTextWidth(form);
	}
	
	public boolean buscar() {
		try {
			if(form.isValid()) {
				ParametrosMap params = getParametrosMap();
				LDAPUtil l = new LDAPUtil(params);
				String login = tLogin.getValue();
				String senha = tSenha.getValue();
				LDAPUser user = l.findUser(login);
				String userDN = user.getUserDN();
				user = l.loginByDN(userDN,login, senha);
				
				if(user == null) {
					msg = "Usuário [" + userDN + "] não encontrado";
				} else {
					userAttributes = user.toString();
				}
			}
		} catch (Exception e) {
			logError(e.getMessage(),e);
			msg = "Erro: " + e.getMessage();
		}
		return false;
	}
	
}
