package br.livetouch.livecom.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;
/**
 * @author Ricardo Lecheta
 * 
 */
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PostDestaque extends net.livetouch.tiger.ddd.Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "POSTDESTAQUE_SEQ")
	private Long id;

	@Column(length=1000)
	private String urlImagem;
	
	@Column(length=1000)
	private String descricaoUrl;
	
	@Column(length=500)
	private String tituloUrl;
	/**
	 * 1 - Imagem 2 - Link 3 - Video
	 */
	private int tipo;

	private String urlSite;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id", nullable = true)
	private Post post;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricaoUrl() {
		return descricaoUrl;
	}

	public void setDescricaoUrl(String descricaoUrl) {
		this.descricaoUrl = descricaoUrl;
	}

	public String getTituloUrl() {
		return tituloUrl;
	}

	public void setTituloUrl(String tituloUrl) {
		this.tituloUrl = tituloUrl;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public String getUrlImagem() {
		return urlImagem;
	}
	
	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}

	public boolean isOk() {
		boolean ok = StringUtils.isNotEmpty(descricaoUrl) || StringUtils.isNotEmpty(tituloUrl) || StringUtils.isNotEmpty(urlImagem);
		return ok;
	}

	@Override
	public String toString() {
		return "PostDestaque [id=" + id + ", urlImagem=" + urlImagem + ", descricaoUrl=" + descricaoUrl + ", tituloUrl=" + tituloUrl + ", tipo=" + tipo + "]";
	}

	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}
	
	public String getUrlSite() {
		return urlSite;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}
}
