package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.List;

public class PushIdsVO {

	private List<Long> ids;

	/**
	 * Login na V1, JSON na V2 {"id":1,"login":"admin@livetouch.com.br","badge":"1"}
	 */
	private List<String> logins;

	public PushIdsVO() {
		super();
	}
	
	public void setIds(List<Long> ids) {
		this.ids = ids;
	}
	
	public void setLogins(List<String> logins) {
		this.logins = logins;
	}
	
	public List<Long> getIds() {
		return ids;
	}
	
	public List<String> getLogins() {
		return logins;
	}
	
	public boolean isEmpty() {
		boolean idsEmpty = ids == null || ids.isEmpty();
		boolean loginsEmpty = logins == null || logins.isEmpty();
		return idsEmpty || loginsEmpty;
	}

	public void add(Long id, String json) {
		if(ids == null) {
			ids = new ArrayList<Long>();
		}
		if(logins == null) {
			logins = new ArrayList<String>();
		}
		ids.add(id);
		logins.add(json);
	}

	public int size() {
		if(isEmpty()) {
			return 0;
		}
		return ids.size();
	}

	@Override
	public String toString() {
		return "PushIdsVO [ids=" + ids + ", logins=" + logins + "]";
	}
}
