package br.livetouch.livecom.rest.resource;

import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


@Path("/authorize")
public class AuthorizeResource {
	
	@Context
	HttpServletRequest req;
	
	@GET
	public Response get(@QueryParam("oauth_token") String oauth_token) throws URISyntaxException { 

		if (oauth_token == null) { 
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		String path = req.getContextPath();
		java.net.URI uri = new 
		java.net.URI(String.format(path + "/oauth/authorize.htm?oauth_token=%s", oauth_token)); 
		return Response.temporaryRedirect(uri).build();
	} 
	
	
}
