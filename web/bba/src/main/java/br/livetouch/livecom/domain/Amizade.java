package br.livetouch.livecom.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.domain.enums.StatusAmizade;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name="amizade")
public class Amizade extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = -547676753741012371L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "AMIZADE_SEQ")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario usuario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "amigo_id", nullable = true)
	private Usuario amigo;
	
	@Enumerated(EnumType.STRING)
	private StatusAmizade statusAmizade;
	
	@Override
	public Long getId() {
		return id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public StatusAmizade getStatusAmizade() {
		return statusAmizade;
	}

	public void setStatusAmizade(StatusAmizade statusAmizade) {
		this.statusAmizade = statusAmizade;
	}
	
	public String getStatusAmizadeString() {
		return statusAmizade != null ? statusAmizade.name() : "";
	}

	public Usuario getAmigo() {
		return amigo;
	}

	public void setAmigo(Usuario amigo) {
		this.amigo = amigo;
	}

}
