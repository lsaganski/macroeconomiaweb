package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

public class ExportarUsuariosFiltro implements Serializable {

	private static final long serialVersionUID = -8800402609370460617L;

	public static final String SESSION_FILTRO_KEY = "exportarUsuariosFiltro";

	// LOGIN;NOME;EMAIL;FONE_FIXO;FONE_CEL;DATA_NASC;CARGO;AREA;DIRETORIA;DIRETORIA_EXECUTIVA;PERMISSAO;GRUPO_ORIGEM;GRUPOS
	private String radio;
	private String[] usuariosString;
	private boolean nome;
	private boolean login;
	private boolean email;
	private boolean foneFixo;
	private boolean foneCel;
	private boolean dataNasc;
	private boolean cargo;
	private boolean area;
	private boolean diretoria;
	// private boolean diretoriaExecutiva;
	private boolean permissao;
	private boolean grupos;

	public boolean isNome() {
		return nome;
	}

	public void setNome(boolean nome) {
		this.nome = nome;
	}

	public boolean isLogin() {
		return login;
	}

	public void setLogin(boolean login) {
		this.login = login;
	}

	public boolean isEmail() {
		return email;
	}

	public void setEmail(boolean email) {
		this.email = email;
	}

	public boolean isFoneFixo() {
		return foneFixo;
	}

	public void setFoneFixo(boolean foneFixo) {
		this.foneFixo = foneFixo;
	}

	public boolean isFoneCel() {
		return foneCel;
	}

	public void setFoneCel(boolean foneCel) {
		this.foneCel = foneCel;
	}

	public boolean isDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(boolean dataNasc) {
		this.dataNasc = dataNasc;
	}

	public boolean isCargo() {
		return cargo;
	}

	public void setCargo(boolean cargo) {
		this.cargo = cargo;
	}

	public boolean isArea() {
		return area;
	}

	public void setArea(boolean area) {
		this.area = area;
	}

	public boolean isDiretoria() {
		return diretoria;
	}

	public void setDiretoria(boolean diretoria) {
		this.diretoria = diretoria;
	}

	public boolean isPermissao() {
		return permissao;
	}

	public void setPermissao(boolean permissao) {
		this.permissao = permissao;
	}

	public boolean isGrupos() {
		return grupos;
	}

	public void setGrupos(boolean grupos) {
		this.grupos = grupos;
	}

	public String getRadio() {
		return radio;
	}

	public void setRadio(String radio) {
		this.radio = radio;
	}

	public String[] getUsuariosString() {
		return usuariosString;
	}

	public void setUsuariosString(String[] usuariosString) {
		this.usuariosString = usuariosString;
	}

}
