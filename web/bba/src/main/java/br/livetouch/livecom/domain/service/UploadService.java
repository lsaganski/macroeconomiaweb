package br.livetouch.livecom.domain.service;

import br.livetouch.livecom.domain.vo.UploadRequest;
import br.livetouch.livecom.domain.vo.UploadResponse;
import net.livetouch.tiger.ddd.DomainException;

public interface UploadService  {

	UploadResponse upload(UploadRequest req) throws DomainException;

}
