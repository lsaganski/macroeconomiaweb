package br.livetouch.livecom.web.pages.ws;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.pushserver.lib.PushService;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class RegisterDevicePage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private UsuarioLoginField tCodUser;
	private TextField tDeviceIMEI;
	private TextField tRegistrationId;
	private TextField tOS;
	private TextField tOSVersion;
	private TextField tAppVersion;
	private TextField tAppVersionCode;
	private TextField tDeviceName;
	private TextField tMode;
	private TextField tTokenVoip;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tCodUser = new UsuarioLoginField("cod_user", true, usuarioService, getEmpresa()));
		form.add(tDeviceIMEI = new TextField("imei"));
		form.add(tRegistrationId = new TextField("device.registration_id", "registration_id"));
		form.add(tOS = new TextField("device.so", "so"));
		form.add(tOSVersion = new TextField("device.so_version", "versao so"));
		form.add(tAppVersion = new TextField("app.version", "versao app"));
		form.add(tAppVersionCode = new TextField("app.version_code", "version code app"));
		form.add(tDeviceName = new TextField("device.name", "device nome"));
		form.add(tTokenVoip = new TextField("token_voip"));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		tCodUser.setFocus(true);

		form.add(new Submit("Registrar"));

	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario user = tCodUser.getEntity();
			
			if(user == null) {
				return new MensagemResult("NOK","Usuario não encontrado");
			}
			
			String imei = tDeviceIMEI.getValue();
			String regId = tRegistrationId.getValue();
			String os = tOS.getValue();
			String osVersion = tOSVersion.getValue();
			String appVersion = tAppVersion.getValue();
			String appVersionCode = tAppVersionCode.getValue();
			String device = tDeviceName.getValue();
			String mode = tMode.getValue();
			String tokenVoip = tTokenVoip.getValue();
			
			String projeto = ParametrosMap.getInstance(getEmpresa()).get(Params.PUSH_SERVER_PROJECT, "");
			
			try {
				PushService s = Livecom.getPushService();
				String json = s.register(projeto, user.getLogin(), imei, regId, os, osVersion, appVersion, appVersionCode, device, mode, tokenVoip, null);
				
				if(StringUtils.isEmpty(json)) {
					return new MensagemResult("NOK","Device não Registrado.");
				}
				
				return new MensagemResult("OK","Device Registrado.");
			}catch(Exception e) {
				return new MensagemResult("NOK","Device não Registrado.");
			}
			
		}
		
		return new MensagemResult("NOK","Device não Registrado.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("post", PostVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
