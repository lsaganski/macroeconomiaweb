package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PushReport;
import br.livetouch.livecom.domain.enums.TipoPush;
import br.livetouch.livecom.domain.repository.PushReportRepository;
import br.livetouch.livecom.domain.vo.PushReportFiltro;
import br.livetouch.spring.StringHibernateRepository;
import net.livetouch.extras.util.DateUtils;

@Repository
public class PushReportRepositoryImpl extends StringHibernateRepository<PushReport> implements PushReportRepository {

	public PushReportRepositoryImpl() {
		super(PushReport.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PushReport> findReportByFilter(PushReportFiltro filtro, Empresa empresa) {
		Date inicial = null;
		Date dFinal = null;
		
		TipoPush tipo = filtro.getTipoPushEnum();
		
		if (StringUtils.isNotEmpty(filtro.getDataInicialString())) {
			inicial = DateUtils.setTimeInicioDia(DateUtils.toDate(filtro.getDataInicialString()));
		}
		if (StringUtils.isNotEmpty(filtro.getDataFinalString())) {
			dFinal = DateUtils.setTimeFimDia(DateUtils.toDate(filtro.getDataFinalString()));
		}
		
		StringBuffer sb = new StringBuffer("from PushReport p where 1=1");
		if (inicial != null) {
			sb.append(" and p.dataEnvio >= :dataIni ");
		}
		if (dFinal != null) {
			sb.append(" and p.dataEnvio <= :dataFim ");
		}
		
		if (filtro.getPostId() != null) {
			sb.append(" and p.post.id = :post");
		}
		
		if (empresa != null) {
			sb.append(" and p.empresa = :empresa");
		}
		
		if (tipo != null) {
			sb.append(" and p.tipoPush = :tipo");
		}
		
		sb.append(" and p.status = :status");
		
		sb.append(" order by p.dataEnvio DESC");
		
		Query q = createQuery(sb.toString());
		
		if (inicial != null) {
			q.setParameter("dataIni", inicial);
		}
		
		if (dFinal != null) {
			q.setParameter("dataFim", dFinal);
		}
		
		if (filtro.getPostId() != null) {
			q.setLong("post", filtro.getPostId());
		}
		
		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		if (tipo != null) {
			q.setParameter("tipo", tipo);
		}
		
		q.setParameter("status", filtro.isEnviada());
		
		if (filtro.getMax() != 0) {
			int firstResult = 0;
			if (filtro.getPage() != 0) {
				firstResult = filtro.getPage() * filtro.getMax();
			}
			q.setFirstResult(firstResult);
			q.setMaxResults(filtro.getMax());
		}

		q.setCacheable(true);
		List<PushReport> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PushReport> findByPost(List<Post> posts) {
		StringBuffer sb = new StringBuffer("FROM PushReport p where p.post in(:list)");
		Query q = createQuery(sb.toString());
		q.setParameterList("list", posts);
		return q.list();
	}

	@Override
	public void delete(List<PushReport> pushReports) {
		StringBuffer sb = new StringBuffer("delete FROM PushReport p where p in(:list)");
		Query q = createQuery(sb.toString());
		q.setParameterList("list", pushReports);
		q.executeUpdate();
	}
	
}