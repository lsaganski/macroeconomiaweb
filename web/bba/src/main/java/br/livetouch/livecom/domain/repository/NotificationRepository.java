package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.vo.AudienciaVO;
import br.livetouch.livecom.domain.vo.NotificationBadge;
import br.livetouch.livecom.domain.vo.NotificationVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioNotificationsVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoFiltro;
import br.livetouch.livecom.domain.vo.UserToPushVO;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public interface NotificationRepository extends net.livetouch.tiger.ddd.repository.Repository<Notification> {

	List<NotificationUsuario> findAll(NotificationSearch search);
	List<NotificationUsuario> findAllAgrupada(NotificationSearch search);
	List<Usuario> findNaoLidasByNotification(Notification n, NotificationSearch search);
	
	List<NotificationUsuario> findSolicitacoes(NotificationSearch search);

	List<Notification> findAllToSendPush();

	List<Long> findIdUsersFrom(Notification parent);

	List<UserToPushVO> findUsersFrom(Notification parent);

	Notification findByPost(Post p, TipoNotificacao tipo);

	int markAllAsRead(Usuario u);


	List<RelatorioNotificationsVO> reportNotifications(RelatorioFiltro filtro) throws DomainException;

	List<RelatorioNotificationsVO> reportNotificationsByType(RelatorioFiltro filtro) throws DomainException;

	List<RelatorioNotificationsVO> reportNotificationsByTypeDetalhes(RelatorioFiltro filtro) throws DomainException;

	int markPostAsRead(Post p, Usuario u);

	int markGrupoAsRead(Grupo g, Usuario u);
	
	int markSolicitacaoAsRead(Grupo g, Usuario u);

	int markSolicitacaoAmizadeAsRead(Usuario u, Usuario amigo);

	int markComentarioAsRead(Long p, Long u);

	void delete(List<Notification> notifications);

	/**
	 * Retorna em objeto com o count de tudo, posts, comentarios, total, etc.
	 * 
	 * @param u
	 * @return
	 */
	NotificationBadge findBadges(Usuario u);

	/**
	 * Retorna em array para otimizar a busca em lote
	 * 
	 * user[0] = Long id
	 * user[1] = String login
	 * user[2] = Long badge
	 */
	List<Object[]> findBadgesToPush(TipoNotificacao tipo, List<Long> ids);

	/**
	 * Retorna em array para otimizar a busca em lote
	 * 
	 * user[0] = Long id
	 * user[1] = String login
	 * user[2] = Long badge = 0
	 */
	List<Object[]> findUsersWithoutBadgesToPush(List<Long> ids);
	
	List<Notification> findViewsByFilter(RelatorioVisualizacaoFiltro filtro);
	
	List<NotificationVO> findAllGroupUserByPost(Usuario usuario, Post post);
	
	AudienciaVO getCountAudienciaByPost(Post post);

	List<Notification> findByGrupo(Grupo g);

	List<NotificationUsuario> findNotificationsFilhas(Notification notification);

	int markAsSent(Long id);

	List<NotificationUsuario> findPostVisivel(Usuario u);

	void deleteFilhas(Notification n);

	Long getTotalBadges(Usuario u);
	
	void markNotificationsAsRead(NotificationUsuario notUser, Post p, TipoNotificacao tipo);

}