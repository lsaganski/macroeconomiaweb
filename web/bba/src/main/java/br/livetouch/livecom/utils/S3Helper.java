package br.livetouch.livecom.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import br.infra.util.Log;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.files.AmazonS3FileManager;

/**
 * @author Ricardo Lecheta
 * 
 */
public class S3Helper {
	
	protected static final Logger log = Log.getLogger(S3Helper.class);
	private String server;
	private String serverLambda;
	private String bucket;
	private String bucketLambda;
	private String bucketResize;
	private String accessKey;
	private String secretKey;
	
	public S3Helper(ParametrosMap map) {
		server = map.get(Params.FILE_MANAGER_AMAZON_SERVER,"https://s3-sa-east-1.amazonaws.com");
		serverLambda = map.get(Params.FILE_MANAGER_AMAZON_SERVER_LAMBDA,"https://s3-us-west-1.amazonaws.com");
		bucket = map.get(Params.FILE_MANAGER_AMAZON_BUCKET,"livecom-livetouch");
		bucketLambda = map.get(Params.FILE_MANAGER_AMAZON_BUCKET_CHAT,"livecom-chat");
		bucketResize = map.get(Params.FILE_MANAGER_AMAZON_BUCKET_RESIZE,"livecom-chat-resize");
		accessKey = map.get(Params.FILE_MANAGER_AMAZON_ACCESSKEY,"AKIAIFLNTEN23RVG67OQ");
		secretKey = map.get(Params.FILE_MANAGER_AMAZON_SECRETKEY,"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");
	}
	
	public static void main(String[] args) throws IOException {
		AmazonS3FileManager s3 = new AmazonS3FileManager(ParametrosMap.getInstance());
		s3.putFile("temp", "nome.txt", "text/html", "Ricardo".getBytes());
	}

	public AmazonS3 connect() throws IOException {
		AWSCredentials credentials = new BasicAWSCredentials(accessKey,secretKey);
		AmazonS3 conn = new AmazonS3Client(credentials);
		return conn;
	}
	
	public String putFoto(String fileName, byte[] bytes) throws IOException {
		return putFile("Fotos",fileName, "image/jpg", bytes);
	}
	
	public void createFolder(String dir) throws IOException {
		putFile(dir, "", null, null);
	}

	public String putFile(String dir, String fileName, String contentType, byte[] bytes) throws IOException {
		AmazonS3 conn = connect();

		String path = StringUtils.isNotEmpty(dir) ? dir+"/"+fileName : fileName;

		ObjectMetadata metadata = new ObjectMetadata();
		if(contentType != null) {
			metadata.setContentType(contentType);
			metadata.setContentLength(bytes != null ? bytes.length : 0);
		}
		
		if(bytes != null) {
			conn.putObject(bucket, path, new ByteArrayInputStream(bytes), metadata);
		} else {
			conn.putObject(bucket, path, new ByteArrayInputStream(new byte[]{}),metadata);
		}
		
		//CopyObjectRequest request =new CopyObjectRequest(bucket, );
		conn.setObjectAcl(bucket, path, CannedAccessControlList.PublicRead);
		
		String url = getFileURL(dir,fileName);
		return url;
	}

	public String putFileChat(String dir, String fileName, String contentType, byte[] bytes) throws IOException {
		AmazonS3 conn = connect();
		
		String path = StringUtils.isNotEmpty(dir) ? dir+"/"+fileName : fileName;
		
		ObjectMetadata metadata = new ObjectMetadata();
		if(contentType != null) {
			metadata.setContentType(contentType);
			metadata.setContentLength(bytes != null ? bytes.length : 0);
		}
		
		if(bytes != null) {
			conn.putObject(bucketLambda, path, new ByteArrayInputStream(bytes), metadata);
		} else {
			conn.putObject(bucketLambda, path, new ByteArrayInputStream(new byte[]{}),metadata);
		}
		
		//CopyObjectRequest request =new CopyObjectRequest(bucket, );
		conn.setObjectAcl(bucketLambda, path, CannedAccessControlList.PublicRead);
		
		String url = getFileURL(dir,fileName);
		return url;
	}

	public String putFileThumb(String dir, String fileName, String contentType, byte[] bytes) throws IOException {
		AmazonS3 conn = connect();
		
		String path = StringUtils.isNotEmpty(dir) ? dir+"/"+fileName : fileName;
		
		ObjectMetadata metadata = new ObjectMetadata();
		if(contentType != null) {
			metadata.setContentType(contentType);
			metadata.setContentLength(bytes != null ? bytes.length : 0);
		}
		
		if(bytes != null) {
			conn.putObject(bucketResize, path, new ByteArrayInputStream(bytes), metadata);
		} else {
			conn.putObject(bucketResize, path, new ByteArrayInputStream(new byte[]{}),metadata);
		}
		
		//CopyObjectRequest request =new CopyObjectRequest(bucket, );
		conn.setObjectAcl(bucketResize, path, CannedAccessControlList.PublicRead);
		
		String url = getFileURL(dir,fileName);
		return url;
	}

	public List<S3ObjectSummary> listObjects(AmazonS3 s3, String bucketName) {
		List<S3ObjectSummary> list = new ArrayList<S3ObjectSummary>();
		ObjectListing objectListing = s3.listObjects(new ListObjectsRequest().withBucketName(bucketName));
		for (S3ObjectSummary o : objectListing.getObjectSummaries()) {
			list.add(o);
		}
		return list;
	}
	
	public ObjectListing listObjectsInFolder(AmazonS3 s3, String bucketName, String folder) {

		ObjectListing objectListing = s3.listObjects(new ListObjectsRequest().withBucketName(bucketName).withPrefix(folder).withDelimiter("/"));
		return objectListing;
	}
	
	public ObjectListing listFilesInFolder(AmazonS3 s3, String bucketName, String folder) {

		ObjectListing objectListing = s3.listObjects(new ListObjectsRequest().withBucketName(bucketName).withPrefix(folder));
		return objectListing;
	}
	
	public String getFileURL(String dir, String file) {
		String url = null;
		if(StringUtils.isNotEmpty(dir)) {
			url = server + "/" + bucket + "/" +dir+"/"+file;
		} else {
			url = server+ "/" + bucket + "/" + file;
		}
		return url;
	}
	
	public String getFileURL(String path) {
		String url = server+ "/" + bucket + "/" +path;
		return url;
	}
	
	public String getDirFromURL(String url) {
		String dir  = url.replaceFirst(server+ "/" + bucket + "/", "");
		String[] split = dir.split("/");
		dir = dir.replaceFirst(split[split.length-1], "");
		dir = removeBarra(dir);
		return dir;
	}
	
	public String getFileNameFromURL(String url) {
		String dir  = url.replaceFirst(server+ "/" + bucket + "/", "");
		String[] split = dir.split("/");
		while (split.length > 1) {
			dir = dir.replaceFirst(split[0], "");
			dir = dir.replaceFirst("/", "");
			split = dir.split("/");
		}
		dir = dir.replaceAll("/", "");
		return dir;
	}
	
	public String removeBarra(String str) {

	  if (str.length() > 0 && str.charAt(str.length()-1)=='/') {
	    str = str.substring(0, str.length()-1);
	  }
	  return str;
	}
	
	public String getFileLambdaURL(String dir, String file) {
		String url = null;
		if(StringUtils.isNotEmpty(dir)) {
			url = serverLambda + "/" + bucketLambda + "/" +dir+"/"+file;
		} else {
			url = serverLambda+ "/" + bucketLambda + "/" + file;
		}
		return url;
	}

	public String getThumbLambdaURL(String dir, String file) {
		String url = null;
		if(StringUtils.isNotEmpty(dir)) {
			url = serverLambda + "/" + bucketResize + "/" +dir+"/"+file;
		} else {
			url = serverLambda+ "/" + bucketResize + "/" + file;
		}
		return url;
	}
	
	public String getServer() {
		return server;
	}
	
	public String getBucket() {
		return bucket;
	}

	public String getBucketChat() {
		return bucketLambda;
	}

	public void setBucketChat(String bucketChat) {
		this.bucketLambda = bucketChat;
	}

	public String getBucketResize() {
		return bucketResize;
	}
	
	public void setBucketResize(String bucketResize) {
		this.bucketResize = bucketResize;
	}

	public String getServerLambda() {
		return serverLambda;
	}

	public void setServerLambda(String serverLambda) {
		this.serverLambda = serverLambda;
	}
}
