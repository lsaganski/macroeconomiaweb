package br.livetouch.livecom.rest.resource;


import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.StatusChatService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.rest.domain.MessageResult;

/**
 * GET
 * http://localhost:8080/livecom/rest/v1/loginFake?id=2&senha=123

{
  "status": "OK",
  "message": "Login OK"
}

 * @author rlech
 *
 */
@Path("/v1/loginFake")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LoginTestResource {

	protected static final Logger log = Log.getLogger(LoginTestResource.class);
	
	@Autowired
	protected StatusChatService statusChatService;
	
	@Autowired
	protected UsuarioService usuarioService;
	
	@GET
	public Response loginGet(@QueryParam("id") Long  id, @QueryParam("senha") String senha) {
		return login(id, senha);
	}

	@POST
	public Response loginPost(@FormParam("id") Long  id, @FormParam("senha") String senha) {
		return login(id, senha);
	}

	private Response login(Long id, String senha) {
		Usuario usuario = usuarioService.get(id);
		if(usuario != null) {
			boolean ok = usuario.validaSenha(senha);
			if(ok) {
				return Response.status(Status.OK).entity(MessageResult.ok("Login OK.")).build();
			} else {
				return Response.status(Status.OK).entity(MessageResult.error("Senha inválida.")).build();
			}
		}
		return Response.status(Status.OK).entity(MessageResult.error("Erro no Login.")).build();
	}
}