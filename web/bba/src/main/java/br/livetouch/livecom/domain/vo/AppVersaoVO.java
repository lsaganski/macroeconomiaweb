package br.livetouch.livecom.domain.vo;

import br.livetouch.livecom.domain.Usuario;

public class AppVersaoVO {

	private Long id;
	private String login;
	private String email;
	private String nome;
	private String appVersion;
	private String appVersionCode;
	private Long count;
	
	public AppVersaoVO(){
		
	}
	
	public AppVersaoVO(Long id, String login, String email, String nome, String appVersion,
			String appVersionCode, Long count) {
		super();
		this.id = id;
		this.login = login;
		this.email = email;
		this.nome = nome;
		this.appVersion = appVersion;
		this.appVersionCode = appVersionCode;
		this.count = count;
	}

	public AppVersaoVO(String versao, Long count) {
		super();
		this.appVersion = versao;
		this.count = count;
	}

	public AppVersaoVO(Usuario u) {
		if(u != null) {
			this.setId(u.getId());
			this.setLogin(u.getLogin());
			this.setEmail(u.getEmail());
			this.setNome(u.getNome());
			this.setAppVersion(u.getDeviceAppVersion());
			this.setAppVersionCode(u.getDeviceVersionCode());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getAppVersionCode() {
		return appVersionCode;
	}

	public void setAppVersionCode(String appVersionCode) {
		this.appVersionCode = appVersionCode;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

}


