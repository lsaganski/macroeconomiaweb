package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.StatusChat;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.web.pages.ws.MensagemResult;

public class UsuarioVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public String login;
	public String nome;
	public String email;
	public String empresa;

	public Boolean chatOn;
	public String dataOnlineChat;
	
	public String conector;
	public Date dataLogin;
	public long timestampLogin;
	private String wstoken;

	public String cargo;
	
	public MensagemResult controleVersao;
	
	public String urlFotoUsuario;
	public String urlFotoUsuarioThumb;
	public String urlFotoCapa;
	
	public String deviceRegistrationId;
	public String deviceRegistrationNome;
	
	// 
	public String telefoneFixo;
	public String telefoneCelular;
	public String fixo;
	public String celular;
	public String setor;
	public String unidade;
	public String identificacao;
	public String status;
	public String origem;
	
	// invictus
	public String genero;
	public String distribuidor;
	public String funcao;
	public Long funcaoId;
	public String canal;
	public String formacao;
	public String complemento;
	public String estado;
	public String cidade;
	public String dataNasc;
	public String dataCreated;
	public String dataUltimoLogin;
	public Long timestampOnlineChat;
	
	public List<GrupoVO> grupos;
	public String gruposStr;
	public GrupoVO grupoOrigem;
	private Boolean isBot;
	private String botKey;
	
	public PerfilVO permissao;
	public PermissoesVO permissoes;
	
	public LivecomConfigVO livecomConfig;
	
	
	// Mural
	public ProfileVO profile;

	private NotificationBadge badges;
	public String desc1;
	public String desc2;
	public String statusChat;
	public Boolean perfilAtivo;
	private List<CategoriaVO> menuCategorias;
	
	public Boolean preCadastro;
	private String corHeader;
	
	public String amigo;

	private NotificationChatVO notificacoes;

	private IdiomaVO idioma;
	
	public static List<UsuarioVO> setUsuarios(List<Usuario> usuarios) {
		List<UsuarioVO> list = new ArrayList<UsuarioVO>();
		
		//Set usuarios apenas que me pediram solicitação de amizade
		if(usuarios != null) {
			for (Usuario u : usuarios) {
				UsuarioVO vo = new UsuarioVO();
				vo.setUsuario(u);
				list.add(vo);
			}
		}
		
		return list;
	}

	public void setUsuario(Usuario u) {
		setUsuario(u, null, null);
	}
	
	public void setUsuario(Usuario u, UsuarioService usuarioService, GrupoService grupoService) {
		
		this.id = u.getId();
		this.login = u.getLogin();
		this.empresa = u.getEmpresa().getId() + ": " + u.getEmpresa().getNome();
		this.nome = StringUtils.isNotEmpty(u.getSobrenome()) ? u.getNome() + " " + u.getSobrenome() : u.getNome();
		this.email = u.getEmail();
		this.conector = u.getFlags();

		StatusChat statusChatU = u.getStatusChat();
		if(statusChatU != null) {
			this.statusChat = statusChatU.getNome();
		}
		
		this.botKey = u.getBotKey();
		this.isBot = u.isBot();
		this.cargo = u.getCargo();
		this.telefoneFixo = u.getTelefoneFixo();
		this.telefoneCelular = u.getTelefoneCelular();
		this.fixo = u.getFixo();
		this.celular = u.getCelular();
		this.setor = u.getSetor();
		this.unidade = u.getUnidade();
		this.identificacao = u.getIdentificacao();
		this.status = u.getStatusString();
		
		this.preCadastro = u.isPreCadastro();
		
		this.origem = u.getOrigemCadastro() != null ? u.getOrigemCadastro().name() : "";

		this.urlFotoUsuario = u.getUrlFoto();
		this.urlFotoUsuarioThumb = u.getUrlThumb();
		this.chatOn = Livecom.getInstance().isUsuarioLogadoChat(this.id);
		this.dataOnlineChat = u.getDataOnlineChatString();
		this.timestampOnlineChat = u.getDataOnlineChat();
		
		this.urlFotoCapa = u.getUrlFotoCapa();
		
		
		this.perfilAtivo=u.isPerfilAtivo();

		// invictus
		this.genero= u.getGenero();
		this.distribuidor= u.getDistribuidor();
		if(u.getFuncao() != null) {
			this.funcao= u.getFuncao().getNome();
			this.funcaoId= u.getFuncao().getId();
		}
		if(u.getCanal() != null) {
			this.canal= u.getCanal().getNome();
		}
		if(u.getFormacao() != null) {
			this.formacao= u.getFormacao().getNome();
		}
		if(u.getComplemento() != null) {
			this.complemento = u.getComplemento().getNome();
		}
		if(u.getEstado() != null) {
			this.estado= u.getEstado().toString();
		}
		if(u.getCidade() != null) {
			this.cidade= u.getCidade().getNome();
		}
		this.dataNasc= u.getDataNascString();
		this.dataCreated = u.getDataCreatedString();
		this.dataUltimoLogin = u.getDataLastLoginString();
		
		// Permissao
		if(u.getPermissao() != null) {
			this.permissao = new PerfilVO();
			this.permissao.setPerfil(u.getPermissao());
			this.permissao.setMapPermissoes(u.getPermissao().getPermissoes());
		}
		
		// Idioma
		if(u.getIdioma() != null) {
			this.idioma = new IdiomaVO(u.getIdioma());
		}
		
		// Grupos
		List<Grupo> grupos = null;
		if(usuarioService != null && grupoService != null) {
			grupos = usuarioService.getGrupos(u);
			if(grupos != null) {
				this.grupos = new ArrayList<GrupoVO>();
				for (Grupo g : grupos) {
					GrupoVO vo = grupoService.setGrupo(g);
					this.grupos.add(vo);
				}

				if(this.grupos.size() > 0) {
					this.grupoOrigem = this.grupos.get(0);
				}
			}
		}
		
		initGruposStr();
		
		// Desc
		this.desc1 = u.getDesc1();
//		this.desc2 = u.getDesc2();
	}
	
	public PermissoesVO setPermissoes(Usuario u) {
		PermissoesVO permissoesVO = new PermissoesVO();
		permissoesVO.setMapPermissoes(u.getPermissao().getPermissoes());
		return permissoesVO;
	}

	public static List<UsuarioVO> toList(List<Usuario> list, UsuarioService usuarioService, GrupoService grupoService) {
		List<UsuarioVO> users = new ArrayList<UsuarioVO>();
		if (list != null) {
			for (Usuario u : list) {
				UsuarioVO vo = new UsuarioVO();
				vo.setUsuario(u, usuarioService, grupoService);
				users.add(vo);
			}
		}
		return users;
	}
	
//	public static List<UsuarioVO> admins(List<UsuarioVO> list, List<Long> admins) {
//		if (list != null) {
//			for (UsuarioVO vo : list) {
//				if(admins.contains(vo.id)) {
//					vo.adminChat = true;
//				}
//			}
//		}
//		return list;
//	}
	
	public void resumir() {
//		this.id = null;
//		this.login=null;
//		this.nome=null;
//		this.cargo=null;
		this.telefoneFixo=null;
		this.telefoneCelular=null;
		this.setor=null;
		this.unidade=null;
		this.identificacao=null;
		this.status=null;
		
//		this.urlFotoUsuario=null;
		
		this.deviceRegistrationId=null;
		this.deviceRegistrationNome=null;
		this.permissao = null;
	}
	
	public void resumirParaIdNomeLoginEmail() {
		resumir();
		
		this.grupoOrigem = null;
		this.grupos = null;
		this.genero = null;
		this.estado = null;
		this.dataNasc = null;
		
//		this.funcao = null;
//		this.cargo = null;
	}
	
	public void resumirBBA() {
		resumir();
		resumirParaIdNomeLoginEmail();
		
		this.nome = null;
		this.funcao = null;
		this.cargo = null;
		this.empresa = null;
		this.chatOn = null;
		this.conector = null;
		this.controleVersao = null;
		this.urlFotoUsuario = null;
		this.urlFotoUsuarioThumb = null;
		this.urlFotoCapa = null;
		this.deviceRegistrationId = null;
		this.deviceRegistrationNome = null;
		this.telefoneFixo = null;
		this.telefoneCelular = null;
		this.fixo = null;
		this.celular = null;
		this.setor = null;
		this.unidade = null;
		this.identificacao = null;
		this.status = null;
		this.origem = null;
		this.genero = null;
		this.distribuidor = null;
		this.funcao = null;
		this.funcaoId = null;
		this.canal = null;
		this.formacao = null;
		this.complemento = null;
		this.estado = null;
		this.cidade = null;
		this.dataNasc = null;
		this.dataCreated = null;
		this.dataUltimoLogin = null;
		this.timestampOnlineChat = null;
		this.grupos = null;
		this.gruposStr = null;
		this.grupoOrigem = null;
		this.isBot = null;
		this.botKey = null;
		this.permissao = null;
		this.permissoes = null;
		this.livecomConfig = null;
		this.profile = null;
		this.badges = null;
		this.desc1 = null;
		this.desc2 = null;
		this.statusChat = null;
		this.menuCategorias = null;
		this.corHeader = null;
		this.amigo = null;
		this.notificacoes = null;
		this.perfilAtivo = null;
		this.preCadastro = null;
	}
	
	private void initGruposStr() {
		StringBuffer sb = new StringBuffer("");
		if(grupos != null) {
			boolean first = true;
			for (GrupoVO g : grupos) {
				if(!first) {
					sb.append(", ");
				}
				sb.append(g.getNome());
				first = false;
			}
		}
		this.gruposStr = sb.toString();
	}
	
	public List<GrupoVO> getGrupos() {
		return grupos;
	}
	public String getGruposStr() {
		return gruposStr;
	}
	public PerfilVO getPermissao() {
		return permissao;
	}
	
	public void setBadges(NotificationBadge badges) {
		this.badges = badges;
	}
	
	public NotificationBadge getBadges() {
		return badges;
	}
	
	public void setDataLogin(Date dataLogin) {
		this.dataLogin = dataLogin;
		this.timestampLogin = dataLogin.getTime();
	}
	
	public String getWstoken() {
		return wstoken;
	}
	
	public void setWstoken(String wstoken) {
		this.wstoken = wstoken;
	}

	public MensagemResult getControleVersao() {
		return controleVersao;
	}

	public void setControleVersao(MensagemResult mensagemResult) {
		this.controleVersao = mensagemResult;
	}

	public List<CategoriaVO> getMenuCategorias() {
		return menuCategorias;
	}


	public void setMenuCategorias(List<CategoriaPost> menuCategoria) {
		this.menuCategorias = CategoriaVO.create(menuCategoria);
	}

	public String getCorHeader() {
		return corHeader;
	}

	public void setCorHeader(String corHeader) {
		this.corHeader = corHeader;
	}

	public Long getId() {
		return id;
	}
	@Override
	public String toString() {
		return id+":"+login;
	}

	public NotificationChatVO getNotificacoes() {
		return notificacoes;
	}

	public void setNotificacoes(NotificationChatVO notificacoes) {
		this.notificacoes = notificacoes;
	}

	public Boolean getIsBot() {
		return isBot;
	}

	public void setIsBot(Boolean isBot) {
		this.isBot = isBot;
	}

	public String getBotKey() {
		if(StringUtils.isNotEmpty(botKey)) {
			return botKey.trim();
		}
		return botKey;
	}

	public void setBotKey(String botKey) {
		this.botKey = botKey;
	}

	public IdiomaVO getIdioma() {
		return idioma;
	}

	public void setIdioma(IdiomaVO idioma) {
		this.idioma = idioma;
	}
}
