package br.livetouch.livecom.domain.service;

import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.PerfilPermissao;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

public interface PerfilService extends Service<Perfil> {

	List<Perfil> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Perfil c) throws DomainException;
	
	@Transactional(rollbackFor = Exception.class)
	void saveOrUpdate(PerfilPermissao p);
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Perfil c, Usuario user) throws DomainException;
	
	List<Perfil> findAll(Empresa e);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Perfil c, Empresa e) throws DomainException;

	List<Object[]> findIdNomePerfil();

	Perfil findDefault();

	Perfil findByNome(String nome, Empresa empresa);

	Perfil createPerfil(String perfil, Empresa empresa) throws DomainException;

	Perfil setMenusDefault(Perfil perfil, String nome, Empresa e) throws DomainException;
	
	Perfil findByNomeValid(Perfil perfil, Empresa e);

	void generatePerfis(Empresa e) throws DomainException;

	void removePermissoes(Perfil perfil);

	void savePermissoes(Perfil perfil, Set<PerfilPermissao> permissoes);

	void generatePerfisPermissaoDefault(Perfil p, Set<PerfilPermissao> permissoes) throws DomainException;

}
