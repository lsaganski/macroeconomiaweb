package br.livetouch.livecom.web.pages.admin;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.SqlTable;
import br.livetouch.livecom.domain.LogTransacao;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextArea;

/**
 * Logs das transações de consulta celular.
 * 
 * Admin pode ver todos os logs
 * Usuario celular somente verifica os seus logs.
 * 
 */
@Controller
@Scope("prototype")
public class LogSqlPage extends LivecomAdminPage {	

	private SqlTable table;
	public Form form = new Form();
	private TextArea tSql;
	public boolean exportar;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissoes(true, ParamsPermissao.VIZUALIZAR_RELATORIOS, ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		if(clear) {
			getContext().removeSessionAttribute(LogTransacao.SESSION_FILTRO_KEY);
		}
		
		if (applicationContext != null) {
			DataSource ds = getDataSource();
			
			if(ds != null) {
				table = new SqlTable(ds);
				
				table.setClass("simple");
				/*table.setId("table");*/
				table.setId("logSql");
				addModel("table", table);
			}
		}

		form();
	}

	public void form() {
		tSql = new TextArea("sql", getMessage("sql.label"), 80, 20);
		form.add(tSql);
		
		Submit s = new Submit("consultar", getMessage("consultar.label"), this, "consultar");
		s.setAttribute("class", "botao salvar");
		form.add(s);
		
		if(isRoot() || "rlecheta@livetouch.com.br".equals(getUsuario().getLogin())) {
			s = new Submit("executar", getMessage("executar.label"), this, "executar");
			s.setAttribute("class", "botao salvar");
			form.add(s);
		}
		//form.add(new Submit("consultar", this, "consultar"));
		
		Submit exportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		exportar.setAttribute("class", "botao info");
		form.add(exportar);
		//form.add(new Submit("exportar", this, "exportar"));
	}

	public boolean consultar() {		
		String sql = tSql.getValue();

		try {
			table.executeQuery(sql);
		} catch (SQLException e) {
			form.setError(e.getMessage() + " - "  +e.getClass());
		}
		
		return true;
	}
	
	public boolean executar() {		
		String sql = tSql.getValue();

		try {
			int i = table.execute(sql);
			msg = "Registros alterados: " + i;
		} catch (SQLException e) {
			form.setError(e.getMessage() + " - "  +e.getClass());
		}
		
		return true;
	}
	
	public boolean exportar() {		
		consultar();
		exportar = true;
		return true;
	}
	
	@Override
	public String getTemplate() {
		if(exportar) {
			return getPath();
		}
		return super.getTemplate();
	}
}
