package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Permissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.PerfilPermissaoVO;
import net.livetouch.tiger.ddd.DomainException;

public interface PermissaoService extends Service<Permissao> {

	List<Permissao> findAll(Empresa e);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Permissao c, Empresa e) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Permissao p, Usuario user) throws DomainException;

	List<Permissao> findByNome(String nome, Empresa empresa);

	Permissao findByNomeValid(Permissao permissao, Empresa empresa);

	Permissao findByCodigoValid(Permissao permissao, Empresa empresa);

	Permissao findByCodigo(String codigo, Empresa empresa);

	List<PerfilPermissaoVO> findByPerfil(Perfil perfil);

}