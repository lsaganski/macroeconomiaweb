package br.livetouch.livecom.jobs.info;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.LogSistema;

/**
 * Os logs adicionados aqui serao processados pelo LivecomJob
 * 
 * @author live
 *
 */
public class LogSistemaJob {
	
	protected List<LogSistema> list;
	protected static LogSistemaJob instance = null;
	
	
	public LogSistemaJob() {
		list = new ArrayList<>();
	}
	
	public static LogSistemaJob getInstance() {
		if (instance == null) {
			instance = new LogSistemaJob();
		}		
		return instance;
	}

	public void addLogSistema(LogSistema logSistema) {
		if(br.livetouch.livecom.utils.HostUtil.isLocalhost()) {
			System.err.println("Debug Localhost LogSistemaJob: " + logSistema);
		}
		list.add(logSistema);
	}
	
	public List<LogSistema> getList() {
		return list;
	}
	
	public List<LogSistema> getListAndDelete() {
		ArrayList<LogSistema> logsList = new ArrayList<>(this.list);
		this.list.removeAll(this.list);
		
		return logsList;
	}
	
	
}
