package br.livetouch.livecom.domain.vo;


import java.io.Serializable;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.LogImportacao;
import br.livetouch.livecom.utils.DateUtils;

public class LogImportacaoVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String status;
	private String arquivoNome;
	private String dataIni;
	private String dataFim;
	private String dataDif;
	private String porcentagem;
	private String header;
	private int linhasOk;
	private int linhasNok;
	
	public LogImportacaoVO(LogImportacao logImportacao){
		this.id = logImportacao.getId();
		this.arquivoNome = logImportacao.getArquivoNome();
		this.dataIni = DateUtils.toString(logImportacao.getDataInicio(),DateUtils.DATE_TIME_24h);
		this.dataFim = DateUtils.toString(logImportacao.getDataFim(),DateUtils.DATE_TIME_24h);
		this.linhasOk = logImportacao.getLinhasOk();
		this.linhasNok = logImportacao.getLinhasNok();
		this.porcentagem = ((linhasOk*100)/(linhasOk+linhasNok))+"%";
		this.header = logImportacao.getHeader();
		
		Long sec = DateUtils.getDiffInSeconds(logImportacao.getDataInicio(), logImportacao.getDataFim());
		this.dataDif = Utils.getDurationString(sec);
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getArquivoNome() {
		return arquivoNome;
	}
	public void setArquivoNome(String arquivoNome) {
		this.arquivoNome = arquivoNome;
	}
	public String getDataIni() {
		return dataIni;
	}
	public void setDataIni(String dataIni) {
		this.dataIni = dataIni;
	}
	public String getDataFim() {
		return dataFim;
	}
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	public String getDataDif() {
		return dataDif;
	}
	public void setDataDif(String dataDif) {
		this.dataDif = dataDif;
	}
	public String getPorcentagem() {
		return porcentagem;
	}
	public void setPorcentagem(String porcentagem) {
		this.porcentagem = porcentagem;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public int getLinhasOk() {
		return linhasOk;
	}
	public void setLinhasOk(int linhasOk) {
		this.linhasOk = linhasOk;
	}
	public int getLinhasNok() {
		return linhasNok;
	}
	public void setLinhasNok(int linhasNok) {
		this.linhasNok = linhasNok;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}