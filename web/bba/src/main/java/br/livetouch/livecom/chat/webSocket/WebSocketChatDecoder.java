package br.livetouch.livecom.chat.webSocket;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import br.livetouch.livecom.chatAkka.protocol.RawMessage;
 
public class WebSocketChatDecoder implements Decoder.Text<RawMessage> {
	
	@Override
	public void init(final EndpointConfig config) {
	}
 
	@Override
	public void destroy() {
	}
 
	@Override
	public RawMessage decode(final String json) throws DecodeException {
		RawMessage rawMessage = RawMessage.apply(json);
		return rawMessage;
	}
 
	@Override
	public boolean willDecode(final String s) {
		return true;
	}
}