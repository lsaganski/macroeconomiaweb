package br.livetouch.livecom.domain.repository;

import java.util.List;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostTask;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.repository.Repository;

public interface PostTaskRepository extends Repository<PostTask>{

	List<PostTask> findAllByUser(Usuario u);

	PostTask findByUserPost(Usuario u, Post p);

	List<PostTask> findAllByPost(Post post);

}