package br.livetouch.livecom.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import net.livetouch.tiger.ddd.DomainException;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name="post_idioma")
public class PostIdioma extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = -547576754442312371L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "POSTIDIOMA_SEQ")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id", nullable = true)
	private Post post;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idioma_id", nullable = true)
	private Idioma idioma;
	
	@Column(columnDefinition = "clob")
	private String mensagem;
	
	private String titulo;
	private String resumo;
	
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Idioma getIdioma() {
		return idioma;
	}

	public void setIdioma(Idioma idioma) {
		this.idioma = idioma;
	}
	
	public static void isFormValid(PostIdioma postIdioma, boolean isUpdate) throws DomainException {
		if(postIdioma == null) {
			throw new DomainException("Parametros inválidos");
		}
		
		if(StringUtils.isEmpty(postIdioma.mensagem)) {
			throw new DomainException("Campo conteudo não pode ser vazio");
		}
		
		if(isUpdate && postIdioma.getId() == null) {
			throw new DomainException("Idioma não localizado");
		}
		
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getMensagemFixed() {
		return StringEscapeUtils.escapeEcmaScript(mensagem);
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public String getTituloDesc() {
		if(StringUtils.isNotEmpty(titulo)) {
			return titulo;
		}
		if(StringUtils.isNotEmpty(mensagem)) {
			if(mensagem.length() > 40) {
				return mensagem.substring(0,40)+"...";
			}
			return mensagem;
		}
		return null;
	}

	public String getResumo() {
		return resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

}
