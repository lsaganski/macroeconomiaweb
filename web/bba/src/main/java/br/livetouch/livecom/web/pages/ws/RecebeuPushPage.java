package br.livetouch.livecom.web.pages.ws;



import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.pushserver.lib.PushService;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LongField;

@Controller
@Scope("prototype")
public class RecebeuPushPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private IntegerField tStatus;
	private LongField tPushId;
	private TextField tRegistrationId;
	
	@Override
	public void onInit() {
		super.onInit();
		form.setMethod("post");
		form.add(tPushId = new LongField("pushId","id do push", true));
		form.add(tRegistrationId = new TextField("registration_id","Registration Id", true));
		form.add(tStatus = new IntegerField("status","recebeu=1, leu=2, cancelou=3", true));
		form.add(tMode = new TextField("mode"));
		form.add(new Submit("Enviar"));
		tStatus.setValue("1");
		tMode.setValue("json");
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			String tipo = tStatus.getValue();
			if(!tipo.equals("1") && !tipo.equals("2") && !tipo.equals("3")) { 
				return new MensagemResult("NOK", "Tipo não reconhecido pelo sistema: " + tipo);
			}
			PushService service = Livecom.getPushService();
			ParametrosMap params = ParametrosMap.getInstance(getEmpresaId());
			String projeto = params.get(Params.PUSH_SERVER_PROJECT, "Livecom");
			service.pushNotification(projeto, tMode.getValue(), tStatus.getValue(), tPushId.getValue(), tRegistrationId.getValue());
			return new MensagemResult("OK", "status atualizado");
		}
		return getFormErrorMensagemResult(form);
	}

	@Override
	protected void xstream(XStream x) {
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
	
}