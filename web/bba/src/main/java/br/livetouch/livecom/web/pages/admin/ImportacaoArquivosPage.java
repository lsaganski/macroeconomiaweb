package br.livetouch.livecom.web.pages.admin;

import java.io.File;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;
import br.livetouch.livecom.jobs.importacao.JobImportacaoUtil;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.sf.click.control.FileField;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

/**
 * 
 */
@Controller
@Scope("prototype")
public class ImportacaoArquivosPage extends LivecomAdminPage {
	public Form form = new Form();

	private FileField fileField;

	public String entrada;

	public String count;
	public String countOk;
	public String countError;
	public String countInsert;
	public String countUpdate;
	
	JobInfo jobInfo;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissoes(true, ParamsPermissao.VIZUALIZAR_RELATORIOS, ParamsPermissao.IMPORTACAO)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();

		jobInfo = JobInfo.getInstance(getEmpresaId());
		
		form.add(fileField = new FileField("file", true));

		entrada = ParametrosMap.getInstance(getEmpresaId()).get(Params.PASTAENTRADA);

		Submit tImportar = new Submit("upload", getMessage("upload.label"), this, "upload");
		tImportar.setAttribute("class", "botao adicionar");
		form.add(tImportar);

		if (jobInfo != null && jobInfo.getResponse() != null) {
			count = String.valueOf(JobInfo.getInstance(getEmpresaId()).getResponse().list.size());
			countOk = String.valueOf(JobInfo.getInstance(getEmpresaId()).getResponse().countOk);
			countError = String.valueOf(JobInfo.getInstance(getEmpresaId()).getResponse().countError);
			countInsert = String.valueOf(JobInfo.getInstance(getEmpresaId()).getResponse().countInsert);
			countUpdate = String.valueOf(JobInfo.getInstance(getEmpresaId()).getResponse().countUpdate);
		}

	}

	public boolean upload() {
		if (form.isValid()) {
			
			FileItem fileItem = fileField.getFileItem();
			try {
				
				ParametrosMap map = ParametrosMap.getInstance(getEmpresaId());
				InputStream in = fileItem.getInputStream();
				
				if (in == null) {
					setMessageError(getMessage("msg.arquivo.invalido.error"));
					return false;
				}
				
				String importOn = ParametrosMap.getInstance(getEmpresaId()).get(Params.IMPORTACAO_ARQUIVO_ON, "0");
				if(!StringUtils.equals(importOn, "1")){
					setMessageError("Parametro importacao.arquivo.on está desativado");
					return false;
				}
				
				byte[] bytes = fileItem.get();
				if (StringUtils.isEmpty(entrada)) {
					setMessageError(getMessage("pastaDeEntrada.error"));
					return false;
				}
				
				String[] split = StringUtils.split(StringUtils.trim(fileItem.getName()), ".");
				if(split == null) {
					setMessageError("Não foi possíve fazer o parse do arquivo, formato não identificado");
					return false;
				}
				
				Empresa empresa = getEmpresa();
				if(empresa == null) {
					setMessageError("Empresa não identificada");
					return false;
				}
				String nome = JobImportacaoUtil.normalizeNomeArquivo(split[0]);
				String ext = split[1];
				jobInfo.arquivo = nome;
				
				nome += "." + ext;
				if(JobImportacaoUtil.isImportFile(nome, map)) {
					setMessageError("O arquivo <b>"+ nome + "</b> não é reconhecido pelo sistema");
					return false;
				}
				
				jobInfo.arquivo += "_" + empresa.getId() + "." + ext;
				
				
				File f = new File(StringUtils.trim(entrada) + "/" + jobInfo.arquivo);
				Log.debug("Importando arquivo: " + f);
				
				FileUtils.writeByteArrayToFile(f, bytes);
				
				jobInfo.temArquivo = true;
				setFlashAttribute("msg", getMessage("msg.importacao.arquivo.salvar.sucess"));
				refresh();

				return true;
			} catch (Exception e) {
				logError(e.getMessage(), e);
				return false;
			}
		}
		setMessageError(getMessage("msg.importacao.arquivo.salvar.error"));
		return false;
	}
}
