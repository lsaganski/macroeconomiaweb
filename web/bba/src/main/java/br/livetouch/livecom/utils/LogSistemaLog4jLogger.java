package br.livetouch.livecom.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.spi.LoggingEvent;
import org.springframework.beans.factory.annotation.Autowired;

import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.jobs.info.LogSistemaJob;

public class LogSistemaLog4jLogger extends RollingFileAppender {

	@Autowired
	protected LogService logService;
	
	@Override
	public synchronized void doAppend(LoggingEvent event) {
		super.doAppend(event);
		
		Object msg = event.getMessage();
		if(msg != null && event.getLevel().equals(Level.ERROR)) {
			String errorMessage = msg.toString();
			String stackTrace = getStackTrace(event);
			
			logSistema(errorMessage, stackTrace);
		}
	}

	private void logSistema(String errorMessage, String stackTrace) {
		if(ParametrosMap.getInstance().getBoolean("flag.LogSistemaLog4jLogger.logOn")) {
			System.err.println("LogSistemaLog4jLogger: " + errorMessage);
			if(stackTrace != null) {
				System.err.println("LogSistemaLog4jLogger: " + stackTrace);
			}
		}
		
		String erro = StringUtils.isEmpty(stackTrace) ? errorMessage : stackTrace;
		
		LogSistema log = LogSistema.logError(null, erro);
		LogSistemaJob.getInstance().addLogSistema(log);
	}

	private String getStackTrace(LoggingEvent event) {
		String[] array = event.getThrowableStrRep();
		if(array == null) {
			return null;
		}
		StringBuffer sb = new StringBuffer();
		for (String s : array) {
			sb.append(s).append("\n");
		}
		return sb.toString();
	}
}
