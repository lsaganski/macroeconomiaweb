package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.ConversaNotification;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.BadgeChatVO;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.NotificationChatVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class GetConversaUserPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	
	public ConversaVO conversaVo;
	private UsuarioLoginField tUserFrom;
	private UsuarioLoginField tUserTo;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		form.add(tUserFrom = new UsuarioLoginField("user_from_id", false, usuarioService, getEmpresa()));
		form.add(tUserTo = new UsuarioLoginField("user_to_id", false, usuarioService, getEmpresa()));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));
		
		tUserFrom.setFocus(true);
		tMode.setValue("json");

		form.add(new Submit("Enviar"));

		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario userFrom = tUserFrom.getEntity();
			Usuario userTo = tUserTo.getEntity();

			/**
			 * Busca a conversa ou cria uma nova entre estes dois users
			 */
			MensagemConversa c = mensagemService.getMensagemConversaByUser(userFrom,userTo);
			if(isWsVersion3()) {
				MensagemVO mensagemVO = new MensagemVO();
				mensagemVO.setMensagem(c, userFrom);
				ConversaNotification notifications = conversaNotificationService.findByConversationUser(c.getId(), userFrom.getId());
				mensagemVO.setNotifications(new NotificationChatVO(notifications));
				
				BadgeChatVO b = new BadgeChatVO();
				Long badge = mensagemService.getBadge(c, userFrom);
				b.mensagens = badge;
				mensagemVO.setBadge(b);
				
				Response r = Response.ok("OK");
				r.msg = mensagemVO;
				return r;
			}
			ConversaVO vo = new ConversaVO();
			vo.setConversa(userFrom, c);
			return vo;
		}
		return new MensagemResult("NOK","Erro ao encontrar a conversa.");
	}

	
	@Override
	protected void xstream(XStream x) {
		x.alias("conversa", ConversaVO.class);
		x.alias("msg", MensagemVO.class);
		x.alias("arquivo", FileVO.class);
		super.xstream(x);
	}
}
