package br.livetouch.livecom.web.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.owasp.esapi.ESAPI;

public class XSSRequestWrapper extends HttpServletRequestWrapper { 
	  
    public XSSRequestWrapper(HttpServletRequest servletRequest) {
        super(servletRequest);
    } 
  
    @Override 
    public String[] getParameterValues(String parameter) {
        String[] values = super.getParameterValues(parameter);
  
        if (values == null) {
            return null; 
        } 
  
        int count = values.length;
        String[] encodedValues = new String[count];
        for (int i = 0; i < count; i++) {
            encodedValues[i] = stripXSS(values[i]);
        } 
  
        return encodedValues;
    } 
  
    @Override 
    public String getParameter(String parameter) {
        String value = super.getParameter(parameter);
  
        return stripXSS(value);
    } 
  
    @Override 
    public String getHeader(String name) {
        String value = super.getHeader(name);
        return stripXSS(value);
    } 
    
    private String stripXSS(String value) {
        if (value != null) {
        	// Use the ESAPI library to avoid encoded attacks.
            value = ESAPI.encoder().canonicalize(value);

            // Avoid null characters
            value = value.replaceAll("\0", "");

            // Clean out HTML
            value = Jsoup.clean(value, Whitelist.none());
        } 
        return value;
    } 
} 