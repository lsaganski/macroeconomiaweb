package br.livetouch.livecom.web.pages.cadastro;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;


@Controller
@Scope("prototype")
public class NewsletterCancelarPage extends LivecomLogadoPage {

	public boolean escondeChat = true;
	public Long empresaId;
	public String urlNewsletter;
	public Long usuarioId;
	public Long id;
	
//	@Override
//	public boolean onSecurityCheck() {
//		super.onSecurityCheck();
//			
//		if(hasPermissao(ParamsPermissao.CADASTRAR_NEWSLETTER)) {
//			return true;
//		}
//		
//		setRedirect(MuralPage.class);
//		return onSecurityCheckNotOk();
//	}
	
	@Override
	public void onInit() {
		empresaId = getUserInfo().getEmpresa().getId();
		urlNewsletter = ParametrosMap.getInstance(empresaId).get("url.node.newsletter", "http://localhost:3001/api/newsletter");
		usuarioId =  getUserInfo().getId();
		super.onInit();
	}


	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
