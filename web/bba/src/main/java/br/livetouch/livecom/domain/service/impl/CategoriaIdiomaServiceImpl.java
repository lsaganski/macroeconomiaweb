package br.livetouch.livecom.domain.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.CategoriaIdioma;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.repository.CategoriaIdiomaRepository;
import br.livetouch.livecom.domain.service.CategoriaIdiomaService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class CategoriaIdiomaServiceImpl implements CategoriaIdiomaService {
	@Autowired
	private CategoriaIdiomaRepository rep;

	@Override
	public CategoriaIdioma get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(CategoriaIdioma ci) throws DomainException {
		rep.saveOrUpdate(ci);
	}

	@Override
	public void delete(CategoriaIdioma ci) throws DomainException {
		rep.delete(ci);
	}
	
	@Override
	public List<CategoriaIdioma> findAll() {
		return rep.findAll();
	}

	@Override
	public Set<CategoriaIdioma> findByCategoria(CategoriaPost c) {
		return rep.findByCategoria(c);
	}

}
