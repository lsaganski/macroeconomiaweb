package br.livetouch.livecom.web.pages.training;

import java.util.List;

import net.sf.click.control.Field;
import net.sf.click.control.Form;

public class TrainingUtil {

	@SuppressWarnings("unchecked")
	public static void setTrainingStyle(Form form) {
		List<Field> fields = form.getFieldList();
		List<Field> buttonList = form.getButtonList();

		for (Field field : fields) {
			field.setAttribute("class", "input required");
		}

		for (Field field : buttonList) {
			field.setAttribute("class", "botaoSombra direita comMargin");
		}		
	}

	

}
