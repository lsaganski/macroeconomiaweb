package br.livetouch.livecom.web.pages.admin;

import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboSelectPalestrante;
import br.livetouch.livecom.domain.Agenda;
import br.livetouch.livecom.domain.Evento;
import net.livetouch.click.control.IdField;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Option;
import net.sf.click.control.Submit;
import net.sf.click.control.TextArea;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.DateField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class AgendaPage extends LivecomAdminPage {

	public Form form = new Form();
	public String msgErro;
	public Long id;
	public ComboSelectPalestrante comboPalestrante;

	public int page;

	public Agenda agenda;
	public Evento evento;
	public String eventoId; 

	@Override
	public void onInit() {
		super.onInit();

		form();

		eventoId = StringUtils.isEmpty(getParam("evento")) ? getParam("evento") : getContext().getSessionAttribute("eventoId").toString();
		if(eventoId != null && StringUtils.isNotEmpty(eventoId)) {
			evento = eventoService.get(Long.valueOf(eventoId));
			getContext().setSessionAttribute("eventoId", evento.getId());
		}
		
		id = id == null ? (Long) getContext().getSessionAttribute("id") : id;
		
		if (id != null) {
			agenda = agendaService.get(id);
		} else {
			agenda = new Agenda();
		}
		form.copyFrom(agenda);
		
	}

	public void form() {
		form.add(new IdField());

		TextField t = null;

		t = new TextField("titulo", getMessage("titulo.label"), true);
		t.setAttribute("class", "input");
		form.add(t);
		TextArea descricao = new TextArea("descricao", getMessage("descricao.label"), true);
		descricao.setAttribute("class", "input");
		descricao.setRows(15);
		form.add(descricao);

		DateField dataInicio = new DateField("dataInicio", getMessage("dataInicio.label"), true);
		dataInicio.setAttribute("class", "input");
		dataInicio.setAttribute("id", "form_dataPublicacao");
		dataInicio.setFormatPattern("dd/MM/yyyy");
		form.add(dataInicio);
		
		DateField dataFim = new DateField("dataFim", getMessage("dataFim.label"),  true);
		dataFim.setAttribute("class", "input");
		dataFim.setAttribute("id", "form_dataPublicacao");
		dataFim.setFormatPattern("dd/MM/yyyy");
		form.add(dataFim);
		
		t = new TextField("horaInicio", getMessage("horaInicio.label"), true);
		t.setAttribute("class", "input placeholder inputHorario ativarPlaceholder picker__input");
		t.setAttribute("id", "form_horaPublicacao");
		form.add(t);

		t = new TextField("horaFim", getMessage("dataFim.label"), true);
		t.setAttribute("class", "input placeholder inputHorario ativarPlaceholder picker__input");
		t.setAttribute("id", "form_horaPublicacao");
		form.add(t);

		comboPalestrante = new ComboSelectPalestrante(palestranteService);
		comboPalestrante.add(new Option("", getMessage("selecione.label")));
		comboPalestrante.setValue("");
		comboPalestrante.setRequired(true);
		comboPalestrante.setLabel(getMessage("combo.palestrante"));
		form.add(comboPalestrante);

		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);

		if (id != null) {
			Submit tDeletar = new Submit("deletar", getMessage("deletar.label"), this, "deletar");
			tDeletar.setAttribute("class", "botao btSalvar");
			form.add(tDeletar);
		}
		Submit cancelar = new Submit("cancelar", getMessage("cancelar.label"), this, "cancelar");
		cancelar.setAttribute("class", "botao");
		form.add(cancelar);

	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if(form.isValid()){
		try {
			agenda = id != null ? agendaService.get(id) : new Agenda();
			form.copyTo(agenda);
			
			if(StringUtils.isEmpty(eventoId)) {
				eventoId = getContext().getSessionAttribute("eventoId").toString();
				evento = eventoService.get(Long.valueOf(eventoId));
				getContext().removeSessionAttribute("eventoId");
			}
			agenda.setEvento(evento);

			agendaService.saveOrUpdate(agenda);

			setFlashAttribute("msg", getMessage("msg.agenda.salvar.success", agenda.getTitulo()));
			setRedirect(AgendasPage.class, "evento", String.valueOf(evento.getId()));

			return false;

		} catch (DomainException e) {
			form.setError(e.getMessage());
		} catch (Exception e) {
			logError(e.getMessage(), e);
			form.setError(e.getMessage());
		}
		} else {
			setFlashAttribute("msg", getMessage("campos.obrigatorios.error"));
		}
	
		return true;
	}

	public boolean cancelar() {
		setRedirect(AgendasPage.class);
		return true;
	}

	public boolean deletar() {
		try {
			try {
				agenda = id != null ? agendaService.get(id) : new Agenda();

				agendaService.delete(agenda);

				setFlashAttribute("msg", getMessage("msg.agenda.deletar.success"));
				setRedirect(AgendasPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (ConstraintViolationException e) {
				logError(e.getMessage(), e);
				this.msgErro = getMessage("msg.agenda.possuirelacionamento.error");
			} catch (Exception e) {
				logError(e.getMessage(), e);
				form.setError(e.getMessage());
			}
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(), e);
			this.msgErro = getMessage("msg.agenda.possuirelacionamento.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
