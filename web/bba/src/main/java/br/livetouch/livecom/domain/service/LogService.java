package br.livetouch.livecom.domain.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.LogTransacao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.DeviceVO;
import br.livetouch.livecom.domain.vo.LogSistemaFiltro;
import br.livetouch.livecom.domain.vo.LogTransacaoFiltro;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.tiger.ddd.DomainException;


public interface LogService extends Service<LogSistema> {

	List<LogSistema> findLogs(int page, Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	void delete(LogSistema log);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(LogSistema log, Empresa empresa);

	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRES_NEW)
	void saveOrUpdateNewTransaction(LogSistema log, Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	void delete(String string);

	long getCountByFilter(LogTransacaoFiltro filtro, Usuario usuarioSistema, Empresa empresa) throws DomainException;
	long getCountByFilter(LogSistemaFiltro filtro, Empresa empresa) throws DomainException;
	
	long getLogTransacaoMaxId(Empresa empresa);

	List<LogSistema> findAll(Empresa empresa);

	/**
	 * Find by example
	 * 
	 * @param log
	 * @return
	 */
	public List<LogSistema> findbyFilter(LogSistemaFiltro log, int page,int max, Empresa empresa) throws DomainException;
	public List<LogTransacao> findbyFilter(LogTransacaoFiltro log, Usuario usuarioSistema, int page, int max, Empresa empresa) throws DomainException;

	LogTransacao getLogTransacao(Long id);

	LogSistema getLogSistema(Long id);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(LogTransacao log, Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	void delete(LogTransacao log);

	String exportarLogsSistema(LogSistemaFiltro filtro, int max, Empresa empresa) throws DomainException;

	LogTransacao findByOtp(Usuario usuario, String otp, Empresa empresa);

	HashMap<String, DeviceVO> getRegistrationIds(Empresa empresa);
	
	List<LogTransacao> findAllLogTransacao(Empresa empresa);

	List<RelatorioAcessosVersaoVO> findLogsByVersao(RelatorioFiltro filtro, Empresa empresa) throws DomainException;

	long getCountLogsByVersao(RelatorioFiltro filtro, Empresa empresa) throws DomainException;
	
	List<String> findAllVersoes(Empresa empresa);
	
	String csvAcessos(RelatorioFiltro filtro, Empresa empresa) throws DomainException;
	
	String csvAcessosVersao(RelatorioFiltro filtro, Empresa empresa) throws DomainException;

	String csvLikes(RelatorioFiltro filtro, Empresa empresa) throws DomainException;

	String csvFuncionais(RelatorioFiltro filtro, Empresa empresa) throws DomainException;

	String csvTransacao(LogTransacaoFiltro filtro, Usuario usuario, int page, int max, Empresa empresa) throws DomainException;

	String csvDetalhesAcessoVersao(RelatorioFiltro filtro, Empresa empresa) throws DomainException;
	
}