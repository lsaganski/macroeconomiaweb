package br.livetouch.livecom.web.pages;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.click.Page;

@Controller
@Scope("prototype")
public class TimePage extends Page {

	public Date now = new Date();
	
	@Override
	public String getTemplate() {
		return getPath();
	}
	
	@Override
	public void onInit() {
		super.onInit();
	}
}
