package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.gson.Gson;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.livetouch.livecom.domain.enums.OrigemCadastro;
import br.livetouch.livecom.domain.enums.StatusPagamento;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.enums.TipoSistema;
import br.livetouch.livecom.domain.vo.CamposMap;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.livecom.utils.LivecomUtil;
import net.livetouch.extras.util.CripUtil;
import net.sf.click.extras.control.Menu;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Usuario extends JsonEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8771734275567884350L;

	public static final String SESSION_FILTRO_KEY = "filtro";

	public static final long TEMPO_EXPIRAR_OTP = 60;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "USUARIO_SEQ")
	private Long id;
	
	public static final String FOTO_DEFAULT = "usuario.png";
	public static final String CAPA_DEFAULT = "img/capa.png";

	@Column(nullable = true)
	private String login;
	private String senha;
	
	private String cpf;
	private String cnpj;

	private String nome;
	private String sobrenome;
	private String email;

	private String cargo;
	private String telefoneFixo;

	@Column(length = 5)
	private String dddTelefone;
	@Column(length = 5)
	private String dddCelular;
	private String telefoneCelular;
	private String setor;
	private String unidade;
	private String identificacao;
	private String twitter;

	private String botKey;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "diretoria_id", nullable = true)
	private Diretoria diretoria;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "area_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Area area;
	
	@OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
	@XStreamOmitField
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<GrupoUsuarios> grupos;

	@OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
	@XStreamOmitField
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<PostUsuarios> postUsuarios;

	@OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
	@XStreamOmitField
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<PostTask> postTasks;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "grupo_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Grupo grupo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "responsavel_id", nullable = true)
	private Usuario responsavel;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "permissao_id", nullable = true)
	private Perfil permissao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "status_chat_id", nullable = true)
	private StatusChat statusChat;

	@OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
	private Set<Post> posts;

	private String fotoS3;

	private Boolean ativo;
	private Boolean perfilAtivo;

	@Enumerated(EnumType.STRING)
	private TipoSistema tipoSistema;

	@Enumerated(EnumType.STRING)
	private StatusUsuario status;

	@Enumerated(EnumType.STRING)
	private OrigemCadastro origemCadastro;
	
	@Enumerated(EnumType.STRING)
	private StatusPagamento statusPag;
	
	// invictus
	private String genero;
	private String distribuidor;
	private String estado;
	private String regiao;
	private Date dataNasc;
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "formacao_id", nullable = true)
	private Formacao formacao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "canal_id", nullable = true)
	private Canal canal;

	private String chave;

	@OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
	@XStreamOmitField
	private Set<Arquivo> arquivos;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cidade_id", nullable = true)
	private Cidade cidade;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "complemento_id", nullable = true)
	private Complemento complemento;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcao_id")
	private Funcao funcao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "arquivo_foto_id", nullable = true)
	private Arquivo foto;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "arquivo_foto_capa_id", nullable = true)
	private Arquivo capa;
	
	private Date dataCreated;
	private Date dataUpdated;
	private Date dataFirstLogin;
	private Date dataLastLogin;
	private Date dataLastChat;
	private Date dataAtivado;
	private Date dataUpdatedSenha;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_create_id", nullable = true)
	private Usuario userCreate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_update_id", nullable = true)
	private Usuario userUpdate;
	
	@Column(nullable = true)
	private Boolean canDelete;
	
	@Column(nullable = true)
	private String deviceRegId;

	@Column(nullable = true)
	private String deviceVersionCode;

	@Column(nullable = true)
	private String deviceAppVersion;
	
	@Column(nullable = true,length = 50)
	private String flags;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "expediente_id", nullable = true)
	private Expediente expediente;
	
	private Date horaAbertura;
	private Date horaFechamento;

	private boolean preCadastro = true;
	private boolean primeiroAcesso = false;
	private boolean receivePush = true;
	
	@OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
	private Set<Token> tokens;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idioma_id", nullable = true)
	private Idioma idioma;

//	private boolean excluido;
	
	public Usuario() {
		ativo = false;
		status = StatusUsuario.NOVO;
	}

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public String getLoginNotNull() {
		return StringUtils.isEmpty(login) ? "" : login;
	}

	public void setLogin(String login) {
		if(StringUtils.isNotEmpty(login)) {
			this.login = StringUtils.trim(login);
		}
	}

	public String getSenha() {
		return senha;
	}

	public void setSenhaCript(String senha) {
		if(LivecomUtil.hasSenhaPadrao(getEmpresa())) {
			setSenha(senha);
		} else {
			String cript = CripUtil.criptToMD5(senha);
			setSenha(cript);
		}
	}
	
	public void setSenha(String senha) {
		if(StringUtils.isNotEmpty(senha)) {
			this.senha = senha;
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		email = StringUtils.trim(email);
		this.email = email;
	}

	/**
	 * Admin ROOT principal
	 * 
	 * @return
	 */
	public boolean isAdmin() {
		if(isRoot()) {
			return true;
		}
		boolean b = isLoginAdmin();
		if(b) {
			return b;
		}
		if(permissao != null) {
			boolean admin = permissao.isAdmin();
			return admin;
		}
		return "admin".equalsIgnoreCase(login);
	}
	
	public boolean isAdminEmpresa() {
		if(empresa == null) {
			return false;
		}
		Usuario admin = empresa.getAdmin();
		boolean isAdmin = admin != null && admin.equals(this);
		return isAdmin;
	}

	public boolean isLoginAdmin() {
		if(StringUtils.startsWith(login, "admin@")) {
			return true;
		}
		return false;
	}

	/**
	 * Admin ROOT principal
	 * 
	 * @return
	 */
	public boolean isRoot() {
		if(StringUtils.startsWith(login, "root@")) {
			return true;
		}
		boolean b = isLoginRoot();
		if(b) {
			return b;
		}
		if (permissao != null) {
			return permissao.isRoot();
		}
		boolean a = "root".equalsIgnoreCase(login);
		return a;
	}
	
	public boolean isRootEmpresa() {
		if(empresa != null && empresa.getAdmin() != null) {
			if(empresa.getAdmin().getId() == id) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isLoginRoot() {
		if(StringUtils.startsWith(login, "root@")) {
			return true;
		}
		return false;
	}

	public boolean validaSenha(String pwd) {
		boolean senhaNormalOK = StringUtils.equals(this.senha, pwd);
		boolean senhaCriptOK = StringUtils.equals(this.senha, CripUtil.criptToMD5(pwd));
		boolean ok = senhaNormalOK || senhaCriptOK;

		return ok;
	}

	public String getDesc() {
		return login + " – " + nome;
	}

	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}

	public String getCargo() {
		if (StringUtils.isEmpty(cargo)) {
			return "";
		}
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getTelefoneFixo() {
		return telefoneFixo;
	}

	public void setTelefoneFixo(String telefoneFixo) {
		this.telefoneFixo = telefoneFixo;
	}

	public String getTelefoneCelular() {
		return telefoneCelular;
	}

	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public StatusUsuario getStatus() {
		return status;
	}
	
	public void setStatus(StatusUsuario status) {
		this.status = status;
		if(status != null && status == StatusUsuario.ATIVO) {
			setDataAtivado(new Date());
		}
	}

	public String getUrlFotoUsuario() {
		if(foto != null) {
			return foto.getUrlThumb();
		}
		String url = getFotoS3();
		if (LivecomUtil.isUrl(url)) {
			return url;
		}
		Empresa empresa = getEmpresa();
		ParametrosMap map;
		if(empresa != null) {
			map = ParametrosMap.getInstance(empresa.getId());
		} else {
			map = ParametrosMap.getInstance(1L);
		}
		return LivecomUtil.getUrlFotoUsuario(map, url);
	}
	
	public String getUrlFotoCapa() {
		if(capa != null) {
			return capa.getUrlThumb();
		}
		Empresa empresa = getEmpresa();
		ParametrosMap map = ParametrosMap.getInstance(empresa.getId());
		String url = map.get(Params.LAYOUT_CAPA_MURAL, CAPA_DEFAULT);
		if (LivecomUtil.isUrl(url)) {
			return url;
		}
		return LivecomUtil.getUrlFotoCapa(map, url);
	}
	

	public String getUrlFoto() {
		return getUrlFotoUsuario();
	}

	public void setFotoS3(String fotoS3) {
		this.fotoS3 = fotoS3;
	}

	public String getFotoS3() {
		if(StringUtils.isEmpty(fotoS3)) {
			if(getFoto() != null) {
				String url = getFoto().getUrl();
				if(StringUtils.isNotEmpty(url)) {
					return url;
				}
			}
			
			Empresa empresa = getEmpresa();
			ParametrosMap map;
			if(empresa != null) {
				map = ParametrosMap.getInstance(empresa.getId());
			} else {
				map = ParametrosMap.getInstance(1L);
			}
			
			return map.get(Params.FOTO_USUARIO_DEFAULT, FOTO_DEFAULT);
		}
		return fotoS3;
	}

	public Boolean getAtivo() {
		if(status != null) {
			boolean b = StatusUsuario.ATIVO.equals(status);
			return b;
		}
		if (ativo == null) {
			return false;
		}
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public boolean validarCadastroAtivo(String build) {
		boolean ok = true;	

		CamposMap instance = CamposMap.getInstance(this);
		
		if(instance.isCampoObrigatorioCadastroAdmin("email")) {
			ok &= StringUtils.isNotEmpty(email);
			if(!ok) {
				throw new RuntimeException("Informe o e-mail");
			}
		}

		return ok;
	}

	public Set<GrupoUsuarios> getGrupos() {
		return grupos;
	}

	public void setGrupos(Set<GrupoUsuarios> grupos) {
		this.grupos = grupos;
	}
	
	public Grupo getGrupo() {
		return grupo;
	}
	
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	@SuppressWarnings("unchecked")
	public boolean containsRoles(Menu menu, List<String> roles) {
		if (roles == null || roles.isEmpty()) {
			return true;
		}
		if (isAdmin()) {
			if (roles.contains("usuario")) {
				return false;
			}
			return true;
		}

		List<Menu> children = menu.getChildren();
		if (children != null && children.size() > 0) {
			boolean ok = false;

			// Valida se algum menu filhos aparece
			for (Menu mChild : children) {
				ok |= containsRoles(mChild, mChild.getRoles());
			}

			if (!ok) {
				// Se nao tem filhos ativos, nao pode aparecer
				return false;
			}
			return true;
		}

//		if (grupos != null) {
//			List<Grupo> gruposList = getGruposList();
//			for (Grupo g : gruposList) {
//				if (g != null) {
//					for (String role : roles) {
//
//						if ("*".equals(role)) {
//							return true;
//						}
//
//						if ("usuario".equals(role)) {
//							// retorna true somente se usuraio está ativo
//							boolean ativo = getAtivo();
//							return ativo;
//						}
//					}
//				}
//			}
//		}

		return false;
	}

	public String getGenero() {
		if (StringUtils.isEmpty(genero)) {
			return "M";
		}
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Formacao getFormacao() {
		return formacao;
	}

	public void setFormacao(Formacao formacao) {
		this.formacao = formacao;
	}

	public String getDistribuidor() {
		return distribuidor;
	}

	public void setDistribuidor(String distribuidor) {
		this.distribuidor = distribuidor;
	}

	public Funcao getFuncao() {
		return funcao;
	}
	
	public void setFuncao(Funcao funcao) {
		this.funcao = funcao;
	}
	
	public Complemento getComplemento() {
		return complemento;
	}
	
	public void setComplemento(Complemento complemento) {
		this.complemento = complemento;
	}

	public String getEstado() {
		if (cidade == null) {
			return "SP";
		}
		String s = cidade.getEstado().getNome();
		if(StringUtils.isEmpty(s)) {
			// todo remover esta coluna
			return this.estado;
		}
		return s;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		if(cidade != null) {
			this.cidade = cidade;
		}
	}

	public Date getDataNasc() {
		return dataNasc;
	}

	public String getDataNascString() {
		if(dataNasc == null) {
			return "";
		}
		return net.livetouch.extras.util.DateUtils.toString(dataNasc, "dd/MM/yyyy");
	}

	public String getDataCreatedString() {
		if(dataCreated == null) {
			return "";
		}
		return net.livetouch.extras.util.DateUtils.toString(dataCreated, "dd/MM/yyyy");
	}
	
	public String getDataLastLoginString() {
		if(dataLastLogin == null) {
			return "";
		}
		return net.livetouch.extras.util.DateUtils.toString(dataLastLogin, "dd/MM/yyyy HH:mm");
	}

	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}
	
	public void setDataLastChat(Date dataLastChat) {
		this.dataLastChat = dataLastChat;
	}
	
	public Date getDataLastChat() {
		return dataLastChat;
	}

	public Perfil getPermissao() {
		return permissao;
	}

	public void setPermissao(Perfil perfil) {
		if(perfil != null) {
			this.permissao = perfil;
		}
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public Set<Arquivo> getArquivos() {
		return arquivos;
	}

	public void setArquivos(Set<Arquivo> arquivos) {
		this.arquivos = arquivos;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getDddCelular() {
		return dddCelular;
	}

	public String getDddTelefone() {
		return dddTelefone;
	}

	public void setDddCelular(String dddCelular) {
		this.dddCelular = dddCelular;
	}

	public void setDddTelefone(String dddTelefone) {
		this.dddTelefone = dddTelefone;
	}
	
	public Canal getCanal() {
		return canal;
	}
	
	public void setCanal(Canal canal) {
		this.canal = canal;
	}

	public Arquivo getFoto() {
		return foto;
	}

	public void setFoto(Arquivo foto) {
		this.foto = foto;
	}
	
	public String getUrlThumb() {
		if (getFoto() == null) {
			return getUrlFotoUsuario();
		} else {
			try {
				Set<ArquivoThumb> thumbs = getFoto().getThumbs();
				for (ArquivoThumb arquivoThumb : thumbs) {
					if (arquivoThumb.getCod() == 325) {
						return arquivoThumb.getUrl();
					}
				}
			} catch (Exception e) {
				// FIXME Hibernate: lazy loading estranho
				e.printStackTrace();
			}
			return getUrlFotoUsuario();
		}

	}
	
	public String getStatusString() {
		if(getStatus() != null) {
			String s = getStatus().name();
			return s;
		}
		return "";
	}
	
	public StatusPagamento getStatusPag() {
		return statusPag;
	}
	
	public void setStatusPag(StatusPagamento statusPag) {
		this.statusPag = statusPag;
	}

	public String getStatusPagString() {
		if(getStatusPag() != null) {
			String s = getStatusPag().name();
			return s;
		}
		return "";
	}
	
	public boolean isValidNomeSobrenome() {
		if(StringUtils.isEmpty(nome)) {
			return false;
		}
		String[] split = StringUtils.split(nome," ");
		boolean ok = split != null && split.length >= 2;
		return ok;
	}

	public boolean isStatusPagOk() {
		if(getStatusPag() != null) {
			boolean ok = StatusPagamento.PAGO.equals(getStatusPag());
			return ok;
		}
		return false;
	}

	public Date getDataCreated() {
		return dataCreated;
	}

	public void setDataCreated(Date dataCreated) {
		this.dataCreated = dataCreated;
	}

	public Date getDataUpdated() {
		return dataUpdated;
	}

	public void setDataUpdated(Date dataUpdated) {
		this.dataUpdated = dataUpdated;
	}

	public Date getDataFirstLogin() {
		return dataFirstLogin;
	}

	public void setDataFirstLogin(Date dataFirstLogin) {
		this.dataFirstLogin = dataFirstLogin;
	}
	
	public Date getDataLastLogin() {
		return dataLastLogin;
	}
	
	public void setDataLastLogin(Date dataLastLogin) {
		this.dataLastLogin = dataLastLogin;
	}

	public Date getDataAtivado() {
		return dataAtivado;
	}

	public void setDataAtivado(Date dataAtivado) {
		this.dataAtivado = dataAtivado;
	}

	public String getCnpj() {
		return cnpj;
	}
	public String getCpf() {
		return cpf;
	}
	public String getCpfCnpj() {
		return cpf != null ? cpf : cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDesc1() {
		if("invictusbook".equals(ParametrosMap.getInstance(1L).getBuildType())) {
			if(canal != null) {
				return canal.getNome();
			}
			return "";
		}
		if("livecom".equals(ParametrosMap.getInstance(1L).getBuildType())){
			if(funcao != null) {
				return funcao.getNome();
	
			}else if(complemento != null){
				return complemento.getNome();
			}
				return nome;
		}
		return "";
	}
	
	public Usuario getUserCreate() {
		return userCreate;
	}

	public void setUserCreate(Usuario userCreate) {
		this.userCreate = userCreate;
	}

	public Usuario getUserUpdate() {
		return userUpdate;
	}

	public void setUserUpdate(Usuario userUpdate) {
		this.userUpdate = userUpdate;
	}

	public String toStringAll() {
		return "Usuario [id=" + id + ", login=" + login + "]";
	}
	
	public TipoSistema getTipoSistema() {
		return tipoSistema;
	}
	
	public void setTipoSistema(TipoSistema tipoSistema) {
		this.tipoSistema = tipoSistema;
	}

	@Override
	public String toString() {
		return toStringIdDesc();
	}
	
	public String toStringIdDesc() {
		return id + ": " + getDesc();
	}

	public Boolean isCanDelete() {
		if(canDelete == null) {
			return true;
		}
		return canDelete;
	}
	
	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}

	public boolean isTipoSistemaImportacao() {
		return tipoSistema != null && tipoSistema.equals(TipoSistema.IMPORTACAO);
	}

	public void setRegistrationId(String deviceRegId) {
		this.deviceRegId = deviceRegId;
	}
	
	public String getRegistrationId() {
		return this.deviceRegId;
	}

	public StatusChat getStatusChat() {
		return statusChat;
	}

	public void setStatusChat(StatusChat statusChat) {
		this.statusChat = statusChat;
	}

	public Diretoria getDiretoria() {
		return diretoria;
	}

	public void setDiretoria(Diretoria diretoria) {
		this.diretoria = diretoria;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Boolean isPerfilAtivo() {
		return perfilAtivo != null ? perfilAtivo : false;
	}

	public void setPerfilAtivo(Boolean perfilAtivo) {
		this.perfilAtivo = perfilAtivo;
	}
	
	public String getFlags() {
		return flags;
	}
	
	public void setFlags(String flags) {
		this.flags = flags;
	}
	
	public static List<Long> getIds(Collection<Usuario> list) {
		List<Long> ids = new ArrayList<Long>();
		if(list == null || list.size() == 0) {
			return ids;
		}
		for (net.livetouch.tiger.ddd.Entity e : list) {
			ids.add(e.getId());
		}
		return ids;
	}

	public boolean isLoginLDAP() {
		return "ldap".equalsIgnoreCase(getFlags());
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public Expediente getExpediente() {
		return expediente;
	}

	public void setExpediente(Expediente expediente) {
		this.expediente = expediente;
	}

	public Date getHoraAbertura() {
		return horaAbertura;
	}

	public void setHoraAbertura(Date horaAbertura) {
		this.horaAbertura = horaAbertura;
	}

	public Date getHoraFechamento() {
		return horaFechamento;
	}

	public void setHoraFechamento(Date horaFechamento) {
		this.horaFechamento = horaFechamento;
	}

	public Boolean isLoginLivecom() {
		if(StringUtils.isEmpty(this.flags) || StringUtils.equalsIgnoreCase(this.flags, "livecom")) {
			return true;
		}
		return false;
	}
	
	public String getFixo() {
		String ddd = StringUtils.isEmpty(this.dddTelefone) ? "" : this.dddTelefone;
		String telefone = StringUtils.isEmpty(this.telefoneFixo) ? "" : this.telefoneFixo;
		return ddd + telefone;
	}
	
	public String getCelular() {
		String ddd = StringUtils.isEmpty(this.dddCelular) ? "": this.dddCelular;
		String telefone = StringUtils.isEmpty(this.telefoneCelular) ? "": this.telefoneCelular;
		return ddd + telefone;
	}
	
	@Override
	public String toJson() {
		UsuarioVO vo = new UsuarioVO();
		vo.setUsuario(this);
		String json = new Gson().toJson(vo);
		return json;
	
	}

	public boolean isFoto(Arquivo a) {
		if(a == null || foto == null) {
			return false;
		}
		boolean isFoto = foto.equals(a);
		return isFoto;
	}

	public OrigemCadastro getOrigemCadastro() {
		return origemCadastro;
	}

	public void setOrigemCadastro(OrigemCadastro origemCadastro) {
		this.origemCadastro = origemCadastro;
	}
	
	public Boolean isStatusNovo() {
		return status != null ? status.equals(StatusUsuario.NOVO) : false;
	}
	
	public Boolean isStatusEnviadoConvite() {
		return status != null ? status.equals(StatusUsuario.ENVIADO_CONVITE) : false;
	}

	public Arquivo getCapa() {
		return capa;
	}

	public void setCapa(Arquivo capa) {
		this.capa = capa;
	}
	
	public Long getDataOnlineChat() {
		if(this.dataLastChat == null) {
			if(this.dataLastLogin != null) {
				return this.dataLastLogin.getTime();
			}
			return null;
		}
		return this.dataLastChat.getTime();
	}

	public boolean isPreCadastro() {
		return preCadastro;
	}

	public void setPreCadastro(boolean preCadastro) {
		this.preCadastro = preCadastro;
	}

	public boolean isAdmin(Grupo g) {
		if(g != null) {
			return g.isAdmin(this);
		}
		return false;
	}

	public String getDeviceVersionCode() {
		return deviceVersionCode;
	}

	public void setDeviceVersionCode(String deviceVersionCode) {
		this.deviceVersionCode = deviceVersionCode;
	}

	public String getDeviceAppVersion() {
		return deviceAppVersion;
	}

	public void setDeviceAppVersion(String deviceAppVersion) {
		this.deviceAppVersion = deviceAppVersion;
	}

	public boolean isReceivePush() {
		return receivePush;
	}

	public void setReceivePush(boolean receivePush) {
		this.receivePush = receivePush;
	}

	public Set<PostUsuarios> getPostUsuarios() {
		return postUsuarios;
	}

	public void setPostUsuarios(Set<PostUsuarios> postUsuarios) {
		this.postUsuarios = postUsuarios;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getRegiao() {
		return regiao;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

	public String getBotKey() {
		return botKey;
	}

	public void setBotKey(String botKey) {
		this.botKey = botKey;
	}
	
	public boolean isBot() {
		return StringUtils.isNotEmpty(this.botKey);
	}
	
	public boolean isRobo() {
		return StringUtils.isNotEmpty(this.botKey);
	}

	public String getDataOnlineChatString() {
		if(this.dataLastChat == null) {
			return DateUtils.toString(this.dataLastLogin, "dd/MM/yyyy HH:mm");
		}
		return DateUtils.toString(this.dataLastChat, "dd/MM/yyyy HH:mm");
	}

	public boolean isPrimeiroAcesso() {
		return primeiroAcesso;
	}

	public void setPrimeiroAcesso(boolean primeiroAcesso) {
		this.primeiroAcesso = primeiroAcesso;
	}

	public Usuario getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}

	public Set<PostTask> getPostTasks() {
		return postTasks;
	}

	public void setPostTasks(Set<PostTask> postTasks) {
		this.postTasks = postTasks;
	}

	public Date getDataUpdatedSenha() {
		return dataUpdatedSenha;
	}

	public void setDataUpdatedSenha(Date dataUpdatedSenha) {
		this.dataUpdatedSenha = dataUpdatedSenha;
	}

	public Set<Token> getTokens() {
		return tokens;
	}

	public void setTokens(Set<Token> tokens) {
		this.tokens = tokens;
	}

	public Idioma getIdioma() {
		return idioma;
	}

	public void setIdioma(Idioma idioma) {
		this.idioma = idioma;
	}

}