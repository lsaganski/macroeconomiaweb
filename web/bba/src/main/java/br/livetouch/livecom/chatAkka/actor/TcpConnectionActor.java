package br.livetouch.livecom.chatAkka.actor;

import org.apache.log4j.Logger;
import org.json.JSONException;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.actor.UntypedActorContext;
import akka.io.Tcp;
import akka.io.Tcp.Received;
import akka.io.Tcp.Write;
import akka.util.ByteString;
import br.infra.util.Log;
import br.livetouch.livecom.chatAkka.actor.UserActor.Stop;
import br.livetouch.livecom.chatAkka.protocol.AssociateWithUser;
import br.livetouch.livecom.chatAkka.protocol.ChatUtils;
import br.livetouch.livecom.chatAkka.protocol.JsonRawMessage;
import br.livetouch.livecom.chatAkka.protocol.RawMessage;
import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.vo.UserInfoVO;

/**
 * Ator para escrever no socket.
 * 
 * Faz o loop de leitura das mensagens e envia pro UserMessageRouter
 * 
 * @author rlech
 *
 */
public class TcpConnectionActor extends UntypedActor {

	private static Logger log = Log.getLogger("TcpConnectionActor");
	
	public enum State {
		Idle, StartReadHeader, StartReadMsg;
	}

	private boolean continueToReadSplitMessage = false;
	private ByteString msgPartsContinue = null;
	private int sizeContinue = 0;
	private int totalBytesReaded = 0;

	public ActorSelection userMessageRouter;
	/**
	 * UserActor criado quando fez a conexão TCP.
	 * É o UserActor que contém várias conexões.
	 */
	public ActorRef userActorWhenLogged;
	public Long userId;
	public String userSo;

	/**
	 * Ator da conexão. Contém o socket para escrever.
	 */
	public ActorRef tcpConnection;
	private int nextPercent;
	private long uploadIdentifier;
	
	public static ActorRef create(UntypedActorContext context, ActorRef sender) {
		ActorRef a = context.actorOf(Props.create(TcpConnectionActor.class, sender));
		return a;
	}

	/**
	 * Construtor chamado por reflexão
	 * 
	 * @param connection
	 */
	public TcpConnectionActor(ActorRef sender) {
		this.tcpConnection = sender;
	}

	@Override
	public void preStart() throws Exception {
		userMessageRouter = AkkaFactory.getUserMessageRouter(getContext());
		super.preStart();
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		try {
			if (msg instanceof Received) {

				read((Received) msg);
				return;
				
			} else if (msg instanceof Stop) {
				log("TcpUserActor Stop");

				getContext().stop(getSelf());
			} else if (msg instanceof Tcp.PeerClosed$) {
				// Fechou socket com socket.close
				log("TcpUserActor PeerClosed");
				stop();
			} else if (msg instanceof Tcp.ErrorClosed) {
				log("TcpUserActor ErrorClosed");
				stop();
			} else if (msg instanceof Tcp.Closed$) {
				log("TcpUserActor Closed");
				stop();
			} else if (msg instanceof Tcp.ConfirmedClosed$) {
				log("TcpUserActor ConfirmedClosed");
				stop();
			} else if (msg instanceof Tcp.Aborted$) {
				log("TcpUserActor Aborted");
				stop();
			} else if (msg instanceof Write) {
				/**
				 * Escreve os dados enviados no Web Socket
				 */
				byte[] bytes = ((Write) msg).data().toArray();
				RawMessage rawMessage = new RawMessage(bytes);
				
				AkkaHelper.logJson(userSo,userId," << " + rawMessage);
				
				/**
				 * Escreve os dados enviados no TCP Socket
				 */
				tcpConnection.tell(msg, getSelf());
			} else if (msg instanceof AssociateWithUser) {
				userActorWhenLogged = ((AssociateWithUser) msg).userActor;
				userId = ((AssociateWithUser) msg).userId;
				userSo = ((AssociateWithUser) msg).userSo;
				log("TcpUserActor Associando conexão com usuário ["+userId+"/"+userSo+"]: " + userActorWhenLogged);
			} else {
				log("TcpUserActor else: " + msg);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			log.error("TcpConnectionActor.onReceive: " + ex.getMessage(), ex);
		}
	}

	private void read(Received msg) {
		ByteString dataReceived = ((Received) msg).data();
		/**
		 * Descobrir tamanho dos bytes recebidos para enviar a % no caso de arquivos.
		 */
		int lenghtBytesReceived = 0;
		try {
			byte[] bytes = dataReceived.toArray();
			lenghtBytesReceived = bytes.length;
//			if(nextPercent > 0) {
				String sreceived = new String(bytes,"UTF-8");
				log(">> TcpConnectionActor.onReceive: " + sreceived, false);
//			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		boolean reading = true;
		ByteString msgParts = ByteString.empty();
		State state = State.Idle;
		int size = 0;

		do {
			if (state == State.Idle) {
				if (dataReceived.length() > 0) {
					state = State.StartReadHeader;
					if (continueToReadSplitMessage) {
						continueToReadSplitMessage = false;
						msgParts = msgPartsContinue;
						size = sizeContinue;
						state = State.StartReadMsg;
					}
				} else {
					reading = false;
				}
			} else if (state == State.StartReadHeader) {
				ByteString firstPart = dataReceived.slice(0, 4 - msgParts.length());
				msgParts = msgParts.concat(firstPart);
				if (msgParts.length() >= 4) {
					state = State.StartReadMsg;
					size = (int) RawMessage.headerBytesToSize(msgParts.slice(0, 4).toArray()); // 114
					dataReceived = dataReceived.slice(4, dataReceived.length());
					log("TcpUserActor - StartReadHeader size " + size);
				} else {
					reading = false;
				}
			} else if (state == State.StartReadMsg) {
				log("TcpUserActor - StartReadMsg");

				ByteString contents = dataReceived.slice(0, size - (msgParts.length() - 4));
				msgParts = msgParts.concat(contents);
				dataReceived = dataReceived.slice(msgParts.length() - 4, dataReceived.length());
				if (msgParts.length() >= 4 + size) {
					log("TcpUserActor - StartReadMsg FSM:Frame Reached");
					ByteString frame = msgParts.slice(0, size + 4);
					/**
					 * FIM LEITURA
					 */
					if(nextPercent > 80) {
						sendProgressUploadFile(lenghtBytesReceived, size);
					}

					// Envia o arquivo lido pro Router
					onReceiveMessage(frame);

					// Zera dados para proxima leitura
					size = 0;
					msgParts = ByteString.empty();
					totalBytesReaded = 0;
					nextPercent = 0;
					continueToReadSplitMessage = false;
					state = State.Idle;
					continue;
				} else {

					sendProgressUploadFile(lenghtBytesReceived, size);

					// Continua a leitura do arquivo
					continueToReadSplitMessage = true;
					msgPartsContinue = msgParts;
					sizeContinue = size;
				}
				reading = false;
			}
		} while (reading);
	}

	private void sendProgressUploadFile(int lengghtBytesReceived, int size) {
		totalBytesReaded += lengghtBytesReceived;
		int percent = totalBytesReaded * 100 / size;
		
		if(percent >= nextPercent) {
			log("TcpUserActor - total size readed: " + percent + "%");
			sendPercentToRouter(percent);
			
			nextPercent += 20;
		}
	}

	private void sendPercentToRouter(int percent) {
		if(userActorWhenLogged != null) {
			percent = percent > 100? 100: percent;
			JsonRawMessage raw = ChatUtils.getJsonProgress(percent, uploadIdentifier);
			userActorWhenLogged.tell(raw, getSelf());
		}
	}

	/**
	 * Depois de le a mensagem, envia pro proximo ator.
	 * 
	 * @param frame
	 * @throws JSONException 
	 */
	private void onReceiveMessage(ByteString frame) {
		try {
			byte[] array = frame.toArray();
			RawMessage message = new RawMessage(array);

			String json = message.getText();

			String code = ChatUtils.getCode(json);

			if ("msgFile".equals(code)) {
				// Salva o identifier para enviar o progresso de upload
				uploadIdentifier = ChatUtils.getIdentifier(json);
				log("receivemsgFile uploadIdentifier ["+uploadIdentifier+"]: " + json);
			} else { 
				JsonRawMessage raw = new JsonRawMessage(getSelf(), message, code, userSo, userId);
				sendToUserMessageRouter(raw);
			}
			
		} catch (Exception ex) {
			log.error("Error processMessage: " + ex.getMessage(), ex);
		}
	}

	protected void sendToUserMessageRouter(JsonRawMessage raw) {
		
		AkkaHelper.logJson(userSo,userId," >> " + raw);
		
		userMessageRouter.tell(raw, getSelf());
	}

	public void stop() {

		if (userActorWhenLogged != null) {
			userActorWhenLogged.tell(new UserActor.RemoveConnection(getSelf(),userSo), getSelf());
		}

		getContext().stop(getSelf());
	}
	
	private void log(String string) {
		log(string,true);
	}

	private void log(String string, boolean trace) {
		int max = 200;
		if(string.length() > max) {
			string = string.substring(0,max);
		}
		if(userActorWhenLogged != null) {
			UserInfoVO user = Livecom.getInstance().getUserInfo(userId);
			if(trace) {
				log.trace("("+user+"/"+userSo+"): " + string);
			} else {
				log.debug("("+user+"/"+userSo+"): " + string);
			}
		}
		else {
			if(trace) {
				log.trace(string);
			} else {
				log.debug(string);
			}
		}
	}
}
