package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.vo.CategoriaVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class CategoriasPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private TextField tIdioma;

	@Override
	public void onInit() {
		super.onInit();

		
		form.setMethod("post");
		form.add(tIdioma = new TextField("idioma"));
		form.add(tMode = new TextField("mode"));

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
			
		tMode.setValue("json");

		form.add(new Submit("buscar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			try {
				
				String idioma = tIdioma.getValue();
				Idioma i = null;
				if(StringUtils.isNotEmpty(idioma)) {
					i = idiomaService.findByCodigo(idioma);
				}
				
				List<CategoriaVO> vos = new ArrayList<CategoriaVO>();
				List<CategoriaPost> pais = categoriaPostService.findAllParentByIdioma(getEmpresa(), i);
				vos = categoriaPostService.mountCategoriasVO(pais, i);
				
				if(isWsVersion3()) {
					Response r = Response.ok("OK");
					r.categorias = vos;
					return r;
				}
				return vos;
			} catch (Exception e) {
				logError(e.getMessage(), e);
				if(isWsVersion3()) {
					Response r = Response.error("Erro ao buscar.");
					return r;
				}
				return new MensagemResult("NOK","Erro ao buscar.");
			}
		}
		
		if(isWsVersion3()) {
			Response r = Response.error("Categorias não encontradas");
			return r;
		}
		
		return new MensagemResult("NOK","Categorias não encontradas");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("categoria", CategoriaVO.class);
		super.xstream(x);
	}
}
