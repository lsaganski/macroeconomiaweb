package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Usuario;

public class UsuarioAtendimentoVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public String login;
	public String nome;
	public String email;
	
	public String cargo;
	public String urlFotoUsuario;
	public String urlFotoUsuarioThumb;

	public void setUsuario(Usuario u) {
		
		this.id = u.getId();
		this.login = u.getLogin();
		this.nome = u.getNome();
		this.email = u.getEmail();

		this.cargo = u.getCargo();

		this.urlFotoUsuario = u.getUrlFoto();
		this.urlFotoUsuarioThumb = u.getUrlThumb();
	}

	public static List<UsuarioAtendimentoVO> toList(List<Usuario> list) {
		List<UsuarioAtendimentoVO> users = new ArrayList<UsuarioAtendimentoVO>();
		if (list != null) {
			for (Usuario u : list) {
				UsuarioAtendimentoVO vo = new UsuarioAtendimentoVO();
				vo.setUsuario(u);
				users.add(vo);
			}
		}
		return users;
	}
}
