package br.livetouch.livecom.web.pages.pages;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Playlist;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.vo.PostVO;

/**
 * 
 */
@Controller
@Scope("prototype")
public class PlaylistPage extends PostComunicadoPage {
	public List<Playlist> playlists;
	public List<PostVO> posts;

	@Override
	public void onInit() {
		super.onInit();
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onPost() {
		super.onPost();
	}
	
	@Override
	public void onRender() {
		super.onRender();

		playlists = playlistService.findAllByUser(getUsuario());

		List<Post> list = new ArrayList<Post>();
		for (Playlist f : playlists) {
			Post p = f.getPost();
			if(!list.contains(p)) {
				list.add(p);
			}
		}
		
		posts = postService.toListVo(getUsuario(), list);
		
		formAberto = false;
		
	}
}
