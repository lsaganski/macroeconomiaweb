package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.web.pages.admin.LivecomPage;

@Controller
@Scope("prototype")
public class ReloadParamsPage extends LivecomPage {
	
	@Override
	public boolean onSecurityCheck() {
		return true;
	}

	@Override
	public void onGet() {
		super.onGet();
		
		boolean on = getParams().getBoolean(Params.SECURITY_HOST_ON,false);
		System.out.println(on);

		ParametrosMap parametros = parametroService.init();
		log("Parametros carregados: " + parametros);
		
		msg = "Parametros carregados: " + parametros.getMap();
		
		on = getParams().getBoolean(Params.SECURITY_HOST_ON,false);
		System.out.println(on);
	}
}
