package br.livetouch.livecom.domain.vo;

import java.util.List;

public class RelatorioAppVersaoVO {

	private List<AppVersaoVO> versoes;
	private Long total;
	
	public RelatorioAppVersaoVO(){
	}

	public RelatorioAppVersaoVO(List<AppVersaoVO> versoes, Long total){
		this.setVersoes(versoes);
		this.setTotal(total);
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public List<AppVersaoVO> getVersoes() {
		return versoes;
	}

	public void setVersoes(List<AppVersaoVO> versoes) {
		this.versoes = versoes;
	}

}