package br.livetouch.livecom.web.pages.ws;

import java.io.Serializable;

public class MensagemResult implements Serializable {
	private static final long serialVersionUID = -319066101047853468L;

	public String status;
	public String mensagem;
	public String link;
	public String errorCode;
	
	public Object result;
	
	public MensagemResult() {
		
	}
	
	public static MensagemResult ok(String msg) {
		MensagemResult m = new MensagemResult();
		m.status = "OK";
		m.mensagem = msg;
		return m;
	}
	
	public static MensagemResult exception(String msg) {
		MensagemResult m = new MensagemResult();
		m.status = "EXCEPTION";
		m.mensagem = msg;
		return m;
	}

	public static MensagemResult erro(String msg) {
		MensagemResult m = new MensagemResult();
		m.status = "ERRO";
		m.mensagem = msg;
		return m;
	}

	public static MensagemResult erro(String msg, String errorCode) {
		MensagemResult m = new MensagemResult();
		m.status = "ERRO";
		m.mensagem = msg;
		m.errorCode = errorCode;
		return m;
	}
	
	
	public MensagemResult(String status, String mensagem) {
		super();
		this.status = status;
		this.mensagem = mensagem;
	}
	
	public MensagemResult(String status, String mensagem, Object object) {
		super();
		this.status = status;
		this.mensagem = mensagem;
		this.result = object;
	}
	
	public MensagemResult(String status, String tipo, String mensagem) {
		super();
		this.status = status;
		this.mensagem = mensagem;
	}

	public boolean isError() {
		return "ERRO".equalsIgnoreCase(status) || "ERROR".equalsIgnoreCase(status) || "NOK".equalsIgnoreCase(status);
	}

	@Override
	public String toString() {
		return "MensagemResult [status=" + status + ", mensagem=" + mensagem + "]";
	}
}
