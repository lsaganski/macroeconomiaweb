package br.livetouch.livecom.files;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.utils.FTPFileLivecom;
import net.sf.click.extras.tree.TreeNode;

/**
 * 
 Apache:
 * 
 * /etc/apache2/sites-available/livetouch.conf
 * 
 * <VirtualHost *:80> ServerName static.livetouchdev.com.br ServerAlias
 * static.livetouchdev.com.br DocumentRoot /var/www/html/static ErrorLog
 * ${APACHE_LOG_DIR}/error.log CustomLog ${APACHE_LOG_DIR}/access.log combined
 * </VirtualHost>
 * 
 * 
 * 
 * @author rlech
 * 
 */
public class FTPFileManager implements FileManager {
	protected static final Logger log = Log.getLogger(FileManager.class);

	private Integer connected = null;
	private FTPClient ftp;
	/** EX: livetouchdev.com.br **/
	private String server;
	/** EX: livetouch **/
	private String login;
	/** EX: livetouch@2013 **/
	private String senha;
	/** Default: 21 **/
	private Integer porta;

	ParametrosMap params;
	
	private TreeNode tree = null;

	/**
	 * Caminho para enviar os arquivos para o ftp <br>
	 * Defautl: /var/www/html
	 **/
	private String workplace;
	/**
	 * Default: livecom
	 * */
	private String bucket;

	private void connect() throws IOException {
		boolean notConnected = connected == null || ftp == null || !FTPReply.isPositiveCompletion(connected);
		if (notConnected) {
			ftp = new FTPClient();
			ftp.connect(server, porta);
			ftp.login(login, senha);

			connected = ftp.getReplyCode();

			if (!FTPReply.isPositiveCompletion(connected)) {
				ftp.disconnect();
				log.error("FTP server refused connection.");
				throw new IOException("Não foi possível realiar conexão com o Servidor de FPT, usuário ou senha inválidos");
			} else {
				ftp.changeWorkingDirectory(workplace);
				createBucketDir();
			}
		}
	}
	
	
	public FTPFileManager(ParametrosMap params) throws IOException {
		this.params = params;
		server = params.get(Params.FILE_MANAGER_FTP_SERVER, "static.livetouchdev.com.br");
		if (StringUtils.isEmpty(server)) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.ftp.server]");
		}

		login = params.get(Params.FILE_MANAGER_FTP_LOGIN, "livetouch");
		if (StringUtils.isEmpty(login)) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.ftp.login]");
		}

		senha = params.get(Params.FILE_MANAGER_FTP_SENHA, "livetouch@2013");
		if (StringUtils.isEmpty(senha)) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.ftp.senha]");
		}

		porta = params.getInt(Params.FILE_MANAGER_FTP_PORT, 21);
		if (porta == null) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.ftp.port]");
		}

		workplace = params.get(Params.FILE_MANAGER_FTP_WORKPLACE, "/static");
		if (StringUtils.isEmpty(workplace)) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.ftp.workplace]");
		}

		bucket = params.get(Params.FILE_MANAGER_FTP_BUCKET, "cielo");
		if (StringUtils.isEmpty(bucket)) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.ftp.bucket]");
		}
		connect();
	}

	private void createBucketDir() {
		try {
			String dir = getPathWorplaceBucket();
			if (!ftp.changeWorkingDirectory(dir)) {
				ftp.makeDirectory(dir);
			}
			setPublic(dir);
		} catch (IOException e) {
			log.debug("Não foi possível criar o arquivo o bucket para o projeto " + getPathWorplaceBucket(), e);
		}
	}

	@Override
	public List<LivecomFile> getFiles() {
		try {
			FTPFile[] files = ftp.listFiles();
			List<LivecomFile> list = new ArrayList<LivecomFile>();
			for (FTPFile ftpFile : files) {
				FTPFileLivecom f = new FTPFileLivecom(ftpFile);
				list.add(f);
			}
			return list;

		} catch (IOException e) {
			log.debug("Não foi possível pegar os arquivos " + e.getMessage(), e);
			return new ArrayList<LivecomFile>();
		} finally {
		}
	}

	@Override
	public LivecomFile getFile(String name) {
		List<LivecomFile> files = getFiles();
		if (files != null) {
			for (LivecomFile f : files) {
				if (StringUtils.equals(name, f.getFileName())) {
					return f;
				}
			}
		}
		return null;
	}

	public byte[] getBytesFromFile(String dir, String fileName) throws FileNotFoundException, IOException {
		if (getFile(fileName) != null) {
			InputStream inputStream = ftp.retrieveFileStream(dir + fileName);
			byte bytes[] = IOUtils.toByteArray(inputStream);
			return bytes;
		} else {
			return null;
		}
	}

	public TreeNode getTree() throws IOException {
		tree = new TreeNode("root");
		return createTree(tree, workplace, "", null);
	}

	private TreeNode createTree(TreeNode nodeTree, String parentDir, String currentDir, String posicao) throws IOException {
		String dirToList = parentDir;
		if (!currentDir.equals("")) {
			dirToList += "/" + currentDir;
		}
		FTPFile[] subFiles = ftp.listFiles(dirToList);
		if (subFiles != null && subFiles.length > 0) {
			int contador = 0;
			for (FTPFile aFile : subFiles) {
				String currentFileName = aFile.getName();
				if (currentFileName.equals(".") || currentFileName.equals("..")) {
					// skip parent directory and directory itself
					continue;
				}
				String posLocal = StringUtils.isNotEmpty(posicao) ? posicao + "." + (contador + 1) : (contador + 1) + "";
				TreeNode node = new TreeNode(aFile.getName(), posLocal, nodeTree, aFile.isDirectory());
				if (aFile.isDirectory()) {
					createTree(node, dirToList, currentFileName, posLocal);
				} else {

				}
				contador++;
			}
		}
		return tree;
	}

	@Override
	public void delete(String file) throws IOException {
		String path = getPathWorplaceBucket() + "/" + file;
		ftp.deleteFile(path);
	}

	@Override
	public List<LivecomFile> getFilesInFolder(String folder) {
		try {
			FTPFile[] files = ftp.listFiles(folder);
			List<LivecomFile> list = new ArrayList<LivecomFile>();
			for (FTPFile ftpFile : files) {
				if (ftpFile.isFile()) {
					FTPFileLivecom f = new FTPFileLivecom(ftpFile);
					f.setKey(folder + "/" + f.getFileName());
					f.setServer(getServerUrlBucket());
					list.add(f);
				}
			}
			return list;

		} catch (IOException e) {
			log.debug("Não foi possivel listar as pastas " + e.getMessage(), e);
			return new ArrayList<LivecomFile>();
		} finally {
		}
	}

	@Override
	public List<String> getFoldersInFolder(String folder) {
		try {
			FTPFile[] files = ftp.listFiles(folder);
			List<String> list = new ArrayList<String>();
			for (FTPFile ftpFile : files) {
				if (ftpFile.isDirectory()) {
					list.add(ftpFile.getName());
				}
			}
			return list;
		} catch (IOException e) {
			log.debug("Não foi possivel pegar as pastas  " + e.getMessage(), e);
			return new ArrayList<String>();
		} finally {
		}
	}

	@Override
	public String putFile(String dir, String fileName, String contentType, byte[] bytes) {
		try {
			ByteArrayInputStream input = new ByteArrayInputStream(bytes);

			if (!contentType.equals("text/plain")) {
				ftp.setFileType(FTP.BINARY_FILE_TYPE);
				ftp.setFileTransferMode(FTP.BINARY_FILE_TYPE);
			} else {
				ftp.setFileType(FTP.ASCII_FILE_TYPE, FTP.ASCII_FILE_TYPE);
				ftp.setFileTransferMode(FTP.ASCII_FILE_TYPE);
			}
			if (!ftp.changeWorkingDirectory(dir)) {
				dir = makeDiretories(dir, ftp);
			}
			dir = getPathWorplaceBucket() + "/" + dir;
			if(ftp.changeWorkingDirectory(dir)) {
				ftp.storeFile(fileName, input);
				setPublic(fileName + "/" + fileName);
				return fileName;
			}
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
		}
		return null;
	}
	
	@Override
	public String putFileLambda(String dir, String fileName, String contentType, byte[] bytes) {
		return putFile(dir, fileName, contentType, bytes);
	}

	@Override
	public String putFileThumb(String dir, String fileName, String contentType, byte[] bytes) {
		return putFile(dir, fileName, contentType, bytes);
	}

	public void setPublic(String path) throws IOException {
		ftp.sendSiteCommand("chmod " + "755 " + path);
	}

	@Override
	public String getFileUrl(String dir, String file) {
		String url = null;
		if (StringUtils.isNotEmpty(dir)) {
			url = getServerUrlBucket() + "/" + dir + "/" + file;
		} else {
			url = getServerUrlBucket() + "/" + file;
		}
		if(!url.contains("http")) {
			url = "http://" + url;
		}
		return url;
	}

	@Override
	public String getFileLambdaUrl(String dir, String file) {
		return getFileUrl(dir, file);
	}

	@Override
	public String getThumbLambdaURL(String dir, String file) {
		return getFileUrl(dir, file);
	}

	@Override
	public void createFolder(String dir) {
		dir = getPathWorplaceBucket() + "/" + dir;
		try {
			if (!ftp.changeWorkingDirectory(dir)) {
				ftp.makeDirectory(dir);
			}
			setPublic(dir);
		} catch (IOException e) {
			log.debug("Não foi possível criar o diretorio " + dir);
		} finally {
		}
	}

	@Override
	public boolean somenteFotosProfile(String dir) {
		return false;

	}

	private String getPathWorplaceBucket() {
		if (StringUtils.isNotEmpty(bucket)) {
			return workplace + "/" + bucket;
		}
		return bucket;
	}

	private String getServerUrlBucket() {
		if (StringUtils.isNotEmpty(bucket)) {
			return server + "/" + bucket;
		}
		return server;
	}

	@Override
	public void close() {
		connected = null;
		try {
			ftp.disconnect();
			ftp = null;
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}

	public String makeDiretories(String diretorios, FTPClient ftp) throws Exception {
		if (diretorios == null || diretorios.trim().length() == 0) {
			throw new Exception("Diretório [" + diretorios + "] required !");
		}
		
		String[] dirs = StringUtils.split(diretorios, "/");
		String path = "";
		if(dirs != null && dirs.length > 0) {
			for (String s : dirs) {
				path += s + "/";
				createFolder(path);
			}
		}
		return path;
	}

	public String resolvePath(String path) {
		File dir = new File(path);
		return dir.getPath().replace("//", "/");
	}

	public static void main(String[] args) throws IOException {
		ParametrosMap map = ParametrosMap.getInstance();
		FTPFileManager ftp = new FTPFileManager(map);
		ftp.createFolder("teste");
	}
}
