package br.livetouch.livecom.web.pages.report;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.CamposMap;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import br.livetouch.livecom.web.pages.pages.MuralPage;


/**
 * 
 * @author ricardo
 *
 */
@Controller
@Scope("prototype")
public class RelatorioPage extends LivecomLogadoPage {

	public CamposMap campos = getCampos();
	
	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
	}
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
		Usuario u = getUsuario();
		if (u != null) {
			
			if(hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
				return true;
			}
		}
		
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}
}
