package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.EntityField;
import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.CategoriasMap;
import br.livetouch.livecom.domain.Chapeu;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.BuscaPost;
import br.livetouch.livecom.domain.vo.ControleVersaoVO;
import br.livetouch.livecom.domain.vo.GrupoUsuariosCountVO;
import br.livetouch.livecom.domain.vo.GrupoUsuariosVO;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.NotificationBadge;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.livecom.domain.vo.TagVO;
import br.livetouch.livecom.domain.vo.UsuarioAutocompleteFiltro;
import br.livetouch.livecom.domain.vo.UsuarioPostsResponseVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.security.UserAgentUtil;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.DateField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class MuralPage extends WebServiceXmlJsonPage {
	
	public Form form = new Form();
	private TextField tTexto;
	private TextField tMode;
	private TextField tBuscaUsuarios;
	private TextField tBuscaResumida;
	private TextField tBuscaComAnexo;
	private EntityField<Arquivo> tBuscaPostsArquivo;
	private TextField tBuscaMeusFavoritos;
	private TextField tBuscaMeusRascunhos;
	private TextField tBuscaPosts;
	private UsuarioLoginField tUser;
	private UsuarioLoginField tUserPost;
	private TextField tCategorias;
	private TextField tTags;
	private DateField tDataInicial;
	private DateField tDataFinal;
	private TextField tBuscaDestaque;
	private TextField tBuscaGrupo;
	private LongField tChapeu;
	private LongField tNovosPostsId;
	private LongField tExcludePostId;
	
	private TextField tSomenteEu;
	private TextField tMeusAmigos;
	private TextField tAppVersionCode;
	private TextField tAppVersion;
	
	private LongField tStatusPost;

	private TextField tPrioritarios;
	private TextField tTasks;
	private TextField tReminders;

	private TextField tIdioma;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tUser = new UsuarioLoginField("user_id", true, usuarioService, getEmpresa()));
		if (getUsuario() != null) {
			tUser.setValue(getUsuario().getLogin());
		}
		tUser.setFocus(true);

		form.add(tTexto = new TextField("texto"));

		form.add(tUserPost = new UsuarioLoginField("user_post_id", false, usuarioService, getEmpresa()));
		form.add(tCategorias = new TextField("categoria_id", false));
		form.add(tTags = new TextField("tags"));

		form.add(tBuscaPostsArquivo = new EntityField<>("arquivo_id", arquivoService));
		
		tDataInicial = new DateField("dataInicial", false);
		tDataFinal = new DateField("dataFinal", false);
		tDataInicial.setFormatPattern("dd/MM/yyyy");
		tDataFinal.setFormatPattern("dd/MM/yyyy");
		form.add(tDataInicial);
		form.add(tDataFinal);

		tDataInicial.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		tDataFinal.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));

		tDataInicial.setMaxLength(10);
		tDataFinal.setMaxLength(10);

		form.add(tSomenteEu = new TextField("somenteEu"));
		form.add(tMeusAmigos = new TextField("meusAmigos"));
		
		form.add(tBuscaUsuarios = new TextField("buscaUsuarios"));
		form.add(tBuscaPosts = new TextField("buscaPosts"));
		form.add(tBuscaResumida = new TextField("buscaResumida"));
		form.add(tBuscaGrupo = new TextField("buscaGrupos"));
		form.add(tBuscaComAnexo = new TextField("comAnexo"));
		form.add(tBuscaMeusFavoritos = new TextField("meusFavoritos"));
		form.add(tBuscaMeusRascunhos = new TextField("meusRascunhos"));
		form.add(tBuscaDestaque = new TextField("destaque","0 (nada), 1 (destaques), 2 (não busca destaque)"));
		form.add(tChapeu = new LongField("chapeu_id"));
		form.add(tAppVersion = new TextField("app.version", "APP VERSION"));
		form.add(tAppVersionCode = new TextField("app.version_code", "APP VERSION CODE"));
		form.add(new TextField("grupo_ids"));
		
		form.add(tNovosPostsId = new LongField("novos_posts_id","novos_posts_id"));
		form.add(tExcludePostId = new LongField("exclude_post_id","exclude_post_id"));
		form.add(tStatusPost = new LongField("status_post","status_post"));

		form.add(tPrioritarios = new TextField("prioritarios","Posts prioritarios (1 - prioritarios), (0 ou nada - todos) "));
		form.add(tTasks = new TextField("tasks","Tasks (1 - tasks), (0 ou nada - todos) "));
		form.add(tReminders = new TextField("reminders","Reminders (1 - reminders), (0 ou nada - todos) "));

		tBuscaUsuarios.setValue("0");
		tBuscaPosts.setValue("1");
		tBuscaResumida.setValue("0");
		tBuscaComAnexo.setValue("0");
		tBuscaMeusFavoritos.setValue("0");
		tBuscaMeusRascunhos.setValue("0");

		form.add(tIdioma = new TextField("idioma"));

		form.add(new TextField("maxRows"));
		form.add(new TextField("page"));

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode", true));
		tMode.setValue("json");

		form.add(new Submit("consultar"));

		// setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario user = tUser.getEntity();
			Usuario userPost = tUserPost.getEntity();			

			if (user == null) {
				if(isWsVersion3()) {
					Response r = Response.error("Informe o usuário.");
					return r;
				}
				return new MensagemResult("ERROR", "Informe o usuário.");
			}

			//Valida se esta dentro do expediente do usuario
			validaHorario(user.getId());
			
			List<Long> categoriasIdIn = new ArrayList<>();
			List<Long> categoriasIdNotIn = new ArrayList<>();
			List<String> categoriasCodigoIn = new ArrayList<>();
			List<String> categoriasCodigoNotIn = new ArrayList<>();
			String[] splitCategoriasId = StringUtils.split(tCategorias.getValue(), ",");
			
			
			for (String s : splitCategoriasId) {
				
				boolean isContains = StringUtils.contains(s, "-");
				String r = isContains ? s.replace("-", "").trim() : s;
				CategoriaPost categ = StringUtils.isNumeric(r) ? categoriaPostService.get(Long.parseLong(r)) : categoriaPostService.findByCodigo(user.getEmpresa(), r); 
				
				if(categ != null) {
					String codigo = categ.getCodigo();
					if(StringUtils.isNotEmpty(codigo) && !isContains) {
						String c = codigo.replace("-", "").trim();
						CategoriaPost catPost = categoriaPostService.findByCodigo(user.getEmpresa(), c);
						if(catPost != null && StringUtils.contains(codigo, "-")) {
							categoriasIdNotIn.add(catPost.getId());
							continue;
						}
					}
					
					if(isContains) {
						categoriasIdNotIn.add(categ.getId());
					} else {
						categoriasIdIn.add(categ.getId());
					}
				}
				
			}
			
			String texto = tTexto.getValue();

			int maxRows = NumberUtils.toInt(getContext().getRequestParameter("maxRows"), 20);

			boolean buscaUsuarios = "1".equals(tBuscaUsuarios.getValue());
			boolean buscaPosts = "1".equals(tBuscaPosts.getValue());
			boolean buscaResumida = "1".equals(tBuscaResumida.getValue());
			boolean comAnexo = "1".equals(tBuscaComAnexo.getValue());
			boolean meusFavoritos = "1".equals(tBuscaMeusFavoritos.getValue());
			boolean meusRascunhos = "1".equals(tBuscaMeusRascunhos.getValue());
			boolean buscaDestaque = "1".equals(tBuscaDestaque.getValue());
			boolean buscaNotDestaques = "2".equals(tBuscaDestaque.getValue());
			boolean buscaGrupos = "1".equals(tBuscaGrupo.getValue());
			boolean prioritario = "1".equals(tPrioritarios.getValue());
			boolean tasks = "1".equals(tTasks.getValue());
			boolean reminders = "1".equals(tReminders.getValue());
			Long novosPostsId = tNovosPostsId.getLong();
			Long excludePostId = tExcludePostId.getLong();


			if (StringUtils.isEmpty(texto)) {
				buscaUsuarios = false;
			}
			
			BuscaPost b = new BuscaPost();
			b.empresa = getEmpresa();
			b.comAnexo = comAnexo;
			b.favoritos = meusFavoritos;
			b.rascunhos = meusRascunhos;
			b.user = user;
			b.userPost = userPost;

			boolean subcategorias = getParametrosMap().getBoolean(Params.SUBCATEGORIAS_ON, false);
			if(subcategorias) {
				b.categoriaIds = CategoriasMap.getInstance(getEmpresa()).getCategorias(categoriasIdIn);
			} else {
				b.categoriaIds = categoriasIdIn;
			}
			b.texto = texto;
			b.destaque = buscaDestaque;
			b.notDestaques = buscaNotDestaques;
			b.buscaGrupos = buscaGrupos;
			b.categoriasNotIn = categoriasIdNotIn;
			b.categoriasCodigo = categoriasCodigoIn;
			b.categoriasCodigoNotIn = categoriasCodigoNotIn;
			b.amigosIds = usuarioService.findIdsAmigos(getUsuario());
			b.novosPosts = novosPostsId;
			b.excludePostId = excludePostId;
			b.somenteEu = "true".equals(tSomenteEu.getValue());
			b.meusAmigos = "true".equals(tMeusAmigos.getValue());
			b.arquivo = tBuscaPostsArquivo.getEntity();
			b.prioritario = prioritario;
			b.tasks = tasks;
			b.reminders = reminders;

			Long statusPostId = tStatusPost.getLong();
			if(statusPostId != null) {
				b.statusPost = statusPostService.get(statusPostId); 
			}

			String idiomaCodigo = tIdioma.getValue();
			if(StringUtils.isNotEmpty(idiomaCodigo)) {
				b.idioma = idiomaService.findByCodigo(idiomaCodigo);
			}

			Long idChapeu = tChapeu.getLong();
			if (idChapeu != null) {
				Chapeu chapeu = chapeuService.get(idChapeu);
				b.chapeu = chapeu;
			}

			List<Long> tagsids = getListIds("tags");
			List<Tag> tags = tagService.findAllByIds(tagsids, getEmpresa());
			b.tags = StringUtils.isEmpty(tTags.getValue()) ? null : tags;
			b.dataInicial = StringUtils.isEmpty(tDataFinal.getValue()) ? null : tDataInicial.getDate();
			b.dataFinal = StringUtils.isEmpty(tDataFinal.getValue()) ? null : tDataFinal.getDate();

			List<Long> grupoIds = getListIds("grupo_ids");
			if (grupoIds != null && grupoIds.size() > 0) {
				b.grupos = grupoService.findAllByIds(grupoIds);
			}

			// Busca Posts
			List<PostVO> postsVO = getPosts(user, maxRows, buscaPosts, buscaResumida, b);

			// Busca Usuarios
			List<UsuarioVO> usersList = getUsers(texto, maxRows, buscaUsuarios);

			Grupo filtro = new Grupo();
			filtro.setNome(texto);
			
			List<GrupoUsuariosCountVO> grupos = null;
			if(buscaGrupos) {
				// Busca Grupos
				grupos = getGrupos(user, maxRows,filtro);
			}
			NotificationBadge badges = null;
			if(!UserAgentUtil.isWeb(getContext())) {
				badges = getBadges(user);
			}
			
			String appVersion = tAppVersion.getValue();
			String appVersionCode = tAppVersionCode.getValue();
			ControleVersaoVO controleVersao = null;
			if(StringUtils.isNotEmpty(appVersion) && StringUtils.isNotEmpty(appVersionCode)) {
				String userAgentSO = UserAgentUtil.getUserAgentSO(getContext());
				controleVersao = usuarioService.validateVersion(getEmpresaId(),appVersion , Integer.parseInt(appVersionCode), userAgentSO);
			}
			
			if(isWsVersion3()) {
				Response r = Response.ok("OK");
				r.posts = postsVO;
				r.users = usersList;
				r.posts = postsVO;
				r.grupos = grupos;
				r.badges = badges;
				r.controleVersao = controleVersao;
				return r;
			}
			
			UsuarioPostsResponseVO response = new UsuarioPostsResponseVO();
			response.badges = badges;
			response.users = usersList;
			response.posts = postsVO;
			response.grupos = grupos;
			response.controleVersao = controleVersao;
			return response;
		}

		return new MensagemResult("NOK", "Erro ao buscar.");
	}


	private List<GrupoUsuariosCountVO> getGrupos(Usuario user, int maxRows,Grupo filtro) throws DomainException {
		List<GrupoUsuariosVO> grupos = grupoService.findbyFilter(filtro, user, page, maxRows);
		List<GrupoUsuariosCountVO> gruposVO = GrupoUsuariosCountVO.toListVO(grupoService,grupos);
		return gruposVO;
	}

	private List<UsuarioVO> getUsers(String texto, int maxRows,	boolean buscaUsuarios) {
		UsuarioAutocompleteFiltro filtro = new UsuarioAutocompleteFiltro();
		filtro.setNome(texto);
		filtro.setPage(page);
		filtro.setMaxRows(maxRows);
		List<Usuario> users = buscaUsuarios ? usuarioService.findByFiltro(filtro, getEmpresa()) : new ArrayList<Usuario>();
		List<UsuarioVO> usersList = UsuarioVO.toList(users, usuarioService, grupoService);
		for (UsuarioVO p : usersList) {
			p.resumir();
		}
		return usersList;
	}

	private List<PostVO> getPosts(Usuario user, int maxRows,boolean buscaPosts, boolean buscaResumida, BuscaPost b) {
		List<PostVO> postsVO = new ArrayList<>();
		if(wordpress.isWordpressOn()) {
			if(StringUtils.isNotEmpty(b.texto)) {
				postsVO = wordpress.getPostByTitle(b.texto.replaceAll("\\s+","-"));
			} else {
				postsVO = wordpress.getPosts(b.user, maxRows, ++page);
			}
			if(postsVO == null) {
				postsVO = new ArrayList<>();
			}
		} else {
			// Busca posts
			List<Post> posts = buscaPosts ? postService.findAllPostsByUserAndTitle(b, page, maxRows) : new ArrayList<Post>();
			// VO
			
			if(!posts.isEmpty()) {
				postViewService.visualizarPosts(posts, user);
			}
			
			postsVO = postService.toListVo(user, posts, b.idioma); 
					
		}
		if (buscaResumida) {
			for (PostVO p : postsVO) {
				p.resumir();
			}
		}
		return postsVO;
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("post", PostVO.class);
		x.alias("user", UsuarioVO.class);
		x.alias("busca", UsuarioPostsResponseVO.class);
		x.alias("grupo", GrupoVO.class);
		x.alias("tag", TagVO.class);
		x.alias("grupoCount", GrupoUsuariosCountVO.class);
		super.xstream(x);
	}
}