/*package br.livetouch.livecom.domain.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.GrupoMensagem;
import br.livetouch.livecom.domain.GrupoMensagemUsuarios;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.GrupoMensagemRepository;
import br.livetouch.livecom.domain.service.GrupoMensagemService;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.domain.vo.GrupoMensagemVO;
import br.livetouch.livecom.domain.vo.UsuarioAutocompleteFiltro;
import net.livetouch.extras.util.ExceptionUtil;
import net.livetouch.tiger.ddd.DomainException;


@Service
public class GrupoMensagemServiceImpl implements GrupoMensagemService {
	@Autowired
	private GrupoMensagemRepository rep;

	@Autowired
	private LogService logService;
	
	@Override
	public GrupoMensagem get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Usuario userInfo, GrupoMensagem grupo)
			throws DomainException {
		try {
			Validate.notNull(grupo);
			Validate.notEmpty(grupo.getNome());

			boolean create = grupo.getId() == null;
//			if(create) {
//				boolean existe = rep.exists(grupo.getNome());
//				if (existe) {
//					throw new DomainMessageException("validate.grupo.ja.existe");
//				}
//			}
			
			auditSave(userInfo, grupo, create);
			
		} catch (Exception e) {
			// Qualquer erro decorrente desta rotina deverá ser gravado no “Log
			// do Sistema”.
			LogSistema l = LogSistema.logError(userInfo, ExceptionUtil
					.getStackTrace(e));
			logService.saveOrUpdateNewTransaction(l, userInfo.getEmpresa());
			throw new RuntimeException(e);
		}
	}
	
	protected void auditSave(Usuario userInfo, GrupoMensagem t, boolean create) {
		if (t.getDateCreate() == null) {
			t.setDateCreate(new Date());
			t.setUserCreate(userInfo);
		}
		t.setDateUpdate(new Date());
		if (userInfo != null) {
			t.setUserUpdate(userInfo);
		}
		rep.saveOrUpdate(t);

		if (userInfo != null) {
			// audit
			//StringBuffer sb = new StringBuffer("Grupo " + (create ? "criado" : "atualizado") + ": " + t.toStringAll());
			//String msgAudit = StringUtils.abbreviate(sb.toString(), 1000);
			//GrupoAuditoria a = GrupoAuditoria.audit(t, userInfo, msgAudit, 1);
			//rep.saveOrUpdate(a);
		}
	}

	@Override
	public void delete(Usuario userInfo, GrupoMensagem g) throws DomainException {
		
		List<GrupoMensagemUsuarios> usuariosGrupo = findAllGrupoMensagemUsuariosByGrupo(g);
		
		for (GrupoMensagemUsuarios userGrupo : usuariosGrupo) {
			userGrupo.setUsuario(null);
			userGrupo.setGrupo(null);
			
			rep.delete(userGrupo);
		}
		
		
		g.setUserCreate(null);
		g.setUserUpdate(null);
		
		rep.delete(g);
	}
	
	protected void auditDelete(Usuario userInfo, GrupoMensagem c) {
		//StringBuffer sb = new StringBuffer("Grupo Deletado: " + c.toStringAll());
		// audit
		//String msgAudit = StringUtils.abbreviate(sb.toString(),1000);
		//GrupoAuditoria a = GrupoAuditoria.audit(c,userInfo,msgAudit,2);
		//rep.saveOrUpdate(a);
	}
	
	@Override
	public List<GrupoMensagem> findAll() {
		return rep.findAll(true);
	}

	@Override
	public GrupoMensagem findByNome(String nome) {
		return rep.findByNome(nome);
	}
	
	@Override
	public void saveOrUpdate(GrupoMensagemUsuarios gu) {
		rep.saveOrUpdate(gu);
	}

	@Override
	public List<Usuario> findUsersByConversa(Long id) {
		return rep.findAllUsersByID(id);
	}

	@Override
	public List<GrupoMensagemUsuarios> findAllGrupoMensagemUsuariosByGrupo(
			GrupoMensagem grupo) {
		return rep.findAllGrupoMensagemUsuariosByGrupo(grupo);
	}

	@Override
	public void saveOrUpdate(GrupoMensagem g) throws DomainException {
		rep.saveOrUpdate(g);
	}

	@Override
	public List<GrupoMensagemVO> findByNomeAutocomplete(UsuarioAutocompleteFiltro filtro) {
		return rep.findByNomeAutocomplete(filtro);
	}
	
}
*/