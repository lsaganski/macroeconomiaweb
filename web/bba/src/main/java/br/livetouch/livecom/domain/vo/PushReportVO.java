package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.chatAkka.DateUtils;
import br.livetouch.livecom.domain.PushReport;

public class PushReportVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	private String data;
	private String mensagem;
	private boolean status;
	private String postTitulo;
	private Long postId;
	private String tipo;
	
	public PushReportVO() {
	}
	
	public PushReportVO(PushReport pushRep) {
		if(pushRep != null){
			this.id = pushRep.getId();
			this.setData(pushRep.getDataEnvio() != null ? DateUtils.toString(pushRep.getDataEnvio(), "dd/MM/yyyy HH:mm:ss") : "-");
			this.setMensagem(pushRep.getMensagem());
			this.setStatus(pushRep.isStatus());
			this.setPostTitulo(pushRep.getPost() != null ? pushRep.getPost().getTitulo() : "-");
			this.setPostId(pushRep.getPost() != null ? pushRep.getPost().getId() : 0L);
			this.setTipo(pushRep.getTipoPush() != null ? pushRep.getTipoPush().name() : "-");
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getPostTitulo() {
		return postTitulo;
	}

	public void setPostTitulo(String postTitulo) {
		this.postTitulo = postTitulo;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	

}
