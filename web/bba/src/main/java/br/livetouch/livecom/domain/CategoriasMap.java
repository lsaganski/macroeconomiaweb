package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public class CategoriasMap {

	private static final HashMap<Long, CategoriasMap> mapInstance = new HashMap<Long, CategoriasMap>();

	public static final Long EMPRESA_LIVECOM = 1L;

	private HashMap<Long, Set<Long>> hash;
	
	@SuppressWarnings("unused")
	private Long empresaId;

	
	public static CategoriasMap getInstance() {
		return getInstance(EMPRESA_LIVECOM);
	}
	
	public static CategoriasMap getInstance(Empresa e) {
		return getInstance(e != null ? e.getId() : null);
	}
	
	public static CategoriasMap getInstance(Long empresaId) {
		CategoriasMap map = mapInstance.get(empresaId);
		if(map == null) {
			map = new CategoriasMap();
			map.empresaId = EMPRESA_LIVECOM;
			map.hash = new LinkedHashMap<Long, Set<Long>>();
			mapInstance.put(empresaId, map);
		}
		return map;
	}

	public static CategoriasMap setCategorias(Long empresaId, HashMap<Long, Set<Long>> mapCategorias) {
		CategoriasMap map = getInstance(empresaId);
		map.hash = mapCategorias;
		return getInstance();
	}
	
	private Set<Long> getCategorias(Long key) {
		return hash.get(key);
	}

	public List<Long> getCategorias(List<Long> keys) {
		Set<Long> list = new HashSet<Long>();
		list.addAll(keys);
		for (Long key : keys) {
			Set<Long> set = hash.get(key);
			if(set != null && !set.isEmpty()) {
				list.addAll(set);
			}
		}
		return new ArrayList<>(list);
	}

	public Set<Long> get(Long key) {
		Set<Long> list = getCategorias(key);
		return list;
	}

	public void put(Long key, Set<Long> value) {
		if(hash != null) {
			Set<Long> list = getCategorias(key);
			if(list != null) {
				list= value;
			} else {
				list = value;
				hash.put(key, list);
			}
		}
	}

	public static Set<Long> getAllEmpresas() {
		if(mapInstance == null) {
			return null;
		}
		return mapInstance.keySet();
	}
	
	public static void clear() {
		if(mapInstance != null) {
			mapInstance.clear();
		}
	}

	public HashMap<Long, Set<Long>> getMap() {
		return hash;
	}
}
