package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.OrigemCadastro;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.vo.GrupoFilter;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.UsuarioAutocompleteFiltro;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LongField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class GruposUsuariosAutoCompletePage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	
	public boolean get;
	public List<GrupoVO> grupos;
	private TextField tNaoAtivo;
	private TextField tGrupoId;
	private TextField tLogin;
	private TextField tFilter;
	
	private UsuarioAutocompleteFiltro filtro;
	
	@Override
	public void onInit() {
		
		filtro = new UsuarioAutocompleteFiltro();
		
		form.setMethod("post");

		TextField tQ = new TextField("q");
		form.add(tQ);
		tQ.setFocus(true);
		form.add(tGrupoId = new TextField("grupoId"));
		form.add(new TextField("not_users_ids"));
		form.add(tNaoAtivo = new TextField("naoAtivo"));
		form.add(new TextField("not_grupo_ids"));
		form.add(new TextField("usuarios"));
		form.add(new TextField("page"));
		form.add(new TextField("maxRows"));
		form.add(new TextField("coluna"));
		form.add(new TextField("ordenacao"));
		form.add(new LongField("cargoId"));
		form.add(new TextField("acesso"));
		form.add(new TextField("origem"));
		form.add(tFilter = new TextField("filter"));
		form.add(tLogin = new TextField("user"));
		form.add(new IntegerField("statusAtivacao"));

		tNaoAtivo.setValue("-1");
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		form.add(new Submit("buscar"));

	}

	@Override
	public void onGet() {
		super.onGet();
		
		get = true;
	}

	@Override
	protected Object execute() throws Exception {
		
		if(form.isValid()) {
			form.copyTo(filtro);
			String filter = tFilter.getValue() != null ? tFilter.getValue() : "all";
			List<UsuarioVO> users = new ArrayList<>();
			List<GrupoVO> grupos = new ArrayList<>();
			
			switch (filter) {
			case "all":
				users = getUsersAutocomplete();
				grupos = getGruposAutocomplete();
				break;
			case "users":
				users = getUsersAutocomplete();
				break;
			case "groups":
				grupos = getGruposAutocomplete();
				break;
			default:
				users = getUsersAutocomplete();
				grupos = getGruposAutocomplete();
				break;
			}
			
			Response r = Response.ok("OK");
			r.users = users;
			r.groups = grupos;
			return r;
		}
		return new MensagemResult("NOK", "Erro ao efetuar busca");
	}

	private List<UsuarioVO> getUsersAutocomplete() {
		List<Long> usuariosIds = getListIds("usuarios");
		filtro.setUsuariosIds(usuariosIds);
		
		List<Long> gruposIds = getListIds("grupoId");
		List<Long> notIds = getListIds("not_users_ids");
		
		filtro.setGruposIds(gruposIds);
		filtro.setNotUsersIds(notIds);
		
		if(filtro.getStatusAtivacao() != null) {
			StatusUsuario statusUsuario = StatusUsuario.values()[Integer.valueOf(filtro.getStatusAtivacao())];
			filtro.setStatusUsuario(statusUsuario);
		}

		if(StringUtils.isNotEmpty(filtro.getOrigem())) {
			OrigemCadastro origemCadastro = OrigemCadastro.valueOf(filtro.getOrigem());
			filtro.setOrigemCadastro(origemCadastro);
		}
		
		filtro.setNome(filtro.getQ());
		
		List<Usuario> list = usuarioService.findByFiltro(filtro, getEmpresa());
		
		List<UsuarioVO> users = UsuarioVO.toList(list, usuarioService, grupoService);
		
		if(users == null) {
			users = new ArrayList<UsuarioVO>();
		}
		return users;
	}
	
	public List<GrupoVO> getGruposAutocomplete() {
		String nome = getContext().getRequestParameter("q");
		String maxRowsParameter = getContext().getRequestParameter("maxRows");

		int maxRows = NumberUtils.toInt(maxRowsParameter);
		String login = tLogin.getValue();
		if(StringUtils.isEmpty(login)) {
			throw new RuntimeException("Informe o parâmetro user_id.");
		}
		// Somente grupos deste usuario
		Usuario user = usuarioService.findByLogin(login, getEmpresa());
		if(user == null) {
			throw new RuntimeException("Usuário não encontrado.");
		}

		GrupoFilter filter = new GrupoFilter();
		filter.nome = nome;
		filter.page = page;
		filter.maxRows = maxRows;
		if(StringUtils.isNotEmpty(tGrupoId.getValue())) {
			filter.id = Long.parseLong(tGrupoId.getValue());
		}
		
		// Meus Grupos
		filter.gruposQueCriei = "1".equals(getContext().getRequestParameter("meus_grupos"));
		// Grupos que participo
		filter.gruposQueParticipo = "1".equals(getContext().getRequestParameter("participo"));
		
		
		// Ao postar, exclui grupos ids que já estão na lista
		filter.notIds = getListIds("not_grupo_ids");
		
		// Retorna somente grupos que o usuario pode ver.
		filter.podePostar = "1".equals(getContext().getRequestParameter("pode_postar"));
		
		List<GrupoUsuarios> meusGrupos = grupoService.findMeusGrupos(filter, user);
		List<Grupo> list = new ArrayList<Grupo>();

		grupos = new ArrayList<GrupoVO>();
		for (GrupoUsuarios gu : meusGrupos) {
			Grupo grupo = gu.getGrupo();
			if(grupo != null) {
				list.add(grupo);
				GrupoVO vo = grupoService.setGrupo(grupo);
				long count = 0L;
				count = grupoService.countUsers(grupo);
				vo.countUsers = count;
				vo.favorito = gu.isFavorito();
				vo.participa = true;
				vo.statusSolicitacao = gu.getStatusParticipacaoString();
				grupos.add(vo);
			}
		}
		
		if(!filter.gruposQueParticipo) {
			List<Grupo> gruposParticipar = grupoService.findGruposParticipar(filter, user, list);
			
			for (Grupo g : gruposParticipar) {
				Grupo grupo = g;
				if(grupo != null) {
					GrupoVO vo = grupoService.setGrupo(grupo);
					long count = 0L;
					count = grupoService.countUsers(grupo);
					vo.countUsers = count;
					vo.favorito = false;
					vo.participa = false;
					vo.statusSolicitacao = "";
					grupos.add(vo);
				}
			}
		}
		
		return grupos;
		
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("list", Response.class);
		super.xstream(x);
	}
}
