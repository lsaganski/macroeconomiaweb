package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.LoginReport;
import net.livetouch.tiger.ddd.DomainException;

public interface LoginReportService extends Service<LoginReport> {

	List<LoginReport> findAll();
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(LoginReport f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(LoginReport f) throws DomainException;
	
}
