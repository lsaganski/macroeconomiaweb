package br.livetouch.livecom.domain.repository.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.MensagemRepository;
import br.livetouch.livecom.domain.repository.StatusMensagemUsuarioRepository;
import br.livetouch.livecom.domain.vo.RelatorioConversaFiltro;
import br.livetouch.livecom.domain.vo.StatusMensagemUsuarioVO;
import br.livetouch.livecom.domain.vo.UpdateStatusMensagemResponseVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.utils.ListUtils;

@SuppressWarnings("unchecked")
@Repository
public class MensagemRepositoryImpl extends LivecomRepository<Mensagem> implements MensagemRepository {

	@Autowired
	private StatusMensagemUsuarioRepository statusMensagemUsuarioRepository;

	public MensagemRepositoryImpl() {
		super(Mensagem.class);
	}

	@Override
	public List<MensagemConversa> findAllConversas() {
		Query q = null;

		StringBuffer sb = new StringBuffer("from MensagemConversa c ");

		q = createQuery(sb.toString());

		q.setCacheable(true);

		List<MensagemConversa> list = q.list();

		return list;
	}

	@Override
	public List<Mensagem> findAllByUser(Usuario userFrom, Usuario userTo, int tipo, int page, int max) {
		Query q = null;

		StringBuffer sb = new StringBuffer("from Mensagem m where tipo=:tipo ");

		if (userFrom != null) {
			sb.append(" and m.from.id = :from");
		}
		if (userTo != null) {
			sb.append(" and m.to.id = :to");
		}

		sb.append(" order by id desc");

		// Query
		q = createQuery(sb.toString());
		q.setParameter("tipo", tipo);

		if (userFrom != null) {
			q.setParameter("from", userFrom.getId());
		}
		if (userTo != null) {
			q.setParameter("to", userTo.getId());
		}

		if (max > 0) {
			int firstResult = page * max;
			q.setFirstResult(firstResult);
			q.setMaxResults(max);
		}

		q.setCacheable(true);
		List<Mensagem> list = q.list();

		return list;
	}

	@Override
	public List<MensagemConversa> findAllConversasByUser(Usuario user, String search, int page, int maxRows) {

		Query q = null;
		
		StringBuffer sb = new StringBuffer("select distinct c from MensagemConversa c ");
		
		sb.append(" left join c.grupo g ");
		sb.append(" left join c.from userFrom ");
		sb.append(" left join c.to to ");

		sb.append("where 1=1 ");

		if (user != null) {

			sb.append("and ( ((userFrom.id = :from and g.id is null) or to.id = :to) ");

			    sb.append(" OR ");
			    sb.append(" (g.id in (select gu.grupo.id from GrupoUsuarios gu where gu.usuario.id = :from) )");

			sb.append(" ) ");
		}

		if (StringUtils.isNotEmpty(search)) {
			sb.append(" and (g.nome like :search or ( ((userFrom.nome like :search and userFrom.id != :from) or (to.nome like :search and to.id != :to))))");
		}

		// LECHETA COMENTOU 10/07
		sb.append(" and c.lastMensagem is not null");

		sb.append(" order by c.dataUpdated desc");

		q = createQuery(sb.toString());

		if (user != null) {
			q.setParameter("from", user.getId());
			q.setParameter("to", user.getId());
		}

		if (org.apache.commons.lang.StringUtils.isNotEmpty(search)) {
			q.setParameter("search", "%" + search + "%");
		}

		if (maxRows > 0) {
			int firstResult = page * maxRows;
			q.setFirstResult(firstResult);
			q.setMaxResults(maxRows);
		}

		q.setCacheable(true);

//		System.err.println("-----------------");
//		System.err.println("Antes -  query com problema");
		List<MensagemConversa> list = q.list();
//		System.err.println("Depois -  query com problema. ela faz muitos SQL");
//		System.err.println("-----------------");
		
		return list;
	}
	
	@Override
	public Long getBadges(Usuario usuario) {
		/**
		 * Grupos
		 */
		long count = 0;
		StringBuffer sb = new StringBuffer();
		sb.append("select distinct count(smu.mensagem.id) from StatusMensagemUsuario smu inner join smu.conversa c ");
		sb.append(" where smu.usuario.id = :userId and smu.excluida = 0 and smu.lida = 0 ");
		sb.append(" AND c.grupo.id in(SELECT gu.grupo.id from  GrupoUsuarios gu where gu.usuario.id = :userId)");
		Query q = createQuery(sb.toString());
		q.setLong("userId", usuario.getId());
		q.setCacheable(true);
		count += getCount(q);

		/**
		 * Usuarios
		 */
		sb = new StringBuffer("select count(m.id) from Mensagem m where m.to.id = :user ");
		sb.append(" and m.lida=0");
		q = createQuery(sb.toString());
		q.setLong("user", usuario.getId());
		q.setCacheable(true);
		count += getCount(q);

		return count;
	}

	@Override
	public Long countMensagensNaoLidas() {
		long count = 0;
		
		/**
		 * Usuarios
		 */
		StringBuffer sb = new StringBuffer("select count(m.id) from Mensagem m where 1=1 ");
		sb.append(" and m.lida=0");
		Query q = createQuery(sb.toString());
		q.setCacheable(true);
		count = getCount(q);
		
		return count;
	}

	@Override
	public List<Long> usersToSilent(Empresa e) {
		/**
		 * Usuarios
		 */
		StringBuffer sb = new StringBuffer("select distinct m.to.id from Mensagem m where 1=1");
		sb.append(" and m.lida=0 and m.to.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setParameter("empresa", e);
		q.setCacheable(true);
		
		return q.list();
	}

	@Override
	public MensagemConversa getMensagemConversaByUser(Usuario userFrom, Usuario userTo) {
		Query q = createQuery("from MensagemConversa c where (c.from.id = ? and c.to.id = ?) or (c.from.id = ? and c.to.id = ?) order by c.id  ");

		q.setLong(0, userFrom.getId());
		q.setLong(1, userTo.getId());

		q.setLong(2, userTo.getId());
		q.setLong(3, userFrom.getId());

		q.setCacheable(true);
		List<MensagemConversa> list = q.list();

		MensagemConversa c = list.size() > 0 ? list.get(0) : null;
		return c;
	}
	
	@Override
	public MensagemConversa getMensagemConversaByUserGrupo(Usuario user, Grupo grupo) {
		Query q = createQuery("from MensagemConversa c where grupo.id=? ");
		q.setLong(0, grupo.getId());
		List<MensagemConversa> list = q.list();
		MensagemConversa c = list.size() > 0 ? list.get(0) : null;
		return c;
	}


	@Override
	public List<Mensagem> getMensagensByConversa(MensagemConversa conversa, Usuario user, int page, int maxRows, boolean orderAsc) {
		List<Mensagem> mensagens = null;
		List<Long> mensagensIds = null;
		StringBuffer sb = new StringBuffer();

		sb.append("from Mensagem m ");

		// sb.append(" inner join fetch m.arquivos arquivo ");

		sb.append(" where m.conversa.id = :conversaId ");

		// Nao excluidas

		if (conversa.getGrupo() == null) {
			sb.append(" and ( (m.from.id = :userId and m.excluida_from = 0) or (m.to.id = :userId and m.excluida_to = 0)) ");
		} else {

			mensagens = statusMensagemUsuarioRepository.findAllMessagesNaoExcluidasUserConversa(user, conversa);
			mensagensIds = Mensagem.getIds(mensagens);

			if (mensagensIds != null && mensagensIds.size() > 0) {
				sb.append("and (m.id in (:mensagensIds) )");
			}
		}

		// Order By
		if (orderAsc) {
			sb.append("order by m.id ");
		} else {
			sb.append("order by m.id desc ");
		}

		Query q = createQuery(sb.toString());

		q.setLong("conversaId", conversa.getId());

		if (conversa.getGrupo() == null) {
			q.setLong("userId", user.getId());
		}

		if (ListUtils.isNotEmpty(mensagensIds)) {
			q.setParameterList("mensagensIds", mensagensIds);
		}

		if (maxRows > 0) {
			int firstResult = page * maxRows;
			q.setFirstResult(firstResult);
			q.setMaxResults(maxRows);
		}
		q.setCacheable(true);

		List<Mensagem> list = q.list();

		return list;
	}

	@Override
	public List<Long> findUsuariosComQuemTemConversa(long userId) {
		Query q = createQuery("select c.from.id,c.to.id from MensagemConversa c where c.from.id = ? or c.to.id=? ");
		q.setLong(0, userId);
		q.setLong(1, userId);
		q.setCacheable(true);
		List<Object[]> list = q.list();

		List<Long> ids = new ArrayList<Long>();
		for (Object[] longs : list) {
			Long idFrom = (Long) longs[0];
			Long idTo = (Long) longs[1];

			Long id = null;

			if (idFrom == userId) {
				id = idTo;
			} else {
				id = idFrom;
			}

			if (id != null) {
				ids.add(id);
			}
		}

		return ids;
	}

	@Override
	public List<Mensagem> findDetalhesByConversa(RelatorioConversaFiltro filtro) {
		StringBuffer sb = new StringBuffer();

		sb.append("from Mensagem m ");

		if (filtro.getId() != null) {
			sb.append(" where m.conversa.id = :conversaId ");
		}

		sb.append("order by m.id desc ");

		Query q = createQuery(sb.toString());

		if (filtro.getId() != null) {
			q.setLong("conversaId", filtro.getId());
		}

		if (filtro.getMax() > 0) {
			int firstResult = filtro.getPage() * filtro.getMax();
			q.setFirstResult(firstResult);
			q.setMaxResults(filtro.getMax());
		}
		q.setCacheable(false);

		return q.list();
	}
	
	@Override
	public List<StatusMensagemUsuarioVO> findDetalhesByMensagem(Long id) {

		List<StatusMensagemUsuarioVO> mensagens = null;
		StringBuffer sb = new StringBuffer();

		sb.append("select new br.livetouch.livecom.domain.vo.StatusMensagemUsuarioVO(s) from StatusMensagemUsuario s ");

		if (id != null) {
			sb.append(" where s.mensagem.id = :id ");
		}

		sb.append("order by lida desc, entregue desc, s.dataLida desc, s.dataEntregue desc ");

		Query q = createQuery(sb.toString());

		if (id != null) {
			q.setLong("id", id);
		}

		q.setCacheable(true);

		mensagens = q.list();

		return mensagens;
	}

	@Override
	public long getCountDetalhesByConversa(RelatorioConversaFiltro filtro) {

		StringBuffer sb = new StringBuffer();

		sb.append("select count(m) from Mensagem m ");

		if (filtro.getId() != null) {
			sb.append(" where m.conversa.id = :conversaId ");
		}

		Query q = createQuery(sb.toString());

		if (filtro.getId() != null) {
			q.setLong("conversaId", filtro.getId());
		}

		Object obj = q.uniqueResult();

		return new Long(obj.toString());
	}

	/**
	 * status: 1 - recebida, 2 lida
	 */
	@Override
	public int updateStatusConversa(Long userId, Long conversaId, Integer status) {
		StringBuffer sb = new StringBuffer("update Mensagem m set ");
		if (status == 1) {
			sb.append("m.entregue = 1, dataEntregue = :data ");
		} else if (status == 2) {
			sb.append("m.lida = 1, dataLida = :data ");
		} else {
			return 0;
		}
		
		// Update Conversa
		sb.append("where m.to.id = :userId AND m.conversa.id = :conversaId ");
		
		if (status == 1) {
			sb.append("and m.entregue != 1 ");
		} else if (status == 2) {
			sb.append("and m.lida != 1");
		}

		Query q = createQuery(sb.toString());
		q.setParameter("userId", userId);
		q.setParameter("conversaId", conversaId);
		q.setParameter("data", new Date());
		int qtdModified = q.executeUpdate();
		return qtdModified;
	}

	/**
	 * status: 1 - recebida, 2 lida
	 */
	@Override
	public int updateStatusMensagem(Long userId, Long mensagemId, Integer status) {
		StringBuffer sb = new StringBuffer("update Mensagem m set ");
		if (status == 1) {
			sb.append("m.entregue = 1, m.dataEntregue = :data");
		} else if (status == 2) {
			// Além de setar como lida
			sb.append("m.lida = 1, m.dataLida = :data");
		} else {
			return 0;
		}
		
		// Update Mensagem
		sb.append(" where m.id = :mensagemId ");
		
		if (status == 1) {
			sb.append("and m.entregue != 1 ");
		} else if (status == 2) {
			sb.append("and m.lida != 1");
		}
		
		Query q = createQuery(sb.toString());
		q.setParameter("mensagemId", mensagemId);
		q.setParameter("data", new Date());
		int qtdModified = q.executeUpdate();
		return qtdModified;
	}
	
	@Override
	public void checkHibernateProxy(Mensagem mensagem) {
		try {
			initialize(mensagem);
			initialize(mensagem.getArquivos());
			initialize(mensagem.getFrom());
			initialize(mensagem.getFrom().getFoto());
			initialize(mensagem.getFrom().getUrlThumb());

			initialize(mensagem.getStatusUsuarios());

			initialize(mensagem.getFrom());
			if (mensagem.getFrom() != null && mensagem.getFrom().getFoto() != null) {
				initialize(mensagem.getFrom().getFoto());
				initialize(mensagem.getFrom().getFoto().getThumbs());
			}

			initialize(mensagem.getTo());
			if (mensagem.getTo() != null && mensagem.getTo().getFoto() != null) {
				initialize(mensagem.getTo().getFoto());
				initialize(mensagem.getTo().getFoto().getThumbs());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean initialize(Object o) {
		if (o != null) {
			boolean is = Hibernate.isInitialized(o);
			if (!is) {
				Hibernate.initialize(o);
			}
			return is;
		}
		return false;
	}

	@Override
	public Long findConversaByMensagem(Long mensagemId) {
		StringBuffer sb = new StringBuffer();
		sb.append("select max(m.id) from Mensagem m ");
		sb.append(" where m.conversa.id  = (select c.id from Mensagem m inner join m.conversa c ");
		sb.append(" where m.id = :mensagemId) ");
		Query q = createQuery(sb.toString());
		q.setParameter("mensagemId", mensagemId);
		return (Long) (q.list().size() > 0 ? q.list().get(0) : null);
	}

	@Override
	public List<Long> findAllIdsConversasByUser(Usuario u) {
		List<Long> list = queryIds("select c.id from MensagemConversa c where c.from.id = ? or c.to.id=?",true,u.getId(),u.getId());
		return list;
	}

	@Override
	public List<Long> findAllMensagensIdsOwnerByUser(Usuario u) {

		// Mensagens do user
		List<Long> list = queryIds("select m.id from Mensagem m where m.from.id = ? or m.to.id=?",true,u.getId(),u.getId());
		
		// Mensagens que estão em algum grupo que o user é admin.
		List<Long> list2 = queryIds("select m.id from Mensagem m where m.conversa.id in (select id from MensagemConversa where grupo.id in (select id from Grupo where userCreate.id=?))",true,u.getId());
		list.addAll(list2);

		return list;
	}

	@Override
	public UpdateStatusMensagemResponseVO countStatusMensagemGrupo(long msgId) {
		String joinGrupos = " AND s.usuario.id in(SELECT gu.usuario.id from  GrupoUsuarios gu where gu.grupo.id = c.grupo.id) ";
		StringBuffer sb = new StringBuffer("select ");
		sb.append("new br.livetouch.livecom.domain.vo.UpdateStatusMensagemResponseVO(");
		sb.append(" (select count(s.id) from StatusMensagemUsuario s inner join s.conversa c where s.mensagem.id=:msgId "+joinGrupos+"), ");
		sb.append(" (select count(s.id) from StatusMensagemUsuario s inner join s.conversa c where s.mensagem.id=:msgId and entregue=1 "+joinGrupos+"), ");
		sb.append(" (select count(s.id) from StatusMensagemUsuario s inner join s.conversa c where s.mensagem.id=:msgId and lida=1 "+joinGrupos+") ");
		sb.append(") from StatusMensagemUsuario s inner join s.conversa c where s.mensagem.id=:msgId ");
		
		Query q = createQuery(sb.toString());
		q.setMaxResults(1);
		q.setParameter("msgId", msgId);
		List<UpdateStatusMensagemResponseVO> list = q.list();
		
		UpdateStatusMensagemResponseVO response = (UpdateStatusMensagemResponseVO) (list.size() > 0 ? list.get(0) : null);
		
		return response;
	}

	@Override
	public List<UsuarioVO> findUsuariosGrupo(Mensagem msg) {
		Query q = createQuery("select s.usuario from StatusMensagemUsuario s where s.mensagem.id=:msgId");
		q.setParameter("msgId", msg.getId());
		List<Usuario> usuarios = q.list();
		List<UsuarioVO> list = UsuarioVO.setUsuarios(usuarios);
		return list;
	}

	@Override
	public List<MensagemConversa> findAllConversasByUser(long userId) {
		Query q = createQuery("from MensagemConversa c where c.from.id = :from or c.to.id = :to");
		q.setParameter("from", userId);
		q.setParameter("to", userId);
		q.setCacheable(true);
		List<MensagemConversa> list = q.list();
		return list;
	}
	
	/**
	 * 
	 * @param usuario
	 * @param lastMsgId
	 * @return
	 */
	@Override
	public List<Mensagem> findMensagensMobile(Usuario usuario, long lastMsgId) {
		StringBuffer sb = new StringBuffer("select m from Mensagem m inner join m.conversa c");
		
		sb.append(" left join m.statusUsuarios status ");
//		// Maiores que lastMsgId ou não lidas.
//		sb.append("where (m.id > :lastMsgId and (m.to.id = :user or status.usuario.id = :user))");
//		sb.append(" or ((m.to.id = :user or status.usuario.id = :user) and ((status.lida is null and m.lida = 0) or (status.lida is not null and status.lida = 0)))");

		sb.append(" where m.id > :lastMsgId ");
		// Join status do usuario
		sb.append(" and ( (m.to.id = :user or m.from.id = :user)  or status.usuario.id = :user )");
		// order by
		sb.append(" order by m.id");

		Query q = createQuery(sb.toString());

		q.setLong("lastMsgId", lastMsgId);
		q.setLong("user", usuario.getId());

		List<Mensagem> list = q.list();

		return list;
	}
	
	/**
	 * 
	 * @param usuario
	 * @param lastMsgId
	 * @return
	 */
	@Override
	public List<Mensagem> findMensagensById(List<Long> msgsIds) {
		if(ListUtils.isEmpty(msgsIds)) {
			return null;
		}
		
		StringBuffer sb = new StringBuffer("select m from Mensagem m ");

		sb.append(" where m.id in (:msgsIds) ");

		// order by
		sb.append(" order by m.id");

		Query q = createQuery(sb.toString());

		q.setParameterList("msgsIds", msgsIds);

		List<Mensagem> list = q.list();

		return list;
	}
	
	@Override
	public Long getBadges(MensagemConversa c, Usuario user) {
		StringBuffer sb = new StringBuffer("SELECT count(m.id) FROM Mensagem m ");
		sb.append(" left join m.statusUsuarios status ");
		sb.append(" WHERE m.conversa.id = :conversaId ");
		sb.append(" AND (");
			sb.append("(m.to.id = :userId AND m.lida = 0)");
			sb.append(" OR ");
			sb.append("( status.usuario.id = :userId AND status.conversa.id = :conversaId AND status.lida = 0)");
		sb.append(")");
		Query q = createQuery(sb.toString());
		q.setParameter("conversaId", c.getId());
		q.setParameter("userId", user.getId());
		q.setCacheable(true);
		return (Long) (q.list().size() > 0 ? q.list().get(0) : 0);
	}

	@Override
	public List<Mensagem> findMensagensCallback(MensagemConversa conversa, Usuario usuario, Long lastMsgId) {
		boolean isGroup = conversa != null ? conversa.isGrupo() : false;
		StringBuffer sb = new StringBuffer("select m from Mensagem m ");
		if(isGroup && usuario != null) {
			sb.append(" left join m.statusUsuarios status");
		}
		
		sb.append(" where 1=1 ");
		
		if(conversa != null) {
			sb.append(" and m.conversa.id = :conversaId ");
		}

		// Apenas as que o mobile nao recebeu.
		if(lastMsgId != null && lastMsgId > 0) {
			sb.append(" and m.id > :lastMsgId ");
		}
		
		if(isGroup && usuario != null) {
			sb.append(" and status.usuario.id = :user and status.lida = 0");
		} else if(usuario != null) {
			/**
			 * Aqui é from
			 */
			sb.append(" and m.from.id = :user");
		}
		
		// nao lida
		sb.append(" and ( (m.entregue=1 and m.callbackCheck2Cinza=0) or (m.lida=1 and m.callbackCheck2Azul=0) ) ");

		// order by
		sb.append(" order by m.id");

		Query q = createQuery(sb.toString());

		if(lastMsgId != null && lastMsgId > 0) {
			q.setLong("lastMsgId", lastMsgId);
		}
		if(conversa != null) {
			q.setLong("conversaId", conversa.getId());
		}
		if(usuario != null) {
			q.setLong("user", usuario.getId());
		}

//		q.setCacheable(true);

		List<Mensagem> list = q.list();

		return list;
	}

	@Override
	public boolean updateDataPushMsg(Long msgId) {
		Query q = createQuery("update Mensagem m set m.dataPush = :data where m.id = :msgId ");
		q.setParameter("msgId", msgId);
		q.setParameter("data", new Date());
		int qtdModified = q.executeUpdate();
		return qtdModified > 0;
	}

	@Override
	public boolean updateCallbackMensagem(long conversaId,Long msgId,boolean callback1cinza, boolean callback2cinza, boolean callback2azul) {
		StringBuffer sb = new StringBuffer("update Mensagem m set m.dataUpdated = :data ");
		if(callback1cinza) {
			sb.append(", m.callbackCheck1Cinza=1");
		}
		if(callback2cinza) {
			sb.append(", m.callbackCheck1Cinza=1, m.callbackCheck2Cinza=1");
		}
		if(callback2azul) {
			sb.append(", m.callbackCheck1Cinza=1, m.callbackCheck2Cinza=1, m.callbackCheck2Azul=1");
		}

		if(msgId > 0) {
			// Atualiza o status de uma msg individual
			sb.append(" where m.id = :msgId");
		} else if(conversaId > 0) {
			// Atualiza o status de todas as msgs desta conversa.
			sb.append(" where m.conversa.id = :conversaId");
		}
		
		Query q = createQuery(sb.toString());
		if(msgId > 0) {
			q.setParameter("msgId", msgId);
		}else if(conversaId > 0) {
			q.setParameter("conversaId", conversaId);
		}
		q.setParameter("data", new Date());
		int qtdModified = q.executeUpdate();
		return qtdModified > 0;
	}

	@Override
	public Mensagem findByIdentifier(Long conversaId, Long idUserFrom, Long identifier) {
		Query q = createQuery("from Mensagem m where m.conversa.id = ? and m.from.id = ? and m.identifier = ?");
		q.setParameter(0, conversaId);
		q.setParameter(1, idUserFrom);
		q.setParameter(2, identifier);
		q.setCacheable(true);
		List<Mensagem> list = q.list();
		Mensagem msg = list.isEmpty() ? null : list.get(0);
		return msg;
	}

	@Override
	public List<Mensagem> findMensagensNaoLidas(MensagemConversa c) {

		StringBuffer sb = new StringBuffer("select m from Mensagem m ");

		sb.append(" where m.conversa.id = :conversaId ");
		
		// Nao precisa ir no Status Mensagem porque atualizamos na Conversa.
		sb.append(" and m.lida=0");

		// order by
		sb.append(" order by m.id");

		Query q = createQuery(sb.toString());
		
		q.setLong("conversaId", c.getId());

		List<Mensagem> list = q.list();

		return list;
	
	}

	@Override
	public void forward(Mensagem msg, MensagemConversa c) {
		msg.setId(null);
		
		Mensagem msg2 = new Mensagem();
		try {
			BeanUtils.copyProperties(msg2, msg);
		
//			saveOrUpdate(msg1);
			
			// 
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		// tira da session
		getSession().evict(msg);
	
		
	}

}