package br.livetouch.livecom.web.pages.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.LogTransacao;
import br.livetouch.livecom.web.pages.report.RelatorioPage;



@Controller
@Scope("prototype")
public class DetalhesLogTransacaoPage extends RelatorioPage {

	public Long id;
	public LogTransacao log;		

	@Override
	public void onGet() {
		log = logService.getLogTransacao(id);
	}
}
