package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Chapeu;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.repository.ChapeuRepository;
import br.livetouch.livecom.domain.vo.ChapeuFilter;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class ChapeuRepositoryImpl extends StringHibernateRepository<Chapeu>
		implements ChapeuRepository {

	public ChapeuRepositoryImpl() {
		super(Chapeu.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Chapeu> findAllByFilter(ChapeuFilter filter) {
		StringBuffer sb = new StringBuffer();

		sb.append("select c from Chapeu c ");

		sb.append(" where 1=1 ");

		if (StringUtils.isNotEmpty(filter.nome)) {
			sb.append(" and (c.nome like :nome)");
		}

		if (filter.categoria != null) {
			sb.append(" and c.categoria = :categ)");
		}

		if (filter.notIds != null && filter.notIds.size() > 0) {
			sb.append(" and c.id not in (:notIds) ");
		}

		sb.append(" order by c.nome ");

		Query query = createQuery(sb.toString());
		if (StringUtils.isNotEmpty(filter.nome)) {
			query.setParameter("nome", "%" + filter.nome + "%");
		}
		if (filter.notIds != null && filter.notIds.size() > 0) {
			query.setParameterList("notIds", filter.notIds);
		}
		if (filter.categoria != null) {
			query.setParameter("categ", filter.categoria);
		}

		query.setCacheable(true);

		query.setMaxResults(filter.maxRows);;

		List<Chapeu> chapeus = query.list();

		return chapeus;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Chapeu findByName(Chapeu chapeu, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Chapeu c where c.nome = :nome");
//		sb.append(" and c.empresa = :empresa");
		
		if(chapeu.getId() != null) {
			sb.append(" and c.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
//		q.setParameter("empresa", empresa);
		q.setParameter("nome", chapeu.getNome());
		
		if(chapeu.getId() != null) {
			q.setParameter("id", chapeu.getId());
		}
		
		List<Chapeu> list = q.list();
		Chapeu c = (Chapeu) (list.size() > 0 ? q.list().get(0) : null);
		return c;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Chapeu> findAll() {
		StringBuffer sb = new StringBuffer("from Chapeu c order by c.ordem");
		
		Query q = createQuery(sb.toString());
		
		List<Chapeu> list = q.list();
		return list;
	}

}