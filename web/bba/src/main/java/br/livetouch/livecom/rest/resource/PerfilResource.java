package br.livetouch.livecom.rest.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;

import br.infra.util.Log;
import br.infra.util.TempExceptionUtils;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.vo.PerfilPermissaoVO;
import br.livetouch.livecom.domain.vo.PerfilVO;
import br.livetouch.livecom.domain.vo.PermissaoCategoriaVO;
import br.livetouch.livecom.domain.vo.PermissaoVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/perfil")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class PerfilResource extends MainResource {
	protected static final Logger log = Log.getLogger(PerfilResource.class);
	
	@POST
	public Response create(Perfil perfil) {
		
		Response mesageResult = existe(perfil);
		if(mesageResult != null){
			return mesageResult;
		}
		
		try {
			perfilService.saveOrUpdate(perfil, perfil.getEmpresa());
			perfilService.savePermissoes(perfil, perfil.getPermissoes());
			PerfilVO perfilVO = new PerfilVO();
			perfilVO.setPerfil(perfil);
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", perfilVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel realizar o cadastro do perfil " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel realizar o cadastro do perfil ")).build();
		}
	}
	
	@PUT
	public Response update(Perfil perfil) {
		
		if(perfil == null || perfil.getId() == null) {
			return Response.ok(MessageResult.error("Perfil inexistente")).build();
		}
		Response mesageResult = existe(perfil);
		if(mesageResult != null){
			return mesageResult;
		}
		
		perfilService.removePermissoes(perfil);
		perfilService.savePermissoes(perfil, perfil.getPermissoes());
		
		try {
			perfilService.saveOrUpdate(perfil, perfil.getEmpresa());
			PerfilVO perfilVO = new PerfilVO();
			perfilVO.setPerfil(perfil);
			return Response.ok(MessageResult.ok("Perfil atualizado com sucesso", perfilVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel atualizar o cadastro do perfil " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar o cadastro do perfil ")).build();
		}
	}
	
	@PUT
	@Path("/old")
	public Response updateOld(Perfil perfil) {
		if(perfil == null || perfil.getId() == null) {
			return Response.ok(MessageResult.error("Perfil inexistente")).build();
		}
		Response mesageResult = existe(perfil);
		if(mesageResult != null){
			return mesageResult;
		}
		
		try {
			Perfil old = perfilService.get(perfil.getId());
			
			//perfil
			old.setPublicar(perfil.isPublicar());
			old.setExcluirPostagem(perfil.isExcluirPostagem());
			old.setEditarPostagem(perfil.isEditarPostagem());
			old.setComentar(perfil.isComentar());
			old.setVisualizarComentario(perfil.isVisualizarComentario());
			old.setCurtir(perfil.isCurtir());
			old.setEnviarMensagem(perfil.isEnviarMensagem());
			old.setArquivos(perfil.isArquivos());
			old.setCadastrarUsuarios(perfil.isCadastrarUsuarios());
			old.setCodigo(perfil.isCodigo());
			old.setPadrao(perfil.isPadrao());
			old.setCadastrarTabelas(perfil.isCadastrarTabelas());
			old.setVisualizarRelatorio(perfil.isVisualizarRelatorio());
			
			old.setNome(perfil.getNome());
			old.setDescricao(perfil.getDescricao());
			perfilService.saveOrUpdate(old, getEmpresa());
			PerfilVO perfilVO = new PerfilVO();
			perfilVO.setPerfil(perfil);
			return Response.ok(MessageResult.ok("Perfil atualizado com sucesso", perfilVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel atualizar o cadastro do perfil " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar o cadastro do perfil ")).build();
		}
	}
	
	@GET
	public Response findAll() {
		
		Long empresaId = getUserInfoVO().getEmpresaId();
		if(empresaId != null) {
			Empresa empresa = empresaService.get(empresaId);
			List<Perfil> perfis = perfilService.findAll(empresa);
			List<PerfilVO> vos = PerfilVO.fromList(perfis);
			return Response.ok().entity(vos).build();
		}
		return Response.ok(MessageResult.error("Perfis não encontrados")).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		Perfil perfil = perfilService.get(id);
		if(perfil == null) {
			return Response.ok(MessageResult.error("Perfil não encontrado")).build();
		}
		
		List<PerfilPermissaoVO> permissoes = permissaoService.findByPerfil(perfil);
		PerfilVO perfilVO = new PerfilVO(perfil);
		perfilVO.setMapPermissoesVO(permissoes);
		return Response.ok().entity(perfilVO).build();
	}

	@GET
	@Path("/categoria/{id}")
	public Response findGroup(@PathParam("id") Long id) {
		
		Perfil perfil = perfilService.get(id);
		if(perfil == null) {
			return Response.ok(MessageResult.error("Perfil não encontrado")).build();
		}
		
		List<PerfilPermissaoVO> permissoes = permissaoService.findByPerfil(perfil);
		
		Map<String, List<PerfilPermissaoVO>> map = new HashMap<String, List<PerfilPermissaoVO>>();
		for (PerfilPermissaoVO pp : permissoes) {
			PermissaoVO permissao = pp.getPermissao();
			PermissaoCategoriaVO categoria = permissao.getCategoria();
			
			if(permissao != null && categoria != null) {
				
				List<PerfilPermissaoVO> list = map.get(categoria.codigo);
				if(list == null) {
					list = new ArrayList<PerfilPermissaoVO>();
					list.add(pp);
				} else {
					list.add(pp);
				}
				map.put(categoria.codigo, list);
			}
		}
		
		return Response.ok().entity(map).build();
	}
	
	@PUT
	@Path("/default/{id}")
	public Response update(@PathParam("id") Long id) {
		
		if(id == null) {
			return Response.ok(MessageResult.error("Perfil inexistente")).build();
		}
		try {
			
			String perfilAdmin = ParametrosMap.getInstance(1L).get(Params.PERFIL_ADMIN_PADRAO, "Admin");
			String perfilUsuario = ParametrosMap.getInstance(1L).get(Params.PERFIL_USER_PADRAO, "Usuário");
			
			Empresa empresa = empresaService.get(id);
			if(empresa == null) {
				return Response.ok(MessageResult.error("Perfil inexistente")).build();
			}
			
			Perfil admin = perfilService.findByNome(perfilAdmin + " " + empresa.getNome(), empresa);
			if(admin == null) {
				admin = perfilService.createPerfil(perfilAdmin, empresa);
			}else {
				perfilService.setMenusDefault(admin, perfilAdmin, empresa);
			}
			
			Perfil user = perfilService.findByNome(perfilUsuario + " " + empresa.getNome(), empresa);
			
			Response mesageResult = existe(user);
			if(mesageResult != null){
				return mesageResult;
			}
			
			if(user == null) {
				perfilService.createPerfil(perfilUsuario, empresa);
			}else {
				perfilService.setMenusDefault(user, perfilUsuario, empresa);
			}
			
			return Response.ok(MessageResult.ok("Perfil atualizado com as permissões padrões")).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel atualizar as permissões do perfil " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar as permissões do perfil ")).build();
		}
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		
		Perfil perfil = perfilService.get(id);
		if(perfil == null) {
			return Response.ok(MessageResult.error("Perfil não encontrado")).build();
		}
		try {
			perfilService.delete(perfil, getUserInfo());
			return Response.ok().entity(MessageResult.ok("Perfil deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error("Não foi possível excluir o perfil id: " + id + " ERRO:" + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o perfil  " + id)).build();
		} catch (DataIntegrityViolationException e) {
			log.error(e.getMessage(), e);
			if(TempExceptionUtils.isErrorContraint(e)) {
				return Response.ok(MessageResult.error("Não foi possível excluir o perfil " + id + " porque ele possui relacionamentos.")).build();
			}
			return Response.ok(MessageResult.error("Não foi possível excluir o perfil  " + id + " porque ele possui relacionamentos.")).build();
		} catch (Exception e) {
			log.error("Não foi possível excluir o perfil id: " + id + " ERRO:" + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o perfil  " + id)).build();
		}
	}
	
	public Response existe(Perfil perfil) {
		
		if(perfilService.findByNomeValid(perfil, perfil.getEmpresa()) != null) {
			log.debug("Ja existe um perfil com este nome");
			return Response.ok(MessageResult.error("Ja existe um perfil com este nome")).build();
		}

		return null;
	}
	
	
}
