package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Complemento;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.repository.ComplementoRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class ComplementoRepositoryImpl extends StringHibernateRepository<Complemento> implements ComplementoRepository {

	public ComplementoRepositoryImpl() {
		super(Complemento.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Complemento> findAll(Empresa empresa) {
		Query q = createQuery("FROM Complemento c WHERE c.empresa.id = :empresaId");
		q.setLong("empresaId", empresa.getId());
		q.setCacheable(true);
		return q.list();
	}

}