package br.livetouch.livecom.utils;

import java.io.ByteArrayInputStream;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;

public class HelloS3 {

	public static void main(String[] args) {
		// properties
		String accessKey = "AKIAIFLNTEN23RVG67OQ";
		String secretKey = "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";
		String bucket = "livetouch-temp";
		String dir = "Einstein";

		String fileName = "nome.txt";
		String contentType = "text/plain";
		String texto = "Ricardo";
		byte bytes[] = texto.getBytes();
		
		String path = dir + "/" + fileName; 

		AWSCredentials credentials = new BasicAWSCredentials(accessKey,secretKey);
		AmazonS3 conn = new AmazonS3Client(credentials);

		ObjectMetadata metadata = new ObjectMetadata();
		if(contentType != null) {
			metadata.setContentType(contentType);
			metadata.setContentLength(bytes != null ? bytes.length : 0);
		}

		if(bytes != null) {
			conn.putObject(bucket, path, new ByteArrayInputStream(bytes), metadata);
		} else {
			conn.putObject(bucket, path, new ByteArrayInputStream(new byte[]{}),metadata);
		}
		
		conn.setObjectAcl(bucket, path, CannedAccessControlList.PublicRead);
		
		String url = "https://s3-sa-east-1.amazonaws.com/"+bucket+"/"+path;
		
		System.out.println(url);
	}
}
