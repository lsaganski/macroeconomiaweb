package br.livetouch.livecom.rest.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Expediente;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.vo.ExpedienteVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/expediente")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class ExpedienteResource extends MainResource {
	protected static final Logger log = Log.getLogger(ExpedienteResource.class);
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		Expediente expediente = expedienteService.get(id);
		if(expediente == null) {
			return Response.ok(MessageResult.error("Expediente não encontrado")).build();
		}
		ExpedienteVO vo = new ExpedienteVO(expediente);
		return Response.ok().entity(vo).build();
	}
	
	@POST
	public Response create(Expediente expediente) {

		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			//Validar se ja existe um expediente com o mesmo codigo
			boolean existe = expedienteService.findByCodigo(expediente, getEmpresa()) != null;
			if(existe) {
				log.debug("Ja existe um expediente com este codigo");
				return Response.ok(MessageResult.error("Ja existe um expediente com este codigo")).build();
			}
			
			expedienteService.saveOrUpdate(expediente);
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", new ExpedienteVO(expediente))).build();
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel efetuar o cadastro do expediente")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@PUT
	public Response update(Expediente expediente) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			//Validar se ja existe um expediente com o mesmo codigo
			boolean existe = expedienteService.findByCodigo(expediente, getEmpresa()) != null;
			if(existe) {
				log.debug("Ja existe um expediente com este codigo");
				return Response.ok(MessageResult.error("Ja existe um expediente com este codigo")).build();
			}
			
			expedienteService.saveOrUpdate(expediente);
			return Response.ok(MessageResult.ok("Expediente atualizado com sucesso", new ExpedienteVO(expediente))).build();
		}  catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar o expediente")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@GET
	public Response findAll() {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		List<Expediente> expedientes = expedienteService.findAll();
		List<ExpedienteVO> vos = ExpedienteVO.fromList(expedientes);
		return Response.ok().entity(vos).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Expediente expediente = expedienteService.get(id);
			if(expediente == null) {
				return Response.ok(MessageResult.error("Expediente não encontrado")).build();
			}
			expedienteService.delete(expediente);
			return Response.ok().entity(MessageResult.ok("Expediente deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Expediente " + id)).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Expediente " + id)).build();
		}
	}
	
}
