package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.email.EmailUtils;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextArea;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class EnviarEmailPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private TextField tFrom;
	private TextField tTo;
	private TextField tSubject;
	private TextArea tMsg;
	private Checkbox tHtml;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		tMode = new TextField("mode");
		tFrom = new TextField("from", true);
		tTo = new TextField("to", true);
		tSubject = new TextField("subject", true);
		tMsg = new TextArea("msg", true);
		tHtml = new Checkbox("html", true);

		form.add(tMode);
		form.add(tFrom);
		form.add(tTo);
		form.add(tSubject);
		form.add(tMsg);
		form.add(tHtml);

		tMode.setValue("json");

		form.add(new Submit("Enviar"));

	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			
			String from = tFrom.getValue();
			String to = tTo.getValue();
			String subject = tSubject.getValue();
			String msg = tMsg.getValue();
			boolean html = tHtml.isChecked();

			if (EmailUtils.enviar(from, to, subject, msg, html)) {
				return new MensagemResult("OK", "Email enviado! [" + msg + "]");
			} else {
				return new MensagemResult("NOK", "Erro ao enviar email");
			}

		}

		return new MensagemResult("NOK", "Preencha os campos.");
	}

	@Override
	protected void xstream(XStream x) {
		super.xstream(x);
	}
}
