package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.PostTask;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.DateUtils;

public class PostTaskVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Long id;
	public Long usuarioId;
	public String usuarioNome;
	public Long postId;
	public String expiracao;
	public Boolean notification;

	public PostTaskVO() {
		
	}

	public PostTaskVO(PostTask pt) {
		id = pt.getId();
		Usuario usuario = pt.getUsuario();
		if(usuario != null) {
			usuarioId = usuario.getId();
			usuarioNome = usuario.getNome();
		}
		postId = pt.getPost().getId();
		expiracao = pt.getExpiracao() != null ? DateUtils.toString(pt.getExpiracao(), "dd/MM/yyyy HH:mm") : "";
		notification = pt.isNotification();
	}

	public static List<PostTaskVO> fromList(List<PostTask> postTasks) {
		List<PostTaskVO> vos = new ArrayList<>();
		if(postTasks == null) {
			return vos;
		}
		for (PostTask p : postTasks) {
			PostTaskVO postTaskVO = new PostTaskVO(p);
			vos.add(postTaskVO);
		}
		return vos;
	}

}
