package br.livetouch.livecom.domain.enums;

public enum AuditoriaEntidade {
	USUARIO("Usuario"),
	ARQUIVO("Arquivo"), 
	POST("Post"),
	CATEGORIA("Categoria"),
	TAG("Tag"), 
	COMENTARIO("Comentario"),
	GRUPO("Grupo"),
	AREA("Area"),
	DIRETORIA("Diretoria"),
	CARGO("Cargo"), 
	IMPORTACAO("Importação");

	private final String label;
	AuditoriaEntidade(String label){
		this.label = label;
	}

	@Override
	public String toString() {
		return label != null ? label : "?";
	}
}
