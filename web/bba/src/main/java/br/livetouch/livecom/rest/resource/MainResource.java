package br.livetouch.livecom.rest.resource;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.PostDestaqueRepository;
import br.livetouch.livecom.domain.service.AreaService;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.CampoService;
import br.livetouch.livecom.domain.service.CategoriaIdiomaService;
import br.livetouch.livecom.domain.service.CategoriaPostService;
import br.livetouch.livecom.domain.service.ChapeuService;
import br.livetouch.livecom.domain.service.CidadeService;
import br.livetouch.livecom.domain.service.ConversaNotificationService;
import br.livetouch.livecom.domain.service.DiretoriaService;
import br.livetouch.livecom.domain.service.EmailReportService;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.ExpedienteService;
import br.livetouch.livecom.domain.service.FavoritoService;
import br.livetouch.livecom.domain.service.FeriadoService;
import br.livetouch.livecom.domain.service.FuncaoService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.IdiomaService;
import br.livetouch.livecom.domain.service.LikeService;
import br.livetouch.livecom.domain.service.LogAuditoriaService;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.domain.service.MensagemService;
import br.livetouch.livecom.domain.service.MenuPerfilService;
import br.livetouch.livecom.domain.service.MenuService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.OperacoesCieloService;
import br.livetouch.livecom.domain.service.ParametroService;
import br.livetouch.livecom.domain.service.PerfilService;
import br.livetouch.livecom.domain.service.PermissaoCategoriaService;
import br.livetouch.livecom.domain.service.PermissaoService;
import br.livetouch.livecom.domain.service.PostIdiomaService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.PostTaskService;
import br.livetouch.livecom.domain.service.PushReportService;
import br.livetouch.livecom.domain.service.ReportService;
import br.livetouch.livecom.domain.service.StatusPostService;
import br.livetouch.livecom.domain.service.TagService;
import br.livetouch.livecom.domain.service.TemplateEmailService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import net.sf.click.util.FlashAttribute;

@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class MainResource {
	
	@Autowired
	protected MensagemService mensagemService;
	
	@Autowired
	protected UsuarioService usuarioService;
	
	@Autowired
	protected PostService postService;

	@Autowired
	protected PostIdiomaService postIdiomaService;
	
	@Autowired
	protected TagService tagService;

	@Autowired
	protected FavoritoService favoritoService;

	@Autowired
	protected LikeService likeService;
	
	@Autowired
	protected LogService logService;
	
	@Autowired
	protected ReportService reportService;
	
	@Context
	protected HttpServletRequest req;
	
	@Autowired
	protected AreaService areaService;

	@Autowired
	protected StatusPostService statusPostService;

	@Autowired
	protected IdiomaService idiomaService;

	@Autowired
	protected CategoriaIdiomaService categoriaIdiomaService;
	
	@Autowired
	protected DiretoriaService diretoriaService;
	
	@Autowired
	protected CategoriaPostService categoriaPostService;
	
	@Autowired
	protected GrupoService grupoService;
	
	@Autowired
	protected MenuService menuService;
	
	@Autowired
	protected EmpresaService empresaService;
	
	@Autowired
	protected ArquivoService arquivoService;
	
	@Autowired
	protected PerfilService perfilService;

	@Autowired
	protected PermissaoService permissaoService;

	@Autowired
	protected PermissaoCategoriaService permissaoCategoriaService;
	
	@Autowired
	protected ParametroService parametroService;
	
	@Autowired
	protected PushReportService pushReportService;
	
	@Autowired
	protected FeriadoService feriadoService;
	
	@Autowired
	protected FuncaoService funcaoService;
	
	@Autowired
	protected OperacoesCieloService transacaoService;
	
	@Autowired
	protected CidadeService cidadeService;

	@Autowired
	protected NotificationService notificationService;
	
	@Autowired
	protected ChapeuService chapeuService;

	@Autowired
	protected PostTaskService postTaskService;

	@Autowired
	protected CampoService campoService;

	@Autowired
	protected ExpedienteService expedienteService;
	
	@Autowired
	protected LogAuditoriaService auditoriaService;
	
	@Autowired
	protected TemplateEmailService templateEmailService;

	@Autowired
	protected EmailReportService emailReportService;
	
	@Autowired
	protected PostDestaqueRepository postDestaqueRepository;

	@Autowired
	protected MenuPerfilService menuPerfilService;
	
	@Autowired
	protected ConversaNotificationService conversaNotificationService;
	
	@Autowired
	protected ApplicationContext applicationContext;
	
	protected HttpSession getSession() {
		HttpSession session = req.getSession();
		if(session == null) {
			return null;
		}
		return session;
	}
	
	protected void setAttributeSession(String name, Object attribute) {
		HttpSession session = getSession();
		session.setAttribute(name, attribute);
	}
	
	protected void setMsg(Object attribute) {
		HttpSession session = getSession();
		session.setAttribute("msg", new FlashAttribute(attribute));
	}
	
	protected void setError(Object attribute) {
		HttpSession session = getSession();
		session.setAttribute("msgError", new FlashAttribute(attribute));
	}
	
	protected UserInfoVO getUserInfoVO() {
		HttpSession session = getSession();
		if(session == null) {
			return null;
		}
		
		UserInfoVO userInfo = (UserInfoVO) session.getAttribute(UserInfoVO.USER_INFO_KEY);
		if(userInfo == null) {
			userInfo = getUserInfoByWsToken();
		}
		return userInfo;
	}
	
	protected Usuario getUserInfo() {
		if(usuarioService == null) {
			return null;
		}
		UserInfoVO info = getUserInfoVO();
		if (info == null) {
			return null;
		}
		Usuario u = usuarioService.get(info.getId());
		return u;
	}
	
	protected Empresa getEmpresa() {
		
		UserInfoVO userInfoVo = getUserInfoVO();
		if(userInfoVo != null) {
			Long empresaId = userInfoVo.getEmpresaId();
			return empresaService.get(empresaId);
		}
		
		Usuario userInfo = getUserInfo();
		Empresa e = userInfo != null ? userInfo.getEmpresa() : null;

		if(e == null) {
			Usuario u = getUsuarioRequest();
			if(u != null) {
				e = u.getEmpresa();
			} else {
				e = empresaService.get(1L);
			}
		}

		if (e == null) {
			List<Empresa> list = empresaService.findAll();
			e = list.size() > 0 ? list.get(0) : null;
		}
		return e;
	}

	protected List<Long> getListIds(String s) {
		Set<Long> ids = new HashSet<Long>();
		String[] split = StringUtils.split(s, ",");
		if (split != null) {
			for (String id : split) {
				id = StringUtils.trim(id);
				if (NumberUtils.isNumber(id)) {
					ids.add(new Long(id));
				}
			}
		}
		return new ArrayList<Long>(ids);
	}

	protected Usuario getUsuarioRequest() {
		String id = req.getParameter("user_id");
		if(id == null) {
			id = req.getParameter("user");
		}

		if(id == null) {
			return null;
		}
		Usuario usuario = null;
		if(StringUtils.isNotEmpty(id)) {
			if(StringUtils.isNumeric(id)) {
				usuario = usuarioService.get(Long.parseLong(id));
			} else {
				usuario = usuarioService.findByLogin(id);
			}
		}

		return usuario;
	}
	
	protected boolean hasPermissao(String permissao) {
		UserInfoVO user = getUserInfoVO();
		if(user != null) {
			return user.isPermissao(permissao);
		}
		
		return false;
	}

	protected boolean hasPermissoes(boolean checkAll, String... permissoes) {
		UserInfoVO user = getUserInfoVO();
		if(user != null) {
			if(checkAll) {
				for (String permissao : permissoes) {
					if(!user.isPermissao(permissao)) {
						return false;
					}
				}
				return true;
			} else {
				for (String permissao : permissoes) {
					if(user.isPermissao(permissao)) {
						return true;
					}
				}
				return false;
			}
		}
		
		return false;
	}

	public UserInfoVO getUserInfoByWsToken() {
		String wstoken = getWsToken();
		return Livecom.getInstance().getUserInfoByWsToken(wstoken, null);
	}

	protected String getWsToken() {
		String tokenStr = req.getHeader("Authorization");
		if(org.apache.commons.lang3.StringUtils.isEmpty(tokenStr)){
			tokenStr = req.getParameter("wstoken");
		}
		if(org.apache.commons.lang3.StringUtils.isEmpty(tokenStr)){
			return null;
		}
		return tokenStr;
	}
	
	/**
	 * 2. Referência direta a objetos internos da aplicação
	 * 
	 * A variável user_id está vulnerável a ataques do tipo Referência Insegura e Direta a Objeto, 
	 * o que permite que, ao alterar o seu valor, seja possível acessar dados de outros 
	 * usuários sem a devida autorização.
	 * 
	 */
	protected boolean validateUserIdWithUserInfoToken(Long userId) {
		UserInfoVO userInfo = getUserInfoByWsToken();
		if(userInfo == null) {
			// Token não encontrado
			return false;
		}
		
		if(userId == null ) {
			return false;
		}
		
		boolean ok = userInfo.getId().equals(userId);
		
		return ok;
	}

	protected void setUserInfoVO(UserInfoVO vo) {
			HttpSession session = getSession();
			session.setAttribute(UserInfoVO.USER_INFO_KEY, vo);
	}
}
