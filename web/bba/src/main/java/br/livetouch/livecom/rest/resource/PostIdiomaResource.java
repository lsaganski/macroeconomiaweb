package br.livetouch.livecom.rest.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.PostIdioma;
import br.livetouch.livecom.domain.vo.PostIdiomaVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/postIdioma")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class PostIdiomaResource extends MainResource {
	protected static final Logger log = Log.getLogger(PostIdiomaResource.class);
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		PostIdioma idioma = postIdiomaService.get(id);
		if(idioma == null) {
			return Response.ok(MessageResult.error("Idioma não encontrado")).build();
		}
		PostIdiomaVO vo = new PostIdiomaVO(idioma);
		return Response.ok().entity(vo).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			PostIdioma idioma = postIdiomaService.get(id);
			if(idioma == null) {
				return Response.ok(MessageResult.error("Idioma não encontrado")).build();
			}
			postIdiomaService.delete(idioma);
			return Response.ok().entity(MessageResult.ok("Idioma deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error(e.getMessage())).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Idioma  " + id)).build();
		}
	}
	
}
