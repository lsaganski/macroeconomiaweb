package br.livetouch.livecom.rest.request;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.enums.TipoParametro;

public class ConfigMobileRequest {
	public boolean loginAutomatico;
	public boolean permiteLogout;
	public boolean podePostar;
	public boolean chatOn;
	public boolean showWhatsNews;
	public boolean showAudienciaPost;
	public boolean optionsMenu;
	public boolean menuCategoria;
    public boolean loginOffline;
    public boolean loginByPass;
	
	
	public List<Parametro> toParametros() {
		List<Parametro> parametros = new ArrayList<Parametro>();
		Parametro param = new Parametro();
		param.setNome(Params.APP_LOGIN_AUTOMATICO);
		param.setValor(isBoolean(loginAutomatico));
		param.setTipoParametro(TipoParametro.MOBILE);
		parametros.add(param);
		
		
		param = new Parametro();
		param.setNome(Params.APP_PERMITE_LOGOUT);
		param.setValor(isBoolean(permiteLogout));
		param.setTipoParametro(TipoParametro.MOBILE);
		parametros.add(param);
		
		param = new Parametro();
		param.setNome(Params.APP_PODE_POSTAR);
		param.setValor(isBoolean(podePostar));
		param.setTipoParametro(TipoParametro.MOBILE);
		parametros.add(param);
		
		
		param = new Parametro();
		param.setNome(Params.APP_CHAT_ON);
		param.setValor(isBoolean(chatOn));
		param.setTipoParametro(TipoParametro.MOBILE);
		parametros.add(param);
		
		
		param = new Parametro();
		param.setNome(Params.APP_SHOW_WHATS_NEWS);
		param.setValor(isBoolean(showWhatsNews));
		param.setTipoParametro(TipoParametro.MOBILE);
		parametros.add(param);

		
		param = new Parametro();
		param.setNome(Params.APP_SHOW_AUDIENCIA_POST);
		param.setValor(isBoolean(showAudienciaPost));
		param.setTipoParametro(TipoParametro.MOBILE);
		parametros.add(param);
		
		param = new Parametro();
		param.setNome(Params.APP_OPTIONS_MENU);
		param.setValor(isBoolean(optionsMenu));
		param.setTipoParametro(TipoParametro.MOBILE);
		parametros.add(param);
		
		param = new Parametro();
		param.setNome(Params.APP_MENU_CATEGORIA);
		param.setValor(isBoolean(menuCategoria));
		param.setTipoParametro(TipoParametro.MOBILE);
		parametros.add(param);


		param = new Parametro();
		param.setNome(Params.APP_BY_PASS_LOGIN);
        param.setValor(isBoolean(loginByPass));
        param.setTipoParametro(TipoParametro.MOBILE);
        parametros.add(param);

        param = new Parametro();
        param.setNome(Params.APP_LOGIN_OFFLINE);
        param.setValor(isBoolean(loginOffline));
        param.setTipoParametro(TipoParametro.MOBILE);
        parametros.add(param);

		return parametros;
	}


	public static List<Parametro> getDefaults() {
        List<Parametro> parametros = new ArrayList<Parametro>();
        Parametro param = new Parametro();
        param.setNome(Params.APP_LOGIN_AUTOMATICO);
        param.setValor(isBooleanDefault(true));
        param.setTipoParametro(TipoParametro.MOBILE);
        parametros.add(param);


        param = new Parametro();
        param.setNome(Params.APP_PERMITE_LOGOUT);
        param.setValor(isBooleanDefault(true));
        param.setTipoParametro(TipoParametro.MOBILE);
        parametros.add(param);

        param = new Parametro();
        param.setNome(Params.APP_PODE_POSTAR);
        param.setValor(isBooleanDefault(true));
        param.setTipoParametro(TipoParametro.MOBILE);
        parametros.add(param);


        param = new Parametro();
        param.setNome(Params.APP_CHAT_ON);
        param.setValor(isBooleanDefault(true));
        param.setTipoParametro(TipoParametro.MOBILE);
        parametros.add(param);


        param = new Parametro();
        param.setNome(Params.APP_SHOW_WHATS_NEWS);
        param.setValor(isBooleanDefault(true));
        param.setTipoParametro(TipoParametro.MOBILE);
        parametros.add(param);


        param = new Parametro();
        param.setNome(Params.APP_SHOW_AUDIENCIA_POST);
        param.setValor(isBooleanDefault(true));
        param.setTipoParametro(TipoParametro.MOBILE);
        parametros.add(param);

        param = new Parametro();
        param.setNome(Params.APP_OPTIONS_MENU);
        param.setValor(isBooleanDefault(true));
        param.setTipoParametro(TipoParametro.MOBILE);
        parametros.add(param);

        param = new Parametro();
        param.setNome(Params.APP_MENU_CATEGORIA);
        param.setValor(isBooleanDefault(false));
        param.setTipoParametro(TipoParametro.MOBILE);
        parametros.add(param);
        return parametros;
    }


	private String isBoolean(boolean value) {
		return value ? "1" : "0";
	}


	private static String isBooleanDefault(boolean value) {
        return value ? "1" : "0";
    }

}