package br.livetouch.livecom.chatAkka.protocol;

import akka.actor.ActorRef;

public class AssociateWithUser {
	public ActorRef userActor;
	public Long userId;
	public String userSo;

	public AssociateWithUser(ActorRef userActor, Long userId, String so) {
		this.userActor = userActor;
		this.userId = userId;
		this.userSo = so;
	}

	@Override
	public String toString() {
		return "AssociateWithUser [userActor=" + userActor + "]";
	}

}