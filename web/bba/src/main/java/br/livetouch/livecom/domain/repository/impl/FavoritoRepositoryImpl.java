package br.livetouch.livecom.domain.repository.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Favorito;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.FavoritoRepository;
import br.livetouch.livecom.domain.vo.PostCountVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.spring.SearchInfo;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public class FavoritoRepositoryImpl extends LivecomRepository<Favorito> implements FavoritoRepository {

	public FavoritoRepositoryImpl() {
		super(Favorito.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Favorito findByUserAndPost(Usuario u, Post post) {
		StringBuffer sb = new StringBuffer("from Favorito f where f.favorito=1 and f.usuario.id=? and f.arquivo is null ");
		
		if(post != null) {
			sb.append(" and f.post.id=? ");
		}
		
		Query q = createQuery(sb.toString());
		q.setCacheable(true);
		
		q.setLong(0, u.getId());
		if(post != null) {
			q.setLong(1, post.getId());
		}
		
		q.setCacheable(true);
		List<Favorito> list = q.list();
		Favorito f = list.isEmpty() ? null : list.get(0);
		return f;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Favorito findByUserAndFile(Usuario u, Arquivo file) {
		Query q = createQuery("select distinct f from Favorito f where f.favorito=1 and f.usuario.id=? and f.arquivo.id=?");
		q.setLong(0, u.getId());
		q.setLong(1, file.getId());
		
		q.setCacheable(true);
		List<Favorito> list = q.list();
		Favorito f = list.isEmpty() ? null : list.get(0);
		return f;
	}
	
	@Override
	public List<Favorito> findAllByUser(Usuario u) {
		// favoritos de posts
		List<Favorito> list = query("select distinct f from Favorito f where f.favorito=1 and f.usuario.id=? and f.arquivo is null order by f.post.id desc", true,u.getId());
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Favorito> findAllFavoritosByUserAndPostIds(Usuario user, List<Long> ids) {
		Criteria c = createCriteria();
		c.add(Restrictions.eq("favorito","1"));
		c.add(Restrictions.eq("usuario.id", user.getId()));
		c.add(Restrictions.in("post.id", ids));
		List<Favorito> list = c.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Favorito> findAllFavoritosNotificationByUser(Usuario user) {
		Query q = null;

		StringBuffer sb = new StringBuffer("select f from Favorito f inner join f.post p where 1=1 ");

		if(user != null) {
			// se o post é seu, e o favorito é do outro. Nao deixa ler seu proprio favorito
			sb.append(" and p.usuario.id = :userId and f.usuario.id != :userId");
		}
		
		sb.append(" and (f.favorito = 1 and (f.lidaNotification=0 or f.lidaNotification is null)) ");
		
		sb.append(" order by f.id desc");
		
		// Query
		q = createQuery(sb.toString());

		if(user != null) {
			q.setParameter("userId", user.getId());
		}

//		if( maxRows > 0) {
//			int firstResult =  page * maxRows;
//			q.setFirstResult(firstResult);
//			q.setMaxResults(maxRows);
//		}
		
		q.setCacheable(true);

		List<Favorito> list = q.list();

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> findAllPostsByUser(Usuario user, SearchInfo search) {
		if(user == null) {
			return new ArrayList<Post>();
		}
		
		Query q = null;

		StringBuffer sb = new StringBuffer("select distinct p from Favorito f inner join f.post p where 1=1 ");
		
		if(user != null) {
			sb.append(" and f.usuario.id=?");
		}

		sb.append(" and f.favorito = 1");

		sb.append(" order by f.id desc");

		// Query
		q = createQuery(sb.toString());

		if(user != null) {
			q.setLong(0, user.getId());
		}

		q.setMaxResults(search.maxRows);;

		q.setCacheable(true);
		List<Post> list = q.list();

		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Favorito> findAllByPosts(Usuario user, List<Long> ids) {
		
		StringBuffer sql = new StringBuffer("select f from Post p ");
		
		// Favorito
		sql.append(" inner join p.favoritos f");
		
		// Where
		sql.append(" where 1=1 ");
		
		if(ids != null && ids.size() > 0) {
			sql.append(" and p.id in (:postsIds) ");
		}
		
		// Favorito
		if(user != null) {
			sql.append(" and (f.favorito = 1 and f.usuario.id = :user and f.arquivo is null)");
		}
		
		Query q = createQuery(sql.toString());
		
		if(user != null) {
			q.setParameter("user", user.getId());
		}
		if(ids != null && ids.size() > 0) {
			q.setParameterList("postsIds", ids);
		}
		
		q.setCacheable(true);
		
		List<Favorito> list = q.list();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PostCountVO> findCountByPosts(List<Long> ids) {
		
		StringBuffer sql = new StringBuffer("select new br.livetouch.livecom.domain.vo.PostCountVO(p,count(p.id)) from Post p ");
		
		// Like
		sql.append(" inner join p.favoritos f");

		// Where
			sql.append(" where 1=1 ");
			
			if(ids != null && ids.size() > 0) {
				sql.append(" and p.id in (:postsIds) ");
			}

		// Like
		sql.append(" and (f.favorito = 1 and f.arquivo is null)");

		// Group by
		sql.append(" group by p");

		Query q = createQuery(sql.toString());

		if(ids != null && ids.size() > 0) {
			q.setParameterList("postsIds", ids);
		}
		q.setCacheable(true);
		
		List<PostCountVO> list = q.list();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioVisualizacaoVO> reportFavoritos(RelatorioFiltro filtro) throws DomainException {
		Query query = queryReportFavoritos(filtro);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		query.setCacheable(true);
		List<RelatorioVisualizacaoVO> favs = query.list();

		return favs;
	}

	public Query queryReportFavoritos(RelatorioFiltro filtro) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;

		if (StringUtils.isNotEmpty(filtro.getDataIni())) {
			filtro.setDataInicial(DateUtils.toDate(filtro.getDataIni()));
		}
		if (StringUtils.isNotEmpty(filtro.getDataFim())) {
			filtro.setDataFinal(DateUtils.toDate(filtro.getDataFim()));
		}

		Long empresa = filtro.getEmpresaId();
		
		Usuario usuario = filtro.getUsuario();
		Post post = filtro.getPost();
		CategoriaPost categoria = filtro.getCategoria();
		
		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		sb.append("select new br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO(DATE_FORMAT(p.dataPublicacao,'%d/%m/%Y %H:%m:%s'),count(p.id),p,f) ");

		sb.append(" from Favorito f inner join f.post p");
		
		sb.append(" where 1=1 ");
		
		sb.append(" and f.favorito = 1");

		if (dataInicio != null) {
			sb.append(" and p.dataPublicacao >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and p.dataPublicacao <= :dataFim");
		}

		if (usuario != null) {
			sb.append(" and f.usuario = :usuario ");
		}

		if (post != null) {
			sb.append(" and p = :post");
		}

		if (empresa != null) {
			sb.append(" and p.usuario.empresa.id = :empresa");
		}
		
		if (categoria != null) {
			sb.append(" and p.categoria = :categoria");
		}

		sb.append(" group by p");
		
		if(filtro.isAgrupar()) {
			sb.append(" , f");
		}
		
		sb.append(" order by p.dataPublicacao desc");

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}

		if (post != null) {
			q.setParameter("post", post);
		}
		
		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		if (categoria != null) {
			q.setParameter("categoria", categoria);
		}
		
		return q;

	}
}