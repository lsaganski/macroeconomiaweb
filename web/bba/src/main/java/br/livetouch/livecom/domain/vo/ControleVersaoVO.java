package br.livetouch.livecom.domain.vo;

public class ControleVersaoVO {
	public String status;
	public String mensagem;
	public String link;
	public String errorCode;
	public Boolean forceUpdate;
	
	public ControleVersaoVO(String mensagem, String errorCode) {
		this.mensagem = mensagem;
		this.errorCode = errorCode;
		this.status = "NOK";
	}
	
	public ControleVersaoVO(String mensagem, Boolean forceUpdate) {
		this.mensagem = mensagem;
		this.errorCode = "forceUpdate";
		this.forceUpdate = forceUpdate;
		this.status = "NOK";
	}
	
}
