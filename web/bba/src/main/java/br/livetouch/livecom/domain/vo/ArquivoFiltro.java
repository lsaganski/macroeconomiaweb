package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;

public class ArquivoFiltro implements Serializable{
	private static final long serialVersionUID = -4131941141560346730L;
	public String texto;
	public List<Tag> tags;
	public List<Grupo> grupos;
	public Boolean todos;
	public Usuario usuario;
	public Integer pageSize;
	public Integer page;
	public String ordem;
	
	public String start;
	public String end;

	private Date dataInicial;
	private Date dataFinal;
	public String extensao;
	
	public Date getDataInicial() {
		return dataInicial;
	}
	
	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}
	
	public Date getDataFinal() {
		return dataFinal;
	}
	
	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}
	
}
