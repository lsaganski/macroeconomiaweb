package br.livetouch.livecom.web.pages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.service.ParametroService;
import net.sf.click.Page;

@Controller
@Scope("prototype")
public class PingPage extends Page {

	@Autowired
	ParametroService parametroService;
	
	public String ping;
	
	@Override
	public String getTemplate() {
		return getPath();
	}
	
	@Override
	public void onInit() {
		super.onInit();
		
		long count = parametroService.getCount();
		if(count > 0)
			ping = "ok:" + count;
		else
			ping = "ok";
	}
}
