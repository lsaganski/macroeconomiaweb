package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Rate;

public class RateVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public long userId;
	public String urlFotoUsuario;
	public String userNome;
	public String userLogin;
	public long postId;
	public String postTitulo;
	public Long comentarioId;
	public int valor;
	public int rateMedia;
	public Long rateCount;

	public void setRate(Rate f) {
		this.id = f.getId();
		this.userId 	= f.getUsuario().getId();
		this.userNome 	= f.getUsuario().getNome();
		this.userLogin 	= f.getUsuario().getLogin();
		this.urlFotoUsuario = f.getUsuario().getUrlFoto();
		if(f.getPost() != null) {
			this.postId 	= f.getPost().getId();
			this.postTitulo	= f.getPost().getTitulo();
		}
		this.valor = f.getValue();
	}

	public static List<RateVO> toListVO(List<Rate> list) {
		List<RateVO> vos = new ArrayList<RateVO>();
		for (Rate h : list) {
			RateVO vo = new RateVO();
			vo.setRate(h);
			vos.add(vo);
		}
		return vos;
	}


	@Override
	public String toString() {
		return "RateVO [userId=" + userId + ", postId=" + postId + ", titulo=" + postTitulo + "]";
	}
}
