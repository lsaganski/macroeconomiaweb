package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.EntityField;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.vo.CategoriaVO;
import net.sf.click.control.Form;

/**
 * @author Juillian Lee
 * 
 */
@Controller
@Scope("prototype")
public class DeletarCategoriaPage extends WebServiceXmlJsonPage {
	public Form form = new Form();
	private EntityField<CategoriaPost> tCategoria;

	protected void form() {
		form.add(tCategoria = new EntityField<>("id", true, categoriaPostService));
	}
	
	@Override
	public void onInit() {
		super.onInit();
		form();
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			CategoriaPost c = tCategoria.getEntity();
			if(c == null) {
				return new MensagemResult("NOK", "Categoria não encontrada");
			} 
			categoriaPostService.delete(getUsuario(), c);
			return new MensagemResult("OK", "Categoria deletada com sucesso");
		}
		return new MensagemResult("NOK", "Formulario invalido");
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("categoria", CategoriaVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
