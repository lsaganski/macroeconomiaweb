package br.livetouch.livecom.web.pages.training;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.web.pages.admin.LivecomPage;
import net.sf.click.control.Form;
import net.sf.click.control.PasswordField;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;


/**
 * @author Ricardo Lecheta
 * @created 06/03/2013
 */
@Controller
@Scope("prototype")
public class CadastroPage extends LivecomPage {
	
	public Form form = new Form();
	
	@Override
	public void onInit() {
		super.onInit();
		TextField t;
		form.add(t = new TextField("nome",true));
		form.add(new TextField("login",true));
		form.add(new PasswordField("senha",true));
		form.add(new PasswordField("confirmaSenha",true));

		t.setFocus(true);

		form.add(new Submit("cadastrar",this,"cadastrar"));
		
		TrainingUtil.setTrainingStyle(form);
		setAutoCompleteOff(form);
		
	}
	
	@Override
	public void onPost() {
		super.onPost();
	}

	public boolean cadastrar() {

		String nome = getParam("nome");
		String login = getParam("login");
		String senha = getParam("senha");
		String confirmaSenha = getParam("confirmaSenha");

		if(StringUtils.isBlank(nome) || StringUtils.isBlank(login) || StringUtils.isBlank(senha) || StringUtils.isBlank(confirmaSenha)) {
			setMessageError("Digite todos os campos");
			return true;
		}
		
		Usuario usuario = usuarioService.findByLogin(login, null);
		if(usuario != null) {
			form.setError("Já existe usuário cadastrado com este e-mail.");
			return false;
		}

		usuario = new Usuario();
//		usuario.setDtAcesso(new Date());
		usuario.setNome(nome);
		usuario.setEmail(login);
		usuario.setLogin(login);
		usuario.setSenha(senha);
		usuario.setStatus(StatusUsuario.ATIVO);

		try {
			if(!usuario.isValidNomeSobrenome()) {
				form.setError("Informe o nome e sobrenome");
				return false;
			}
			
			if(!StringUtils.equals(senha, confirmaSenha)) {
				form.setError("Confirmação de senha incorreta");
				return false;
			}
			
			usuarioService.saveOrUpdate(getUsuario(),usuario);

			// User Info
			setUserInfo(usuario);
			
			setRedirect(IndexPage.class);
		}
		catch (Exception e) {
			setMessageError(e);
		}

		return false;
	}

	protected void setUserInfo(Usuario usuario) {
		UserInfoVO userInfo = UserInfoVO.create(usuario);
		userInfo.addSession("web");
		UserInfoVO.setHttpSession(getContext(),userInfo);
	}
}
