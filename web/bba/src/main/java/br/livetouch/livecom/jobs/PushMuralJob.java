package br.livetouch.livecom.jobs;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.enums.TipoPush;
import br.livetouch.livecom.domain.service.ComentarioService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.vo.UserToPushVO;
import br.livetouch.livecom.jobs.info.NotificationInfo;
import br.livetouch.livecom.push.PushLivecomService;
import br.livetouch.livecom.push.PushNotificationVO;
import br.livetouch.livecom.utils.DateUtils;
import net.livetouch.tiger.ddd.DomainException;

/**
 * V2: Varre a tabela de Notification para enviar Pushs.
 * 
 * @author rlech
 *
 */
@Service
public class PushMuralJob extends SpringJob {
	protected static final Logger log = Log.getLogger(PushMuralJob.class);

	@Autowired
	private PushLivecomService pushLivecomService;

	@Autowired
	private PostService postService;

	@Autowired
	protected ComentarioService comentarioService;

	@Autowired
	protected NotificationService notificationService;

	@Autowired WorkDispatcherService dispatcher;
	
	@Autowired
	protected SessionFactory sessionFactory;
	
	private static boolean running = false;

	@Override
	protected void execute(Map<String, Object> params) throws Exception {
		if (running) {
			log("PushJob already running.");
			return;
		}
		if(br.livetouch.livecom.utils.HostUtil.isLocalhost()) {
			return;
		}
		
		running = true;
		
		try {
			if (params != null && notificationService != null) {

				List<Notification> list = notificationService.findAllToSendPush();

				if(list != null && list.size() > 0) {
					log("PushJob.execute(), list: " + list.size());
					
					for (Notification n : list) {
						Usuario u = n.getUsuario();
						Empresa e = u.getEmpresa();
						boolean pushOn = ParametrosMap.getInstance(e).isPushOn();
						if(!pushOn) {
							log("Push desligado para empresa: " + e);
							continue;
						}
						try {
							log(">> Push notification["+n.getId()+"/"+n.getTipo()+"]: " + n);
	
							if (TipoNotificacao.LIKE.equals(n.getTipo())) {
								pushLike(n);
							} else if (TipoNotificacao.FAVORITO.equals(n.getTipo())) {
								pushFavorito(n);
							} else if (TipoNotificacao.COMENTARIO.equals(n.getTipo())) {
								pushComentario(n);
							} else if (TipoNotificacao.NEW_POST.equals(n.getTipo())) {
								pushPost(n);
							} else if (TipoNotificacao.REMINDER.equals(n.getTipo())) {
								Post p = n.getPost();
								if(DateUtils.isMenor(new Date(), p.getReminder(), "dd/MM/yyyy HH:mm")) {
									continue;
								}
								pushReminder(e, n);
							} 
						}catch (Exception ex) {
							ex.printStackTrace();
							log("<< push notification[" + ex.getMessage() + "]: ERROR");
						} finally {
							
							if (!TipoNotificacao.REMINDER.equals(n.getTipo())) {
								n.setDataPush(new Date());
								n.setSendPush(false);
								notificationService.save(n);
								log("<< push notification["+n.getId()+"/"+n.getTipo()+"]: OK");
							}
							
						}
					}
				}
			}
		}finally {
			running = false;
		}
	}

	private void pushReminder(Empresa e, Notification n) {
		boolean autorReceivePush = ParametrosMap.getInstance(e).getBoolean(Params.PUSH_POST_AUTOR_RECEBE_ON, false);
		NotificationInfo.getInstance().addNotification(n, autorReceivePush);
		JobUtil.executeJob(applicationContext, InsertNotificationsJob.class, null);
	}

	private void pushFavorito(Notification n) throws IOException {
		Post post = n.getPost();
		
		String tituloMsg = n.getTexto();
		String tituloOriginal = n.getTitulo();

		PushNotificationVO pushNotifVO = new PushNotificationVO();
		pushNotifVO.setTipo(TipoPush.favorito.toString());
		pushNotifVO.setTitle(tituloMsg);
		pushNotifVO.setTituloOriginal(tituloOriginal);
		pushNotifVO.setPostId(post.getId());
		pushNotifVO.setTitleNot(n.getTituloNot());
		pushNotifVO.setSubTitleNot(n.getSubTituloNot());

		long pushId = post.getId();
		sendPushNotification(n, pushNotifVO, pushId);
	}

	private void pushLike(Notification n) throws IOException {

		Post post = n.getPost();
		Comentario comentario = n.getComentario();

		String tituloMsg = n.getTexto();
		String tituloOriginal = n.getTitulo();

		PushNotificationVO pushNotifVO = new PushNotificationVO();
		pushNotifVO.setTipo(TipoPush.like.toString());
		pushNotifVO.setTitle(tituloMsg);
		pushNotifVO.setTituloOriginal(tituloOriginal);
		pushNotifVO.setTitleNot(n.getTituloNot());
		pushNotifVO.setSubTitleNot(n.getSubTituloNot());

		Long pushId = post.getId();
		if (post != null) {
			pushId = post.getId();
			pushNotifVO.setPostId(pushId);
		}
		if (comentario != null) {
			Post p = comentario.getPost();
			if (p != null) {
				pushNotifVO.setPostId(p.getId());
			}
//			pushId = comentario.getId();
			pushNotifVO.setComentarioId(pushId);
		}
		
		sendPushNotification(n, pushNotifVO, pushId);
	}

	private void sendPushNotification(Notification n, PushNotificationVO pushNotifVO, Long pushId) throws IOException {
		List<Long> userIds = notificationService.findIdUsersFrom(n);
		if(userIds.size() > 0) {
			pushLivecomService.push(pushId, userIds, pushNotifVO);
			int updateSendPushNotification = notificationService.updateSendPushNotification(n.getId(), userIds);
			log.debug("sendPushNotification update " + updateSendPushNotification);
		} else {
			log.debug("Nenhum usuario encontrado para enviar o push da notification " + n.toString());
		}
		
	}

	private void pushComentario(Notification n) throws IOException {

		Post post = n.getPost();
		Comentario c = n.getComentario();

		String tituloMsg = n.getTexto();
		String tituloOriginal = n.getTitulo();

		long pushId = post.getId();

		// Envia Push
		PushNotificationVO vo = new PushNotificationVO();
		vo.setTipo(TipoPush.comentario.toString());
		vo.setPostId(post.getId());
		vo.setComentarioId(c.getId());
		vo.setTitle(tituloMsg);
		vo.setTituloOriginal(tituloOriginal);
		vo.setTitleNot(n.getTituloNot());
		vo.setSubTitleNot(n.getSubTituloNot());

		sendPushNotification(n, vo, pushId);
	}

	private void pushPost(final Notification n) throws IOException, DomainException, JSONException {
		Usuario u = n.getUsuario();
		final Post post = n.getPost();
		Comentario c = n.getComentario();

		String tituloMsg = n.getTexto();
		String tituloOriginal = n.getTitulo();

		long pushId = post.getId();

		// Envia Push
		PushNotificationVO vo = new PushNotificationVO();
		vo.setDataAgendamento(n.getDataPublicacao());
		vo.setTipo(TipoPush.newPost.toString());
		vo.setPostId(post.getId());
		if(c != null) {
			vo.setComentarioId(c.getId());
		}
		vo.setTitle(tituloMsg);
		vo.setTituloOriginal(tituloOriginal);
		vo.setTitleNot(n.getTituloNot());
		vo.setSubTitleNot(n.getSubTituloNot());

		// Pega filhos desta notificação (push para todos ouvintes do comentario)
		List<UserToPushVO> users = notificationService.findUsersFrom(n);
		
		int size = users.size();
		Empresa empresa = u.getEmpresa();
		ParametrosMap params = ParametrosMap.getInstance(empresa);
		int batchSize = params.getInt(Params.PUSH_BATCH_SIZE, 20000);
		
		boolean sendToAll = params.getBoolean(Params.PUSH_SEND_TOALL, false);
		
		if(!sendToAll) {
			if(size > batchSize) {
				
				log("Enviando push em batch");
				
				//Salva apenas a mensagem no push
				if(n.getPushId() == null) {
					vo.setProcessando(true);
					String resp = pushLivecomService.push(pushId, empresa, vo);
					
					if(resp != null) {
						/**
						 * No envio por lote, primeiro avisa push que vai enviar lote.
						 * Na 1a vez, nao envia nenhum usuario.
						 * Le a mensagemId de retorno do Push e salva na Notification.
						 * As futuras request vao enviar os usuarios informando esse pushId
						 */
						//Get json com a msgId
						JSONObject json = new JSONObject(resp);
						JSONObject pushResponse = json.optJSONObject("pushResponse") != null ? json.optJSONObject("pushResponse") : json.optJSONObject("response");
						if(pushResponse != null && pushResponse.has("mensagemId")) {
							Long msgId = pushResponse.getLong("mensagemId");
							
							if(msgId != null) {
								vo.setMsgId(msgId);
								n.setPushId(msgId);
								notificationService.save(n);						
							}
						}
					}
				}
				
				vo.setMsgId(n.getPushId());
				
				//save parent as sent
				n.setDataPush(new Date());
				n.setSendPush(false);
				notificationService.save(n);
				
				int total = 0;
				
				//envia em batchs
				List<List<UserToPushVO>> partition = Lists.partition(users, batchSize);
				for (final List<UserToPushVO> list : partition) {
					
					// Push!
					List<Long> listIds = UserToPushVO.getListIds(list);
					
					System.out.println("Push para " + listIds.size());
					total += listIds.size();
					System.out.println("Total pushs " + total);
					
					pushLivecomService.push(pushId, listIds, vo);
					
					dispatcher.insertNotificationUsuariosWork(n, list, post, false);
				}
				
				System.out.println("# Total pushs " + total);
				
				//fim processando
				vo.setProcessando(false);
				pushLivecomService.push(pushId, empresa, vo);
				
				n.setDataPush(new Date());
				n.setSendPush(false);
				notificationService.save(n);
				
			} else if(users.size() > 0){
				
				pushLivecomService.push(pushId, UserToPushVO.getListIds(users), vo);
				
				dispatcher.insertNotificationUsuariosWork(n, users, post, false);
			}
		} else {
			pushLivecomService.sendToAll(params, pushId, vo);
			dispatcher.insertNotificationUsuariosWork(n, users, post, false);
		}
		n.setDataPush(new Date());
		n.setSendPush(false);
		// TODO verificar o que fazer quando nao tiver salvo as notificacoes filhas por algum erro de sistema.
		notificationService.save(n);
		postService.saveOrUpdate(null, post);
	}

	private void log(String string) {
		log.debug(string);
	}
}
