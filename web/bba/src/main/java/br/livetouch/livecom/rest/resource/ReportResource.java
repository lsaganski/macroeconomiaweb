package br.livetouch.livecom.rest.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PushReport;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.PushReportFiltro;
import br.livetouch.livecom.domain.vo.PushReportVO;
import br.livetouch.livecom.domain.vo.QuantidadeVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioAudienciaVO;
import br.livetouch.livecom.domain.vo.RelatorioEmailVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioLoginVO;
import br.livetouch.livecom.domain.vo.RelatorioNotificationsVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/report")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class ReportResource extends MainResource {

	protected static final Logger log = Log.getLogger(ReportResource.class);

	@GET
	@Path("/logins")
	public Response getLogins(@BeanParam RelatorioFiltro filtro) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}

		setAttributeSession(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		filtro.setEmpresaId(getEmpresa().getId());

		if (filtro.getUsuarioId() != null) {
			Usuario usuario = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(usuario);
			filtro.setUsuarioNome(usuario.getNome());
			filtro.setUsuarioLogin(usuario.getLogin());
		}

		if (filtro.getModo().equals("consolidado")) {
			List<RelatorioLoginVO> logins = reportService.loginsResumido(filtro);
			HashMap<String, HashMap<String, Long>> map = reportService.buildMapLogin(logins);
			return Response.ok(map).build();
		} else {
			List<RelatorioLoginVO> logins = reportService.loginsExpandido(filtro);
			return Response.ok(logins).build();
		}

	}

	@GET
	@Path("/logins/detalhes")
	public List<RelatorioLoginVO> getDetalhesLogins(@BeanParam RelatorioFiltro filtro) throws DomainException {

		if (filtro == null) {
			return new ArrayList<RelatorioLoginVO>();
		}
		filtro.setEmpresaId(getEmpresa().getId());

		if (filtro.getUsuarioId() != null) {
			Usuario u = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(u);
		}

		List<RelatorioLoginVO> logs = new ArrayList<RelatorioLoginVO>();
		if (filtro.isAgrupar()) {
			logs = reportService.loginsAgrupado(filtro);
		} else {
			logs = reportService.loginsExpandido(filtro);
		}
		return logs;
	}
	
	@GET
	@Path("/app/versao")
	public Response getAppVersion(@BeanParam RelatorioFiltro filtro) throws DomainException {

		if(!hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		setAttributeSession(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		Empresa empresa = getEmpresa();
		if(empresa != null) {
			filtro.setEmpresaId(empresa.getId());
		}

		List<RelatorioLoginVO> logins = reportService.reportVersao(filtro);
		return Response.ok(logins).build();

	}
	
	@GET
	@Path("/app/versao/grafico")
	public Response getAppVersionGraficoLinhas(@BeanParam RelatorioFiltro filtro) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		setAttributeSession(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		
		Empresa empresa = getEmpresa();
		if(empresa != null) {
			filtro.setEmpresaId(empresa.getId());
		}
		
		Map<String, List<QuantidadeVersaoVO>> retorno = reportService.reportVersaoGrafico(filtro);
		return Response.ok(retorno).build();
		
	}

	@GET
	@Path("/favoritos")
	public List<RelatorioVisualizacaoVO> getFavoritos(@BeanParam RelatorioFiltro filtro) throws DomainException {

		setAttributeSession(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		filtro.setEmpresaId(getEmpresa().getId());

		if (filtro.getUsuarioId() != null) {
			Usuario usuario = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(usuario);
			filtro.setUsuarioNome(usuario.getNome());
			filtro.setUsuarioLogin(usuario.getLogin());
		}

		if (filtro.getPostId() != null) {
			Post post = postService.get(filtro.getPostId());
			filtro.setPost(post);
			filtro.setPostTitulo(post.getTitulo());
		}
		if (filtro.getCategoriaId() != null) {
			CategoriaPost categoria = categoriaPostService.get(filtro.getCategoriaId());
			filtro.setCategoria(categoria);
		}

		filtro.setAgrupar(false);

		List<RelatorioVisualizacaoVO> favs = new ArrayList<RelatorioVisualizacaoVO>();

//		favs = notificationService.reportNotificationsByType(filtro);
		favs = favoritoService.reportFavoritos(filtro);
		
		return favs;
	}
	
	@GET
	@Path("/emails")
	public List<RelatorioEmailVO> getEmails(@BeanParam RelatorioFiltro filtro) throws DomainException {

		setAttributeSession(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		filtro.setEmpresaId(getEmpresa().getId());

		if (filtro.getUsuarioId() != null) {
			Usuario usuario = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(usuario);
			filtro.setUsuarioNome(usuario.getNome());
			filtro.setUsuarioLogin(usuario.getLogin());
		}

		List<RelatorioEmailVO> emails = new ArrayList<RelatorioEmailVO>();

		emails = emailReportService.reportEmail(filtro);
		
		return emails;
	}
	

	@GET
	@Path("/notifications")
	public List<RelatorioNotificationsVO> getNotifications(@BeanParam RelatorioFiltro filtro) throws DomainException {
		
		setAttributeSession(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		filtro.setEmpresaId(getEmpresa().getId());
		
		if (filtro.getUsuarioId() != null) {
			Usuario usuario = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(usuario);
			filtro.setUsuarioNome(usuario.getNome());
			filtro.setUsuarioLogin(usuario.getLogin());
		}
		
		if (filtro.getPostId() != null) {
			Post post = postService.get(filtro.getPostId());
			filtro.setPost(post);
			filtro.setPostTitulo(post.getTitulo());
		}
		
		List<RelatorioNotificationsVO> notifications = new ArrayList<RelatorioNotificationsVO>();
		
		notifications = notificationService.reportNotifications(filtro);
		
		return notifications;
	}

	@GET
	@Path("/favoritos/detalhes")
	public List<RelatorioVisualizacaoVO> getDetalhesFavoritos(@BeanParam RelatorioFiltro filtro, @QueryParam("id") Long postId) throws DomainException {

		if (postId != null) {
			Post p = postService.get(postId);
			filtro.setPost(p);
			filtro.setPostId(postId);
		}

		filtro.setAgrupar(true);

		List<RelatorioVisualizacaoVO> favs = favoritoService.reportFavoritos(filtro);
		return favs;
	}

	

	@GET
	@Path("/pushReport")
	public List<PushReportVO> getPushReport(@BeanParam PushReportFiltro filtro) throws DomainException {

		if (filtro == null) {
			return new ArrayList<PushReportVO>();
		}
		setAttributeSession(PushReportFiltro.SESSION_FILTRO_KEY, filtro);

		List<PushReportVO> reports = pushReportService.findReportByFilterVO(filtro, getEmpresa());
		return reports;
	}
	
	@GET
	@Path("/pushReport/detalhes")
	public PushReportVO getDetalhesPushReport(@QueryParam("id") Long id) throws DomainException {

		PushReport report = pushReportService.get(id);
		PushReportVO  vo = new PushReportVO(report);
		return vo;
	}
	
	@GET
	@Path("/audiencia")
	public List<RelatorioAudienciaVO> getAudiencia(@BeanParam RelatorioFiltro filtro) throws DomainException {

		setAttributeSession(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		filtro.setEmpresaId(getEmpresa().getId());

		if (filtro.getUsuarioId() != null) {
			Usuario usuario = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(usuario);
			filtro.setUsuarioNome(usuario.getNome());
			filtro.setUsuarioLogin(usuario.getLogin());
		}

		if (filtro.getPostId() != null) {
			Post post = postService.get(filtro.getPostId());
			filtro.setPost(post);
			filtro.setPostTitulo(post.getTitulo());
		}

		List<RelatorioAudienciaVO> listaAudiencia = new ArrayList<RelatorioAudienciaVO>();

		listaAudiencia = reportService.findAudiencia(filtro);
		return listaAudiencia;
	}

	@GET
	@Path("/audiencia/detalhes")
	public List<RelatorioAudienciaVO> getDetalhesAudiencia(@BeanParam RelatorioFiltro filtro, @QueryParam("postId") Long postId) throws DomainException {

		if (filtro == null) {
			return new ArrayList<RelatorioAudienciaVO>();
		}

		if (postId != null) {
			Post p = postService.get(postId);
			filtro.setPost(p);
			filtro.setPostId(postId);
		}
		
		filtro.setAgrupar(false);

		List<RelatorioAudienciaVO> audiencia = reportService.audienciaDetalhes(filtro);
		return audiencia;
	}
	
	@GET
	@Path("/audiencia/detalhes/{id}")
	public List<RelatorioAudienciaVO> getDetalhesAudienciaMobile(@PathParam("id") Long id) throws DomainException {

		if (id == null) {
			return new ArrayList<RelatorioAudienciaVO>();
		}
		
		RelatorioFiltro filtro = new RelatorioFiltro();
		filtro.setPostId(id);
		filtro.setAgrupar(false);

		List<RelatorioAudienciaVO> audiencia = reportService.audienciaDetalhes(filtro);
		return audiencia;
	}
	
	@GET
	@Path("/likes")
	public List<RelatorioVisualizacaoVO> getLikes(@BeanParam RelatorioFiltro filtro) throws DomainException {

		setAttributeSession(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		filtro.setEmpresaId(getEmpresa().getId());

		if (filtro.getUsuarioId() != null) {
			Usuario usuario = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(usuario);
			filtro.setUsuarioNome(usuario.getNome());
			filtro.setUsuarioLogin(usuario.getLogin());
		}

		if (filtro.getPostId() != null) {
			Post post = postService.get(filtro.getPostId());
			filtro.setPost(post);
			filtro.setPostTitulo(post.getTitulo());
		}
		if (filtro.getCategoriaId() != null) {
			CategoriaPost categoria = categoriaPostService.get(filtro.getCategoriaId());
			filtro.setCategoria(categoria);
		}
		
		filtro.setAgrupar(false);

		List<RelatorioVisualizacaoVO> likes = new ArrayList<RelatorioVisualizacaoVO>();

		likes = likeService.reportLikes(filtro);
		
		return likes;
	}

	@GET
	@Path("/likes/detalhes")
	public List<RelatorioVisualizacaoVO> getDetalhesLikes(@BeanParam RelatorioFiltro filtro, @QueryParam("id") Long postId) throws DomainException {
		if (postId != null) {
			Post p = postService.get(postId);
			filtro.setPost(p);
			filtro.setPostId(postId);
		}
		
		filtro.setAgrupar(true);

		List<RelatorioVisualizacaoVO> likes = likeService.reportLikes(filtro);
		return likes;
	}

}