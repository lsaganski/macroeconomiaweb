package br.livetouch.livecom.web.pages.cadastro;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;


@Controller
@Scope("prototype")
public class CamposPage extends CadastrosPage {

	public Long id;
	public boolean escondeChat = true;
	public Form form = new Form();
	
	@Override
	public void onInit() {
		super.onInit();
		
		form();
	}
	
	public void form() {

		form.add(setCheck("visivel"));
		form.add(setCheck("publico"));
		form.add(setCheck("editavel"));
		form.add(setCheck("obrigatorioCadastroAdmin"));

		//teste
		
		
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();
	}
	
	public Checkbox setCheck (String nome){
		Checkbox check = new Checkbox();
		check = new Checkbox(nome);
		check.setAttribute("class", "bootstrap");
		check.setAttribute("data-off-color", "danger");
		check.setAttribute("data-off-text", "Não");
		check.setAttribute("data-on-text", "Sim");
		check.setId(nome);
		return check;
	}
}
