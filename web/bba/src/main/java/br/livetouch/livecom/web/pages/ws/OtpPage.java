package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.token.TokenHelper;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class OtpPage extends WebServiceXmlJsonPage {
	
	public Form form = new Form();
	public String secret;
	
	public void onInit() {
		form.add(new TextField("secret"));
		
		form.add(new Submit("generate OTP"));
		
		setFormTextWidth(form, 600);
	};

	@Override
	protected Object execute() throws Exception {
		if(form.isValid()) {
			msg = TokenHelper.generateTimeToken(secret, 30);
		}
		return msg;
	}
}
