package br.livetouch.livecom.domain.service;

import java.io.File;
import java.io.IOException;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.ImportarArquivoResponse;
import net.livetouch.tiger.ddd.DomainException;

/**
 * Service para disparar Work do Hibernate 
 * 
 * Utilizado para o Spring controlar a transação com @Transactional.
 * 
 * O Work não deve mexer na transação, e deve sempre lançar exception em caso de erro.
 * 
 * O Spring que fará o rollback.
 * 
 * @author rlech
 *
 */
public interface WorkService {

	@Transactional(rollbackFor=Exception.class)
	ImportarArquivoResponse execute_importarArquivo(File f) throws DomainException, IOException;
}
