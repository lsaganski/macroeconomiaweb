package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import akka.actor.ActorRef;
import br.infra.util.Utils;
import br.livetouch.livecom.chatAkka.protocol.JsonRawMessage;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.DateUtils;
import net.sf.click.Context;

public class UserInfoVO implements Serializable {
	public static final String USER_INFO_KEY = "userInfo";

	private static final long serialVersionUID = 1L;
	public Long id;

	public String nome;
	public String cpfCnpj;
	public String login;

	private PermissoesVO permissao;
	private Perfil perfil;
	private Long empresaId;
	private EmpresaVO empresaRoot;


	private String cargo;
	private String funcao;
	private String complemento;
	private String urlFotoUsuario;
	private String password;

	private Map<String, Boolean> menusAtivo = new HashMap<>();

	private Map<String, UserInfoSession> mapLogins = new HashMap<>();

	// TODO Chat keep alive mandar pro session
	private Date lastKeepAliveChat;

	private String flags;

	private List<MenuVO> menus;
	
	private String sideboxTab;
	private String notificationOption;
	private EmpresaVO empresa;
	private Boolean primeiroAcesso;
	private Boolean senhaExpirada;

	private Idioma idioma;

	private UserInfoVO() {
		
	}
	
	public void clear() {
		permissao = null;
	}

	public static UserInfoVO create(Usuario usuario) {
		UserInfoVO userInfo = Livecom.getInstance().getUserInfo(usuario.getId());
		if (userInfo == null) {
			userInfo = new UserInfoVO();
			userInfo.setUsuario(usuario);
			Livecom.getInstance().add(userInfo);
		} else {
			PermissoesVO permissoesVO = userInfo.setPermissoes(usuario);
			userInfo.permissao = permissoesVO;
		}
		return userInfo;
	}

	public static UserInfoVO update(Usuario usuario) {
		UserInfoVO userInfo = Livecom.getInstance().getUserInfo(usuario.getId());
		if (userInfo != null) {
			userInfo.setUsuario(usuario);
			Livecom.getInstance().add(userInfo);
		}
		return userInfo;
	}

	public void setUsuario(Usuario u) {
		org.hibernate.Hibernate.initialize(u.getPermissao());
		org.hibernate.Hibernate.initialize(u.getEmpresa());
		org.hibernate.Hibernate.initialize(u.getIdioma());

		this.id = u.getId();
		this.nome = StringUtils.isNotEmpty(u.getSobrenome()) ? u.getNome() + " " + u.getSobrenome() : u.getNome();
		this.cpfCnpj = u.getCpfCnpj();
		this.cargo = u.getCargo();
		if (u.getComplemento() != null) {
			this.complemento = u.getComplemento().getNome();
		}
		if (u.getFuncao() != null) {
			this.funcao = u.getFuncao().getNome();
		}
		this.urlFotoUsuario = u.getUrlFotoUsuario();
		this.login = u.getLogin();
		this.perfil = u.getPermissao();
		PermissoesVO permissoesVO = setPermissoes(u);
		this.permissao = permissoesVO;
		this.empresaId = u.getEmpresa().getId();
		this.empresa = new EmpresaVO(u.getEmpresa());
		String senha = u.getSenha();
		this.password = senha;
		this.primeiroAcesso = u.isPrimeiroAcesso();
		this.setFlags(u.getFlags());
		this.setIdioma(u.getIdioma());
	}

	public PermissoesVO setPermissoes(Usuario u) {
		PermissoesVO permissoesVO = new PermissoesVO();
		permissoesVO.setMapPermissoes(u.getPermissao().getPermissoes());
		return permissoesVO;
	}

	public static void setHttpSession(Context context, UserInfoVO info) {
		Long empresaId = info.getEmpresaId();
		if(empresaId == null) {
			empresaId = 1L;
		}
		ParametrosMap map = ParametrosMap.getInstance(empresaId);
		String sessionTime = map.get(Params.SESSION_TIME, "240");
		int time = Integer.parseInt(sessionTime);
		context.getSession().setMaxInactiveInterval(time * 60);
		context.setSessionAttribute(USER_INFO_KEY, info);
		Livecom.getInstance().add(info);
	}

	public static UserInfoVO getHttpSession(Context context) {
		UserInfoVO usuario = (UserInfoVO) context.getSessionAttribute(USER_INFO_KEY);
		return usuario;
	}

	public static void removeHttpSession(Context context) {
		context.removeSessionAttribute(USER_INFO_KEY);
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public String getLogin() {
		return login;
	}

	public Long getEmpresaId() {
		return empresaId;
	}

	/**
	 * Admin ROOT principal
	 * 
	 * @return
	 */
	public boolean isAdmin() {
		if (isRoot()) {
			return true;
		}
		if (StringUtils.startsWith(login, "admin@")) {
			return true;
		}
		boolean admin = false;
		if (perfil != null) {
			admin = perfil.isAdmin();
			return admin;
		}
		admin = "admin".equalsIgnoreCase(login);
		return admin;
	}

	/**
	 * Admin ROOT principal
	 * 
	 * @return
	 */
	public boolean isRoot() {
		if (StringUtils.startsWith(login, "root@")) {
			return true;
		}
		boolean root = false;
		if (perfil != null) {
			root = perfil.isRoot();
			return root;
		}
		root = "root".equalsIgnoreCase(login);
		return root;
	}
	
	public boolean isRootEmpresa() {
		if(empresa != null && empresa.adminId == id) {
			return true;
		}
		return false;
	}

	public void setNome(String nome) {
		this.nome = nome;

	}

	public String getCargo() {
		return cargo;
	}

	public String getUrlFotoUsuario() {
		return Utils.s3ToEndpoint(urlFotoUsuario);
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public String getFuncao() {
		return funcao;
	}

	public String getComplemento() {
		return complemento;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	@Override
	public String toString() {
		return "UserInfo: " + login;
	}
	
	public String toStringIdDesc() {
		return id+":"+login;
	}

	public void addStateMenu(String menu, Boolean status) {
		if (menusAtivo == null) {
			menusAtivo = new HashMap<>();
		}

		if (status != null) {
			menusAtivo.put(menu, status);
			return;
		}

		Boolean isBool = menusAtivo.get(menu);
		if (isBool == null) {
			menusAtivo.put(menu, true);
		} else {
			menusAtivo.put(menu, !isBool);
		}
	}

	public Boolean isMenuAtivo(String menu) {
		if (menusAtivo == null) {
			return false;
		}
		Boolean isAtivo = menusAtivo.get(menu);
		if (isAtivo != null) {
			return isAtivo;
		}
		return false;
	}

	public Map<String, Boolean> getMenusAtivo() {
		return menusAtivo;
	}

	public void setMenusAtivo(Map<String, Boolean> menusAtivo) {
		this.menusAtivo = menusAtivo;
	}

	public boolean isLogadoChat() {
		return Livecom.getInstance().isUsuarioLogadoChat(id);
	}
	
	public UserInfoSession addSession(String so) {
		return addSession(so, null);
	}

	/**
	 * @param u
	 * @param key
	 *            = web,android,ios
	 */
	public UserInfoSession addSession(String so, ActorRef chatTcpWebActor) {
		if (so == null) {
			return null;
		}
		so = so.toLowerCase();
		
		/**
		 * Cria apenas uma session por SO (Web, Android, iOS)
		 */
		UserInfoSession session = mapLogins.get(so);
		if(session == null) {
			// Cria uma nova session
			session = new UserInfoSession();
		}
		
		Date now = new Date();
		
		session.tipo = so.toLowerCase();
		if(chatTcpWebActor != null) {
			session.setDataLoginChat(now);
			session.setActorRef(chatTcpWebActor.toString());

			this.lastKeepAliveChat = null;
			session.setKeepAliveOk(true);
			session.setDataUltChat(now);
			
		} else {
			session.setDataLogin(now);
		}
		mapLogins.put(so, session);
		return session;
	}
	
	public Map<String, UserInfoSession> getMapLogins() {
		return mapLogins;
	}

	public boolean hasSession(String key) {
		boolean b = mapLogins.containsKey(key);
		return b;
	}

	public UserInfoSession removeSession(String so) {
		UserInfoSession session = mapLogins.get(so);
		if (session != null) {
			mapLogins.remove(so);
			return session;
		}
		return null;
	}

	public long getTempoLogadoMinutos() {
		UserInfoSession first = getLastUserSession();
		return first != null ? first.getTempoLogadoMinutos() : 0;
	}

	public List<UserInfoSession> getListSession() {
		if (mapLogins == null && mapLogins.isEmpty()) {
			return new ArrayList<>();
		}
		return new ArrayList<>(mapLogins.values());
	}

	private UserInfoSession getLastUserSession() {
		if (mapLogins == null || mapLogins.isEmpty()) {
			return null;
		}
		UserInfoSession lastSession = null;
		Set<String> keys = mapLogins.keySet();
		for (String key : keys) {
			UserInfoSession session = mapLogins.get(key);
			if(lastSession == null) {
				lastSession = session;
			} else {
				Date d1 = session.getDtUltTransacao();
				Date d2 = lastSession.getDtUltTransacao();
				if(d1 != null && d2 != null) {
					if(DateUtils.isMaior(d1, d2,"yyyyMMddHHmmss")) {
						lastSession = session;
					}
				}
			}
		}
		return lastSession;
	}

	public UserInfoSession getUserSession(String so) {
		if(so == null) {
			return null;
		}
		if (mapLogins == null || mapLogins.isEmpty()) {
			return null;
		}
		UserInfoSession session = mapLogins.get(so);
		return session;
	}

	public String getTipoLogin() {
		UserInfoSession first = getLastUserSession();
		return first != null ? first.getTipo() : null;
	}

	public Date getDataLogin() {
		UserInfoSession first = getLastUserSession();
		return first != null ? first.getDtLogin() : null;
	}

	public String getDataLoginString() {
		UserInfoSession first = getLastUserSession();
		return first != null ? first.dataLogin : null;
	}

	public Date getDataUltTransacao() {
		UserInfoSession first = getLastUserSession();
		return first != null ? first.getDtUltTransacao() : null;
	}

	public String getDataUltTransacaoString() {
		UserInfoSession first = getLastUserSession();
		return first != null ? first.dataUltTransacao : null;
	}
	
	public String getDataLoginChatString() {
		UserInfoSession first = getLastUserSession();
		return first != null ? first.dataLoginChat : null;
	}
	
	public String getDataUltChatString() {
		UserInfoSession first = getLastUserSession();
		return first != null ? first.dataUltChat : null;
	}
	
	/**
	 * web,android,ios
	 * 
	 * @return
	 */
	public String getTipoLoginList() {
		if (mapLogins == null || mapLogins.isEmpty()) {
			return null;
		}
		Set<String> keys = mapLogins.keySet();
		String tipoLogin = null;
		for (String key : keys) {
			UserInfoSession session = mapLogins.get(key);
			if (tipoLogin == null) {
				tipoLogin = session.getTipo();
			} else {
				tipoLogin += "," + session.getTipo();
			}
		}

		return tipoLogin;
	}

	public Date getLastKeepAliveChat() {
		return lastKeepAliveChat;
	}

	public void setLastKeepAliveChat(Date dt) {
		this.lastKeepAliveChat = dt;
	}

	public void setEmpresaId(Long empresaId) {
		this.empresaId = empresaId;
	}
	
	public String getFlags() {
		return flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}
	
	public Boolean isConnectorDefault() {
		if(StringUtils.isEmpty(this.flags) || StringUtils.equalsIgnoreCase(this.flags, "livecom")) {
			return true;
		}
		return false;
	}

	public List<MenuVO> getMenus() {
		return menus;
	}

	public void setMenus(List<MenuVO> menus) {
		this.menus = menus;
	}
	
	public String getSideboxTab() {
		return sideboxTab;
	}

	public void setSideboxTab(String sideboxTab) {
		this.sideboxTab = sideboxTab;
	}

	public EmpresaVO getEmpresa() {
	    if(isRoot()) {
	        return empresaRoot;
        }
		return empresa;
	}

	public void setEmpresa(EmpresaVO empresa) {
		this.empresa = empresa;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNotificationOption() {
		return notificationOption;
	}

	public void setNotificationOption(String notificationOption) {
		this.notificationOption = notificationOption;
	}
	
	public static UserInfoVO getUserInfo(Long userId) {
		UserInfoVO userInfo = Livecom.getInstance().getUserInfo(userId);
		return userInfo;
	}

	public static UserInfoSession getUserSession(JsonRawMessage raw) {
		return getUserSession(raw.userId, raw.userSo);
	}
	
	public static UserInfoSession getUserSession(Long userId, String so) {
		UserInfoVO userInfo = getUserInfo(userId);
		if (userInfo != null) {
			UserInfoSession session = userInfo.getUserSession(so);
			return session;
		}
		return null;
	}

	/**
	 * Retonra true se está com web aberta.
	 * 
	 * @param so
	 * @return
	 */
	public boolean isWebOnline() {
		UserInfoSession session = getUserSession("web");
		if(session != null) {
			boolean online = session.isOnline() && !session.isAway();
			return online;
		}
		return false;
	}

	public Boolean getPrimeiroAcesso() {
		return primeiroAcesso;
	}

	public void setPrimeiroAcesso(Boolean primeiroAcesso) {
		this.primeiroAcesso = primeiroAcesso;
	}

	public PermissoesVO getPermissao() {
		return permissao;
	}

	public void setPermissao(PermissoesVO permissoes) {
		this.permissao = permissoes;
	}

	public boolean isPermissao(String key) {
		if(permissao == null) {
			return false;
		}

		Map<String, Boolean> permissoes = permissao.getPermissoes();
		if(permissoes == null) {
			return false;
		}

		return permissoes.get(key) != null ? permissoes.get(key) : false;
	}

    public void setEmpresaRoot(EmpresaVO empresaRoot) {
        this.empresaRoot = empresaRoot;
    }

    public EmpresaVO getEmpresaRoot() {
        return empresaRoot;
    }

	public Boolean getSenhaExpirada() {
		return senhaExpirada;
	}

	public void setSenhaExpirada(Boolean senhaExpirada) {
		this.senhaExpirada = senhaExpirada;
	}

	public Idioma getIdioma() {
		return idioma;
	}

	public void setIdioma(Idioma idioma) {
		this.idioma = idioma;
	}
}