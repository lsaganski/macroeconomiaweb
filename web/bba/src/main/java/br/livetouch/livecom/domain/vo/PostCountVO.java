package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.Post;

/**
 * Para um post, tem o contador de alguma ação (qtde de likes, qtde de comentarios, qtde de badges de comentarios, etc)
 * 
 * @author Usuário
 *
 */
public class PostCountVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7910500909184102513L;
	private final Post post;
	private Long count;

	public PostCountVO(Post post,Long count) {
		super();
		this.post = post;
		this.count = count;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Post getPost() {
		return post;
	}

	@Override
	public String toString() {
		return "post=" + post.getTituloDesc() + ", count=" + count + "]";
	}
}
