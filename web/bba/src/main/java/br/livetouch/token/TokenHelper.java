package br.livetouch.token;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Classe que gera a OTP utilizando o padrao OATH TOTP para uma semente
 * conhecida
 * 
 * @author ricardo
 * 
 */
public class TokenHelper {
	private static final int DEFAULT_CODE_DIGITS = 6;
	public static final boolean DONT_REPEAT_OTP_MODE = true;
	public static boolean LOG_ON;

	public static String generateTimeToken(String secret, long expires) {
		long time = getTime();
		return generateTimeToken(secret,time, expires);
	}
	
	public static String generateTimeToken(String secret, long time, long expires) {
		String timeOfT = TotpToken.getTimeOfT(time, expires);
		String otp = TotpToken.generateOtp(secret, timeOfT, DEFAULT_CODE_DIGITS);
		return otp;
	}
	
	public static String generateEventToken(String secret, long timeShift, long expires) {
		long movingFactor = getEventMovingFactor(getTime(),timeShift, expires, 0);
		String otp = HotpToken.generateOtp(secret, movingFactor);
		return otp;
	}
	
	public static long getEventMovingFactor(long timeShift, long expires) {
		long time = getTime();
		
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		int seconds = c.get(Calendar.SECOND);
		
		return getEventMovingFactor(time,  timeShift, expires, seconds);
	}

	private static long getTime() {
		return getTime(0);
	}
	
	private static long getTime(int diffInSeconds) {
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		if(diffInSeconds != 0) {
			c.add(Calendar.SECOND, diffInSeconds);
		}
		long time = c.getTime().getTime();
		return time;
	}

	/**
	 * Retorna o MovingFactor
	 * 
	 * Se o MovingFactor for o mesmo, gera a mesma OTP.
	 * 
	 * @param time
	 *            - time atual
	 * @param timeShift
	 *            Diferenca em segundos entre a hora do cliente e servidor
	 * @param expires
	 * @return
	 */
	public static long getEventMovingFactor(long time, long timeShift, long expires, long seconds) {
		
		// converte para segundo e tira o timeShift
		// O sinal de menos faz a magica
		// time cliente < time servidor, timeshift for numero negativo. Entao
		// vai somar o timeshift
		// time cliente > time servidor, timeshift for numero negativo. Entao
		// vai subtrair o timeshift
		long timeSegundos = (time / 1000) - timeShift;

		// movingFactor que eh o segredo
		// ele tem que ser igual para gerar uma otp igual
		// entao a sacada eh entre intervalos de 0-30 segundos por exemplo
		// gerar movingFactor igual
		long movingFactor = timeSegundos / expires;

		if(seconds > 0) {
			
			// para cada segundo gera um movingFactor diferente
			// soma para nao multiplicar por 0
			movingFactor *= (seconds + 1);
		}

		return movingFactor;
	}

	public static boolean validateOtp(String semente, String otp, long timeShift, long expires) {
		if (otp == null) {
			throw new IllegalArgumentException("OTP Vazio");
		}
		if (semente == null) {
			throw new IllegalArgumentException("Semente vazia");
		}
		String newOtp = generateEventToken(semente, timeShift, expires);
		boolean ok = otp.equals(newOtp);
		return ok;
	}
	
	/**
	 * Valida a OTP. Durante o mesmo intervalo (30 seg) vai gerar a mesma OTP
	 * 
	 * @param secret
	 * @param otp
	 * @param expires
	 * @return
	 */
	public static boolean validateTimeToken(String secret, String otp, int expires) {
		return validateTimeToken(secret, otp, expires, false);
	}
	
	/**
	 * Valida a OTP (tenta 1 vez na janela anterior conforme indicado na RFC)
	 * 
	 * Ler: 6: Resynchronization
	 * 
	 * @param secret
	 * @param otp
	 * @param expires - tempo de expiracao em segundos
	 * @param timeBackwards - Qtde em segundos para validar para trás. Assim pega a janela anterior
	 * @return
	 */
	public static boolean validateTimeToken(String secret, String otp, int expires, boolean checkTime) {
		String newOtp = generateTimeToken(secret, getTime(), expires);
		boolean ok = otp.equals(newOtp);
		if(ok) {
			return true;
		}
		
		if(checkTime) {
			/**
			 * Não funcionou, tenta na janala anterior.
			 */
			long time = getTime(-expires);
			newOtp = generateTimeToken(secret, time, expires);
			ok = otp.equals(newOtp);
		}
		
		return ok;
	}
}
