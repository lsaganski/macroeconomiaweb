import java.io.IOException;

import br.livetouch.livecom.jobs.InsertNotificationWork;

public class Log4JGenerator {

	public static void main(String[] args) throws IOException {
		
		Class<?> cls = InsertNotificationWork.class;
		
		String className = cls.getSimpleName();
		String fullName = cls.getName();

		String appenderName = className;
		
		System.out.println(String.format("#%s.log",appenderName));
		System.out.println(String.format("log4j.logger.%s=debug,%s",fullName,appenderName));
		System.out.println(String.format("log4j.additivity.%s=false", fullName));
		System.out.println(String.format("log4j.appender.%s=org.apache.log4j.RollingFileAppender",appenderName));
		System.out.println(String.format("log4j.appender.%s.MaxFileSize=5MB",appenderName));
		System.out.println(String.format("log4j.appender.%s.MaxBackupIndex=5",appenderName));
		System.out.println(String.format("log4j.appender.%s.File=/livecom/logs/%s.log",appenderName,appenderName));
		System.out.println(String.format("log4j.appender.%s.layout=org.apache.log4j.PatternLayout",appenderName));
		System.out.println(String.format("log4j.appender.%s",appenderName)+".layout.ConversionPattern=%d{dd/MM/yyyy HH:mm:ss} %5p %M:%L - %m%n");
	}
}
