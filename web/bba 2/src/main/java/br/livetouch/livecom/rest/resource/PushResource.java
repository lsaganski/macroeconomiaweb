package br.livetouch.livecom.rest.resource;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.vo.PushChatVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.jobs.PushChatJob;
import br.livetouch.livecom.rest.domain.MessageResult;
import br.livetouch.livecom.web.pages.ws.MensagemResult;
import br.livetouch.pushserver.lib.PushService;

@Path("/v1/push")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class PushResource extends MainResource {
	protected static final Logger log = Log.getLogger(PushResource.class);
	
	@GET
	@Path("/usuarios")
	public Response usuarios(@BeanParam RelatorioFiltro filtro) {
		try {
			
			if(!hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			PushService s = Livecom.getPushService();
			String json = s.getUsuarios(filtro.getProjeto(), filtro.getUsuarioLogin());
			
			if(StringUtils.isEmpty(json)) {
				return Response.ok(MessageResult.error("JSON esta em branco")).build();
			}
			
			return Response.status(Status.OK).entity(json).build();
		}catch(Exception e) {
			return Response.ok(MensagemResult.erro("Erro ao consultar usuario")).build();
		}
		
	}
	
	@GET
	@Path("/devices")
	public Response devices(@BeanParam RelatorioFiltro filtro) {
		try {
			
			if(!hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			PushService s = Livecom.getPushService();
			String json = s.getDevicesUsuario(filtro.getProjeto(), filtro.getUsuarioLogin());
			
			if(StringUtils.isEmpty(json)) {
				return Response.ok(MensagemResult.erro("Dados estão em branco")).build();
			}
			
			return Response.status(Status.OK).entity(json).build();
		}catch(Exception e) {
			return Response.ok(MensagemResult.erro("Erro ao consultar device")).build();
		}
		
	}
	
	@GET
	@Path("/geral")
	public Response geral(@BeanParam RelatorioFiltro filtro) {
		try {
			
			if(!hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			PushService s = Livecom.getPushService();
			String json = s.getRelatorioGeral(filtro.getDataIni(), filtro.getDataFim(), filtro.getProjeto(), filtro.getTipoPush(), "-1");
			
			if(StringUtils.isEmpty(json)) {
				return Response.ok(MensagemResult.erro("Dados estão em branco")).build();
			}
			
			return Response.status(Status.OK).entity(json).build();
		}catch(Exception e) {
			return Response.ok(MensagemResult.erro("Erro na consulta")).build();
		}
		
	}
	
	@GET
	@Path("/geral/detalhes/{id}")
	public Response detalhesGeral(@PathParam("id") String id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			PushService s = Livecom.getPushService();
			String json = s.getDetalhesPush(id);
			
			if(StringUtils.isEmpty(json)) {
				return Response.ok(MensagemResult.erro("Dados estão em branco")).build();
			}
			
			return Response.status(Status.OK).entity(json).build();
		}catch(Exception e) {
			return Response.ok(MensagemResult.erro("Erro ao consultar detalhes")).build();
		}
		
	}
	
	@GET
	@Path("/geral/grafico/{id}")
	public Response graficoGeral(@PathParam("id") String id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			PushService s = Livecom.getPushService();
			String json = s.getGraficoPush(id);
			
			if(StringUtils.isEmpty(json)) {
				return Response.ok(MensagemResult.erro("Dados estão em branco")).build();
			}
			
			return Response.status(Status.OK).entity(json).build();
		}catch(Exception e) {
			return Response.ok(MensagemResult.erro("Erro ao consultar grafico")).build();
		}
		
	}
	
	@GET
	@Path("/geral/mensagens/{id}")
	public Response mensagens(@PathParam("id") String id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			PushService s = Livecom.getPushService();
			String json = s.getTabelaUsuarioMensagemPush(id);
			
			if(StringUtils.isEmpty(json)) {
				return Response.ok(MensagemResult.erro("Dados estão em branco")).build();
			}
			
			return Response.status(Status.OK).entity(json).build();
		}catch(Exception e) {
			return Response.ok(MensagemResult.erro("Erro ao consultar mensagem")).build();
		}
		
	}
	
	@POST
	@Path("/geral/detalhes/cancelar/{id}")
	public Response cancelarPush(@PathParam("id") String id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			PushService s = Livecom.getPushService();
			String json = s.cancelarPush(id);
			
			if(StringUtils.isEmpty(json)) {
				return Response.ok(MensagemResult.erro("Dados estão em branco")).build();
			}
			
			return Response.status(Status.OK).entity(json).build();
		}catch(Exception e) {
			return Response.ok(MensagemResult.erro("Erro ao canelar push")).build();
		}
		
	}

	@POST
    @Path("/message/chat")
    public Response sendPushMessageChat(PushChatVO vo) {
        log.debug("LivecomChatInterface> SendPushChatJob: " + vo);
        PushChatJob.add(vo);
        return Response.status(Status.OK).entity(MensagemResult.ok("Push adicionado a lista para ser enviado")).build();
    }
}
