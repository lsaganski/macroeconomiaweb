package br.livetouch.livecom.web.pages.ws;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Grupo;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class EditarConversaEmGrupoPage extends WebServiceFormPage {

	public Long grupo_id;
	public String nomeGrupo;
	public Long fotoGrupo;

	@Override
	protected void form() {
		form.add(new TextField("grupo_id", true));
		form.add(new TextField("nomeGrupo"));
		form.add(new TextField("fotoGrupo"));
	}

	@Override
	protected Object go() throws Exception {
		if (!form.isValid()) {
			if(isWsVersion3()) {
				Response r = Response.error(getFormErrorMensagemResult(form).toString());
				return r;
			}
			return getFormErrorMensagemResult(form);
		}
		Grupo grupo = grupoService.get(grupo_id);
		if (grupo == null) {
			if(isWsVersion3()) {
				Response r = Response.error("Não foi possivel editar o grupo. Grupo [" + grupo_id + "] inexistente.");
				return r;
			}
			return new MensagemResult("NOK", "Não foi possivel editar o grupo. Grupo [" + grupo_id + "] inexistente.");
		} else {
			if (!StringUtils.isEmpty(nomeGrupo)) {
				grupo.setNome(nomeGrupo);
			}
			if (fotoGrupo != null) {
				Arquivo foto = arquivoService.get(fotoGrupo);
				if (foto != null)
					grupo.setFoto(arquivoService.get(fotoGrupo));
			}
			
			grupoService.saveOrUpdate(getUsuario(),grupo);
			
			if(isWsVersion3()) {
				Response r = Response.ok("Grupo editado com sucesso.");
				return r;
			}
			return new MensagemResult("OK", "Grupo editado com sucesso.");
		}
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
