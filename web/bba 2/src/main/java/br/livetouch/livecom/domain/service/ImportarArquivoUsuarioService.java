package br.livetouch.livecom.domain.service;

import java.io.IOException;
import java.io.InputStream;

import br.livetouch.livecom.domain.ImportarArquivoResponse;
import br.livetouch.livecom.domain.Usuario;
import jxl.read.biff.BiffException;
import net.livetouch.tiger.ddd.DomainException;

public interface ImportarArquivoUsuarioService extends Service<Usuario> {

	public ImportarArquivoResponse importar(InputStream in) throws DomainException, BiffException, IOException;
}
