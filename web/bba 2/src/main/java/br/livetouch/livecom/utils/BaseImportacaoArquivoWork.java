package br.livetouch.livecom.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import br.infra.util.Log;
import br.livetouch.livecom.jobs.JobUtil;

public class BaseImportacaoArquivoWork {
	protected Logger log;
	
	@Autowired
	protected ApplicationContext applicationContext;

	public BaseImportacaoArquivoWork() {

	}

	public BaseImportacaoArquivoWork(String logger) {
		log = Log.getLogger(logger);
	}

	public void init(String logger) {
		log = Log.getLogger(logger);
	}

	public static int countLines(String filename) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(filename));
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return (count == 0 && !empty) ? 1 : count;
		} finally {
			is.close();
		}
	}
	
	/**
	 * Executa um Job do Quartz
	 * 
	 * @param jobName
	 */
	
	protected void executeJob(final Class<? extends Job> cls,String theadName,final Map<String, Object> params) {
		JobUtil.executeJob(applicationContext, cls,theadName, params);
	}

}
