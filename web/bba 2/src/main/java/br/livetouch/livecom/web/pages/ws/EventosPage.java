package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Evento;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.EventoVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LongField;

@Controller
@Scope("prototype")
public class EventosPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	public int maxRows;
	public int page;
	private LongField id;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(new IntegerField("page"));
		form.add(id = new LongField("id", true));
		form.add(new IntegerField("maxRows"));
		form.add(new TextField("search"));
		form.add(new TextField("wstoken"));
		form.add(tMode = new TextField("mode"));
		tMode.setValue("json");
		form.add(new Submit("Enviar"));
		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			
			if(id == null) {
				return new MensagemResult("NOK", "Id invalido");
			}
			
			Usuario u = usuarioService.get(id.getLong());
			
			if(u == null) {
				return new MensagemResult("NOK", "Nenhum usuario encontrado com o id [" + id + "]");
			}
			
			List<Grupo> grupos = new ArrayList<Grupo>();
			for (GrupoUsuarios gu : u.getGrupos()) {
				grupos.add(gu.getGrupo());
			}
			
			if(grupos.size() > 0){
				List<EventoVO> eventosVo = new ArrayList<EventoVO>();
				List<Evento> eventos = eventoService.findByAtivo(grupos);
				
				for (Evento evento : eventos) {
					eventosVo.add(new EventoVO(evento));
				}
			
			return eventosVo;
			} else {
				return new MensagemResult("NOK","Usuário não possui nenhum Grupo");
			}
		}

		return new MensagemResult("NOK","Erro ao listar mensagens: " + getFormErrorMensagemResult(form));
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("evento", EventoVO.class);
	}
}
