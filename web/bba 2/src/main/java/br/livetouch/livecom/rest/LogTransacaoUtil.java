package br.livetouch.livecom.rest;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.ServletUtil;
import br.infra.util.ServletUtils;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogTransacao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.jobs.info.LogTransacaoJob;
import br.livetouch.livecom.utils.HostUtil;

public class LogTransacaoUtil {
	
	@Autowired
	protected LogService logService;

	@Autowired
	protected EmpresaService empresaService;

	@Autowired
	protected UsuarioService usuarioService;

	@Context
	HttpServletRequest request;
	@Context
	HttpServletResponse response;
	
	protected void saveLogTransacao(Throwable ex) {
		saveLogTransacao(null, ex);
	}
	
	protected void saveLogTransacao(String json) {
		saveLogTransacao(json, null);
	}
	
	protected void saveLogTransacao(String json, Throwable ex) {
		if (logService != null) {
			LogTransacao log = new LogTransacao();
			log.setDataInicio(new Date());
			log.setEmpresa(getEmpresa());
			log.setException(ex);
			log.setXml(json);
			log.setTransacaoRequest(request, getUsuarioRequest(),getHost() + getPath(), getRequestParams());
			log.finalizar();
			try {
				LogTransacaoJob.getInstance().addLogTransacao(log);
			} catch (Throwable e) {
			}
		}
	}

	public String getRequestParams() {
		return ServletUtil.getRequestParams(request);
	}
	
	public String getPath() {
		String path = (String) request.getAttribute("javax.servlet.include.servlet_path");
		String info = (String) request.getAttribute("javax.servlet.include.path_info");
		if (path == null) {
			path = request.getServletPath();
			info = request.getPathInfo();
		}

		if (info != null) {
			path += info;
		}
		return path;
	}

	protected Empresa getEmpresa() {
		String domain = getHost();
		Empresa e = empresaService.findDominio(domain);
		if ("livecom.livetouchdev.com.br".contains("domain") && e == null) {
			e = empresaService.get(1L);
		}
		if ((isLocalhost() || HostUtil.isLocalhost()) && e == null) {
			e = empresaService.get(1L);
		}
		if (e == null) {
			Usuario u = getUsuarioRequest();
			if (u != null) {
				e = u.getEmpresa();
			} else {
				e = empresaService.get(1L);
			}
		}
		return e;
	}

	protected String getHost() {
		return ServletUtils.getHost(request);
	}

	protected boolean isLocalhost() {
		return getHost().contains("localhost") && getHost().contains("livecom");
	}

	protected Usuario getUsuarioRequest() {
		String id = request.getParameter("user_id");
		if (id == null) {
			id = request.getParameter("user");
		}

		if (id == null) {
			return null;
		}
		Usuario usuario = null;
		if (StringUtils.isNotEmpty(id)) {
			if (StringUtils.isNumeric(id)) {
				usuario = usuarioService.get(Long.parseLong(id));
			} else {
				usuario = usuarioService.findByLogin(id);
			}
		}

		return usuario;
	}
}
