package br.livetouch.livecom.jobs.info;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.LogAuditoria;

public class LogAuditoriaJob {
	
	protected List<LogAuditoria> list;
	protected static LogAuditoriaJob instance = null;
	
	
	public LogAuditoriaJob() {
		list = new ArrayList<>();
	}
	
	public static LogAuditoriaJob getInstance() {
		if (instance == null) {
			instance = new LogAuditoriaJob();
		}		
		return instance;
	}

	public void add(LogAuditoria l) {
		list.add(l);
	}
	
	public List<LogAuditoria> getList() {
		return list;
	}
	
	public List<LogAuditoria> getListToSave() {
		ArrayList<LogAuditoria> logsList = new ArrayList<>(this.list);
		this.list.removeAll(this.list);
		return logsList;
	}
}
