package br.livetouch.livecom.web.pages.cadastro;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;


@Controller
@Scope("prototype")
public class UsuariosPage extends CadastrosPage {

	public boolean escondeChat = true;
	
	@Override
	public void onInit() {
		super.onInit();
	}


	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
