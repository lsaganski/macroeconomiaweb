package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.Card;
import br.livetouch.livecom.domain.CardButton;

public class CardVO implements Serializable{
	private static final long serialVersionUID = -6702688067486576715L;
	private String imageUrl;
	private String title;
	private String subTitle;
	private Long id;
	private List<CardButtonVO> buttons;

	public CardVO(Card card) {
		setId(card.getId());
		imageUrl = card.getImageUrl();
		title = card.getTitle();
		subTitle = card.getSubTitle();
		Set<CardButton> btns = card.getButtons();
		if(btns != null && btns.size() > 0) {
			this.buttons = new ArrayList<>();
			for (CardButton cardButton : btns) {
				this.buttons.add(new CardButtonVO(cardButton));
			}
		}
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<CardButtonVO> getButtons() {
		return buttons;
	}

	public void setButtons(List<CardButtonVO> buttons) {
		this.buttons = buttons;
	}
}
