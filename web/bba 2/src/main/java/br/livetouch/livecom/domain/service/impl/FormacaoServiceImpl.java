package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Formacao;
import br.livetouch.livecom.domain.repository.FormacaoRepository;
import br.livetouch.livecom.domain.service.FormacaoService;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class FormacaoServiceImpl implements FormacaoService {
	protected static final Logger log = Log.getLogger(FormacaoService.class);
	
	@Autowired
	private FormacaoRepository rep;

	@Override
	public Formacao get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Formacao f) throws DomainException {
		rep.saveOrUpdate(f);
	}

	@Override
	public List<Formacao> findAll() {
		return rep.findAll(true);
	}
	
	@Override
	public List<Formacao> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	@Override
	public void delete(Formacao f) throws DomainException {
		rep.delete(f);
	}

	@Override
	public void deleteAll() {
		rep.delete("delete from Formacao");
	}

	@Override
	public String csvFormacao(Empresa empresa) {
		List<Formacao> findAll = findAll(empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("CODIGO").add("NOME");
		for (Formacao c : findAll) {
			body.add(c.getCodigoDesc()).add(c.getNome());
			body.end();
		}
		return generator.toString();
	}
}
