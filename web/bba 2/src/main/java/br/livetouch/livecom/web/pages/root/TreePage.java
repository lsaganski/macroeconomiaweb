package br.livetouch.livecom.web.pages.root;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.web.pages.LivecomRootPage;
import net.sf.click.Context;
import net.sf.click.extras.tree.Tree;
import net.sf.click.extras.tree.TreeListener;
import net.sf.click.extras.tree.TreeNode;

@Controller
@Scope("prototype")
public class TreePage extends LivecomRootPage {

	public static final String TREE_NODES_SESSION_KEY = "ftpTreeNodes";

	protected Tree tree;
	private TreeNode treeNode;

	public String entrada;
	public String caminho;
	
	public String tomcat;
	public String pastaEntrada;
	public String trace = "";

	public boolean clear;

	@Override
	public void onInit() {
		super.onInit();
		
		tomcat = System.getProperty("catalina.home");
		pastaEntrada = ParametrosMap.getInstance(getEmpresaId()).get(Params.PASTAENTRADA);

		if(StringUtils.isEmpty(entrada)) {
			entrada = ParametrosMap.getInstance(getEmpresaId()).get(Params.PASTAENTRADA);
			if(StringUtils.isNotEmpty(caminho)) {
				entrada = caminho;
			}
		}
		
		try {
			tree = buildTree();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (clear) {
			clearSession();
		}
		
		addControl(tree);
		
		tree.addListener(new TreeListenerImplementation());

	}
	
	@Override
	public void onGet() {
		String selectId = getContext().getRequestParameter(Tree.SELECT_TREE_NODE_PARAM);
		String expandId = getContext().getRequestParameter(Tree.EXPAND_TREE_NODE_PARAM);
		if (selectId == null && expandId == null) {
			return;
		}

		TreeNode node = null;
		if (selectId != null) {
			node = tree.find(selectId);
		} else {
			node = tree.find(expandId);
		}
		if (!node.hasChildren()) {
			baixarArquivo(node);
		}
	}

	private void baixarArquivo(TreeNode node) {
		int level = node.getLevel();
		
		String caminho = "";
		String aux = "";
		String nomeArquivo = node.getValue().toString();
		for (int i = 0; i < level; i++) {
			TreeNode parent = node.getParent();
			String p = parent.getValue().toString();
			if (StringUtils.equals(p, "root")) {
				break;
			}
			aux = p + "/" + aux;
			node = parent;
		}

		caminho += "/" + aux;

		downloadFile(new File(caminho + nomeArquivo));
	}

	protected Tree createTree() {
		return new Tree("tree");
	}

	protected Tree buildTree() throws IOException {
		tree = createTree();

		TreeNode existingRootNode = loadNodesFromSession();

		if (existingRootNode != null) {
			if(StringUtils.isEmpty(caminho)) {
				tree.setRootNode(existingRootNode);
				return tree;
			}
			tree.setRootNode(null);
			entrada = caminho;
		} 
		
		if(entrada.equals("/")) {
			treeNode = new TreeNode("root");
		} else {
			treeNode = new TreeNode(entrada);
		}
		
		treeNode = createTree(treeNode, entrada, "", null);
		tree.setRootNode(treeNode);
		tree.expand(treeNode);
		storeNodesInSession(treeNode);

		return tree;
	}
	
	private TreeNode createTree(TreeNode nodeTree, String parentDir, String currentDir, String posicao) throws IOException {
		String dirToList = parentDir;
		if (!currentDir.equals("")) {
			dirToList += "/" + currentDir;
		}
		
		File f = new File(StringUtils.trim(dirToList));
		if (f.exists()) {
			File[] subFiles = f.listFiles();
			if (subFiles != null && subFiles.length > 0) {
				int contador = 0;
				for (File aFile : subFiles) {
					String currentFileName = aFile.getName();
					if (currentFileName.equals(".") || currentFileName.equals("..")) {
						// skip parent directory and directory itself
						continue;
					}
					String posLocal = StringUtils.isNotEmpty(posicao) ? posicao + "." + (contador + 1) : (contador + 1) + "";
					TreeNode node = new TreeNode(aFile.getName(), posLocal, nodeTree, aFile.isDirectory());
					if (aFile.isDirectory() && aFile.getParentFile() != null && aFile.canWrite()) {
						createTree(node, dirToList, currentFileName, posLocal);
					} 
					contador++;
				}
			}
		}
		return treeNode;
	}

	protected String getSessionKey() {
		return TREE_NODES_SESSION_KEY;
	}

	protected void storeNodesInSession(TreeNode rootNode) {
		if (tree.getRootNode() == null) {
			return;
		}
		getContext().getSession().setAttribute(getSessionKey(), rootNode);
	}

	protected void clearSession() {
		if (tree.getRootNode() == null) {
			return;
		}
		getContext().getSession().setAttribute(getSessionKey(), null);
	}

	protected TreeNode loadNodesFromSession() {
		return (TreeNode) getContext().getSession().getAttribute(getSessionKey());
	}
	
	private final class TreeListenerImplementation implements TreeListener {
		@Override
		public void nodeSelected(Tree tree, TreeNode node, Context context, boolean oldValue) {
			trace = "";
			if(node != null) {
				TreeNode parent = node.getParent();
				do {
					if(parent != null) {
						trace = "/" + parent.getValue() + trace;
					}
					parent = parent.getParent();
				} while (parent != null); 
				trace += "/" + node.getValue();
				trace = trace.replaceAll("//", "/");

			}
		}

		@Override
		public void nodeExpanded(Tree tree, TreeNode node, Context context, boolean oldValue) {
			trace = "";
			if(node != null) {
				TreeNode parent = node.getParent();
				do {
					if(parent != null) {
						trace = "/" + parent.getValue() + trace;
					}
					parent = parent.getParent();
				} while (parent != null); 
				trace += "/" + node.getValue();
				trace = trace.replaceAll("//", "/");
			}
		}

		@Override
		public void nodeDeselected(Tree tree, TreeNode node, Context context, boolean oldValue) {
			
		}

		@Override
		public void nodeCollapsed(Tree tree, TreeNode node, Context context, boolean oldValue) {
			
		}
	}
	
}