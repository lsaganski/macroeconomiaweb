package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;

@Repository
public interface NotificationUsuarioRepository extends net.livetouch.tiger.ddd.repository.Repository<NotificationUsuario> {

	List<Notification> findAllToSendPush();

	int updateSendPushNotification(Long id, List<Long> userIds);

	void markAllAsRead(Long id);

	
}