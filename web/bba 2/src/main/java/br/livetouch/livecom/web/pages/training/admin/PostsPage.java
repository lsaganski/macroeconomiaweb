package br.livetouch.livecom.web.pages.training.admin;


import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Post;
import net.livetouch.click.control.link.Link;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.Context;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Decorator;
import net.sf.click.extras.control.LinkDecorator;

/**
 * 
 */
@Controller
@Scope("prototype")
public class PostsPage extends TrainingAdminPage {

	public PaginacaoTable table = new PaginacaoTable();
	public ActionLink editLink = new ActionLink("editar", this,"editar");
	public Link deleteLink = new Link("deletar", this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public Post device;

	public List<Post> Posts;
	
	@Override
	public void onInit() {
		super.onInit();

		table();
//		System.out.println(table.getAttribute("class"));

		if (id != null) {
			device = postService.get(id);
		} else {
			device = new Post();
		}
	}
	
	private void table() {
		Column c = new Column("id");
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);
		
		c = new Column("categoria.path","Categoria");
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("titulo");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		c = new Column("videoPlayer");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		c.setDecorator(new Decorator() {
			@Override
			public String render(Object object, Context context) {
				Post p = (Post) object;
				return "<a href='"+p.getUrlVideo()+"'>"+p.getVideoPlayer()+"</a>";
			}
		});
		table.addColumn(c);
		
		c = new Column("tags");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes");
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean novo() {
		setRedirect(PostsPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		device = postService.get(id);
		setRedirect(PostPage.class,"id",String.valueOf(id));
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Post p = postService.get(id);
			postService.delete(getUsuario(),p, false);
			setRedirect(getClass());
			setFlashAttribute("msg", "Post deletado com sucesso.");
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = "Post possui relacionamentos. Não foi possível excluir.";
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		Posts = postService.findAll();

		// Count(*)
		int pageSize = 100;

		table.setPageSize(pageSize);
		table.setRowList(Posts);
	}
}
