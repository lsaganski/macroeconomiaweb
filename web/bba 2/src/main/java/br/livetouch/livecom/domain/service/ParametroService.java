package br.livetouch.livecom.domain.service;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.enums.TipoParametro;
import br.livetouch.livecom.domain.vo.ParametroVO;
import br.livetouch.livecom.email.EmailArgs;
import net.livetouch.tiger.ddd.DomainException;

public interface ParametroService extends Service<Parametro> {

	List<Parametro> findAll();
	Map<String,String> findAllParametrosKeyPair();
	
	Parametro findByNome(String nome, Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Parametro c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Parametro c) throws DomainException;

	EmailArgs getEmailArgs(Empresa empresa);

	String get(String string, Empresa empresa);
	
	ParametrosMap init();
	
	List<Parametro> findDefaultByNomeNotIn(List<String> inEmpresa);
	
	List<Parametro> findAllByEmpresa(Long id);
	
	List<ParametroVO> parametrosAutocomplete(Long empresa, String nome);
	
	List<Parametro> findAllParametros(Empresa empresa, Integer page, Integer max, boolean count) throws DomainException;
	
	List<Parametro> findAllParametrosByTipo(TipoParametro tipo, Empresa empresa);
	
	long getCount(Empresa e, int page, int max, boolean b) throws DomainException;
	List<Parametro> findAllParametros(Empresa e) throws DomainException;
	
	long getCount();
	
	List<Parametro> findAll(Long empresaId, TipoParametro mobile);
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(List<Parametro> parametros, Empresa empresa);
	
	List<Parametro> findParametrosMobile(Empresa empresa);

    List<Parametro> findMobileDefault();

    void saveCloneDefaults(List<Parametro> parametrosEmpresa, Empresa empresa);
}
