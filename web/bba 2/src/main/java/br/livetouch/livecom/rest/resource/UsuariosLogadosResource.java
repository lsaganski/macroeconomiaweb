package br.livetouch.livecom.rest.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.vo.UserInfoSimpleVO;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.rest.domain.MessageResult;

@Path("/v1/usuariosLogados")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class UsuariosLogadosResource extends MainResource {
	protected static final Logger log = Log.getLogger(UsuariosLogadosResource.class);
	
	@GET
	public Response findAll() {
		
		if(!hasPermissoes(true, ParamsPermissao.VIZUALIZAR_RELATORIOS, ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Empresa e = getEmpresa();
		List<UserInfoVO> users = Livecom.getInstance().getUsuarios(e);
		List<UserInfoSimpleVO> list = new ArrayList<>();
		for (UserInfoVO vo : users) {
			UserInfoSimpleVO s = new UserInfoSimpleVO(vo);
			list.add(s);
		}
		return Response.ok().entity(list).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissoes(true, ParamsPermissao.VIZUALIZAR_RELATORIOS, ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		UserInfoVO userInfo = Livecom.getInstance().getUserInfo(id);
		if(userInfo == null) {
			return Response.ok(MessageResult.error("Usário não encontrado")).build();
		}
		UserInfoSimpleVO vo = new UserInfoSimpleVO(userInfo);
		return Response.ok().entity(vo).build();
	}
	
}
