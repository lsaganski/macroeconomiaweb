package br.livetouch.livecom.files;

import java.util.Date;

public abstract class LivecomFile {

	public abstract String getUrl();

	public abstract String getFileName();

	public abstract String getExtension();

	public boolean equals(LivecomFile o) {
		return getUrl().equals(o.getUrl());
	}

	public abstract Date getLastModified();

	public abstract long getSize();

	public int hashCode() {
		return getUrl().hashCode();
	}

	public String toString() {
		return getUrl();
	}

	public abstract String getKey();
}
