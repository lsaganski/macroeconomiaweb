package br.livetouch.livecom.utils;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTPFile;

import br.livetouch.livecom.files.LivecomFile;

public class FTPFileLivecom extends LivecomFile {
	private FTPFile file;
	private String server;
	
	private String key;

	public FTPFileLivecom(FTPFile obj) {
		file = obj;
	}
	
	@Override
	public String getUrl() {
		return server +  getKey();
	}
	
	@Override
	public String getFileName() {
		return file.getName();
	}

	@Override
	public String getExtension() {
		String nome = getFileName();
		String[] split = StringUtils.split(nome, ".");
		if (split != null && split.length == 2) {
			return StringUtils.lowerCase(split[1]);
		}
		return nome;
	}
	
	@Override
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	

	@Override
	public Date getLastModified() {
		Calendar timestamp = file.getTimestamp();
		return timestamp.getTime();
	}

	@Override
	public long getSize() {
		return file.getSize();
	}
	
	
	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

}