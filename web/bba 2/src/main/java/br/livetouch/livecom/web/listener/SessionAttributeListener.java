package br.livetouch.livecom.web.listener;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import org.apache.log4j.Logger;

import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.vo.UserInfoVO;

public class SessionAttributeListener implements HttpSessionAttributeListener {
	private static Logger log = Logger.getLogger(SessionAttributeListener.class);

	@Override
	public void attributeAdded(HttpSessionBindingEvent event) {
		String attributeName = event.getName();
		Object attributeValue = event.getValue();
		log.debug("Session Attribute added : " + attributeName + " : " + attributeValue);
	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent event) {
		String attributeName = event.getName();
		Object attributeValue = event.getValue();
		log.debug("Session Attribute removed : " + attributeName + " : " + attributeValue);
		if(UserInfoVO.USER_INFO_KEY.equals(attributeName)) {
			if(attributeValue instanceof UserInfoVO) {
				UserInfoVO userInfo = (UserInfoVO) attributeValue;
				Livecom.getInstance().removeUserInfo(userInfo,"web");
			}
		}
	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent event) {
		String attributeName = event.getName();
		Object attributeValue = event.getValue();
		log.debug("Session Attribute replaced : " + attributeName + " : " + attributeValue);	
	}
}