package br.livetouch.livecom.web.pages.report;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.tiger.ddd.DomainException;

@Controller
@Scope("prototype")
public class DetalhesFavoritosReportPage extends RelatorioPage {

	public int page;
	public Long id;

	public Post post;

	@Override
	public void onInit() {
		super.onInit();

		if (id != null) {
			post = postService.get(id);
		}
	}

	@Override
	public void onPost() {
		super.onPost();
		RelatorioFiltro filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		Post post = postService.get(id);
		if (post != null) {
			String csv;
			if (filtro != null) {
				try {
					csv = reportService.csvDetalhesFavoritos(filtro);
					download(csv, "detalhes-relatorio-favoritos.csv");
				} catch (DomainException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
