package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.EmailReport;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.repository.EmailReportRepository;
import br.livetouch.livecom.domain.service.EmailReportService;
import br.livetouch.livecom.domain.vo.RelatorioEmailVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class EmailReportServiceImpl implements EmailReportService {

	@Autowired
	private EmailReportRepository rep;

	@Override
	public EmailReport get(Long id) {
		return rep.get(id);
	}

	@Override
	public List<EmailReport> findAll(Empresa e) {
		return rep.findAll(e);
	}

	@Override
	public void saveOrUpdate(EmailReport e) throws DomainException {
		rep.saveOrUpdate(e);
	}

	@Override
	public void delete(EmailReport e) throws DomainException {
		rep.delete(e);
	}

	@Override
	public List<RelatorioEmailVO> reportEmail(RelatorioFiltro filtro) throws DomainException {
		return rep.reportEmail(filtro);
	}

}
