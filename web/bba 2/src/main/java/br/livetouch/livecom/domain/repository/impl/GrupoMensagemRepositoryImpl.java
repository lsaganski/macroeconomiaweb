/*package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.GrupoMensagem;
import br.livetouch.livecom.domain.GrupoMensagemUsuarios;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.GrupoMensagemRepository;
import br.livetouch.livecom.domain.repository.UsuarioRepository;
import br.livetouch.livecom.domain.vo.GrupoMensagemVO;
import br.livetouch.livecom.domain.vo.UsuarioAutocompleteFiltro;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class GrupoMensagemRepositoryImpl extends StringHibernateRepository<GrupoMensagem> implements GrupoMensagemRepository {
	
	@Autowired
	UsuarioRepository usuarioRep;

	public GrupoMensagemRepositoryImpl() {
		super(GrupoMensagem.class);
	}

	@Override
	public GrupoMensagem findByNome(String nome) {
		Query q = createQuery("from GrupoMensagem where nome =  ?");
		q.setString(0, nome);
		q.setCacheable(true);
		GrupoMensagem g = (GrupoMensagem) q.uniqueResult();
		if (g != null) {
			deatach(g);
		}
		return g;
	}

	@Override
	public boolean exists(String nome) {
		long count = getCount(createQuery("select count(*) from GrupoMensagem where nome = ?").setParameter(0, nome));
		return count > 0;
	}

	@Override
	public void saveOrUpdate(GrupoMensagemUsuarios gu) {
		getSession().saveOrUpdate(gu);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> findAllUsersByID(Long id) {
		Query q = createQuery("select gu.usuario from GrupoMensagemUsuarios gu where gu.grupo.id = ? order by gu.usuario.nome ");
		q.setLong(0, id);
		q.setCacheable(true);
		List<Usuario> usuarios = (List<Usuario>) q.list();
		return usuarios;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GrupoMensagem> findAllByUser(Usuario u) {
		Query q = createQuery("select gu.grupo from GrupoMensagem gu where gu.userCreate.id = ? or gu.userUpdate.id = ?");
		q.setLong(0, u.getId());
		q.setCacheable(true);
		List<GrupoMensagem> list = (List<GrupoMensagem>) q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GrupoMensagemUsuarios> findAllGrupoMensagemUsuariosByGrupo(
			GrupoMensagem grupo) {
		Query q = createQuery("select gu from GrupoMensagemUsuarios gu where gu.grupo.id = ?");
		q.setLong(0, grupo.getId());
		q.setCacheable(true);
		List<GrupoMensagemUsuarios> list = (List<GrupoMensagemUsuarios>) q.list();
		return list;
	}

	@Override
	public void delete(GrupoMensagemUsuarios gu) {
		getSession().delete(gu);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GrupoMensagemVO> findByNomeAutocomplete(UsuarioAutocompleteFiltro filtro) {
		List<GrupoMensagemVO> grupos = null;
		StringBuffer sb = new StringBuffer();

		sb.append("select new br.livetouch.livecom.domain.vo.GrupoMensagemVO(gm) from GrupoMensagem gm ");

		if(StringUtils.isNotEmpty(filtro.getNome())) {
			sb.append(" where gm.nome like :nome ");
		}

		sb.append("order by gm.nome asc ");
		
		Query q = createQuery(sb.toString());

		if(StringUtils.isNotEmpty(filtro.getNome())) {
			q.setParameter("nome", "%" + filtro.getNome() + "%");
		}
		
		q.setMaxResults(filtro.getMax());
		
		q.setCacheable(true);

		grupos = q.list();
		
		return grupos;
	}
}*/