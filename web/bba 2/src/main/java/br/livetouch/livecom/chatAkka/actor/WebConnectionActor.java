package br.livetouch.livecom.chatAkka.actor;

import java.io.IOException;

import javax.websocket.Session;

import org.apache.log4j.Logger;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.io.Tcp.Write;
import br.infra.util.Log;
import br.livetouch.livecom.chat.webSocket.WebSocketChat;
import br.livetouch.livecom.chatAkka.AkkaChatServer;
import br.livetouch.livecom.chatAkka.protocol.AssociateWithUser;
import br.livetouch.livecom.chatAkka.protocol.JsonRawMessage;
import br.livetouch.livecom.chatAkka.protocol.RawMessage;
import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.vo.UserInfoVO;

public class WebConnectionActor extends UntypedActor {

	private static Logger log = Log.getLogger("WebConnectionActor");

	public ActorSelection userMessageRouter;
	/**
	 * UserActor logado pela web.
	 * 
	 * Associa a conexao da web, pois ao fechar a conexao da web, uma msg
	 * enviada ao akka para fechar conexao.
	 */
	public ActorRef userActor;

	public Session wsSession;

	private Long userId;
	
	public static ActorRef create(ActorSystem actorSystem, Session webSocketSession, String name) {
		// Cria o WebConnectionActor chamando o construtor abaixo (Session)
		ActorRef webUserActor = actorSystem.actorOf(Props.create(WebConnectionActor.class, webSocketSession), name);
		return webUserActor;
	}

	/**
	 * Construtor chamado por reflexão
	 * 
	 * @param wsSession
	 */
	public WebConnectionActor(Session wsSession) {
		this.wsSession = wsSession;
	}

	@Override
	public void preStart() throws Exception {
		userMessageRouter = AkkaFactory.getUserMessageRouter(getContext());
		super.preStart();
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof RawMessage) {
			log(">> WebConnectionActor.onReceive: " + msg);	

			/**
			 * Envia pro UserMessageRouter
			 */
			RawMessage rawMessage = (RawMessage) msg;
			
			AkkaHelper.logJson("web",userId," >> " + rawMessage);
			
			JsonRawMessage json = new JsonRawMessage(getSelf(), rawMessage, null,"web",userId);
			userMessageRouter.tell(json, getSelf());

		} else if (msg instanceof Write) {
			/**
			 * Escreve os dados enviados no Web Socket
			 */
			byte[] bytes = ((Write) msg).data().toArray();
			RawMessage rawMessage = new RawMessage(bytes);
			
			AkkaHelper.logJson("web",userId," << " + rawMessage);
			
			if(wsSession != null && rawMessage != null && userId != null) {
				WebSocketChat.sendMsg(wsSession,userId, rawMessage.toString());
			}
			
		} else if (msg instanceof AssociateWithUser) {
			userActor = ((AssociateWithUser) msg).userActor;
			userId = ((AssociateWithUser) msg).userId;
		} else if (msg instanceof UserActor.Stop) {
			stop();
		}
	}

	public void stop() throws IOException {
		log("Stop userActor");
		if (userActor != null) {
			userActor.tell(new UserActor.RemoveConnection(getSelf(),"web"), getSelf());
		}

		if (wsSession.isOpen()) {
			wsSession.close();
		}

		getContext().stop(getSelf());
	}
	
	private void log(String string) {
		int max = 200;
		if(string.length() > max) {
			string = string.substring(0,max);
		}
		if(userId != null) {
			UserInfoVO user = Livecom.getInstance().getUserInfo(userId);
			log.debug("("+user+"): " + string);
		}
		else {
			log.debug(string);
		}
	}
	
	public static void sendMessage(RawMessage rawMessage, Session wsSession) {
		if (AkkaChatServer.actorSystem != null) {
			ActorSelection webConnectionActor = AkkaFactory.getWebConnectionActor(wsSession);
			AkkaHelper.tellWithFuture(webConnectionActor, rawMessage, null);
		}
	}
}
