package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostView;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.PostViewRepository;

@Repository
public class PostViewRepositoryImpl extends LivecomRepository<PostView> implements PostViewRepository {

	public PostViewRepositoryImpl() {
		super(PostView.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PostView> findAllByUser(Usuario u) {
		StringBuffer sb = new StringBuffer("from PostView h where h.usuario.id=?");

		Query q = createQuery(sb.toString());

		q.setLong(0, u.getId());
		List<PostView> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PostView findByUserPost(Usuario u, Post p) {
		StringBuffer sb = new StringBuffer("from PostView pv where pv.usuario = :usuario and pv.post = :post");

		Query q = createQuery(sb.toString());

		q.setParameter("usuario", u);
		q.setParameter("post", p);
		List<PostView> list = q.list();
		PostView v = list.size() > 0 ? list.get(0) : null;
		return v;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PostView> findAllByPost(Post post) {
		StringBuffer sb = new StringBuffer("from PostView p where p.post.id=?");

		Query q = createQuery(sb.toString());

		q.setLong(0, post.getId());
		List<PostView> list = q.list();
		return list;
	}
	
	@Override
	public void insertPostView(List<Post> posts, Usuario user) {
		StringBuffer sb = new StringBuffer("INSERT INTO PostView (data, lido, visualizado, usuario, post, contador, contadorLido) select distinct :now, :lido, :visualizado, :user, p, :contador, :contadorLido");
				sb.append(" from Post p"); 
				sb.append(" where 1=1 ");
				sb.append(" and p.id not in (select distinct pv.post.id from PostView pv where pv.usuario.id = :userId and pv.post in (:posts))");	
				sb.append(" and p in (:posts)");	
			Query q = createQuery(sb.toString());
			q.setParameter("userId", user.getId());
			q.setParameter("user", user);
			q.setParameter("now", new Date());
			q.setParameter("lido", Boolean.FALSE);
			q.setParameter("visualizado", Boolean.TRUE);
			q.setParameter("contador", 0);
			q.setParameter("contadorLido", 0);
			q.setParameterList("posts", posts);
			q.executeUpdate();
	}
	
	@Override
	public void visualizarPosts(List<Post> posts, Usuario user) {
		StringBuffer sb = new StringBuffer("update PostView pv set pv.contador = (pv.contador + 1) ");
		sb.append(" where pv.post in (:posts) and pv.usuario = :usuario");

		Query q = createQuery(sb.toString());

		q.setParameter("usuario", user);
		q.setParameterList("posts", posts);
		
		q.executeUpdate();
	}

}