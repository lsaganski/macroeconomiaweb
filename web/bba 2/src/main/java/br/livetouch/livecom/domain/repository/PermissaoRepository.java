package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Permissao;
import br.livetouch.livecom.domain.vo.PerfilPermissaoVO;
import net.livetouch.tiger.ddd.repository.Repository;

public interface PermissaoRepository extends Repository<Permissao> {

	List<Permissao> findAll(Empresa empresa);

	List<Permissao> findByNome(String nome, Empresa empresa);

	Permissao findByNomeValid(Permissao nome, Empresa e);

	Permissao findByCodigoValid(Permissao permissao, Empresa empresa);

	List<PerfilPermissaoVO> findByPerfil(Perfil perfil);
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	void delete(Permissao p);

}