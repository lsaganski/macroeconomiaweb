package br.livetouch.livecom.web.pages.api;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.click.Page;

@Controller
@Scope("prototype")
public class IndexPage extends Page {

	@Override
	public void onInit() {
		super.onInit();
	}

}
