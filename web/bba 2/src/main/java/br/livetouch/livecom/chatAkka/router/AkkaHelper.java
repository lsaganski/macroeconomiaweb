package br.livetouch.livecom.chatAkka.router;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.websocket.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.UntypedActorContext;
import akka.dispatch.OnComplete;
import akka.io.Tcp.Command;
import akka.io.TcpMessage;
import akka.pattern.Patterns;
import akka.util.ByteString;
import akka.util.Timeout;
import br.infra.util.Log;
import br.livetouch.livecom.chatAkka.AkkaChatServer;
import br.livetouch.livecom.chatAkka.ChatLogMessages;
import br.livetouch.livecom.chatAkka.LivecomChatInterface;
import br.livetouch.livecom.chatAkka.actor.AkkaFactory;
import br.livetouch.livecom.chatAkka.actor.AkkaFactory.FindActorCallback;
import br.livetouch.livecom.chatAkka.actor.UserActor;
import br.livetouch.livecom.chatAkka.protocol.ChatUtils;
import br.livetouch.livecom.chatAkka.protocol.JsonRawMessage;
import br.livetouch.livecom.chatAkka.protocol.JsonReplicateRawMessage;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.utils.HostUtil;
import scala.concurrent.Future;

/**
 * Classe que centraliza principais regras.
 * 
 * @author rlech
 *
 */
public class AkkaHelper {
	private static Logger logJson = Log.getLogger("chat_json");
	private static Timeout timeout = new Timeout(3, TimeUnit.SECONDS);
	private static Logger log = Log.getLogger("LivecomChatAkkaHelper");
	// private static Logger log = Log.getLogger(LivecomChatAkkaHelper.class);

	/**
	 * Envia uma mensagem JSON de um Ator para outro.
	 */
	public static void tellTcpJson(ActorRef from, ActorRef to, JsonRawMessage raw) {
		if (raw == null) {
			log.error("tellTcpJson raw is null from : " + from + " to " + to);
			return;
		}

		ByteString byteString = raw.getByteString();

		Command write = TcpMessage.write(byteString);

		if (to != null) {
			to.tell(write, from);
		}
	}

	/**
	 * Envia o objeto para um determinado Actor do Akka
	 * 
	 * @param actorTo
	 * @param objMsg
	 *            - Qualquer objeto
	 * @param sender
	 *            - getSelf()
	 */
	public static void tellWithFuture(UntypedActorContext context, final ActorRef from, final ActorSelection to, final Object objMsg) {
		tellWithFuture(context, from, to, objMsg, null);
	}

	/**
	 * Envia o objeto para um determinado Actor do Akka.
	 * 
	 * 1) tellWithFuture Utilizado para converter ActorSelection para ActorRef
	 * 
	 * 2) Depois que converter chama o tell no ActorRef.
	 * 
	 * 3) Vai cair em UserActor.onReceive
	 * 
	 * 4) Que vai chamar tellTcpJson desta mesma classe
	 * 
	 * @param to
	 * @param objMsg
	 *            - Qualquer objeto
	 * @param from
	 *            - getSelf()
	 */
	public static void tellWithFuture(UntypedActorContext context, final ActorRef from, final ActorSelection to, final Object objMsg, final Runnable runnableError) {
		Future<ActorRef> userActorFuture = to.resolveOne(timeout);
		userActorFuture.onComplete(new OnComplete<ActorRef>() {
			@Override
			public void onComplete(Throwable failure, ActorRef actor) throws Throwable {
				boolean ok = failure == null;
				if (ok) {
					actor.tell(objMsg, from);
				} else {
					log.error("tellAsync error from [" + from + "] to [" + to + "] : " + objMsg);
					if (runnableError != null) {
						runnableError.run();
					}
				}
			}
		}, context.dispatcher());
	}

	public static void tellWithFuture(final ActorSelection to, final Object objMsg) {
		tellWithFuture(AkkaChatServer.actorSystem, to, objMsg, null);
	}

	public static void tellWithFuture(ActorSelection to, Object objMsg, Runnable runnableError) {
		tellWithFuture(AkkaChatServer.actorSystem, to, objMsg, null);
	}

	public static void tellWithFuture(ActorSystem actorSystem, final ActorSelection to, final Object objMsg, final Runnable runnableError) {
		if (actorSystem != null) {
			Future<ActorRef> userActorFuture = to.resolveOne(timeout);
			userActorFuture.onComplete(new OnComplete<ActorRef>() {
				@Override
				public void onComplete(Throwable failure, ActorRef actor) throws Throwable {
					boolean ok = failure == null;
					if (ok) {
						actor.tell(objMsg, actor);
					} else {
						log.error("AKKA: tellWithFuture actorSystem error to [" + to + "] : " + objMsg);
						if (runnableError != null) {
							runnableError.run();
						}
					}
				}
			}, actorSystem.dispatcher());
		}
	}

	public static void sendToRouter(UntypedActorContext context, ActorSelection livecomRouter, final Object obj, OnComplete<Object> onComplete) {
		Timeout timeout = new Timeout(60, TimeUnit.SECONDS);

		Future<Object> future = Patterns.ask(livecomRouter, obj, timeout);

		// Depois de salvar no livecom, avisa sender que server
		// recebeu a mensagem
		future.onComplete(onComplete, context.dispatcher());
	}

	public static void sendMsgAdmin(MensagemVO mensagem, List<Long> usersToId) {
		JsonRawMessage jsonWasRead = ChatUtils.getJsonReceiveMessageAdmin(mensagem);
		AkkaHelper.sendToOnlineFriends(jsonWasRead, usersToId);
	}

	/**
	 * Envia o json para todos amigos online
	 * 
	 * @param jsonWasRead
	 * @param usersToId
	 */
	public static void sendToOnlineFriends(JsonRawMessage jsonWasRead, List<Long> usersToId) {
		// Envia pra todos amigos online
		List<Long> onlineIds = AkkaHelper.getOnlineUsersByids(usersToId);
		for (Long id : onlineIds) {
			if (id != null) {
				ActorSelection userActor = AkkaFactory.getUserActor(id);
				AkkaHelper.tellWithFuture(userActor, jsonWasRead);
			}
		}
	}
	
	public static void sendMessageDenied(Long userId, Long cId, Long gId) throws JSONException {
		JsonRawMessage raw = ChatUtils.getJsonDenied(cId, gId);
		ActorSelection userActor = AkkaFactory.getUserActor(userId);
		AkkaHelper.tellWithFuture(userActor, raw);
	}

	public static List<Long> getOnlineUsersByids(List<Long> idsFriends) {
		List<Long> ids = new ArrayList<Long>();
		for (Long id : idsFriends) {
			if (isOnline(id)) {
				ids.add(id);
			}
		}
		return ids;
	}

	public static boolean isOnline(Long id) {
		boolean online = Livecom.getInstance().isUsuarioLogadoChat(id);
		return online;
	}

	public static void tellMessageToUsers(MensagemVO mensagem, List<UsuarioVO> usuarios) {
		if (mensagem != null && mensagem.getFrom() != null && usuarios != null && usuarios.size() > 0) {
			List<UsuarioVO> usersOffline = new ArrayList<UsuarioVO>();
			for (UsuarioVO usuario : usuarios) {
				Long fromId = mensagem.getFromId();
				if (fromId.equals(usuario.getId())) {
					continue;
				}

				boolean isLogadoChat = Livecom.getInstance().isUsuarioLogadoChat(usuario.getId());
				if (isLogadoChat) {
					ActorSelection actorTo = AkkaFactory.getUserActor(usuario.getId());
					if (actorTo != null) {
						JsonRawMessage json = ChatUtils.getJsonMsgJsonRawMessage(mensagem);
						AkkaHelper.tellWithFuture(actorTo, json);
					}
				} else {
					usersOffline.add(usuario);
				}
			}
			if (!usersOffline.isEmpty()) {
				log.debug("AkkaHelper: Enviando push para [" + usersOffline.size() + "] users. ");
				getLivecomInterface().sendPush(mensagem, usersOffline);
			}
		}
	}

	public static void stopWebConnectionActor(Session wsSession) {
		ActorSystem actorSystem = AkkaChatServer.getActorSystem();
		if (actorSystem != null) {
			ActorSelection webConnectionActor = AkkaFactory.getWebConnectionActor(wsSession);

			AkkaHelper.tellWithFuture(actorSystem, webConnectionActor, new UserActor.Stop(), null);
		}
	}

	public static void stopActor(ActorRef actor) {
		if (actor != null) {
			actor.tell(new UserActor.Stop(), ActorRef.noSender());
		}
	}

	public static void stopActor(Long userId) {
		ActorSelection actor = AkkaFactory.getUserActor(userId);
		AkkaFactory.findActorAsync(actor, new FindActorCallback() {
			@Override
			public void actorFound(ActorRef actorRef) {
				stopActor(actorRef);
			}

			@Override
			public void actorNotFound() {

			}
		});
	}

	public static void removeConnection(ActorRef actor, ActorRef conn, String so) {
		if (actor != null) {
			log.debug("Chamando remove [" + actor + "], so: " + so);
			actor.tell(new UserActor.RemoveConnection(conn, so), ActorRef.noSender());
		}
	}

	private static LivecomChatInterface getLivecomInterface() {
		return AkkaChatServer.getLivecomInterface();
	}

	public static void logJson(String so, Long userId, String s) {
		if(s == null) {
			return;
		}
		
		String json = StringUtils.replace(s, ">>", "");
		json = StringUtils.replace(json, "<<", "");
		json = json.trim();
		String code = ChatUtils.getCode(json);
		if(code.endsWith("CC") || code.endsWith("AC")) {
			// ignora callbacks
			return;
		}
		if("userStatus".equals(code)) {
			// ignora callbacks
			return;
		}
		
		int max = 200;
		if (s.length() > max) {
			s = s.substring(0, max);
		}
		boolean localhost = HostUtil.isLocalhost();
		if (userId != null) {
			UserInfoVO user = Livecom.getInstance().getUserInfo(userId);
			if (user != null) {
				String loginInfo = "[" + so + "/" + user.getLogin() + "]";
				print(loginInfo+"\t"+s);
				
			} else {
				String loginInfo = "[" + so + "/" + userId + "]";
				print(loginInfo+"\t"+s);
			}
		} else {
			print(s);
			ChatLogMessages.getInstance().log(s);
			if(localhost) {
				System.out.println(s);
			}
		}
	}
	
	private static void print(String s) {
		logJson.debug(s);
		ChatLogMessages.getInstance().log(s);
		if(HostUtil.isLocalhost()) {
			System.err.println(s);
		}
	}

    public static void sendNotificationWasRead(Long userId, Long postId) {
        JsonRawMessage raw = ChatUtils.getJsonNotificationWasRead(postId);
        ActorSelection userActor = AkkaFactory.getUserActor(userId);
        ActorRef actorRef = AkkaFactory.findActorSync(userActor, 60);
        JsonReplicateRawMessage replicate = new JsonReplicateRawMessage(actorRef, userId, raw);
        AkkaHelper.tellWithFuture(userActor, replicate);
    }

    public static void sendNotificationReadAll(Long userId) {
        JsonRawMessage raw = ChatUtils.getJsonNotificationReadAll();
        ActorSelection userActor = AkkaFactory.getUserActor(userId);
        ActorRef actorRef = AkkaFactory.findActorSync(userActor, 60);
        JsonReplicateRawMessage replicate = new JsonReplicateRawMessage(actorRef, userId, raw);
        AkkaHelper.tellWithFuture(userActor, replicate);
    }
}
