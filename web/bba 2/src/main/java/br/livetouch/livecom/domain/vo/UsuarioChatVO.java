package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Usuario;

public class UsuarioChatVO implements Serializable {
	private static final long serialVersionUID = 8837621952601105868L;

	public Long id;
	public String login;
	public String nome;
	public String email;
	public boolean adminChat;

	public String cargo;
	public String telefoneFixo;
	public String telefoneCelular;
	public String status;

	public String urlFotoUsuario;
	public String urlFotoUsuarioThumb;

	public String genero;
	public String funcao;
	public String estado;
	public String dataNasc;

	public void setUsuario(Usuario u) {
		this.id = u.getId();
		this.login = u.getLogin();
		this.nome = u.getNome();
		this.email = u.getEmail();
		this.cargo = u.getCargo();
		this.telefoneFixo = u.getTelefoneFixo();
		this.telefoneCelular = u.getTelefoneCelular();
		this.status = u.getStatusString();
		this.urlFotoUsuario = u.getUrlFotoUsuario();
		this.urlFotoUsuarioThumb = u.getUrlThumb();
		this.genero = u.getGenero();
		if (u.getFuncao() != null) {
			this.funcao = u.getFuncao().getNome();
		}
		this.estado = u.getEstado();
		this.dataNasc = u.getDataNascString();
	}

	public static List<UsuarioChatVO> toList(Grupo grupo, List<Usuario> usuarios) {
		List<UsuarioChatVO> users = new ArrayList<UsuarioChatVO>();
		if (usuarios != null) {
			for (Usuario usuario : usuarios) {
				UsuarioChatVO vo = new UsuarioChatVO();
				vo.setUsuario(usuario);
				if (usuario.isAdmin(grupo)) {
					vo.adminChat = true;
				}
				users.add(vo);
			}
		}
		return users;
	}
}
