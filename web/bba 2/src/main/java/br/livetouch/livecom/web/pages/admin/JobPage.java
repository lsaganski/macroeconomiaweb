package br.livetouch.livecom.web.pages.admin;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.sf.click.control.ActionButton;

@Controller
@Scope("prototype")
public class JobPage extends LivecomAdminPage{

	public JobInfo jobInfo;
	
	public ActionButton limpar = new ActionButton("limpar", getMessage("limpar.label"), this,"limpar");
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();
		
		limpar.setAttribute("class", "btn btn-primary");
		
		jobInfo = JobInfo.getInstance(getEmpresaId());
		
		addModel("now", new Date());
	}
	
	public boolean limpar() {
		jobInfo.list.clear();
		
		refresh();
		
		return true;
	}
}
