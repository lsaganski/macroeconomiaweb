package br.livetouch.livecom.jobs;

import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * V2: Varre a tabela de Notification para enviar Pushs.
 * 
 * @author rlech
 *
 */
@Service
public class ForcePushJob extends PushMuralJob {
	@Override
	protected void execute(Map<String, Object> params) throws Exception {
		super.execute(params);
	}
}
