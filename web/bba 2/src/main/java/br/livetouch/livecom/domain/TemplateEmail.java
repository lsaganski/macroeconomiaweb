package br.livetouch.livecom.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.livetouch.livecom.domain.enums.TipoTemplate;

@Entity
public class TemplateEmail extends net.livetouch.tiger.ddd.Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3867896454432361135L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "TEMPLATEEMAIL_SEQ")
	private Long id;

	private String nome;
	
	private String nomeHtml;
	
	@Column(columnDefinition = "clob")
	private String html;

	private Date dateCreated;
	private Date dateUpdated;
	
	private TipoTemplate tipoTemplate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getNomeHtml() {
		return nomeHtml;
	}

	public void setNomeHtml(String nomeHtml) {
		this.nomeHtml = nomeHtml;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public TipoTemplate getTipoTemplate() {
		return tipoTemplate;
	}

	public void setTipoTemplate(TipoTemplate tipoTemplate) {
		this.tipoTemplate = tipoTemplate;
	}
	
	public String getHtmlFormatado() {
		String msg = "";
		msg = StringUtils.isNotEmpty(html) ? html : "Email sem conteudo!";
		msg = msg.replace("\n", "").replace("\r", "").replace(System.lineSeparator(), "");
		return msg;
	}

}