package br.livetouch.livecom.domain;


public abstract class ParamsPermissao {
	
	/**
	 * Publicar
	 */
	public static final String PUBLICAR = "publicar";

	/**
	 * Excluir Postagem
	 */
	public static final String EXCLUIR_PUBLICACAO = "excluir_publicacao";
	
	/**
	 * Editar Postagem
	 */
	public static final String EDITAR_PUBLICACAO = "editar_publicacao";
	
	/**
	 * Comentar publicações
	 */
	public static final String COMENTAR = "comentar";
	
	/**
	 * Visualizar comentarios
	 */
	public static final String VIZUALIZAR_COMENTARIOS = "visualizar_comentarios";

	/**
	 * Visualizar relatorios
	 */
	public static final String VIZUALIZAR_RELATORIOS = "visualizar_relatorios";

	/**
	 * Visualizar conteudo
	 */
	public static final String VIZUALIZAR_CONTEUDO = "visualizar_conteudo";
	
	/**
	 * Curtir publicações
	 */
	public static final String CURTIR_PUBLICACAO = "curtir_publicacao";
	
	/**
	 * Curtir comentários
	 */
	public static final String CURTIR_COMENTARIOS = "curtir_comentarios";

	/**
	 * Enviar Mensagem
	 */
	public static final String ENVIAR_MENSAGEM = "enviar_mensagem";

	/**
	 * Gerenciar Arquivos
	 */
	public static final String ARQUIVOS = "arquivos";

	/**
	 * Cadastrar Usuarios
	 */
	public static final String CADASTRAR_USUARIOS = "cadastrar_usuarios";
	
	/**
	 * Cadastrar Grupos
	 */
	public static final String CADASTRAR_GRUPOS = "cadastrar_grupos";

	/**
	 * Cadastrar codigos
	 */
	public static final String CADASTRAR_CODIGOS = "cadastrar_codigos";

	/**
	 * Perfil padrão
	 */
	public static final String PADRAO = "padrao";
	
	/**
	 * Acesso Cadastros
	 */
	public static final String ACESSO_CADASTROS = "acesso_cadastros";

	/**
	 * Editar Empresa
	 */
	public static final String EDITAR_EMPRESA = "editar_empresa";

	/**
	 * Excluir Empresa
	 */
	public static final String EXCLUIR_EMPRESA = "excluir_empresa";

	/**
	 * Configurações avançadas
	 */
	public static final String CONFIGURACOES_AVANCADAS = "configuracoes_avancadas";

	/**
	 * Enviar emails
	 */
	public static final String ENVIAR_EMAILS = "enviar_email";
	
	/**
	 * Importar Arquivos
	 */
	public static final String IMPORTACAO = "importacao";

	/**
	 * Exportar Arquivos
	 */
	public static final String EXPORTACAO = "exportacao";

	/**
	 * Visualizar todos os posts sem participar dos grupos
	 */
	public static final String VISUALIZAR_TODOS_OS_POSTS = "visualizar_todos_os_posts";

	/**
	 * Visualizar todos os posts sem participar dos grupos
	 */
	public static final String VISUALIZAR_TODOS_OS_GRUPOS = "visualizar_todos_os_grupos";

	/**
	 * Postar em todos grupos sem participar
	 */
	public static final String POSTAR_EM_TODOS_OS_GRUPOS = "postar_em_todos_os_grupos";

	/**
	 * Cadastrar newsletter
	 */
	public static final String CADASTRAR_NEWSLETTER = "cadastrar_newsletter";
	
}
