package br.livetouch.livecom.jobs;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.infra.util.Log;

public class SimplesJob implements Job {

	protected static final Logger log = Log.getLogger(SimplesJob.class);


	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println(">> SimplesJob ["+Thread.currentThread()+"]: " + new Date());
		for (int i = 0; i < 10; i++) {
			System.out.println(i + " - SimplesJob ["+Thread.currentThread()+"]");
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("<< SimplesJob ["+Thread.currentThread()+"]: " + new Date());
	}
}
	