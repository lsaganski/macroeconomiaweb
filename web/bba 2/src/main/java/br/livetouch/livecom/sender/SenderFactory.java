package br.livetouch.livecom.sender;

import org.springframework.context.ApplicationContext;

/**
 * http://docs.aws.amazon.com/ses/latest/DeveloperGuide/mailbox-simulator.html
 * 
 * http://docs.aws.amazon.com/ses/latest/DeveloperGuide/deliverability-and-ses.html
 * 
 * @author Ricardo Lecheta
 *
 */
public class SenderFactory {

	public static Sender getSender(final ApplicationContext context, Class<? extends Sender> cls)  {
		Sender bean = (Sender) context.getBean(cls);
		return bean;
	}
}
