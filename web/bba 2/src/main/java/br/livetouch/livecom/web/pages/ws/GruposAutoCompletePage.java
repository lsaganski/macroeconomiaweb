package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 
 */
@Controller
@Scope("prototype")
public class GruposAutoCompletePage extends GruposPage {

	// deprecated
	// usar o grupos
	
}
