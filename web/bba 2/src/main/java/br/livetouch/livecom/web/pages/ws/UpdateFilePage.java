package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Usuario;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class UpdateFilePage extends WebServiceXmlJsonPage {

	public Form form = new Form();

	private LongField tId;

	private TextField tNome;
	private TextField tDesc;

	private LongField tUserId;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		form.add(tId = new LongField("id","id"));
		form.add(tUserId = new LongField("user_id","user_id - Usuário que postou o arquivo", true));
		form.add(tNome = new TextField("nome","nome"));
		form.add(tDesc = new TextField("descricao","descrição"));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		TextField tMode = new TextField("mode", "mode: xml, json");
		tMode.setValue("json");
		form.add(tMode);
		
		form.add(new Submit("Upload File"));
		
		tId.setFocus(true);
		setFormTextWidth(form,350);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			
			long userId = tUserId.getLong();
			Usuario usuario = usuarioService.get(userId);
			if (usuario == null) {
				return new MensagemResult("ERROR", "Usuário inválido.");
			}

			Arquivo a = null;

			Long id = tId.getLong();
			if(id == null) {
				return new MensagemResult("ERROR", "Arquivo não encontrado.");
			}
			
			a = arquivoService.get(new Long(id));
			
			if(a == null) {
				return new MensagemResult("ERROR", "Arquivo não encontrado.");
			}
			
			boolean admin = usuario.isAdmin();
			if(!usuario.equals(a.getUsuario()) && !admin) {
				return new MensagemResult("ERROR", "Sem permissão para alterar o arquivo.");
			}
			
			String nome = tNome.getValue();
			String desc = tDesc.getValue();
			a.setNome(nome);
			a.setDescricao(desc);
			
			arquivoService.saveOrUpdate(a);
			
			return new MensagemResult("OK", "Arquivo atualizado com sucesso.");
		}

		return new MensagemResult("Erro", "Form inválido.");
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
