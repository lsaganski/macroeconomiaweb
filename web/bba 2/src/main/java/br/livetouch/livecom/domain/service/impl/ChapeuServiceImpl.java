package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Chapeu;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.repository.ChapeuRepository;
import br.livetouch.livecom.domain.service.ChapeuService;
import br.livetouch.livecom.domain.vo.ChapeuFilter;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ChapeuServiceImpl implements ChapeuService {
	@Autowired
	private ChapeuRepository rep;

	@Override
	public Chapeu get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Chapeu c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Chapeu> findAll() {
		return rep.findAll();
	}

	@Override
	public void delete(Chapeu c) throws DomainException {
		try {
			rep.delete(c);
		} catch (DataIntegrityViolationException e) {
			throw new DomainMessageException("validate.chapeu.delete.possui.relacionamentos");
		}
	}

	@Override
	public List<Chapeu> findAllByFilter(ChapeuFilter filter) {
		return rep.findAllByFilter(filter);
	}
	
	@Override
	public String csvChapeu() {
		List<Chapeu> findAll = findAll();
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("NOME").add("CATEGORIA");
		for (Chapeu c : findAll) {
			body.add(c.getNome()).add(c.getCategoria() != null ? c.getCategoria().getCodigoDesc() : "");
			body.end();
		}
		return generator.toString();
	}
	
	@Override
	public Chapeu findByName(Chapeu chapeu, Empresa empresa) {
		return rep.findByName(chapeu, empresa);
	}
}
