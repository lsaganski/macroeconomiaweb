package br.livetouch.livecom.domain.repository;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Faq;

@Repository
public interface FaqRepository extends net.livetouch.tiger.ddd.repository.Repository<Faq> {
	
}