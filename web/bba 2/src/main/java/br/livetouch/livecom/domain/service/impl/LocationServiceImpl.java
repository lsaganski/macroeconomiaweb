package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Location;
import br.livetouch.livecom.domain.repository.LocationRepository;
import br.livetouch.livecom.domain.service.LocationService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class LocationServiceImpl implements LocationService {
	@Autowired
	private LocationRepository rep;

	@Override
	public Location get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Location l) throws DomainException {
		rep.saveOrUpdate(l);
	}

	@Override
	public List<Location> findAll() {
		return rep.findAll();
	}

	@Override
	public void delete(Location d) throws DomainException {
		rep.delete(d);
	}
}
