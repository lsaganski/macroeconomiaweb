package br.livetouch.livecom.rest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MultivaluedMap;

import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.glassfish.jersey.server.oauth1.DefaultOAuth1Provider;
import org.glassfish.jersey.server.oauth1.OAuth1ServerFeature;
import org.glassfish.jersey.server.oauth1.OAuth1ServerProperties;

import br.livetouch.livecom.rest.oauth.LivecomOAuthProvider;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

public class MyApplication extends javax.ws.rs.core.Application {

	public final static String NAME = "LIVECOM";
	public final static String CONSUMER_KEY = "6311f16a-0206-4841-b930-7e40a9db3a2b";
	public final static String CONSUMER_SECRET = "771adb26-d046-4f32-b710-2f8e027b5f77";
	
	public MyApplication() {
		BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8080");
        beanConfig.setBasePath("/livecom/rest");
        beanConfig.setResourcePackage("br.livetouch.livecom");
        beanConfig.setScan(true);
	}
	
	@Override
	public Set<Object> getSingletons() {
		Set<Object> singletons = new HashSet<>();
		DefaultOAuth1Provider oauthProvider = new DefaultOAuth1Provider();
		MultivaluedMap<String, String> attributes = new MultivaluedStringMap();
		oauthProvider.registerConsumer(NAME, CONSUMER_KEY,CONSUMER_SECRET,attributes);
		
		LivecomOAuthProvider provider = new LivecomOAuthProvider();
		singletons.add(new OAuth1ServerFeature(provider));
		singletons.add(new MultiPartFeature());
		return singletons;
	}

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> s = new HashSet<Class<?>>();
		// Segurança
		s.add(RolesAllowedDynamicFeature.class);
		//s.add(LoggingFilter.class);
		s.add(XSSFilter.class);
		s.add(CORSResponseFilter.class);
		s.add(Interceptor.class);
		s.add(LogTransacaoResponse.class);
		s.add(ApiListingResource.class);
		s.add(SwaggerSerializers.class);
		s.add(JacksonFeature.class);

		return s;
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> properties = new HashMap<>();
		// Configura o pacote para fazer scan das classes com anotações REST.
		properties.put(ServerProperties.PROVIDER_PACKAGES, "br.livetouch.livecom");
		properties.put(ServerProperties.WADL_FEATURE_DISABLE, true);
		// Ativa as URLs /requestToken e /accessToken do Jersey.
		properties.put(OAuth1ServerProperties.ENABLE_TOKEN_RESOURCES, Boolean.TRUE);
//		properties.put(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
//		properties.put(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true);
		return properties;
	}
}
