package br.livetouch.livecom.chatAkka;//package br.com.livetouch.livecom.chat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import akka.actor.ActorRef;
import br.infra.util.Log;
import br.livetouch.livecom.chatAkka.protocol.ChatUtils;
import br.livetouch.livecom.chatAkka.protocol.JsonRawMessage;
import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.chatAkka.router.ChatVO;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.Card;
import br.livetouch.livecom.domain.CardButton;
import br.livetouch.livecom.domain.ConversaNotification;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.Location;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoPush;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.CardService;
import br.livetouch.livecom.domain.service.ConversaNotificationService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.MensagemService;
import br.livetouch.livecom.domain.service.StatusMensagemUsuarioService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.ControleVersaoVO;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.PushChatVO;
import br.livetouch.livecom.domain.vo.UpdateStatusMensagemResponseVO;
import br.livetouch.livecom.domain.vo.UploadResponse;
import br.livetouch.livecom.domain.vo.UserInfoSession;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.jobs.PushChatJob;
import br.livetouch.livecom.push.PushNotificationVO;
import br.livetouch.livecom.utils.UploadHelper;
import net.livetouch.extras.util.CripUtil;
import net.livetouch.tiger.ddd.DomainException;

/**
 * @author rlech
 *
 */
@Component
public class LivecomChatInterfaceImpl implements LivecomChatInterface {

   private static Logger log = Log.getLogger("interface");
   protected static String API_GOOGLE_MAP_STATIC = "https://maps.googleapis.com/maps/api/staticmap";

   @Autowired
   private UsuarioService usuarioService;

   @Autowired
   private MensagemService mensagemService;

   @Autowired
   private ArquivoService arquivoService;
   
   @Autowired
   private GrupoService grupoService;
   
   @Autowired
   private ConversaNotificationService conversaNotificationService;
   
   @Autowired
   private StatusMensagemUsuarioService statusMensagemUsuarioService;
   
   @Autowired private CardService cardService;
   
   @Override
   public Usuario findByLogin(String login) {
      Usuario u = usuarioService.findByLogin(login);
      org.hibernate.Hibernate.initialize(u.getPermissao());
      org.hibernate.Hibernate.initialize(u.getEmpresa());
      return u;
   }

   @Override
   
   public Usuario findByUserId(Long id) {
      Usuario u = usuarioService.get(id);
      return u;
   }

   @Override
   public List<UsuarioVO> findUsersFromConversation(Long conversaID, Long userID, boolean includeUser) {
      List<Usuario> usuarios = mensagemService.findUsersFromConversation(conversaID, userID, includeUser);
      List<UsuarioVO> list = UsuarioVO.setUsuarios(usuarios);
      return list;
   }
   
   @Override
   public List<Long> findIdsUsersFromConversation(Long conversaID, Long userID, boolean includeUser) {
      List<Usuario> usuarios = mensagemService.findUsersFromConversation(conversaID, userID, includeUser);
      List<Long> ids = new ArrayList<>();
      for (Usuario usuario : usuarios) {
         ids.add(usuario.getId());
      }
      return ids;
   }

   @Override
   public MensagemVO   saveMessage(ChatVO c) throws Exception {
      /**
       * Salvar a msg no banco
       */
      long idUserFrom = c.from;
      long identifier = c.identifier;
      
      String msg = c.msg;
      long conversaId = c.conversaId;
      Set<ChatFile> arquivos64 = c.filesBase64;
      Double lat = c.lat;
      Double lng = c.lng;
      
      log.debug("saveMessage '" + msg + "' do usuario " + idUserFrom + " para a conversa " + conversaId);
      
      MensagemConversa conversa = mensagemService.getMensagemConversa(conversaId);
      if(conversa == null) {
         throw new DomainException("Conversa ["+conversaId+"] não encontrada");
      }
      
      Usuario usuario = usuarioService.get(idUserFrom);
      Empresa empresa = usuario.getEmpresa();
      ParametrosMap map = null;
      if(empresa != null) {
         map = ParametrosMap.getInstance(empresa.getId());
      } else {
         map = ParametrosMap.getInstance(1L);
      }

      /**
       * Verifica se ja nao salvou essa msg
       */
      Mensagem mensagem = mensagemService.findByIdentifier(conversaId, idUserFrom, identifier);
      if(mensagem != null) {
         log.debug("Idenfier ja existe [idUSerFrom: "+idUserFrom+", conversaId: "+ conversaId +", identifier: "+ identifier +"]");
         // Mensagem já encontrada com esse identifier
         // Isso pode acontecer ao ficar relogio azul e server nao confirmou.
         // Depois do timeout o celular vai enviar novamente a msg.
         // Neste caso o servidor vai verificar a duplicata
         MensagemVO vo = new MensagemVO();
         vo.setMensagem(mensagem, usuario);
         return vo;
      }

      JSONArray listCards = c.cards;
      Set<Card> cards = null;
      if(listCards != null && listCards.length() > 0) {
         cards = new HashSet<Card>();
         for(int i = 0; listCards.length() > i; i++) {
            JSONObject item = listCards.getJSONObject(i);
            Card card = new Card();
            card.setImageUrl(item.optString("imageUrl"));
            card.setTitle(item.optString("title"));
            card.setSubTitle(item.optString("subTitle"));
            cardService.saveOrUpdate(card);
            
            JSONArray jsonBtns = item.getJSONArray("buttons");
            if(jsonBtns != null && jsonBtns.length() > 0) {
               Set<CardButton> cardButtons = new HashSet<CardButton>();
               for(int j = 0; jsonBtns.length() > j; j++) {
                  JSONObject btnJson = jsonBtns.getJSONObject(j);
                  CardButton cardButton = new CardButton();
                  cardButton.setAction(btnJson.optString("action"));
                  cardButton.setLabel(btnJson.optString("text"));
                  cardButton.setValue(btnJson.optString("value"));
                  cardButton.setCard(card);
                  cardService.saveOrUpdate(cardButton);
                  cardButtons.add(cardButton);
               }
               card.setButtons(cardButtons);
            }
            cards.add(card);
         }
      }
      
      
      List<Long> arquivosIds = new ArrayList<Long>();
      if(arquivos64 != null) {
         for (ChatFile chatFile : arquivos64) {
            String imgBytes = chatFile.getBase64().substring(chatFile.getBase64().indexOf(",") + 1);

            // BASE64Decoder decoder = new BASE64Decoder();
            // byte[] decodedBytes = decoder.decodeBuffer(imageDataBytes);

            byte[] decodeBase64 = Base64.decodeBase64(imgBytes);

            String nome = chatFile.getNome();

            boolean isLambda = ParametrosMap.getInstance(empresa).getBoolean(Params.AMAZON_LAMBDA_CHAT_RESIZE_ON, false);
            
            //validar se é imagem também
            String dirUpload = isLambda ? empresa.getCodigo() : "filesChat";

            long timestamp = new Date().getTime();
            UploadResponse uploadInfo = UploadHelper.uploadFile(usuario, nome, decodeBase64, decodeBase64.length, dirUpload, isLambda, timestamp);

            Arquivo arquivo = uploadInfo.arquivo;
            arquivo.setTipo(chatFile.getTipo());

            if(!isLambda) {
                arquivo = UploadHelper.createThumbs(map, arquivo, uploadInfo, dirUpload, timestamp);
            } else {
                arquivo = UploadHelper.createThumbsLambda(map, arquivo, uploadInfo, dirUpload, usuario, timestamp);
            }
            
            arquivoService.saveOrUpdate(arquivo);
            
            Set<ArquivoThumb> thumbs = arquivo.getThumbs();
            if (thumbs != null) {
                UploadHelper.log.debug(thumbs.size() + " thumbs criadas para arquivo: " + arquivo.getId());
                for (ArquivoThumb t : thumbs) {
                    t.setArquivo(arquivo);
                    arquivoService.saveOrUpdate(t);
                }
            }
            
            arquivosIds.add(arquivo.getId());
         }
      }
      Location location = null;
      if(lat != 0 && lng != 0) {
         if(!Double.isNaN(lat) && !Double.isNaN(lng)) {
            location = new Location();
            location.setLat(lat);
            location.setLng(lng);
         }
      }

      if (conversa.getGrupo() != null) {
         mensagem = mensagemService.saveMensagemGrupo(null, conversaId, conversa.getGrupo().getId(), idUserFrom, msg, arquivosIds, location, identifier, cards);
      } else {
         long idUserTo;
         if (conversa.getFrom().getId().equals(idUserFrom)) {
            idUserTo = conversa.getTo().getId().longValue();
         } else {
            idUserTo = conversa.getFrom().getId().longValue();
         }

         mensagem = mensagemService.saveMensagem(null, conversaId, idUserFrom, idUserTo, msg, arquivosIds, location, identifier, cards);
      }
      mensagem.setFrom(usuario);
      
      JSONArray buttons = c.buttons;
      if(buttons != null && buttons.length() > 0) {
         Set<CardButton> cardButtons = new HashSet<CardButton>();
         for(int j = 0; buttons.length() > j; j++) {
            JSONObject btnJson = buttons.getJSONObject(j);
            CardButton cardButton = new CardButton();
            cardButton.setAction(btnJson.optString("type"));
            cardButton.setLabel(btnJson.optString("title"));
            cardButton.setValue(btnJson.optString("payload"));
            cardButton.setMensagem(mensagem);
            cardService.saveOrUpdate(cardButton);
            cardButtons.add(cardButton);
         }
         mensagem.setButtons(cardButtons);
      }

      MensagemVO vo = new MensagemVO();
      vo.setMensagem(mensagem, usuario);

      return vo;
   }

   @Override
   public int chatPort() {
      return 9091;
   }

   @Override
   /**
    * Envia o push, alterar para enviar pelo JOB
    */
   public void sendPush(MensagemVO msgVO, List<UsuarioVO> users) {
      if(users == null || users.size() == 0) {
         return;
      }
      
      log.debug("LivecomChatInterface: Enviando push para [" + users.size() + "] users. ");
      
      for (int i = 0; users.size() > i; i++) {
         UsuarioVO usuarioVO = users.get(i);
         /**
          * Config para nao mandar push para este usuario.
          */
         ConversaNotification noti = conversaNotificationService.findByConversationUser(msgVO.getConversaId(), usuarioVO.getId());
         if(noti != null && !noti.getSendPush()) {
            log.debug("Push Notific silenciada: " + usuarioVO.login);
            users.remove(i);
         }
      }

      // TODO PUSH CHAT deve evniar badges
      if(users.size() > 0) {
         Long msgId = msgVO.getId();
         Long conversaId = msgVO.getConversaId();
         String msg = msgVO.getMsg();
         String fromNome = msgVO.getFromNome();
         
         String sound = ParametrosMap.getInstance().get(Params.PUSH_NOTIFICATION_SOUND, "default");
         
         PushNotificationVO not = new PushNotificationVO();
         not.setTipo(TipoPush.mensagem.toString());
         if(StringUtils.isNotEmpty(sound)) {
        	 not.setSound(sound);
         }
         not.setData(new Date());
         not.setMsg(msg);
         String titulo = ParametrosMap.getInstance().get(Params.MSG_PUSH_MENSAGEM, "%s");
         titulo = String.format(titulo, fromNome);
         
         not.setTitle(titulo);
         not.setTitleNot(titulo);
         not.setSubTitleNot(msg);
         List<FileVO> arquivos = msgVO.getArquivos();
         if(arquivos != null && !arquivos.isEmpty()) {
            String imagem = ParametrosMap.getInstance().get(Params.MSG_PUSH_MENSAGEM_IMAGEM, "Enviou uma imagem");
            not.setSubTitleNot(imagem);
         }
         not.setTituloOriginal(msg);
         not.setMensagemId(msgId);
         not.setConversaId(conversaId);

         // Usuarios
         List<Long> ids = new ArrayList<>();
         for (UsuarioVO usuario : users) {
            ids.add(usuario.getId());
         }
         
         // VO para enviar Push
         PushChatVO vo = new PushChatVO();
         vo.setConversa(conversaId);
         vo.setNotification(not);
         vo.setUsuarios(ids);

         // PUSH
         log.debug("LivecomChatInterface> SendPushChatJob: " + vo);
         PushChatJob.add(vo);

         // Atualiza data de push da msg.
         mensagemService.updateDataPushMsg(msgId);
      }
   }
   
   @Override
   public void sendPush(MensagemVO m, UsuarioVO u) {
      List<UsuarioVO> users = new ArrayList<>();
      users.add(u);
      sendPush(m, users);
      
   }

   @Override
   public List<Long> findUsuariosComQuemTemConversa(long userId) {
      return mensagemService.findUsuariosComQuemTemConversa(userId);
   }

   @Override
   public UpdateStatusMensagemResponseVO updateStatusMensagem(long userId,long msgId, int status) throws DomainException {
      // ver LerMensagemPage
      Usuario u = usuarioService.get(userId);
      if(status == 2) {
         // No caso da mensagem de ler, manda tb o status=1
         // Para salvar o status de lido caso ainda nao tenha salvo
         mensagemService.updateStatusMensagemLida(u, msgId, 1);
      }
      UpdateStatusMensagemResponseVO update = mensagemService.updateStatusMensagemLida(u, msgId, status);
      
      return update;
   }

   @Override
   public void saveMessageAdminAndSendAkka(Usuario admin, Grupo g, MensagemConversa conversa, String message) throws DomainException {
      List<Usuario> usersToId = mensagemService.findUsersFromConversation(conversa.getId(),admin.getId(), true);
      List<Long> ids = Usuario.getIds(usersToId);

      Mensagem mensagem = mensagemService.saveMensagemGrupo(null, conversa.getId(),g.getId(), admin.getId(), message, null, null, null, null);
      mensagem.setMsgAdmin(true);
      mensagemService.saveOrUpdate(mensagem);
      
      MensagemVO vo = new MensagemVO();
      vo.setMensagem(mensagem, admin);
      
      AkkaHelper.sendMsgAdmin(vo,ids);
   }

   @Override
   public UsuarioVO getUsuario(Long userId) {
      Usuario u = usuarioService.get(userId);
      UsuarioVO vo = new UsuarioVO();
      vo.setUsuario(u);
      return vo;
   }

   @Override
   public ConversaVO findConversaUsers(Long userFromId, Long userToId) {
      try {
         Usuario userFrom = usuarioService.get(userFromId);
         Usuario userTo = usuarioService.get(userToId);
         MensagemConversa c = mensagemService.getMensagemConversaByUser(userFrom, userTo);
         ConversaVO vo = new ConversaVO();
         vo.setConversa(userFrom, c);
         return vo;
      } catch (DomainException e) {
         log.error("findConversaUsers : " + e.getMessage(), e);
      }
      return null;
   }

   @Override
   public Date updateLoginChat(Long userId) {
      Date date = usuarioService.updateDataLastChat(userId);
      return date;
   }

   @Override
   public boolean markConversationAsRead(long fromUserId, long conversationId) throws DomainException {
      mensagemService.markConversationAsRead(fromUserId, conversationId);

      MensagemConversa c = mensagemService.getMensagemConversa(conversationId);

      if(c != null && c.isGrupo()) {
         /**
          * Se for grupo só retorna o conversationWasRead (c2A) se todos leram a msg.
          */
         boolean todasLidas = mensagemService.isTodasMensagensLidas(conversationId, fromUserId, true);
         return todasLidas;
      } else {
         // Para conversa individual sempre retorna true.
         return true;
      }  
   }

   @Override
   public List<MensagemVO> findMensagensMobile(Long from, Long lastMsgId) {
      Usuario user = usuarioService.get(from);

      List<Mensagem> msgs = mensagemService.findMensagensMobile(user, lastMsgId);
      for (Mensagem mensagem : msgs) {
         MensagemConversa c = mensagem.getConversa();
         if(c.isGrupo()) {
            mensagem.setLida(mensagem, c, user, statusMensagemUsuarioService);
         }
      }
      List<MensagemVO> list = MensagemVO.create(msgs, user);
      return list;
   }
   
   
   @Override
   public List<MensagemVO> findMensagensCallback(long userId, Long lastReceivedMessageID) {
      Usuario user = usuarioService.get(userId);
      List<Mensagem> msgs = mensagemService.findMensagensCallback(null, user,lastReceivedMessageID);
      List<MensagemVO> list = MensagemVO.create(msgs, user);
      return list;
   }

   @Override
   public UserInfoVO login(String login,String pass, String so, ActorRef tcpWebActor, boolean away) {
      if (StringUtils.isEmpty(login) || StringUtils.isEmpty(pass) || StringUtils.isEmpty(so)) {
         return null;
      }

      Usuario u = usuarioService.findByLogin(login);
      if(u == null) {
         return null;
      }

      if (StringUtils.isEmpty(pass)) {
         return null;
      } else {
         
         String md5 = CripUtil.criptToMD5(pass);
         boolean ok = u.getLogin().equals(login) && (u.getSenha().equals(md5) || u.getSenha().equals(pass));
         if(ok) {
            // Cria UserInfo
            UserInfoVO userInfo = Livecom.getInstance().getUserInfo(login);
            if(userInfo == null) {
               // Cria o user info se nao existir.
               userInfo = UserInfoVO.create(u);
            }
            
            // Cria session
            UserInfoSession session = userInfo.addSession(so, tcpWebActor);

            // Se android loga em background devido a push fica away.
            session.setAway(away);

            // user status
            ChatUtils.sendUserStatus(this,u.getId(),true,away,so);
            
            return userInfo;
         }
      }

      return null;
   }

   @Override
   public MensagemVO getMensagem(long msgId) {
      Mensagem msg = mensagemService.get(msgId);
   
      MensagemVO vo = new MensagemVO();
      vo.setMensagem(msg, null);
      
      return vo;
   }

   @Override
   public void updateStatusMensagemCallback(long conversaId, long msgId, int code) throws DomainException {
      /**
       * 1 - check1cinza, 2 check2cinza, 3 check2azul, 4 conversationWasRead
       */
      switch (code) {
      case 1:
         // 1 check cinza
         mensagemService.updateCallbackMensagem(conversaId, msgId, true, false, false);
         break;
      case 2:
         // 2 check cinza
         mensagemService.updateCallbackMensagem(conversaId, msgId, true, true, false);
         break;
      case 3:
         /** 2 check azul **/
         mensagemService.updateCallbackMensagem(conversaId, msgId, true, true, true);
         break;
      default:
         break;
      }
   }

   @Override
   public ControleVersaoVO validateVersion(Long empresaId, String appVersion, Integer appVersionCode, String userAgentSO) {
      return usuarioService.validateVersion(empresaId, appVersion, appVersionCode, userAgentSO);
   }

   @Override
   public MensagemVO forward(ChatVO c) throws DomainException {
      Long msgId = c.msgId;
      Long grupoId = c.grupoId;
      Long to = c.to;
      Long cId = c.conversaId;
      Long from = c.from;
      Long identifier = c.identifier;
      
      Mensagem mensagem = mensagemService.get(msgId);
      Usuario userFrom = usuarioService.get(from);
      MensagemConversa conversa = null;
      if(userFrom == null) {
         throw new DomainException("From user ["+from+"] não encontrado.");
      }
      
      if(mensagem == null) {
         throw new DomainException("Mensagem ["+msgId+"] não encontrada.");
      }
      
      if(grupoId > 0) {
         Grupo grupo = grupoService.get(grupoId);
         if(grupo == null) {
            throw new DomainException("Grupo ["+grupoId+"] não encontrado.");
         }
         conversa = mensagemService.getMensagemConversaByUserGrupo(userFrom,grupo, true);
      } else if(to > 0) {
         Usuario userTo = usuarioService.get(to);
         if(userTo == null) {
            throw new DomainException("To user ["+from+"] não encontrado.");
         }
         conversa = mensagemService.getMensagemConversaByUser(userFrom, userTo);
      } else if(cId > 0) {
         conversa = mensagemService.getMensagemConversa(cId);
      } else {
         throw new DomainException("Favor informar para quem deseja enviar esta mensagem.");
      }
      
      if(conversa != null) {
         Set<Arquivo> arquivos = mensagem.getArquivos();
         List<Long> arquivosIdsCopy = new ArrayList<>();
         if(arquivos != null && arquivos.size() > 0) {
            for (Arquivo arquivo : arquivos) {
               
               Arquivo arquivoCopy = new Arquivo();
               arquivoCopy.setEmpresa(arquivo.getEmpresa());
               arquivoCopy.setDataCreate(new Date());
               arquivoCopy.setDataUpdate(new Date());
               arquivoCopy.setDescricao(arquivo.getDescricao());
               arquivoCopy.setDestaque(false);
               arquivoCopy.setDimensao(arquivo.getDimensao());
               arquivoCopy.setHeight(arquivo.getHeight());
               arquivoCopy.setLength(arquivo.getLength());
               arquivoCopy.setMetadata(arquivo.getMetadata());
               arquivoCopy.setMimeType(arquivo.getMimeType());
               arquivoCopy.setNome(arquivo.getNome());
               arquivoCopy.setTipo(arquivo.getTipo());
               arquivoCopy.setUrl(arquivo.getUrl());
               arquivoCopy.setUrlThumb(arquivo.getUrlThumb());
               arquivoCopy.setUsuario(userFrom);
               arquivoCopy.setWidth(arquivo.getWidth());
               arquivoService.saveOrUpdate(arquivoCopy);
               Set<ArquivoThumb> thumbs = arquivo.getThumbs();
               
               for (ArquivoThumb thumb : thumbs) {
                  ArquivoThumb thumbCopy = new ArquivoThumb();
                  thumbCopy.setArquivo(arquivoCopy);
                  thumbCopy.setCod(thumb.getCod());
                  thumbCopy.setHeight(thumb.getHeight());
                  thumbCopy.setUrl(thumb.getUrl());
                  thumbCopy.setWidth(thumb.getWidth());
                  arquivoService.saveOrUpdate(thumbCopy);
               }
               arquivosIdsCopy.add(arquivoCopy.getId());
            }
         }
         Location location = mensagem.getLocation();
         
         Location locCopy = null;
         if(location != null) {
            locCopy = new Location();
            locCopy.setLat(location.getLat());
            locCopy.setLng(location.getLng());
         }
         
         Set<Card> cards = mensagem.getCards();
         Set<Card> cardsCopy = null;
         if(cards != null && cards.size() > 0) {
            cardsCopy = new HashSet<>();
            for (Card card : cards) {
               Card cardCopy = new Card();
               cardCopy.setImageUrl(card.getImageUrl());
               cardCopy.setSubTitle(card.getSubTitle());
               cardCopy.setTitle(card.getTitle());
               cardService.saveOrUpdate(cardCopy);
               Set<CardButton> buttons = card.getButtons();
               if (buttons != null && buttons.size() > 0) {
                  for (CardButton cardButton : buttons) {
                     CardButton cardButtonCopy = new CardButton();
                     cardButtonCopy.setAction(cardButton.getAction());
                     cardButtonCopy.setLabel(cardButton.getLabel());
                     cardButtonCopy.setValue(cardButton.getValue());
                     cardButtonCopy.setCard(cardCopy);
                     cardService.saveOrUpdate(cardButtonCopy);
                     cardCopy.addButton(cardButtonCopy);
                  }
               }
               cardsCopy.add(cardCopy);
            }
         }
         
         String msg = mensagem.getMsg();
         
         Long conversaId = conversa.getId();
         if (conversa.getGrupo() != null) {
            mensagem = mensagemService.saveMensagemGrupo(null, conversaId, conversa.getGrupo().getId(), from, msg, arquivosIdsCopy, locCopy, identifier, cardsCopy);
         } else {
            long idUserTo;
            if (conversa.getFrom().getId().equals(from)) {
               idUserTo = conversa.getTo().getId().longValue();
            } else {
               idUserTo = conversa.getFrom().getId().longValue();
            }
            
            mensagem = mensagemService.saveMensagem(null, conversaId, from, idUserTo, msg, arquivosIdsCopy, locCopy, identifier, cardsCopy);
         }
         MensagemVO vo = new MensagemVO();
         vo.setMensagem(mensagem, userFrom);
         return vo;
      }
      
      return null;
   }
   
   @Override
   public void markConversasReceivedMessages(List<Long> cIds, Long fromId) throws DomainException, JSONException {
      Usuario from = usuarioService.get(fromId);
      if(from == null) {
         throw new DomainException("User from não encontrado");
      }
      for (Long cId : cIds) {
         MensagemConversa conversa = mensagemService.getMensagemConversa(cId);
         if(conversa == null) {
            continue;
         }
         boolean isTodasRecebida = mensagemService.markMessagesWasReceived(conversa, from);
         List<Long> usuarios = findIdsUsersFromConversation(cId, fromId, false);
         JsonRawMessage raw = ChatUtils.getJsonC2C(cId, fromId);
         Grupo grupo = conversa.getGrupo();
         if(grupo != null) {
            raw = ChatUtils.putInt("gId", grupo.getId().intValue(), raw);
            if(isTodasRecebida) {
               raw = ChatUtils.putInt("g2C",1,raw);
            }
         }
         AkkaHelper.sendToOnlineFriends(raw, usuarios);
      }
   }

   @Override
   public boolean isUsuarioDentroDoGrupo(long from, long grupoId) {
      Usuario user = usuarioService.get(from);
      Grupo grupo = grupoService.get(grupoId);
      return usuarioService.isUsuarioDentroDoGrupo(user, grupo);
   }

   @Override
   public List<MensagemVO> findMensagensById(List<Long> msgIds) {
      List<Mensagem> msgs = mensagemService.findMensagensById(msgIds);
      List<MensagemVO> list = MensagemVO.create(msgs, null);
      return list;
   }
   
   @Override
   public List<Long> findIdsConversasLidasById(List<Long> ids, Long userId) {
      List<Long> cids = mensagemService.findIdsConversasLidasById(ids, userId);
      return cids;
   }
}