package br.livetouch.livecom.wordpress;

import java.util.Date;
import java.util.List;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.extras.util.DateUtils;

public class Post {
	private final static String pattern = "yyyy-MM-dd H:mm:ss";
	private Long id;
	private String type;
	private String slug;
	private String url;
	private String title;
	private String title_plain;
	private String content;
	private String excerpt;
	private String date;
	private String modified;
	private Integer comment_count;
	private String comment_status;
	private List<Categories> categories;
	private List<Tags> tags;
	private List<Comments> comments;
	private List<Attachments> attachments;
	private Author author;
	private Long idPostLivecom;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle_plain() {
		return title_plain;
	}

	public void setTitle_plain(String title_plain) {
		this.title_plain = title_plain;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getExcerpt() {
		return excerpt;
	}

	public void setExcerpt(String excerpt) {
		this.excerpt = excerpt;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Integer getComment_count() {
		return comment_count;
	}

	public void setComment_count(Integer comment_count) {
		this.comment_count = comment_count;
	}

	public String getComment_status() {
		return comment_status;
	}

	public void setComment_status(String comment_status) {
		this.comment_status = comment_status;
	}

	public List<Categories> getCategories() {
		return categories;
	}

	public void setCategories(List<Categories> categories) {
		this.categories = categories;
	}

	public List<Tags> getTags() {
		return tags;
	}

	public void setTags(List<Tags> tags) {
		this.tags = tags;
	}

	public List<Comments> getComments() {
		return comments;
	}

	public void setComments(List<Comments> comments) {
		this.comments = comments;
	}

	public List<Attachments> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachments> attachments) {
		this.attachments = attachments;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public static br.livetouch.livecom.domain.Post copyToPost(Post post) {
		final String pattern = "yyyy-MM-dd H:mm:ss";
		br.livetouch.livecom.domain.Post p = new br.livetouch.livecom.domain.Post();
		p.setId(post.idPostLivecom);
		p.setMensagem(post.content);
		p.setTitulo(post.title);
		Date datePublicacao = DateUtils.toDate(post.date, pattern);
		p.setDataPublicacao(datePublicacao);
		Date dataEdicao = DateUtils.toDate(post.modified, pattern);
		p.setDataUpdated(dataEdicao);
		
		
		/*
		 * Setá as informações do author do post 
		 */
		Author a = post.author;
		Usuario usuario = new Usuario();
		usuario.setId(a.getId());
		usuario.setNome(a.getName());
		p.setUsuario(usuario);
		
		List<Categories> cList = post.categories;
		if(cList != null && cList.size() > 0) {
			Categories c = cList.get(0);
			CategoriaPost catPost = new CategoriaPost();
			catPost.setId(c.getId());
			catPost.setNome(c.getSlug());
			catPost.setCor("70c7a0");
			p.setCategoria(catPost);
		}
		
		return p;
	}
	

	public void setIdPostLivecom(Long idPostLivecom) {
		this.idPostLivecom = idPostLivecom;
	}
	
	public Long getIdPostLivecom() {
		return idPostLivecom;
	}

	public void copyToPostLivecom(br.livetouch.livecom.domain.Post p) {
		p.setMensagem(this.content);
		p.setTitulo(this.title);
		Date datePublicacao = DateUtils.toDate(this.date, pattern);
		p.setDataPublicacao(datePublicacao);
		Date dataEdicao = DateUtils.toDate(this.modified, pattern);
		p.setDataUpdated(dataEdicao);
		
		
		/*
		 * Setá as informações do author do post 
		 */
		Author a = this.author;
		Usuario usuario = new Usuario();
		usuario.setId(a.getId());
		usuario.setNome(a.getName());
		p.setUsuario(usuario);
		
		List<Categories> cList = this.categories;
		if(cList != null && cList.size() > 0) {
			Categories c = cList.get(0);
			CategoriaPost catPost = new CategoriaPost();
			catPost.setId(c.getId());
			catPost.setNome(c.getSlug());
			catPost.setCor("70c7a0");
			p.setCategoria(catPost);
		}
	}

	public static br.livetouch.livecom.domain.Post copyToPostLivecom(Post post) {
		br.livetouch.livecom.domain.Post p = new br.livetouch.livecom.domain.Post();
		p.setIdWordpress(post.getId());
		p.setMensagem(post.content);
		p.setTitulo(post.title);
		Date datePublicacao = DateUtils.toDate(post.date, pattern);
		p.setDataPublicacao(datePublicacao);
		Date dataEdicao = DateUtils.toDate(post.modified, pattern);
		p.setDataUpdated(dataEdicao);
		
		
		/*
		 * Setá as informações do author do post 
		 */
		Author a = post.author;
		Usuario usuario = new Usuario();
		usuario.setId(a.getId());
		usuario.setNome(a.getName());
		p.setUsuario(usuario);
		
		List<Categories> cList = post.categories;
		if(cList != null && cList.size() > 0) {
			Categories c = cList.get(0);
			CategoriaPost catPost = new CategoriaPost();
			catPost.setId(c.getId());
			catPost.setNome(c.getSlug());
			catPost.setCor("70c7a0");
			p.setCategoria(catPost);
		}
		return p;
	}
}
