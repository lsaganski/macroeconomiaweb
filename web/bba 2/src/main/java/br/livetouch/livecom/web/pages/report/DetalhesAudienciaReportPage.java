package br.livetouch.livecom.web.pages.report;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.web.pages.pages.MuralPage;

@Controller
@Scope("prototype")
public class DetalhesAudienciaReportPage extends RelatorioPage {

	public Long id;
	public String filtroJson;
	public RelatorioFiltro filtro;
	public String action;
	
	public Post post;
	
	@Override
	public void onInit() {
		super.onInit();
		bootstrap_on = true;
		escondeChat = true;
		
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new RelatorioFiltro();
		}

		if (filtro != null) {
			filtro.setPostId(id);
			filtro.setUsuario(null);
			filtro.setPost(null);
			filtroJson = new Gson().toJson(filtro);
		}
		
	}
	
	@Override
	public boolean onSecurityCheck() {		
		post = postService.get(id);
		
		Usuario u = getUsuario();
		boolean logado = u != null;
		if (logado) {
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS) && post != null) {
				Long autor = post.getUsuario() != null ? post.getUsuario().getId() : null;
				if(!autor.equals(u.getId())) {
					setRedirect(MuralPage.class);
					return onSecurityCheckNotOk();
				}
				
			}
		}
		
		return super.onSecurityCheck();
	}

	@Override
	public String getContentType() {
		return super.getContentType();
	}

	@Override
	public String getTemplate() {
		return super.getTemplate();
	}

	@Override
	public void onRender() {
		super.onRender();

	}

	@Override
	public void onPost() {
		super.onPost();
		if ("1".equals(action)) {
			filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			filtro.setPage(0);
			filtro.setMax(0);
		}
	}
}
