package br.livetouch.livecom.security;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.crypt.AESCrypt;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.RequestMap;
import br.livetouch.livecom.domain.Token;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.exception.SenhaExpiradaException;
import br.livetouch.livecom.domain.service.TokenService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.UserInfoSession;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.livecom.utils.LoginCount;
import br.livetouch.token.TokenHelper;
import br.livetouch.token.TokenSecret;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.Context;

/** 
 * http://jira.livetouchdev.com.br/browse/LIVECETIP-45
 * 
 * @author rlecheta
 *
 */
public class LivecomSecurity {
	
	private static final Logger logSecurity = Log.getLogger("security");
	
	public static RequestMap requestMap = RequestMap.getInstance();
	public static final String NONCE_TOKEN_PARAM = "nonce_token";

	private static final boolean LOG_ON = false;

	private static ParametrosMap getParams(Empresa e) {
		ParametrosMap params = ParametrosMap.getInstance(e.getId());
		return params;
	}
	
	/**
	 * Validate genérico para Web e Mobile.
	 * 
	 * Valida se empresa existe e se host está OK.
	 * 
	 * @param c
	 * @param e
	 * @return
	 */
	public static boolean validate(Context c, Empresa e) {
		if(e == null) {
			// Empresa não existe
			log("Empresa não encontrada: " + getHost(c));
			return false;
		}

		logDebugRequest(c);
		
		// Valida se request é attack replay
		boolean b = validateAttackReplay(c, e);
		if(!b) {
			return false;
		}

		// Valida se pode acessar o site deste host
		b = validateHost(c, e);
		if(!b) {
			return false;
		}
		
		boolean mobile = UserAgentUtil.isMobile(c);
		if(mobile) {
			return validateMobile(c, e);
		}
		
		return validateWeb(c, e);
	}

	private static boolean validateMobile(Context c, Empresa e) {
		return 
			validateWsPath(c, e) && 
			validateUserAgent(c, e);
	}
	
	private static boolean validateWeb(Context c, Empresa e) {
		return 
			validateWsPath(c, e) && // teste TODO
			validateIP(c, e) && 
			validateUserAgent(c, e);
	}

	/**
	 * Verifica se pode fazer get nos ws.
	 * TODO colocar no ROOT na parte de serviço
	 * 
	 * @param e
	 * @return
	 */
	public static boolean isWsGetAllowed(Empresa e) {
		boolean b = getParams(e).getBoolean(Params.WS_SECURITY_GET_ALLOWED,true);
		return b;
	}

	public static boolean isWsCryptOn(Empresa e, int wsVersion) {
		boolean cryptOn = getParams(e).getBoolean(Params.SECURITY_WS_CRYPT_ON, false);
		return cryptOn && wsVersion >= 4;
	}
	
	/**
	 * Valida se pode acessar o servidor por este IP e endereço.
	 * 
	 * IPs 200.196.153.28 - 200.196.153.29 - 200.196.153.27 - 200.196.153.30 - 200.196.153.33
	 * 
	 * IP server
	 * 
	 * http://revistanegocios.livetouchdev.com.br
	 * http://revistanegociosadmin.livetouchdev.com.br (200.196.153.28 - 200.196.153.29)
	 * 
	 * @return
	 */
	private static boolean validateHost(Context context, Empresa e) {
		boolean on = getParams(e).getBoolean(Params.SECURITY_HOST_ON,false);
		if(!on) {
			return true;
		}
		
		String serverName = getHost(context);
		String validHosts = getParams(e).get(Params.SECURITY_ALLOWED_ADMIN_HOST);
		if(StringUtils.isEmpty(validHosts)) {
			log("<< validateHost WARNING: Security Host está ligado, mas sem hosts registrados.");
			return true;
		}
		
		boolean b = contains(validHosts, serverName);
		
		if(!b) {
			log("<< validateHost error: " + serverName);
		}
		
		return b;
	}

	public static boolean isShareLink(Context context) {
		String param = context.getRequestParameter("share");
		return param != null && StringUtils.equals(param, "1");
	}

	/**
	 * Valida se é uma requisição de attack replay
	 * 
	 * @return
	 */
	private static boolean validateAttackReplay(Context c, Empresa e) {
		
		boolean on = ParametrosMap.getInstance(e).getBoolean(Params.SECURITY_ATTACK_REPLAY_ON, false);
		if(on) {
			
			boolean isPost = c.isPost();
			if(isPost) {
				String nonce = (String) c.getRequestParameter(LivecomSecurity.NONCE_TOKEN_PARAM);
				if(StringUtils.isEmpty(nonce)) {
					return false;
				}
				
				String requestToken = requestMap.get(nonce);
				
				if(StringUtils.isNotEmpty(requestToken)) {
					return false;
				} else {
					requestMap.put(nonce, nonce);
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Valida se o servidor pode ser acessado por este IP de origem.
	 * @param c
	 * @param e 
	 * @param e
	 * @return
	 */
	private static boolean validateIP(Context context, Empresa e) {
		boolean mobile = UserAgentUtil.isMobile(context);
		if(mobile) {
			// Não valida IP se for mobile
			return true;
		}
		
		boolean on = getParams(e).getBoolean(Params.SECURITY_IP_ON,false);
		if(!on) {
			return true;
		}

		String ip = getIp(context);
		String validIps = getParams(e).get(Params.SECURITY_ALLOWED_IP);

		boolean b = contains(validIps, ip);
		if(!b) {
			log("<< validateIP error: " + ip);
		}
		return b;
	}

	/**
	 * Verifica se a string está contida dentre as strings válidas (separadas por ,)
	 * 
	 * Utilizado para validar host e IP.
	 * 
	 * @param validStrings
	 * @param string
	 * @return
	 */
	private static boolean contains(String validStrings,String string) {
		if(StringUtils.isEmpty(validStrings) || StringUtils.isEmpty(string)) {
			return false;
		}
		
		String[] split = StringUtils.split(validStrings,",");
		for (String s : split) {
			if(StringUtils.equals(string, s) || StringUtils.contains(string, s)) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Criptografa com AES um JSON de resposta com a chave informada.
	 * 
	 * @param json
	 * @param cryptKey
	 * @return 
	 * @throws Exception 
	 */
	public static String encryptJson(String json, String cryptKey) throws Exception {
		json = AESCrypt.encrypt(cryptKey, json);
		return json;
	}

	/**
	 * Se a segurança por Token no WS está habilitada.
	 * 
	 * @param e
	 * @param wsVersion
	 * @return
	 */
	public static boolean isWsTokenOn(Empresa e, int wsVersion) {
		boolean on = getParams(e).getBoolean(Params.WS_SECURITY_TOKEN_ON, false);
		if (on) {
			boolean validate = wsVersion >= 3;
			return validate;
		}
		return false;
	}

	/**
	 * Se a segurança por OTP no WS está habilitada.
	 * 
	 * @param e
	 * @return
	 */
	public static boolean isOTPOn(Empresa e) {
		return getParams(e).getBoolean(Params.WS_SECURITY_OTP_ON, false);
	}
	
	/**
	 * Valida se o UserInfo passou o token correto para o SO (Android,iOS,web).
	 * 
	 * O Token é retornado no momento do Login e deve ser passado a cada request.
	 */
	public static boolean validateWsToken(UserInfoVO userInfo, String userAgentSO, String wsToken, TokenService tokenService) throws DomainMessageException {
		try {

			if (userInfo != null) {
				UserInfoSession session = userInfo.getUserSession(userAgentSO);

				if (StringUtils.isEmpty(wsToken)) {
					return false;
				} else {
					String token = String.valueOf(session.getWstoken());
					// Valida o timestamp
					if (!StringUtils.equals(token, wsToken)) {
						return false;
					}
					return true;
				}
			} else {
				
				Token t = tokenService.findLastToken(null, wsToken, userAgentSO); 
				if(t != null) {
					userInfo = UserInfoVO.create(t.getUsuario());
		            UserInfoSession session = userInfo.addSession(userAgentSO);
		            Livecom.getInstance().add(userInfo);

		            // OK
		            LoginCount.getInstance().clear(userInfo.login);
					session.setWstoken(t.getToken());
					return true;
				}
				return false;
			}

		} catch (Exception e) {
			return false;
		}
	}
	
	public static void updateToken(String deviceSO, Usuario u, UserInfoSession session, TokenService tokenService) throws DomainException {
		Token lastToken = tokenService.findLastToken(u.getId(), "", deviceSO);
		if(lastToken != null) {
			lastToken.setToken(session.wstoken);
			lastToken.setData(new Date());
		} else {
			lastToken = new Token();
			lastToken.setToken(session.wstoken);
			lastToken.setUsuario(u);
			lastToken.setData(new Date());
			lastToken.setSo(StringUtils.lowerCase(deviceSO));
		}
		tokenService.saveOrUpdate(lastToken);
	}

	/**
	 * Valida OTP.
	 * 
	 */
	public static boolean validateOTP(String otp, String wstoken) throws DomainMessageException {
		
		try {
		
			if(StringUtils.isEmpty(otp)) {
				return false;
			}
			
			String secret = TokenSecret.create(wstoken);
			
			boolean ok = TokenHelper.validateTimeToken(secret, otp, 30, true);
			if (!ok) {
				return false;
			}
			
			return true;
			
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * 
	 * #######
	 * Desktop
	 * #######>
	 * userAgent request: Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko]]
	 * userAgent request: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36]]
	 * userAgent request: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0]]
	 * userAgent request: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/600.3.18 (KHTML, like Gecko) Version/8.0.3 Safari/600.3.18]]
	 * userAgent request: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.125 Safari/537.36]]
	 * userAgent request: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:38.0) Gecko/20100101 Firefox/38.0]]
	 * 
	 * #######
	 * Mobile
	 * #######
	 * userAgent request: Mozilla/5.0 (iPhone; CPU iPhone OS 8_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12F70 Safari/600.1.4]]
	 * userAgent request: Mozilla/5.0 (iPhone; CPU iPhone OS 8_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12F70 Safari/600.1.4]]
	 * Mozilla/5.0 (iPhone; CPU iPhone OS 8_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12D508 Safari/600.1.4]]
	 * userAgent request: Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LMY47Z) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.93 Mobile Safari/537.36]]
	 * userAgent request: Dalvik/2.1.0 (Linux; U; Android 5.1.1; Nexus 6 Build/LMY47Z)]]
	 * 
	 * @return
	 */
	private static boolean validateUserAgent(Context context, Empresa e) {
		boolean on = getParams(e).getBoolean(Params.SECURITY_USERAGENT_ON,false);
		if(!on) {
			return true;
		}
		
		Long empresaId = e.getId();

		String userAgent = UserAgentUtil.getUserAgent(context);
		
		boolean mobile = UserAgentUtil.isMobile(context);
		
		String allowed = ParametrosMap.getInstance(empresaId).get(mobile ? 
				Params.SECURITY_USERAGENT_MOBILE_ALLOWED : Params.SECURITY_USERAGENT_WEB_ALLOWED);
		
		String blocked = ParametrosMap.getInstance(empresaId).get(mobile ? Params.SECURITY_USERAGENT_MOBILE_BLOCKED: 
			Params.SECURITY_USERAGENT_WEB_BLOCKED);

		/**
		 * Allow
		 * 
		 * revistaitau,revista-negocios-android,revista-negocios-ios,android,Dalvik,iPhone,iPad
		 */
		if(StringUtils.isNotEmpty(allowed)) {
			boolean b = contains(allowed, userAgent);
			if(!b) {
				log("User-Agent " + userAgent + " NOT ALLOWED!");
				return false;
			}
		}
		
		/**
		 * Block
		 * 
		 * Firefox,Mozilla,Chrome,Chromium,Internet,Explorer,Windows,Macintosh,Mac,Ubuntu
		 */
		if(StringUtils.isNotEmpty(blocked)) {
			boolean b = contains(blocked, userAgent);
			if(b) {
				log("User-Agent " + userAgent + " BLOCKED!");
				return false;
			}
		}

		return true;
	}
	
	/**
	 * Valida se o path do web service pode ser chamado 
	 * String allowedRequests = getParams(e).get(Params.SECURITY_REQUESTS_ALLOWED);
	 * 
	 * @return
	 */
	private static boolean validateWsPath(Context context, Empresa e) {
		boolean on = getParams(e).getBoolean(Params.SECURITY_WS_PATH_ON,false);
		if(!on) {
			return true;
		}
		
		String validWsPaths = getParams(e).get(Params.SECURITY_WS_PATH_ALLOWED);
		String wsPath = StringUtils.lowerCase(context.getRequest().getServletPath());
		
		if(StringUtils.isEmpty(validWsPaths)) {
			return true;
		}

		boolean b = contains(validWsPaths, wsPath);
		if(b) {
			return true;
		}
		
		log("Request Path " + wsPath + " Negada!");
		return false;
	}
	
	/**
	 * Bloqueio por tentativas
	 */
	public static void checkBlockLogin(String login, Long empresaId) throws DomainException {
        ParametrosMap parametrosMap = ParametrosMap.getInstance(empresaId);
        boolean configLoginOn = parametrosMap.getBoolean(Params.CONFIG_LOGIN_ON, true);
        if(configLoginOn){
	        int qtdeErros = LoginCount.getInstance().get(login);
	        int limiteCountLogin = Integer.valueOf (parametrosMap.get(Params.CONFIG_LOGIN_QTDEMAXERROSLOGIN, "-1"));
	        long timeBloqueioErroLogin = Integer.valueOf(parametrosMap.get(Params.CONFIG_LOGIN_TEMPOBLOQUEIOERROLOGIN, "0"));
	
	        if (qtdeErros >= limiteCountLogin && limiteCountLogin != -1) {
	
	            long tempo = LoginCount.getInstance().isBloqueado(login);
	
	            if (tempo < timeBloqueioErroLogin) {
	                long diferenca = timeBloqueioErroLogin - tempo;
	                throw new DomainMessageException("Bloqueio de login por excesso de tentativas, tente novamente daqui a " + diferenca + " minuto(s)");
	            } else {
	                LoginCount.getInstance().clear(login);
	            }
	        }
        }
    }
	
	/**
	 * Expirar senha inativa
	 */
	public static void checkExpiredPasswordByInactivity(Usuario u, Long empresaId) throws SenhaExpiradaException {
        ParametrosMap parametrosMap = ParametrosMap.getInstance(empresaId);
        boolean expirarSenhaOn = parametrosMap.getBoolean(Params.EXPIRAR_SENHA_INATIVIDADE_ON, false);
        if(expirarSenhaOn && !u.isRoot()){
			Date dtUltLogin = u.getDataLastLogin();
			Date now = new Date();
			long diffDays = DateUtils.getDiffInDays(dtUltLogin, now);
			long maxDays = parametrosMap.getLong(Params.EXPIRAR_SENHA_INATIVIDADE_QTDEDIAS, 90);
			if(diffDays >= maxDays){
				//TODO ver mensagem de resposta
				throw new SenhaExpiradaException("Senha expirada por inatividade");
			}
        }
    }
	
	public static boolean isLocalhost(Context c) {
		String host = getHost(c);
		return host.contains("localhost") && host.contains("livecom");
	}

	private static void logDebugRequest(Context context) {
		if(LOG_ON) {
			log("getServerName: " + context.getRequest().getServerName());
			log("getRequestURI: " + context.getRequest().getRequestURI());
			log("getRequestURL: " + context.getRequest().getRequestURL());
			log("getServletPath: " + context.getRequest().getServletPath());
			log("getServerPort: " + context.getRequest().getServerPort());
			log("getRemoteAddr: " + context.getRequest().getRemoteAddr());
			log("getRemoteHost: " + context.getRequest().getRemoteHost());
			log("getRemotePort: " + context.getRequest().getRemotePort());
			log("user-agent: " + context.getRequest().getHeader("user-agent"));
		}
	}
	private static void log(String string) {
		System.err.println(string);
		logSecurity.debug(string);
	}

	public static String getIp(Context context) {
		return context.getRequest().getRemoteAddr();
	}

	public static String getHost(Context context) {
//		String host = ServletUtils.getHost(context.getRequest());
		return context.getRequest().getServerName();
	}
	
	/**
	 * Troca periódica de senha
	 */
	public static void checkExpiredPasswordByPeriodicity(Usuario u, Long empresaId, UsuarioService usuarioService) throws SenhaExpiradaException {
        ParametrosMap parametrosMap = ParametrosMap.getInstance(empresaId);
        boolean periodicidadeSenhaOn = parametrosMap.getBoolean(Params.EXPIRAR_SENHA_PERIODICIDADE_ON, false);
        if(periodicidadeSenhaOn && !u.isRoot()){
			Date dtUpdatedSenha = u.getDataUpdatedSenha();
			if(dtUpdatedSenha != null){
				Date now = new Date();
				long diffDays = DateUtils.getDiffInDays(dtUpdatedSenha, now);
				long maxDays = parametrosMap.getLong(Params.EXPIRAR_SENHA_PERIODICIDADE_QTDEDIAS, 90);
				if(diffDays >= maxDays){
					//TODO ver mensagem de resposta
					throw new SenhaExpiradaException("Senha expirada por falta de troca");
				}
				
			}else{
				u.setDataUpdatedSenha(new Date());
				usuarioService.saveOrUpdate(u);
			}
        }
    }
	public static boolean isExpiredPasswordByInactivity(Usuario u){
        Long empresaId = u.getEmpresa().getId();
		ParametrosMap parametrosMap = ParametrosMap.getInstance(empresaId);
        boolean expirarSenhaOn = parametrosMap.getBoolean(Params.EXPIRAR_SENHA_INATIVIDADE_ON, false);
        if(expirarSenhaOn && !u.isRoot()){
			Date dtUltLogin = u.getDataLastLogin();
			Date now = new Date();
			long diffDays = DateUtils.getDiffInDays(dtUltLogin, now);
			long maxDays = parametrosMap.getLong(Params.EXPIRAR_SENHA_INATIVIDADE_QTDEDIAS, 90);
			if(diffDays >= maxDays){
				return true;
			}
        }
        return false;
    }
}
