package br.livetouch.livecom.web.pages.report;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.vo.PushReportFiltro;
import br.livetouch.livecom.domain.vo.RelatorioAuditoriaVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

@Controller
@Scope("prototype")
public class DetalhesAuditoriaPostReportPage extends RelatorioPage {
	
	public PushReportFiltro filtro;
	public int page;
	public Long id;
	
	public Form form = new Form();
	
	public List<RelatorioAuditoriaVO> auditorias;
	
	@Override
	public void onInit() {
		super.onInit();

		if(clear) {
			getContext().removeSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		}
		
		if (id != null) {
			List<LogAuditoria> audits = logAuditoriaService.getAuditoriasByEntity(id, AuditoriaEntidade.POST);
			auditorias = RelatorioAuditoriaVO.fromList(audits);
		}
		
		form();

	}
	
	public void form() {
		
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "btn btn-outline blue min-width");
		form.add(tExportar);

	}
	

	@Override
	public void onGet() {
		super.onGet();
	}
	
	@Override
	public String getContentType() {
		return super.getContentType();
	}
	
	@Override
	public String getTemplate() {
		return super.getTemplate();
	}
	
	public boolean exportar() throws DomainException {
		if (id != null) {
			List<LogAuditoria> audits = logAuditoriaService.getAuditoriasByEntity(id, AuditoriaEntidade.POST);
			auditorias = RelatorioAuditoriaVO.fromList(audits);
			String csv = reportService.csvDetalhesAuditoriaPostReport(audits);
			download(csv, "detalhes-envioPost-report.csv");
		}

		return true;
	}
	
}