package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.UsuarioMarcado;
import br.livetouch.livecom.domain.repository.UsuarioMarcadoRepository;
import br.livetouch.livecom.domain.service.UsuarioMarcadoService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class UsuarioMarcadoServiceImpl implements UsuarioMarcadoService {
	@Autowired
	private UsuarioMarcadoRepository rep;

	@Override
	public UsuarioMarcado get(Long id) {
		return rep.get(id);
	}

	@Override
	public List<UsuarioMarcado> findAll() {
		return rep.findAll();
	}

	@Override
	public void saveOrUpdate(UsuarioMarcado f) throws DomainException {
		rep.saveOrUpdate(f);
	}

	@Override
	public void delete(UsuarioMarcado f) throws DomainException {
		rep.delete(f);
	}

	@Override
	public UsuarioMarcado find(Comentario comentario, Usuario usuario) {
		return rep.find(comentario, usuario);
	}

	@Override
	public void delete(List<Long> idsMarcados, Comentario c) {
		rep.delete(idsMarcados, c);
	}
}
