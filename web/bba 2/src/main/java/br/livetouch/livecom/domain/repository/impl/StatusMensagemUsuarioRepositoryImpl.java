package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.StatusMensagemUsuario;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.StatusMensagemUsuarioRepository;
import br.livetouch.livecom.domain.repository.UsuarioRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class StatusMensagemUsuarioRepositoryImpl extends StringHibernateRepository<StatusMensagemUsuario> implements StatusMensagemUsuarioRepository {
	
	public StatusMensagemUsuarioRepositoryImpl(){
		super(StatusMensagemUsuario.class);
	}

	@Autowired
	UsuarioRepository usuarioRep;

	@SuppressWarnings("unchecked")
	@Override
	public List<StatusMensagemUsuario> findAllByUserConversa(Usuario u,
			MensagemConversa c) {

		Query q = createQuery("select distinct smu from StatusMensagemUsuario smu where smu.usuario.id = ? and smu.conversa.id = ?");
		q.setLong(0, u.getId());
		q.setLong(1, c.getId());
		q.setCacheable(true);
		List<StatusMensagemUsuario> mensagensStatus = (List<StatusMensagemUsuario>) q.list();
		return mensagensStatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StatusMensagemUsuario> findAllByConversa(MensagemConversa c) {
		Query q = createQuery("select distinct smu from StatusMensagemUsuario smu where smu.conversa.id = ?");
		q.setLong(0, c.getId());
		q.setCacheable(true);
		List<StatusMensagemUsuario> mensagensStatus = (List<StatusMensagemUsuario>) q.list();
		return mensagensStatus;
	}

	@Override
	public int updateStatusMensagem(Long userId, Long conversaId, int status) {
		StringBuffer sb = new StringBuffer("update StatusMensagemUsuario m set ");
		if(status == 1) {
			sb.append("m.entregue = 1, dataEntregue = :data ");
		} else if(status == 2) {
			sb.append("m.lida = 1, dataLida = :data ");
		} else {
			sb = null;
			return 0;
		}
		
		sb.append("WHERE m.usuario.id = :userId AND m.conversa.id = :conversaId ");
		if(status == 1) {
			sb.append(" AND (m.entregue != 1 AND m.lida != 1)");
		} else if(status == 2) {
			sb.append(" AND m.lida != 1");
		}
		Query q = createQuery(sb.toString());
		q.setParameter("userId", userId);
		q.setParameter("conversaId", conversaId);
		q.setParameter("data", new Date());
		int qtdModified = q.executeUpdate();
		return qtdModified;
	}
	
	/**
	 * status: 1 - recebida, 2 lida
	 */
	@Override
	public int updateStatusMensagemGrupo(Long userId, Long mensagemId, Integer status) {
		StringBuffer sb = new StringBuffer("update StatusMensagemUsuario m set ");
		if (status == 1) {
			sb.append("m.entregue = 1, m.dataEntregue = :data ");
		} else if (status == 2) {
			sb.append("m.lida = 1, m.dataLida = :data ");
		} else {
			return 0;
		}
		
		// Update Mensagem
		sb.append("where m.mensagem.id = :mensagemId and m.usuario.id=:userId ");
		
		if (status == 1) {
			sb.append("and m.entregue != 1 ");
		} else if (status == 2) {
			sb.append("and m.lida != 1");
		}
		
		Query q = createQuery(sb.toString());
		q.setParameter("mensagemId", mensagemId);
		q.setParameter("userId", userId);
		q.setParameter("data", new Date());
		int qtdModified = q.executeUpdate();
		return qtdModified;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StatusMensagemUsuario> findAllByMensagem(Mensagem m) {
		
		Query q = createQuery("from StatusMensagemUsuario smu where smu.mensagem.id = :mensagem");
		q.setLong("mensagem", m.getId());
		q.setCacheable(true);
		List<StatusMensagemUsuario> mensagensStatus = (List<StatusMensagemUsuario>) q.list();
		return mensagensStatus;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Mensagem> findAllMessagesNaoRecebidasUserConversa(Usuario u,
			MensagemConversa c) {
		Query q = createQuery("select distinct smu.mensagem from StatusMensagemUsuario smu where smu.usuario.id = ? and smu.conversa.id = ? and smu.excluida = 0 and smu.entregue = 0 order by smu.mensagem.id");
		q.setLong(0, u.getId());
		q.setLong(1, c.getId());
		q.setCacheable(true);
		List<Mensagem> mensagens = (List<Mensagem>) q.list();
		return mensagens;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Mensagem> findAllMessagesNaoExcluidasUserConversa(Usuario u,
			MensagemConversa c) {
		//Expression #1 of ORDER BY clause is not in SELECT list, references column 'livecom.statusmens0_.mensagem_id' which is not in SELECT list; this is incompatible with DISTINCT
//		Query q = createQuery("select distinct smu.mensagem from StatusMensagemUsuario smu "
//				+ "where smu.usuario.id = ? and smu.conversa.id = ? and smu.excluida = 0 "
//				+ "order by smu.mensagem.id");
		
		Query q = createQuery("select smu.mensagem from StatusMensagemUsuario smu "
				+ "where smu.usuario.id = ? and smu.conversa.id = ? and smu.excluida = 0 "
				+ "order by smu.mensagem.id");
		
		q.setLong(0, u.getId());
		q.setLong(1, c.getId());
		q.setCacheable(true);
		List<Mensagem> mensagens = (List<Mensagem>) q.list();
		return mensagens;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void delete(Usuario u, Grupo grupo) {
		StringBuffer sb = new StringBuffer();
		sb.append("select s.id from StatusMensagemUsuario s where s.conversa.grupo.id = :grupoId and s.usuario.id = :usuarioId");
		Query q = createQuery(sb.toString());
		q.setParameter("grupoId", grupo.getId());
		q.setParameter("usuarioId", u.getId());
		List<Long> ids = q.list();
		
		if(ids != null && ids.size() > 0) {
			sb = new StringBuffer();
			sb.append("delete from StatusMensagemUsuario s where s.id in(:ids)");
			q = createQuery(sb.toString());
			q.setParameterList("ids", ids);
			q.executeUpdate();
		}
	}

	@Override
	public StatusMensagemUsuario findByMsgUsuario(Mensagem msg, Usuario u) {
		StringBuffer sb = new StringBuffer();
		sb.append("from StatusMensagemUsuario s where s.usuario.id = :userId AND s.mensagem.id = :msgId");
		Query q = createQuery(sb.toString());
		q.setParameter("userId", u.getId());
		q.setParameter("msgId", msg.getId());
		q.setCacheable(true);
		return (StatusMensagemUsuario) (q.list().size() > 0 ? q.list().get(0) : null);
	}
}