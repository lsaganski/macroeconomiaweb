package br.livetouch.livecom.rest.oauth;

import java.security.Principal;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.glassfish.jersey.internal.util.collection.ImmutableMultivaluedMap;
import org.glassfish.jersey.server.oauth1.OAuth1Consumer;
import org.glassfish.jersey.server.oauth1.OAuth1Token;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.rest.domain.UserPrincipal;

public class LivecomToken implements OAuth1Token {
	private String token;
	private String secret;
	private String consumerKey;
	private String callbackUrl;
	private Principal principal;
	private Set<String> roles;
	private MultivaluedMap<String, String> attribs;

	/**
	 * Metodo para gerar um access token
	 * 
	 * @param requestToken
	 */
	public void newAccessToken(LivecomToken requestToken) {
		this.token = newUUIDString();
		this.secret = newUUIDString(); 
		this.consumerKey = requestToken.getConsumer().getKey();
		this.callbackUrl = null;
		this.principal = requestToken.principal;
		this.roles = requestToken.roles; 
		this.attribs = ImmutableMultivaluedMap.<String, String> empty();
	}

	public void newAccessToken(String token, String secret, String consumerKey, Usuario usuario) {
		this.token = token;
		this.secret = secret;
		this.consumerKey = consumerKey;
		this.principal = new UserPrincipal(usuario);
		Set<String> roles = new HashSet<>();
		if(usuario.getPermissao() != null) {
			roles.add(usuario.getPermissao().getNome().toLowerCase());
		}
		this.roles = roles;
	}
	
	/**
	 * Metodo para gerar um request token
	 * 
	 * @param consumerKey
	 * @param callbackUrl
	 * @param attributes
	 */
	public void newRequestToken(String consumerKey, String callbackUrl, Map<String, List<String>> attributes) {
		this.consumerKey = consumerKey;
		this.callbackUrl = callbackUrl;
		this.attribs = new ImmutableMultivaluedMap<>(getImmutableMap(attributes));
		this.token = newUUIDString();
		this.secret = newUUIDString();
	}

	/**
	 * Metodo para setar o usuário e suas autorizações
	 * 
	 * @param principal
	 * @param roles
	 */
	public void authorize(Principal principal, Set<String> roles) {
		this.principal = principal;
		this.roles = roles == null ? Collections.<String> emptySet() : new HashSet<>(roles);
	}
	
	@Override
	public String getToken() {
		return token;
	}

	@Override
	public String getSecret() {
		return secret;
	}

	@Override
	public OAuth1Consumer getConsumer() {
		return new LivecomOAuthProvider().getConsumer(consumerKey);
	}

	@Override
	public MultivaluedMap<String, String> getAttributes() {
		return attribs;
	}

	@Override
	public Principal getPrincipal() {
		return principal;
	}

	@Override
	public boolean isInRole(final String role) {
		return roles.contains(role);
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}
	
	/**
	 * Metodo para gerar os token
	 * 
	 * @return
	 */
	public static String newUUIDString() {
		String tmp = UUID.randomUUID().toString();
		return tmp.replaceAll("-", "");
	}
	
	public static MultivaluedMap<String, String> getImmutableMap(Map<String, List<String>> map) {
		MultivaluedHashMap<String, String> newMap = new MultivaluedHashMap<>();
		
		if (map == null) {
			return newMap;
		}
		
		for (Map.Entry<String, List<String>> entry : map.entrySet()) {
			newMap.put(entry.getKey(), entry.getValue());
		}
		
		return newMap;
	}
}