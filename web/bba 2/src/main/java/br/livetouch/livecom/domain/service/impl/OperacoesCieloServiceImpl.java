package br.livetouch.livecom.domain.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.OperacoesCielo;
import br.livetouch.livecom.domain.repository.OperacoesCieloRepository;
import br.livetouch.livecom.domain.service.OperacoesCieloService;
import br.livetouch.livecom.domain.vo.OperacoesCieloVO;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class OperacoesCieloServiceImpl implements OperacoesCieloService {

	@Autowired
	private OperacoesCieloRepository rep;

	@Override
	public OperacoesCielo get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(OperacoesCielo transacao, Empresa empresa) throws DomainException {
		transacao.setEmpresa(empresa);
		rep.saveOrUpdate(transacao);
	}

	@Override
	public void delete(OperacoesCielo transacao) throws DomainException {
		rep.delete(transacao);
	}

	@Override
	public List<Object[]> findIdDataTransacao(Long idEmpresa) {
		return rep.findIdDataTransacao(idEmpresa);
	}
	
	@Override
	public List<OperacoesCielo> transacoesByEmpresa(Empresa empresa, int page, Integer max) {
		return rep.transacoesByEmpresa(empresa, page, max, false);
	}

	@Override
	public long getCountTransacoesByEmpresa(Empresa empresa, int page, Integer max) {
		return rep.getCountTransacoesByEmpresa(empresa, page, max, true);
	}

	@Override
	public String csvTransacao(Empresa empresa) {
		List<OperacoesCielo> findAll = findAll(empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("DATA").add("VOLUMETRIA").add("PICOTPS").add("MEDIA").add("MOMENTO");
		for (OperacoesCielo t : findAll) {
			body.add(t.getDataString())
			.add(t.getVolumetria() != null ? t.getVolumetria().toString() : "")
			.add(t.getPicoTps() != null ? t.getPicoTps().toString() : "")
			.add(t.getMedia() != null ? t.getMedia().toString() : "")
			.add(t.getMomento() != null ? t.getMomento().toString() : "");
			body.end();
		}
		return generator.toString();
	}
	
	@Override
	public List<OperacoesCielo> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	@Override
	public List<OperacoesCieloVO> graficoPicoTps() {
		return rep.graficoPicoTps();
	}
	
	@Override
	public List<OperacoesCieloVO> graficoMomento() {
		return rep.graficoMomento();
	}
	
	@Override
	public List<OperacoesCieloVO> graficoTransacao() {
		return rep.graficoTransacao();
	}

	@Override
	public OperacoesCielo getByData(Date data) {
		return rep.getByData(data);
	}

	@Override
	public void deleteAll(Long empresaId) {
		rep.deleteAll(empresaId);
	}

}
