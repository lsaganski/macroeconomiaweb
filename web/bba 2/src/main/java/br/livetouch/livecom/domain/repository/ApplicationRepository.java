package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Application;
import br.livetouch.livecom.domain.Usuario;

@Repository
public interface ApplicationRepository extends
		net.livetouch.tiger.ddd.repository.Repository<Application> {

	List<Application> findByUsuario(Usuario usuario);

	Boolean exists(Application app);
	
	Application get(Long id, Usuario u);

	Application find(String consumerKey, String consumerSecret);

}