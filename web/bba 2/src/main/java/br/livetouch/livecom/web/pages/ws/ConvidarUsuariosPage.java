package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.EntityField;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.OrigemCadastro;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ConvidarUsuariosPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private TextField tAssunto;
	private LongField tTemplate;
	private TextField tUsuarios;
	private TextField tTipo;
	private EntityField<Usuario> tUser;
	
	@Override
	public void onInit() {
		super.onInit();
		form.setMethod("post");
		form.add(tUser = new EntityField<>("user_id", usuarioService));
		form.add(tUsuarios = new TextField("usuarios", true));
		form.add(tAssunto = new TextField("assunto"));
		form.add(tTemplate = new LongField("template"));
		form.add(tTipo = new TextField("tipo", "tipo: 1 ou vazio - Convida usuários cadastrados no liveocm, 2 - convite usuários que não tem cadastro)"));
		form.add(tMode = new TextField("mode"));
		tMode.setValue("json");
		form.add(new Submit("Enviar"));
	}
	
	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario userInfo = tUser.getEntity();
			if(userInfo == null) {
				return new MensagemResult("NOK", "Usuário que esta convidando não existe");
			}
			
			String assunto = tAssunto.getValue();
			if(StringUtils.isEmpty(assunto)) {
				assunto = getParametrosMap().get(Params.EMAIL_SUBJECT_CONVITE, "Convite Livecom");
			}
			
			Long templateId = tTemplate.getLong();
			
			if(templateId == null) {
				return new MensagemResult("NOK", "Template não encontrado");
			}
			
			TemplateEmail template = templateEmailService.get(templateId);
			if(template == null) {
				return new MensagemResult("NOK", "Template não encontrado");
			}

			Empresa empresa = getEmpresa();
			
			String tipo = tTipo.getValue();
			List<Long> usersId = new ArrayList<>();
			/**
			 * Convite por LOTE
			 */
			if(StringUtils.isEmpty(tipo) || "1".equals(tipo)) {
				usersId = getListIds("usuarios");
			} else if("2".equals(tipo)) {
				String strUsuarios = tUsuarios.getValue();
				
				Perfil perfil = null;
				String perfilPadraoID = ParametrosMap.getInstance(getEmpresa()).get(Params.PERFIL_USER_PADRAO_ID);
				if(StringUtils.isNotEmpty(perfilPadraoID)) {
					perfil = perfilService.get(Long.parseLong(perfilPadraoID));
				} 
				
				if(perfil == null) {
					return new MensagemResult("NOK", "Não foi encontrado o perfil padrão de usuário");
				}
				
				/**
				 * Convite por E-mail de usuário que ainda tem cadastro no SISTEMA
				 */
				String[] split = strUsuarios.split(";");
				for (String s : split) {
					String[] infoUser = s.split(":");
					String nome = infoUser[0];
					String email = infoUser[1];
					
					Usuario usuario = usuarioService.findByEmail(email);
					if(usuario != null) {
						continue;
					}

					usuario = new Usuario();
					usuario.setNome(nome);
					usuario.setLogin(email);
					usuario.setEmail(email);
					usuario.setEmpresa(empresa);
					usuario.setPerfilAtivo(true);
					usuario.setPermissao(perfil);
					usuario.setOrigemCadastro(OrigemCadastro.WEB);
					usuarioService.saveOrUpdate(userInfo, usuario);
					
					if(infoUser.length == 3) {
						String[] splitGrupos = infoUser[2].split(",");
						for (String g : splitGrupos) {
							Grupo grupo = grupoService.get(Long.parseLong(g));
							if(grupo != null) {
								usuarioService.addGrupoToUser(usuario, grupo, true);
							}
 						}
					}
					usersId.add(usuario.getId());
				}
			} else {
				return new MensagemResult("NOK", "Tipo " + tipo + " inválido");
			}
			
			if(usersId.size() > 0) {
				usuarioService.convidarUsuarios(empresa, userInfo, getContextPath(), usersId, template, assunto);
				return new MensagemResult("OK", "E-mail de convite enviado com sucesso.");
			} else {
				return new MensagemResult("NOK", "Não foi enviado nenhum convite para os usuários da lista");
			}
			
		}
		return new MensagemResult("NOK", "Formulario invalido");

	}

	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
