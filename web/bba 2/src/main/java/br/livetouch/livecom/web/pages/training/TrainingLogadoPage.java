package br.livetouch.livecom.web.pages.training;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.HomePage;

public class TrainingLogadoPage extends TrainingPage {
	public static final String TEMPLATE = "/training/border.htm";

	@Override
	public String getTemplate() {
		return TrainingLogadoPage.TEMPLATE;
	}

	@Override
	public boolean onSecurityCheck() {
		if(!isBuildBJJ()) {
			// livecom
			setRedirect(HomePage.class);
			return false;
		}
		Usuario u = getUsuario();
		boolean logado = u != null;
		if (logado) {
			return super.onSecurityCheck();
		}
		redirectLogin();
		return onSecurityCheckNotOk();
	}
}