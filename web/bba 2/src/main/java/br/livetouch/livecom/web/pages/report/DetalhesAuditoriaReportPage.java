package br.livetouch.livecom.web.pages.report;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.vo.PushReportFiltro;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;

@Controller
@Scope("prototype")
public class DetalhesAuditoriaReportPage extends RelatorioPage {
	
	public PushReportFiltro filtro;
	public int page;
	public Long id;
	
	public LogAuditoria auditoria;
	
	@Override
	public void onInit() {
		super.onInit();

		if(clear) {
			getContext().removeSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		}
		
		if (id != null) {
			auditoria = logAuditoriaService.get(id);
		}

	}
	

	@Override
	public void onGet() {
		super.onGet();
	}
	
	@Override
	public String getContentType() {
		return super.getContentType();
	}
	
	@Override
	public String getTemplate() {
		return super.getTemplate();
	}
	
}
