package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Likes;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostUsuarios;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.GrupoNotification;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.repository.LikeRepository;
import br.livetouch.livecom.domain.repository.PostRepository;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.LikeService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.LikeVO;
import br.livetouch.livecom.domain.vo.PostCountVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioLikesVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class LikeServiceImpl extends LivecomService<Likes> implements LikeService {
	@Autowired
	private LikeRepository rep;

	@Autowired
	private PostRepository repPost;
	
	@Autowired
	protected NotificationService notificationService;
	
	@Autowired
	protected UsuarioService usuarioService;
	
	@Autowired
	protected GrupoService grupoService;
	
	@Autowired
	PostService postService;

	@Override
	public Likes get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Likes c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Likes> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void delete(Likes c) throws DomainException {
		rep.delete(c);
	}
	
	@Override
	public void delete(List<Long> ids) throws DomainException {
		executeIn("delete from Likes where id in (:ids)","ids",ids);
	}

	@Override
	public Likes findByUserAndPost(Usuario u, Post post) {
		return rep.findLikeByUserAndPost(u, post);
	}

	@Override
	public Likes findByUserAndComentario(Usuario u, Comentario c) {
		return rep.findLikeByUserAndComentario(u, c);
	}

	@Override
	public List<Likes> findAllLikesOfPostByUser(Usuario userInfo) {
		return rep.findAllLikesOfPostByUser(userInfo);
	}
	
	@Override
	public List<Likes> findAllLikesByUser(Usuario u) {
		return rep.findAllLikesByUser(u);
	}

	@Override
	public List<Likes> findAllLikesNotificationByUser(Usuario user) {
		if (user == null) {
			return new ArrayList<Likes>();
		}
		return rep.findAllLikesNotificationByUser(user);
	}

	@Override
	public List<Likes> findAllLikesOfComentarioByPost(Post post) {
		return rep.findAllLikesOfComentarioByPost(post);
	}

	@Override
	public List<Likes> findAllLikesByPost(Post p) {
		return rep.findAllLikesByPost(p);
	}
	
	@Override
	public long getCountLikesByPost(Post p) {
		return rep.getCountLikesByPost(p);
	}

	@Override
	public List<Likes> findAllLikesByComentario(Comentario c) {
		return rep.findAllLikesByComentario(c);
	}

	@Override
	public void clearNotification(Long id) throws DomainException {
		Likes likes = get(id);
		likes.setLidaNotification(true);
		saveOrUpdate(likes);
	}

	@Override
	public long getCountByFilter(RelatorioFiltro filtro, boolean count, boolean geral) throws DomainException {
		return rep.getCountByFilter(filtro, count, geral);
	}

	@Override
	public List<RelatorioLikesVO> findbyFilter(RelatorioFiltro filtro, boolean count) throws DomainException {
		return rep.findbyFilter(filtro, count);
	}

	@Override
	public List<LikeVO> findDetalhesByDate(RelatorioFiltro filtro) {
		if (filtro.getPost() == null && filtro.getPostId() != null) {
			filtro.setPost(repPost.get(filtro.getPostId()));
		}
		List<Likes> list = rep.findDetalhesByDate(filtro, false);
		List<LikeVO> vos = new ArrayList<LikeVO>();
		for (Likes like : list) {
			LikeVO vo = new LikeVO();
			vo.setLikes(like);
			vos.add(vo);
		}
		return vos;
	}

	@Override
	public long getCountDetalhesByDate(RelatorioFiltro filtro, boolean b) {
		return rep.getCountDetalhesByDate(filtro, b);
	}

	@Override
	public String csvDetalhesLikes(RelatorioFiltro filtro) {
		List<LikeVO> list = findDetalhesByDate(filtro);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("Data").add("Usuario").add("Curtida");
		for (LikeVO vo : list) {
			body.add(vo.getData() != null ? vo.getData() : "").add(vo.getUserNome() != null ? vo.getUserNome() : "")
					.add(vo.getPostTitulo() != null ? vo.getPostTitulo() : vo.getComentario() != null ? vo.getComentario() : "");
			body.end();
		}
		return generator.toString();
	}

	@Override
	public List<RelatorioLikesVO> findbyFilterConsolidado(RelatorioFiltro filtro) throws DomainException {
		return rep.findbyFilterConsolidado(filtro);
	}

	@Override
	public String csvLikes(RelatorioFiltro filtro) throws DomainException {
		if (StringUtils.equals(filtro.getModo(), "expandido")) {
			List<RelatorioLikesVO> list = findbyFilter(filtro, false);
			CSVGenerator generator = new CSVGenerator();
			CSVHeader header = generator.getHeader();
			CSVBody body = generator.getBody();
			header.add("Data").add("Post").add("Usuario");
			for (RelatorioLikesVO vo : list) {
				body.add(vo.getData() != null ? vo.getData() : "").add(vo.getTitulo() != null ? vo.getTitulo() : "").add(vo.getUsuario() != null ? vo.getUsuario() : "");
				body.end();
			}
			return generator.toString();
		} else {
			List<RelatorioLikesVO> list = findbyFilterConsolidado(filtro);
			CSVGenerator generator = new CSVGenerator();
			CSVHeader header = generator.getHeader();
			CSVBody body = generator.getBody();
			header.add("Data").add("Post").add("Curtidas");
			for (RelatorioLikesVO vo : list) {
				body.add(vo.getData() != null ? vo.getData() : "").add(vo.getTitulo() != null ? vo.getTitulo() : "").add(String.valueOf(vo.getCount()));
				body.end();
			}
			return generator.toString();
		}
	}

	@Override
	public Likes like(Usuario u, Post post, Comentario comentario, boolean liked) throws DomainException {
		// Like
		Likes l = null;
		if (comentario != null) {
			comentario.like(liked);
			l = findByUserAndComentario(u, comentario);
		} else if (post != null) {
			post.like(liked);
			l = findByUserAndPost(u, post);
		} 

		if (l == null) {
			l = new Likes();
			l.setUsuario(u);
			l.setPost(post);
			l.setComentario(comentario);
		}
		l.setFavorito(liked);
		l.setData(new Date());
		if (liked) {
			l.setLidaNotification(false);
		}

		// Like Report
		saveOrUpdate(l);

		// Notification
		if (liked) {
			push(l);
		}

		return l;
	}
	
	/**
	 * Salva o Push
	 */
	private void push(Likes l) throws DomainException {
		
		Usuario u = l.getUsuario();
		Post post = l.getPost();
		Comentario comentario = l.getComentario();
		Usuario userTo = post.getUsuario();
		
		boolean isComentario = false;
		
		if(comentario != null) {
			isComentario = true;
			userTo = comentario.getUsuario();
		}
		
		Boolean isSend = true;
		Set<Grupo> grupos = new HashSet<Grupo>();
		grupos.addAll(post.getGrupos());
		List<Grupo> subgrupos = grupoService.findSubgruposInGrupos(post.getGrupos());
		if(subgrupos != null && subgrupos.size() > 0) {
			grupos.addAll(subgrupos);
		}
		
		if(grupos.isEmpty() && !u.getId().equals(userTo.getId())) {
			isSend = true;
		}
		
		for (Grupo grupo : grupos) {
			GrupoUsuarios grupoUsuario = usuarioService.getGrupoUsuario(userTo, grupo);
			if(grupoUsuario != null && grupoUsuario.isPush()) {
				isSend = true;
				break;
			}
		}
		
		if(isComentario) {
			isSend &= Livecom.getInstance().hasPermissao(ParamsPermissao.CURTIR_COMENTARIOS, userTo);
		} else {
			isSend &= Livecom.getInstance().hasPermissao(ParamsPermissao.CURTIR_PUBLICACAO, userTo);
		}
		
		if(isSend == false) {
			return;
		}
		
		Empresa e = u.getEmpresa();

		String texto = null;
		String titulo = null;
		String tituloNot = null;
		String subTituloNot = null;
		String textoNotificacao = null;
		ParametrosMap params = ParametrosMap.getInstance(e);
		if (comentario != null) {
			texto = params.get(Params.MSG_PUSH_LIKE_COMENTARIO, "[%user%] curtiu seu comentário [%titulo%]");
			titulo = post.getTituloDesc();
			
			tituloNot = params.get(Params.PUSH_LIKE_COMENTARIO_TITULO_NOTIFICATION, "Comentário curtido");
			subTituloNot = params.get(Params.PUSH_LIKE_COMENTARIO_SUB_TITULO_NOTIFICATION, "[%user%] curtiu seu comentário [%titulo%]");
			
			textoNotificacao = u.getNome() + " curtiu seu comentário no post " + titulo;
		} else {
			texto = params.get(Params.MSG_PUSH_LIKE_POST, "[%user%] curtiu seu post [%titulo%]");
			titulo = post.getTituloDesc();
			
			tituloNot = params.get(Params.PUSH_LIKE_POST_TITULO_NOTIFICATION, "Post curtido");
			subTituloNot = params.get(Params.PUSH_LIKE_POST_SUB_TITULO_NOTIFICATION, "[%user%] curtiu seu post [%titulo%]");
			
			textoNotificacao = u.getNome() + " curtiu a publicação " + titulo;
			
		}
		
		boolean pushOn = params.isPushOn();
		
		if(StringUtils.isNotEmpty(subTituloNot)) {
			subTituloNot = StringUtils.replace(subTituloNot,"%user%", u.getNome());
			subTituloNot = StringUtils.replace(subTituloNot,"%titulo%", post.getTituloDesc());
		}

		if(StringUtils.isNoneEmpty(texto)) {
			texto = StringUtils.replace(texto,"%user%", u.getNome());
			texto = StringUtils.replace(texto,"%titulo%", post.getTituloDesc());
		}
		
		Notification n = new Notification();
		n.setData(new Date());
		n.setTipo(TipoNotificacao.LIKE);
		n.setGrupoNotification(GrupoNotification.MURAL);
		n.setTitulo(titulo);
		n.setTexto(texto);
		n.setPost(post);
		n.setTituloNot(tituloNot);
		n.setSubTituloNot(subTituloNot);
		n.setComentario(comentario);
		n.setTextoNotificacao(textoNotificacao);
		n.setUsuario(u);
		n.setSendPush(pushOn);
		n.setVisivel(true);
		
		PostUsuarios postUsuarios = repPost.getPostUsuarios(userTo, post);
		if(postUsuarios != null) {
			n.setSendPush(postUsuarios.isPush());
			n.setVisivel(postUsuarios.isNotification());
		}
		// Salva a notification. O Job vai enviar depois
		notificationService.save(n);
		
		
		// salva a notification para ser enviada pelo job
		NotificationUsuario notUsuario = new NotificationUsuario();
		notUsuario.setUsuario(userTo);
		notUsuario.setNotification(n);
		notUsuario.setVisivel(true);
		PostUsuarios postUser = postService.getPostUsuarios(userTo, post);
		if(postUser != null && !postUser.isNotification()) {
			notUsuario.setLida(true);
		}
		notificationService.saveOrUpdate(notUsuario);
	}

	@Override
	public List<Likes> findAllByPosts(Usuario user, List<Long> ids) {
		return rep.findAllByPosts(user, ids);
	}

	@Override
	public List<PostCountVO> findCountByPosts(List<Long> ids) {
		return rep.findCountByPosts(ids);
	}

	@Override
	public Long countByPost(Post p) {
		return rep.countByPost(p);
	}
	
	@Override
	public List<RelatorioVisualizacaoVO> reportLikes(RelatorioFiltro filtro) throws DomainException {
		return rep.reportLikes(filtro);
	}
}
