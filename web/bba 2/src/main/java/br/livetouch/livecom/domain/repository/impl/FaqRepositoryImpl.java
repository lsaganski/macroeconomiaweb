package br.livetouch.livecom.domain.repository.impl;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Faq;
import br.livetouch.livecom.domain.repository.FaqRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class FaqRepositoryImpl extends StringHibernateRepository<Faq> implements FaqRepository {

	public FaqRepositoryImpl() {
		super(Faq.class);
	}

}