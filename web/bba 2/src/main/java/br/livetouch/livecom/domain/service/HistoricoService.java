package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Historico;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

public interface HistoricoService extends Service<Historico> {

	List<Historico> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Historico f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Historico f) throws DomainException;

	List<Historico> findAllByUser(Usuario u);

	List<Post> findAllPostsByUser(Usuario u, int page, int max);

	Historico findByUserPost(Usuario u, Post p);

}
