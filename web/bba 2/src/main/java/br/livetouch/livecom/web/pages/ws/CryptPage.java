package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.crypt.AESCrypt;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextArea;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class CryptPage extends WebServiceXmlJsonPage {
	
	public Form form = new Form();
	public String key, text;
	public String decrypt;
	
	public void onInit() {
		form.add(new TextField("key",true));
		form.add(new TextArea("text",50,20));
		form.add(new TextField("decrypt","decrypt=1"));
		
		form.add(new Submit("execute"));
		
		setFormTextWidth(form, 600);
	};

	@Override
	protected Object execute() throws Exception {
		if(form.isValid()) {
			boolean decrypt = "1".equals(this.decrypt);
			if(decrypt) {
				msg = AESCrypt.decrypt(key, text);
			} else {
				msg = AESCrypt.encrypt(key, text);
			}
			
		}
		return msg;
	}
}
