package br.livetouch.livecom.rest.request;

import br.livetouch.livecom.domain.Menu;

public class MenuMobileRequest {
	public String id;
	public String label;
	public String descricao;
	public String codigo;
	public String parametro;
	public String icone;
	public String ordem;
	public boolean divider;
	public boolean menuDefault;
	
	public Menu toMobile(Menu menu) {
		if(menu == null) {
			menu = new Menu();
		}
		menu.setDescricao(descricao);
		menu.setLabel(label);
		menu.setLink(codigo);
		menu.setIcone(icone);
		menu.setMobile(true);
		menu.setDefault(menuDefault);
		menu.setDivider(divider);
		menu.setParametro(parametro);
		return menu;
	}
}
