package br.livetouch.livecom.sender;

import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.enums.TipoTemplate;

@Service
public class SenderRecuperarSenha extends Sender {

	@Override
	public String getSubject() {
		String subject = getParamMap(Params.EMAIL_SUBJECT_RECUPERAR_SENHA, "Livecom - nova senha");
		return subject;
	}

	@Override
	protected TipoTemplate getTipoTemplate() {
		TipoTemplate tipo = TipoTemplate.alterarSenha;
		return tipo;
	}

	@Override
	protected String getNameFileTemplate() {
		String file = "email_recuperar_senha.htm";
		return file;
	}
}
