package br.livetouch.livecom.domain.enums;

/**
 * @author Ricardo Lecheta
 * @created 23/02/2013
 */
public enum StatusParticipacao {

	APROVADA("APROVADA","Participação aprovada para participar do grupo"),
	RECUSADA("RECUSADA","Participação recusada para participar do grupo"),
	SOLICITADA("SOLICITADA","Participação solicitada para participar do grupo");
	private final String s;
	private final String desc2;

	StatusParticipacao(String s,String desc2){
		this.s = s;
		this.desc2 = desc2;
	}

	@Override
	public String toString() {
		return s != null ? s : "?";
	}

	public String getDesc() {
		return s;
	}
	
	public String getDesc2() {
		return desc2;
	}
}
