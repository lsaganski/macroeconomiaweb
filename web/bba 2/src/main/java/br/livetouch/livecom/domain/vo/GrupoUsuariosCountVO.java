package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.service.GrupoService;

public class GrupoUsuariosCountVO {

	private GrupoVO grupo;

	public GrupoVO getGrupo() {
		return grupo;
	}

	@Override
	public String toString() {
		return "GrupoUsuarios [grupo=" + grupo.getNome() + ", countUsers=" + grupo.countUsers + "]";
	}
	
	public static List<GrupoUsuariosCountVO> toListVO(GrupoService grupoService,List<GrupoUsuariosVO> grupos) {
		List<GrupoUsuariosCountVO> list = new ArrayList<GrupoUsuariosCountVO>();
		if(grupos != null) {
			for (GrupoUsuariosVO g : grupos) {
				Grupo grupo = g.getGrupo();
				Long countUsers = g.getCountUsers();

				GrupoUsuariosCountVO gvo2 = new GrupoUsuariosCountVO();
				
				gvo2.grupo = grupoService.setGrupo(grupo);
				gvo2.grupo.countUsers = countUsers;
				
				list.add(gvo2);
			}
		}
		return list;
	}
}
