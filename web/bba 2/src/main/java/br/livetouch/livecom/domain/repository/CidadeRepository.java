package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Cidade;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.enums.Estado;

@Repository
public interface CidadeRepository extends net.livetouch.tiger.ddd.repository.Repository<Cidade> {

	List<Cidade> findAllByEstado(Estado estado);

	List<Cidade> findAllOrderByEstado();

	List<Cidade> findByNome(String nome, Estado estado, int page, int maxRows);

	Long countByNome(String nome, Estado estado, int page, int maxRows);

	Cidade findByName(Cidade cidade, Empresa empresa);
	
}