package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.StatusChat;
import br.livetouch.livecom.domain.Usuario;

@Repository
public interface StatusChatRepository extends net.livetouch.tiger.ddd.repository.Repository<StatusChat> {

	List<StatusChat>  findAll(Usuario user);

	List<StatusChat> getStatusUsuario();
	
}