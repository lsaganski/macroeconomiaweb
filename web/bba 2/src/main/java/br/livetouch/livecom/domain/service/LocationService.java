package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Location;
import net.livetouch.tiger.ddd.DomainException;

public interface LocationService extends Service<Location> {

	List<Location> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Location f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Location f) throws DomainException;

}
