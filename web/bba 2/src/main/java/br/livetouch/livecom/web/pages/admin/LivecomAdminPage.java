
package br.livetouch.livecom.web.pages.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.LogonPage;

/**
 * Page mae para usar o Autowired do Spring
 * 
 * @author ricardo
 * 
 */
@Controller
@Scope("prototype")
public class LivecomAdminPage extends LivecomLogadoPage {
	
	@Override
	public boolean onSecurityCheck() {
		Usuario u = getUsuario();
		boolean logado = u != null;
		if (logado) {
			
			if(hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return true;
			}
			
		}
		redirectLogin();
		return onSecurityCheckNotOk();
	}

	protected void redirectLogin() {
		LogonPage.redirect(this);
	}
}
