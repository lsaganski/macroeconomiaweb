package br.livetouch.livecom.jobs.importacao;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.dialect.Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Service;

import br.infra.livetouch.dialect.OracleDialect;
import br.infra.util.Log;
import br.infra.util.SQLUtils;
import br.infra.util.Utils;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Expediente;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.ImportarArquivoResponse;
import br.livetouch.livecom.domain.LinhaImportacao;
import br.livetouch.livecom.domain.LogImportacao;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.enums.OrigemCadastro;
import br.livetouch.livecom.domain.enums.Semana;
import br.livetouch.livecom.domain.service.AreaService;
import br.livetouch.livecom.domain.service.DiretoriaService;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.ExpedienteService;
import br.livetouch.livecom.domain.service.FuncaoService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.LinhaImportacaoService;
import br.livetouch.livecom.domain.service.LogImportacaoService;
import br.livetouch.livecom.domain.service.PerfilService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.livecom.utils.LivecomUtil;
import net.livetouch.extras.util.CripUtil;
import net.livetouch.hibernate.HibernateUtil;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ImportarArquivoUsuarioDefaultWork extends ImportacaoArquivoWork {

	protected static final Logger log = Log.getLogger("importacao_usuario");
	protected static final Logger logError = Log.getLogger("importacao_usuario_error");

	@Autowired
	protected UsuarioService usuarioService;
	@Autowired
	protected GrupoService grupoService;
	@Autowired
	protected PerfilService permissaoService;
	@Autowired
	protected ExpedienteService expedienteService;
	@Autowired
	protected FuncaoService cargoService;
	@Autowired
	protected AreaService areaService;
	@Autowired
	protected DiretoriaService diretoriaService;
	@Autowired
	protected LogImportacaoService logImportacaoService;
	@Autowired
	protected LinhaImportacaoService linhaImportacaoService;

	@Autowired
	protected EmpresaService empresaService;

	private HashMap<String, Object[]> mapLogins;
	private HashMap<String, Long> mapGrupos;
	private HashMap<String, Long> mapPermissao;
	private HashMap<String, Long> mapExpediente;
	private HashMap<String, Long> mapCargo;
	private HashMap<String, Long> mapArea;
	private HashMap<String, Long> mapDiretoria;
	private HashMap<String, Integer> mapHeader;
	private List<Long> idsUsuarios;
	private Session session;

	private List<LinhaImportacao> linhaErros;

	private File file;
	private Scanner scanner;
	private ImportarArquivoResponse response;

	private int linhas;
	private double porcentagem;

	private Long empresaId;

	private Dialect dialect;

	@Override
	public void init(Session session, File file, Dialect dialect) throws DomainException, IOException {

		this.session = session;
		this.file = file;
		this.dialect = dialect;

		empresaId = JobImportacaoUtil.getIdEmpresa(file);

		linhas = countLines(file.getAbsolutePath());
		JobInfo jobInfo = JobInfo.getInstance(empresaId);
		jobInfo.setFim(linhas);
		jobInfo.setTemArquivo(true);
		jobInfo.setStatus("");
		
		Empresa empresa = empresaService.get(empresaId);
		if (empresa == null) {
			throw new IllegalArgumentException("Empresa não cadastrada no sistema, arquivo: " + file);
		}
		jobInfo.setEmpresa(empresa);
		Boolean importacaoByNome = "1".equals(ParametrosMap.getInstance(empresaId).get(Params.IMPORTACAO_ARQUIVO_BY_NOME, "0"));

		log.debug("Carregando logins");
		List<Object[]> logins = usuarioService.findIdLoginsUsuario();
		mapLogins = new HashMap<String, Object[]>();
		for (Object[] user : logins) {
			String login = (String) user[1];
			mapLogins.put(login, user);
		}
		log.debug("Existem [" + mapLogins.size() + "] logins na base.");

		log.debug("Carregando grupos");
		List<Object[]> grupos = importacaoByNome ? grupoService.findIdNomeGrupos(empresaId) : grupoService.findIdCodGrupos(empresaId);
		mapGrupos = new HashMap<String, Long>();
		for (Object[] grupo : grupos) {
			Long idGrupo = (Long) grupo[0];
			String codGrupo = (String) grupo[1];
			if (StringUtils.isEmpty(codGrupo)) {
				codGrupo = idGrupo.toString();
			}
			mapGrupos.put(codGrupo, idGrupo);
		}
		log.debug("Existem [" + mapGrupos.size() + "] grupos na base.");

		log.debug("Carregando permissões");
		List<Object[]> permissoes = permissaoService.findIdNomePerfil();
		mapPermissao = new HashMap<String, Long>();
		for (Object[] permissao : permissoes) {
			Long id = (Long) permissao[0];
			String cod = (String) permissao[1];
			if (StringUtils.isEmpty(cod)) {
				cod = id.toString();
			}
			mapPermissao.put(cod, id);
		}
		log.debug("Existem [" + mapPermissao.size() + "] permissoes na base.");
		
		log.debug("Carregando expedientes");
		List<Object[]> expedientes = expedienteService.findIdNomeExpediente();
		mapExpediente = new HashMap<String, Long>();
		for (Object[] expediente : expedientes) {
			Long id = (Long) expediente[0];
			String cod = (String) expediente[1];
			if (StringUtils.isEmpty(cod)) {
				cod = id.toString();
			}
			mapExpediente.put(cod, id);
		}
		log.debug("Existem [" + mapExpediente.size() + "] expedientes na base.");

		log.debug("Carregando funções");
		List<Object[]> cargos = cargoService.findIdNomeFuncao(empresaId);
		mapCargo = new HashMap<String, Long>();
		for (Object[] cargo : cargos) {
			Long id = (Long) cargo[0];
			String cod = (String) cargo[1];
			if (StringUtils.isEmpty(cod)) {
				cod = id.toString();
			}
			mapCargo.put(cod, id);
		}
		log.debug("Existem [" + mapCargo.size() + "] cargos na base.");

		log.debug("Carregando areas");
		List<Object[]> areas = importacaoByNome ? areaService.findIdNomeAreas(empresaId) : areaService.findIdCodAreas(empresaId);
		mapArea = new HashMap<String, Long>();
		for (Object[] area : areas) {
			Long id = (Long) area[0];
			String cod = (String) area[1];
			if (StringUtils.isEmpty(cod)) {
				cod = id.toString();
			}
			mapArea.put(cod, id);
		}
		log.debug("Existem [" + mapArea.size() + "] areas na base.");

		log.debug("Carregando diretorias");

		List<Object[]> diretorias = importacaoByNome ? diretoriaService.findIdNomeDiretorias(empresaId) : diretoriaService.findIdCodDiretorias(empresaId);
		mapDiretoria = new HashMap<String, Long>();
		for (Object[] diretoria : diretorias) {
			Long id = (Long) diretoria[0];
			String cod = (String) diretoria[1];
			if (StringUtils.isEmpty(cod)) {
				cod = id.toString();
			}
			mapDiretoria.put(cod, id);
		}
		log.debug("Existem [" + mapDiretoria.size() + "] diretorias na base.");
	}

	@Override
	public void execute(Connection connection) throws SQLException {
		if (file == null || !file.exists()) {
			throw new IllegalArgumentException("Arquivo inválido");
		}
		
		DateFormat formatter = new SimpleDateFormat("HH:mm");

		response = new ImportarArquivoResponse();
		int countOk = 0;
		int countError = 0;

		long timeA = System.currentTimeMillis();

		Date inicio = new Date();

		/**
		 * https://docs.oracle.com/javase/7/docs/api/java/nio/charset/Charset.
		 * html
		 **/
		String set = ParametrosMap.getInstance(empresaId).get(Params.IMPORTACAO_ARQUIVO_CHARSET, "UTF-8");
		final Charset ENCODING = Charset.isSupported(set) ? Charset.forName(set) : Charset.forName("UTF-8");
		Path path = Paths.get(file.getAbsolutePath());

		/**
		 * cod de grupo padrão não pode ser vazio, e os demais grupos devem ser
		 * separados por virgula
		 **/

		// LOGIN;NOME;EMAIL;FONE_FIXO;FONE_CEL;DATA_NASC;CARGO;AREA;DIRETORIA;DIRETORIA_EXECUTIVA;PERMISSAO;GRUPO_ORIGEM;GRUPOS
		PreparedStatement stmtInsert = createStatementInsert(dialect, connection);
		PreparedStatement stmtInsertGrupoUsuarios = createStatementInsertGrupoUsuario(dialect, connection);
		PreparedStatement stmtUpdate = connection.prepareStatement(
				"update usuario set login=? ,nome=?, email=?, ddd_telefone=?, telefone_fixo=?, ddd_celular=?, telefone_celular=?, data_nasc=?, funcao_id=?, area_id=? ,diretoria_id=? ,permissao_id=?, grupo_id=?, expediente_id=?, hora_abertura=?, hora_fechamento=?, flags=?, genero=?, origem_cadastro=? where id=?");
		PreparedStatement stmtDelete = connection.prepareStatement("delete from usuario where id=?");
		PreparedStatement stmtDeleteGrupoUsuario = connection
				.prepareStatement("delete from grupo_usuarios where usuario_id=?");
		PreparedStatement stmtFindGrupoUsuarios = connection
				.prepareStatement("select id from grupo_usuarios where usuario_id = ? and grupo_id = ?");
		ResultSet rsGrupoUsuarios = null;

		int row = 0;
		String linha = "";
		JobInfo jobInfo = JobInfo.getInstance(empresaId);
		try {
			// Cargo por padrao importa como USUARIO 
			Funcao c = cargoService.findDefaultByEmpresa(empresaId);
			Long cargoDefaultId = c != null ? c.getId() : null;

			// Grupo por padrao importa como Geral (1L)
			Grupo grupoDefault = grupoService.findDefaultByEmpresa(empresaId);
			Long grupoDefaultId = grupoDefault != null ? grupoDefault.getId() : null;
			
			// Permissão por padrao importa como USUARIO
			Perfil p = permissaoService.findDefault();
			Long permissaoDefaultId = p != null ? p.getId() : null;
			
			log.debug("Iniciou ImportarArquivoWork com Batch ...");

			scanner = new Scanner(path, ENCODING.name());
			
			idsUsuarios = new ArrayList<>();

			boolean insert = false;
			mapHeader = new HashMap<String, Integer>();
			linhaErros = new ArrayList<LinhaImportacao>();
			jobInfo.setStatus("Inserindo usuarios");
			jobInfo.setSuccessful(true);
			while (scanner.hasNextLine()) {
				try {
					row++;

					boolean header = row == 1;

					linha = scanner.nextLine();
					String[] split = null;// = StringUtils.split(linha, ";");
					if (StringUtils.contains(linha, ";")) {
						split = linha.split(";");
					}

					if (StringUtils.startsWith(linha, "--")) {
						continue;
					}

					if (header) {
						jobInfo.setHeader(linha);
						if (split != null) {
							for (int i = 0; i < split.length; i++) {
								if (StringUtils.isNotEmpty(split[i]) && StringUtils.isNotBlank(split[i])) {
									mapHeader.put(split[i].toUpperCase(), i + 1);
								}
							}
						}
					} else {
						// LOGIN;NOME;EMAIL;FONE_FIXO;FONE_CEL;DATA_NASC;CARGO;AREA;DIRETORIA;DIRETORIA_EXECUTIVA;PERMISSAO;GRUPO_ORIGEM;GRUPOS
						String login = getValue(split, "LOGIN");
						String nome = getValue(split, "NOME");
						String email = getValue(split, "EMAIL");
						String dddFixo = getValue(split, "DDD_FIXO");
						String foneFixo = getValue(split, "FONE_FIXO");
						String dddCel = getValue(split, "DDD_CEL");
						String foneCel = getValue(split, "FONE_CEL");
						String sexo = getValue(split, "SEXO");
						Date dataNasc = getValueDate(split, "DATA_NASC");
						Long permissaoId = getValueLongId(split, mapPermissao, "PERMISSAO");
						Long expedienteId = getValueLongId(split, mapExpediente, "PERIODO_TRAB");
						String password = getValue(split, "SENHA");
						String horaInicio = getValue(split, "HORARIO_TRAB_INI");
						String horaFim = getValue(split, "HORARIO_TRAB_FIM");
						
						String origem = OrigemCadastro.IMPORTADO.name();
						
						//Método extração DDD
//						String dddFixo = Utils.getDDD(foneFixo);
//						String dddCel = Utils.getDDD(foneCel);
//						foneFixo = Utils.getTelefoneSemDDD(foneFixo);
//						foneCel = Utils.getTelefoneSemDDD(foneCel);

						if (StringUtils.isEmpty(email)) {
							throw new DomainException("Email não informado");
						}

						if (StringUtils.isEmpty(login)) {
							throw new DomainException("Login não informado");
						}
						
						if(!Utils.validaEmail(email)){
							throw new DomainException("Email inválido");
						}
						
						sexo = Utils.validaSexo(sexo);
						
						if (permissaoId == null) {
							// Permissao por padrao importa como VIZUALIZAÇÃO
							permissaoId = permissaoDefaultId;
						}
						
						// Expedientes
						boolean containsExpediente = expedienteId == null && mapHeader.containsKey("PERIODO_TRAB");
						if (containsExpediente) {
							String codExpediente = JobImportacaoUtil.get(split, mapHeader.get("PERIODO_TRAB"));
							if (codExpediente != null) {
								expedienteId = createExpediente(codExpediente);
								if(expedienteId != null) {
									mapExpediente.put(codExpediente, expedienteId);
								} else {
									log.error("PERIODO_TRAB nao encontrado: " + codExpediente);
								}
							}
						}
						
						Long grupoId = getValueLongId(split, mapGrupos, "GRUPO_ORIGEM");
						String grupoOrigem = getValue(split, "GRUPO_ORIGEM");

						// Grupo Padrao
						List<Long> idGrupos = new ArrayList<Long>();
						if (grupoId != null) {
							idGrupos.add(grupoId);
						}

						// Grupos adicionais
						boolean containsGrupos = mapHeader.containsKey("GRUPOS");
						if (containsGrupos) {
							String codGrupos = JobImportacaoUtil.get(split, mapHeader.get("GRUPOS"));
							// ADD GRUPO PADRÃO EM GRUPOS
							if (codGrupos != null) {
								codGrupos = codGrupos.replace(" ", "");
								String[] grupos = codGrupos.split(",");
								if (grupos != null && grupos.length > 0) {
									
									//ADD grupo padrao aqui se null
									if(grupoId == null) {
										grupoId = mapGrupos.get(grupos[0]);
									}
									
									for (String g : grupos) {
										Long idGrupo = mapGrupos.get(g);
										if (idGrupo != null) {
											idGrupos.add(idGrupo);
										}
									}
								}
							}
						}
						
						if (grupoId == null) {
							grupoId = grupoDefaultId;
						}

						Long cargoId = getValueLongId(split, mapCargo, "CARGO");
						if (cargoId == null) {
							cargoId = cargoDefaultId;
						}

						Long areaId = getValueLongId(split, mapArea, "AREA");

						Long diretoriaId = getValueLongId(split, mapDiretoria, "DIRETORIA");

						insert = false;
						Long id = null;
						Object[] user = mapLogins.get(login);
						if (user != null) {
							id = (Long) user[0];
						}

						if (id == null) {

							insert = true;

							// insert into usuario
							log("insert row[" + row + "] " + login);
							stmtInsert.setString(1, login);
							stmtInsert.setString(2, nome);
							stmtInsert.setString(3, email);
							stmtInsert.setString(4, dddFixo);
							stmtInsert.setString(5, foneFixo);
							stmtInsert.setString(6, dddCel);
							stmtInsert.setString(7, foneCel);
							if (dataNasc == null) {
								stmtInsert.setNull(8, java.sql.Types.DATE);
							} else {
								stmtInsert.setDate(8, DateUtils.toSqlDate(dataNasc));
							}
							if (cargoId == null) {
								stmtInsert.setNull(9, java.sql.Types.BIGINT);
							} else {
								stmtInsert.setLong(9, cargoId);
							}
							if (areaId == null) {
								stmtInsert.setNull(10, java.sql.Types.BIGINT);
							} else {
								stmtInsert.setLong(10, areaId);
							}
							if (diretoriaId == null) {
								stmtInsert.setNull(11, java.sql.Types.BIGINT);
							} else {
								stmtInsert.setLong(11, diretoriaId);
							}
							// AREA;DIRETORIA;DIRETORIA_EXECUTIVA
							if (permissaoId == null) {
								stmtInsert.setNull(12, java.sql.Types.BIGINT);
							} else {
								stmtInsert.setLong(12, permissaoId);
							}
							if (grupoId == null) {
								stmtInsert.setNull(13, java.sql.Types.BIGINT);
							} else {
								stmtInsert.setLong(13, grupoId);
							}
							if (empresaId == null) {
								stmtInsert.setNull(14, java.sql.Types.BIGINT);
							} else {
								stmtInsert.setLong(14, empresaId);
							}
							if (expedienteId == null) {
								stmtInsert.setNull(15, java.sql.Types.BIGINT);
							} else {
								stmtInsert.setLong(15, expedienteId);
							}
							
							if (StringUtils.isEmpty(horaInicio)) {
								stmtInsert.setNull(16, java.sql.Types.TIME);
							} else {
								stmtInsert.setTime(16, new java.sql.Time(formatter.parse(horaInicio).getTime()));
							}
							
							if (StringUtils.isEmpty(horaFim)) {
								stmtInsert.setNull(17, java.sql.Types.TIME);
							} else {
								stmtInsert.setTime(17, new java.sql.Time(formatter.parse(horaFim).getTime()));
							}
							
							if (!isLDAP(grupoOrigem)) {
								stmtInsert.setNull(18, java.sql.Types.VARCHAR);
							} else {
								stmtInsert.setString(18, "ldap");
							}
							
							// Senha
							String senha;
							if (StringUtils.isEmpty(password)) {
								senha = LivecomUtil.gerarSenha(empresaId);
								senha = CripUtil.criptToMD5(senha);
							} else {
								senha = CripUtil.criptToMD5(password);
							}
							stmtInsert.setString(19, senha);
							
							stmtInsert.setString(20, sexo);
							stmtInsert.setString(21, origem);

							stmtInsert.execute();

							// ID auto incremento
							id = SQLUtils.getGeneratedId(stmtInsert);
							user = new Object[] { id, login, empresaId };
							mapLogins.put(login, user);
							if (id == null) {
								LinhaImportacao erro = new LinhaImportacao();
								erro.setLinha(linha);
								erro.setMensagem("Erro ao inserir usuario " + login);
								erro.setNumeroLinha(row);
								linhaErros.add(erro);
								logError.error("Erro row [" + row + "] ao inserir usuario: " + login);
								continue;
							}

						} else {
							// update

							Long empresa = (Long) user[2];
							if (empresa != empresaId) {
								throw new DomainException("Login ja esta em uso");
							}

							stmtUpdate.setString(1, login);
							stmtUpdate.setString(2, nome);
							stmtUpdate.setString(3, email);
							stmtUpdate.setString(4, dddFixo);
							stmtUpdate.setString(5, foneFixo);
							stmtUpdate.setString(6, dddCel);
							stmtUpdate.setString(7, foneCel);
							if (dataNasc == null) {
								stmtUpdate.setNull(8, java.sql.Types.DATE);
							} else {
								stmtUpdate.setDate(8, DateUtils.toSqlDate(dataNasc));
							}
							if (cargoId == null) {
								stmtUpdate.setNull(9, java.sql.Types.BIGINT);
							} else {
								stmtUpdate.setLong(9, cargoId);
							}
							if (areaId == null) {
								stmtUpdate.setNull(10, java.sql.Types.BIGINT);
							} else {
								stmtUpdate.setLong(10, areaId);
							}
							if (diretoriaId == null) {
								stmtUpdate.setNull(11, java.sql.Types.BIGINT);
							} else {
								stmtUpdate.setLong(11, diretoriaId);
							}
							// AREA;DIRETORIA;DIRETORIA_EXECUTIVA
							if (permissaoId == null) {
								stmtUpdate.setNull(12, java.sql.Types.BIGINT);
							} else {
								stmtUpdate.setLong(12, permissaoId);
							}
							if (grupoId == null) {
								stmtUpdate.setNull(13, java.sql.Types.BIGINT);
							} else {
								stmtUpdate.setLong(13, grupoId);
							}

							if (expedienteId == null) {
								stmtUpdate.setNull(14, java.sql.Types.BIGINT);
							} else {
								stmtUpdate.setLong(14, expedienteId);
							}
							
							if (StringUtils.isEmpty(horaInicio)) {
								stmtUpdate.setNull(15, java.sql.Types.TIME);
							} else {
								stmtUpdate.setTime(15, new java.sql.Time(formatter.parse(horaInicio).getTime()));
							}
							
							if (StringUtils.isEmpty(horaFim)) {
								stmtUpdate.setNull(16, java.sql.Types.TIME);
							} else {
								stmtUpdate.setTime(16, new java.sql.Time(formatter.parse(horaFim).getTime()));
							}
							
							if (!isLDAP(grupoOrigem)) {
								stmtUpdate.setNull(17, java.sql.Types.VARCHAR);
							} else {
								stmtUpdate.setString(17, "ldap");
							}
							
							stmtUpdate.setString(18, sexo);
							stmtUpdate.setString(19, origem);
							
							stmtUpdate.setLong(20, id);
							
							stmtUpdate.execute();
							log("update row[" + row + "] " + login);
						}
						
						// Insert Grupos Adicionais
						if (idGrupos != null && idGrupos.size() > 0) {

							boolean doinsert = false;

							for (Long idGrupo : idGrupos) {

								// Valida se usuario já está no grupo
								stmtFindGrupoUsuarios.setLong(1, id);
								stmtFindGrupoUsuarios.setLong(2, idGrupo);
								rsGrupoUsuarios = stmtFindGrupoUsuarios.executeQuery();

								if (!rsGrupoUsuarios.next()) {
									doinsert = true;
									break;
								}
							}

							// insert grupo_usuarios
							if (doinsert) {
								// limpa grupos
								stmtDeleteGrupoUsuario.setLong(1, id);
								stmtDeleteGrupoUsuario.execute();

								// insert
								for (Long idGrupo : idGrupos) {
									log("insert grupo usuario row[" + row + "] " + login + " - " + idGrupo);
									stmtInsertGrupoUsuarios.setLong(1, id);
									stmtInsertGrupoUsuarios.setLong(2, idGrupo);
									stmtInsertGrupoUsuarios.execute();
								}
							}

						}
						
						// Ids dos novos usuarios
						if (id != null) {
							// count
							if (insert) {
								insert = false;
								response.countInsert++;
							} else {
								response.countUpdate++;
							}
						}

						countOk++;

						porcentagem = ((double) (row - 1) * 100) / (double) linhas;
						jobInfo.setPorcentagem(porcentagem);
						jobInfo.setAtual(row);
					}

				} catch (Exception e) {
					countError++;
					jobInfo.setSuccessful(false);
					LinhaImportacao erro = new LinhaImportacao();
					erro.setLinha(linha);
					erro.setMensagem(e.getMessage());
					erro.setNumeroLinha(row);
					linhaErros.add(erro);
					log.error("Erro usuario linha [" + row + "]: " + e.getMessage(), e);
				}
			}

			log.debug("total usuarios insert/update: " + idsUsuarios.size());

			long timeB = System.currentTimeMillis();
			log("TimeB min: " + (timeB - timeA) / 1000 / 60);
			jobInfo.setSuccessful(true);

		} catch (Exception e) {
			jobInfo.setSuccessful(false);
			String msgError = "Erro importar grupos row[" + row + "] " + e.getMessage();
			logError.error(msgError, e);
			throw new SQLException(msgError, e);
		} finally {
			
			JdbcUtils.closeStatement(stmtInsert);
			JdbcUtils.closeStatement(stmtUpdate);
			JdbcUtils.closeStatement(stmtDelete);
			JdbcUtils.closeStatement(stmtInsertGrupoUsuarios);

			HibernateUtil.clearCache(session);

			log.debug("Início: " + inicio);
			log.debug("Final: " + new Date());
			long timeC = System.currentTimeMillis();
			log("TimeC min: " + (timeC - timeA) / 1000 / 60);

			response.countOk = countOk;
			response.countError = countError;
			response.nomeArquivo = JobImportacaoUtil.getNomeFileSemEmpresa(file);
			jobInfo.setResponse(response);

			// Log_Importacao
			finish(jobInfo);
		}

	}

	private void finish(JobInfo jobInfo) {
		LogImportacao logImportacao = new LogImportacao(jobInfo);
		try {
			logImportacaoService.saveOrUpdate(logImportacao);
			jobInfo.response.idArquivo = logImportacao.getId();
			for (LinhaImportacao erro : linhaErros) {
				erro.setLogImportacao(logImportacao);
				linhaImportacaoService.saveOrUpdate(erro);
			}
		} catch (DomainException e) {
			jobInfo.setSuccessful(false);
			log.debug("ERRO" + e.getMessage());
		}
		jobInfo.setStatus("Fim da Importação");
		jobInfo.setTemArquivo(false);
		jobInfo.setFim(0);
		jobInfo.setAtual(0);
	}
	
	private boolean isLDAP(String grupoOrigem) {
		if(StringUtils.isEmpty(grupoOrigem)) {
			return false;
		}
		String ldapGrupos = ParametrosMap.getInstance(empresaId).get(Params.IMPORTAR_USUARIO_LDAP_GRUPO, "CIELO,YEP");
		if(StringUtils.isEmpty(ldapGrupos)) {
			return false;
		}
		String[] split = ldapGrupos.split(",");
		if(split == null || split.length == 0) {
			return false;
		}
		
		
		for (String s : split) {
			String upperCase = StringUtils.upperCase(s.trim());
			grupoOrigem = StringUtils.upperCase(grupoOrigem.trim());
			if(grupoOrigem.equals(upperCase)) {
				return true;
			}
		}
		
		return false;
	}

	private Long createExpediente(String codExpediente) {
		
		if(StringUtils.isNotEmpty(codExpediente)) {
			String inicioString = StringUtils.substringBefore(codExpediente, "_");
			String fimString = StringUtils.substringAfterLast(codExpediente, "_");
			
			int inicio = Semana.from(inicioString).getOrdinal();
			int fim = Semana.from(fimString).getOrdinal();
			
			Expediente expediente = new Expediente();
			expediente.setCodigo(codExpediente);
			
			for(int i = inicio; i <= fim; i++) {
				Semana dia = Semana.values()[i];
				switch(dia) {
				case SEG:
					expediente.setSegunda(true);
					break;
				case TER:
					expediente.setTerca(true);
					break;
				case QUA:
					expediente.setQuarta(true);
					break;
				case QUI:
					expediente.setQuinta(true);
					break;
				case SEX:
					expediente.setSexta(true);				
					break;
				case SAB:
					expediente.setSabado(true);
					break;
				case DOM:
					expediente.setDomingo(true);
					break;
				}
			}
			
			try {
				expedienteService.saveOrUpdate(expediente);
			} catch (DomainException e) {
				e.printStackTrace();
			}
			
			return expediente.getId();
			
		}
		
		return null;
		
	}

	private Long getValueLongId(String[] split, HashMap<String, Long> map, String key) {
		Long id = null;
		if (split != null) {
			String s = getValue(split, key);
			if("N/D".equals(s)) {
				return null;
			}
			if (map.containsKey(s)) {
				id = map.get(s);
			} else {
				log.debug(key + " não encontrada");
			}
		}
		return id;
	}

	private Date getValueDate(String[] split, String key) {
		String s = getValue(split, key);
		if("N/D".equals(s)) {
			return null;
		}
		String pattern = ParametrosMap.getInstance(empresaId).get(Params.IMPORTACAO_ARQUIVO_DATE_PATTERN, "dd/MM/yyyy");
		return s != null && StringUtils.isNotEmpty(s) && isDateValid(s, pattern) ? DateUtils.toDate(s, pattern) : null;
	}

	private String getValue(String[] split, String key) {
		String s = null;
		if (split != null) {
			if (mapHeader.containsKey(key)) {
				s = JobImportacaoUtil.get(split, mapHeader.get(key));
			}
			if("N/D".equals(s)) {
				return "";
			}
			return s;
		}
		return "";
	}

	public static boolean isDateValid(String date, String pattern) {
		try {
			DateFormat df = new SimpleDateFormat(pattern);
			df.setLenient(false);
			df.parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	private void log(String string) {
		JobInfo.getInstance(empresaId).lastMessageImportacao = string;
		log.debug(string);
	}

	@Override
	public ImportarArquivoResponse getResponse() {
		return response;
	}

	private PreparedStatement createStatementInsert(Dialect dialect, Connection conn) throws SQLException {

		if (dialect instanceof OracleDialect) {
			return conn.prepareStatement(
					"insert into usuario (id,login,nome,email,ddd_telefone,telefone_fixo,ddd_celular,telefone_celular,data_nasc,funcao_id,area_id,diretoria_id,permissao_id,grupo_id, empresa_id, expediente_id, hora_abertura, hora_fechamento, flags, senha, genero, origem_cadastro, perfil_ativo) VALUES(USUARIO_SEQ.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1)",
					new String[] { "id" });
		}

		return conn.prepareStatement(
				"insert into usuario (login,nome,email,ddd_telefone,telefone_fixo,ddd_celular,telefone_celular,data_nasc,funcao_id,area_id,diretoria_id,permissao_id,grupo_id, empresa_id, expediente_id, hora_abertura, hora_fechamento, flags, senha, genero, origem_cadastro, perfil_ativo) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1)",
				Statement.RETURN_GENERATED_KEYS);

	}

	private PreparedStatement createStatementInsertGrupoUsuario(Dialect dialect, Connection conn) throws SQLException {

		if (dialect instanceof OracleDialect) {
			return conn.prepareStatement(
					"insert into grupo_usuarios (id,usuario_id,grupo_id,postar) VALUES(GRUPOUSUARIOS_SEQ.nextval,?,?,1)",
					new String[] { "id" });
		}

		return conn.prepareStatement("insert into grupo_usuarios (usuario_id,grupo_id,postar) VALUES(?,?,1)",
				Statement.RETURN_GENERATED_KEYS);

	}

}
