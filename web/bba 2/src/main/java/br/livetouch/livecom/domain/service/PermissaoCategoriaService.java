package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.PermissaoCategoria;
import net.livetouch.tiger.ddd.DomainException;

public interface PermissaoCategoriaService extends Service<PermissaoCategoria> {

	List<PermissaoCategoria> findAll(Empresa e);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(PermissaoCategoria p, Empresa e) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(PermissaoCategoria p) throws DomainException;
	
	PermissaoCategoria findByNomeValid(PermissaoCategoria p, Empresa empresa);

	PermissaoCategoria findByCodigoValid(PermissaoCategoria p, Empresa empresa);

}