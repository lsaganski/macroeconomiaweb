package br.livetouch.livecom.domain.repository;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.ConversaNotification;

@Repository
public interface ConversaNotificationRepository extends net.livetouch.tiger.ddd.repository.Repository<ConversaNotification> {

	ConversaNotification findByConversationUser(Long conversationId, Long userId);

}