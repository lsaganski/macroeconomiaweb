package br.livetouch.livecom.web.pages.ws;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.ComentarioVO;
import br.livetouch.livecom.domain.vo.FileVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class EsqueciSenhaPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private UsuarioLoginField tLogin;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tLogin = new UsuarioLoginField("login","login",true , usuarioService, getEmpresa()));
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		form.add(new Submit("Enviar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		try{
			if (form.isValid()) {
				Usuario u = tLogin.getEntity();
				if(u == null) {
					return new MensagemResult("NOK", "Login não encontrado");
				}
				
				if(u.getEmpresa() == null || u.getEmpresa() != getEmpresa()) {
					return new MensagemResult("NOK", "Login não encontrado");
				}
				
				if(u.isLoginLivecom()) {
					usuarioService.recuperarSenha(u.getEmpresa(), getUsuario(), u);
					return MensagemResult.ok("A nova senha foi enviada para o e-mail");
				} else {
					ParametrosMap map = getParametrosMap();
					String msg = map.get(Params.LOGIN_CONNECTOR_RECUPERAR_SENHA, "Entre em contato com a equipe de TI e solicite uma nova senha.");
					if(StringUtils.isEmpty(msg)) {
						msg = "Entre em contato com a equipe de TI e solicite uma nova senha.";
					}
					return new MensagemResult("NOK", msg);
				}
				
			}
		} catch (Exception e) {
			form.setError(e.getMessage());
			return new MensagemResult("NOK", "Erro ao criar nova senha.");
		}
		return new MensagemResult("NOK", "Erro ao criar nova senha.");

	}

	@Override
	protected void xstream(XStream x) {
		x.alias("comentario", ComentarioVO.class);
		x.alias("arquivo", FileVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
