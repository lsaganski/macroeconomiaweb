package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Favorito;
import br.livetouch.livecom.domain.Likes;
import br.livetouch.livecom.domain.Mensagem;

public class NotificacaoVO implements Serializable {
	private static final long serialVersionUID = 1L;

	public Long id;

	public String tipo;
	public String nomeUsuario;
	public Long idUsuario;
	public String loginUsuario;
	public String urlFotoUsuario;
	public String urlFotoUsuarioThumb;
	public String msg;
	public String data;
	public Long timestamp;
	public String titulo;
	public Long postId;
	public Long conversaId;
	private Long badge;

	public void setLikes(Likes f) {
		this.id = f.getId();
		this.tipo = "like";
		this.idUsuario = f.getUsuario().getId();
		this.loginUsuario = f.getUsuario().getLogin();
		this.nomeUsuario = f.getUsuario().getNome();
		this.urlFotoUsuario = f.getUsuario().getUrlFoto();
		this.urlFotoUsuarioThumb = f.getUsuario().getUrlThumb();
		this.postId = f.getPost().getId();
		this.titulo = f.getPost().getTitulo();
		this.msg = f.getPost().getMensagem();
		this.data = f.getDataStringHojeOntem();
		if(f.getData() != null) {
			this.timestamp = f.getData().getTime();
		}
	}

	public void setFavorito(Favorito f) {
		this.id = f.getId();
		this.tipo = "favorito";
		this.idUsuario = f.getUsuario().getId();
		this.loginUsuario = f.getUsuario().getLogin();
		this.nomeUsuario = f.getUsuario().getNome();
		this.urlFotoUsuario = f.getUsuario().getUrlFoto();
		this.urlFotoUsuarioThumb = f.getUsuario().getUrlThumb();
		this.postId = f.getPost().getId();
		this.titulo = f.getPost().getTitulo();
		this.msg = f.getPost().getMensagem();
		this.data = f.getDataStringHojeOntem();
		if(f.getData() != null) {
			this.timestamp = f.getData().getTime();
		}
	}
	
	public void setComentario(Comentario f) {
		this.id = f.getId();
		this.tipo = "comentario";
		this.idUsuario = f.getUsuario().getId();
		this.loginUsuario = f.getUsuario().getLogin();
		this.nomeUsuario = f.getUsuario().getNome();
		this.urlFotoUsuario = f.getUsuario().getUrlFoto();
		this.urlFotoUsuarioThumb = f.getUsuario().getUrlThumb();
		this.msg = f.getComentario();
		this.postId = f.getPost().getId();
		this.titulo = f.getPost().getTitulo();
		this.data = f.getDataStringHojeOntem();
		if(f.getData() != null) {
			this.timestamp = f.getData().getTime();
		}
	}

	public void setMensagem(Mensagem f) {
		this.id = f.getId();
		this.conversaId = f.getConversa().getId();
		this.tipo = "mensagem";
		this.idUsuario = f.getFrom().getId();
		this.loginUsuario = f.getFrom().getLogin();
		this.nomeUsuario = f.getFrom().getNome();
		this.urlFotoUsuario = f.getFrom().getUrlFoto();
		this.urlFotoUsuarioThumb = f.getFrom().getUrlThumb();
		this.msg = f.getMsg();
		this.postId = null;
		this.titulo = f.getMsg();
		this.data = f.getDataCreatedStringHojeOntem();
		if(f.getDataCreated() != null) {
			this.timestamp = f.getDataCreated().getTime();
		}
	}

	@Override
	public String toString() {
		return "NotificacaoVO [id=" + id + ", tipo=" + tipo + ", nomeUsuario=" + nomeUsuario + ", urlFotoUsuario=" + urlFotoUsuario + ", msg=" + msg + ", data=" + data + "]";
	}

	public Long getId() {
		return id;
	}

	public String getTipo() {
		return tipo;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public String getUrlFotoUsuario() {
		return urlFotoUsuario;
	}

	public String getMsg() {
		return msg;
	}

	public String getData() {
		return data;
	}

	public String getTitulo() {
		return titulo;
	}

	public Long getPostId() {
		return postId;
	}

	public Long getConversaId() {
		return conversaId;
	}
	
	public Long getTimestamp() {
		return timestamp;
	}

	public String getTituloDesc() {
		if(StringUtils.isNotEmpty(titulo)) {
			return titulo;
		}
		if(StringUtils.isNotEmpty(msg)) {
			if(msg.length() > 40) {
				return msg.substring(0,40)+"...";
			}
			return msg;
		}
		return null;
	}

	public Long getBadge() {
		return badge;
	}

	public void setBadge(Long badge) {
		this.badge = badge;
	}
}
