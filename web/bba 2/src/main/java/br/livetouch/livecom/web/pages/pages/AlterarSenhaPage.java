package br.livetouch.livecom.web.pages.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.AlterarSenhaVO;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.PasswordField;
import net.sf.click.control.Submit;



/**
 * Alterar senha do usuario logado
 * 
 * @author ricardo
 *
 */
@Controller
@Scope("prototype")
public class AlterarSenhaPage extends LivecomLogadoPage {

	public Form form = new Form();

	public String msg;

	protected Submit btAlterar;

	@Override
	public void onInit() {
		super.onInit();
		
		bootstrap_on = true;
		
		PasswordField t = new PasswordField("senhaAtual", getMessage("senha.atual.label"), true);
		t.setAttribute("class", "input");
		t.setFocus(true);
		form.add(t);

		t = new PasswordField("senhaNova", getMessage("senha.nova.label"), true);
		t.setAttribute("class", "input");
		form.add(t);

		t = new PasswordField("confirmaSenha", getMessage("senha.confima.label"), true);
		t.setAttribute("class", "input");
		form.add(t);

		btAlterar = new Submit("alterar", getMessage("alterar.label"), this, "alterar");
		btAlterar.setAttribute("class", "btn btn-primary min-width hidden");
		form.add(btAlterar);
	}

	public boolean alterar() {
		try {
			if(form.isValid()) {
				Usuario usuario = getUsuario();

				AlterarSenhaVO alterar = new AlterarSenhaVO();
				form.copyTo(alterar);

				usuarioService.alterarSenha(usuario, alterar);

				msg = getMessage("msg.senha.atualizada.success");
				
				setFlashAttribute("msg", getMessage("msg.senha.atualizada.success"));
				setRedirect(getClass());
			} else {
				msg = error(form);
			}
		} catch (DomainException e) {
			form.setError(getMessage(e.getMessage()));
		}

		return true;
	}

}
