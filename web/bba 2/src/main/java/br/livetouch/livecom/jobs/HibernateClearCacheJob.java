package br.livetouch.livecom.jobs;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.infra.util.Log;
import br.infra.util.Utils;
import net.livetouch.hibernate.HibernateUtil;

public class HibernateClearCacheJob implements Job {

	protected static final Logger log = Log.getLogger(HibernateClearCacheJob.class);

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		log("Memoria Total: " + Utils.sizeToHumanReadable(Runtime.getRuntime().totalMemory()) + ", Free: " + Utils.sizeToHumanReadable(Runtime.getRuntime().freeMemory()) + ", Utilizada: " + Utils.sizeToHumanReadable((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())));
		
		HibernateUtil.clearCache();
		
		log("HibernateClearCacheJob clear Hibernate cache.");
		log("Memoria Total: " + Utils.sizeToHumanReadable(Runtime.getRuntime().totalMemory()) + ", Free: " + Utils.sizeToHumanReadable(Runtime.getRuntime().freeMemory()) + ", Utilizada: " + Utils.sizeToHumanReadable((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())));
	}

	private void log(String s) {
		log.debug(s);
	}
}
	