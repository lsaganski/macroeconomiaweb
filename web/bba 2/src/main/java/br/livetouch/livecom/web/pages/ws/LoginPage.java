package br.livetouch.livecom.web.pages.ws;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.util.Log;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.LoginReport;
import br.livetouch.livecom.domain.Menu;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.PostDestaque;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.exception.HorarioGrupoException;
import br.livetouch.livecom.domain.exception.HorarioUsuarioException;
import br.livetouch.livecom.domain.exception.SenhaExpiradaException;
import br.livetouch.livecom.domain.exception.SenhaInvalidaException;
import br.livetouch.livecom.domain.vo.CampoUsersVO;
import br.livetouch.livecom.domain.vo.CamposMap;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.LivecomConfigVO;
import br.livetouch.livecom.domain.vo.MenuMobileVO;
import br.livetouch.livecom.domain.vo.NotificationBadge;
import br.livetouch.livecom.domain.vo.ParametroVO;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.livecom.domain.vo.ProfileVO;
import br.livetouch.livecom.domain.vo.TagVO;
import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.domain.vo.UserInfoSession;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.jobs.RegisterToPushJob;
import br.livetouch.livecom.security.LivecomSecurity;
import br.livetouch.livecom.security.UserAgentUtil;
import br.livetouch.livecom.utils.LoginCount;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.FieldSet;
import net.sf.click.control.Form;
import net.sf.click.control.PasswordField;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;

/**
 * Erro de senha
 * <p>
 * {"mensagem": {
 * "status": "ERRO",
 * "mensagem": "Senha inválida.",
 * "errorCode": "senha"
 * }}
 * <p>
 * Erro horario
 * <p>
 * {"mensagem": {
 * "status": "ERRO",
 * "mensagem": "O sistema não é acessível fora do seu horário de trabalho.",
 * "errorCode": "horario"
 * }}
 * <p>
 * Validação de versao
 * <p>
 * "mensagem": {
 * "status": "ERRO",
 * "mensagem": "favor atualizar",
 * "link": "https://ww2.itau.com.br/hotsites/itau/apprevista/",
 * "errorCode": "versaoInformativo"
 * },
 */
@Controller
@Scope("prototype")
public class LoginPage extends WebServiceXmlJsonPage {

    private static Logger logger = Log.getLogger(LoginPage.class);

    public Form form = new Form();
    public String user;
    private TextField tUser;
    private PasswordField tPassword;
    private TextField tDeviceRegId;
    private TextField tDeviceIMEI;
    private TextField tDeviceNome;
    private TextField tDeviceSO;
    private TextField tMode;
    private TextField tDeviceSOVersion;
    private TextField tAppVersion;
    private IntegerField tWsVersion;
    private IntegerField tAppVersionCode;
    private TextField tTokenVoip;

    public int notifications;

    public CamposMap campos;

    private Usuario usuario;

    @Override
    public void onInit() {
        super.onInit();

        campos = getCampos();

        form.setMethod("post");

        FieldSet set = new FieldSet("Login");
        form.add(set);
        set.add(tUser = new TextField("user", false));
        set.add(tPassword = new PasswordField("password", false));
        set.add(tDeviceRegId = new TextField("device.registration_id"));
        set.add(tDeviceIMEI = new TextField("device.imei"));
        set.add(tDeviceNome = new TextField("device.name"));
        set.add(tDeviceSO = new TextField("device.so"));
        set.add(tDeviceSOVersion = new TextField("device.so_version"));
        set.add(tAppVersion = new TextField("app.version"));
        set.add(tAppVersionCode = new IntegerField("app.version_code"));
        set.add(tTokenVoip = new TextField("token_voip"));
        set.add(tWsVersion = new IntegerField("wsVersion"));
        set.add(tMode = new TextField("mode"));

        set = new FieldSet("Mural");
        form.add(set);
        set.add(new IntegerField("notifications"));
        set.add(new IntegerField("page"));
        set.add(new IntegerField("maxRows"));

        tMode.setValue("json");

        tUser.setFocus(true);

        form.add(new Submit("login"));

        setFormTextWidth(form);
    }

    @Override
    protected Object execute() throws Exception {
        if (form.isValid()) {
            String login = tUser.getValue();
            String password = tPassword.getValue();
            
            /**
             * Login Fake no BBA.
             * Os apps vão enviar login/senha vazios, e por isso no form do Click estes campos estão como
             * não obrigatórios.
             * No root está cadastrado o usuário root.
             */
            if(StringUtils.isEmpty(login) && StringUtils.isEmpty(password)) {
            	login = ParametrosMap.getInstance().get("user.fake.login","root@livetouch.com.br");
            	password = ParametrosMap.getInstance().get("user.fake.password","senharoot123");
            }
            
            String deviceRegId = tDeviceRegId.getValue();
            String deviceIMEI = tDeviceIMEI.getValue();
            String deviceNome = tDeviceNome.getValue();
            String deviceSO = tDeviceSO.getValue();
            String deviceSOVersion = tDeviceSOVersion.getValue();
            String appVersion = tAppVersion.getValue();
            String tokenVoip = tTokenVoip.getValue();
            Integer appVersionCode = tAppVersionCode.getInteger();
            if (StringUtils.isNotEmpty(tWsVersion.getValue()) && StringUtils.isNumeric(tWsVersion.getValue())) {
                wsVersion = tWsVersion.getInteger();
            }


            if (StringUtils.isEmpty(deviceSOVersion)) {
                if (StringUtils.isNotEmpty(getParam("device.so.version"))) {
                    deviceSOVersion = getParam("device.so_version");
                } else {
                    deviceSOVersion = "1.0";
                }
            }

            if (StringUtils.isEmpty(deviceSO)) {
            	deviceSO = UserAgentUtil.getUserAgentSO(getContext());
            }

            log("Livecom login [" + login + "]");
            log("Livecom password [**********]");
            log("Livecom host [" + getHost() + "]");
            log("Livecom empresa [" + getEmpresa() + "]");
            log("Livecom deviceKey [" + deviceRegId + "]");
            log("Livecom deviceSO [" + deviceSO + "]");
            log("Livecom AppVersion [" + appVersion + "]");
            log("Livecom Token Voip [" + tokenVoip + "]");

            if (StringUtils.isEmpty(login)) {
                return new MensagemResult("NOK", "Informe o login.");
            }

            LoginReport report = new LoginReport();
            try {
                report.setValues(deviceSO);
                report.setLogin(login);

                UsuarioVO userVO = doLogin(login, password, deviceIMEI, deviceRegId, deviceNome, deviceSO, deviceSOVersion, appVersion, appVersionCode, tokenVoip);
                Usuario u = usuarioService.get(userVO.id);

                if (u != null) {
                    MensagemResult mensagemResult = validateVersion(u, appVersion, appVersionCode);
                    userVO.setControleVersao(mensagemResult);
                }

                if (!u.isPerfilAtivo()) {
                    throw new DomainMessageException("Perfil inativo");
                }

                if (notifications == 1) {
                    NotificationBadge badges = notificationService.findBadges(u);
                    userVO.setBadges(badges);
                }

                report.setUsuario(u);
                report.setStatus(true);
                report.setEmpresa(u.getEmpresa());

//				if(true) {
//					Response r = Response.ok("OK");
//					r.user = userVO;
//					
//					r.errorCode="horario";
//					r.mensagem="fora do horario eheh";
//					return r;
//				}

                if(getParametrosMap().getBoolean(Params.RESUMIR_LOGIN_BBA, true)) {
                	userVO.resumirBBA();
                }
                
                if (isWsVersion3()) {
                    Response r = Response.ok("OK");
                    r.user = userVO;
                    return r;
                }

                return userVO;
            } catch (DomainMessageException e) {
                // Error
                report.setErro(e.getMessage());
                report.setStatus(false);
                LoginCount.getInstance().increment(login);
                logError(e.getMessage(), e);
                return MensagemResult.erro("Erro no login: " + e.getMessage());
            }catch (SenhaExpiradaException e) {
                // Error
                report.setErro(e.getMessage());
                report.setStatus(false);
                logError(e.getMessage(), e);
                return MensagemResult.erro(e.getMessage(), "senhaExpirada");
            }
            catch (HorarioGrupoException e) {
                // Error
                report.setErro(e.getMessage());
                report.setStatus(false);
                LoginCount.getInstance().increment(login);
                logError(e.getMessage(), e);
                return MensagemResult.erro(e.getMessage(), "horario");
            } catch (HorarioUsuarioException e) {
                // Error
                report.setErro(e.getMessage());
                report.setStatus(false);
                LoginCount.getInstance().increment(login);
                logError(e.getMessage(), e);
                return MensagemResult.erro(e.getMessage(), "horario");
            } catch (SenhaInvalidaException e) {
                // Error
                report.setErro(e.getMessage());
                report.setStatus(false);
                LoginCount.getInstance().increment(login);
                logError(e.getMessage(), e);
                return MensagemResult.erro(e.getMessage(), e.getErrorCode());
            } catch (Exception e) {
                // Error
                report.setErro(e.getMessage());
                report.setStatus(false);
                LoginCount.getInstance().increment(login);
                logError(e.getMessage(), e);
                return MensagemResult.erro("NOK", "Erro no login.");
            } catch (Error e) {
                // Error
                report.setErro(e.getMessage());
                report.setStatus(false);
                logError(e.getMessage(), e);
                return MensagemResult.erro("NOK", e.getMessage());
            } finally {
                loginReportService.saveOrUpdate(report);

            }
        }

        return new MensagemResult("NOK", "Usuario nao encontrado");
    }

    private MensagemResult validateVersion(Usuario usuario, String appVersion, Integer appVersionCode) throws DomainMessageException {
        Long empresaId = usuario.getEmpresa().getId();
        log("Validando Versao Usuario " + usuario.getLogin() + ", empresa: " + usuario.getEmpresa());
        log("User-Agent: " + UserAgentUtil.getUserAgentSO(getContext()));

        ParametrosMap parametrosMap = ParametrosMap.getInstance(empresaId);

        String isAtivado = parametrosMap.get(Params.VERSION_CONTROL_ON, "0");
        if (!"1".equals(isAtivado)) {
            return null;
        }

        MensagemResult r = null;

        // Android
        if (UserAgentUtil.isAndroid(getContext())) {
            String link = parametrosMap.get(Params.VERSION_CONTROL_ANDROID_URLDOWNLOAD, "https://play.google.com/apps/testing/br.livetouch.livecom.livetouch");
            String mensagem = parametrosMap.get(Params.VERSION_CONTROL_ANDROID_MENSAGEM, "Existe uma nova versão do aplicativo, baixar?");
            boolean isInformativo = "informativo".equals(parametrosMap.get(Params.VERSION_CONTROL_ANDROID_MODE));
            boolean isImpeditivo = "impeditivo".equals(parametrosMap.get(Params.VERSION_CONTROL_ANDROID_MODE));
            String paramsNumberVersion = parametrosMap.get(Params.VERSION_CONTROL_ANDROID_NUMBER, "");
            int paramsNumberCode = NumberUtils.toInt(parametrosMap.get(Params.VERSION_CONTROL_ANDROID_NUMBER_CODE, "0"), 0);

            log(String.format("Validando Android: %s %s %s %s %s %s", isInformativo, isImpeditivo, paramsNumberVersion, paramsNumberCode, appVersion, appVersionCode));

            r = validateVersion(appVersion, appVersionCode, link, mensagem, isInformativo, isImpeditivo, paramsNumberVersion, paramsNumberCode);

        } else if (UserAgentUtil.isIos(getContext())) {
            String link = parametrosMap.get(Params.VERSION_CONTROL_IOS_URLDOWNLOAD, "http://livecom.livetouchdev.com.br/download");
            String mensagem = parametrosMap.get(Params.VERSION_CONTROL_IOS_MENSAGEM, "Existe uma nova versão do aplicativo, baixar?");
            boolean isInformativo = "informativo".equals(parametrosMap.get(Params.VERSION_CONTROL_IOS_MODE));
            boolean isImpeditivo = "impeditivo".equals(parametrosMap.get(Params.VERSION_CONTROL_IOS_MODE));
            String paramsNumberVersion = parametrosMap.get(Params.VERSION_CONTROL_IOS_NUMBER, "");
            int paramsNumberCode = NumberUtils.toInt(parametrosMap.get(Params.VERSION_CONTROL_IOS_NUMBER_CODE, "0"), 0);
            ;

            log(String.format("Validando iOS: %s %s %s %s %s", isInformativo, isImpeditivo, paramsNumberVersion, paramsNumberCode, appVersion));

            r = validateVersion(appVersion, appVersionCode, link, mensagem, isInformativo, isImpeditivo, paramsNumberVersion, paramsNumberCode);

        }
        return r;
    }

    private MensagemResult validateVersion(String appVersion, Integer appVersionCode, String link, String mensagem, boolean isInformativo, boolean isImpeditivo, String paramsNumberVersion,
                                           int paramsNumberCode) {
        MensagemResult mensagemResult = null;

        boolean hasCorrectVersion = true;

        /**
         * Valida pelo version name: ex: 0.0.1
         */
        if (StringUtils.isNotEmpty(paramsNumberVersion)) {
            paramsNumberVersion = paramsNumberVersion.replaceAll("\\.", "");

            if (!StringUtils.isEmpty(appVersion)) {
                appVersion = appVersion.replaceAll("\\.", "");

                if (!appVersion.equals(paramsNumberVersion)) {
                    log("Versao incorreta: " + appVersion + " Desejada : " + paramsNumberVersion);
                    hasCorrectVersion = false;
                }
            }
        }

        /**
         * Valida pelo version Code, ex: 1
         */
        if (paramsNumberCode > 0 && appVersionCode != null) {
            if (appVersionCode == 0 || appVersionCode != paramsNumberCode) {
                hasCorrectVersion = false;
                log("Versao Code incorreta: " + appVersionCode + " Desejada : " + paramsNumberCode);
            }
        }

        if (!hasCorrectVersion) {

            if (isImpeditivo) {
                mensagemResult = MensagemResult.erro(mensagem, "impeditivo");
                mensagemResult.link = link;
            } else if (isInformativo) {
                mensagemResult = MensagemResult.erro(mensagem, "informativo");
                mensagemResult.link = link;
            }
        }

        return mensagemResult;
    }

    protected Usuario getUsuarioRequest() {
        if (usuario != null) {
            return usuario;
        }
        return super.getUsuarioRequest();
    }

    protected UsuarioVO doLogin(String login, String password, String deviceIMEI, String deviceRegId, String deviceNome, String deviceSO, String deviceSOVersion, String appVersion, Integer appVersionCode, String tokenVoip) throws DomainException {

        Empresa empresa = getEmpresa();
        Long empresaId = getEmpresaId();

        log(">> LoginPage doLogin user: " + login);
        log(">> LoginPage doLogin empresa: " + empresa);
        boolean isMobile = true;
        Usuario u = usuarioService.login(login, password, empresa, isMobile);
        log("<< LoginPage: " + u);
        if (u != null) {
        	LivecomSecurity.checkExpiredPasswordByInactivity(u, empresaId);
        	LivecomSecurity.checkExpiredPasswordByPeriodicity(u, empresaId, usuarioService);
            if (!u.getAtivo()) {
                u.setStatus(StatusUsuario.ENVIADO_CONVITE_JA_LOGOU_MOBILE);
            }

            if (StringUtils.isEmpty(u.getChave()) || u.getChave() == null) {
                usuarioService.gerarChave(u);
            }

            if (StringUtils.isNotEmpty(deviceRegId)) {
                u.setRegistrationId(deviceRegId);
            }

            if (StringUtils.isNotEmpty(appVersion)) {
                u.setDeviceAppVersion(appVersion);
            }

            if (appVersionCode != null) {
                u.setDeviceVersionCode(String.valueOf(appVersionCode));
            }

            u.setDataLastLogin(new Date());
            usuarioService.saveOrUpdate(getUsuario(), u);

            UsuarioVO userVO = new UsuarioVO();
            userVO.setUsuario(u, usuarioService, grupoService);


            LivecomConfigVO livecomConfig = createLivecomConfig(empresa);
            userVO.livecomConfig = livecomConfig;

            ParametrosMap parametrosMap = getParametrosMap();
            String cor = parametrosMap.get(Params.COR_MENU, "#0096d0");
            userVO.setCorHeader(cor);

            List<CategoriaPost> menuCategorias = categoriaPostService.findMenuCategoria(empresa);
            if (menuCategorias != null && !menuCategorias.isEmpty()) {
                userVO.setMenuCategorias(menuCategorias);
            }

            String pushServerHost = ParametrosMap.getInstance(empresaId).get(Params.PUSH_SERVER_HOST, "");
            boolean pushServerOn = StringUtils.isNotEmpty(pushServerHost);

            if (pushServerOn) {
                if (StringUtils.isNotEmpty(deviceRegId)) {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put(RegisterToPushJob.EMPRESA_KEY, empresaId);
                    params.put(RegisterToPushJob.LOGIN_KEY, login);
                    params.put(RegisterToPushJob.IMEI_KEY, deviceIMEI);
                    params.put(RegisterToPushJob.REG_ID_KEY, deviceRegId);
                    params.put(RegisterToPushJob.DEVICE_NOME_KEY, deviceNome);
                    params.put(RegisterToPushJob.DEVICE_SO_KEY, deviceSO);
                    params.put(RegisterToPushJob.DEVICE_SO_VERSION_KEY, deviceSOVersion);
                    params.put(RegisterToPushJob.APP_VERSION_KEY, appVersion);
                    params.put(RegisterToPushJob.TOKEN_VOIP, tokenVoip);
                    // No job os params sao tratos como string.
                    params.put(RegisterToPushJob.APP_VERSION_CODE_KEY, String.valueOf(appVersionCode));
                    executeJob(RegisterToPushJob.class, "RegisterToPushJob_" + login, params);
                }
            } else {
                log("PushServer off, não registrando device login [" + login + "].");
            }

            if (StringUtils.isEmpty(deviceSO)) {
                deviceSO = deviceNome;
            }

            // User Info
            UserInfoVO userInfo = UserInfoVO.create(u);
            UserInfoSession session = userInfo.addSession(deviceSO);
            Livecom.getInstance().add(userInfo);

            validaHorario(userInfo);

            // Token: Json de retorno
            userVO.setDataLogin(session.getDataLoginChatDate());

            //Save or Update token
            LivecomSecurity.updateToken(deviceSO, u, session, tokenService);
            userVO.setWstoken(session.wstoken);

            // Profile
            String buildType = getContext().getRequest().getParameter("buildType");
            ProfileVO p = ProfileVO.create(u, usuarioService, buildType, campos);
            userVO.profile = p;

            log("Login: " + login);

            // OK
            LoginCount.getInstance().clear(login);

            this.usuario = u;

            return userVO;
        }

        throw new DomainMessageException("Usuário não encontrado.");
    }

	private LivecomConfigVO createLivecomConfig(Empresa empresa) throws DomainException {
        empresaService.setParametrosDefaultEmpresa(empresa);

        List<Menu> menu = menuService.findAllMobile(getEmpresa());
        List<Parametro> parametrosMobile = parametroService.findParametrosMobile(empresa);
        List<ParametroVO> parametrosVO = ParametroVO.toList(parametrosMobile);


        LivecomConfigVO livecomConfigVO = new LivecomConfigVO();
		livecomConfigVO.parametros = parametrosVO;
		livecomConfigVO.menuMobile = MenuMobileVO.createByList(menu);
		return livecomConfigVO;
	}

	@Deprecated
    private Long checkBlockLogin(String login) throws DomainException {
        Long empresaId = getEmpresaId();
        ParametrosMap parametrosMap = ParametrosMap.getInstance(empresaId);
        boolean configLoginOn = Boolean.valueOf(parametrosMap.get(Params.CONFIG_LOGIN_ON, "1"));
        if(configLoginOn){
	        int qtdeErros = LoginCount.getInstance().get(login);
	        int limiteCountLogin = Integer.valueOf(parametrosMap.get(Params.CONFIG_LOGIN_QTDEMAXERROSLOGIN, "-1"));
	        long timeBloqueioErroLogin = Integer.valueOf(parametrosMap.get(Params.CONFIG_LOGIN_TEMPOBLOQUEIOERROLOGIN, "0"));

	        if (qtdeErros == limiteCountLogin) {

	            long tempo = LoginCount.getInstance().isBloqueado(login);

	            if (tempo < timeBloqueioErroLogin) {
	                long diferenca = timeBloqueioErroLogin - tempo;
	                throw new DomainMessageException("Bloqueio de login por excesso de tentativas, tente novamente daqui a " + diferenca + " minuto(s)");
	            } else {
	                LoginCount.getInstance().clear(login);
	            }
	        }
        }
        return empresaId;
    }

    @Override
    protected void xstream(XStream x) {
        x.alias("user", UsuarioVO.class);
        x.alias("grupo", GrupoVO.class);

        x.alias("post", PostVO.class);
        x.alias("destaque", PostDestaque.class);
        x.alias("arquivo", FileVO.class);
        x.alias("tags", TagVO.class);
        x.alias("thumb", ThumbVO.class);
        x.alias("permissao", Perfil.class);

        x.alias("campo", CampoUsersVO.class);
        x.alias("mensagem", MensagemResult.class);

        super.xstream(x);
    }

    @Override
    protected String getCryptKey() {
        return user;
    }

    @Override
    protected boolean isWsTokenOn() {
        return false;
    }

    @Override
    protected boolean isWsCryptOn() {
    	return false;
    }

    @Override
    public boolean isSaveLogTransacao() {
        return true;
    }

    protected void log(String string) {
        String host = getHost();
        String s = host + ": " + string;
        logger.debug(s);
        System.out.println(s);
    }

    protected void logError(Object message, Throwable t) {
        String host = getHost();
        logger.error(host + ": " + message, t);
    }

    protected void logError(Object message) {
        String host = getHost();
        logger.error(host + ": " + message);
    }
}
