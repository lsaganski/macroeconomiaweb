package br.livetouch.livecom.web.pages.admin;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoFiltro;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
import net.livetouch.extras.util.DateUtils;

@Controller
@Scope("prototype")
public class RelatorioVisualizacaoPage extends RelatorioPage {

	public RelatorioVisualizacaoFiltro filtro;
	public int page;
	public String action;
	
	@Override
	public void onInit() {
		super.onInit();

		if (clear) {
			getContext().removeSessionAttribute(RelatorioVisualizacaoFiltro.SESSION_FILTRO_KEY);
			filtro = new RelatorioVisualizacaoFiltro();
			filtro.setDataInicialString(DateUtils.toString(br.livetouch.livecom.utils.DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
			filtro.setDataFinalString(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		} else {
			filtro = (RelatorioVisualizacaoFiltro) getContext().getSessionAttribute(RelatorioVisualizacaoFiltro.SESSION_FILTRO_KEY);
			if (filtro != null) {
				if (filtro.getUsuarioId() != null) {
					Usuario usuario = usuarioService.get(filtro.getUsuarioId());
					if (usuario != null) {
						filtro.setUsuario(usuario);
					}
				}
				if (filtro.getPostId() != null) {
					Post post = postService.get(filtro.getPostId());
					if (post != null) {
						filtro.setPost(post);
					}
				}
				if (filtro.getCategoriaId() != null) {
					CategoriaPost categoria = categoriaPostService.get(filtro.getCategoriaId());
					if (categoria != null) {
						filtro.setCategoria(categoria);
					}
				}
			}
		}
	}

	@Override
	public void onPost() {
		super.onPost();
		if ("1".equals(action)) {
			String csv = reportService.csvVisualizacao(filtro);
			download(csv, "relatorio-visualizacao.csv");
		}
	}
}
