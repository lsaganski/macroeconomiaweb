package br.livetouch.livecom.web.pages.root;

import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;

import br.infra.util.LongOperation.LongOperationRunner;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.LivecomRootPage;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;

/**
 * 
 */
@Controller
@Scope("prototype")
public class ConfirmarDeletarEmpresaPage extends LivecomRootPage {

	public Long id;
	public Link deleteLink = new Link("deletar", this, "deletar");
	public Empresa empresa;
	
	@Override
	public void onGet() {
		super.onGet();
		if(id != null) {
			// ler empresa ao entrar na página
			empresa = empresaService.get(id);
		}
	}
	
	public boolean deletar() {
		Long id = deleteLink.getValueLong();
		if(empresa == null) {
			empresa = empresaService.get(id);
		}
		if(empresa == null) {
			setFlashAttribute("msg", "Desculpe empresa não localizada");
		} else {
			final String cod = "delete_empresa_"+empresa.getId();
			executeLongOperation(cod, new LongOperationRunner() {
				@Override
				public void run(Usuario userInfo) throws Exception {
					try {
						empresaService.delete(userInfo,empresa);
					} catch (DataIntegrityViolationException e) {
						throw new DomainException("Os relacionamentos deta empresa não puderam ser excluídos. " + e.getRootCause().getMessage());
					} catch (Exception e) {
						throw new DomainException("Desculpe não possível excluir está empresa: " + e.getMessage());
					}
				}
			});
		}
//		setRedirect(EmpresasPage.class);
		return true;
	}
	
}
