package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.LinhaImportacao;
import br.livetouch.livecom.domain.vo.LinhaImportacaoVO;
import net.livetouch.tiger.ddd.DomainException;

public interface LinhaImportacaoService extends Service<LinhaImportacao> {

	List<LinhaImportacao> findAll();
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(LinhaImportacao f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(LinhaImportacao f) throws DomainException;

	LinhaImportacaoVO findAllVOByLogId(Long id);
	
	List<LinhaImportacao> findAllByLogId(Long id);
	
	List<LinhaImportacao> findAllByIdLog(Long id);
}
