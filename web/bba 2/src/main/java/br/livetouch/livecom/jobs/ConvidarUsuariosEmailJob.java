package br.livetouch.livecom.jobs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.enums.TipoLogSistema;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.sender.Sender;
import br.livetouch.livecom.sender.SenderConvidar;
import br.livetouch.livecom.sender.SenderFactory;
import br.livetouch.livecom.web.pages.admin.ConvidarUsuariosPage;
import br.livetouch.pushserver.lib.PushNotification;
import br.livetouch.pushserver.lib.UsuarioToPush;
import net.livetouch.extras.util.ExceptionUtil;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ConvidarUsuariosEmailJob extends SpringJob {

	protected static final Logger log = Log.getLogger(ConvidarUsuariosPage.class);

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	protected LogService logService;

	@Autowired
	protected EmpresaService empresaService;


	@SuppressWarnings("unchecked")
	@Override
	protected void execute(Map<String, Object> params) throws Exception {
		log.debug("ConvidarUsuariosEmailJob.execute()");

		int count = 0;

		if (params != null) {
			List<Long> usuarios_ids = (List<Long>) params.get("usuarios_ids");
			TemplateEmail template = (TemplateEmail) params.get("template");
			Empresa empresa = (Empresa) params.get("empresa");
			Usuario userInfo = (Usuario) params.get("userInfo");
			String assunto = (String) params.get("assunto");
		
			
			if(StringUtils.isEmpty(assunto)) {
				log.debug("E-mail esta indo sem assunto");
			}
			
			if (usuarios_ids == null || usuarios_ids.size() == 0 ) {
				log.debug("USUARIO ERROR: Nenhum usuário informado para o envio de convite");
				return;
			}
			
			if(template == null || StringUtils.isEmpty(template.getHtml())) {
				log.debug("TEMPLATE ERROR: Nenhum template informado para o envio de convite ou o template esta sem conteudo");
				return;
			}
			
			if(empresa == null) {
				log.debug("EMPRESA ERROR: Nenhum template informado para o envio de convite");
				return;
			}
			
			log.debug("enviarEmailParaUsuarios(): " + usuarios_ids);
			List<Usuario> usuarios = usuarioService.findAllByIds(usuarios_ids);
			sendEmail(empresa, userInfo, usuarios, assunto, template.getHtml());
			
		}
		log.debug("ConvidarUsuariosEmailJob, push_count: " + count);
	}

	private int sendEmail(Empresa empresa, Usuario userInfo, List<Usuario> usuarios, String assunto, String template) {
		int count = 0;

		List<UsuarioToPush> usuariosToSend = new ArrayList<>();

		for (Usuario u : usuarios) {
			try {
				count++;
				log.debug("Convidando user: " + u.getId() + " > " + u.getEmail());
				String senha = null;
				try {
					senha = usuarioService.createPassword(u);
				} catch (DomainException e1) {
					log.debug("Erro ao gerar senha do usuário " + e1.getMessage(), e1);
					continue;
				}

				if(empresa == null) {
					empresa = u.getEmpresa();
				}
				
				Map<String, Object> params = new HashMap<>();
				params.put("senha", senha);
				params.put("login", u.getLogin());
				if (empresa != null) {
					params.put("dominio", empresa.getDominio());
					params.put("nomeEmpresa", empresa.getNome());
					params.put("trialCode", empresa.getTrialCode());
				}
				params.put("urlBuildAndroid", Livecom.getUrlBuildAndroid(empresa));
				params.put("urlBuildIOS", Livecom.getUrlBuilIOS(empresa));

				UsuarioToPush usuarioToPush = new UsuarioToPush();
				usuarioToPush.setEmail(u.getEmail());
				usuarioToPush.setCodigo(u.getLogin());
				usuarioToPush.setNome(u.getNome());
				usuarioToPush.setIdLivecom(u.getId());
				usuarioToPush.setParams(params);
				usuariosToSend.add(usuarioToPush);

				log.debug("User convidado com sucesso: " + u.getId() + " > " + u.getEmail() + ". Alterando status para ENVIADO_CONVITE");
				u.setStatus(StatusUsuario.ENVIADO_CONVITE);
				usuarioService.saveOrUpdate(userInfo, u);
			} catch (Exception e) {
				LogSistema logInfo = LogSistema.logError(userInfo, "Exception ao convidar usuário: " + u.getId() + " > " + u.getEmail());
				logInfo.setException(ExceptionUtil.getStackTrace(e));
				logInfo.setUsuario(u);
				logInfo.setTipo(TipoLogSistema.Convite);
				logService.saveOrUpdate(logInfo, empresa);
			}
		}
		
		if(usuariosToSend.size() > 0) {
			PushNotification not = new PushNotification();
			not.setUsuarios(usuariosToSend);
			not.setTitulo(assunto);
			not.setTemplate(template);
			not.setIsEmail(true);
			not.setIsPush(false);
			Sender sender = SenderFactory.getSender(applicationContext, SenderConvidar.class);
			sender.send(empresa, not);
		}
		return count;
	}
}
