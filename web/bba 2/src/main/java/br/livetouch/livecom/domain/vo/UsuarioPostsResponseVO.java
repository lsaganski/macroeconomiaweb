package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.List;

public class UsuarioPostsResponseVO implements Serializable {
	private static final long serialVersionUID = 1L;

	public NotificationBadge badges;
	public List<UsuarioVO> users;
	public List<PostVO> posts;
	public List<GrupoUsuariosCountVO> grupos;
	public ControleVersaoVO controleVersao;

	public List<PostVO> getPosts() {
		return posts;
	}

	public List<UsuarioVO> getUsers() {
		return users;
	}

	public List<GrupoUsuariosCountVO> getGrupos() {
		return grupos;
	}
}
