package br.livetouch.livecom.domain.enums;

public enum TipoPush {
	newPost("newPost"),
	like("like"),
	comentario("comentario"),
	favorito("favorito"), 
	mensagem("mensagem"),
	email("email"), 
	push("push"),
	silent("silent");
	
	private String label;
	
	TipoPush(String label) {
		this.label = label;
	}
	
	@Override
	public String toString() {
		return this.label != null ? this.label : "";
	}
	
}
