package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.ConversaNotification;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.BadgeChatVO;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.LocationVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.NotificationChatVO;
import br.livetouch.livecom.domain.vo.StatusMensagemUsuarioVO;
import br.livetouch.livecom.domain.vo.ThumbVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ConversasPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private UsuarioLoginField tUser;
	private TextField tMode;

	public int maxRows;
	public int page;
	public String search;
	private TextField tVersao;

	/**
	 * TODO Zerar msgs nao entregues de callback (ex: check2cinza)
	 */
	public int reload;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		form.add(tUser = new UsuarioLoginField("user_id", true, usuarioService, getEmpresa()));
		form.add(new IntegerField("page"));
		form.add(new IntegerField("maxRows"));
		form.add(new TextField("search"));

		form.add(tVersao = new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(new TextField("reload"));

		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		tUser.setFocus(true);

		form.add(new Submit("Enviar"));

		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario user = tUser.getEntity();
			
			if(user == null) {
				return new MensagemResult("NOK", "Usuário não encontrado");
			}

			// Conversas
			List<MensagemConversa> conversas = mensagemService.findAllConversasByUser(user, search, page, maxRows);

			MensagemConversa.sortByDatUpdatedLastFirst(conversas);

			List<MensagemVO> msgsVO = new ArrayList<MensagemVO>();
			for (MensagemConversa c : conversas) {
				// Last msg
				MensagemVO vo = new MensagemVO();
				Mensagem mensagem = c.getLastMensagem();
				mensagem.setLida(mensagem, c, user, statusMensagemUsuarioService);
				vo.setMensagem(mensagem, user);
				
				ConversaNotification notifications = conversaNotificationService.findByConversationUser(c.getId(), user.getId());
				vo.setNotifications(new NotificationChatVO(notifications));
				msgsVO.add(vo);
				
				// badge
				BadgeChatVO b = new BadgeChatVO();
				Long badges = mensagemService.getBadge(c, user);
				b.mensagens = badges;
				vo.setBadge(b);
			}

			Response r = Response.ok("OK");
			r.mensagens = msgsVO;
			return r;
		}

		return new MensagemResult("NOK", "Erro ao listar mensagens: " + getFormErrorMensagemResult(form));
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("conversa", ConversaVO.class);
		x.alias("msg", MensagemVO.class);
		x.alias("arquivo", FileVO.class);
		x.alias("statusMensagemUsuario", StatusMensagemUsuarioVO.class);
		x.alias("thumb", ThumbVO.class);
		x.alias("loc", LocationVO.class);
		super.xstream(x);
	}

	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
