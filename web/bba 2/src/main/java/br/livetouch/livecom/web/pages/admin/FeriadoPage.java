package br.livetouch.livecom.web.pages.admin;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.click.control.Form;
import net.sf.click.control.Submit;


@Controller
@Scope("prototype")
public class FeriadoPage extends LivecomAdminPage {

	public Long id;
	public Form form = new Form();
	
	@Override
	public void onInit() {
		super.onInit();

		form();
	}

	public void form(){
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip botao btIcone marginFloat");
		form.add(tExportar);
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();
	}
	public boolean exportar(){
		String csv = feriadoService.csvFeriado(getEmpresa());
		download(csv, "feriados.csv");
		return true;
	}
}
