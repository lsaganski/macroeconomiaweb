package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Palavra;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.repository.PalavraRepository;
import br.livetouch.livecom.domain.service.PalavraService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class PalavraServiceImpl implements PalavraService {
	@Autowired
	private PalavraRepository rep;

	@Override
	public Palavra get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Palavra c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Palavra> findAll() {
		return rep.findAll();
	}

	@Override
	public void delete(Palavra c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public List<String> censurar(Post p) {
		List<String> msg = null;
		if (p != null) {
			List<Palavra> palavras = findAll();
			msg = new ArrayList<String>();
			for (Palavra palavra : palavras) {
				if (StringUtils.contains(p.getMensagem(), palavra.getPalavra())) {
					msg.add(palavra.getPalavra());
					continue;
				}
				if (StringUtils.contains(p.getTitulo(), palavra.getPalavra())) {
					msg.add(palavra.getPalavra());
					continue;
				}
			}
		}
		return msg;
	}

	@Override
	public boolean isCensurado(String msg) {
		if (StringUtils.isNotEmpty(msg)) {
			List<Palavra> palavras = findAll();
			for (Palavra palavra : palavras) {
				if (StringUtils.contains(msg, palavra.getPalavra())) {
					return true;
				}
			}
			return false;
		}
		return false;
	}
}
