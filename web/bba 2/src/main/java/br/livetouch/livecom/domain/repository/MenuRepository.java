package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Menu;
import br.livetouch.livecom.domain.Perfil;
import net.livetouch.tiger.ddd.repository.Repository;

public interface MenuRepository extends Repository<Menu> {

	List<Menu> findAll(Empresa empresa);

	List<Menu> findAllDefault();

	List<Menu> findByLabel(String label, Empresa e);
	
	List<Menu> findPaiByLabel(String label, Empresa e);

	List<Menu> findByPerfil(Perfil p, Empresa e);

	List<Menu> findMenusNotInPerfil(Perfil p, Empresa e);

	List<Menu> findPaiByPerfil(Perfil perfil, Empresa empresa, boolean in);

	List<Menu> findFilhoByPerfil(Perfil perfil, Empresa empresa, boolean in);
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	void delete(Menu c);

	List<Menu> findFilhos(Menu m);

	List<Menu> paisByPerfil(Perfil p, Empresa e);

	List<Menu> filhosByPerfil(Perfil p, Empresa e);

	@Transactional(rollbackFor=Exception.class)
	void deleteFromEmpresa(Empresa empresa);

	List<Menu> findAllMobile(Empresa empresa);

	List<Menu> findMenuMobileDefault();

	void delete(Empresa empresa);

    void removeDefaultMenuMobile(Empresa empresa);
}