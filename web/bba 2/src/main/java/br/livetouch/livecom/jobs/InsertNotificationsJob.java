package br.livetouch.livecom.jobs;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.vo.NotificationJobVO;
import br.livetouch.livecom.domain.vo.UserToPushVO;
import br.livetouch.livecom.jobs.info.NotificationInfo;
import br.livetouch.livecom.utils.DateUtils;

/**
 * V2: Varre a tabela de Notification para enviar Pushs.
 * 
 * @author rlech
 *
 */
@Service
public class InsertNotificationsJob extends SpringJob {

	protected static final Logger log = Log.getLogger(InsertNotificationsJob.class);
	
	public static Queue<Map<String, Object>> fila = new ConcurrentLinkedQueue<>();

	@Autowired
	protected NotificationService notificationService;
	
	@Autowired
	protected WorkDispatcherService dispatcher;
	
	@Autowired
	protected SessionFactory sessionFactory;
	
	@Autowired
	protected PostService postService;

	private static boolean running = false;

	@Override
	protected void execute(Map<String, Object> params) throws Exception {
		if (running) {
			log("PushJob already running.");
			return;
		}
		
		running = true;
		
		try {
			
			List<NotificationJobVO> notifications = NotificationInfo.getInstance().getListAndDelete();
			if (notifications != null && notifications.size() > 0) {
				
				for (NotificationJobVO vo : notifications) {
					
					Notification n = vo.notification;
					
					if(n == null) {
						log("Notificalção não informada, abortando job.");
						running = false;
						return;
					}
					
					Usuario from = n.getUsuario();
					ParametrosMap parametrosMap = from != null ? ParametrosMap.getInstance(from.getEmpresa()) : ParametrosMap.getInstance();
					String sendToAll = parametrosMap.get(Params.PUSH_SEND_TOALL, "0");

					Post p = vo.post;
					if(p == null) {
						log("Post não informado, abortando job.");
						running = false;
						return;
					}
					
					TipoNotificacao tipo = n.getTipo();
					if(tipo.equals(TipoNotificacao.REMINDER)) {
						if(DateUtils.isMenor(new Date(), p.getReminder(), "dd/MM/yyyy HH:mm")) {
							NotificationInfo.getInstance().addNotification(vo);
							continue;
						}
					}

					Usuario userPost = vo.usuario;
					if(userPost == null) {
						log("Autor do post não informado, abortando job.");
						running = false;
						return;
					}
					if("1".equals(sendToAll)) {
						log("Enviando para todo mundo registrado no PUSH push.send.toAll ativado");
						boolean sendPush = vo.isSendPush;
						n.setSendPush(sendPush);
						if (sendPush) {
							n.setDataPush(null);
						}
						notificationService.save(n);
					} else {
						// Remove o autor do post.
						
						List<Long> usersToPush;
						boolean sendPush = vo.isSendPush;
						
						if(tipo.equals(TipoNotificacao.REMINDER)) {
							usersToPush = postService.getUsuariosReminder(p, n.getIdioma());
							sendPush = false;
							n.setDataPush(new Date());
						} else {
							usersToPush = postService.getUsuariosPushPost(p, n.getIdioma());
						}
						

						// Verifica se o autor do post deve receber push
						if (!vo.sendPushAutor && !tipo.equals(TipoNotificacao.REMINDER)) {
							// Remove a notification parent
							// O autor é dono da notification parent. As demais são filhas da
							// parent.
							usersToPush.remove(userPost.getId());
						}

						// Salva todas as notifications: InsertNotificationsWork
						if(usersToPush.size() > 0) {
							n.setSendPush(sendPush);
							if (sendPush) {
								n.setDataPush(null);
							}
							List<UserToPushVO> listUsers = UserToPushVO.toListUsers(usersToPush);
							
							dispatcher.insertNotificationUsuariosWork(n, listUsers, p, true);
						} else {
							log("Nenhum usuario informado para gerar notificação, abortando job.");
							running = false;
							return;
						}
					}
				}
				log("Teminou o JOB de PUSH");
			}
		} finally {
			running = false;
		}
	}

	private void log(String s) {
		log.debug(s);
	}
	
	public static void add(Map<String, Object> map) {
		fila.add(map);
	}
}
