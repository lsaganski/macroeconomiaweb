package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Formacao;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

public interface DomainService extends Service<Usuario> {

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Formacao f,Usuario userInfo) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Formacao f) throws DomainException;

	Formacao getFormacao(Long id);

	List<Formacao> findAllFormacao(Usuario userInfo);
}
