package br.livetouch.livecom.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.gson.Gson;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.livetouch.livecom.domain.vo.StatusPostVO;
import net.livetouch.tiger.ddd.DomainException;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StatusPost extends JsonEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1424214675602321861L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "STATUSPOST_SEQ")
	private Long id;

	private String nome;
	private String codigo;
	private String cor;

	@OneToMany(mappedBy = "statusPost", fetch = FetchType.LAZY)
	@OrderBy(value = "id")
	@XStreamOmitField
	private Set<Post> posts;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;
	
	public StatusPost() {
		super();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}
	
	@Override
	public String toJson() {
		StatusPostVO vo = new StatusPostVO(this);
		return new Gson().toJson(vo);
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public static void isFormValid(StatusPost status, boolean isUpdate) throws DomainException {
		if(status == null) {
			throw new DomainException("Parametros inválidos");
		}
		
		if(StringUtils.isEmpty(status.nome)) {
			throw new DomainException("Campo nome não pode ser vazio");
		}
		
		if(isUpdate && status.getId() == null) {
			throw new DomainException("StatusPost não localizado");
		}
		
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

}
