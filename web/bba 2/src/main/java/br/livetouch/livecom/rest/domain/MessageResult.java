package br.livetouch.livecom.rest.domain;

import java.io.Serializable;

public class MessageResult implements Serializable {
	private static final long serialVersionUID = 8637905716979758426L;
	public String status;
	public String message;
	public Object entity;

	public static MessageResult ok(String message) {
		MessageResult r = new MessageResult();
		r.status = "OK";
		r.message = message;
		return r;
	}
	
	public static MessageResult ok(String message, Object entity) {
		MessageResult r = new MessageResult();
		r.status = "OK";
		r.message = message;
		r.entity = entity;
		return r;
	}
	
	public static MessageResult ok(Object entity) {
		MessageResult r = new MessageResult();
		r.status = "OK";
		r.entity = entity;
		return r;
	}
	
	public static MessageResult error(String message) {
		MessageResult r = new MessageResult();
		r.status = "ERROR";
		r.message = message;
		return r;
	}
}
