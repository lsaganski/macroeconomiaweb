package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Agenda;
import br.livetouch.livecom.domain.repository.AgendaRepository;
import br.livetouch.livecom.domain.service.AgendaService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class AgendaServiceImpl implements AgendaService {
	@Autowired
	private AgendaRepository rep;

	@Override
	public Agenda get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Agenda c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Agenda> findAll() {
		return rep.findAll();
	}

	@Override
	public void delete(Agenda c) throws DomainException {
		rep.delete(c);
	}
}
