package br.livetouch.livecom.web.pages.admin;

import net.sf.click.control.Field;
import net.sf.click.control.Form;
import net.sf.click.control.HiddenField;

/**
 * @author Ricardo Lecheta
 *
 */
public class FormNaoValidaPost extends Form {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7499777412279299013L;

	/**
	 * @param edicaoUsuarioPage
	 */
	public FormNaoValidaPost() {

	}

	@Override
	public void onInit() {
		super.onInit();

		HiddenField tTipoHidden = new HiddenField("tipoHidden", ""){
			private static final long serialVersionUID = -2018294750961746931L;

			@Override
			public String getValue() {
				return "";
			}
		};
		add(tTipoHidden);
	}

	@Override
	public void validate() {
		super.validate();

	}

	@Override
	public boolean onProcess() {
		boolean b = super.onProcess();

		// Nao valida se o hidden=1
		String tipoHidden = getContext().getRequestParameter("tipoHidden");
		if("1".equals(tipoHidden)) {
			setError(null);

			if (isFormSubmission()) {
				for (int i = 0, size = getFieldList().size(); i < size; i++) {
					Field f = (Field) getFieldList().get(i);
					f.setError(null);
				}
			}
		}

		return b;
	}
}