package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.EmailContato;
import br.livetouch.livecom.domain.repository.EmailContatoRepository;
import br.livetouch.livecom.domain.service.EmailContatoService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class EmailContatoServiceImpl implements EmailContatoService {
	@Autowired
	private EmailContatoRepository rep;

	@Override
	public EmailContato get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(EmailContato c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<EmailContato> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void delete(EmailContato c) throws DomainException {
		rep.delete(c);
	}
}
