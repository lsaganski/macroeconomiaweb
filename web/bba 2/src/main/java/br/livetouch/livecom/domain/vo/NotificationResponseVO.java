package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NotificationResponseVO implements Serializable {
	private static final long serialVersionUID = 1L;

	public NotificationBadge badges = new NotificationBadge();
	public List<NotificationVO> list = new ArrayList<NotificationVO>();
	public List<NotificationVO> solicitacoes = new ArrayList<NotificationVO>();
	public Boolean loginLivecomOk;

	@Override
	public String toString() {
		return "NotificationResponseVO [count=" + badges + ", list=" + list + "]";
	}
}
