package br.livetouch.livecom.chatAkka;

import br.livetouch.livecom.utils.FileExtensionUtils;

public class ChatFile {
	private String base64;
	private String nome;
	private String tipo;

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "ChatFile [nome=" + nome + ", tipo=" + tipo + "]";
	}

	public String getExtensao() {
		return FileExtensionUtils.getExtensao(nome);
	}
}
