package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.PerfilPermissao;
import net.livetouch.tiger.ddd.repository.Repository;

public interface PerfilRepository extends Repository<Perfil> {
	
	void saveOrUpdate(PerfilPermissao p);

	Perfil findByNome(String nome, Empresa e);

	List<Object[]> findIdNomePerfil();
	
	List<Perfil> findAll(Empresa empresa);
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	void delete(Perfil c);

	Perfil findByNomeValid(Perfil login, Empresa e);

	List<Perfil> findDefaults();

	void removePermissoes(Perfil perfil);

}