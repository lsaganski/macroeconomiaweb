package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;

import br.livetouch.livecom.utils.DateUtils;

public class RelatorioAcessosVersaoVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2353343218707587848L;
	
	private Long logados;
	private String versao;
	private String login;
	private String deviceSo;
	private String deviceSoVersion;
	private String data;
	
	public RelatorioAcessosVersaoVO(String deviceSo, String versao, Long logados) {
		this.setLogados(logados);
		this.setVersao(versao);
		this.setDeviceSo(deviceSo);
	}
	
	public RelatorioAcessosVersaoVO(String versao, String login) {
		this.setVersao(versao);
		this.setLogin(login);
	}
	public RelatorioAcessosVersaoVO(Date data, String deviceSo, String deviveSoVersion,String versao, String login) {
		this.setVersao(versao);
		this.setLogin(login);
		this.setDeviceSo(deviceSo);
		this.setDeviceSoVersion(deviveSoVersion);
		this.data = DateUtils.toString(data, "dd/MM/yyyy HH:mm");
	}

	public Long getLogados() {
		return logados;
	}

	public void setLogados(Long logados) {
		this.logados = logados;
	}

	public String getVersao() {
		return versao;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getDeviceSo() {
		return deviceSo;
	}

	public void setDeviceSo(String deviceSo) {
		this.deviceSo = deviceSo;
	}

	public String getDeviceSoVersion() {
		return deviceSoVersion;
	}

	public void setDeviceSoVersion(String deviceSoVersion) {
		this.deviceSoVersion = deviceSoVersion;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
