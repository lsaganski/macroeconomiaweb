package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Grupo;

public class GrupoUsuariosVO {

	private final Grupo grupo;
	private final Long countUsers;

	public GrupoUsuariosVO(Grupo grupo,Long countUsers) {
		this.grupo = grupo;
		this.countUsers = countUsers;
	}
	
	public Long getCountUsers() {
		return countUsers;
	}
	
	public Grupo getGrupo() {
		return grupo;
	}

	@Override
	public String toString() {
		return "GrupoUsuarios [grupo=" + grupo.getNome() + ", countUsers=" + countUsers + "]";
	}

	public static List<GrupoUsuariosVO> fixListEmpty(List<GrupoUsuariosVO> list) {
		List<GrupoUsuariosVO> newList = new ArrayList<GrupoUsuariosVO>();
		for (GrupoUsuariosVO gu : list) {
			if(gu.getGrupo() != null && gu.getCountUsers() > 0) {
				newList.add(gu);
			}
		}
		return newList;
	}
	
	
}
