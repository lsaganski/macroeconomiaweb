package br.livetouch.livecom.rest.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Permissao;
import br.livetouch.livecom.domain.vo.PermissaoCategoriaVO;
import br.livetouch.livecom.domain.vo.PermissaoVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/permissao")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class PermissaoResource extends MainResource {
	protected static final Logger log = Log.getLogger(PermissaoResource.class);
	
	@POST
	public Response create(Permissao permissao) {
		
		//Validação
		Response messageResult = existe(permissao);
		if(messageResult != null){
			return messageResult;
		}
		
		try {
			permissaoService.saveOrUpdate(permissao, permissao.getEmpresa());
			PermissaoVO permissaoVO = new PermissaoVO();
			permissaoVO.setPermissao(permissao);
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", permissaoVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel realizar o cadastro da permissão " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel realizar o cadastro da permissão ")).build();
		}
	}
	
	@PUT
	public Response update(Permissao permissao) {
		
		if(permissao == null || permissao.getId() == null) {
			return Response.ok(MessageResult.error("Permissão inexistente")).build();
		}
		Response messageResult = existe(permissao);
		if(messageResult != null){
			return messageResult;
		}
		
		try {
			permissaoService.saveOrUpdate(permissao, permissao.getEmpresa());
			PermissaoVO permissaoVO = new PermissaoVO();
			permissaoVO.setPermissao(permissao);
			return Response.ok(MessageResult.ok("Permissão atualiza com sucesso com sucesso", permissaoVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel atualizar o cadastro da permissão " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar o cadastro da permissão ")).build();
		}
	}
	
	@GET
	public Response findAll() {
		
		Long empresaId = getUserInfoVO().getEmpresaId();
		Empresa empresa = empresaService.get(empresaId);
		List<Permissao> permissoes = permissaoService.findAll(empresa);
		List<PermissaoVO> vos = PermissaoVO.fromList(permissoes);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/categoria")
	public Response findGroup() {
		
		List<Permissao> permissoes = permissaoService.findAll(getEmpresa());
		
		Map<String, List<PermissaoVO>> map = new HashMap<String, List<PermissaoVO>>();
		for (Permissao p : permissoes) {
			
			if(p != null && p.getCategoria() != null) {
				PermissaoVO permissao = new PermissaoVO(p);
				PermissaoCategoriaVO categoria = permissao.getCategoria();
				
				List<PermissaoVO> list = map.get(categoria.codigo);
				if(list == null) {
					list = new ArrayList<PermissaoVO>();
					list.add(permissao);
				} else {
					list.add(permissao);
				}
				map.put(categoria.codigo, list);
			}
		}
		
		return Response.ok().entity(map).build();
	}	
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		Permissao permissao = permissaoService.get(id);
		if(permissao == null) {
			return Response.ok(MessageResult.error("Permissão não encontrada")).build();
		}
		PermissaoVO permissaoVO = new PermissaoVO();
		permissaoVO.setPermissao(permissao);
		return Response.ok().entity(permissaoVO).build();
	}
	
	@GET
	@Path("/find")
	public Response findByNome(@QueryParam("nome") String nome) {
		List<Permissao> permissoes = permissaoService.findByNome(nome, getEmpresa());
		if(permissoes.isEmpty()) {
			return Response.ok(MessageResult.error("Permissões não encontradas")).build();
		}
		List<PermissaoVO> vos = PermissaoVO.fromList(permissoes);
		return Response.ok().entity(vos).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		Permissao permissao = permissaoService.get(id);
		if(permissao == null) {
			return Response.ok(MessageResult.error("Permissão não encontrada")).build();
		}
		try {
			permissaoService.delete(permissao, getUserInfo());
			return Response.ok().entity(MessageResult.ok("Permissão deletada com sucesso")).build();
		} catch (DomainException e) {
			log.error("Não foi possível excluir a permissão id: " + id + " ERRO:" + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir a Permissão  " + id)).build();
		}
	}
	
	
	public Response existe(Permissao permissao) {
		
		if(permissaoService.findByNomeValid(permissao, permissao.getEmpresa()) != null) {
			log.debug("Ja existe uma permissão com este nome");
			return Response.ok(MessageResult.error("Ja existe uma permissão com este nome")).build();
		}
		if(permissaoService.findByCodigoValid(permissao, getEmpresa()) != null){
			log.debug("Ja existe uma area com este codigo");
			return Response.ok(MessageResult.error("Ja existe uma area com este codigo")).build();
			
		}

		return null;
	}
	
}
