package br.livetouch.livecom.rest.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.repository.NotificationSearch;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.PerfilService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.vo.NotificationVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/reminder")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class ReminderResource extends MainResource{

	protected static final Logger log = Log.getLogger(ReminderResource.class);

	@Context
	private SecurityContext securityContext;

	@Autowired
	protected PostService postService;

	@Autowired
	PerfilService permissaoService;

	@Autowired
	protected GrupoService grupoService;

	@POST
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getReminders(NotificationSearch search) {
		
		List<NotificationVO> notificacoes = new ArrayList<NotificationVO>();
		search.tipo = TipoNotificacao.REMINDER.toString();
		
		List<NotificationUsuario> notifications = notificationService.findAll(search);
		if(notifications != null) {
			for (NotificationUsuario n : notifications) {
				NotificationVO vo = new NotificationVO();
				vo.copyFrom(n);
				notificacoes.add(vo);
			}
		}
		
		return Response.ok(MessageResult.ok("Reminders", notificacoes)).build();
		
	}
	
	@POST
	@Path("/read/{id}")
	public Response readReminder(@PathParam("id") Long id) throws DomainException {
		
		try {
			if(id == null) {
				log.debug("Não foi possivel ler o reminder");
				return Response.ok(MessageResult.error("Não foi possivel ler o reminder")).build();
			}
			
			NotificationUsuario nu = notificationService.getNotificationUsuario(id);
			
			if(nu == null) {
				log.debug("Não foi possivel ler o reminder");
				return Response.ok(MessageResult.error("Não foi possivel ler o reminder")).build();
			}

			Notification n = nu.getNotification();
			
			if(n == null) {
				log.debug("Não foi possivel ler o reminder");
				return Response.ok(MessageResult.error("Não foi possivel ler o reminder")).build();
			}
			
			Post p = n.getPost();
			
			if(p == null) {
				log.debug("Não foi possivel ler o reminder");
				return Response.ok(MessageResult.error("Não foi possivel ler o reminder")).build();
			}
			
			notificationService.delete(n);
			postService.saveOrUpdate(getUserInfo(), p);
			
			log.debug("Reminder lido com sucesso.");
			return Response.ok(MessageResult.ok("Reminder lido com sucesso.")).build();
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel ler o reminder")).build();
		}
	}

}