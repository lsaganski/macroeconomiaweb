package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.GrupoFilter;
import br.livetouch.livecom.domain.vo.GrupoVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class GruposPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	public boolean get;
	public List<GrupoVO> grupos;
	private UsuarioLoginField tUser;
	private LongField tGrupoId;
	
	@Override
	public void onInit() {
		form.setMethod("post");

		TextField tQ = new TextField("q");
		form.add(tQ);
		tQ.setFocus(true);
		form.add(tUser = new UsuarioLoginField("user_id","user_id", false, usuarioService, getEmpresa()));
		if(getUsuario() != null) {
			tUser.setValue(getUsuario().getLogin());
		}
		form.add(tGrupoId = new LongField("grupo_id"));
		form.add(new TextField("not_grupo_ids"));
		form.add(new TextField("pode_postar"));
		form.add(new TextField("meus_grupos"));
		form.add(new TextField("participo"));
		form.add(new TextField("admin_all","Se for admin, buscar todos"));
		form.add(new TextField("page"));
		form.add(new TextField("maxRows"));

		form.add(new TextField("tipo"));
		form.add(new TextField("fisico"));
		form.add(new TextField("grupo_cods"));

		form.add(new TextField("padrao"));

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		form.add(new Submit("buscar"));

//		setFormTextWidth(form);
	}

	@Override
	public void onGet() {
		super.onGet();
		
		get = true;
	}

	@Override
	protected boolean isHtml() {
		return super.isHtml();
	}
	
	@Override
	public String getTemplate() {
		return super.getTemplate();
	}
	
	@Override
	public String getContentType() {
		return super.getContentType();
	}

	@Override
	protected Object execute() throws Exception {
		String nome = getContext().getRequestParameter("q");
		String maxRowsParameter = getContext().getRequestParameter("maxRows");

		int maxRows = NumberUtils.toInt(maxRowsParameter);
		
		// Somente grupos deste usuario
		Usuario user = tUser.getEntity();
		if(user == null) {
			throw new RuntimeException("Informe o parâmetro user_id.");
		}

		GrupoFilter filter = new GrupoFilter();
		filter.nome = nome;
		filter.page = page;
		filter.maxRows = maxRows;
		filter.empresa = getEmpresa();

		filter.id = tGrupoId.getLong();
		
		// Meus Grupos
		filter.gruposQueCriei = "1".equals(getContext().getRequestParameter("meus_grupos"));
		// Grupos que participo
		filter.gruposQueParticipo = "1".equals(getContext().getRequestParameter("participo"));

		// tipo grupo
		filter.tipo = form.getFieldValue("tipo");

		// Ao postar, exclui grupos ids que já estão na lista
		filter.notIds = getListIds("not_grupo_ids");
		
		// Retorna somente grupos que o usuario pode ver.
		filter.podePostar = "1".equals(getContext().getRequestParameter("pode_postar"));
		
		// Verifica se os tipos de grupo está habilitado (novo)
		filter.tipoOn = getParametrosMap().getBoolean(Params.CADASTRO_GRUPO_TIPO_ON, false);

		// Flag para mostrar virtuais.
		filter.fisicos = "1".equals(getContext().getRequestParameter("fisico"));

		// Flag para mostrar somente grupos padrão.
		filter.padrao = "1".equals(getContext().getRequestParameter("padrao"));

		filter.cods = getListCods("grupo_cods");

		grupos = new ArrayList<GrupoVO>();

		Long count = 0L;
		if(!filter.tipoOn || hasPermissao(ParamsPermissao.VISUALIZAR_TODOS_OS_GRUPOS)) {
			count = grupoService.countAll(filter, user);
			grupos = grupoService.findAll(filter, user);
		} else {
			count = grupoService.countFindGrupos(filter, user);
			grupos = grupoService.findGrupos(filter, user);
		}
		
		if(isWsVersion3()) {
			Response r = Response.ok("OK");
			r.groups = grupos;
			r.count = count;
			return r;
		}

		return grupos;
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("user", Grupo.class);
		x.alias("user", GrupoVO.class);
		super.xstream(x);
	}
}
