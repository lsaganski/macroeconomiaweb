package br.livetouch.livecom.web.pages.ws;


import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.vo.UserInfoVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class NotificationOptionPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tFiltro;
	private TextField tMode;

	public Long id;
	
	@Override
	public void onInit() {
		super.onInit();

		form.add(tFiltro = new TextField("filtro"));
		tFiltro.setFocus(true);
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		form.add(new Submit("consultar"));

	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			
			UserInfoVO userInfo = getUserInfoVO();
			
			String option = tFiltro.getValue();
			if(StringUtils.isNotEmpty(option)) {
				if(userInfo != null) {
					userInfo.setNotificationOption(option);
					return new MensagemResult("OK","Opção da notificação alterada.");
				}
				else{
					return new MensagemResult("OK","Usuario não logado (null).");
				}
				
			}else{
				return new MensagemResult("NOK","Opção da notificação em branco.");
			}
		}
		return new MensagemResult("NOK","Opção da notificação não validada.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("result", MensagemResult.class);
		super.xstream(x);
	}
}
