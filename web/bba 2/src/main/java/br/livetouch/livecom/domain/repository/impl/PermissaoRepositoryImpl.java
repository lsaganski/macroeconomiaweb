package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Permissao;
import br.livetouch.livecom.domain.repository.PermissaoRepository;
import br.livetouch.livecom.domain.vo.PerfilPermissaoVO;

@Repository
public class PermissaoRepositoryImpl  extends LivecomRepository<Permissao> implements PermissaoRepository {

	public PermissaoRepositoryImpl() {
		super(Permissao.class);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Permissao> findAll(Empresa empresa) {
		StringBuffer sb = new StringBuffer("FROM Permissao p WHERE p.empresa.id = :empresaId or p.empresa is null");
		sb.append(" order by p.categoria, p.nome ");
		Query q = createQuery(sb.toString());
		q.setParameter("empresaId", empresa.getId());
		q.setCacheable(true);
		return q.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Permissao> findByNome(String nome, Empresa e) {
		StringBuffer sb = new StringBuffer("FROM Permissao p WHERE 1=1 ");
		
		if(StringUtils.isNotEmpty(nome)) {
			sb.append(" and p.nome like :nome or p.codigo like :nome");
		}
		 
		sb.append(" and p.empresa.id = :empresaId");
		
		Query q = createQuery(sb.toString());
		
		if(StringUtils.isNotEmpty(nome)) {
			q.setParameter("nome", "%" + nome + "%");
		}
		
		q.setParameter("empresaId", e.getId());
		q.setCacheable(true);
		
		List<Permissao> list = q.list();
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Permissao findByNomeValid(Permissao permissao, Empresa e) {
		
		StringBuffer sb = new StringBuffer("from Permissao p where p.nome = :nome");
		sb.append(" and p.empresa = :empresa ");
		
		if(permissao.getId() != null) {
			sb.append(" and p.id != :id");
		}

		Query q = createQuery(sb.toString());
		
		q.setParameter("nome", permissao.getNome());
		q.setParameter("empresa", e);
		
		if(permissao.getId() != null) {
			q.setParameter("id", permissao.getId());
		}
			
		List<Permissao> list = q.list();
		Permissao d = (Permissao) (list.size() > 0 ? q.list().get(0) : null);
		return d;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Permissao findByCodigoValid(Permissao permissao, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Permissao p where p.codigo = :codigo");
		sb.append(" and p.empresa = :empresa ");
		
		if(permissao.getId() != null) {
			sb.append(" and p.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("codigo", permissao.getCodigo());
		q.setParameter("empresa", empresa);
		
		if(permissao.getId() != null) {
			q.setParameter("id", permissao.getId());
		}
		
		List<Permissao> list = q.list();
		Permissao d = (Permissao) (list.size() > 0 ? q.list().get(0) : null);
		return d;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PerfilPermissaoVO> findByPerfil(Perfil perfil) {
		StringBuffer sb = new StringBuffer("select new br.livetouch.livecom.domain.vo.PerfilPermissaoVO(p, COALESCE(pp.ligado, false)) from Permissao p left join p.perfis pp ");
		
		if(perfil != null) {
			sb.append(" WITH (pp.perfil.id = :perfil) ");
			sb.append(" WHERE 1=1 ");
			sb.append(" and (p.empresa = :empresa or p.empresa is null) ");
		}

		sb.append(" order by p.categoria, p.nome ");

		Query q = createQuery(sb.toString());
		
		if(perfil != null) {
			q.setParameter("perfil", perfil.getId());
			q.setParameter("empresa", perfil.getEmpresa());
		}
		
		return q.list();
	}
	
	@Override
	public void delete(Permissao p) {
		//Remover Perfis associados a MenuPerfil
		execute("delete from PerfilPermissao pp where pp.permissao.id = ?)", p.getId());
		execute("delete from Permissao p where p.id = ?)", p.getId());
	}

}
