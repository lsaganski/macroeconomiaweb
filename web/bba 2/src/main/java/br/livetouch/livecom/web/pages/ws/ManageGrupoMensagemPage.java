package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ManageGrupoMensagemPage extends WebServiceFormPage {

	public LongField adminId;
	public LongField grupoId;
	public TextField user;
	public String action;

	@Override
	protected void form() {
		// user deve ser user_admin_id
		// E usuario podia ser user_id
		form.add(adminId = new LongField("user_admin_id", "admin", true));
		form.add(grupoId = new LongField("grupo_id", "grupo", true));
		form.add(user = new TextField("usuarios", "users", true));
		form.add(new TextField("action", true));
	}

	@Override
	protected Object go() throws Exception {
		if (form.isValid()) {
			try {
				Long userAdmin = adminId.getLong();
				Long grupo = grupoId.getLong();
				String usuario = user.getValue();

				//verificar permissão para adicionar e remover membros do grupo
				Usuario admin = null;
				if (userAdmin != null && userAdmin > 0) {
					admin = usuarioService.get(userAdmin);
					if (admin == null) {
						throw new DomainException("Admin do grupo não encontrado com o id [" + userAdmin + "]");
					}
				} 
				
				Grupo g = null;
				if (grupo != null && grupo > 0) {
					g = grupoService.get(grupo);
					if (g == null) {
						throw new DomainException("Grupo não encontrado com o id [" + grupo + "]");
					}
				}
				
				MensagemConversa conversa = mensagemService.findByGrupo(g);
				if(conversa == null) {
					throw new DomainException("Conversa não encontrado para grupo id [" + grupo + "]");
				}

				List<Usuario> list = new ArrayList<Usuario>();
				if (usuario != null && usuario.length() > 0) {
					String[] codigosUsuarios = usuario.split(",");
					for (String user : codigosUsuarios) {
						Usuario u = new Usuario();
						u = usuarioService.get(Long.valueOf(user));
						if (u == null) {
							throw new DomainException("Usuario não encontrado com o id [" + usuario + "]");
						}
						list.add(u);
					}
				}
				
				GrupoUsuarios gu = null;
				gu = grupoService.findByGrupoEUsuario(g, admin);
				if (gu == null) {
					throw new DomainException("Usuario [" + admin + "] não possui permissão para acessar o grupo");
				}
				
				boolean isAdminGrupo = g.isAdmin(admin);
				if (!isAdminGrupo) {
					throw new DomainException("Usuario [" + admin + "] não possui permissão para adicionar e/ou remover membros do grupo");
				}
				
				boolean isAdicionar = action.equalsIgnoreCase("adicionar");
				boolean isRemover = action.equalsIgnoreCase("remover");
				if (isAdicionar || isRemover) {
					for (Usuario u : list) {
						try {
							String msgAdmin = "";
							if(isRemover) {
								gu = grupoService.findByGrupoEUsuario(g, u);
								if (gu == null) {
									throw new DomainException("Usuario não encontrado no grupo [" + g.getNome() + ".");
								}
								usuarioService.deleteGrupoUsuarios(gu);
								msgAdmin = admin.getNome() + " removeu " +u.getNome();
								
								AkkaHelper.sendMessageDenied(u.getId(), conversa.getId(), g.getId());
							} else {
								gu = null;
								gu = grupoService.findByGrupoEUsuario(g, u);
								if (gu == null) {
									gu = usuarioService.addGrupoToUser(u, g, true);
								}
								if(gu == null) {
									throw new DomainException("Não foi possivel adicionar o usuario ao grupo [" + g.getNome() + "]. ");
								}
								msgAdmin = admin.getNome() + " adicionou " +u.getNome();
							}
							livecomChatInterface.saveMessageAdminAndSendAkka(admin, g, conversa, msgAdmin);
							log.debug("OK, Usuário adicionado no grupo");
						} catch (Exception e) {
							throw new DomainException("Não foi possivel adicionar o usuario ao grupo [" + g.getNome() + "]. " + e);
						}
						
					}
				} else {
					throw new DomainException("Solicitação [" + mode + "] não reconhecida");
				}
				if(isWsVersion3()) {
					Response r = Response.ok("OK");
					return r;
				}
				return new MensagemResult("OK", "Grupo atualizado");
			} catch(DomainException e) {
				if(isWsVersion3()) {
					Response r = Response.error(e.getMessage());
					return r;
				}
				return new MensagemResult("NOK", e.getMessage());
			}
		}

		if(isWsVersion3()) {
			Response r = Response.error("Preencha o formulario");
			return r;
		}
		return new MensagemResult("NOK", "Preencha o formulario");

	}

	

	@Override
	protected void xstream(XStream x) {
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
