package br.livetouch.livecom.domain.repository;


import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.UsuarioMarcado;

@Repository
public interface UsuarioMarcadoRepository extends net.livetouch.tiger.ddd.repository.Repository<UsuarioMarcado> {

	UsuarioMarcado find(Comentario comentario, Usuario usuario);

	void delete(List<Long> idsMarcados, Comentario c);

}