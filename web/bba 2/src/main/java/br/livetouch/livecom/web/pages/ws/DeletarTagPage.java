package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Tag;
import net.sf.click.control.Form;

/**
 * @author Juillian Lee
 * 
 */
@Controller
@Scope("prototype")
public class DeletarTagPage extends WebServiceXmlJsonPage {
	public Form form = new Form();
	
	public Long id;

	@Override
	public void onInit() {
		super.onInit();
	}

	@Override
	protected Object execute() throws Exception {
		if(id != null) {
			Tag tag = tagService.get(id);
			if(tag != null) {
				try {
					tagService.delete(getUsuario(), tag, false);
				}catch(Exception e) {
					return new MensagemResult("NOK", "Não foi possivel remover a Tag");
				}
				return new MensagemResult("OK", "Tag deletada com sucesso");
			}
			return new MensagemResult("NOK", "Tag não encontrada");
		}
		return new MensagemResult("NOK", "Tag não encontrada");
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
