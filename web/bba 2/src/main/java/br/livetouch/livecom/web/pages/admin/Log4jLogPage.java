package br.livetouch.livecom.web.pages.admin;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.InvalidParameterException;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;


@Controller
@Scope("prototype")
public class Log4jLogPage extends LivecomAdminPage {

	public String msg;
	public Form form = new Form();
	public TextField tNomeLog;
	public String now;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissoes(true, ParamsPermissao.VIZUALIZAR_RELATORIOS, ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();

		form.add(tNomeLog = new TextField("nome", getMessage("digite.nome.arquivo.label"), true));
		tNomeLog.setFocus(true);
		tNomeLog.addStyleClass("form-control input-sm");
		
		Submit visualizar = new Submit("visualizar", getMessage("visualizar.label"), this, "visualizar");
		visualizar.setAttribute("class", "btn blue");
		form.add(visualizar);
		
		Submit baixar = new Submit("baixar", getMessage("baixar.label"), this, "baixar");
		baixar.setAttribute("class", "btn green");
		form.add(baixar);
		
//		setFormTextWidth(form);
	}
	
	
	public boolean visualizar() {
		if(form.isValid()) {
			String file = tNomeLog.getValue();
			
			try {
				
				if(!file.contains("/log") || !file.contains("/logs")) {
					throw new InvalidParameterException(getMessage("msg.arquivo.invalido.error"));
				}
				
				String text = FileUtils.readFileToString(new File(file));
				
				HttpServletResponse response = getContext().getResponse();
				response.setContentType("text/plain; charset=UTF-8");
				response.setHeader("Pragma", "no-cache");
				
				text = text.replace("\r", "");
				text = text.replace("\n", "<br>");
				
				response.setContentType(getContentType());

				setPath(null);
				PrintWriter writer = response.getWriter();;
				writer.print(text);
				writer.flush();
				writer.close();
				
				return false;
			} catch (IOException e) {
				setMessage(getMessage("msg.arquivo.nao.encontrado") + ": " + e.getMessage() + " - " + e.getClass());
			}  catch (Exception e) {
				form.setError(e.getMessage());
			}
		}
		
		return true;
	}

	public boolean baixar() {
		if(form.isValid()) {
			String file = tNomeLog.getValue();
			try {
				
				if(!file.contains("/log") || !file.contains("/logs")) {
					throw new InvalidParameterException(getMessage("msg.arquivo.invalido.error"));
				}
				
				//byte[] bytes = FileUtils.readFileToByteArray(new File(file));
				String text = FileUtils.readFileToString(new File(file));

				HttpServletResponse response = getContext().getResponse();
				response.setHeader("Pragma", "no-cache");
				response.setContentType("application/text");  
	            response.setHeader("Content-Disposition","attachment;filename=" + file);  
				
				response.setContentType(getContentType());

				setPath(null);
				PrintWriter writer = response.getWriter();;
				writer.print(text);
				writer.flush();
				writer.close();
				
				return false;
				
			} catch (IOException e) {
				setMessage(getMessage("msg.arquivo.nao.encontrado") + ": " + e.getMessage() + " - " + e.getClass());
			} catch (Exception e) {
				form.setError(e.getMessage());
			}
		}
		return true;
	}
}
