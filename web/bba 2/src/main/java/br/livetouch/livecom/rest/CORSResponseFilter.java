package br.livetouch.livecom.rest;


import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;

public class CORSResponseFilter
implements ContainerResponseFilter {


	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		
		boolean CORS_ON = ParametrosMap.getInstance().getBoolean(Params.SECURITY_CORS_ON, true);
		String ORIGIN = ParametrosMap.getInstance().get(Params.SECURITY_CORS_ORIGIN, "*");
		
		if(CORS_ON) {
			MultivaluedMap<String, Object> headers = responseContext.getHeaders();
			
			headers.add("Access-Control-Allow-Origin", ORIGIN);
			headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");			
			headers.add("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Codingpedia, Authorization");
		}
	}

}