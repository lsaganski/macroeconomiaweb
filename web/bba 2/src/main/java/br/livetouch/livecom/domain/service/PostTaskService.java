package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostTask;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

public interface PostTaskService extends Service<PostTask> {

	List<PostTask> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(PostTask f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(PostTask f) throws DomainException;

	List<PostTask> findAllByUser(Usuario u);

	long getCount();

	PostTask findByUserPost(Usuario u, Post p);
	
	List<PostTask> findAllByPost(Post post);

}