package br.livetouch.livecom.web.pages.admin;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.Utils;
import br.infra.web.click.ComboTipoLogSistema;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.LogRepository;
import br.livetouch.livecom.domain.vo.LogSistemaFiltro;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.extras.util.ServletUtil;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.DateField;
import net.sf.click.extras.control.LinkDecorator;

/**
 * Verifica os logs de sistema 'informação' ou 'erros'
 * 
 */
@Controller
@Scope("prototype")
public class LogSistemaPage extends RelatorioPage {

	public PaginacaoTable table = new PaginacaoTable();
	public ActionLink detalhes = new ActionLink("detalhes", getMessage("detalhes.label"), this, "detalhes");
	public Form form = new Form();
	public int page;
	public ComboTipoLogSistema comboTipo;

	public LogSistemaFiltro filtro;

	@Override
	public void onInit() {
		super.onInit();
		
		bootstrap_on = true;
		escondeChat = true;

		if (clear) {
			getContext().removeSessionAttribute(LogSistemaFiltro.SESSION_FILTRO_KEY);
		}

		filtro = (LogSistemaFiltro) getContext().getSessionAttribute(LogSistemaFiltro.SESSION_FILTRO_KEY);
		if (filtro != null) {
			form.copyFrom(filtro);
		}

		table();

		form();
	}

	protected void table() {
		Column c = new Column("id", getMessage("coluna.id.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("dataString", getMessage("coluna.data.label"));
		c.setTextAlign("center");
		c.setAttribute("align", "center");
		table.addColumn(c);

		c = new Column("tipo", getMessage("coluna.tipo.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("usuario.login", getMessage("coluna.login.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("msg", getMessage("coluna.mensagem.label"));
		// limita o maximo de caracteres para 100 , e ainda coloca as
		// reticiencias ...
		c.setMaxLength(100);
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("detalhes", getMessage("coluna.detalhes.label"));
		c.setAttribute("align", "center");
		c.setDecorator(new LinkDecorator(table, detalhes, "id"));
		table.addColumn(c);
	}

	public void form() {

		DateField tDataInicio = new DateField("dataInicial", getMessage("periodoDe.label"), false);
		DateField tDataFim = new DateField("dataFinal", getMessage("ate.label"));
		tDataInicio.setAttribute("class", "input data datepicker");
		tDataFim.setAttribute("class", "input data datepicker");
		tDataInicio.setFormatPattern("dd/MM/yyyy");
		tDataFim.setFormatPattern("dd/MM/yyyy");

		tDataInicio.setValue(DateUtils.toString(DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
		tDataFim.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));

		tDataInicio.setMaxLength(10);
		tDataFim.setMaxLength(10);

		form.setColumns(1);

		TextField tLogin = new TextField("usuarioId", getMessage("coluna.login.label"), false);
		tLogin.setAttribute("class", "input");
		tLogin.setId("usuario");
		// tLogin.setFocus(true);

		comboTipo = new ComboTipoLogSistema();
		comboTipo.setLabel(getMessage("comboTipoLog.label"));
		form.add(comboTipo);

		form.add(tLogin);

		form.add(tDataInicio);
		form.add(tDataFim);

		Submit tfiltrar = new Submit("filtrar", getMessage("filtrar.label"), this, "filtrar");
		tfiltrar.setAttribute("class", "btn btn-primary min-width");
		form.add(tfiltrar);

		Submit texportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		texportar.setAttribute("class", "btn btn-outline blue min-width");
		form.add(texportar);

	}

	@Override
	public void onGet() {
		super.onGet();
		filtro = (LogSistemaFiltro) getContext().getSessionAttribute(LogSistemaFiltro.SESSION_FILTRO_KEY);
		if (filtro != null) {
			form.copyFrom(filtro);
			if (filtro.getUsuarioId() != null) {
				Usuario u = usuarioService.get(filtro.getUsuarioId());
				filtro.setUsuario(u);
			} else {
				filtro.setUsuario(null);
			}
		}
	}

	public boolean filtrar() {
		filtro = (LogSistemaFiltro) getContext().getSessionAttribute(LogSistema.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new LogSistemaFiltro();

			getContext().setSessionAttribute(LogSistemaFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);
		if (filtro.getUsuarioId() != null) {
			Usuario u = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(u);
		} else {
			filtro.setUsuario(null);
		}
		return true;
	}

	public boolean detalhes() {
		DetalhesLogSistemaPage page = (DetalhesLogSistemaPage) getContext().createPage(DetalhesLogSistemaPage.class);
		page.id = detalhes.getValueLong();
		setForward(page);
		return false;
	}

	public boolean exportar() {
		filtro = (LogSistemaFiltro) getContext().getSessionAttribute(LogSistemaFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new LogSistemaFiltro();
			getContext().setSessionAttribute(LogSistemaFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);
		if (filtro.getUsuarioId() != null) {
			Usuario u = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(u);
		} else {
			filtro.setUsuario(null);
		}

		String csv;
		try {
			csv = logService.exportarLogsSistema(filtro, getMaxResults(), getEmpresa());
			try {
				// log.info("Encode padrao: " +
				// getContext().getRequest().getCharacterEncoding());
				ServletUtil.exportBytesDownload(getContext().getResponse(), csv.getBytes(Utils.CHARSET), "log.csv");
			} catch (IOException e) {
				logError(e.getMessage(), e);
			}
		} catch (DomainException e1) {
			form.setError(e1.getMessage());
		}

		setPath(null);

		return false;
	}

	private int getMaxResults() {
		return 50;
	}

	@Override
	public void onRender() {
		super.onRender();

		try {
			if (filtro == null) {
				filtro = new LogSistemaFiltro();
				form.copyTo(filtro);
			}
			// Count(*)
			long count = logService.getCountByFilter(filtro, getEmpresa());
			table.setCount(count);

			List<LogSistema> logs = logService.findbyFilter(filtro, page, getMaxResults(), getEmpresa());

			int max = LogRepository.MAX;
			table.setPageSize(max);

			table.setRowList(logs);
		} catch (DomainException e) {
			form.setError(e.getMessage());
			return;
		}
	}
}
