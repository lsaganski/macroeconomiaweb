package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Idioma;

@Repository
public interface IdiomaRepository extends net.livetouch.tiger.ddd.repository.Repository<Idioma> {

	Idioma findByCodigo(Idioma idioma);

	List<Idioma> findByCodigos(String[] idiomas);

}