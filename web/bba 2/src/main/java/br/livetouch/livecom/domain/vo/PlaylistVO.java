package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Playlist;

public class PlaylistVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public long userId;
	public String urlFotoUsuario;
	public String userNome;
	public String userLogin;
	public long postId;
	public String postTitulo;
	public Long comentarioId;

	public void setPlaylist(Playlist f) {
		this.id = f.getId();
		this.userId 	= f.getUsuario().getId();
		this.userNome 	= f.getUsuario().getNome();
		this.userLogin 	= f.getUsuario().getLogin();
		this.urlFotoUsuario = f.getUsuario().getUrlFoto();
		if(f.getPost() != null) {
			this.postId 	= f.getPost().getId();
			this.postTitulo	= f.getPost().getTitulo();
		}
	}

	public static List<PlaylistVO> toListVO(List<Playlist> list) {
		List<PlaylistVO> vos = new ArrayList<PlaylistVO>();
		for (Playlist h : list) {
			PlaylistVO vo = new PlaylistVO();
			vo.setPlaylist(h);
			vos.add(vo);
		}
		return vos;
	}


	@Override
	public String toString() {
		return "PlaylistVO [userId=" + userId + ", postId=" + postId + ", titulo=" + postTitulo + "]";
	}
}
