package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;

import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.StringUtils;

import br.livetouch.livecom.chatAkka.DateUtils;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;

public class RelatorioVisualizacaoFiltro implements Serializable {

	private static final long serialVersionUID = -8800402609370460617L;

	public static final String SESSION_FILTRO_KEY = "relatorioVisualizacaoFiltro";

	@QueryParam("dataInicial")
	private String dataInicialString;
	@QueryParam("dataFinal")
	private String dataFinalString;
	@QueryParam("page")
	private int page;
	@QueryParam("max")
	private int max;
	@QueryParam("usuario")
	private Long usuarioId;
	@QueryParam("post")
	private Long postId;
	@QueryParam("categoria")
	private Long categoriaId;
	@QueryParam("modo")
	private String modo;

	private Usuario usuario;
	private Post post;
	private CategoriaPost categoria;
	public Date dataInicial;
	private Date dataFinal;
	private Long postImp;
	private Long userImp;
	
	private Long empresaId;
	
	// public RelatorioVisualizacaoFiltro(Date dataInicial, Date dataFinal, int
	// page, int max, Usuario usuario, Post post) {
	// super();
	// this.dataInicial = dataInicial;
	// this.dataFinal = dataFinal;
	// this.page = page;
	// this.max = max;
	// this.setUsuario(usuario);
	// this.setPost(post);
	// }

	public RelatorioVisualizacaoFiltro() {

	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public String getDataInicialString() {
		return dataInicialString;
	}

	public void setDataInicialString(String dataInicialString) {
		this.dataInicialString = dataInicialString;
	}

	public String getDataFinalString() {
		return dataFinalString;
	}

	public void setDataFinalString(String dataFinalString) {
		this.dataFinalString = dataFinalString;
	}

	public Date getDataInicial() {
		if (dataInicial == null) {
			if (StringUtils.isNotEmpty(this.dataInicialString) && StringUtils.isNotBlank(this.dataInicialString)) {
				this.dataInicial = DateUtils.toDate(this.dataInicialString);
			}
		}
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		if (dataFinal == null) {
			if (StringUtils.isNotEmpty(this.dataFinalString) && StringUtils.isNotBlank(this.dataFinalString)) {
				this.dataFinal = DateUtils.toDate(this.dataFinalString);
			}
		}
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Long getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Long categoriaId) {
		this.categoriaId = categoriaId;
	}

	public CategoriaPost getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaPost categoria) {
		this.categoria = categoria;
	}

	public Long getPostImp() {
		return postImp;
	}

	public void setPostImp(Long postImp) {
		this.postImp = postImp;
	}

	public Long getUserImp() {
		return userImp;
	}

	public void setUserImp(Long userImp) {
		this.userImp = userImp;
	}

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	}

	public Long getEmpresaId() {
		return empresaId;
	}

	public void setEmpresaId(Long empresaId) {
		this.empresaId = empresaId;
	}

}
