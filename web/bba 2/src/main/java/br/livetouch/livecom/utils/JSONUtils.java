package br.livetouch.livecom.utils;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@SuppressWarnings("unchecked")
public class JSONUtils {

	public static String toJSON(Object obj) {
        return toJSON(obj,false);
    }

    public static String toJSON(Object obj, boolean prettyPrinting) {
        Gson gson = prettyPrinting ? new GsonBuilder().setPrettyPrinting().create() : new Gson();
        String json = gson.toJson(obj);
        return json;
    }

	public static <T> T fromJSON(String json, Class<? extends Object> classOfT) {
        Gson gson = new Gson();
        T obj = (T) gson.fromJson(json, classOfT);
        return obj;
    }

    public static <T> T fromJSON(String json, Type typeOfT) {
        Gson gson = new Gson();
        List<T> obj = gson.fromJson(json, typeOfT);
        return (T) obj;
    }
}
