package br.livetouch.livecom.web.pages.cadastro;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;

@Controller
@Scope("prototype")
public class ExpedientesPage extends CadastrosPage {
	public boolean escondeChat = true;
	public Long id;
	public Form form = new Form();
	
	public Checkbox segunda;
	public Checkbox terca;
	public Checkbox quarta;
	public Checkbox quinta;
	public Checkbox sexta;
	public Checkbox sabado;
	public Checkbox domingo;

	@Override
	public void onInit() {
		super.onInit();
		form();
	}

	public void form() {
		
		segunda = new Checkbox("segunda", "Segunda");
		segunda.setId("segunda");
		form.add(segunda);
		
		terca = new Checkbox("terca", "Terça");
		terca.setId("terca");
		form.add(terca);
		
		quarta = new Checkbox("quarta", "Quarta");
		quarta.setId("quarta");
		form.add(quarta);
		
		quinta = new Checkbox("quinta", "Quinta");
		quinta.setId("quinta");
		form.add(quinta);
		
		sexta = new Checkbox("sexta", "Sexta");
		sexta.setId("sexta");
		form.add(sexta);
		
		sabado = new Checkbox("sabado", "Sabado");
		sabado.setId("sabado");
		form.add(sabado);
		
		domingo = new Checkbox("domingo", "Domingo");
		domingo.setId("domingo");
		form.add(domingo);
		
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();
	}
	
}
