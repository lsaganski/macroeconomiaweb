package br.livetouch.livecom.web.pages.pages;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.LongOperation;
import br.livetouch.livecom.web.pages.admin.LivecomAdminPage;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class DemoWaitPage extends LivecomAdminPage {

	public String cod;
	public Form form = new Form();

	@Override
	public void onInit() {
		super.onInit();
		form.setMethod("post");

		form.add(new TextField("cod"));
		form.add(new Submit("ok"));
	}
	
	@Override
	public void onPost() {
		if(StringUtils.isEmpty(cod)) {
			throw new RuntimeException("Informe o codigo");
		}
		
		startWork();
		
		setRedirect(WaitPage.class,"cod",cod);
	}
	
	public void startWork() {
		LongOperation.start(cod);
		
		// Simula um trabaho pesado
		new Thread(){
			public void run() {
				try {
					for (int i = 0; i < 20; i++) {
						try {
							System.out.println("JOB " + i);
							Thread.sleep(1000);
						} catch (InterruptedException e) {
						}
					}
				} finally {
					LongOperation.finish(cod);
				}
			}
		}.start();
	}
}
