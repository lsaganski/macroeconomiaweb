package br.livetouch.livecom.domain.repository.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.spring.StringHibernateRepository;

@Repository
@SuppressWarnings("unchecked")
public abstract class LivecomRepository<T> extends StringHibernateRepository<T> {

	public LivecomRepository(Class<T> clazz) {
		super(clazz);
	}
	
	@Override
	protected Query createQuery(String query) {
//		System.out.println("-- 1 -- ");
		Query q = super.createQuery(query);
//		System.out.println("-- 2 -- ");
		return q;
	}
	
	/**
	 * @see net.livetouch.ddd.entity.Repository#getCount()
	 */
	public long getCount() {
		// TODO LivetouchWeb -fix  colocar Long lá. Gerar nova versao 
		Long i = (Long)createCriteria().setProjection(Projections.rowCount()).list().get(0);
		if(i == null){
			return 0;
		}
		long count = i.longValue();
		return count;
	}

	/**
	 * Seta o cache na query conforme o parametro.
	 * 
	 * @param q
	 */
	protected void setCache(Query q) {
		boolean cache = ParametrosMap.getInstance().getBoolean(Params.HIBERNATE_CACHE_ON, true);
		if (cache) {
			q.setCacheable(true);
		}
	}

	// protected void toSQLDateFormat(String format) {
	// %%
	// Dialect dialect;
	// if (dialect instanceof OracleDialect) {
	// dialect = ((SessionFactoryImplementor) sessionFactory).getDialect();
	// replace @@
	// }
	// }

	public int exec(String hql) {
		Query q = createQuery(hql);

		int count = q.executeUpdate();
		log.debug("Rows updated [" + count + "]");

		return count;
	}
	
	public List<Long> queryIds(String hql, boolean cache, Object... params) {
		Query q = createQuery(hql);
		for (int i = 0; i < params.length; i++) {
			q.setParameter(i, params[i]);
		}

		q.setCacheable(cache);
		
		List<Long> list = (List<Long>) q.list();
		return list;
	}
	
	public List<T> query(String hql, boolean cache, Object... params) {
		return query(hql, cache, -1, -1, params);
	}

	public List<T> query(String hql, int page, int maxSize, Object... params) {
		return query(hql, false, page, maxSize, params);
	}

	public List<T> query(String hql, boolean cache, int page, int maxSize, Object... params) {
		Query q = createQuery(hql);
		for (int i = 0; i < params.length; i++) {
			q.setParameter(i, params[i]);
		}

		if (maxSize > 0) {
			int firstResult = 0;
			if (page != 0) {
				firstResult = page * maxSize;
			}
			q.setFirstResult(firstResult);
			q.setMaxResults(maxSize);
		}

		q.setCacheable(cache);
		List<T> list = (List<T>) q.list();
		return list;
	}
	
	public int execute(String hql,List<Long> ids) {
		Query q = getSession().createQuery(hql);
		if(ids != null) {
			for (int i = 0; i < ids.size(); i++) {
				q.setLong(i, ids.get(i));
			}
		}

		int count  = q.executeUpdate();
		log.debug("Rows updated [" + count + "]");

		return count;
	}
	
	public int execute(String hql,Object... params) {
		Query q = getSession().createQuery(hql);
		for (int i = 0; i < params.length; i++) {
			q.setParameter(i, params[i]);
		}

		int count  = q.executeUpdate();
		log.debug("Rows updated [" + count + "]");

		return count;
	}
	
	public int execute(String hql,Long... ids) {
		Query q = getSession().createQuery(hql);
		for (int i = 0; i < ids.length; i++) {
			q.setLong(i, ids[i]);
		}

		int count  = q.executeUpdate();
		log.debug("Rows updated [" + count + "]");

		return count;
	}
	
	@SuppressWarnings("rawtypes")
	public int executeIn(String hql,String param,Collection list) {
		if(list == null || list.isEmpty()) {
			return 0;
		}

		Query q = getSession().createQuery(hql);
		q.setParameterList(param, list);

		int count  = q.executeUpdate();
		log.debug("Rows updated [" + count + "]");

		return count;
	}
	
	@SuppressWarnings("rawtypes")
	public List<T> queryIn(String hql,boolean cache,String param,Collection ids) {
		if(ids == null || ids.isEmpty()) {
			return new ArrayList<T>();
		}
		
		Query q = createQuery(hql);
		q.setParameterList(param, ids);

		q.setCacheable(cache);
		List<T> list = (List<T>) q.list();
		return list;
	}
	
	protected Date getDateTrimMinute() {
		Date now = new Date();
		now = org.apache.commons.lang.time.DateUtils.truncate(now, Calendar.MINUTE);
		return now;
	}
}