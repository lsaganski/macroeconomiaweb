package br.livetouch.livecom.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.gson.Gson;

import br.livetouch.livecom.domain.vo.FuncaoVO;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Funcao extends JsonEntity{
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "FUNCAO_SEQ")
	private Long id;

	private String nome;
	
	private String codigo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public String toString() {
		return "Formacao [id=" + id + ", nome=" + nome + "]";
	}

	public String getCodigoDesc() {
		return codigo != null ? codigo : id.toString();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Override
	public String toJson() {
		FuncaoVO vo = new FuncaoVO(this);
		return new Gson().toJson(vo);
	}
	
}
