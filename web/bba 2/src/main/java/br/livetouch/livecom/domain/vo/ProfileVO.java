package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.UsuarioService;

/**
 * Campo para usar na tela de meus dados
 * 
 * @author Ricardo Lecheta
 * 
 */
public class ProfileVO implements Serializable {

	protected static Logger log = Logger.getLogger(ProfileVO.class);
	
	enum Type {
		FONE, LADO, LINHA
	}

	private static final long serialVersionUID = 1721950199925705014L;
	
	private String name;
	private String subName;
	private List<CampoUsersVO> values = new ArrayList<CampoUsersVO>();
//	private List<CampoUsersVO> valuesHeader = new ArrayList<CampoUsersVO>();

	public ProfileVO(String name, String subName) {
		super();
		this.name = name;
		this.subName = subName;
	}

	public boolean put(String key, String value) {
//		return values.add(new CampoUsersVO(key, value, null));
		return put(key, value, null);
	}

	public boolean put(String key, String value, Type type) {
		return values.add(new CampoUsersVO(key, value, type));
	}

	public static ProfileVO create(Usuario user, UsuarioService service, String buildType, CamposMap campos) {
		if (user == null) {
			return new ProfileVO("", "");
		}
		
		if(campos == null) {
			campos = CamposMap.getInstance(user);
		}

//		String buildType = ParametrosMap.getInstance().get("buildType");
		log.debug("Profile [" + user + "]" + buildType);

		String subName = user.getFuncao() != null ? user.getFuncao().getNome() : "";
		String name = user.getNome();
		
		ProfileVO p = new ProfileVO(name, subName);
		if(StringUtils.equals(buildType, "livecom") || StringUtils.equals(buildType, "invictus")){
			
			log.debug("Montando profile livecom");
			
			if(campos.isCampoVisivel("dataNasc")) {
				p.put("Data de Nascimento", StringUtils.isNotEmpty(user.getDataNascString()) ? user.getDataNascString() : "-", Type.LADO);	
			}
			if(campos.isCampoVisivel("genero")) {
				p.put("Sexo", StringUtils.isNotEmpty(user.getGenero()) ? user.getGenero() : "-", Type.LADO);	
			}
			if(campos.isCampoVisivel("telefoneFixo")) {
				p.put("Telefone", StringUtils.isNotEmpty(user.getTelefoneFixo()) ? user.getTelefoneFixo() : "-", Type.FONE);	
			}
			if(campos.isCampoVisivel("telefoneCelular")) {
				p.put("Telefone Celular", StringUtils.isNotEmpty(user.getTelefoneCelular()) ? user.getTelefoneCelular() : "-", Type.FONE);				
			}
			
			p.put("", "", Type.LINHA);
			
			if(campos.isCampoVisivel("funcao")) {
				p.put("Função", subName);
			}
			if(campos.isCampoVisivel("unidade")){
				p.put("Unidade", StringUtils.isNotEmpty(user.getUnidade()) ? user.getUnidade() : "-");	
			}
			if(campos.isCampoVisivel("estado")){
				p.put("Estado", StringUtils.isNotEmpty(user.getEstado()) ? user.getEstado() : "-");				
			}
			if(campos.isCampoVisivel("cidade")){
				if (user.getCidade() != null) {
					p.put("Cidade", StringUtils.isNotEmpty(user.getCidade().getNome()) ? user.getCidade().getNome() : "-");
				}	
			}

			p.put("", "", Type.LINHA);
		}
		
		if(campos.isCampoVisivel("grupos")){
			p.put("", "", Type.LINHA);
			List<Grupo> grupos = service.getGrupos(user);
			if (grupos != null) {
				String s = "";
				boolean firstTime = true;
				for (Grupo g : grupos) {
					if (!firstTime) {
						s += ", ";
					}
					s += g.getNomeString(user);
					firstTime = false;
				}
				p.put("Grupos que este usuário pode visualizar", s);
				p.put("", "", Type.LINHA);
			}	
		}
		
		return p;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubName() {
		return subName;
	}

	public void setSubName(String subName) {
		this.subName = subName;
	}

//	public List<CampoUsersVO> getValuesHeader() {
//		return valuesHeader;
//	}
//
//	public void setValuesHeader(List<CampoUsersVO> valuesHeader) {
//		this.valuesHeader = valuesHeader;
//	}
}
