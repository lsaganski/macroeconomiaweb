package br.livetouch.livecom.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class FileExtensionUtils {

	/**
	 * @param file
	 * @return
	 */
	public static String getMimeType(String file) {
		String mimeType = com.amazonaws.services.s3.internal.Mimetypes.getInstance().getMimetype(file);
		if (mimeType != null) {
			return mimeType;
		}

		if (file == null) {
			return "text/plain";
		}
		if (file.endsWith(".txt")) {
			return "text/plain";
		}
		if (file.endsWith(".jpg") || file.endsWith(".jpeg")) {
			return "image/jpeg";
		}
		if (file.endsWith(".gif")) {
			return "image/gif";
		}
		if (file.endsWith(".png")) {
			return "image/png";
		}
		if (file.endsWith(".csv")) {
			return "application/csv";
		}
		if (file.endsWith(".pdf")) {
			return "application/pdf";
		}
		if (file.endsWith(".xls")) {
			return "application/excel";
		}
		if (file.endsWith(".doc")) {
			return "application/doc";
		}

		return "text/plain";
	}

	public static String getMimeType(File file) {
		String mimeType = com.amazonaws.services.s3.internal.Mimetypes.getInstance().getMimetype(file);
		if (mimeType != null) {
			return mimeType;
		}

		String nome = file.getName();
		return getMimeType(nome);
	}

	public static String getExtensao(String nome) {
		if (StringUtils.isEmpty(nome)) {
			return "";
		}
		int idx = nome.lastIndexOf(".");
		if (idx == -1) {
			return "";
		}
		String ext = nome.substring(idx + 1);
		return ext;
	}

	public static String getNome(String nome) {
		if (StringUtils.isEmpty(nome)) {
			return "";
		}
		int idx = nome.lastIndexOf(".");
		if (idx == -1) {
			return "";
		}
		String ext = nome.substring(0, idx);
		return ext;
	}

	public static String getFileNameFixed(String nomeOriginal) {
		String nome = nomeOriginal;
		int idxIE = nome.lastIndexOf("\\");
		if (idxIE != -1) {
			// fix ie
			nome = nome.substring(idxIE + 1);
		}
		nome = StringUtils.replace(nome, " ", "_");
		// nome = StringUtils.replace(nome, ".", "_");

		String fileName = FileExtensionUtils.getNome(nome);
		String ext = FileExtensionUtils.getExtensao(nome);
		if (StringUtils.isEmpty(ext)) {
			throw new RuntimeException("Extensão do arquivo não reconhecida [" + nome + "]");
		}

		fileName = StringUtils.replace(fileName, ".", "_");
		nome = fileName + "." + ext;
		return nome;
	}

	public static boolean isImage(String nome) {
		if (nome == null) {
			return false;
		}
		nome = nome.toLowerCase();
		if (nome.endsWith("jpg") || nome.endsWith("jpeg")) {
			return true;
		}
		if (nome.endsWith("gif") || nome.endsWith("png")) {
			return true;
		}
		return false;
	}

	public static boolean isVideo(String nome) {
		if (nome == null) {
			return false;
		}
		nome = nome.toLowerCase();
		if (nome.endsWith("mp4") || nome.endsWith("3gp")) {
			return true;
		}
		if (nome.endsWith("mpeg")) {
			return true;
		}
		return false;
	}

	public static String[] getExtensionsByTipo(String[] tipo) {

		if (tipo.length <= 0) {
			return null;
		}

		String[] ext = null;
		List<String> list = new ArrayList<String>();

		for (String extencao : tipo) {

			switch (extencao) {
			case "imagem":
				ext = new String[] { "IMAGE/JPG", "IMAGE/JPEG", "IMAGE/GIF", "IMAGE/PNG", "IMAGE/PSD" };
				list.addAll(Arrays.asList(ext));
				break;
			case "video":
				ext = new String[] { "VIDEO/MP4", "VIDEO/3GP", "VIDEO/MPEG", "VIDEO/AVI" };
				list.addAll(Arrays.asList(ext));
				break;
			case "documento":
				ext = new String[] { "APPLICATION/DOC", "APPLICATION/DOCX", "APPLICATION/PDF", "APPLICATION/PPT", "APPLICATION/PPTX", "APPLICATION/XLS", "APPLICATION/CSV" };
				list.addAll(Arrays.asList(ext));
				break;
			default:
				ext = new String[] { "IMAGE/JPG", "IMAGE/JPEG", "IMAGE/GIF", "IMAGE/PNG", "IMAGE/PSD", "VIDEO/MP4", "VIDEO/3GP", "VIDEO/MPEG", "VIDEO/AVI", "DOC", "DOCX", "PDF", "PPT", "PPTX", "XLS", "CSV" };
				list.addAll(Arrays.asList(ext));
				break;
			}

		}
		ext = list.toArray(ext);
		return ext;

	}

}
