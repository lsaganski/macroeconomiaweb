package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.EntityField;
import br.livetouch.livecom.domain.Funcao;
import net.sf.click.control.Form;

/**
 * @author Juillian Lee
 * 
 */
@Controller
@Scope("prototype")
public class DeletarFuncaoPage extends WebServiceXmlJsonPage {
	public Form form = new Form();
	private EntityField<Funcao> tFuncao;

	protected void form() {
		form.add(tFuncao = new EntityField<>("id", true, funcaoService));
	}
	
	@Override
	public void onInit() {
		super.onInit();
		form();
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Funcao f = tFuncao.getEntity();
			if(f == null) {
				return new MensagemResult("NOK", "Função não encontrada");
			} 
			funcaoService.delete(getUsuario(), f);
			return new MensagemResult("OK", "Função deletada com sucesso");
		}
		return new MensagemResult("NOK", "Formulario invalido");
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("funcao", Funcao.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
