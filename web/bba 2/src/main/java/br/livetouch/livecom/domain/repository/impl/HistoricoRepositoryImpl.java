package br.livetouch.livecom.domain.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Historico;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.HistoricoRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class HistoricoRepositoryImpl extends StringHibernateRepository<Historico> implements HistoricoRepository {

	public HistoricoRepositoryImpl() {
		super(Historico.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Historico> findAllByUser(Usuario u) {
		StringBuffer sb = new StringBuffer("from Historico h where h.usuario.id=?");

		Query q = createQuery(sb.toString());

		q.setLong(0, u.getId());
		List<Historico> list = q.list();
		return list;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Post> findAllPostsByUser(Usuario u, int page, int maxRows) {
		if(u == null) {
			return new ArrayList<Post>();
		}
		
		StringBuffer sb = new StringBuffer("select distinct p from Historico h inner join h.post p where 1=1 ");
		
		if(u != null) {
			sb.append(" and h.usuario.id=?");
		}
		
		sb.append(" order by h.data desc");

		Query q = createQuery(sb.toString());
		
		if( maxRows > 0) {
			int firstResult =  page * maxRows;
			q.setFirstResult(firstResult);
			q.setMaxResults(maxRows);
		}

		if(u != null) {
			q.setLong(0, u.getId());
		}
		List<Post> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Historico findByUserPost(Usuario u, Post p) {
		if(u == null) {
			return null;
		}

		StringBuffer sb = new StringBuffer("from Historico h where h.usuario.id=? and h.post.id=?");

		Query q = createQuery(sb.toString());
		
		q.setLong(0, u.getId());
		q.setLong(1, p.getId());
		List<Historico> list = q.list();
		Historico historico = list.size() > 0 ? list.get(0) : null;
		return historico;
	}
}