package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.PostIdioma;

public class PostIdiomaVO implements Serializable {

	private static final long serialVersionUID = 1L;
	public Long id;
	
	public String titulo;
	public String mensagem;
	public String resumo;

	public IdiomaVO idioma;

	public PostIdiomaVO() {

	}

	public PostIdiomaVO(PostIdioma postIdioma) {
		this.id = postIdioma.getId();
		
		this.titulo = postIdioma.getTitulo();
		this.mensagem = postIdioma.getMensagem();
		this.resumo = postIdioma.getResumo();

		Idioma i = postIdioma.getIdioma();
		if (i != null) {
			this.idioma = new IdiomaVO(i);
		}

	}

	public static List<PostIdiomaVO> fromList(Set<PostIdioma> postIdioma) {
		List<PostIdiomaVO> vos = new ArrayList<>();
		if (postIdioma == null) {
			return vos;
		}
		for (PostIdioma pi : postIdioma) {
			vos.add(new PostIdiomaVO(pi));
		}
		return vos;
	}

}
