package br.livetouch.livecom.web.pages.cadastro;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.service.TagService;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class TagsPage extends CadastrosPage {

	@Autowired
	TagService tagService;

	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;
	
	public boolean escondeChat = true;

	public int page;
//	public int lastPage;

	public List<Tag> tags;

	public Tag tag;


	@Override
	public void onInit() {
		super.onInit();
		
		form();
	}
	
	
	public void form(){
		form.add(new IdField());

		TextField tNome = new TextField("nome", getMessage("nome.label"));
		tNome.setAttribute("autocomplete", "off");
		tNome.setAttribute("rel", getMessage("tags.adicionar.label"));
		tNome.setAttribute("class", "mascara");
		form.add(tNome);

		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip botao export");
		form.add(tExportar);
		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao salvar btSalvar");
		form.add(tsalvar);
		
		form.add(new Submit("novo", getMessage("novo.label"), this, "novo"));

	}

	@Override
	public void onGet() {
		super.onGet();
		
		if (id != null) {
			tag = tagService.get(id);
		} else {
			tag = new Tag();
		}
		
		if(tag != null) {
			form.copyFrom(tag);
		}
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				tag = id != null ? tagService.get(id) : new Tag();
				form.copyTo(tag);

				tagService.saveOrUpdate(getUsuario(),tag);

				setMessageSesssion(getMessage("msg.tags.salvar.sucess", tag.getNome()),getMessage("msg.tags.salvar.label.sucess"));
				setRedirect(TagsPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(TagsPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		tag = tagService.get(id);
		form.copyFrom(tag);
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Tag tag = tagService.get(id);
			tagService.delete(getUsuario(),tag, false);
			setRedirect(TagsPage.class);
			setMessageSesssion(getMessage("msg.tags.excluir.sucess"),getMessage("msg.tags.excluir.label.sucess"));
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.tags.excluir.error");
		} catch (Exception e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.tags.excluir.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		tags = tagService.findAll(getEmpresa());

		// Count(*)
		int pageSize = 10;
		long count = tags.size();

		createPaginator(page, count, pageSize);
	}
	
	public boolean exportar(){
		String csv = tagService.csvTag(getEmpresa());
		download(csv, "tags.csv");
		return true;
	}
}
