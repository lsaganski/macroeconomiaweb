package br.livetouch.livecom.domain;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
public class StatusMensagemUsuario extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "STATUSMENSAGEMUSUARIO_SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "conversa_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
	private MensagemConversa conversa;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
	private Usuario usuario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mensagem_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
	private Mensagem mensagem;

	/**
	 * Usuario leu
	 */
	private boolean lida;
	private Date dataLida;

	/**
	 * Chegou no device do usuario
	 */
	private boolean entregue;
	private Date dataEntregue;
	
	private boolean excluida;
	private Date dataExcluida;

	@Override
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public MensagemConversa getConversa() {
		return conversa;
	}
	public void setConversa(MensagemConversa conversa) {
		this.conversa = conversa;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Mensagem getMensagem() {
		return mensagem;
	}
	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}
	public boolean isExcluida() {
		return excluida;
	}
	public void setExcluida(boolean excluida) {
		this.excluida = excluida;
	}
	public boolean isLida() {
		return lida;
	}
	public boolean isEntregue() {
		return entregue;
	}
	public void setLida(boolean lida) {
		this.lida = lida;
	}
	public void setEntregue(boolean entregue) {
		this.entregue = entregue;
	}
	public Date getDataLida() {
		return dataLida;
	}
	public void setDataLida(Date dataLida) {
		this.dataLida = dataLida;
	}
	public Date getDataExcluida() {
		return dataExcluida;
	}
	public void setDataEntregue(Date dataEntregue) {
		this.dataEntregue = dataEntregue;
	}
	
	public Date getDataEntregue() {
		return dataEntregue;
	}
	public void setDataExcluida(Date dataExcluida) {
		this.dataExcluida = dataExcluida;
	}
	
	public static List<Long> getIds(Collection<StatusMensagemUsuario> list) {
		List<Long> ids = new ArrayList<Long>();
		if(list == null || list.size() == 0) {
			return ids;
		}
		for (net.livetouch.tiger.ddd.Entity e : list) {
			ids.add(e.getId());
		}
		return ids;
	}
	
	public boolean isFrom(Usuario u) {
		return getUsuario().getId() == u.getId();
	}
	@Override
	public String toString() {
		return "StatusMsgUsuario [id=" + id + ", msg=" + mensagem.getId() + ", usuario=" + usuario.getLogin() + ", entregue=" + entregue
				+ ", dataEntregue=" + dataEntregue  + ", lida=" + lida + ", dataLida=" + dataLida + "]";
	}
}
