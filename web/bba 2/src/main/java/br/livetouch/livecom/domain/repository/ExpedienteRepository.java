package br.livetouch.livecom.domain.repository;

import java.util.List;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Expediente;
import net.livetouch.tiger.ddd.repository.Repository;

public interface ExpedienteRepository extends Repository<Expediente> {

	Expediente findByNome(String nome);

	List<Object[]> findIdNomeExpediente();
	
	Expediente findByCodigo(Expediente expediente, Empresa empresa);

}