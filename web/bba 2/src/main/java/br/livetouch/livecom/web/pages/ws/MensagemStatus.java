package br.livetouch.livecom.web.pages.ws;

import java.io.Serializable;

public class MensagemStatus implements Serializable {
	private static final long serialVersionUID = -6151343606701955567L;
	private String msg;
	private boolean status;
	private String id;

	public MensagemStatus(String msg, String id) {
		super();
		this.msg = msg;
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
}
