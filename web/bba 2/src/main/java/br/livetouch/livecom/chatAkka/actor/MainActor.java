package br.livetouch.livecom.chatAkka.actor;

import org.apache.log4j.Logger;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.UntypedActor;
import br.infra.util.Log;
import br.livetouch.livecom.chatAkka.actor.AkkaFactory.FindActorCallback;
import br.livetouch.livecom.chatAkka.protocol.JsonRawMessage;
import br.livetouch.livecom.chatAkka.router.UserMessageRouter;

public class MainActor extends UntypedActor {

	private static Logger log = Log.getLogger("MainActor");

	public static class Start {
		public int port;

		public Start(int port) {
			this.port = port;
		}
	}

	public static class CreateUserActor {
		public JsonRawMessage msg;
		public String userId;
		private String so;

		public CreateUserActor(JsonRawMessage msg,String so, String userId) {
			this.msg = msg;
			this.so = so;
			this.userId = userId;
		}
	}

	@Override
	public void preStart() throws Exception {
		log.debug("Start Main Actor");
		super.preStart();
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof Start) {
			// Evento para inicializar o AKKA, disparado pelo StartServerListener

			log.debug("Start UserMessageRouter");
			UserMessageRouter.create(getContext());

			int port = ((Start) msg).port;
			log.debug("Start TCP Actor");
			StartTcpConnectionActor.start(getContext(), port,getSelf());

		} else if (msg instanceof CreateUserActor) {
			/**
			 * Usuário conectou. Vamos criar um UserActor
			 */
			createUserActor((CreateUserActor) msg);
		}
	}

	/**
	 * Tenta criar um usuário.
	 * 
	 * Nome: userActor_id
	 * 
	 * Se logado, adiciona no singleton map do chat de usuarios logados. Se ja
	 * logado, associa a conexão.
	 * 
	 * @param create
	 */
	public void createUserActor(CreateUserActor create) {

		final JsonRawMessage tcpRawMessage = create.msg;
		final Long userId = Long.parseLong(create.userId);

		final String name = "userActor_" + userId;
		
		final String so = create.so;
		
		final ActorRef tcpWebActor = tcpRawMessage.actor;

		ActorSelection selection = AkkaFactory.getUserActor(getContext(), userId);
		AkkaFactory.findActorAsync(selection, new FindActorCallback() {
			
			@Override
			public void actorNotFound() {
				log.debug("notFound: Create user Actor: " + name);

				// Cria actor
				ActorRef actor = UserActor.create(getContext(),name);

				// Add connection.
				actor.tell(new UserActor.AddConnection(tcpWebActor,userId, so), getSelf());
				
				// Add session no chat
				//Livecom.getInstance().addSession(userId, so, tcpWebActor);
			}
			
			@Override
			public void actorFound(ActorRef userActor) {
				log.debug("actorFound: " + name + " > " + userActor);
				
				// Add connection.
				userActor.tell(new UserActor.AddConnection(tcpWebActor,userId, so), getSelf());
				
				// Add session no chat
//				Livecom.getInstance().addSession(userId, so, tcpWebActor);
			}
		});
	}
}
