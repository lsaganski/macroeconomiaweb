
package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.ComentarioVO;
import br.livetouch.livecom.domain.vo.FileVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ComentarPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private LongField tUserId;
	private LongField tId;
	private LongField tPostId;
	private TextField tComentario;
	private TextField tUsuariosMarcado;
	private TextField tMode;

	@Override
	public void onInit() {
		super.onInit();
		form.setMethod("post");
		form.add(tId = new LongField("comment_id"));
		form.add(tUserId = new LongField("user_id", true));
		form.add(tPostId = new LongField("post_id", true));
		form.add(tComentario = new TextField("comentario"));
		form.add(tUsuariosMarcado = new TextField("usuarios_marcado"));


		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));

		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		tUserId.setFocus(true);

		form.add(new Submit("Comentar"));

		// setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		String texto = null;
		if (form.isValid()) {
			
			String isOn = ParametrosMap.getInstance(getEmpresa()).get(Params.MANUTENCAO_ON, "0");
			String msgManutencao = ParametrosMap.getInstance(getEmpresa()).get(Params.MANUTENCAO_MESSASGE, "Sistema em manutenção, não é possível postar comunicados nem comentar.");
			if("1".equals(isOn)) {
				return new MensagemResult("NOK", msgManutencao); 
			}
			
			long userId = tUserId.getLong();
			long postId = tPostId.getLong();
			Long id = tId.getLong();
			texto = tComentario.getValue();

			log("Comentario [: " + texto + "]");
			
			List<Long> arquivoIds = getListIds("arquivo_ids");
			List<Arquivo> arquivos = arquivoService.findAllByIds(arquivoIds);
			
			if (StringUtils.isEmpty(texto) && arquivos.isEmpty()) {
				logError("Comentario ERROR: " + texto);
				return new MensagemResult("ERROR", "Texto do comentário inválido.");
			}

			Usuario u = usuarioService.get(userId);
			if (u == null) {
				return new MensagemResult("ERROR", "Usuário inválido.");
			}

			Post post = postService.get(postId);
			if (post == null) {
				return new MensagemResult("ERROR", "Post inválido.");
			}
			
			if(isWordPress()) {
				return new MensagemResult("Atenção", "Funcionalidade de comentário em desenvolvimento.");
			} else {

				// Usuarios Marcados
				String usuariosMarcados = tUsuariosMarcado.getValue();

				// Salva o comentario
				Comentario c = comentarioService.comentar(id,u,post,texto,arquivos,usuariosMarcados);

				// JSON
				ComentarioVO vo = new ComentarioVO();
				vo.setComentario(c);
				
				notificationService.markPostAsRead(post, u);
				notificationService.markComentarioAsRead(post.getId(), u.getId());
				
				if(isWsVersion3()) {
					Response r = Response.ok("OK");
					r.comentario = vo;
					return r;
				}
				
				return vo;
			}
		}

		return new MensagemResult("NOK", "Erro ao comentar post.");
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("comentario", ComentarioVO.class);
		x.alias("arquivo", FileVO.class);
		super.xstream(x);
	}
}
