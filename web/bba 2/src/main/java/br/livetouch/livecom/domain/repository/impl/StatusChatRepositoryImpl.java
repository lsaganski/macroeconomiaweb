package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.StatusChat;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.StatusChatRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class StatusChatRepositoryImpl extends StringHibernateRepository<StatusChat> implements StatusChatRepository {

	public StatusChatRepositoryImpl() {
		super(StatusChat.class);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<StatusChat> findAll(boolean cache) {
		Query q = createQuery("from StatusChat where usuario is null");
		q.setCacheable(cache);
		List<StatusChat> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StatusChat> findAll(Usuario user) {
		Query q = createQuery("from StatusChat where usuario is null or usuario = ?");
		q.setParameter(0, user);
		List<StatusChat> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StatusChat> getStatusUsuario() {
		Query q = createQuery("from StatusChat where 1=1 ");
		
		List<StatusChat> list = q.list();
		return list;
	}

}