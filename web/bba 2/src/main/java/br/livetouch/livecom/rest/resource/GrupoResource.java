package br.livetouch.livecom.rest.resource;


import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusParticipacao;
import br.livetouch.livecom.domain.enums.TipoGrupo;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.PerfilService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.ConfiguracoesVO;
import br.livetouch.livecom.domain.vo.Filtro;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import br.livetouch.livecom.utils.UploadHelper;
import br.livetouch.livecom.web.pages.ws.MensagemResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/grupo")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class GrupoResource extends MainResource{

	protected static final Logger log = Log.getLogger(GrupoResource.class);
	
	@Context
	private SecurityContext securityContext;
	
	@Autowired
	protected UsuarioService usuarioService;
	
	@Autowired
	PerfilService permissaoService;
	
	@Autowired
	protected GrupoService grupoService;
	
	@Context
	protected ServletContext context;
	
	@POST
	@Path("/uploadImagem")
	public Response uploadImagem(@QueryParam("fotoId") Long fotoId, @QueryParam("grupoId") Long grupoId) throws DomainException {
		
		try {

			if(grupoId == null || fotoId == null) {
				return Response.ok(MessageResult.error("Não foi possivel efetuar o upload da imagem")).build();
			}
			
			Grupo grupo = grupoService.get(grupoId);

			//permissão
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS) && !grupoService.isAdminOrSubadminGrupo(getUserInfo(), grupo)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}

			Arquivo a = arquivoService.get(fotoId);
			
			UploadHelper.log.debug("Arquivo: " + a.getId());
			UploadHelper.log.debug("Gerando thumbs arquivo: " + a.getNome());
			
			ParametrosMap params = ParametrosMap.getInstance(getEmpresa().getId());
			ThumbVO voThumb;
			voThumb = UploadHelper.createThumbFotoUsuario(params, "/grupo", a.getUrl(), a.getNome());
			ArquivoThumb t = new ArquivoThumb();
			t.setThumbVo(voThumb);
			t.setArquivo(a);
			arquivoService.saveOrUpdate(t);
			grupo.setFoto(a);
			grupoService.saveOrUpdate(getUserInfo(), grupo);
			
			return Response.ok(MessageResult.ok("Upload ok")).build();
		} catch (IOException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel efetuar o upload da imagem")).build();
		}
	}
	
	@POST
	@Path("/addUsuarios")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED + ";charset=utf-8")
	public Response addUsuarios(
			@FormParam("grupoId") String grupoId,
			@FormParam("usuariosId")  String usuariosId,
			@FormParam("usuariosRemoveId") String usuariosRemoveId) 
					throws DomainException {
		
		if(StringUtils.isEmpty(grupoId)) {
			return Response.ok(MessageResult.error("Id do grupo não informado")).build();
		}
		Grupo grupo = grupoService.get(Long.parseLong(grupoId));
		
		if(grupo == null) {
			return Response.ok(MessageResult.error("Grupo " + grupoId + " não encontrado")).build();
		}
		
		//permissão
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS) && !grupoService.isAdminOrSubadminGrupo(getUserInfo(), grupo)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}

		List<Usuario> usersToSave = new ArrayList<>();
		if(StringUtils.isNotEmpty(usuariosId)) {
			String[] split = usuariosId.split(",");
			for (String s : split) {
				if(StringUtils.isNotEmpty(s.trim())) {
					Usuario usuario = usuarioService.get(Long.parseLong(s.trim()));
					if(usuario != null) {
						usersToSave.add(usuario);
					} 
				}
			}
		}
		List<Usuario> usersRemove = new ArrayList<>();
		if(StringUtils.isNotEmpty(usuariosRemoveId)) {
			
			boolean removerAdmin = ParametrosMap.getInstance(getEmpresa()).getBoolean(Params.REMOVER_ADMIN_GRUPO_ON, false);
			
			String[] split = usuariosRemoveId.split(",");
			for (String s : split) {
				if(StringUtils.isNotEmpty(s.trim())) {
					Usuario usuario = usuarioService.get(Long.parseLong(s.trim()));
					if(usuario != null) {
						boolean isAdmin = grupoService.isAdmin(usuario, grupo);
						GrupoUsuarios gu = usuarioService.getGrupoUsuario(usuario, grupo);
						//Valida grupo usuario porque existem grupos em que o admin não esta em grupo usuarios
						if(!removerAdmin && isAdmin && gu != null) {
							return Response.ok(MessageResult.error("Você é o admin e não pode sair do grupo ["+ grupo.getNome() +"]")).build();
						}
						usersRemove.add(usuario);
					} else {
						return Response.ok(MessageResult.error("Usuario à ser deletado inexistente")).build();
					}
				}
			}
		}
		
		String msg = "";
		if(usersToSave.size() > 0) {
			usuarioService.saveUsersToGrupo(getUserInfo(), grupo, usersToSave);
			msg = "Usuário adicionado ao grupo com sucesso";
		}
		
		if(usersRemove.size() > 0) {
			usuarioService.revemoUsersToGrupo(getUserInfo(), grupo, usersRemove);
			msg = "Usuário removido do grupo com sucesso";
		}
		
		if(usersRemove.size() > 0 && usersToSave.size() > 0) {
			msg = "Grupo modificado com sucesso";
		}
		
		return Response.ok(MessageResult.ok(msg)).build();
	}
	
	@POST
	@Path("/addGrupos")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED + ";charset=utf-8")
	public Response addGrupos(
			@FormParam("grupoId") String grupoId,
			@FormParam("gruposId")  String gruposId,
			@FormParam("gruposRemoveId") String gruposRemoveId) 
					throws DomainException {
		
		if(StringUtils.isEmpty(grupoId)) {
			return Response.ok(MessageResult.error("Id do grupo não informado")).build();
		}
		Grupo grupo = grupoService.get(Long.parseLong(grupoId));
		
		if(grupo == null) {
			return Response.ok(MessageResult.error("Grupo " + grupoId + " não encontrado")).build();
		}

		//permissão
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS) && !grupoService.isAdminOrSubadminGrupo(getUserInfo(), grupo)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}

		if(!grupo.isGrupoVirtual()) {
			return Response.ok(MessageResult.error("Este não é um grupo virtual")).build();
		}
		
		//lista de subgrupos
		List<Grupo> list = grupoService.findSubgruposByGrupo(grupo);
		List<Grupo> subgruposToSave = new ArrayList<Grupo>();
		if(StringUtils.isNotEmpty(gruposId)) {
			String[] split = gruposId.split(",");
			for (String s : split) {
				if(StringUtils.isNotEmpty(s.trim())) {
					Grupo g = grupoService.get(Long.parseLong(s.trim()));
					if(g != null) {
						
						if(g.isGrupoVirtual()) {
							continue;
						}

						if(list.contains(g)) {
							continue;
						}

						subgruposToSave.add(g);
					} 
				}
			}
		}
		List<Grupo> subgruposRemove = new ArrayList<>();
		if(StringUtils.isNotEmpty(gruposRemoveId)) {
			String[] split = gruposRemoveId.split(",");
			for (String s : split) {
				if(StringUtils.isNotEmpty(s.trim())) {
					Grupo subgrupo = grupoService.get(Long.parseLong(s.trim()));
					if(subgrupo != null) {
						subgruposRemove.add(subgrupo);
					} else {
						return Response.ok(MessageResult.error("Grupo à ser deletado inexistente")).build();
					}
				}
			}
		}
		
		String msg = "";
		if(subgruposToSave.size() > 0) {
			grupoService.saveSubgruposToGrupo(getUserInfo(), grupo, new ArrayList<Grupo>(subgruposToSave));
			msg = "Subgrupos adicionado ao grupo com sucesso";
		}
		
		if(subgruposRemove.size() > 0) {
			grupoService.removeSubgruposToGrupo(getUserInfo(), grupo, subgruposRemove);
			msg = "Subgrupos removido do grupo com sucesso";
		}
		
		if(subgruposRemove.size() > 0 && subgruposToSave.size() > 0) {
			msg = "Grupo modificado com sucesso";
		}
		
		return Response.ok(MessageResult.ok(msg)).build();
	}
	
	@GET
	@Path("/exportar")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response exportar() throws IOException {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}

		Empresa empresa = getEmpresa();
		String csv = grupoService.csvGrupos(empresa);
		
		String charset = ParametrosMap.getInstance(empresa).get(Params.EXPORTACAO_ARQUIVO_CHARSET, "ISO-8859-1");

		String attachament = "attachment; filename=" + URLEncoder.encode("grupos.csv", "UTF-8");
		ResponseBuilder b = Response.status(Status.OK);
		b.encoding(charset);
		b.header("Content-Disposition", attachament);
		b.header("Content-Type", "application/octet-stream"+";charset="+charset);
		b.entity(csv);
		Response r = b.build();
		return r;
	}
	
	@PUT
	@Path("/favorito")
	public Response favorito(@FormParam("usuarioId") Long usuarioId, @FormParam("grupoId") Long grupoId, @FormParam("favorito") boolean favorito) throws DomainException {
		
		try {
			if(grupoId == null || usuarioId == null) {
				log.debug("Não foi possivel definir este grupo como favorito");
				return Response.ok(MessageResult.error("Não foi possivel definir este grupo como favorito")).build();
			}
			
			Usuario usuario = usuarioService.get(usuarioId);
			Grupo grupo = grupoService.get(grupoId);
			
			GrupoUsuarios grupoUsuario = usuarioService.getGrupoUsuario(usuario, grupo);
			
			if(grupoUsuario == null) {
				log.debug("Não foi possivel definir este grupo ["+ grupo.getNome() +"] como favorito");
				return Response.ok(MessageResult.error("Não foi possivel definir este grupo ["+ grupo.getNome() +"] como favorito")).build();
			}
			
			grupoUsuario.setFavorito(favorito);
			
			usuarioService.saveOrUpdate(grupoUsuario);
			
			log.debug(usuario.getNome() + " alterou o status de favorito do grupo ["+ grupo.getNome() +"]");
			return Response.ok(MessageResult.ok("As definições de grupo  ["+ grupo.getNome() +"] favorito foram alteradas com sucesso.")).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel definir este grupo como favorito")).build();
		}
	}
	
	@PUT
	@Path("/participar")
	public Response participar(@FormParam("usuarioId") Long usuarioId, @FormParam("grupoId") Long grupoId, @FormParam("participar") boolean participar) throws DomainException {
		
		try {
			if(grupoId == null || usuarioId == null) {
				log.debug("Não foi possivel participar deste grupo, ids null");
				setError("Não foi possivel participar deste grupo");
				return Response.ok(MessageResult.error("Não foi possivel participar deste grupo")).build();
			}
			
			Usuario usuario = usuarioService.get(usuarioId);
			Grupo grupo = grupoService.get(grupoId);
			
			boolean isAdmin = grupoService.isAdminOrSubadminGrupo(usuario, grupo);
			GrupoUsuarios gu = usuarioService.getGrupoUsuario(usuario, grupo);
			//Valida grupo usuario porque existem grupos em que o admin não esta em grupo usuarios
			if(isAdmin && gu != null) {
				log.debug("[" + usuario.getNome() + "] é o admin e não pode sair do grupo ["+ grupo.getNome() +"]");
				setError("Você é o admin e não pode sair do grupo ["+ grupo.getNome() +"]");
				return Response.ok(MessageResult.error("Você é o admin e não pode sair do grupo ["+ grupo.getNome() +"]")).build();
			}
			
			if(grupo.getTipoGrupo().equals(TipoGrupo.PRIVADO) && !isAdmin && gu != null)  {
				log.debug("[" + usuario.getNome() + "] solicitou sair de um grupo privado ["+ grupo.getNome() +"]");
				setError("Você não pode sair do grupo ["+ grupo.getNome() +"] pois ele é privado");
				return Response.ok(MessageResult.error("Você não pode sair do grupo ["+ grupo.getNome() +"] pois ele é privado")).build();
			} 

			GrupoUsuarios grupoUsuario = grupoService.participar(usuario, grupo, participar);
			
			if(grupoUsuario == null) {
				log.debug("[" + usuario.getNome() + "] deixou de participar do grupo ["+ grupo.getNome() +"]");
				setMsg("Você deixou de participar do grupo ["+ grupo.getNome() +"]");
				return Response.ok(MessageResult.ok("Você deixou de participar do grupo ["+ grupo.getNome() +"]")).build();
			}
			
			if(grupo.getTipoGrupo().equals(TipoGrupo.ABERTO)) {
				log.debug("[" + usuario.getNome() + "] passou a participar do grupo ["+ grupo.getNome() +"]");
				setMsg("Você passou a participar do grupo ["+ grupo.getNome() +"]");
				return Response.ok(MessageResult.ok("Você passou a participar do grupo ["+ grupo.getNome() +"]")).build();
			} else {
				log.debug("[" + usuario.getNome() + "] solicitou participação no grupo ["+ grupo.getNome() +"]");
				setMsg("Você solicitou participação no grupo ["+ grupo.getNome() +"]");
				return Response.ok(MessageResult.ok("Você solicitou participação no grupo ["+ grupo.getNome() +"]")).build();
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			setError("Não foi possivel participar deste grupo");
			return Response.ok(MessageResult.error("Não foi possivel participar deste grupo")).build();
		}
	}

	@PUT
	@Path("/participar/aprovar")
	public Response aprovacao(@FormParam("usuarioId") Long usuarioId, @FormParam("grupoId") Long grupoId, @FormParam("aprovar") boolean aprovar) throws DomainException {
		
		try {
			if(grupoId == null || usuarioId == null) {
				log.debug("Não foi possivel participar deste grupo, ids null");
				return Response.ok(MessageResult.error("Não foi possivel aprovar a participação do usuario neste grupo")).build();
			}
			
			Usuario usuario = usuarioService.get(usuarioId);
			Grupo grupo = grupoService.get(grupoId);
			
			//permissão
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS) && !grupoService.isAdminOrSubadminGrupo(getUserInfo(), grupo)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}

			GrupoUsuarios grupoUsuario = usuarioService.getGrupoUsuario(usuario, grupo);
			
			if(grupoUsuario == null) {
				log.debug("Não foi possivel aprovar a participação do usuario [" + usuario.getNome() + "] neste grupo ["+ grupo.getNome() +"]");
				return Response.ok(MessageResult.error("Não foi possivel aprovar a participação do usuario neste grupo ["+ grupo.getNome() +"]")).build();
			}
			Usuario admin = grupo.getAdmin();
			if(aprovar) {
				grupoUsuario.setStatusParticipacao(StatusParticipacao.APROVADA);
				grupoService.criarNotificacao(grupoUsuario, TipoNotificacao.GRUPO_USUARIO_ACEITO);
			} else {
				// não aprovar participação do usuario
				usuarioService.deleteGrupoUsuarios(grupo, usuario);
				List<UsuarioVO> users = grupoService.findUsuariosByStatusParticipacao(StatusParticipacao.SOLICITADA, grupo);
				
				TipoGrupo tipo = grupo.getTipoGrupo();
				if(tipo.equals(TipoGrupo.ABERTO_FECHADO)) {
					
					notificationService.markSolicitacaoAsRead(grupo, admin);
				}
				grupoService.criarNotificacao(grupoUsuario, TipoNotificacao.GRUPO_USUARIO_RECUSADO);
				log.debug("Você recusou a participação do usuario [" + usuario.getNome() + "] no grupo ["+ grupo.getNome() +"]");
				return Response.ok(MessageResult.ok("Você recusou a participação do usuario no grupo ["+ grupo.getNome() +"]", users)).build();
			}
			
			usuarioService.saveOrUpdate(grupoUsuario);
			notificationService.markSolicitacaoAsRead(grupo, admin);
			
			List<UsuarioVO> users = grupoService.findUsuariosByStatusParticipacao(StatusParticipacao.SOLICITADA, grupo);
			log.debug("Você aprovou a participação do usuario [" + usuario.getNome() + "] no grupo ["+ grupo.getNome() +"]");
			return Response.ok(MessageResult.ok("Você aprovou a participação do usuario no grupo ["+ grupo.getNome() +"]", users)).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel aprovar a participação do usuario neste grupo")).build();
		}
	}
	
	@GET
	@Path("/aprovar/usuarios/{id}")
	public Response usuarios(@PathParam("id") Long id) throws DomainException {
		
		try {
			if(id == null) {
				return Response.ok(MessageResult.error("Não foi possivel verificar os usuarios que solicitaram participação no grupo")).build();
			}
			
			Grupo grupo = grupoService.get(id);
			
			List<UsuarioVO> users = grupoService.findUsuariosByStatusParticipacao(StatusParticipacao.SOLICITADA, grupo);
			
			return Response.ok(MessageResult.ok(users)).build();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel verificar os usuarios que solicitaram participação no grupo")).build();
		}
	}
	
	@POST
	@Path("/configuracoes")
	public Response configuracoes(ConfiguracoesVO configuracoes) {
		if(configuracoes.grupoId == null || configuracoes.userId == null) {
			return Response.ok().entity(MensagemResult.erro("Usuário ou grupo não informado")).build();
		}
		Usuario usuario = usuarioService.get(configuracoes.userId);
		if(usuario == null) {
			return Response.ok().entity(MensagemResult.erro("Usuário não encontrado")).build();
		}
		Grupo grupo = grupoService.get(configuracoes.grupoId);
		if(grupo == null) {
			return Response.ok().entity(MensagemResult.erro("Grupo não encontrado")).build();
		}
		
		GrupoUsuarios grupoUsuario = usuarioService.getGrupoUsuario(usuario, grupo);
		if(grupoUsuario == null) {
			return Response.ok().entity(MensagemResult.erro("Você não participa deste grupo")).build();
		}
		
		if(configuracoes.pushOn != null) {
			grupoUsuario.setPush(configuracoes.pushOn);
		}
		
		if(configuracoes.mostraMuralOn != null) {
			grupoUsuario.setMostraMural(configuracoes.mostraMuralOn);
		}
		usuarioService.saveOrUpdate(grupoUsuario);
		return Response.ok().entity(MensagemResult.ok("Configurações alteradas com sucesso")).build();
	}
	
	@PUT
	@Path("/change/admin")
	public Response changeAdmin(@FormParam("id") Long id, @FormParam("admin") Long admin) throws DomainException {
		
		try {
			if(id == null || admin == null) {
				log.debug("Não foi possivel alterar o admin deste grupo, ids null");
				return Response.ok(MessageResult.error("Não foi possivel alterar o admin deste grupo")).build();
			}
			
			Usuario usuario = usuarioService.get(admin);
			Grupo grupo = grupoService.get(id);
			
			GrupoUsuarios grupoUsuario = usuarioService.getGrupoUsuario(usuario, grupo);
			
			if(grupoUsuario == null) {
				log.debug("Não foi possivel alterar o admin do grupo ["+ grupo.getNome() +"] pois o usuario [" + usuario.getNome() + "] não participa do grupo");
				return Response.ok(MessageResult.error("Não foi possivel alterar o admin do grupo ["+ grupo.getNome() +"] pois o usuario [" + usuario.getNome() + "] não participa do grupo")).build();
			}
			
			grupo.setUsuario(usuario);
			grupo.setUserCreate(usuario);
			grupoService.saveOrUpdate(usuario, grupo);
			
			log.debug("[" + usuario.getNome() + "] agora é administrador do grupo ["+ grupo.getNome() +"]");
			return Response.ok(MessageResult.ok("[" + usuario.getNome() + "] agora é administrador do grupo ["+ grupo.getNome() +"]")).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel alterar o admin do grupo")).build();
		}
	}
	
	@POST
	@Path("/virtual/subgrupos")
	public Response virtuais(@FormParam("nome") String nome, @FormParam("page") int page, @FormParam("max") int max, @FormParam("not_ids") String notIds) throws DomainException {
		
		try {
			
			Filtro filtro = new Filtro();
			filtro.nome = nome;
			filtro.page = page;
			filtro.max = max;
			filtro.notIds = notIds;
			
			ParametrosMap params = ParametrosMap.getInstance(getEmpresa());
			boolean tipoOn = params.getBoolean(Params.CADASTRO_GRUPO_TIPO_ON, false);
			
			filtro.ids = getListIds(filtro.notIds);
			
			List<Grupo> subgrupos = new ArrayList<Grupo>();
			if(tipoOn) {
				subgrupos = grupoService.findAllSubgruposWithAcesso(filtro, getUserInfo());
			} else {
				subgrupos = grupoService.findAllSubgrupos(filtro, getEmpresa());
			}
			
			List<GrupoVO> vos = new ArrayList<GrupoVO>();
			for (Grupo  g : subgrupos) {
				GrupoVO vo = grupoService.setGrupo(g);
				long count = 0L;
				if(g.isGrupoVirtual()) {
					count = grupoService.countSubgrupos(g);
					vo.countSubgrupos = count;
				} 
				count = grupoService.countUsers(g);
				vo.countUsers = count;
				vos.add(vo);
			}
			
			return Response.ok(MessageResult.ok(vos)).build();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel verificar os grupos não virtuais")).build();
		}
	}
	
	@PUT
	@Path("/user/permissao")
	public Response permissao(@FormParam("userId") Long userId, @FormParam("grupoId") Long grupoId, @FormParam("postar") boolean postar) throws DomainException {
		
		try {
			
			if(userId == null || grupoId == null) {
				log.debug("Não foi possivel alterar a permissão de postagem deste participante");
				return Response.ok(MessageResult.error("Não foi possivel alterar a permissão de postagem deste participante")).build();
			}
			
			Usuario usuario = usuarioService.get(userId);
			Grupo grupo = grupoService.get(grupoId);
			
			//permissão
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS) && !grupoService.isAdminOrSubadminGrupo(getUserInfo(), grupo)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}

			GrupoUsuarios grupoUsuario = usuarioService.getGrupoUsuario(usuario, grupo);
			
			if(grupoUsuario == null) {
				usuarioService.addGrupoToUser(usuario, grupo, postar);
				log.debug("Usuario [" + usuario.getNome() + "] inserido no grupo ["+ grupo.getNome() +"] com permissão de postagem");
				return Response.ok(MessageResult.ok("Usuario [" + usuario.getNome() + "] inserido no grupo ["+ grupo.getNome() +"] com permissão de postagem")).build();
			}
			
			grupoUsuario.setPostar(postar);
			usuarioService.saveOrUpdate(grupoUsuario);
			
			if(postar) {
				log.debug("[" + usuario.getNome() + "] agora pode publicar no grupo ["+ grupo.getNome() +"]");
				return Response.ok(MessageResult.ok("[" + usuario.getNome() + "] agora pode publicar no grupo ["+ grupo.getNome() +"]")).build();
			}
			log.debug("[" + usuario.getNome() + "] não pode mais publicar no grupo ["+ grupo.getNome() +"]");
			return Response.ok(MessageResult.ok("[" + usuario.getNome() + "] não pode mais publicar no grupo ["+ grupo.getNome() +"]")).build();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel alterar a permissão de postagem")).build();
		}
	}

	@PUT
	@Path("/user/subadmin")
	public Response subadmin(@FormParam("userId") Long userId, @FormParam("grupoId") Long grupoId, @FormParam("admin") boolean admin) throws DomainException {

		try {

			if(userId == null || grupoId == null) {
				log.debug("Não foi possivel alterar a permissão de subadmin deste participante");
				return Response.ok(MessageResult.error("Não foi possivel alterar a permissão de subadmin deste participante")).build();
			}

			Usuario usuario = usuarioService.get(userId);
			Grupo grupo = grupoService.get(grupoId);

			GrupoUsuarios grupoUsuario = usuarioService.getGrupoUsuario(usuario, grupo);

			if(grupoUsuario == null) {
				usuarioService.addSubadmin(usuario, grupo, admin);
				log.debug("Usuario [" + usuario.getNome() + "] inserido no grupo ["+ grupo.getNome() +"] com permissão de subadmin");
				return Response.ok(MessageResult.ok("Usuario [" + usuario.getNome() + "] inserido no grupo ["+ grupo.getNome() +"] com permissão de subadmin")).build();
			}

			grupoUsuario.setAdmin(admin);
			usuarioService.saveOrUpdate(grupoUsuario);

			if(admin) {
				log.debug("[" + usuario.getNome() + "] agora pode gerenciar o grupo ["+ grupo.getNome() +"]");
				return Response.ok(MessageResult.ok("[" + usuario.getNome() + "] agora pode gerenciar o grupo ["+ grupo.getNome() +"]")).build();
			}
			log.debug("[" + usuario.getNome() + "] não pode mais gerenciar o grupo ["+ grupo.getNome() +"]");
			return Response.ok(MessageResult.ok("[" + usuario.getNome() + "] não pode mais gerenciar o grupo ["+ grupo.getNome() +"]")).build();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel alterar a permissão de subadmin")).build();
		}
	}

	@PUT
	@Path("/postar")
	public Response permissaoPostar(@FormParam("grupoId") Long grupoId, @FormParam("postar") boolean postar) throws DomainException {
		
		try {
			
			if(grupoId == null) {
				log.debug("Não foi possivel alterar a permissão de postagem deste grupo");
				return Response.ok(MessageResult.error("Não foi possivel alterar a permissão de postagem deste grupo")).build();
			}
			
			Grupo grupo = grupoService.get(grupoId);
			
			if(grupo == null) {
				log.debug("Não foi possivel alterar a permissão de postagem deste grupo");
				return Response.ok(MessageResult.error("Não foi possivel alterar a permissão de postagem deste grupo")).build();
			}
			
			//permissão
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS) && !grupoService.isAdminOrSubadminGrupo(getUserInfo(), grupo)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}

			grupo.setTodosPostam(postar);
			if(!postar) {
				grupoService.changePermissaoPostarUsers(grupo, postar);
			}
			grupoService.saveOrUpdate(getUserInfo(), grupo);
			
			log.debug("Permissão de postagem alterada para o grupo ["+ grupo.getNome() +"]");
			return Response.ok(MessageResult.ok("Permissão de postagem alterada para o grupo ["+ grupo.getNome() +"]")).build();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel alterar a permissão de postagem")).build();
		}
	}
	
	@GET
	@Path("/subgrupos/{id}")
	public Response subgruposByGrupo(@PathParam("id") Long id) throws DomainException {
		
		try {
			
			if(id == null) {
				log.debug("Não foi possivel encontrar os subgrupos deste grupo");
				return Response.ok(MessageResult.error("Não foi possivel encontrar os subgrupos deste grupo")).build();
			}
			
			Grupo grupo = grupoService.get(id);
			
			if(grupo == null) {
				log.debug("Não foi possivel encontrar os subgrupos deste grupo");
				return Response.ok(MessageResult.error("Não foi possivel encontrar os subgrupos deste grupo")).build();
			}
			
			//permissão
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS) && !grupoService.isAdminOrSubadminGrupo(getUserInfo(), grupo)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}

			List<Grupo> subgrupos = grupoService.findSubgruposByGrupo(grupo);
			List<GrupoVO> vos = GrupoVO.toList(subgrupos);
			
			return Response.ok(MessageResult.ok(vos)).build();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel verificar os grupos não virtuais")).build();
		}
	}
	
	@GET
	@Path("/subadmins/{id}")
	public Response subadminsByGrupo(@PathParam("id") Long id) throws DomainException {

		try {

			if(id == null) {
				log.debug("Não foi possivel encontrar os subadmins deste grupo");
				return Response.ok(MessageResult.error("Não foi possivel encontrar os subadmins deste grupo")).build();
			}

			Grupo grupo = grupoService.get(id);

			if(grupo == null) {
				log.debug("Não foi possivel encontrar os subadmins deste grupo");
				return Response.ok(MessageResult.error("Não foi possivel encontrar os subadmins deste grupo")).build();
			}

			List<Usuario> usuarios = grupoService.findSubadminsByGrupo(grupo);
			List<UsuarioVO> vos = UsuarioVO.setUsuarios(usuarios);

			return Response.ok(MessageResult.ok(vos)).build();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel verificar os subadmins")).build();
		}
	}

	@GET
	@Path("/mural/header/{id}")
	public Response grupoHeader(@PathParam("id") Long id) {
		try {
			if(id == null) {
				return Response.ok(MessageResult.error("Não foi possivel encontrar este grupo")).build();
			}
			Grupo grupo = grupoService.get(id);
			if(grupo == null) {
				return Response.ok(MessageResult.error("Não foi possivel encontrar este grupo")).build();
			}
			
			GrupoVO grupoVO = new GrupoVO();
			grupoVO.setGrupo(grupo);
			grupoVO.countUsers = grupoService.countUsers(grupo);
			grupoVO.countSubgrupos = grupoService.countSubgrupos(grupo);
			
			GrupoUsuarios gu = grupoService.findByGrupoEUsuario(grupo, getUserInfo());
			if(gu != null) {
				grupoVO.favorito = gu.isFavorito();
				grupoVO.participa = true;
				grupoVO.statusSolicitacao = gu.getStatusParticipacaoString();
				grupoVO.postar = grupo.isTodosPostam() || gu.isPostar();
			} else {
				grupoVO.favorito = false;
				grupoVO.participa = false;
				grupoVO.statusSolicitacao = "";
				grupoVO.postar = grupo.isGrupoVirtual() && grupo.isTodosPostam();
			}
			return Response.ok(MessageResult.ok(grupoVO)).build();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel verificar os grupos não virtuais")).build();
		}
	}
}
	
