package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.StatusChat;

public class ChatStatusVO {
	private Long id;
	private String nome;
	private Long userId;
	private boolean chatOn;
	private boolean away;
	private String dataLogin;
	private String dataUltTransacao;
	private String dataLoginChat;
	private String dataUltChat;
	
	private String tipoLogin;
	private List<UserInfoSession> sessions;

	public ChatStatusVO() {
		
	}
	
	public ChatStatusVO(StatusChat statusChat) {
		copyFrom(statusChat);
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public void setChatOn(boolean chatOn) {
		this.chatOn = chatOn;
	}
	
	public boolean isChatOn() {
		return chatOn;
	}

	public void copyFrom(StatusChat s) {
		if(s == null) {
			// Logado mas nunca alterou o status
			this.setNome("Disponivel");
		} else {
			this.id = s.getId();
			this.nome = s.getNome();
			this.userId = s.getUsuario() != null ? s.getUsuario().getId() : null;
		}
	}

	public static List<ChatStatusVO> create(List<StatusChat> list) {
		List<ChatStatusVO> vos = new ArrayList<ChatStatusVO>();
		for (StatusChat s : list) {
			ChatStatusVO e = new ChatStatusVO();
			e.copyFrom(s);
			vos.add(e);
		}
		return vos;
	}
	
	public String getDataLogin() {
		return dataLogin;
	}
	
	public String getDataUltTransacao() {
		return dataUltTransacao;
	}
	
	public void setDataLogin(String dataLogin) {
		this.dataLogin = dataLogin;
	}
	
	public void setDataUltTransacao(String dataUltTransacao) {
		this.dataUltTransacao = dataUltTransacao;
	}
	
	public void setDataLoginChat(String dataLoginChat) {
		this.dataLoginChat = dataLoginChat;
	}
	
	public String getDataLoginChat() {
		return dataLoginChat;
	}
	
	public String getDataUltChat() {
		return dataUltChat;
	}
	
	public void setDataUltChat(String dataUltChat) {
		this.dataUltChat = dataUltChat;
	}
	
	public void setSessions(List<UserInfoSession> sessions) {
		this.sessions = sessions;
	}
	
	public List<UserInfoSession> getSessions() {
		return sessions;
	}
	
	public void setTipoLogin(String tipoLogin) {
		this.tipoLogin = tipoLogin;
	}
	
	public String getTipoLogin() {
		return tipoLogin;
	}
	
	public void setAway(boolean away) {
		this.away = away;
	}
	public boolean isAway() {
		return away;
	}

	@Override
	public String toString() {
		return "ChatStatusVO [id=" + id + ", nome=" + nome + ", userId=" + userId + ", chatOn=" + chatOn + "]";
	}
	
}
