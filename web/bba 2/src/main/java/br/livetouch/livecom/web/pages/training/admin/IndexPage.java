package br.livetouch.livecom.web.pages.training.admin;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * 
 */
@Controller
@Scope("prototype")
public class IndexPage extends TrainingAdminPage {
	@Override
	public void onGet() {
		super.onGet();
		
		setRedirect(PostsPage.class);
	}
}
