package br.livetouch.livecom.rest.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Campo;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.vo.CampoVO;
import br.livetouch.livecom.domain.vo.CamposMapCache;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/campos")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class CampoResource extends MainResource {
	protected static final Logger log = Log.getLogger(CampoResource.class);
	
	@POST
	public Response create(Campo campo) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Response result = existe(campo);
			if(result != null){
				return result;
			}
			campoService.saveOrUpdate(campo, campo.getEmpresa());
			CampoVO campoVO = new CampoVO();
			campoVO.setCampo(campo);
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", campoVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel realizar o cadastro do campo " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel realizar o cadastro do campo " + e.getMessage())).build();
		}
	}
	
	@PUT
	public Response update(Campo campo) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		if(campo == null || campo.getId() == null) {
			return Response.ok(MessageResult.error("Campo inexistente")).build();
		}
		
		try {
			campoService.saveOrUpdate(campo, campo.getEmpresa());
			CampoVO campoVO = new CampoVO();
			campoVO.setCampo(campo);
			return Response.ok(MessageResult.ok("Campo atualizado com sucesso", campoVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel atualizar o cadastro do campo " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar o cadastro do campo " + e.getMessage())).build();
		}
	}
	
	@GET
	@Path("/all/{id}")
	public Response findAll(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		List<Campo> campos = CamposMapCache.getInstance().getAll();
		Map<Long, Campo> map = new HashMap<Long, Campo>();
		for (Campo campo : campos) {
			map.put(campo.getId(), campo);
		}
		campos.clear();
		campos = CamposMapCache.getInstance(id).getAll();
		for (Campo campo : campos) {
			map.put(campo.getId(), campo);
		}
		List<CampoVO> vos = CampoVO.fromList(new ArrayList<>(map.values()));
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Campo campo = campoService.get(id);
		if(campo == null) {
			return Response.ok(MessageResult.error("Campo não encontrado")).build();
		}
		CampoVO campoVO = new CampoVO();
		campoVO.setCampo(campo);
		return Response.ok().entity(campoVO).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Campo campo = campoService.get(id);
		if(campo == null) {
			return Response.ok(MessageResult.error("Campo não encontrado")).build();
		}
		try {
			campoService.delete(campo, getEmpresa());
			return Response.ok().entity(MessageResult.ok("Campo deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error("Não foi possível excluir o campo id: " + id + " ERRO:" + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Campo  " + id)).build();
		}
	}
	
	public Response existe(Campo campo) {
		
		if(campoService.findByNameValid(campo, campo.getEmpresa()) != null) {
			log.debug("Ja existe uma campo com este nome");
			return Response.ok(MessageResult.error("Ja existe uma campo com este nome")).build();
		}

		return null;
	}
	
}
