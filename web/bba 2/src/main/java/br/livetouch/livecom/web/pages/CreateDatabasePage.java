package br.livetouch.livecom.web.pages;

import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.admin.LivecomPage;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.PasswordField;
import net.sf.click.control.Submit;

/**
 * 
 */
@Controller
@Scope("prototype")
public class CreateDatabasePage extends LivecomPage {

	public Form form = new Form();
	private PasswordField tSenha;

	@Override
	public void onInit() {
		super.onInit();
		form();
	}

	private void form() {
		Submit tcreate = new Submit("create", this, "create");
		tcreate.setAttribute("class", "botao");
		form.add(tcreate);
		
		tSenha = new PasswordField("senha", true);
		tSenha.addStyleClass("input");
		form.add(tSenha);
	}

	public boolean create() {
		try {
			if (form.isValid()) {
				String senha = tSenha.getValue();
				if (StringUtils.equals(senha, "livetouch@2013")) {
					createDatabase();
					setFlashAttribute("msg", "Banco populado com sucesso.");
				} else {
					setFlashAttribute("msg", "Senha inválida.");
				}
			} else {
				setFlashAttribute("msg", "Informe a senha.");
			}

		} catch (Exception e) {
			logError("Erro ao criar o banco: " + e.getMessage(), e);
		}
		return false;
	}

	private void createDatabase() throws DomainException {
		Empresa e = new Empresa();
		// e.setId(1L);
		e.setNome("Livecom");
		empresaService.saveOrUpdate(e);

		Grupo g = new Grupo();
		// g.setId(1L);
		g.setNome("Admin");
		grupoService.saveOrUpdate(null, g);

		// g.setId(5L);
		Grupo g2 = new Grupo();
		g2.setNome("Geral");
		// g2.setPadrao(true);
		grupoService.saveOrUpdate(null, g2);

		CategoriaPost c = new CategoriaPost();
		// c.setId(1L);
		c.setNome("Geral");
		c.setPadrao(true);
		categoriaPostService.saveOrUpdate(getUsuario(), c);

		Perfil uP = new Perfil();
		// uP.setId(1L);
		uP.setNome("admin");
		uP.setDescricao("admin");
		perfilService.saveOrUpdate(uP);

		Perfil uP2 = new Perfil();
		// uP.setId(2L);
		uP2.setNome("user");
		uP2.setDescricao("user");
		perfilService.saveOrUpdate(uP2);

		Perfil uP3 = new Perfil();
		// uP.setId(3L);
		uP3.setNome("root");
		uP3.setDescricao("root");
		perfilService.saveOrUpdate(uP3);

		Perfil p = new Perfil();
		// p.setId(1L);
		p.setComentar(true);
		p.setCurtir(true);
		p.setDescricao("cadastrar novos usuários, publicar postagens, apagar postagens, comentar, curtir e enviar mensagens");
		p.setEnviarMensagem(true);
		p.setNome("Admin");
		p.setPublicar(true);
		p.setExcluirPostagem(true);
		p.setCadastrarUsuarios(true);
		p.setEditarPostagem(true);
		p.setArquivos(true);
		p.setPadrao(false);
		p.setCadastrarTabelas(true);
		perfilService.saveOrUpdate(p);

		Perfil p2 = new Perfil();
		// p.setId(2L);
		p2.setComentar(true);
		p2.setCurtir(true);
		p2.setDescricao("comentar, curtir e enviar mensagens");
		p2.setEnviarMensagem(true);
		p2.setNome("Usuário");
		p2.setPublicar(true);
		p2.setExcluirPostagem(false);
		p2.setCadastrarUsuarios(false);
		p2.setEditarPostagem(false);
		p2.setArquivos(false);
		p2.setPadrao(true);
		p2.setCadastrarTabelas(false);
		perfilService.saveOrUpdate(p2);

		Perfil p3 = new Perfil();
		// p.setId(3L);
		p3.setComentar(false);
		p3.setCurtir(false);
		p3.setDescricao("visualizar conteúdo");
		p3.setEnviarMensagem(false);
		p3.setNome("Visualização");
		p3.setPublicar(false);
		p3.setExcluirPostagem(false);
		p3.setCadastrarUsuarios(false);
		p3.setEditarPostagem(false);
		p3.setArquivos(false);
		p3.setPadrao(false);
		p3.setCadastrarTabelas(false);
		perfilService.saveOrUpdate(p3);

		Usuario u = new Usuario();
		// u.setId(1L);
		u.setEmail("admin@livetouch.com.br");
		u.setLogin("admin@livetouch.com.br");
		u.setNome("admin");
		u.setSenha("admin");
		Perfil permissao = perfilService.get(1L);
		u.setPermissao(permissao);

		Empresa empresa = empresaService.get(1L);
		u.setEmpresa(empresa);
		usuarioService.saveOrUpdate(getUsuario(), u);

		Usuario u2 = new Usuario();
		// u.setId(2L);
		u2.setEmail("root@livetouch.com.br");
		u2.setLogin("root@livetouch.com.br");
		u2.setNome("root");
		u2.setSenha("root");
		permissao = perfilService.get(3L);
		u2.setPermissao(permissao);
		u2.setEmpresa(empresa);
		usuarioService.saveOrUpdate(getUsuario(), u2);

		Usuario u3 = new Usuario();
		// u.setId(3L);
		u3.setEmail("rlecheta@livetouch.com.br");
		u3.setLogin("rlecheta@livetouch.com.br");
		u3.setNome("Ricardo");
		u3.setSenha("ricardo");
		permissao = perfilService.get(2L);
		u3.setPermissao(permissao);
		u3.setEmpresa(empresa);
		usuarioService.saveOrUpdate(getUsuario(), u3);
		
		Usuario u4 = new Usuario();
		// u.setId(3L);
		u4.setEmail("marcio@livetouch.com.br");
		u4.setLogin("marcio@livetouch.com.br");
		u4.setNome("Marcio");
		u4.setSenha("marcio");
		permissao = perfilService.get(2L);
		u4.setPermissao(permissao);
		u4.setEmpresa(empresa);
		usuarioService.saveOrUpdate(getUsuario(), u4);

		GrupoUsuarios gU = new GrupoUsuarios();
		Usuario usuario = usuarioService.get(1L);
		gU.setUsuario(usuario);
		usuarioService.saveOrUpdate(gU);

		usuario = usuarioService.get(2L);
		gU.setUsuario(usuario);
		usuarioService.saveOrUpdate(gU);

		usuario = usuarioService.get(3L);
		gU.setUsuario(usuario);
		usuarioService.saveOrUpdate(gU);

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("file.manager.amazon.server", "https://s3-sa-east-1.amazonaws.com");
		params.put("file.manager.amazon.credentials", "credentials_itau_revista_s3.csv");
		params.put("chat_on", "0");
		params.put("file.manager.amazon.bucket", "livecom-itau-revista");
		params.put("server.host", "itaurevista.livetouchdev.com.br");
		params.put("push.server.on", "1");
		params.put("push.newPost.onlyLoggedUsers", "0");
		params.put("push.newPost.allUsersFromGroup", "1");
		params.put("push.server.host", "push.livetouchdev.com.br");
		params.put("push.server.project", "LivecomItau");
		params.put("buildType", "itau");
		params.put("ws.security.on", "0");
		params.put("web_template_chat_painel_on", "0");
		params.put("js.version", "1");
		params.put("web.cookie.maxAge", "2592000");
		params.put("importacao.grupo_id", "2");
		params.put("importacao.arquivoUsuario", "usuarios.csv");
		params.put("importacao.permissao_id", "3");
		params.put("importacao.ligado", "0");
		params.put("pastaEntrada", "/livecom/in");
		params.put("email.smtp_server", "email-smtp.us-east-1.amazonaws.com");
		params.put("email.smtp_password", "AieXbWqtVbQ8Et3xcyeIKkkYIrrDjon5+KZeacjuYfP2");
		params.put("email.smtp_user", "AKIAJYT22PIFZ25K6R5Q");
		params.put("infra.saveLogTransacao", "1");

		for (String key : params.keySet()) {
			String value = params.get(key);
			Parametro param = new Parametro();
			param.setNome(key);
			param.setValor(value);
			parametroService.saveOrUpdate(param);
		}

	}
}
