package br.livetouch.livecom.web.pages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.admin.LivecomPage;

@Controller
@Scope("prototype")
public class TestConnectionPage extends LivecomPage {

	private Connection conn;

	@Override
	public void onInit() {
		super.onInit();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			DataSource ds = getDataSource();
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select count(*) from usuario");
			if(rs.next()) {
				int count = rs.getInt(1);
				msg = "Connection OK. Count users: " + count;
			} else {
				msg = "Connection OK. Sem registros.";
			}
		} catch(Exception e) {
			e.printStackTrace();
			msg = e.getMessage() + " - " + e.getClass();
		} finally {
			JdbcUtils.closeResultSet(rs);
			JdbcUtils.closeStatement(stmt);
			JdbcUtils.closeConnection(conn);
		}
	}
	
	@Override
	public String getTemplate() {
		return getPath();
	}
}
