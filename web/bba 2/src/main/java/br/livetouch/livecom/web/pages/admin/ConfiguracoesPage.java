package br.livetouch.livecom.web.pages.admin;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Campo;
import br.livetouch.livecom.web.pages.LivecomRootPage;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LinkDecorator;

/**
 * 
 */
@Controller
@Scope("prototype")
public class ConfiguracoesPage extends LivecomRootPage {

	public PaginacaoTable table = new PaginacaoTable();
	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public int page;
//	public int lastPage;

	public Campo campo;

	public List<Campo> campos;

	@Override
	public void onInit() {
		super.onInit();

		form();
		
		table();
	}
	
	private void table() {
		Column c = new Column("id",getMessage("codigo.label"));
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("nome");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		c = new Column("publico");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		c = new Column("dependeCampo");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		c = new Column("dependeCampoValor");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		c = new Column("editavel");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		c = new Column("obrigatorioCadastroAdmin");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes",getMessage("coluna.detalhes.label"));
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	public void form(){
		form.add(new IdField());

		TextField t = new TextField("nome");
		t.setAttribute("autocomplete", "off");
		form.add(t);
		
		t = new TextField("publico");
		t.setAttribute("autocomplete", "off");
		form.add(t);
		
		t = new TextField("dependeCampo");
		t.setAttribute("autocomplete", "off");
		form.add(t);
		
		t = new TextField("dependeCampoValor");
		t.setAttribute("autocomplete", "off");
		form.add(t);
		
		t = new TextField("editavel");
		t.setAttribute("autocomplete", "off");
		form.add(t);
		
		t = new TextField("obrigatorioCadastroAdmin");
		t.setAttribute("autocomplete", "off");
		form.add(t);

		if (id != null) {
			campo = campoService.get(id);
		} else {
			campo = new Campo();
		}

		Submit tsalvar = new Submit("salvar", this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);
		
		Submit cancelar = new Submit("novo",this,"novo");
		cancelar.setAttribute("class", "botao");
		form.add(cancelar);

		form.copyFrom(campo);
		
//		setFormTextWidth(form);
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				campo = id != null ? campoService.get(id) : new Campo();
				form.copyTo(campo);

				campoService.saveOrUpdate(campo, getEmpresa());

				setFlashAttribute("msg",  "Campo ["+campo.getNome()+"] salvo com sucesso.");
				setRedirect(ConfiguracoesPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(ConfiguracoesPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		campo = campoService.get(id);
		form.copyFrom(campo);
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Campo Campo = campoService.get(id);
			campoService.delete(Campo, getEmpresa());
			setRedirect(ConfiguracoesPage.class);
			setFlashAttribute("msg", "Campo deletado com sucesso.");
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = "Campo possui relacionamentos. Não pode ser excluído para manter o histórico.";
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		campos = campoService.findAll(getEmpresa());

		// Count(*)
		int pageSize = 10;
		long count = campos.size();

		createPaginator(page, count, pageSize);

		table.setPageSize(pageSize);
		table.setRowList(campos);
	}
}
