package br.livetouch.livecom.web.pages.report;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboAcao;
import br.infra.web.click.ComboTipoEntidade;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.extras.util.DateUtils;
import net.sf.click.control.Form;

@Controller
@Scope("prototype")
public class AuditoriaReportPage extends RelatorioPage {

	public Form form = new Form();
	public RelatorioFiltro filtro;
	public int page;
	public ComboTipoEntidade
	comboEntidade;
	public ComboAcao comboAcao;
	
	@Override
	public void onInit() {
		super.onInit();

		if (clear) {
			getContext().removeSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			filtro = new RelatorioFiltro();
			filtro.setDataIni(DateUtils.toString(br.livetouch.livecom.utils.DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
			filtro.setDataFim(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		} else {
			filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			if (filtro != null) {
				if (filtro.getUsuarioId() != null) {
					Usuario usuario = usuarioService.get(filtro.getUsuarioId());
					if (usuario != null) {
						filtro.setUsuario(usuario);
					}
				}
			}
		}
		
		form();
	}
	
	public void form() {
		
		form.add(comboEntidade = new ComboTipoEntidade());
		form.add(comboAcao= new ComboAcao());
		
	}

}
