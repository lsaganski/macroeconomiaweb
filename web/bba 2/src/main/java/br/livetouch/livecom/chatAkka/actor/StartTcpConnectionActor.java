package br.livetouch.livecom.chatAkka.actor;

import java.net.InetSocketAddress;

import org.apache.log4j.Logger;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.actor.UntypedActorContext;
import akka.io.Tcp;
import akka.io.Tcp.Command;
import akka.io.TcpMessage;
import br.infra.util.Log;

/**
 * Este ator abre o Socket e fica esperando novas conexões.
 * 
 * @author rlech
 *
 */
public class StartTcpConnectionActor extends UntypedActor {

	private static Logger log = Log.getLogger("TcpConnectionActor");

	/**
	 * Cria o StartTcpConnectionActor que fica esperando conexões TCP.
	 * 
	 * @param context
	 * @param port
	 * @param sender
	 */
	public static void start(UntypedActorContext context, int port, ActorRef sender) {
		ActorRef tcpActor = context.actorOf(Props.create(StartTcpConnectionActor.class), "tcpActor");
		tcpActor.tell(new StartTcpConnectionActor.Start(port), sender);		
	}
	
	public static class Start {
		public int port;

		public Start(int port) {
			this.port = port;
		}
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof Start) {
			// Faz start do AKKA server
			log.debug("Akka Server - Bind to socket:" + ((Start) msg).port);
			final ActorRef tcp = Tcp.get(getContext().system()).manager();

			tcp.tell(TcpMessage.bind(getSelf(), new InetSocketAddress(((Start) msg).port), 100), getSelf());

		} else if (msg instanceof Tcp.Connected) {
			// Cliente conectou o socket
			log.debug("Akka Server - Ator conectado: " + getSender());
			
			// Cria o ator para leitura TCP do Socket
			ActorRef tcpUserActor = TcpConnectionActor.create(getContext(),getSender());
			ActorRef connectionActor = getSender();
			Command register = TcpMessage.register(tcpUserActor);
			connectionActor.tell(register, getSelf());
			
		} else if (msg instanceof Tcp.CommandFailed) {
			log.error("Akka Server - Error to bind to port");
			getContext().stop(self());
		} else if (msg instanceof Tcp.ConnectionClosed) {
			System.err.println("Tcp.ConnectionClosed !!");
			log.error("Tcp.ConnectionClosed !!");
        } else {
			log.error("StartTcpConnectionActor ERROR! " + msg);
			System.err.println("StartTcpConnectionActor ERROR! " + msg);
		}
	}
}
