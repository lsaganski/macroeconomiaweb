package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Menu;
import br.livetouch.livecom.domain.MenuPerfil;

public class MenuPerfilVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public List<MenuPerfilVO> in;
	public List<MenuPerfilVO> filhos;
	public List<MenuPerfilVO> notIn;
	public List<MenuPerfilVO> filhosNotIn;
	
	public Long id;
	public int ordem;
	public MenuVO menu;
	public PerfilVO perfil;
		
	public MenuPerfilVO() {}

	public MenuPerfilVO(MenuPerfil menuPerfil) {
		this.id = menuPerfil.getId();
		this.ordem = menuPerfil.getOrdem();
		if(menuPerfil.getMenu() != null) {
			this.menu = new MenuVO(menuPerfil.getMenu());
		}
	}

	public MenuPerfilVO(Menu menu) {
		this.ordem = menu.getOrdem();
		this.menu = new MenuVO(menu);
	}
	
	public void addFilho(MenuPerfilVO filho) {
		if(this.filhos != null) {
			this.filhos.add(filho);
		} else {
			this.filhos = new ArrayList<MenuPerfilVO>();
			this.filhos.add(filho);
		}
	}

	public void addFilhoNotIn(MenuPerfilVO filho) {
		if(this.filhosNotIn != null) {
			this.filhosNotIn.add(filho);
		} else {
			this.filhosNotIn = new ArrayList<MenuPerfilVO>();
			this.filhosNotIn.add(filho);
		}
	}

}
