package br.livetouch.livecom.web.pages.training;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.AlterarSenhaVO;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.PasswordField;
import net.sf.click.control.Submit;

@Controller
@Scope("prototype")
public class AlterarSenhaPage extends TrainingLogadoPage {

	public Form form = new Form();

	public String msg;

	protected Submit btAlterar;

	@Override
	public void onInit() {
		super.onInit();
		
		PasswordField t = new PasswordField("senhaAtual", true);
		t.setFocus(true);
		form.add(t);

		t = new PasswordField("senhaNova", true);
		form.add(t);

		t = new PasswordField("confirmaSenha", true);
		form.add(t);

		btAlterar = new Submit("alterar", this, "alterar");
		btAlterar.setAttribute("class", "botao btSalvar");
		form.add(btAlterar);
		
		TrainingUtil.setTrainingStyle(form);
	}

	public boolean alterar() {
		try {
			if(form.isValid()) {
				Usuario usuario = getUsuario();

				AlterarSenhaVO alterar = new AlterarSenhaVO();
				form.copyTo(alterar);

				usuarioService.alterarSenha(usuario, alterar);

				msg = "Senha atualizada com sucesso.";
				
				setFlashAttribute("msg", "Senha atualizada com sucesso.");
				setRedirect(getClass());
			}
		} catch (DomainException e) {
			form.setError(e.getMessage());
		}

		return true;
	}


}
