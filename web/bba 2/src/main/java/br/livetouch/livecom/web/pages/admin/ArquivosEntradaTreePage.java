package br.livetouch.livecom.web.pages.admin;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.sf.click.extras.tree.Tree;
import net.sf.click.extras.tree.TreeNode;

@Controller
@Scope("prototype")
public class ArquivosEntradaTreePage extends LivecomAdminPage {
	public static final String TREE_NODES_SESSION_KEY = "treeNodes";

	protected Tree tree;

	public String entrada;
	
	public boolean clear;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();

		entrada = ParametrosMap.getInstance(getEmpresaId()).get(Params.PASTAENTRADA);

		tree = buildTree();
		addControl(tree);
		
		if(clear){
			clearSession();
		}
	}

	@Override
	public void onGet() {
		String selectId = getContext().getRequestParameter(Tree.SELECT_TREE_NODE_PARAM);
		String expandId = getContext().getRequestParameter(Tree.EXPAND_TREE_NODE_PARAM);
		if (selectId == null && expandId == null) {
			return;
		}

		TreeNode node = null;
		if (selectId != null) {
			node = tree.find(selectId);
		} else {
			node = tree.find(expandId);
		}
		if (!node.hasChildren()) {
			baixarArquivo(node);
		}
	}

	private void baixarArquivo(TreeNode node) {
		int level = node.getLevel();
		String entrada = ParametrosMap.getInstance(getEmpresaId()).get(Params.PASTAENTRADA);
		if (StringUtils.isEmpty(entrada)) {
			setMessageError(getMessage("pastaDeEntrada.error"));
		}
		String caminho = entrada;
		String aux = "";
		String nomeArquivo = node.getValue().toString();
		for (int i = 0; i < level; i++) {
			TreeNode parent = node.getParent();
			String p = parent.getValue().toString();
			if (StringUtils.equals(p, "root")) {
				break;
			}
			aux = p + "/" + aux;
			node = parent;
		}

		caminho += "/" + aux;

		downloadFile(new File(caminho + nomeArquivo));
	}

	protected Tree createTree() {
		return new Tree("tree");
	}

	protected Tree buildTree() {
		tree = createTree();

		TreeNode existingRootNode = loadNodesFromSession();

		if (existingRootNode != null) {
			tree.setRootNode(existingRootNode);
			return tree;
		}

		TreeNode root = createNodes();
		tree.setRootNode(root);
		tree.expand(root);
		storeNodesInSession(root);

		return tree;
	}

	protected TreeNode createNodes() {

		TreeNode root = new TreeNode("root");

		String entrada = ParametrosMap.getInstance(getEmpresaId()).get(Params.PASTAENTRADA);
		if (StringUtils.isEmpty(entrada)) {
			setMessageError(getMessage("pastaDeEntrada.error"));
			return null;
		}

		File f = new File(StringUtils.trim(entrada));
		if (f.exists()) {
			File[] listFiles = f.listFiles();
			createTree(listFiles, root, "");
		}
		return root;
	}

	private void createTree(File[] listFiles, TreeNode nodeTree, String posicao) {
		if (listFiles == null) {
			return;
		}
		for (int i = 0; i < listFiles.length; i++) {
			File file = listFiles[i];
			File[] filhos = file.listFiles();
			boolean isDiretorio = filhos != null;
			String posLocal = StringUtils.isNotEmpty(posicao) ? posicao + "." + (i + 1) : (i + 1) + "";
			TreeNode node = new TreeNode(file.getName(), posLocal, nodeTree, isDiretorio);
			if (isDiretorio) {
				createTree(filhos, node, posLocal);
			}
		}
	}

	protected String getSessionKey() {
		return TREE_NODES_SESSION_KEY;
	}

	protected void storeNodesInSession(TreeNode rootNode) {
		if (tree.getRootNode() == null) {
			return;
		}
		getContext().getSession().setAttribute(getSessionKey(), rootNode);
	}

	protected void clearSession() {
		if (tree.getRootNode() == null) {
			return;
		}
		getContext().getSession().setAttribute(getSessionKey(), null);
	}

	protected TreeNode loadNodesFromSession() {
		return (TreeNode) getContext().getSession().getAttribute(getSessionKey());
	}
}