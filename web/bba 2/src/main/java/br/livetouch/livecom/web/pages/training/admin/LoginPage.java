package br.livetouch.livecom.web.pages.training.admin;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.ServletUtil;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.HomePage;
import br.livetouch.livecom.web.pages.LogonPage;
import br.livetouch.livecom.web.pages.training.TrainingUtil;
import net.livetouch.click.page.BorderPage;

@Controller
@Scope("prototype")
public class LoginPage extends LogonPage {
	
	@Override
	public String getTemplate() {
		return TrainingAdminPage.TEMPLATE;
	}

	@Override
	public boolean onSecurityCheck() {
		if(!isBuildBJJ()) {
			// livecom
			setRedirect(HomePage.class);
			return onSecurityCheckNotOk();
		}
		return super.onSecurityCheck();
	}
	
	@Override
	public void onInit() {
		super.onInit();
		
		TrainingUtil.setTrainingStyle(form);
	}
	
	@Override
	public void onPost() {
		super.onPost();
	}
	
	@Override
	protected boolean loginOk(Usuario u) {
		
		setRedirect(PostsPage.class);
		
		return true;
	}
	
	public static void redirect(BorderPage page) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("from",page.getPath());
		HttpServletRequest request = page.getContext().getRequest();
		String s = ServletUtil.getRequestParams(request);
		if (StringUtils.isNotEmpty(s)) {
			params.put("fromParams", s);
		}
		page.setRedirect(LoginPage.class,params);
	}

}