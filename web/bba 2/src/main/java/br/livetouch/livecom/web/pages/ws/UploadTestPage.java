package br.livetouch.livecom.web.pages.ws;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.IOUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.utils.FileExtensionUtils;
import br.livetouch.livecom.web.pages.admin.LivecomPage;
import net.sf.click.control.FileField;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class UploadTestPage extends LivecomPage {

	public Form form = new Form();

	private FileField fileField;

	public String msgError;
	public String msg;
	
	@Override
	public boolean onSecurityCheck() {
		return true;
	}

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		form.add(fileField = new FileField("fileField"));
		
		form.add(new Submit("Upload"));
	}

	@Override
	public void onPost() {
		if (form.isValid()) {

			try {
				FileItem item = fileField.getFileItem();
				
				String nome = item.getName();
				
				nome = FileExtensionUtils.getFileNameFixed(nome);
				
				byte[] bytes = item.get();
				
				String dir = ParametrosMap.getInstance(getEmpresaId()).get(Params.FILE_UPLOAD_PHP_DIR);
				if(dir != null) {
					File file = new File(dir,nome);

					IOUtils.write(bytes, new FileOutputStream(file));
					
					addModel("arquivo", nome);
				}
				
				msg = "Upload ok: " + nome;
				
			} catch (Exception e) {
				e.printStackTrace();
				msgError = e.getMessage();
			}
		}
	}
}
