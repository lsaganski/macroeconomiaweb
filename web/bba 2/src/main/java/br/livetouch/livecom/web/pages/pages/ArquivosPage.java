package br.livetouch.livecom.web.pages.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.amazonaws.services.s3.model.S3ObjectSummary;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.files.FileManager;
import br.livetouch.livecom.files.FileManagerFactory;
import br.livetouch.livecom.files.LivecomFile;
import br.livetouch.livecom.utils.S3File;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import net.sf.click.control.Form;
import net.sf.click.control.HiddenField;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * Page mae para usar o Autowired do Spring
 * 
 * @author ricardo
 * 
 */
@Controller
@Scope("prototype")
public class ArquivosPage extends LivecomLogadoPage {
public Form form = new Form();
	
	public Form formPasta = new Form();
	private TextField tBusca;
	
	private TextField tNomePasta;

	public List<Arquivo> arquivos;

	public Usuario usuario;

	public String modeBusca;
	
	public String dir;
	public String mode;
	
	//public String paginaAtiva = "meusArquivos";
	
	public String msgError;
	public List<String> listDir;
	
	public boolean escondeChat = true;
	public boolean bootstrap_on = true;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.ARQUIVOS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();

		usuario = usuarioService.get(getUsuario().getId());
		form();
	}

	private void form() {
		tBusca = new TextField("busca");
		tBusca.setAttribute("class", "placeholder input borda lupa");
		tBusca.setAttribute("placeholder", getMessage("buscar.arquivos.label"));
		form.add(tBusca);
		form.setMethod("get");
		form.add(new HiddenField("modeBusca",String.class));
		form.add(new Submit("MeusArquivos", getMessage("meus.arquivos.label"), this, "onClickBuscarMeusArquivos"));
		form.add(new Submit("Todos", getMessage("todos.label"), this, "onClickBuscarTodos"));
		
		formPasta.add(tNomePasta = new TextField("nomePasta", true));
		formPasta.add(new Submit("Criar Pasta", this, "onClickCriarPasta"));
		
	}
	
	protected ArrayList<S3File> toListS3File(List<S3ObjectSummary> objs) {
		ArrayList<S3File> s3List = new ArrayList<S3File>();
		ParametrosMap map = getParametrosMap();
		for (S3ObjectSummary obj : objs) {
			S3File s3 = new S3File(obj, map);
			s3List.add(s3);
		}
		return s3List;
	}
	
	public boolean conteudoDiretorio() {
		
		try {
			System.out.println("dir:" + dir);
			
			String folder;
			if (dir == null || dir.equals("/")) {
				folder = usuario.getChave();
			} else {
				 folder = usuario.getChave() + "/" + dir;
			}
			System.out.println("folder: " + folder);
			System.out.println("Listing files ");
			FileManager fileManager = FileManagerFactory.getFileManager(getParametrosMap());
			List<LivecomFile> files =  fileManager.getFilesInFolder(folder);
			for (LivecomFile livecomFile : files) {
				System.out.println(" - " + livecomFile.getKey() + "  " + "(size = " + livecomFile.getSize() + ")");
			}
			
			System.out.println("Listing folders: ");
			listDir  = fileManager.getFoldersInFolder(folder);
			for (String dir : listDir) {
				System.out.println("Nome pasta: "+ dir);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	

	public boolean onClickCriarPasta() {
		if (formPasta.isValid()) {
			try {
				FileManagerFactory.getFileManager(getParametrosMap()).createFolder(usuario.getChave() + "/" + tNomePasta.getValue());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();
	}

}
