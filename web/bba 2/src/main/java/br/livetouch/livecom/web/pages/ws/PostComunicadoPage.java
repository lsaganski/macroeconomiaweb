package br.livetouch.livecom.web.pages.ws;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.ChapeuIdField;
import br.infra.web.click.ComboCategoriaPost;
import br.infra.web.click.ComboVisibilidade;
import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostDestaque;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.Visibilidade;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.vo.PostInfoVO;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.pushserver.lib.PushService;
import net.livetouch.click.control.wysiwyg.RichTextArea;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Field;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LongField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class PostComunicadoPage extends WebServiceXmlJsonPage {

	public Form form = new Form();

	public String msg;

	private TextField tMode;

	private UsuarioLoginField tUser;

	private LongField tPostId;

	private LongField tArquivoDestaqueId;

	private TextField tDestaqueDescricaoUrl;
	private TextField tDestaqueTituloUrl;
	private IntegerField tDestaqueTipo;

	private ComboVisibilidade comboVisibilidade;

	public String tags;

	public String dataPublicacao;
	public String horaPublicacao;

	public String dataExpiracao;
	public String horaExpiracao;
	
	public String dataReminder;
	public String horaReminder;

	public String push;
	public String twittar;
	public String rascunho;
	public String webview;
	public String html;

	public String prioritario;

	@Override
	public void onInit() {
		super.onInit();

		form.add(tPostId = new LongField("post_id"));

		form.add(tArquivoDestaqueId = new LongField("arquivo_destaque_id"));
		
		form.add(tUser = new UsuarioLoginField("user", true, usuarioService, getEmpresa()));

		TextField tTitulo = new TextField("titulo");
		tTitulo.setFocus(true);
		form.add(tTitulo);
		form.add(new TextField("tags"));

		form.add(TextFieldLen("resumo", 255));

		RichTextArea messageTextArea = new RichTextArea("mensagem", 300, 100);
		// messageTextArea.setRequired(true);
		messageTextArea.setLabel("Mensagem:");
		messageTextArea.setCols(45);
		messageTextArea.setRows(8);
		form.add(messageTextArea);

		form.add(new TextField("urlSite"));
		form.add(new TextField("urlImagem"));
		form.add(new TextField("urlVideo"));

		form.add(new TextField("grupo_ids"));
		form.add(new TextField("arquivo_ids"));

		form.add(new ComboCategoriaPost(categoriaPostService, getEmpresa()));
		form.add(new ChapeuIdField("chapeu", false, chapeuService));

		// Destaque Itau
		form.add(new TextField("destaque", "destaque (0 ou 1)"));
		form.add(new TextField("push", "enviar push (0 ou 1 'se vazio manda')"));
		form.add(new TextField("twittar", "enviar twitter (0 ou 1 'se vazio manda')"));

		form.add(new TextField("prioritario", "prioritario (0 ou 1)"));

		form.add(comboVisibilidade = new ComboVisibilidade());

		// Destaque
		form.add(tDestaqueDescricaoUrl = new TextField("destaqueDescricaoUrl"));
		form.add(tDestaqueTituloUrl = new TextField("destaqueTituloUrl"));
		form.add(tDestaqueTipo = new IntegerField("destaqueTipo"));

		// Datas
		form.add(new TextField("dataPublicacao", "dataPublicacao (dd/MM/yyyy)"));
		form.add(new TextField("horaPublicacao", "horaPublicacao (hh:mm)"));

		form.add(new TextField("dataExpiracao", "dataExpiracao (dd/MM/yyyy)"));
		form.add(new TextField("horaExpiracao", "horaExpiracao (hh:mm)"));
		
		form.add(new TextField("dataReminder", "dataReminder (dd/MM/yyyy)"));
		form.add(new TextField("horaReminder", "horaReminder (hh:mm)"));

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));

		form.add(tMode = new TextField("mode"));
		tMode.setValue("json");

		form.add(new TextField("grupo_cods"));
		form.add(new TextField("categoria_cod"));
		form.add(new LongField("status_post_id"));
		
		form.add(new TextField("webview", "webview (0 ou 1)"));
		form.add(new TextField("html", "html (0 ou 1)"));

		form.add(new Submit("publicar"));
		
		// setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		try {
			if (form.isValid()) {

				String isOn = ParametrosMap.getInstance(getEmpresa()).get(Params.MANUTENCAO_ON, "0");
				String msgManutencao = ParametrosMap.getInstance(getEmpresa()).get(Params.MANUTENCAO_MESSASGE, "Sistema em manutenção, não é possível postar comunicados nem comentar.");
				if("1".equals(isOn)) {
					return new MensagemResult("NOK", msgManutencao); 
				}
				

				Long postId = tPostId.getLong();

				List<Long> arquivoIds = getListIds("arquivo_ids");

				Usuario user = tUser.getEntity();
				if (user == null) {
					return new MensagemResult("NOK", "Usuario nao encontrado");
				}

				Post p = postId != null ? postService.get(new Long(postId)) : null;
				if (p == null) {
					p = new Post();
				} 

				Date publicacao = p.getDataPublicacao();
				form.copyTo(p);
				
				if (p.getVisibilidade() == null) {
					p.setVisibilidade(Visibilidade.GRUPOS);
				}
				
				if(publicacao != null) {
					p.setDataPublicacao(publicacao);
				}

				CategoriaPost categoria = p.getCategoria();
				if (categoria == null) {
					
					String codigo = get("categoria_cod");
					if(StringUtils.isNotEmpty(codigo)) {
						categoria = categoriaPostService.findByCodigo(getEmpresa(), codigo);
						
						if(categoria == null) {
							return new MensagemResult("NOK", "Favor selecione uma categoria para este post.");
						}
					}

					if (categoria == null) {
						categoria = categoriaPostService.findAllDefault(getEmpresa()).get(0);
					}
					
					if (categoria == null) {
						return new MensagemResult("NOK", "Favor selecione uma categoria para este post.");
					}
				}
				
				
				// Destaque
				PostDestaque destaque = p.getPostDestaque();
				if (destaque == null) {
					destaque = new PostDestaque();
				}
				destaque.setUrlSite(p.getUrlSite());
				destaque.setUrlImagem(p.getUrlImagem());
				destaque.setDescricaoUrl(tDestaqueDescricaoUrl.getValue());
				destaque.setTituloUrl(tDestaqueTituloUrl.getValue());
				destaque.setTipo(tDestaqueTipo.getInteger() == null ? 0 : tDestaqueTipo.getInteger());

				// Parametros para salvar
				PostInfoVO info = new PostInfoVO();
				info.arquivoIds = arquivoIds;
				info.destaque = destaque;
				info.dataPublicacao = dataPublicacao;
				info.horaPublicacao = horaPublicacao;
				info.dataExpiracao = dataExpiracao;
				info.horaExpiracao = horaExpiracao;
				info.dataReminder = dataReminder;
				info.horaReminder = horaReminder;
				info.tagsIds = getListIds("tags");
				info.tags = tags;
				boolean checked = "1".equals(push);
				info.push = checked;
				info.prioritario = "1".equals(prioritario);
				info.grupoIds = getListIds("grupo_ids");
				info.grupos = getListCods("grupo_cods");
				info.statusPost = getLongParam("status_post_id");
				info.webview = "1".equals(webview);
				info.html = "1".equals(html);
				info.arquivoDestaqueId = tArquivoDestaqueId.getLong();
				
				//Rascunho
				boolean isRascunho = p.isRascunho();
				if(p.isRascunho()) {
					p.setDataPublicacao(new Date());
				}
				if("1".equals(rascunho)) {
					p.setRascunho(true);
				} else {
					p.setRascunho(false);
				}
				
				// Post
				p = postService.post(p, user, info, isRascunho);
				
				PostVO vo = postService.setPost(p, user);
				
				if("1".equals(twittar)) {
					
					String usuarios = "";
					
					if (p != null) {
						Set<Grupo> grupos = p.getGrupos();
						usuarios = getTwitters(grupos, p, user.getTwitter(), usuarios);
					}

					PushService s = Livecom.getPushService();
					try {
						s.tweet("twitter", p.getTitulo(), null, usuarios, "json");
					} catch(Exception e) {
						return new MensagemResult("NOK", e.getMessage());
					}
				}
				
				if(isWsVersion3()) {
					Response r = Response.ok("OK");
					r.post = vo;
					return r;
				}
				return vo;

			} else {
				return getFormErrorMensagemResult(form);
			}
		} catch (DomainMessageException e) {
			return new MensagemResult("NOK", e.getMessage());
		} catch (DomainException e) {
			return new MensagemResult("ERROR", "Ocorreu um erro: " + e.getMessage());
		} catch (Exception e) {
			logError(e.getMessage(), e);
			return new MensagemResult("ERROR", "Ocorreu um erro inesperado, aguarde alguns instantes e tente novamente.");
		}
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("post", PostVO.class);
		super.xstream(x);
	}

	private Field TextFieldLen(String name, int length) {
		TextField t = new TextField(name);
		t.setMaxLength(length);
		return t;
	}
	
	private String getTwitters(Set<Grupo> grupos, Post post, String user, String usuarios) {
		for (Grupo g : grupos) {
			List<Long> listUsers = grupoService.findUsuariosToPush(g, post, null);
			List<Usuario> users = usuarioService.findAllByIds(listUsers);
			if(listUsers.size() > 0) {
				for (Usuario usuario : users) {
					String twitter = usuario.getTwitter();
					if(StringUtils.isNotEmpty(twitter) && !twitter.equals(user)) {
						usuarios+=twitter+",";
					}
				}
			}
		}
		
		return usuarios;
	}
}
