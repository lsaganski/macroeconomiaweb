package br.livetouch.livecom.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.LogTransacao;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Provider
public class LogTransacaoException extends LogTransacaoUtil implements javax.ws.rs.ext.ExceptionMapper<Throwable> {

	private static final Logger log = Log.getLogger(LogTransacao.class);
	
	@Override
	public Response toResponse(Throwable ex) {
		MessageResult erro = null;
		if(ex instanceof DomainException) {
			 erro = MessageResult.error(ex.getMessage());
			 log.error("Log transacao Domain Exception: " + ex.getMessage(), ex);
		} else {
			erro = MessageResult.error("Ocorreu um erro na transação, por favor tente novamente mais tarde.");
			log.error("Log Transacao Exception: " + ex.getMessage(), ex);
		}
		
		saveLogTransacao(ex);
		
		return Response.ok().entity(erro).build();
	}
}
