package br.livetouch.livecom.chatAkka.router;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.livetouch.livecom.chatAkka.ChatFile;
import br.livetouch.livecom.chatAkka.protocol.JsonRawMessage;

public class ChatVO {

	public JsonRawMessage raw;
	public String code;
	
	/**
	 * Login
	 */
	public String user;
	public String pass;
	public String so;
	public String version;
	public Integer versionCode;
	/**
	 * Mensagem
	 */
	public long identifier;
	
	public long conversaId;
	public long grupoId;
	public String msg;
	public long from;
	public long to;
	// usado no forward para enviar varias msg de uma vez
	public JSONArray msgs;
	
	public JSONArray buttons;

	/**
	 * Files
	 */
	public HashSet<ChatFile> filesBase64;
	
	public JSONArray cards;

	/**
	 * Lat/Lng
	 */
	public double lat;
	public double lng;

	/**
	 * Get Messages
	 */
	public Long lastMsgId;
	public Long msgId;

	/**
	 * Paginacao
	 */
	public int pag;
	public int max;
	public String flag;
	
	/**
	 * User status
	 */
	public boolean away;
	
	/**
	 * Msgs pendentes ao fazer getAll
	 */
	public List<Long> msgIds1C;
	public List<Long> msgIds2C;
	public List<Long> cIds;

	public ChatVO() {
		
	}
	
	public ChatVO(JsonRawMessage raw) throws JSONException {
		this.raw = raw;

		JSONObject json = new JSONObject(raw.getJson());

		this.code = json.optString("code");

		/**
		 * Login
		 */
		this.user = json.optString("user");
		this.pass = json.optString("pass");
		this.so = json.optString("so");
		this.versionCode = json.optInt("versionCode");
		this.version = json.optString("version");
		if (StringUtils.isEmpty(so)) {
			so = "android";
		}

		/**
		 * Mensagem
		 */
		this.identifier = json.optLong("identifier");
		this.conversaId = json.optLong("cId");
		this.grupoId = json.optLong("gId");
		this.msgId = json.optLong("msgId");
		this.msg = json.optString("msg");
		this.from = json.optLong("from");
		this.to = json.optLong("to");
		this.msgs = json.optJSONArray("msgs");

		this.filesBase64 = new HashSet<ChatFile>();

		boolean isSendMessage = StringUtils.equals(code, "msg") || StringUtils.equals(code, "msgFile");
		
		this.cards = json.optJSONArray("cards");
		
		this.buttons = json.optJSONArray("buttons");
		
		if(isSendMessage) {
			readFiles(json);

			readLatLng(json);
		}

		/**
		 * Get Messages
		 */
		this.lastMsgId = json.optLong("lastMsgId");

		/**
		 * Paginacao
		 */
		this.pag = json.optInt("pag");
		this.max = json.optInt("max");
		this.flag = json.optString("flag");

		/**
		 * Status
		 */
		this.away = json.optBoolean("away",false);

		/**
		 * Msgs pendentes (callback)
		 */
		this.msgIds1C = getListLong(json,"msgIds1C");
		this.msgIds2C = getListLong(json,"msgIds2C");
		this.cIds = getListLong(json,"cIds");
	}

	private List<Long> getListLong(JSONObject json, String key) throws JSONException {
		JSONArray array = json.optJSONArray(key);
		List<Long> ids = new ArrayList<Long>();
		if (array != null) {
			for (int i = 0; i < array.length(); i++) {
				Long msgId = array.getLong(i);
				ids.add(msgId);
			}
		}
		return ids;
	}

	public void readFiles(JSONObject json) throws JSONException {
		/**
		 * Arquivos
		 */
		JSONArray jsonArrayFiles = json.optJSONArray("files");
		if (jsonArrayFiles != null) {
			for (int i = 0; i < jsonArrayFiles.length(); i++) {
				JSONObject jsonFile = jsonArrayFiles.getJSONObject(i);
				ChatFile chatFile = new ChatFile();
				chatFile.setNome(jsonFile.getString("nome"));
				// audio, video, etc
				chatFile.setTipo(jsonFile.getString("tipo"));
				chatFile.setBase64(jsonFile.getString("base64"));
				// Base 64
				filesBase64.add(chatFile);
			}
		}
	}

	public void readLatLng(JSONObject json) {
		JSONObject loc = json.optJSONObject("loc");
		if(loc != null) {
			lat = loc.optDouble("lat");
			lng = loc.optDouble("lng");
		}
	}
	
	@Override
	public String toString() {
		return raw.toString();
	}
}
