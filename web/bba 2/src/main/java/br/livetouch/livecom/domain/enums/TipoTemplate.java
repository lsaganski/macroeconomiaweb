package br.livetouch.livecom.domain.enums;

/**
 * Tipo de email pra logar no Email Report
 * 
 * @author Juillian Lee
 *
 */
public enum TipoTemplate {
	convite("Convite"),
	recuperarSenha("Recuperar Senha"),
	emailMarketing("Email Marketing"),
	alterarSenha("Alterar Senha"),
	newsletter("Newsletter"), 
	conviteTrial("Convite Trial");

	private String label;
	
	TipoTemplate(String label) {
		this.label = label;
	}
	
	@Override
	public String toString() {
		return this.label != null ? this.label : "";
	}
	
	public static TipoTemplate valueOfLabel(int value) {
		TipoTemplate tipo = value > -1 ? TipoTemplate.values()[value] : null;
		return tipo != null ? tipo : null;
	}
	
}
