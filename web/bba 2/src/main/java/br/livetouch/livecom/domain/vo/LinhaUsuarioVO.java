package br.livetouch.livecom.domain.vo;

import org.apache.log4j.Logger;

public class LinhaUsuarioVO extends LinhaArquivoVO {
	protected static Logger log = Logger.getLogger(LinhaUsuarioVO.class);

	private String login;
	private String dataNascimento;

	@Override
	public LinhaArquivoVO parser(String line, String[] split, int row) {
		if (row == 1) {
			// ignora o header
			return null;
		}

		try {
			LinhaUsuarioVO vo = new LinhaUsuarioVO();
			vo.row = row;
			vo.string = line;
			
			int length = split.length;
			
			// ignora coluna 1 (começa em 1)
			vo.setLogin(get(split, length == 2 ? 1 : 2));
			vo.setDataNascimento(get(split, length == 2 ? 2: 3));

			log.debug("Linha [" + row + "]: " + vo);
			
			return vo;
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new RuntimeException("Verifique se todos os campos da planilha foram preenchidos");
		}
	};

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

}
