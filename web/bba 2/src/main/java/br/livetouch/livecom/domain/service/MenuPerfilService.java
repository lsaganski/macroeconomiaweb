package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.MenuPerfil;
import br.livetouch.livecom.domain.Perfil;
import net.livetouch.tiger.ddd.DomainException;

public interface MenuPerfilService extends Service<MenuPerfil> {

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(MenuPerfil c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(MenuPerfil c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void saveList(List<MenuPerfil> menus) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void removeIn(List<MenuPerfil> menus) throws DomainException;
	
	List<MenuPerfil> findPaiByPerfil(Perfil perfil, Empresa empresa, boolean in);

	List<MenuPerfil> findFilhoByPerfil(Perfil perfil, Empresa empresa, boolean in);

	List<MenuPerfil> findByPerfil(Perfil p, Empresa empresa);

	void removeNotIn(List<MenuPerfil> menus, Perfil perfil) throws DomainException;

}