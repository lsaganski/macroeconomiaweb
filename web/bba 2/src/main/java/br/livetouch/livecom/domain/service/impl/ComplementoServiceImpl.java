package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Complemento;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.repository.ComplementoRepository;
import br.livetouch.livecom.domain.service.ComplementoService;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ComplementoServiceImpl implements ComplementoService {
	@Autowired
	private ComplementoRepository rep;

	@Override
	public Complemento get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Complemento c, Empresa empresa) throws DomainException {
		c.setEmpresa(empresa);
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Complemento> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	@Override
	public void delete(Complemento c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public String csvComplemento(Empresa empresa) {
		List<Complemento> findAll = findAll(empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("CODIGO").add("NOME");
		for (Complemento c : findAll) {
			body.add(c.getCodigoDesc()).add(c.getNome());
			body.end();
		}
		return generator.toString();
	}
}
