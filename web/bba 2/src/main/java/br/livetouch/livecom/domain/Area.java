package br.livetouch.livecom.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;

import br.livetouch.livecom.domain.vo.AreaVO;
import net.livetouch.tiger.ddd.DomainException;

@Entity
public class Area extends JsonEntity{
	private static final long serialVersionUID = 691495770535792242L;
	@Id
	@Column(name="id", unique=true, nullable=false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "AREA_SEQ")
	private Long id;
	
	private String nome;
	private String codigo;
	private boolean padrao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "diretoria_id")
	private Diretoria diretoria;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;
	
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigo() {
		return codigo;
	}
	
	public String getCodigoDesc() {
		return codigo != null ? codigo : id.toString();
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Diretoria getDiretoria() {
		return diretoria;
	}

	public void setDiretoria(Diretoria diretoria) {
		this.diretoria = diretoria;
	}

	public static void isFormValid(Area area, boolean isUpdate) throws DomainException {
		if(area == null) {
			throw new DomainException("Parametros inválidos");
		}
		
		if(StringUtils.isEmpty(area.nome)) {
			throw new DomainException("Campo nome não pode ser vazio");
		}
		
		if(isUpdate && area.getId() == null) {
			throw new DomainException("Área não localizada");
		}
		
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}

	public boolean isPadrao() {
		return padrao;
	}

	public void setPadrao(boolean padrao) {
		this.padrao = padrao;
	}

	@Override
	public String toJson() {
		AreaVO vo = new AreaVO(this);
		return new Gson().toJson(vo);
	}
	
	@Override
	public String toString() {
		return toStringIdDesc();
	}
	
	public String toStringIdDesc() {
		return id + ": " + nome;
	}
}
