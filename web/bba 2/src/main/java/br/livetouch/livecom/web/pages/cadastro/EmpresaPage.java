package br.livetouch.livecom.web.pages.cadastro;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.vo.CamposMap;
import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.domain.vo.UploadResponse;
import br.livetouch.livecom.email.EmailUtils;
import br.livetouch.livecom.utils.LivecomUtil;
import br.livetouch.livecom.utils.UploadHelper;
import br.livetouch.livecom.web.pages.root.EmpresasPage;
import br.livetouch.pushserver.lib.PushNotification;
import br.livetouch.pushserver.lib.UsuarioToPush;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.extras.util.CripUtil;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;
import net.sf.click.control.PasswordField;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class EmpresaPage extends CadastrosPage {

	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", this, "editar");
	public Link deleteLink = new Link("deletar", this, "deletar");
	public String msgErro;
	public String id;

	/**
	 * Usada para trazer as tab selecionadas
	 */
	public String tab1;
	public String tab2;

	/**
	 * Se o web service enviar este id, atualiza a foto com o arquivo.
	 */
	public Long arquivoFotoId;
	public Long arquivoLogoId;
	public Long arquivoBgId;
	public Long arquivoFavId;
	public Long arquivoMuralCapaId;
	public Long arquivoCapaGrupoId;

	// Dados da Empresa
	public TextField tNome;
	public Empresa empresa;
	public Usuario usuario;
	private Checkbox checkTrialOn;

	// Dados do Admin
	public TextField tNomeUsuario;
	public TextField tEmail;
	public PasswordField tNovaSenha;
	public PasswordField tConfirmarSenha;

	public Map<String, Boolean> paramDisplayTable = new HashMap<>();

	public int page;

	public CamposMap campos;

	public List<Parametro> params;

	@Override
	public void onInit() {
		super.onInit();
		form();
		campos = getCampos();
		setParamDisplayTable();

		tab1 = getParam("tab1");
		tab2 = getParam("tab2");

		Long id = null;
		if (StringUtils.isNotEmpty(this.id) && StringUtils.isNumeric(this.id)) {
			id = Long.parseLong(this.id);
		}

		if (id != null) {
			empresa = empresaService.get(id);

			if (empresa != null && empresa.getAdmin() != null) {
				usuario = empresa.getAdmin();
				tNome.setValue(usuario.getNome());
				form.copyFrom(usuario);

			}

			params = parametroService.findAllByEmpresa(empresa.getId());
			checkTrialOn.setChecked(StringUtils.isNotEmpty(empresa.getTrialCode()));
			// SET COMPONENTES

			form.copyFrom(empresa);
		}
		
		/**
		 * Layout
		 */
		setText(Params.LAYOUT_BACKGROUD);
		setText(Params.LAYOUT_LOGO);
		setText(Params.LAYOUT_FAVICON);
		setText(Params.LAYOUT_CAPA_MURAL);
		setText(Params.FOTO_USUARIO_DEFAULT);
		setText(Params.LAYOUT_THUMB_GRUPO);
		setText(Params.COR_MENU);
		setText(Params.COR_BUSCA);
		setText(Params.COR_BADGE_SPAN);
		setText(Params.COR_BADGE_TEXT);
		setText(Params.COR_POST_AGENDADO);
		setText(Params.COR_POST_DESTACADO);
		setText(Params.COR_POST_EXPIRADO);
		setText(Params.COR_POST_PRIORITARIO);

		/**
		 * PARAMETROS
		 * 
		 * Chat
		 */
		setChecked(Params.CHAT_ON);
		setChecked(Params.AMAZON_LAMBDA_CHAT_RESIZE_ON);
		setChecked(Params.AMAZON_LAMBDA_CHAT_RESIZE_GARANTIR_ON);
		setText(Params.FILE_AMAZON_TRIES_GET);
		setText(Params.FILE_MANAGER_AMAZON_BUCKET_CHAT);
		setText(Params.FILE_MANAGER_AMAZON_SERVER_LAMBDA);
		setText(Params.FILE_MANAGER_AMAZON_BUCKET_CHAT_RESIZE);
		
		/**
		 * Analtyics
		 */
		setText(Params.WEB_TEMPLATE_ANALYTICS_KEY);

		/**
		 * Controle Versao
		 */

		setChecked(Params.REDIRECT_USER_DOWNLOAD_APP_ON);
		setChecked(Params.THIRD_PARTY_VERSION_CONTROL_ON);
		setText(Params.THIRD_PARTY_VERSION_CONTROL_URL);
		setChecked(Params.VERSION_CONTROL_ON);
		

		// android
		setText(Params.VERSION_CONTROL_ANDROID_NUMBER);
		setText(Params.VERSION_CONTROL_ANDROID_NUMBER_CODE);
		setText(Params.VERSION_CONTROL_ANDROID_MENSAGEM);
		setText(Params.VERSION_CONTROL_ANDROID_URLDOWNLOAD);
		setCheckInformativoAndroid(Params.VERSION_CONTROL_ANDROID_MODE);

		// ios
		setText(Params.VERSION_CONTROL_IOS_NUMBER);
		setText(Params.VERSION_CONTROL_IOS_NUMBER_CODE);
		setText(Params.VERSION_CONTROL_IOS_MENSAGEM);
		setText(Params.VERSION_CONTROL_IOS_URLDOWNLOAD);
		setCheckInformativoIos(Params.VERSION_CONTROL_IOS_MODE);
		
		//web
		setText(Params.VERSION_CONTROL_WEB_IMAGE);
		setText(Params.VERSION_CONTROL_WEB_TEXT);
		setText(Params.URL_GOOGLE_PLAY_DOWNLOAD);
		setText(Params.URL_APP_STORE_DOWNLOAD);

		/**
		 * Download de Aplicativos
		 */
		setText(Params.BUILD_BETA_ANDROID_URL);
		setChecked(Params.BUILD_APP_IOS_TEST_FLIGHT);
		setText(Params.BUILD_APP_ANDROID_URL);
		setText(Params.BUILD_APP_IOS_URL);

		/**
		 * Email
		 */
		setText(Params.EMAIL_EMAIL_ADMIN);
		setText(Params.SERVER_HOST);
		setText(Params.EMAIL_SMTP_PORT);
		setText(Params.EMAIL_SMTP_USER);
		setText(Params.EMAIL_SMTP_PASSWORD);
		setChecked(Params.EMAIL_SMTP_SSL);

		/**
		 * Excluir Regras
		 */
		setChecked(Params.DELETAR_TAG_R1_ON);
		setChecked(Params.DELETAR_TAG_R2_ON);
		setChecked(Params.DELETAR_TAG_C1_ON);
		setChecked(Params.DELETAR_CATEGORIA_R2_ON);
		setChecked(Params.DELETAR_CATEGORIA_C1_ON);
		setChecked(Params.DELETAR_CATEGORIA_C2_ON);
		setChecked(Params.DELETAR_GRUPO_R2_ON);
		setChecked(Params.DELETAR_GRUPO_R3_ON);
		setChecked(Params.DELETAR_GRUPO_R4_ON);
		setChecked(Params.DELETAR_GRUPO_R5_ON);
		setChecked(Params.DELETAR_GRUPO_C1_ON);
		setChecked(Params.DELETAR_GRUPO_C2_ON);
		setChecked(Params.DELETAR_GRUPO_C3_ON);
		setChecked(Params.DELETAR_GRUPO_C4_ON);
		setChecked(Params.DELETAR_GRUPO_C5_ON);
		setChecked(Params.DELETAR_USUARIO_R2_ON);
		setChecked(Params.DELETAR_USUARIO_R3_ON);
		setChecked(Params.DELETAR_USUARIO_R4_ON);
		setChecked(Params.DELETAR_USUARIO_C1_ON);
		setChecked(Params.DELETAR_USUARIO_C2_ON);
		setChecked(Params.DELETAR_AREA_R1_ON);
		setChecked(Params.DELETAR_DIRETORIA_R1_ON);
		setChecked(Params.DELETAR_DIRETORIA_R2_ON);
		setChecked(Params.DELETAR_DIRETORIA_R3_ON);

		/**
		 * LDAP
		 */
		setText(Params.LDAP_PROVIDER_URL);
		setText(Params.LDAP_SEARCH_BASE);
		setText(Params.LDAP_SECURITY_PRINCIPAL);
		setText(Params.LDAP_SECURITY_PASSWD);
		setText(Params.LDAP_SEARCH_FILTER);

		/**
		 * Login
		 */
		setChecked(Params.LOGIN_CONNECTOR_ON);
		setChecked(Params.ITAU_LOGIN_HASHMD5);
		setText(Params.LOGIN_CONNECTOR_RECUPERAR_SENHA);

		/**
		 * Mural
		 */
		setChecked(Params.MURAL_SIDEBOX_ON);
		setChecked(Params.SIDEBOX_NOTIFICATION_AGRUPAR_ON);
		setChecked(Params.MURAL_POST_TITULO_OBRIGATORIO_ON);
		setChecked(Params.MURAL_TAG_ON);
		setChecked(Params.MURAL_CHAPEU_ON);
		setChecked(Params.MURAL_RESUMO_ON);
		setChecked(Params.MURAL_HORA_ON);
		setChecked(Params.MURAL_RASCUNHO_ON);
		setChecked(Params.MURAL_POST_TASK_ON);
		setChecked(Params.MURAL_ENVIAR_ARQUIVOS_ON);
		setChecked(Params.MURAL_POST_DESTAQUE_ON);
		setChecked(Params.MURAL_WEBVIEW_ON);
		setChecked(Params.MURAL_GALERIA_ON);
		setChecked(Params.MURAL_GALERIA_THUMBS_ON);
		setChecked(Params.MURAL_GALERIA_THUMBS_EDIT_ON);
		setChecked(Params.MURAL_GALERIA_THUMBS_FIXA_ON);
		setText(Params.MURAL_GALERIA_THUMBS_TAMANHO_FIXO);
		setChecked(Params.MURAL_GALERIA_CROP_ON);
		setChecked(Params.MURAL_GALERIA_RESIZE_ON);
		setChecked(Params.MURAL_GALERIA_ROTATE_ON);
		setChecked(Params.MURAL_LAYOUT_ANEXOS_REDUZIDO_ON);
		setChecked(Params.MURAL_VISIBILIDADE_POST_ON);
		setChecked(Params.MURAL_POST_PRIORITARIO_ON);
		setChecked(Params.MURAL_GRUPO_HEADER_ON);
		setChecked(Params.MURAL_AMIZADE_ON);
		setChecked(Params.MURAL_GRUPO_PARTICIPACAO_ON);
		setChecked(Params.MURAL_UPLOAD_CROP_ON);
		setChecked(Params.MURAL_UPLOAD_CROP_THUMB_ON);
		setChecked(Params.MURAL_PREVIEW_POST_REVISTA_ON);
		setChecked(Params.MURAL_DATA_AGENDAMENTO_POST_ON);
		setChecked(Params.MURAL_DATA_EXPIRACAO_POST_ON);
		setChecked(Params.MURAL_DATA_REMINDER_POST_ON);
		setText(Params.MURAL_PLACEHOLDER_TITULO_POST);
		setChecked(Params.MURAL_POSTAR_INCIDENTE_ON);
		setChecked(Params.AMAZON_CLOUDFRONT_ON);
		
		/**
		 * Tutorial
		 */
		setChecked(Params.MURAL_TUTORIAL_ON);
		
		/**
		 * Usuários (cadastro)
		 */
		setChecked(Params.USUARIOS_COLUNA_CARGO_ON);
		
		/**
		 * Integrações
		 */
		setChecked(Params.INTEGRACAO_SISTEMA_ON);
		setText(Params.INTEGRACAO_SISTEMA_URL_POSTAR_INCIDENTE);
		setText(Params.INTEGRACAO_SISTEMA_URL_DELETAR_INCIDENTE);
		
		/**
		 * Categorias
		 */
		setChecked(Params.CATEGORIA_PAI_ON);

		/**
		 * Grupos
		 */
		setChecked(Params.CADASTRO_GRUPO_CATEGORIA_ON);
		setChecked(Params.CADASTRO_GRUPO_TIPO_ON);
		setChecked(Params.CADASTRO_GRUPO_VIRTUAL_ON);

		/**
		 * Push
		 */
		setChecked(Params.PUSH_ON);
		setChecked(Params.PROJETO_PUSH_SILENT_ON);
		setChecked(Params.PUSH_CHECKBOXON);
		setChecked(Params.PUSH_CHECKBOX_CHECKED);
		setChecked(Params.PUSH_POST_AUTOR_RECEBE_ON);
		setChecked(Params.PUSH_POST_CREATE_NOTIFICATION_UPDATE);
		setChecked(Params.PUSH_COMENTARIO_AUTOR_RECEBE_ON);
		setText(Params.MSG_PUSH_NEWPOST);
		setText(Params.PUSH_SERVER_PROJECT);
		setText(Params.PUSH_SERVER_HOST);
		setText(Params.PUSH_API_KEY);
		
		/**
		 * Firebase
		 */
		setChecked(Params.PUSH_FIREBASE_ON);
		setText(Params.PUSH_FIREBASE_COLLAPSEKEY);
		setChecked(Params.PUSH_FIREBASE_CONTENTAVALIABLE);
		setChecked(Params.PUSH_FIREBASE_DELAYWHILEIDLE);
		setText(Params.PUSH_FIREBASE_PRIORITY);
		setText(Params.PUSH_FIREBASE_TIMETOLIVE);

		/**
		 * Firebase Chat
		 */
		setText(Params.PUSH_FIREBASE_COLLAPSEKEY_CHAT);
		setChecked(Params.PUSH_FIREBASE_CONTENTAVALIABLE_CHAT);
		setChecked(Params.PUSH_FIREBASE_DELAYWHILEIDLE_CHAT);
		setText(Params.PUSH_FIREBASE_PRIORITY_CHAT);
		setText(Params.PUSH_FIREBASE_TIMETOLIVE_CHAT);
		
		/**
		 * Push Menssseges
		 */
		setText(Params.PUSH_COMENTARIO_TITULO_NOTIFICATION);
		setText(Params.PUSH_COMENTARIO_SUB_TITULO_NOTIFICATION);
		setText(Params.PUSH_FAVORITO_TITULO_NOTIFICATION);
		setText(Params.PUSH_FAVORITO_SUB_TITULO_NOTIFICATION);
		setText(Params.PUSH_COMUNICADO_NEW_TITULO_NOTIFICATION);
		setText(Params.PUSH_COMUNICADO_NEW_SUB_TITULO_NOTIFICATION);
		setText(Params.PUSH_REMINDER_NEW_TITULO_NOTIFICATION);
		setText(Params.PUSH_REMINDER_NEW_SUB_TITULO_NOTIFICATION);
		setText(Params.PUSH_COMUNICADO_UPDATE_TITULO_NOTIFICATION);
		setText(Params.PUSH_COMUNICADO_UPDATE_SUB_TITULO_NOTIFICATION);
		setText(Params.PUSH_LIKE_POST_TITULO_NOTIFICATION);
		setText(Params.PUSH_LIKE_POST_SUB_TITULO_NOTIFICATION);
		setText(Params.PUSH_LIKE_COMENTARIO_TITULO_NOTIFICATION);
		setText(Params.PUSH_LIKE_COMENTARIO_SUB_TITULO_NOTIFICATION);
		setText(Params.MSG_PUSH_MENSAGEM_IMAGEM);

		/**
		 * Badges
		 */
		setChecked(Params.SIDEBOX_NOTIFICATION_ON);
		setChecked(Params.PUSH_BADGES_POST_ON);
		setChecked(Params.PUSH_BADGES_FAVORITO_ON);
		setChecked(Params.PUSH_BADGES_LIKE_ON);
		setChecked(Params.PUSH_BADGES_COMENTARIO_ON);
		setChecked(Params.PUSH_BADGES_CHAT_ON);

		/**
		 * Restrições de Horario
		 */
		setCheckdGruposUsuarios(Params.RESTRICAO_HORARIO_MODO);
		setChecked(Params.USUARIO_RESTRICAO_HORARIO_ON);
		setChecked(Params.GRUPO_RESTRICAO_HORARIO_ON);

		/**
		 * Senha
		 */
		setChecked(Params.USER_SENHA_PADRAO_ON);
		setText(Params.USER_SENHA_PADRAO);

		/**
		 * Web
		 */
		setText(Params.WEB_TEMPLATE_TITLE);
		setText(Params.BORDER_COPYRIGHT_EMPRESA);
		// setText(Params.FOTO_USUARIO_DEFAULT);
		setChecked(Params.POST_CENSURA_ON);
		setChecked(Params.VALIDA_EMAIL_UNICO_ON);
		setChecked(Params.VALIDA_LOGIN_UNICO_ON);

		/**
		 * Relatorios
		 */
		setChecked(Params.EXPORTAR_CSV);
		setText(Params.IMPORTACAO_ARQUIVO_CHARSET);
		setText(Params.EXPORTACAO_ARQUIVO_CHARSET);

		/**
		 * Menu
		 */
		setChecked(Params.MENU_DINAMICO_ON);
		
		/**
		 * Chat Keep Alive
		 */
		setChecked(Params.CHAT_KEEP_ALIVE_ON);
		setChecked(Params.CHAT_SEND_KEEP_ALIVE_ON);
		setChecked(Params.CHAT_CHECK_KEEP_ALIVE_ON);
		setText(Params.CHAT_SEND_KEEP_ALIVE_TIME_SECONDS);
		setText(Params.CHAT_CHECK_KEEP_ALIVE_TIME_SECONDS);
		
		/**
		 * Segurança Login
		 */
		setChecked(Params.CONFIG_LOGIN_ON);
		setText(Params.CONFIG_LOGIN_QTDEMAXERROSLOGIN);
		setText(Params.CONFIG_LOGIN_TEMPOBLOQUEIOERROLOGIN);
		setChecked(Params.EXPIRAR_SENHA_INATIVIDADE_ON);
		setText(Params.EXPIRAR_SENHA_INATIVIDADE_QTDEDIAS);
		setChecked(Params.EXPIRAR_SENHA_PERIODICIDADE_ON);
		setText(Params.EXPIRAR_SENHA_PERIODICIDADE_QTDEDIAS);
		setChecked(Params.LOGIN_FORM_AUTOCOMPLETE_ON);
		setChecked(Params.LOGIN_FORM_CAPTCHA_ON);
		setText(Params.LOGIN_FORM_CAPTCHA_QTD);
		
		/**
		 * Segurança
		 */
		setChecked(Params.SECURITY_HOST_ON);
		setChecked(Params.SECURITY_IP_ON);
		setText(Params.SECURITY_ALLOWED_ADMIN_HOST);
		setChecked(Params.SECURITY_WS_PATH_ON);
		setText(Params.SECURITY_WS_PATH_ALLOWED);
		setText(Params.SECURITY_ALLOWED_IP);
		setChecked(Params.SECURITY_USERAGENT_ON);
		setText(Params.SECURITY_USERAGENT_WEB_ALLOWED);
		setText(Params.SECURITY_USERAGENT_MOBILE_ALLOWED);
		setText(Params.SECURITY_USERAGENT_WEB_BLOCKED);
		setText(Params.SECURITY_USERAGENT_MOBILE_BLOCKED);
		setChecked(Params.WS_SECURITY_GET_ALLOWED);
		setChecked(Params.WS_SECURITY_TOKEN_ON);
		setChecked(Params.SECURITY_WS_CRYPT_ON);
		setChecked(Params.SECURITY_ATTACK_REPLAY_ON);
		setChecked(Params.WS_SECURITY_OTP_ON);
		
		/**
		 * Segurança filters
		 */
		setChecked(Params.SECURITY_XSS_ON);
		setChecked(Params.SECURITY_CORS_ON);
		setText(Params.SECURITY_CORS_ORIGIN);
		setText(Params.SECURITY_UPLOAD_MAX_SIZE);
		setText(Params.SECURITY_UPLOAD_MIN_SIZE);
		setText(Params.SECURITY_UPLOAD_ALLOWED_EXTENSIONS);
	}

	private void setCheckdGruposUsuarios(String name) {
		Long id = empresa != null && empresa.getId() != null ? empresa.getId() : 1L;
		ParametrosMap param = ParametrosMap.getInstance(id);
		String value = param.get(name);
		Checkbox check = getCheckSimNao(name);
		
		check.setChecked("1".equals(value));
		check.setChecked("grupo".equals(param.get(Params.RESTRICAO_HORARIO_MODO)));

	}

	private void setCheckInformativoAndroid(String name) {
		Long id = empresa != null && empresa.getId() != null ? empresa.getId() : 1L;
		ParametrosMap param = ParametrosMap.getInstance(id);
		String value = param.get(name);
		Checkbox check = getCheckInformativoInpeditivo(name);
		check.setChecked("1".equals(value));
		check.setChecked("informativo".equals(param.get(Params.VERSION_CONTROL_ANDROID_MODE)));
		
	}
	
	private void setCheckInformativoIos(String name) {
		Long id = empresa != null && empresa.getId() != null ? empresa.getId() : 1L;
		ParametrosMap param = ParametrosMap.getInstance(id);
		String value = param.get(name);
		Checkbox check = getCheckInformativoInpeditivo(name);
		check.setChecked("1".equals(value));
		check.setChecked("informativo".equals(param.get(Params.VERSION_CONTROL_IOS_MODE)));
		
	}
	
	private void setChecked(String name) {
		Long id = empresa != null && empresa.getId() != null ? empresa.getId() : 1L;
		ParametrosMap param = ParametrosMap.getInstance(id);
		String value = param.get(name);
		Checkbox check = getCheckSimNao(name);
		check.setChecked("1".equals(value));
	}

	private void setText(String name) {
		Long id = empresa != null && empresa.getId() != null ? empresa.getId() : 1L;
		ParametrosMap param = ParametrosMap.getInstance(id);
		String value = param.get(name);
		TextField t = getText(name);
		t.setValue(value);
	}

	public void form() {
		boolean novaEmpresa = id == null;
		form.add(new IdField(null));
		/**
		 * Form Dados Empresa
		 */
		form.add(getText("nome", true));
		form.add(getText("codigo", true));
		form.add(getText("dominio", true));
		form.add(checkTrialOn = getCheckSimNao("trialOn"));

		Submit tsalvar = new Submit("salvar", this, "salvar");
		tsalvar.setAttribute("class", "btn btn-primary min-width hidden");
		form.add(tsalvar);

		if (id != null) {
			Submit tDeletar = new Submit("deletar", this, "deletar");
			tDeletar.setAttribute("class", "btn btn-outline red-thunderbird min-width");
			form.add(tDeletar);
		}
		Submit cancelar = new Submit("cancelar", this, "cancelar");
		cancelar.setAttribute("class", "btn btn-default min-width");
		form.add(cancelar);
		/*
		 * 
		 * /** Dados do admin
		 */
		form.add(tNome = getText("nomeUsuario", true));
		form.add(tEmail = getText("email", true));
		
		form.add(tNovaSenha = getTextPassword("nova_senha", novaEmpresa));
		form.add(tConfirmarSenha = getTextPassword("confirmar_senha", novaEmpresa));
		

		tNome.setFocus(true);

		// ADICIONANDO NO FORM

		/**
		 * Layout
		 *
		 */
		form.add(getText(Params.LAYOUT_BACKGROUD));
		form.add(getText(Params.LAYOUT_LOGO));
		form.add(getText(Params.LAYOUT_FAVICON));
		form.add(getText(Params.LAYOUT_CAPA_MURAL));
		form.add(getText(Params.FOTO_USUARIO_DEFAULT));
		form.add(getText(Params.LAYOUT_THUMB_GRUPO));
		form.add(getTextCores(Params.COR_MENU));
		form.add(getTextCores(Params.COR_BUSCA));
		form.add(getTextCores(Params.COR_BADGE_SPAN));
		form.add(getTextCores(Params.COR_BADGE_TEXT));
		form.add(getTextCores(Params.COR_POST_AGENDADO));
		form.add(getTextCores(Params.COR_POST_DESTACADO));
		form.add(getTextCores(Params.COR_POST_EXPIRADO));
		form.add(getTextCores(Params.COR_POST_PRIORITARIO));

		/**
		 * PARAMETROS
		 * 
		 * Chat
		 */
		form.add(getCheckSimNao(Params.CHAT_ON));
		form.add(getCheckSimNao(Params.AMAZON_LAMBDA_CHAT_RESIZE_ON));
		form.add(getCheckSimNao(Params.AMAZON_LAMBDA_CHAT_RESIZE_GARANTIR_ON));
		form.add(getText(Params.FILE_AMAZON_TRIES_GET));
		form.add(getText(Params.FILE_MANAGER_AMAZON_BUCKET_CHAT));
		form.add(getText(Params.FILE_MANAGER_AMAZON_SERVER_LAMBDA));
		form.add(getText(Params.FILE_MANAGER_AMAZON_BUCKET_CHAT_RESIZE));
		
		/**
		 * Analtyics
		 */
		form.add(getText(Params.WEB_TEMPLATE_ANALYTICS_KEY));

		/**
		 * Controle de Versão
		 */
		form.add(getCheckSimNao(Params.REDIRECT_USER_DOWNLOAD_APP_ON));
		form.add(getCheckSimNao(Params.THIRD_PARTY_VERSION_CONTROL_ON));
		form.add(getText(Params.THIRD_PARTY_VERSION_CONTROL_URL));
		form.add(getCheckSimNao(Params.VERSION_CONTROL_ON));

		// android
		form.add(getText(Params.VERSION_CONTROL_ANDROID_NUMBER));
		form.add(getText(Params.VERSION_CONTROL_ANDROID_NUMBER_CODE));
		form.add(getText(Params.VERSION_CONTROL_ANDROID_MENSAGEM));
		form.add(getText(Params.VERSION_CONTROL_ANDROID_URLDOWNLOAD));
		form.add(getCheckInformativoInpeditivo(Params.VERSION_CONTROL_ANDROID_MODE));

		// ios
		form.add(getText(Params.VERSION_CONTROL_IOS_NUMBER));
		form.add(getText(Params.VERSION_CONTROL_IOS_NUMBER_CODE));
		form.add(getText(Params.VERSION_CONTROL_IOS_MENSAGEM));
		form.add(getText(Params.VERSION_CONTROL_IOS_URLDOWNLOAD));
		form.add(getCheckInformativoInpeditivo(Params.VERSION_CONTROL_IOS_MODE));
		
		//web
		form.add(getText(Params.VERSION_CONTROL_WEB_IMAGE));
		form.add(getText(Params.VERSION_CONTROL_WEB_TEXT));
		form.add(getText(Params.URL_GOOGLE_PLAY_DOWNLOAD));
		form.add(getText(Params.URL_APP_STORE_DOWNLOAD));

		/**
		 * Download de Aplicativos
		 */
		form.add(getText(Params.BUILD_BETA_ANDROID_URL));
		form.add(getText(Params.BUILD_APP_ANDROID_URL));
		form.add(getCheckSimNao(Params.BUILD_APP_IOS_TEST_FLIGHT));
		form.add(getText(Params.BUILD_APP_IOS_URL));

		/**
		 * Email
		 */
		form.add(getText(Params.EMAIL_EMAIL_ADMIN));
		form.add(getText(Params.SERVER_HOST));
		form.add(getText(Params.EMAIL_SMTP_PORT));
		form.add(getText(Params.EMAIL_SMTP_USER));
		form.add(getText(Params.EMAIL_SMTP_PASSWORD));
		form.add(getCheckSimNao(Params.EMAIL_SMTP_SSL));

		/**
		 * Excluir Regras
		 */
		form.add(getCheckSimNao(Params.DELETAR_TAG_R1_ON));
		form.add(getCheckSimNao(Params.DELETAR_TAG_R2_ON));
		form.add(getCheckSimNao(Params.DELETAR_TAG_C1_ON));
		form.add(getCheckSimNao(Params.DELETAR_CATEGORIA_R2_ON));
		form.add(getCheckSimNao(Params.DELETAR_CATEGORIA_C1_ON));
		form.add(getCheckSimNao(Params.DELETAR_CATEGORIA_C2_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_R2_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_R3_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_R4_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_R5_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_C1_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_C2_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_C3_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_C4_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_C5_ON));
		form.add(getCheckSimNao(Params.DELETAR_USUARIO_R2_ON));
		form.add(getCheckSimNao(Params.DELETAR_USUARIO_R3_ON));
		form.add(getCheckSimNao(Params.DELETAR_USUARIO_R4_ON));
		form.add(getCheckSimNao(Params.DELETAR_USUARIO_C1_ON));
		form.add(getCheckSimNao(Params.DELETAR_USUARIO_C2_ON));
		form.add(getCheckSimNao(Params.DELETAR_AREA_R1_ON));
		form.add(getCheckSimNao(Params.DELETAR_DIRETORIA_R1_ON));
		form.add(getCheckSimNao(Params.DELETAR_DIRETORIA_R2_ON));
		form.add(getCheckSimNao(Params.DELETAR_DIRETORIA_R3_ON));

		/**
		 * LDAP
		 */
		form.add(getText(Params.LDAP_PROVIDER_URL));
		form.add(getText(Params.LDAP_SEARCH_BASE));
		form.add(getText(Params.LDAP_SECURITY_PRINCIPAL));
		form.add(getText(Params.LDAP_SECURITY_PASSWD));
		form.add(getText(Params.LDAP_SEARCH_FILTER));

		/**
		 * Loguin
		 */
		form.add(getCheckSimNao(Params.LOGIN_CONNECTOR_ON));
		form.add(getCheckSimNao(Params.ITAU_LOGIN_HASHMD5));
		form.add(getText(Params.LOGIN_CONNECTOR_RECUPERAR_SENHA));

		/**
		 * Mural
		 */
		form.add(getCheckSimNao(Params.MURAL_SIDEBOX_ON));
		form.add(getCheckSimNao(Params.SIDEBOX_NOTIFICATION_AGRUPAR_ON));
		form.add(getCheckSimNao(Params.MURAL_POST_TITULO_OBRIGATORIO_ON));
		form.add(getCheckSimNao(Params.MURAL_TAG_ON));
		form.add(getCheckSimNao(Params.MURAL_CHAPEU_ON));
		form.add(getCheckSimNao(Params.MURAL_RESUMO_ON));
		form.add(getCheckSimNao(Params.MURAL_HORA_ON));
		form.add(getCheckSimNao(Params.MURAL_RASCUNHO_ON));
		form.add(getCheckSimNao(Params.MURAL_POST_TASK_ON));
		form.add(getCheckSimNao(Params.MURAL_ENVIAR_ARQUIVOS_ON));
		form.add(getCheckSimNao(Params.MURAL_POST_DESTAQUE_ON));
		form.add(getCheckSimNao(Params.MURAL_WEBVIEW_ON));
		form.add(getCheckSimNao(Params.MURAL_GALERIA_ON));
		form.add(getCheckSimNao(Params.MURAL_GALERIA_THUMBS_ON));
		form.add(getCheckSimNao(Params.MURAL_GALERIA_THUMBS_EDIT_ON));
		form.add(getCheckSimNao(Params.MURAL_GALERIA_THUMBS_FIXA_ON));
		form.add(getText(Params.MURAL_GALERIA_THUMBS_TAMANHO_FIXO));
		form.add(getCheckSimNao(Params.MURAL_GALERIA_CROP_ON));
		form.add(getCheckSimNao(Params.MURAL_GALERIA_RESIZE_ON));
		form.add(getCheckSimNao(Params.MURAL_GALERIA_ROTATE_ON));
		form.add(getCheckSimNao(Params.MURAL_LAYOUT_ANEXOS_REDUZIDO_ON));
		form.add(getCheckSimNao(Params.MURAL_VISIBILIDADE_POST_ON));
		form.add(getCheckSimNao(Params.MURAL_POST_PRIORITARIO_ON));
		form.add(getCheckSimNao(Params.MURAL_GRUPO_HEADER_ON));
		form.add(getCheckSimNao(Params.MURAL_AMIZADE_ON));
		form.add(getCheckSimNao(Params.MURAL_GRUPO_PARTICIPACAO_ON));
		form.add(getCheckSimNao(Params.MURAL_UPLOAD_CROP_ON));
		form.add(getCheckSimNao(Params.MURAL_UPLOAD_CROP_THUMB_ON));
		form.add(getCheckSimNao(Params.MURAL_PREVIEW_POST_REVISTA_ON));
		form.add(getCheckSimNao(Params.MURAL_DATA_AGENDAMENTO_POST_ON));
		form.add(getCheckSimNao(Params.MURAL_DATA_EXPIRACAO_POST_ON));
		form.add(getCheckSimNao(Params.MURAL_DATA_REMINDER_POST_ON));
		form.add(getText(Params.MURAL_PLACEHOLDER_TITULO_POST));
		form.add(getCheckSimNao(Params.MURAL_POSTAR_INCIDENTE_ON));
		form.add(getCheckSimNao(Params.AMAZON_CLOUDFRONT_ON));
		
		/**
		 * Tutorial
		 */
		form.add(getCheckSimNao(Params.MURAL_TUTORIAL_ON));
		
		/**
		 * Usuários (cadastro)
		 */
		form.add(getCheckSimNao(Params.USUARIOS_COLUNA_CARGO_ON));
		
		/**
		 * Integrações
		 */
		form.add(getCheckSimNao(Params.INTEGRACAO_SISTEMA_ON));
		form.add(getText(Params.INTEGRACAO_SISTEMA_URL_POSTAR_INCIDENTE));
		form.add(getText(Params.INTEGRACAO_SISTEMA_URL_DELETAR_INCIDENTE));
		
		/**
		 * Categorias
		 */
		form.add(getCheckSimNao(Params.CATEGORIA_PAI_ON));

		/**
		 * Grupos
		 */
		form.add(getCheckSimNao(Params.CADASTRO_GRUPO_CATEGORIA_ON));
		form.add(getCheckSimNao(Params.CADASTRO_GRUPO_TIPO_ON));
		form.add(getCheckSimNao(Params.CADASTRO_GRUPO_VIRTUAL_ON));
		

		/**
		 * Push
		 */
		form.add(getCheckSimNao(Params.PUSH_ON));
		form.add(getCheckSimNao(Params.PROJETO_PUSH_SILENT_ON));
		form.add(getCheckSimNao(Params.PUSH_CHECKBOXON));
		form.add(getCheckSimNao(Params.PUSH_CHECKBOX_CHECKED));
		form.add(getCheckSimNao(Params.PUSH_POST_AUTOR_RECEBE_ON));
		form.add(getCheckSimNao(Params.PUSH_POST_CREATE_NOTIFICATION_UPDATE));
		form.add(getCheckSimNao(Params.PUSH_COMENTARIO_AUTOR_RECEBE_ON));
		form.add(getText(Params.MSG_PUSH_NEWPOST));
		form.add(getText(Params.PUSH_SERVER_PROJECT));
		form.add(getText(Params.PUSH_SERVER_HOST));
		form.add(getText(Params.PUSH_API_KEY));

		/**
		 * Firebase
		 */
		form.add(getCheckSimNao(Params.PUSH_FIREBASE_ON));
		form.add(getText(Params.PUSH_FIREBASE_COLLAPSEKEY));
		form.add(getCheckSimNao(Params.PUSH_FIREBASE_CONTENTAVALIABLE));
		form.add(getCheckSimNao(Params.PUSH_FIREBASE_DELAYWHILEIDLE));
		form.add(getText(Params.PUSH_FIREBASE_PRIORITY));
		form.add(getText(Params.PUSH_FIREBASE_TIMETOLIVE));

		/**
		 * Firebase Chat
		 */
		form.add(getText(Params.PUSH_FIREBASE_COLLAPSEKEY_CHAT));
		form.add(getCheckSimNao(Params.PUSH_FIREBASE_CONTENTAVALIABLE_CHAT));
		form.add(getCheckSimNao(Params.PUSH_FIREBASE_DELAYWHILEIDLE_CHAT));
		form.add(getText(Params.PUSH_FIREBASE_PRIORITY_CHAT));
		form.add(getText(Params.PUSH_FIREBASE_TIMETOLIVE_CHAT));

		/**
		 * Push Menssseges
		 */
		form.add(getText(Params.PUSH_COMENTARIO_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_COMENTARIO_SUB_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_FAVORITO_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_FAVORITO_SUB_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_COMUNICADO_NEW_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_COMUNICADO_NEW_SUB_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_REMINDER_NEW_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_REMINDER_NEW_SUB_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_COMUNICADO_UPDATE_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_COMUNICADO_UPDATE_SUB_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_LIKE_POST_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_LIKE_POST_SUB_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_LIKE_COMENTARIO_TITULO_NOTIFICATION));
		form.add(getText(Params.PUSH_LIKE_COMENTARIO_SUB_TITULO_NOTIFICATION));
		form.add(getText(Params.MSG_PUSH_MENSAGEM_IMAGEM));

		/**
		 * Badges
		 */
		form.add(getCheckSimNao(Params.SIDEBOX_NOTIFICATION_ON));
		form.add(getCheckSimNao(Params.PUSH_BADGES_POST_ON));
		form.add(getCheckSimNao(Params.PUSH_BADGES_FAVORITO_ON));
		form.add(getCheckSimNao(Params.PUSH_BADGES_LIKE_ON));
		form.add(getCheckSimNao(Params.PUSH_BADGES_COMENTARIO_ON));
		form.add(getCheckSimNao(Params.PUSH_BADGES_CHAT_ON));

		/**
		 * Restrições de Horario
		 */
		form.add(getCheckGrupoUsuario(Params.RESTRICAO_HORARIO_MODO));
		form.add(getCheckSimNao(Params.USUARIO_RESTRICAO_HORARIO_ON));
		form.add(getCheckSimNao(Params.GRUPO_RESTRICAO_HORARIO_ON));

		/**
		 * Senha
		 */
		form.add(getCheckSimNao(Params.USER_SENHA_PADRAO_ON));
		form.add(getText(Params.USER_SENHA_PADRAO));

		/**
		 * Web
		 */
		form.add(getText(Params.WEB_TEMPLATE_TITLE));
		form.add(getText(Params.BORDER_COPYRIGHT_EMPRESA));
		// form.add(getText(Params.FOTO_USUARIO_DEFAULT));
		form.add(getCheckSimNao(Params.POST_CENSURA_ON));
		form.add(getCheckSimNao(Params.VALIDA_EMAIL_UNICO_ON));
		form.add(getCheckSimNao(Params.VALIDA_LOGIN_UNICO_ON));

		/**
		 * Relatorios
		 */
		form.add(getCheckSimNao(Params.EXPORTAR_CSV));
		form.add(getText(Params.IMPORTACAO_ARQUIVO_CHARSET));
		form.add(getText(Params.EXPORTACAO_ARQUIVO_CHARSET));

		/**
		 * Menu
		 */
		form.add(getCheckSimNao(Params.MENU_DINAMICO_ON));
		
		/**
		 * Chat Keep Alive
		 */
		form.add(getCheckSimNao(Params.CHAT_KEEP_ALIVE_ON));
		form.add(getCheckSimNao(Params.CHAT_SEND_KEEP_ALIVE_ON));
		form.add(getCheckSimNao(Params.CHAT_CHECK_KEEP_ALIVE_ON));
		form.add(getText(Params.CHAT_SEND_KEEP_ALIVE_TIME_SECONDS));
		form.add(getText(Params.CHAT_CHECK_KEEP_ALIVE_TIME_SECONDS));
		
		/**
		 * Segurança Login
		 */
		form.add(getCheckSimNao(Params.CONFIG_LOGIN_ON));
		form.add(getText(Params.CONFIG_LOGIN_QTDEMAXERROSLOGIN));
		form.add(getText(Params.CONFIG_LOGIN_TEMPOBLOQUEIOERROLOGIN));
		form.add(getCheckSimNao(Params.EXPIRAR_SENHA_INATIVIDADE_ON));
		form.add(getText(Params.EXPIRAR_SENHA_INATIVIDADE_QTDEDIAS));
		form.add(getCheckSimNao(Params.EXPIRAR_SENHA_PERIODICIDADE_ON));
		form.add(getText(Params.EXPIRAR_SENHA_PERIODICIDADE_QTDEDIAS));
		form.add(getCheckSimNao(Params.LOGIN_FORM_AUTOCOMPLETE_ON));
		form.add(getCheckSimNao(Params.LOGIN_FORM_CAPTCHA_ON));
		form.add(getText(Params.LOGIN_FORM_CAPTCHA_QTD));
		
		/**
		 * Segurança
		 */
		form.add(getCheckSimNao(Params.SECURITY_HOST_ON));
		form.add(getCheckSimNao(Params.SECURITY_IP_ON));
		form.add(getText(Params.SECURITY_ALLOWED_ADMIN_HOST));
		form.add(getCheckSimNao(Params.SECURITY_WS_PATH_ON));
		form.add(getText(Params.SECURITY_WS_PATH_ALLOWED));
		form.add(getText(Params.SECURITY_ALLOWED_IP));
		form.add(getCheckSimNao(Params.SECURITY_USERAGENT_ON));
		form.add(getText(Params.SECURITY_USERAGENT_WEB_ALLOWED));
		form.add(getText(Params.SECURITY_USERAGENT_MOBILE_ALLOWED));
		form.add(getText(Params.SECURITY_USERAGENT_WEB_BLOCKED));
		form.add(getText(Params.SECURITY_USERAGENT_MOBILE_BLOCKED));
		form.add(getCheckSimNao(Params.WS_SECURITY_GET_ALLOWED));
		form.add(getCheckSimNao(Params.WS_SECURITY_TOKEN_ON));
		form.add(getCheckSimNao(Params.SECURITY_WS_CRYPT_ON));
		form.add(getCheckSimNao(Params.SECURITY_ATTACK_REPLAY_ON));
		form.add(getCheckSimNao(Params.WS_SECURITY_OTP_ON));
		
		/**
		 * Segurança filters
		 */
		form.add(getCheckSimNao(Params.SECURITY_XSS_ON));
		form.add(getCheckSimNao(Params.SECURITY_CORS_ON));
		form.add(getText(Params.SECURITY_CORS_ORIGIN));
		form.add(getText(Params.SECURITY_UPLOAD_MIN_SIZE));
		form.add(getText(Params.SECURITY_UPLOAD_MAX_SIZE));
		form.add(getText(Params.SECURITY_UPLOAD_ALLOWED_EXTENSIONS));
		
	}

	// Set atributos Check padrão
	private Checkbox getCheckSimNao(String name) {
		name = StringUtils.replace(name, ".", "_");
		Checkbox check = (Checkbox) form.getField(name);
		if (check != null) {
			return check;
		}
		check = new Checkbox(name);
		check.setAttribute("class", "bootstrap");
		check.setAttribute("data-off-color", "danger");
		check.setAttribute("data-off-text", "Não");
		check.setAttribute("data-on-text", "Sim");
		return check;
	}

	// Set atributos do TextField para seleção de cores
	private TextField getTextCores(String name) {

		Integer maxLength = 255;

		name = StringUtils.replace(name, ".", "_");
		TextField t = (TextField) form.getField(name);
		if (t != null) {
			return t;
		}
		t = new TextField(name);
		t.setAttribute("class", "form-control minicolors-input");
		t.setAttribute("data-control", "hue");
		t.setAttribute("size", "7");
		t.setMaxLength(maxLength);
		return t;
	}

	// Set atributos do Check para chavear entre Usuario/Grupo
	private Checkbox getCheckGrupoUsuario(String name) {
		name = StringUtils.replace(name, ".", "_");
		Checkbox check = (Checkbox) form.getField(name);
		if (check != null) {
			return check;
		}
		check = new Checkbox(name);
		check.setAttribute("class", "bootstrap");
		check.setAttribute("data-off-color", "success");
		check.setAttribute("data-off-text", "Usuário");
		check.setAttribute("data-on-text", "Grupo");
		return check;
	}
	
	private Checkbox getCheckInformativoInpeditivo(String name) {
		name = StringUtils.replace(name, ".", "_");
		Checkbox check = (Checkbox) form.getField(name);
		if (check != null) {
			return check;
		}
		check = new Checkbox(name);
		check.setAttribute("class", "bootstrap");
		check.setAttribute("data-on-color", "info");
		check.setAttribute("data-off-color", "danger");
		check.setAttribute("data-on-text", "Informativo");
		check.setAttribute("data-off-text", "Impeditivo");
		return check;
	}

	// Set atributos TextField Padrão
	
	private TextField getText(String name){
			return getText(name, false);
	}
	
	private TextField getText(String name, boolean obrigatorio) {
		String classInput = "form-control input-sm";
		String classInputIgnore = classInput + (obrigatorio ? " " : " ignore");
		Integer maxLength = 255;

		name = StringUtils.replace(name, ".", "_");
		TextField t = (TextField) form.getField(name);
		if (t != null) {
			if (obrigatorio) {
				t.setAttribute("required", "required");
			}
			return t;
		}
		t = new TextField(name);
		if (obrigatorio) {
			t.setAttribute("required", "required");
		}
		t.setAttribute("class", classInputIgnore);
		t.setMaxLength(maxLength);

		if (StringUtils.equalsIgnoreCase(name, "nome")) {
			t.setFocus(true);
		}
		return t;
	}

	// Set atributos PasswordField Padrão
	private PasswordField getTextPassword(String name, boolean obrigatorio) {
		String classInput = "form-control input-sm";
		String classInputIgnore = classInput + (obrigatorio ? " " : " ignore");
		Integer maxLength = 255;

		name = StringUtils.replace(name, ".", "_");
		PasswordField t = (PasswordField) form.getField(name);
		if (t != null) {
			if (obrigatorio) {
				t.setAttribute("required", "required");
			}
			return t;
		}
		t = new PasswordField(name);
		if (obrigatorio) {
			t.setAttribute("required", "required");
		}
		t.setAttribute("class", classInputIgnore);
		t.setMaxLength(maxLength);

		if (StringUtils.equalsIgnoreCase(name, "nome")) {
			t.setFocus(true);
		}
		return t;
	}

	@Override
	public void onGet() {
		super.onGet();

	}

	public boolean salvar() throws DomainException, IOException, EmailException {
		if (form.isValid()) {
			boolean salvo = execSalvar();

			if (salvo && usuario != null && usuario.getStatus().equals(StatusUsuario.NOVO)) {
				String senha = StringUtils.isNotEmpty(usuario.getSenha()) ? usuario.getSenha() : usuarioService.createPassword(usuario);

				Map<String, Object> map = new HashMap<>();
				map.put("senha", senha);
				if (empresa != null) {
					map.put("dominio", empresa.getDominio());
				}

				UsuarioToPush usuarioToPush = new UsuarioToPush();
				usuarioToPush.setCodigo(usuario.getLogin());
				usuarioToPush.setNome(usuario.getNome());
				usuarioToPush.setEmail(usuario.getEmail());
				usuarioToPush.setIdLivecom(usuario.getId());
				usuarioToPush.setParams(map);

				PushNotification not = new PushNotification();
				not.setUsuario(usuarioToPush);
				not.setIsEmail(true);

				// A FAZER EMPRESA: Enviar e-amail de cadastro pro admin da
				// empresa depois de cadastrar?

				// Sender sender = SenderFactory.getSender(applicationContext,
				// SenderConvidarInterno.class);
				// sender.send(empresa, not);
				//
				// usuario.setStatus(StatusUsuario.ENVIADO_CONVITE);
				// usuarioService.saveOrUpdate(getUsuario(), usuario);
				// setMessageSesssion("Usuario(a) [" + usuario.getLogin() + "]
				// cadastrado(a) com sucesso. <br/>Os dados de acesso foram
				// enviados para o e-mail cadastrado!", "Cadastro.");
				//
				//
				// usuario.setStatus(StatusUsuario.ENVIADO_CONVITE);
				// usuarioService.saveAudit(getUsuario(), usuario);
				//
				// setMessageSesssion("Usuario(a) [" + usuario.getLogin() + "]
				// cadastrado(a) com sucesso. <br/>Os dados de acesso foram
				// enviados para o e-mail cadastrado!", "Cadastro.");
			}

			return salvo;
		} else {
			msgErro = error(form);
		}

		return true;
	}

	public boolean execSalvar() {
		if (form.isValid()) {
			try {
				Long id = null;
				if(StringUtils.isNotEmpty(this.id) && StringUtils.isNumeric(this.id)) {
					id = Long.parseLong(this.id);
				}
				empresa = id != null ? empresaService.get(id) : new Empresa();
				form.copyTo(empresa);

				String nome = tNome.getValue();
				String email = tEmail.getValue();
				if(!EmailUtils.isEmailValido(email)){
					setMessage("Email inválido: "+email+" verifique se o domínio está correto.");
					return false;
				}	
				
				if(checkTrialOn.isChecked() && StringUtils.isEmpty(empresa.getTrialCode())) {
					String trial = LivecomUtil.createRandomPassword(6);
					empresa.setTrialCode(trial);
				} else if (!checkTrialOn.isChecked()){
					empresa.setTrialCode(null);
				}
				
				
				
				List<Parametro> parametros  = new ArrayList<Parametro>();
				
				
				//ADICIONANDO PARAMETRO
				
				/**Layout
				 *
				 */
				addTextParam(parametros, Params.LAYOUT_BACKGROUD);
				addTextParam(parametros, Params.LAYOUT_LOGO);
				addTextParam(parametros, Params.LAYOUT_FAVICON);
				addTextParam(parametros, Params.LAYOUT_CAPA_MURAL);
				addTextParam(parametros, Params.FOTO_USUARIO_DEFAULT);
				addTextParam(parametros, Params.LAYOUT_THUMB_GRUPO);
				addTextParam(parametros, Params.COR_MENU);
				addTextParam(parametros, Params.COR_BUSCA);
				addTextParam(parametros, Params.COR_BADGE_SPAN);
				addTextParam(parametros, Params.COR_BADGE_TEXT);
				addTextParam(parametros, Params.COR_POST_AGENDADO);
				addTextParam(parametros, Params.COR_POST_DESTACADO);
				addTextParam(parametros, Params.COR_POST_EXPIRADO);
				addTextParam(parametros, Params.COR_POST_PRIORITARIO);
				
				/** PARAMETROS
				 * 
				 * CHAT
				 */
				addCheckParam(parametros,Params.CHAT_ON);
				addCheckParam(parametros,Params.AMAZON_LAMBDA_CHAT_RESIZE_ON);
				addCheckParam(parametros,Params.AMAZON_LAMBDA_CHAT_RESIZE_GARANTIR_ON);
				addTextParam(parametros, Params.FILE_AMAZON_TRIES_GET);
				addTextParam(parametros, Params.FILE_MANAGER_AMAZON_SERVER_LAMBDA);
				addTextParam(parametros, Params.FILE_MANAGER_AMAZON_BUCKET_CHAT_RESIZE);
				addTextParam(parametros, Params.FILE_MANAGER_AMAZON_BUCKET_CHAT);
				
				/**
				 * Analtyics
				 */
				addTextParam(parametros,Params.WEB_TEMPLATE_ANALYTICS_KEY);
				
				/**
				 * Controle Versão
				 */
				addCheckParam(parametros, Params.REDIRECT_USER_DOWNLOAD_APP_ON);
				addCheckParam(parametros, Params.THIRD_PARTY_VERSION_CONTROL_ON);
				addTextParam(parametros,Params.THIRD_PARTY_VERSION_CONTROL_URL);
				addCheckParam(parametros, Params.VERSION_CONTROL_ON);
				
				//android
				addTextParam(parametros,Params.VERSION_CONTROL_ANDROID_NUMBER);
				addTextParam(parametros,Params.VERSION_CONTROL_ANDROID_NUMBER_CODE);
				addTextParam(parametros,Params.VERSION_CONTROL_ANDROID_MENSAGEM);
				addTextParam(parametros, Params.VERSION_CONTROL_ANDROID_URLDOWNLOAD);
				addCheckInformativoImpeditivo(parametros, Params.VERSION_CONTROL_ANDROID_MODE);

				//ios
				addTextParam(parametros,Params.VERSION_CONTROL_IOS_NUMBER);
				addTextParam(parametros,Params.VERSION_CONTROL_IOS_NUMBER_CODE);
				addTextParam(parametros,Params.VERSION_CONTROL_IOS_MENSAGEM);
				addTextParam(parametros, Params.VERSION_CONTROL_IOS_URLDOWNLOAD);
				addCheckInformativoImpeditivo(parametros, Params.VERSION_CONTROL_IOS_MODE);
				
				//web
				addTextParam(parametros, Params.VERSION_CONTROL_WEB_IMAGE);
				addTextParam(parametros, Params.VERSION_CONTROL_WEB_TEXT);
				addTextParam(parametros, Params.URL_GOOGLE_PLAY_DOWNLOAD);
				addTextParam(parametros, Params.URL_APP_STORE_DOWNLOAD);
				
				/**
				* Download de Aplicativos
				*/
				addTextParam(parametros, Params.BUILD_BETA_ANDROID_URL);
				addTextParam(parametros, Params.BUILD_APP_ANDROID_URL);
                addCheckParam(parametros, Params.BUILD_APP_IOS_TEST_FLIGHT);
				addTextParam(parametros, Params.BUILD_APP_IOS_URL);
				
				/**
				* Email
				*/
				addTextParam(parametros, Params.EMAIL_EMAIL_ADMIN);
				addTextParam(parametros, Params.SERVER_HOST);
				addTextParam(parametros, Params.EMAIL_SMTP_PORT);
				addTextParam(parametros, Params.EMAIL_SMTP_USER);
				addTextParam(parametros, Params.EMAIL_SMTP_PASSWORD);
				addCheckParam(parametros, Params.EMAIL_SMTP_SSL);
				
				/**
				* Excluir Regras
				*/
				addCheckParam(parametros,Params.DELETAR_TAG_R1_ON);
				addCheckParam(parametros,Params.DELETAR_TAG_R2_ON);
				addCheckParam(parametros,Params.DELETAR_TAG_C1_ON);
				addCheckParam(parametros,Params.DELETAR_CATEGORIA_R2_ON);
				addCheckParam(parametros,Params.DELETAR_CATEGORIA_C1_ON);
				addCheckParam(parametros,Params.DELETAR_CATEGORIA_C2_ON);
				addCheckParam(parametros,Params.DELETAR_GRUPO_R2_ON);
				addCheckParam(parametros,Params.DELETAR_GRUPO_R3_ON);
				addCheckParam(parametros,Params.DELETAR_GRUPO_R4_ON);
				addCheckParam(parametros,Params.DELETAR_GRUPO_R5_ON);
				addCheckParam(parametros,Params.DELETAR_GRUPO_C1_ON);
				addCheckParam(parametros,Params.DELETAR_GRUPO_C2_ON);
				addCheckParam(parametros,Params.DELETAR_GRUPO_C3_ON);
				addCheckParam(parametros,Params.DELETAR_GRUPO_C4_ON);
				addCheckParam(parametros,Params.DELETAR_GRUPO_C5_ON);
				addCheckParam(parametros,Params.DELETAR_USUARIO_R2_ON);
				addCheckParam(parametros,Params.DELETAR_USUARIO_R3_ON);
				addCheckParam(parametros,Params.DELETAR_USUARIO_R4_ON);
				addCheckParam(parametros,Params.DELETAR_USUARIO_C1_ON);
				addCheckParam(parametros,Params.DELETAR_USUARIO_C2_ON);
				addCheckParam(parametros,Params.DELETAR_AREA_R1_ON);
				addCheckParam(parametros,Params.DELETAR_DIRETORIA_R1_ON);
				addCheckParam(parametros,Params.DELETAR_DIRETORIA_R2_ON);
				addCheckParam(parametros,Params.DELETAR_DIRETORIA_R3_ON);
				
				/**
				* LDAP
				*/
				addTextParam(parametros, Params.LDAP_PROVIDER_URL);
				addTextParam(parametros, Params.LDAP_SEARCH_BASE);
				addTextParam(parametros, Params.LDAP_SECURITY_PRINCIPAL);
				addTextParam(parametros, Params.LDAP_SECURITY_PASSWD);
				addTextParam(parametros, Params.LDAP_SEARCH_FILTER);
				
				/**
				* Loguin
				*/
				addCheckParam(parametros, Params.LOGIN_CONNECTOR_ON);
				addCheckParam(parametros, Params.ITAU_LOGIN_HASHMD5);
				addTextParam(parametros, Params.LOGIN_CONNECTOR_RECUPERAR_SENHA);
				
				/**
				* Mural
				*/
				addCheckParam(parametros, Params.MURAL_SIDEBOX_ON);
				addCheckParam(parametros, Params.SIDEBOX_NOTIFICATION_AGRUPAR_ON);
				addCheckParam(parametros, Params.MURAL_POST_TITULO_OBRIGATORIO_ON);
				addCheckParam(parametros, Params.MURAL_TAG_ON);
				addCheckParam(parametros, Params.MURAL_CHAPEU_ON);
				addCheckParam(parametros, Params.MURAL_RESUMO_ON);
				addCheckParam(parametros, Params.MURAL_HORA_ON);
				addCheckParam(parametros, Params.MURAL_RASCUNHO_ON);
				addCheckParam(parametros, Params.MURAL_POST_TASK_ON);
				addCheckParam(parametros, Params.MURAL_ENVIAR_ARQUIVOS_ON);
				addCheckParam(parametros, Params.MURAL_POST_DESTAQUE_ON);
				addCheckParam(parametros, Params.MURAL_WEBVIEW_ON);
				addCheckParam(parametros, Params.MURAL_GALERIA_ON);
				addCheckParam(parametros, Params.MURAL_GALERIA_THUMBS_ON);
				addCheckParam(parametros, Params.MURAL_GALERIA_THUMBS_EDIT_ON);
				addCheckParam(parametros, Params.MURAL_GALERIA_THUMBS_FIXA_ON);
				addTextParam(parametros, Params.MURAL_GALERIA_THUMBS_TAMANHO_FIXO);
				addCheckParam(parametros, Params.MURAL_GALERIA_CROP_ON);
				addCheckParam(parametros, Params.MURAL_GALERIA_RESIZE_ON);
				addCheckParam(parametros, Params.MURAL_GALERIA_ROTATE_ON);
				addCheckParam(parametros, Params.MURAL_LAYOUT_ANEXOS_REDUZIDO_ON);
				addCheckParam(parametros, Params.MURAL_VISIBILIDADE_POST_ON);
				addCheckParam(parametros, Params.MURAL_POST_PRIORITARIO_ON);
				addCheckParam(parametros, Params.MURAL_GRUPO_HEADER_ON);
				addCheckParam(parametros, Params.MURAL_AMIZADE_ON);
				addCheckParam(parametros, Params.MURAL_GRUPO_PARTICIPACAO_ON);
				addCheckParam(parametros, Params.MURAL_UPLOAD_CROP_ON);
				addCheckParam(parametros, Params.MURAL_UPLOAD_CROP_THUMB_ON);
				addCheckParam(parametros, Params.MURAL_PREVIEW_POST_REVISTA_ON);
				addCheckParam(parametros, Params.MURAL_DATA_AGENDAMENTO_POST_ON);
				addCheckParam(parametros, Params.MURAL_DATA_EXPIRACAO_POST_ON);
				addCheckParam(parametros, Params.MURAL_DATA_REMINDER_POST_ON);
				addTextParam(parametros, Params.MURAL_PLACEHOLDER_TITULO_POST);
				addCheckParam(parametros, Params.MURAL_POSTAR_INCIDENTE_ON);
				addCheckParam(parametros, Params.AMAZON_CLOUDFRONT_ON);
				
				/**
				 * Tutorial
				 */
				addCheckParam(parametros, Params.MURAL_TUTORIAL_ON);
				
				/**
				 * Usuários (cadastro)
				 */
				addCheckParam(parametros, Params.USUARIOS_COLUNA_CARGO_ON);
				
				/**
				 * Integrações
				 */
				addCheckParam(parametros, Params.INTEGRACAO_SISTEMA_ON);
				addTextParam(parametros, Params.INTEGRACAO_SISTEMA_URL_POSTAR_INCIDENTE);
				addTextParam(parametros, Params.INTEGRACAO_SISTEMA_URL_DELETAR_INCIDENTE);
				
				/**
				 * Categorias
				 */
				addCheckParam(parametros, Params.CATEGORIA_PAI_ON);

				/**
				 * Grupos
				 */
				addCheckParam(parametros, Params.CADASTRO_GRUPO_CATEGORIA_ON);
				addCheckParam(parametros, Params.CADASTRO_GRUPO_TIPO_ON);
				addCheckParam(parametros, Params.CADASTRO_GRUPO_VIRTUAL_ON);
				
				/**
				* Push
				*/
				addCheckParam(parametros, Params.PUSH_ON);
				addCheckParam(parametros, Params.PROJETO_PUSH_SILENT_ON);
				addCheckParam(parametros, Params.PUSH_CHECKBOXON);
				addCheckParam(parametros, Params.PUSH_CHECKBOX_CHECKED);
				addCheckParam(parametros, Params.PUSH_POST_AUTOR_RECEBE_ON);
				addCheckParam(parametros, Params.PUSH_POST_CREATE_NOTIFICATION_UPDATE);
				addCheckParam(parametros, Params.PUSH_COMENTARIO_AUTOR_RECEBE_ON);
				addTextParam(parametros, Params.MSG_PUSH_NEWPOST);
				addTextParam(parametros, Params.PUSH_SERVER_PROJECT);
				addTextParam(parametros, Params.PUSH_SERVER_HOST);
				addTextParam(parametros, Params.PUSH_API_KEY);
				
				/**
				 * Firebase
				 */
				addCheckParam(parametros, Params.PUSH_FIREBASE_ON);
				addTextParam(parametros, Params.PUSH_FIREBASE_COLLAPSEKEY);
				addCheckParam(parametros, Params.PUSH_FIREBASE_CONTENTAVALIABLE);
				addCheckParam(parametros, Params.PUSH_FIREBASE_DELAYWHILEIDLE);
				addTextParam(parametros, Params.PUSH_FIREBASE_PRIORITY);
				addTextParam(parametros, Params.PUSH_FIREBASE_TIMETOLIVE);

				/**
				 * Firebase Chat
				 */
				addTextParam(parametros, Params.PUSH_FIREBASE_COLLAPSEKEY_CHAT);
				addCheckParam(parametros, Params.PUSH_FIREBASE_CONTENTAVALIABLE_CHAT);
				addCheckParam(parametros, Params.PUSH_FIREBASE_DELAYWHILEIDLE_CHAT);
				addTextParam(parametros, Params.PUSH_FIREBASE_PRIORITY_CHAT);
				addTextParam(parametros, Params.PUSH_FIREBASE_TIMETOLIVE_CHAT);
				
				/**
				* Push Menssseges
				*/
				addTextParam(parametros, Params.PUSH_COMENTARIO_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_COMENTARIO_SUB_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_FAVORITO_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_FAVORITO_SUB_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_COMUNICADO_NEW_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_COMUNICADO_NEW_SUB_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_REMINDER_NEW_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_COMUNICADO_NEW_SUB_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_COMUNICADO_UPDATE_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_COMUNICADO_UPDATE_SUB_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_LIKE_POST_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_LIKE_POST_SUB_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_LIKE_COMENTARIO_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.PUSH_LIKE_COMENTARIO_SUB_TITULO_NOTIFICATION);
				addTextParam(parametros, Params.MSG_PUSH_MENSAGEM_IMAGEM);
				
				/**
				* Badges
				*/
				addCheckParam(parametros, Params.SIDEBOX_NOTIFICATION_ON);
				addCheckParam(parametros, Params.PUSH_BADGES_POST_ON);
				addCheckParam(parametros, Params.PUSH_BADGES_FAVORITO_ON);
				addCheckParam(parametros, Params.PUSH_BADGES_LIKE_ON);
				addCheckParam(parametros, Params.PUSH_BADGES_COMENTARIO_ON);
				addCheckParam(parametros, Params.PUSH_BADGES_CHAT_ON);
				
				
				/**
				* Restrições de Horario
				*/
				addCheckGrupoUsuarioParam(parametros, Params.RESTRICAO_HORARIO_MODO);
				addCheckParam(parametros, Params.USUARIO_RESTRICAO_HORARIO_ON);
				addCheckParam(parametros, Params.GRUPO_RESTRICAO_HORARIO_ON);
				
				/**
				* Senha
				*/
				addCheckParam(parametros, Params.USER_SENHA_PADRAO_ON);
				addTextParam(parametros, Params.USER_SENHA_PADRAO);
				
				/**
				* Web
				*/
				addTextParam(parametros, Params.WEB_TEMPLATE_TITLE);
				addTextParam(parametros, Params.BORDER_COPYRIGHT_EMPRESA);
				//addTextParam(parametros, Params.FOTO_USUARIO_DEFAULT);
				addCheckParam(parametros,Params.POST_CENSURA_ON);
				addCheckParam(parametros,Params.VALIDA_EMAIL_UNICO_ON);
				addCheckParam(parametros,Params.VALIDA_LOGIN_UNICO_ON);
				
				/**
				* Relatorios
				*/
				addCheckParam(parametros, Params.EXPORTAR_CSV);
				addTextParam(parametros, Params.IMPORTACAO_ARQUIVO_CHARSET);
				addTextParam(parametros, Params.EXPORTACAO_ARQUIVO_CHARSET);
				
				/**
				* Menu
				*/
				addCheckParam(parametros, Params.MENU_DINAMICO_ON);
				
				/**
				* Chat Keep Alive
				*/
				addCheckParam(parametros, Params.CHAT_KEEP_ALIVE_ON);
				addCheckParam(parametros, Params.CHAT_SEND_KEEP_ALIVE_ON);
				addCheckParam(parametros, Params.CHAT_CHECK_KEEP_ALIVE_ON);
				addTextParam(parametros, Params.CHAT_SEND_KEEP_ALIVE_TIME_SECONDS);
				addTextParam(parametros, Params.CHAT_CHECK_KEEP_ALIVE_TIME_SECONDS);
				
				/**
				* Segurança login
				*/
				addCheckParam(parametros, Params.CONFIG_LOGIN_ON);
				addTextParam(parametros, Params.CONFIG_LOGIN_QTDEMAXERROSLOGIN);
				addTextParam(parametros, Params.CONFIG_LOGIN_TEMPOBLOQUEIOERROLOGIN);
				addCheckParam(parametros, Params.EXPIRAR_SENHA_INATIVIDADE_ON);
				addTextParam(parametros, Params.EXPIRAR_SENHA_INATIVIDADE_QTDEDIAS);
				addCheckParam(parametros, Params.EXPIRAR_SENHA_PERIODICIDADE_ON);
				addTextParam(parametros, Params.EXPIRAR_SENHA_PERIODICIDADE_QTDEDIAS);
				addCheckParam(parametros, Params.LOGIN_FORM_AUTOCOMPLETE_ON);
				addCheckParam(parametros, Params.LOGIN_FORM_CAPTCHA_ON);
				addTextParam(parametros, Params.LOGIN_FORM_CAPTCHA_QTD);
				
				/**
				* Segurança
				*/
				addCheckParam(parametros, Params.SECURITY_HOST_ON);
				addCheckParam(parametros, Params.SECURITY_IP_ON);
				addTextParam(parametros, Params.SECURITY_ALLOWED_ADMIN_HOST);
				addCheckParam(parametros, Params.SECURITY_WS_PATH_ON);
				addTextParam(parametros, Params.SECURITY_WS_PATH_ALLOWED);
				addTextParam(parametros, Params.SECURITY_ALLOWED_IP);
				addCheckParam(parametros, Params.SECURITY_USERAGENT_ON);
				addTextParam(parametros, Params.SECURITY_USERAGENT_WEB_ALLOWED);
				addTextParam(parametros, Params.SECURITY_USERAGENT_MOBILE_ALLOWED);
				addTextParam(parametros, Params.SECURITY_USERAGENT_WEB_BLOCKED);
				addTextParam(parametros, Params.SECURITY_USERAGENT_MOBILE_BLOCKED);
				addCheckParam(parametros, Params.WS_SECURITY_GET_ALLOWED);
				addCheckParam(parametros, Params.WS_SECURITY_TOKEN_ON);
				addCheckParam(parametros, Params.SECURITY_WS_CRYPT_ON);
				addCheckParam(parametros, Params.SECURITY_ATTACK_REPLAY_ON);
				addCheckParam(parametros, Params.WS_SECURITY_OTP_ON);
				
				/**
				* Segurança filters
				*/
				addCheckParam(parametros, Params.SECURITY_XSS_ON);
				addCheckParam(parametros, Params.SECURITY_CORS_ON);
				addTextParam(parametros, Params.SECURITY_CORS_ORIGIN);
				addTextParam(parametros, Params.SECURITY_UPLOAD_MIN_SIZE);
				addTextParam(parametros, Params.SECURITY_UPLOAD_MAX_SIZE);
				addTextParam(parametros, Params.SECURITY_UPLOAD_ALLOWED_EXTENSIONS);
				
				if (usuario == null) {
					usuario = new Usuario();
					usuario.setStatus(StatusUsuario.NOVO);
				}
				
				usuario.setEmpresa(empresa);

				usuario.setEmail(email);
				usuario.setLogin(email);
				usuario.setNome(nome);
				usuario.setPerfilAtivo(true);

				//Alteração de senha
				if (!StringUtils.isEmpty(tNovaSenha.getValue())) {
					
					if(StringUtils.equals(tNovaSenha.getValue(), tConfirmarSenha.getValue())){
						usuario.setSenha(CripUtil.criptToMD5(tNovaSenha.getValue()));
					} else {
						form.setError("Confirmação de senha errada");
						return true;
					}
					
				//Continua com a senha anterior ou set senha padrão
				} else {
					if (StringUtils.isEmpty(usuario.getSenha())) {
						String senha = ParametrosMap.getInstance(1L).get(Params.SENHA_PADRAO, "admin");
						usuario.setSenha(senha);

					}
				}
				
				setLogoEmpresa();
				setBgEmpresa();
				setFavicon();

				updateUsuariosByPeriodicidadeSenhaOn();
				empresaService.saveOrUpdate(empresa, usuario, getUsuario(), parametros);
				

				setFlashAttribute("msg", "Empresa [" + empresa.getNome() + "] salvo com sucesso.");
				if(StringUtils.isEmpty(tab1) && StringUtils.isEmpty(tab2)) {
					setRedirect(EmpresaPage.class, "id", String.valueOf(empresa.getId()));
				} else {
					Map<String, Object> params = new HashMap<>();
					params.put("id", empresa.getId());
					params.put("tab1", tab1);
					if(StringUtils.isNotEmpty(tab2)) {
						params.put("tab2", tab2);
					}
					setRedirect(EmpresaPage.class, params);
				}
				

				return true;

			} catch (Exception e) {
				logError(e.getMessage(), e);
				form.setError(e.getMessage());
			}
		}
		return false;
	}

	private void updateUsuariosByPeriodicidadeSenhaOn() {
		Long empresaId = getEmpresaId();
		Checkbox check = getCheckSimNao(Params.EXPIRAR_SENHA_PERIODICIDADE_ON);
		ParametrosMap parametrosMap = ParametrosMap.getInstance(empresaId);
		boolean periodicidadeOn = parametrosMap.getBoolean(Params.EXPIRAR_SENHA_PERIODICIDADE_ON, false);
		if(!periodicidadeOn && check.isChecked()){
			usuarioService.updateAllDataSenha(empresaId);
		}
	}

	private void addTextParam(List<Parametro> parametros, String name) {
		TextField t = getText(name);

		Parametro p = new Parametro();
		p.setNome(name);
		p.setValor(t.getValue());
		parametros.add(p);
	}

	private void addCheckParam(List<Parametro> parametros, String name) {
		Checkbox check = getCheckSimNao(name);
		Parametro p = new Parametro();
		p.setNome(name);
		p.setValor(check.isChecked() ? "1" : "0");
		parametros.add(p);

	}

	private void addCheckGrupoUsuarioParam(List<Parametro> parametros, String name) {
		Checkbox check = getCheckSimNao(name);
		Parametro p = new Parametro();
		p.setNome(name);
		p.setValor(check.isChecked() ? "grupo" : "usuario");

		parametros.add(p);

	}
	
	private void addCheckInformativoImpeditivo(List<Parametro> parametros, String name) {
		Checkbox check = getCheckSimNao(name);
		Parametro p = new Parametro();
		p.setNome(name);
		p.setValor(check.isChecked() ? "informativo" : "impeditivo");
		
		parametros.add(p);
		
	}

	private void setFavicon() throws FileNotFoundException, DomainMessageException, IOException {
		if (arquivoFavId != null) {
			Arquivo a = arquivoService.get(arquivoFavId);

			if (a != null) {
				UploadResponse info = UploadHelper.uploadFile(usuario, a, "fav", new Date().getTime());
				String url = info.arquivo.getUrl();
				empresa.setUrlFav(url);
			}
		}
	}

	private void setLogoEmpresa() throws IOException {
		if (arquivoLogoId != null) {
			Arquivo a = arquivoService.get(arquivoLogoId);
			if (a != null) {
				ArquivoThumb t = createThumbsArquivoLogo(a);
				empresa.setUrlLogo(t.getUrl());
			}
		}
	}

	private void setBgEmpresa() throws DomainMessageException, FileNotFoundException, IOException {
		if (arquivoBgId != null) {
			Arquivo a = arquivoService.get(arquivoBgId);

			if (a != null) {
				UploadResponse info = UploadHelper.uploadFile(usuario, a, "bg", new Date().getTime());
				empresa.setUrlBg(info.arquivo.getUrl());
			}
		}
	}

	public boolean cancelar() {
		setRedirect(EmpresasPage.class);
		return true;
	}

	protected Arquivo createThumbsArquivoFoto(Arquivo a) throws IOException {

		UploadHelper.log.debug("Arquivo: " + a.getId());

		UploadHelper.log.debug("Gerando thumbs arquivo: " + a.getNome());
		ThumbVO voThumb = UploadHelper.createThumbFotoUsuario(getParametrosMap(), usuario.getChave() + "/fotoProfile",
				a.getUrl(), a.getNome());
		ArquivoThumb t = new ArquivoThumb();
		t.setThumbVo(voThumb);
		t.setArquivo(a);
		arquivoService.saveOrUpdate(t);

		return a;
	}

	protected ArquivoThumb createThumbsArquivoLogo(Arquivo a) throws IOException {

		UploadHelper.log.debug("Arquivo: " + a.getId());

		UploadHelper.log.debug("Gerando thumbs arquivo: " + a.getNome());
		ThumbVO voThumb = UploadHelper.createThumbCustom(getParametrosMap(), a, 150, 37);
		ArquivoThumb t = new ArquivoThumb();
		t.setThumbVo(voThumb);
		t.setArquivo(a);
		arquivoService.saveOrUpdate(t);

		return t;
	}

	private void setParamDisplayTable() {
		paramDisplayTable.put(Params.BUILD_APP_ANDROID_URL, false);
		paramDisplayTable.put(Params.BUILD_APP_IOS_URL, false);
		paramDisplayTable.put(Params.WEB_TEMPLATE_TITLE, false);
		paramDisplayTable.put(Params.BORDER_COPYRIGHT_EMPRESA, false);
		paramDisplayTable.put(Params.MSG_PUSH_NEWPOST, false);
		paramDisplayTable.put(Params.PUSH_SERVER_PROJECT, false);
		paramDisplayTable.put(Params.PUSH_ON, false);
		paramDisplayTable.put(Params.CHAT_ON, false);
		paramDisplayTable.put(Params.COR_MENU, false);
		paramDisplayTable.put(Params.COR_BUSCA, false);
		paramDisplayTable.put(Params.COR_BADGE_SPAN, false);
		paramDisplayTable.put(Params.COR_BADGE_TEXT, false);
		paramDisplayTable.put(Params.COR_POST_AGENDADO, false);
		paramDisplayTable.put(Params.COR_POST_DESTACADO, false);
		paramDisplayTable.put(Params.COR_POST_EXPIRADO, false);
		paramDisplayTable.put(Params.COR_POST_PRIORITARIO, false);
	}

	@Override
	public void onRender() {
		super.onRender();

	}
}
