package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogImportacao;
import br.livetouch.livecom.domain.vo.LogImportacaoFiltro;
import br.livetouch.livecom.domain.vo.LogImportacaoVO;
import net.livetouch.tiger.ddd.DomainException;

public interface LogImportacaoService extends Service<LogImportacao> {

	List<LogImportacao> findAll(Empresa empresa);
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(LogImportacao f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(LogImportacao f) throws DomainException;
	
	List<LogImportacaoVO> findAllVO(Empresa empresa);
	
	List<String> findAllTiposCsv(Empresa empresa);

	List<LogImportacaoVO> findAllVOByFiltro(LogImportacaoFiltro filtro, Empresa empresa);
	
	List<LogImportacao> findAllByFiltro(LogImportacaoFiltro filtro, Empresa empresa);

	long countByFiltro(LogImportacaoFiltro filtro, Empresa empresa);
	
	String csvLogImportacao(Long id);

}
