package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.infra.util.Utils;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.LogTransacao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.exception.PeriodoBuscaInvalidoException;
import br.livetouch.livecom.domain.repository.LogRepository;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.LikeService;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.DeviceVO;
import br.livetouch.livecom.domain.vo.LogSistemaFiltro;
import br.livetouch.livecom.domain.vo.LogTransacaoFiltro;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVO;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioFuncionaisVO;
import br.livetouch.livecom.domain.vo.RelatorioLikesVO;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class LogServiceImpl implements LogService {
	protected static final Logger logger = Log.getLogger(LogService.class);

	@Autowired
	private LogRepository rep;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private LikeService likeService;
	
	@Autowired
	private EmpresaService empresaService;

	public List<LogSistema> findLogs(int page, Empresa empresa) {
		return rep.findLogsSistema(page, empresa);
	}

	@Override
	public LogSistema get(Long id) {
		return rep.get(id);
	}

	public List<LogSistema> findAll() {
		return rep.findAll();
	}

	@Override
	public void delete(LogSistema log) {
		rep.delete(log);
	}

	@Override
	public void delete(String hql) {
		rep.delete(hql);
	}

	@Override
	public long getLogTransacaoMaxId(Empresa empresa) {
		return rep.getLogTransacaoMaxId(empresa);
	}

	@Override
	public void saveOrUpdate(LogSistema l, Empresa empresa) {
		try {
			if(l != null && l.getMsg() != null) {
				if(empresa == null) {
					empresa = empresaService.get(1L);
				}
				l.setEmpresa(empresa);
				rep.saveOrUpdate(l);
			}
		} catch (HibernateException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public void saveOrUpdateNewTransaction(LogSistema l, Empresa empresa) {
		try {
			l.setEmpresa(empresa);
			rep.saveOrUpdate(l);
		} catch (HibernateException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public List<LogSistema> findbyFilter(LogSistemaFiltro log, int page, int max, Empresa empresa) throws DomainException {
		try {
			if (Utils.periodoBuscaValido(log)) {
				return rep.findbyFilter(log, page, max, empresa);
			}
		} catch (PeriodoBuscaInvalidoException e) {
			throw new PeriodoBuscaInvalidoException("exception.periodo.busca.invalido");
		}
		return null;

	}

	@Override
	public List<LogTransacao> findbyFilter(LogTransacaoFiltro log, Usuario usuarioSistema, int page, int max, Empresa empresa) throws DomainException {
		try {
			return rep.findbyFilter(log, usuarioSistema, page, max, empresa);
		} catch (PeriodoBuscaInvalidoException e) {
			throw new PeriodoBuscaInvalidoException("exception.periodo.busca.invalido");
		}
	}

	@Override
	public LogTransacao getLogTransacao(Long id) {
		return rep.getLogTransacao(id);
	}

	@Override
	public LogSistema getLogSistema(Long id) {
		return rep.getLogSistema(id);
	}

	@Override
	public void saveOrUpdate(LogTransacao log, Empresa empresa) {
		if(log != null) {
			rep.saveOrUpdate(log, empresa);
		}
	}

	@Override
	public void delete(LogTransacao log) {
		rep.delete(log);
	}

	@Override
	public String exportarLogsSistema(LogSistemaFiltro filtro, int max, Empresa empresa) throws DomainException {
		// String charset = Utils.CHARSET;
		List<LogSistema> logs = findbyFilter(filtro, -1, max, empresa);
		StringBuffer sb = new StringBuffer("id;dataHora;tipo;login;msg;codigo;codExterno");
		for (LogSistema l : logs) {
			sb.append("\r\n");
			sb.append(l.getId());
			sb.append(";");
			sb.append(l.getDataString());
			sb.append(";");
			sb.append(StringUtils.trim(l.getTipo() == null ? "" : l.getTipo().toString() + "  "));
			sb.append(";");
			sb.append(l.getLogin());
			sb.append(";");
			sb.append(l.getMsg());
			sb.append(";");
			sb.append(l.getCodigo());
			sb.append(";");
		}
		String s = sb.toString();
		return s;
	}

	@Override
	public LogTransacao findByOtp(Usuario usuario, String otp, Empresa empresa) {
		return rep.findByOtp(usuario, otp, empresa);
	}

	@Override
	public long getCountByFilter(LogTransacaoFiltro filtro, Usuario usuarioSistema, Empresa empresa) throws DomainException {
		return rep.getCountByFilter(filtro, usuarioSistema, empresa);
	}

	@Override
	public long getCountByFilter(LogSistemaFiltro filtro, Empresa empresa) throws DomainException {
		return rep.getCountByFilter(filtro, empresa);
	}

	@Override
	public HashMap<String, DeviceVO> getRegistrationIds(Empresa empresa) {

		HashMap<String, DeviceVO> map = new HashMap<String, DeviceVO>();
		List<DeviceVO> list = rep.getRegistrationIds(empresa);
		List<DeviceVO> listNull = new ArrayList<DeviceVO>();
		List<DeviceVO> listDuplicate = new ArrayList<DeviceVO>();

		for (DeviceVO vo : list) {
			if (vo.getRegistrationId().contains("registration_id=")) {
				String regId = StringUtils.substringBetween(vo.getRegistrationId(), "registration_id=", "&");

				if (StringUtils.isNotEmpty(regId)) {
					vo.setRegistrationId(regId);
					map.put(regId, vo);
					// regId duplicado
					if (map.containsKey(regId)) {
						listDuplicate.add(vo);
					}
				}
			} else {
				// sem regId
				listNull.add(vo);
			}
		}

		return map;
	}

	@Override
	public List<LogTransacao> findAllLogTransacao(Empresa empresa) {
		return rep.findAllLogTransacao(empresa);
	}

	@Override
	public List<RelatorioAcessosVersaoVO> findLogsByVersao(RelatorioFiltro filtro, Empresa empresa) throws DomainException {
		return rep.findLogsByVersao(filtro, empresa);
	}

	@Override
	public long getCountLogsByVersao(RelatorioFiltro filtro, Empresa empresa) throws DomainException {
		return rep.getCountLogsByVersao(filtro, empresa);
	}

	@Override
	public List<String> findAllVersoes(Empresa empresa) {
		return rep.findAllVersoes(empresa);
	}

	@Override
	public String csvAcessos(RelatorioFiltro filtro, Empresa empresa) throws DomainException {
		List<RelatorioAcessosVO> acessos = usuarioService.findbyFilter(filtro, false, empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("Data").add("Usuarios Logados").add("Usuarios Logados Não Repetidos");
		for (RelatorioAcessosVO a : acessos) {
			body.add(a.getDate()).add(String.valueOf(a.getLogados())).add(String.valueOf(a.getDistinctLogados()));
			body.end();
		}
		return generator.toString();
	}

	@Override
	public String csvAcessosVersao(RelatorioFiltro filtro, Empresa empresa) throws DomainException {
		filtro.setEmpresaId(empresa.getId());
		List<RelatorioAcessosVersaoVO> acessos = usuarioService.findAcessosByVersao(filtro);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("Plataforma").add("Versão").add("Usuarios na versão");
		for (RelatorioAcessosVersaoVO a : acessos) {
			body.add(a.getDeviceSo()).add(a.getVersao()).add(String.valueOf(a.getLogados()));
			body.end();
		}
		return generator.toString();
	}

	@Override
	public String csvLikes(RelatorioFiltro filtro, Empresa empresa) throws DomainException {
		List<RelatorioLikesVO> likes = likeService.findbyFilter(filtro, false);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("Data").add("Post").add("Usuário");
		for (RelatorioLikesVO l : likes) {
			body.add(l.getData()).add(l.getTitulo() != null ? l.getTitulo() : "-").add(l.getUsuario() != null ? l.getUsuario() : "-");
			body.end();
		}
		return generator.toString();
	}

	@Override
	public String csvFuncionais(RelatorioFiltro filtro, Empresa empresa) throws DomainException {
		List<RelatorioFuncionaisVO> funcionais = usuarioService.getFuncionais(filtro, false, empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("Funcional");
		for (RelatorioFuncionaisVO f : funcionais) {
			body.add(f.getLogin());
			body.end();
		}
		return generator.toString();
	}

	@Override
	public String csvTransacao(LogTransacaoFiltro filtro, Usuario usuario, int page, int max, Empresa empresa) throws DomainException {
		List<LogTransacao> logs = findbyFilter(filtro, usuario, page, max, empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("Id").add("Login").add("Request Path").add("Data e Hora").add("Status");
		for (LogTransacao l : logs) {
			body.add(String.valueOf(l.getId()));
			body.add(l.getLogin());
			body.add(l.getRequestPath());
			body.add(l.getDataFimString());
			body.add(l.getStatus().name());
			body.end();
		}
		return generator.toString();
	}

	@Override
	public List<LogSistema> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	@Override
	public String csvDetalhesAcessoVersao(RelatorioFiltro filtro, Empresa empresa) throws DomainException {
		List<RelatorioAcessosVersaoVO> findLogsByVersao = findLogsByVersao(filtro, empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("Data").add("Login").add("Versão").add("Device SO").add("Device Versão");
		for (RelatorioAcessosVersaoVO vo : findLogsByVersao) {
			body.add(vo.getData() != null ? vo.getData() : "").add(vo.getLogin() != null ? vo.getLogin() : "").add(vo.getVersao() != null ? vo.getVersao() : "").add(vo.getDeviceSo() != null ? vo.getDeviceSo() : "")
					.add(vo.getDeviceSoVersion() != null ? vo.getDeviceSoVersion() : "");
			body.end();
		}
		return generator.toString();
	}

}
