package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.EmailReport;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.vo.RelatorioEmailVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public interface EmailReportRepository extends net.livetouch.tiger.ddd.repository.Repository<EmailReport> {

	List<EmailReport> findAll(Empresa e);

	List<RelatorioEmailVO> reportEmail(RelatorioFiltro filtro) throws DomainException;
	
}