package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.vo.FileVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ArquivosByIdPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private LongField tId;
	private TextField tMode;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tId = new LongField("id", true));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		tId.setFocus(true);

		form.add(new Submit("consultar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Long id  = tId.getLong();
			
			if(id == null) {
				return new MensagemResult("NOK","Id não pode ser nulo");
			}

			Arquivo arq = arquivoService.get(id);
			
			if(arq == null) {
				return new MensagemResult("NOK","Nenhum arquivo encontrado");
			}
			
			FileVO vo = new FileVO();
			vo.setArquivo(arq, false);
			
			return vo;
		}
		
		return new MensagemResult("NOK","Erro ao buscar arquivos.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("arquivos", FileVO.class);
		super.xstream(x);
	}
}
