package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.vo.RelatorioConversaFiltro;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public interface MensagemConversaRepository extends net.livetouch.tiger.ddd.repository.Repository<MensagemConversa> {

	List<MensagemConversa> findConversasByFilter(RelatorioConversaFiltro filtro) throws DomainException;

	long getCountVersaoByFilter(RelatorioConversaFiltro filtro) throws DomainException;

	MensagemConversa findByGrupo(Grupo g);
	
	boolean isTodasMensagensLidas(Long conversaId, Long userId, boolean isValidateGroup);

	boolean isTodasEntrege(Long conversaId, Long userId);

	List<Long> findIdsConversasLidasById(List<Long> ids, Long userId);
}