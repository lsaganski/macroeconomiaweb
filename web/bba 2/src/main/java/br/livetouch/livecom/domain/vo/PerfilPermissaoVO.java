package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.Permissao;

public class PerfilPermissaoVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private PerfilVO perfil;
	private PermissaoVO permissao;
	private boolean ligado;
	
	public PerfilPermissaoVO() {}

	public PerfilPermissaoVO(Permissao permissao, boolean ligado) {
		this.permissao = new PermissaoVO(permissao);
		this.ligado = ligado;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PerfilVO getPerfil() {
		return perfil;
	}

	public void setPerfil(PerfilVO perfil) {
		this.perfil = perfil;
	}

	public PermissaoVO getPermissao() {
		return permissao;
	}

	public void setPermissao(PermissaoVO permissao) {
		this.permissao = permissao;
	}

	public boolean isLigado() {
		return ligado;
	}

	public void setLigado(boolean ligado) {
		this.ligado = ligado;
	}

}
