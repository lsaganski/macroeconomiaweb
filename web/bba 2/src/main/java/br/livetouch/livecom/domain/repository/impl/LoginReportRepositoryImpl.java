package br.livetouch.livecom.domain.repository.impl;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.LoginReport;
import br.livetouch.livecom.domain.repository.LoginReportRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class LoginReportRepositoryImpl extends StringHibernateRepository<LoginReport> implements LoginReportRepository {

	public LoginReportRepositoryImpl() {
		super(LoginReport.class);
	}
}