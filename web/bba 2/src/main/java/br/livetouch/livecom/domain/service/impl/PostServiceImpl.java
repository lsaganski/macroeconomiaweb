package br.livetouch.livecom.domain.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.infra.util.Utils;
import br.livetouch.livecom.domain.Amizade;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Favorito;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Likes;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Playlist;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostDestaque;
import br.livetouch.livecom.domain.PostIdioma;
import br.livetouch.livecom.domain.PostTask;
import br.livetouch.livecom.domain.PostUsuarios;
import br.livetouch.livecom.domain.Rate;
import br.livetouch.livecom.domain.StatusPost;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.enums.GrupoNotification;
import br.livetouch.livecom.domain.enums.StatusAmizade;
import br.livetouch.livecom.domain.enums.StatusPublicacao;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.enums.Visibilidade;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.repository.ComentarioRepository;
import br.livetouch.livecom.domain.repository.PostDestaqueRepository;
import br.livetouch.livecom.domain.repository.PostRepository;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.ComentarioService;
import br.livetouch.livecom.domain.service.FavoritoService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.IdiomaService;
import br.livetouch.livecom.domain.service.LikeService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.PalavraService;
import br.livetouch.livecom.domain.service.PlaylistService;
import br.livetouch.livecom.domain.service.PostIdiomaService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.PostTaskService;
import br.livetouch.livecom.domain.service.PushReportService;
import br.livetouch.livecom.domain.service.RateService;
import br.livetouch.livecom.domain.service.StatusPostService;
import br.livetouch.livecom.domain.service.TagService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.BuscaPost;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.NotificationJobVO;
import br.livetouch.livecom.domain.vo.PostCountVO;
import br.livetouch.livecom.domain.vo.PostInfoVO;
import br.livetouch.livecom.domain.vo.PostTaskVO;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.livecom.jobs.InsertNotificationsJob;
import br.livetouch.livecom.jobs.JobUtil;
import br.livetouch.livecom.jobs.info.NotificationInfo;
import br.livetouch.livecom.utils.DateUtils;
import net.livetouch.hibernate.HibernateUtil;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class PostServiceImpl extends LivecomService<Post> implements PostService {
	protected static final Logger log = Log.getLogger(PostService.class);

	@Autowired
	private LikeService likeService;

	@Autowired
	private ComentarioService comentarioService;

	@Autowired
	private PostRepository rep;

	@Autowired
	private PostDestaqueRepository repDestaque;

	@Autowired
	private ArquivoService arquivoService;

	@Autowired
	private FavoritoService favoritoService;

	@Autowired
	private RateService rateService;

	@Autowired
	private PostTaskService postTaskService;

	@Autowired
	private PostIdiomaService postIdiomaService;

	@Autowired
	private IdiomaService idiomaService;

	@Autowired
	private StatusPostService statusPostService;

	@Autowired
	private PlaylistService playlistService;

	@Autowired
	private ComentarioRepository comentarioRep;

	@Autowired
	protected PalavraService palavraService;

	@Autowired
	protected NotificationService notificationService;

	@Autowired
	protected TagService tagService;

	@Autowired
	protected GrupoService grupoService;

	@Autowired
	protected UsuarioService usuarioService;

	@Autowired
	protected PushReportService pushReportService;

	@Override
	public Post get(Long id) {
		return rep.get(id);
	}

	@Override
	public Post post(Post p, Usuario userPost, PostInfoVO info, boolean rascunho) throws DomainException, IOException {
		boolean insert = p.getId() == null;
		
		Empresa empresa = userPost.getEmpresa();
		ParametrosMap params = ParametrosMap.getInstance(empresa);

		List<Long> arquivoIds = info.arquivoIds;
		PostDestaque destaque = info.destaque;
		String dataPublicacao = info.dataPublicacao;
		String horaPublicacao = info.horaPublicacao;
		String dataExpiracao = info.dataExpiracao;
		String horaExpiracao = info.horaExpiracao;
		String dataReminder = info.dataReminder;
		String horaReminder = info.horaReminder;
		List<Long> tagsIds = info.tagsIds;
		String tags = info.tags;
		List<Long> grupoIds = info.grupoIds;
		List<String> grupoCods = info.grupos;
		Long statusPost = info.statusPost;

		Long postId;

		boolean censurar = params.getBoolean(Params.POST_CENSURA_ON, false);
		if(censurar) {
			List<String> censura = palavraService.censurar(p);
			if (censura.size() == 1) {
				throw new DomainMessageException("Post CENSURADO pelo uso indevido da palavra [" + censura.get(0) + "]");
			} else if (censura.size() > 1) {
				String aux = "";
				for (String string : censura) {
					if (censura.get(0) == string) {
						aux = string;
					} else {
						aux += ", " + string;
					}
				}
				throw new DomainMessageException("Post CENSURADO pelo uso indevido das palavras [" + aux.trim() + "]");
			}
		}

		// Data Publicacao
		if (dataPublicacao != null) {
			if (StringUtils.isNotEmpty(horaPublicacao)) {
				// Mandou a hora
				p.setDataPublicacao(dataPublicacao + " " + horaPublicacao + ":00");
			} else {
				// Data sem a hora
				Date dataPub = DateUtils.toDate(dataPublicacao, "dd/MM/yyyy");

				if (DateUtils.isToday(dataPub.getTime())) {
					// Se for hoje, pega hora do sistema
					Calendar now = Calendar.getInstance();
					int hour = now.get(Calendar.HOUR_OF_DAY);
					int minute = now.get(Calendar.MINUTE);
					int second = now.get(Calendar.SECOND);
					p.setDataPublicacao(dataPublicacao + " " + hour + ":" + minute + ":" + second);
				} else {
					// Se não for hoje, entra como meia noite
					p.setDataPublicacao(dataPublicacao + " " + "00:00:00");
				}
			}
		}

		// Data Expiracao
		if (dataExpiracao != null && StringUtils.isNotEmpty(dataExpiracao)) {
			if (horaExpiracao != null && StringUtils.isNotEmpty(horaExpiracao)) {
				p.setDataExpiracao(dataExpiracao + " " + horaExpiracao + ":00");
			} else {
				p.setDataExpiracao(dataExpiracao + " 00:00:00");
			}
		}
		
		// Data Reminder
		Boolean reminder = false;
		if (StringUtils.isNotEmpty(dataReminder)) {
			
			Date oldReminder = p.getReminder();
			
			if (StringUtils.isNotEmpty(horaReminder)) {
				p.setReminderString(dataReminder + " " + horaReminder + ":00");
			} else {
				p.setReminderString(dataReminder + " 00:00:00");
			}
			
			if(oldReminder != null) {
				if(!DateUtils.isIgual(oldReminder, p.getReminder(), "dd/MM/yyyy HH:mm")) {
					reminder = true;
				}
			} else {
				reminder = true;
			}
		}
		
		if(!reminder && p.getId() != null) {
			Notification not = notificationService.findByPost(p, TipoNotificacao.REMINDER);
			if(not != null) {
				NotificationInfo.getInstance().remove(not);
				notificationService.delete(not);
			}
		}

		// Destaque
		p.setPostDestaque(destaque);

		// Tags
		List<Tag> tagsList = tagService.findAllByIds(tagsIds, empresa);
		if (tagsList.isEmpty()) {
			// o mobile pode mandar por nome
			if (!StringUtils.equals("undefined", tags)) {
				List<Tag> tagsList2 = tagService.getTagsAndSave(tags, empresa);
				if (!tagsList2.isEmpty()) {
					tagsList = tagsList2;
				}
			}
		}
		p.setTagsList(new HashSet<Tag>(tagsList));

		// Grupos
		List<Grupo> grupos = grupoService.findAllByIds(grupoIds);
		grupos.addAll(grupoService.findAllByCodigos(grupoCods));
		p.setGrupos(new HashSet<Grupo>(grupos));
		
		if(statusPost != null) {
			StatusPost status = statusPostService.get(statusPost);
			if(status != null) {
				p.setStatusPost(status);
			}
		}

		p.setPrioritario(info.prioritario);
		p.setWebview(info.webview);
		p.setHtml(info.html);

		// Save
		saveOrUpdate(userPost, p);

		auditSave(p, userPost, insert);
		
		//Idiomas
		Set<PostIdioma> idiomas = p.getIdiomas();
		if (idiomas != null && !idiomas.isEmpty()) {
			for (PostIdioma postIdioma : idiomas) {
				Idioma idioma = postIdioma.getIdioma();
				Idioma i = idiomaService.get(idioma.getId());
				
				postIdioma.setIdioma(i);
				postIdioma.setPost(p);
				postIdiomaService.saveOrUpdate(postIdioma);
			}
			p.setIdiomas(idiomas);
		}
		
		log.debug("Deleta os arquivos do post que não vieram na request");
		List<Long> idsDelete = arquivoService.getIdsArquivosNotIn(p, arquivoIds);
		if(idsDelete != null && idsDelete.size() > 0) {
			arquivoService.delete(userPost, idsDelete, false);
		}
		
		
		// Arquivos
		log.debug("arquivoIds: " + arquivoIds);
		if (arquivoIds != null && !arquivoIds.isEmpty()) {
			List<Arquivo> arquivosTemp = arquivoService.findAllByIds(arquivoIds);
			if (arquivosTemp != null && !arquivosTemp.isEmpty()) {
				log.debug("Arquivos: " + arquivosTemp);

				List<Arquivo> arquivos = new ArrayList<Arquivo>();

				if (!insert) {
					// Post já existia, verifica arquivos ids
					Set<Arquivo> arquivosPosts = p.getArquivos();
					if (arquivosPosts != null && arquivosPosts.size() > 0) {
						log.debug("Limpando/Validando arquivos antigos do Post");
						for (Arquivo a : arquivosPosts) {
							if (!arquivosTemp.contains(a)) {
								a.setPost(null);
								arquivoService.saveOrUpdate(a);
								log.debug("Desvinculando arquivo do post: " + a);
							}
						}
					}
				}

				// Arquivos
				Long destaqueId = info.arquivoDestaqueId;
				for (Arquivo arquivo : arquivosTemp) {
					if (insert) {
						if (arquivo.getPost() != null) {
							arquivo = arquivoService.duplicar(arquivo, destaqueId);
						} else {
							arquivoService.setDestaque(destaqueId, arquivo);
						}
					} else {
						Post post = arquivo.getPost();
						if (post != null && !post.getId().equals(p.getId())) {
							arquivo = arquivoService.duplicar(arquivo, destaqueId);
						} else {
							arquivoService.setDestaque(destaqueId, arquivo);
						}
					}

					// Associa arquivos com POST
					log.debug("Atualizando arquivo [" + arquivo.toStringDesc() + "] para post [" + p.toStringDesc() + "]");
					arquivo.setPost(p);
					arquivo.setUsuario(userPost);
					arquivo.setTags(new HashSet<Tag>(tagsList));
					arquivoService.saveOrUpdate(arquivo);
					log.debug("Arquivo atualizado");
					arquivos.add(arquivo);
				}

				// Salva Post e Arquivos
				Set<Arquivo> arquivosSet = new HashSet<Arquivo>(arquivos);
				p.setArquivos(arquivosSet);
				saveOrUpdate(userPost, p);
			} else {
				log.debug("Nenhum arquivo encontrado para ids: " + arquivoIds);
			}
		} else {
			Set<Arquivo> arquivosPosts = p.getArquivos();
			if (arquivosPosts != null) {
				for (Arquivo a : arquivosPosts) {
					a.setPost(null);
					arquivoService.delete(a);
				}
				p.setArquivos(null);
				saveOrUpdate(userPost, p);
			}
		}

		log.debug("UPDATE Post [" + p.getTitulo() + "] publicado com sucesso: " + p);

		postId = p.getId();

		// Get
		p = get(postId);

		if (p.isRascunho()) {
			return p;
		}

		boolean checkSendPush = info.push;
		boolean pushOn = params.isPushOn();

		boolean sendPush = pushOn && checkSendPush;
		if(!p.isRascunho()) {

			if (insert || sendPush) {
				push(p, userPost, insert, sendPush);
			}
			
			if (reminder) {
				reminder(p, userPost, insert, sendPush);
			}
			
		}
		

		// Limpa cache hibernate
		HibernateUtil.clearCacheRegion("BUSCA_NOTICAS");

		return p;
	}

	protected void auditSave(Post p, Usuario userPost, boolean insert) {
		p.translate();
		if (insert) {
			log.debug("Salvando post: " + p.getTitulo() + ", newId: [" + p.getId() + "]");
		} else {
			log.debug("Atualizando post [" + p.getId() + "]: " + p.getTitulo());
		}

		// Loga auditoria
		AuditoriaAcao acao = insert ? AuditoriaAcao.INSERIR : AuditoriaAcao.EDITAR;
		String msg = null;
		if (insert) {
			msg = "Usuário " + userPost.getNome() + " postou um comunicado " + p.getTitulo();
		} else {
			msg = "Usuário " + userPost.getNome() + " editou um comunicado " + p.getTitulo();
		}
		LogAuditoria.log(userPost, p, AuditoriaEntidade.POST, acao, msg);
	}

	private void push(Post p, Usuario userPost, boolean newPost, boolean sendPush) throws DomainException, IOException {
		Notification n = null;
		ParametrosMap params = ParametrosMap.getInstance(userPost.getEmpresa());
		
		boolean newNotification = params.getBoolean(Params.PUSH_POST_CREATE_NOTIFICATION_UPDATE, false);
		
		if (!newPost && !newNotification) {
			n = notificationService.findByPost(p, TipoNotificacao.NEW_POST);
		}

		if (n == null) {
			n = new Notification();
		}

		Set<PostIdioma> idiomas = p.getIdiomas();
		if(idiomas == null || idiomas.isEmpty()) {
			createNotification(p, userPost, newPost, sendPush, n, params, null);
			return;
		} 
		
		for (PostIdioma pi : idiomas) {
			createNotification(p, userPost, newPost, sendPush, n, params, pi);
		}
		
	}

	private void createNotification(Post p, Usuario userPost, boolean newPost, boolean sendPush, Notification n, ParametrosMap params, PostIdioma postIdioma) throws DomainException {
		String texto = null;
		String tituloNot = null;
		String subTituloNot = null;

		if (newPost) {
			texto = params.get(Params.PUSH_MSG_NEW_POST, "%user% postou um novo comunicado %titulo%");
			tituloNot = params.get(Params.PUSH_COMUNICADO_NEW_TITULO_NOTIFICATION, "Novo Comunicado");
			subTituloNot = params.get(Params.PUSH_COMUNICADO_NEW_SUB_TITULO_NOTIFICATION, "[%user%] postou um novo comunicado [%titulo%]");
		} else {
			texto = params.get(Params.PUSH_MSG_UPDATE_POST, "%user% atualizou o comunicado %titulo%");
			tituloNot = params.get(Params.PUSH_COMUNICADO_UPDATE_TITULO_NOTIFICATION, "Comunicado Atualizado");
			subTituloNot = params.get(Params.PUSH_COMUNICADO_UPDATE_SUB_TITULO_NOTIFICATION, "[%user%] atualizou o comunicado [%titulo%]");
		}

		String tituloDesc = postIdioma != null ? postIdioma.getTituloDesc() : p.getTituloDesc();
		String titulo = postIdioma != null ? postIdioma.getTitulo() : p.getTitulo();

		if (StringUtils.isNotEmpty(texto)) {
			texto = StringUtils.replace(texto, "%user%", userPost.getNome());
			texto = StringUtils.replace(texto, "%titulo%", tituloDesc);
		}

		if (StringUtils.isNotEmpty(subTituloNot)) {
			subTituloNot = StringUtils.replace(subTituloNot, "%user%", userPost.getNome());
			subTituloNot = StringUtils.replace(subTituloNot, "%titulo%", tituloDesc);
		}

		String textoNotification = userPost.getNome() + " publicou " + titulo;
		
		n.setData(new Date());
		n.setDataPublicacao(p.getDataPublicacao());
		n.setTipo(TipoNotificacao.NEW_POST);
		n.setGrupoNotification(GrupoNotification.MURAL);
		n.setTitulo(titulo);
		n.setTexto(texto);
		n.setPost(p);
		n.setTextoNotificacao(textoNotification);
		n.setTituloNot(tituloNot);
		n.setSubTituloNot(subTituloNot);
		n.setUsuario(userPost);
		n.setIdioma(postIdioma.getIdioma());
		

		// Salva a notificação parent
		notificationService.save(n);
		
		
		boolean autorReceivePush = params.getBoolean(Params.PUSH_POST_AUTOR_RECEBE_ON, false);
		NotificationJobVO not = new NotificationJobVO(p, autorReceivePush, userPost, n, sendPush);
		NotificationInfo.getInstance().addNotification(not);
		
		boolean fastMode = ParametrosMap.getInstance().getBoolean(Params.PUSH_FAST_MODE,false);
		if(fastMode) {
			JobUtil.executeJob(applicationContext, InsertNotificationsJob.class, null);
		}
	}
	
	private void reminder(Post p, Usuario userPost, boolean newPost, boolean sendPush) throws DomainException, IOException {
		Notification n = null;
		ParametrosMap params = ParametrosMap.getInstance(userPost.getEmpresa());
		sendPush = true;
		
		if(p != null) {
			n = notificationService.findByPost(p, TipoNotificacao.REMINDER);
		}

		if (n == null) {
			n = new Notification();
		} else {
			notificationService.deleteFilhas(n);
		}

		Set<PostIdioma> idiomas = p.getIdiomas();
		if(idiomas == null || idiomas.isEmpty()) {
			createReminder(p, userPost, sendPush, n, params, null);
			return;
		} 
		
		for (PostIdioma pi : idiomas) {
			createReminder(p, userPost, sendPush, n, params, pi);
		}
	}

	private void createReminder(Post p, Usuario userPost, boolean sendPush, Notification n, ParametrosMap params, PostIdioma postIdioma)
			throws DomainException {
		String texto = null;
		String tituloNot = null;
		String subTituloNot = null;

		texto = params.get(Params.PUSH_MSG_REMINDER, "%user% adicionou um novo reminder para o comunicado %titulo%");
		tituloNot = params.get(Params.PUSH_REMINDER_NEW_TITULO_NOTIFICATION, "Novo Reminder");
		subTituloNot = params.get(Params.PUSH_REMINDER_NEW_SUB_TITULO_NOTIFICATION, "[%user%] adicionou um novo reminder para o comunicado [%titulo%]");

		String titulo = postIdioma != null ? postIdioma.getTitulo() : p.getTitulo();
		
		texto = StringUtils.replace(texto, "%user%", userPost.getNome());
		texto = StringUtils.replace(texto, "%titulo%", titulo);

		subTituloNot = StringUtils.replace(subTituloNot, "%user%", userPost.getNome());
		subTituloNot = StringUtils.replace(subTituloNot, "%titulo%", titulo);

		String textoNotification = userPost.getNome() + " publicou " + titulo;
		
		n.setData(new Date());
		n.setDataPublicacao(p.getDataPublicacao());
		n.setTipo(TipoNotificacao.REMINDER);
		n.setGrupoNotification(GrupoNotification.MURAL);
		n.setTitulo(titulo);
		n.setTexto(texto);
		n.setPost(p);
		n.setTextoNotificacao(textoNotification);
		n.setTituloNot(tituloNot);
		n.setSubTituloNot(subTituloNot);
		n.setUsuario(userPost);
		n.setSendPush(sendPush);
		

		// Salva a notificação parent
		notificationService.save(n);
	}

	/**
	 * @param userPost
	 * @param post
	 * @return
	 */
	public List<Long> getUsuariosPushPost(Post post, Idioma idioma) {

		log.debug("Post: " + post.getId() + ":" + post.getTitulo());
		Set<Long> list = new LinkedHashSet<>();

		if (post != null) {
			
			Visibilidade visibilidade = post.getVisibilidade() != null ? post.getVisibilidade() : Visibilidade.GRUPOS;
			
			List<Long> idsAmigos = usuarioService.findIdsAmigos(post.getUsuario());
			if(idsAmigos.isEmpty() && visibilidade.equals(Visibilidade.MEUS_AMIGOS)) {
				return new ArrayList<>(list);
			}
			
			Set<Grupo> grupos = new HashSet<Grupo>();
			List<Grupo> subgrupos = grupoService.findSubgruposInGrupos(post.getGrupos());
			grupos.addAll(subgrupos);
			grupos.addAll(post.getGrupos());
			
			if(grupos.isEmpty() && !visibilidade.equals(Visibilidade.SOMENTE_EU)) {
				List<Long> listUsers = usuarioService.findUsuariosToPush(post, idsAmigos, idioma);
				if (listUsers.size() > 0) {
					list.addAll(listUsers);
				}
			}
			
			for (Grupo g : grupos) {
				
				if(g.isGrupoVirtual()) {
					continue;
				}
				
				List<Long> listUsers = grupoService.findUsuariosToPush(g, post, idioma);
				if (listUsers.size() > 0) {
					list.addAll(listUsers);
				}
			}
		}

		return new ArrayList<>(list);
	}
	
	public List<Long> getUsuariosReminder(Post post, Idioma idioma) {

		log.debug("Post: " + post.getId() + ":" + post.getTitulo());
		Set<Long> list = new LinkedHashSet<>();
		
		post = get(post.getId());

		if (post != null) {
			
			if (post.getGrupos() == null) {
				return new ArrayList<Long>();
			}
			
			
			Set<Grupo> grupos = new HashSet<Grupo>();
			List<Grupo> subgrupos = grupoService.findSubgruposInGrupos(post.getGrupos());
			grupos.addAll(subgrupos);
			grupos.addAll(post.getGrupos());
			
			for (Grupo g : grupos) {
				
				if(g.isGrupoVirtual()) {
					continue;
				}
				
				List<Long> listUsers = grupoService.findUsuariosReminder(g, post);
				if (listUsers.size() > 0) {
					list.addAll(listUsers);
				}
			}
		}

		return new ArrayList<>(list);
	}

	@Override
	public void saveOrUpdate(Usuario userInfo, Post p) throws DomainException {
		if (p.getPostDestaque() != null) {
			repDestaque.saveOrUpdate(p.getPostDestaque());
		}

		boolean create = p.getId() == null;
		
		if (create) {
			p.setData(new Date());
			p.setUsuario(userInfo);
			p.setStatusPublicacao(StatusPublicacao.EMPTY);
		}

		if (p.getDataPublicacao() == null) {
			p.setDataPublicacao(new Date());
			p.setStatusPublicacao(StatusPublicacao.PUBLICADO);
		} else {
			Date dtPub = p.getDataPublicacao(); 
			boolean maior = DateUtils.isMaior(dtPub, new Date(),"yyyyMMddHHmm");
			if(maior) {
				p.setStatusPublicacao(StatusPublicacao.AGENDADO);
			} else {
				p.setStatusPublicacao(StatusPublicacao.PUBLICADO);
			}
		}

		// Save
		p.setDataUpdated(new Date());
		if (userInfo != null) {
			p.setUsuarioUpdate(userInfo);
		}

		try {
			rep.saveOrUpdate(p);
			
			if(p.getPostDestaque() != null) {
				PostDestaque postDestaque = p.getPostDestaque();
				postDestaque.setPost(p);
				repDestaque.saveOrUpdate(postDestaque);
			}
			
		} catch (DataException e) {
			log.error(e.getMessage(), e);
			throw new DomainException("Ocorreu um erro ao salvar o post: " + e.getMessage(), e);
		} catch (DataIntegrityViolationException e) {
			log.error(e.getMessage(), e);
			throw new DomainException("Ocorreu um erro ao salvar o post: " + e.getMessage(), e);
		}
	}

	@Override
	public void saveOrUpdate(Usuario userInfo, PostDestaque postDestaque) {
		repDestaque.saveOrUpdate(postDestaque);
	}

	@Override
	public List<Post> findAll() {
		return rep.findAll(true);
	}

	@Override
	public List<Post> findAllByDate(Date data) {
		return rep.findAllByDate(data);
	}

	@Override
	public void delete(Usuario userInfo, Post p, boolean force) throws DomainException {
		delete(userInfo, p, false, force);
	}

	@Override
	public void delete(Usuario userInfo, Post p, boolean deleteArquivosAmazon, boolean force) throws DomainException {
		// Auditoria
		LogAuditoria auditoria = null;
		if (!force) {
			if (userInfo != null) {
				String msg = "Usuário " + userInfo.getNome() + " deletou um comunicado " + p.getTitulo();
				auditoria = LogAuditoria.log(userInfo, p, AuditoriaEntidade.POST, AuditoriaAcao.DELETAR, msg, false);
			}
		}

		List<Long> ids = new ArrayList<>();
		ids.add(p.getId());
		delete(userInfo, ids, deleteArquivosAmazon, force);

		// OK
		if(auditoria != null) {
			auditoria.finish();
		}
	}

	@Override
	public void delete(Usuario userInfo, List<Long> ids, boolean deleteArquivosAmazon, boolean force) throws DomainException {
		if (ids == null || ids.isEmpty()) {
			return;
		}

		Post p = null;

		try {
			executeNativeIn("delete from post_tags where post_id in (:ids)", "ids", ids);
			executeNativeIn("delete from post_grupos where post_id in (:ids)", "ids", ids);
			executeNativeIn("delete from post_usuarios where post_id in (:ids)", "ids", ids);
			executeIn("delete from Likes where post.id in (:ids)", "ids", ids);
			executeIn("delete from Favorito where post.id in (:ids)", "ids", ids);
			executeIn("delete from Historico where post.id in (:ids)", "ids", ids);
			executeIn("delete from Playlist where post.id in (:ids)", "ids", ids);
			executeIn("delete from Rate where post.id in (:ids)", "ids", ids);
			executeIn("delete from PushReport where post.id in (:ids)", "ids", ids);
			executeIn("delete from PostView where post.id in (:ids)", "ids", ids);
			executeIn("delete from PostTask where post.id in (:ids)", "ids", ids);
			executeIn("delete from UsuarioAcessoMural where post.id in (:ids)", "ids", ids);
			executeIn("delete from PostIdioma where post.id in (:ids)", "ids", ids);

			notificationService.deleteBy(Post.class, ids);
			
			// Arquivos
			List<Long> idsArquivos = queryIdsIn("select id from Arquivo where post.id in (:ids)",false,"ids", ids);
			arquivoService.delete(userInfo, idsArquivos, deleteArquivosAmazon);
			
			// Comentarios
			List<Long> idsComentarios = queryIdsIn("select id from Comentario where post.id in (:ids)",false,"ids", ids);
			comentarioService.delete(userInfo, idsComentarios, deleteArquivosAmazon);
			
			// Delete
			List<Long> idsDestaque = queryIdsIn("select p.postDestaque.id from Post p where p.id in (:ids)",false,"ids", ids);
			executeIn("UPDATE PostDestaque p set  p.post = null WHERE p.id in(:ids)", "ids", idsDestaque);
			executeIn("delete Post where id in (:ids)","ids", ids);
			executeIn("delete PostDestaque where id in (:ids)","ids", idsDestaque);
			
		} catch (Exception e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if (root == null) {
				root = e;
			}
			log.error("Erro ao excluir o post [" + p + "]: " + root.getMessage(), root);

			throw new DomainException("Não foi possível excluir o post [" + p + "]");
		}
	}

	@Override
	public void delete(Usuario userInfo, PostDestaque c) {
		repDestaque.delete(c);
	}
	
	@Override
	public List<PostVO> toListVo(Usuario user, List<Post> posts) {
		return toListVo(user, posts, user.getIdioma());
	}

	@Override
	public List<PostVO> toListVo(Usuario user, List<Post> posts, Idioma idioma) {
		List<Long> ids = Post.getIds(posts);

		// Se o user info favoritou estes posts
		List<Favorito> favsOfPosts = favoritoService.findAllByPosts(user, ids);

		// Se o user info fez like nestes posts;
		List<Likes> likeOfPosts = likeService.findAllByPosts(user, ids);

		// Qtde de likes por posts.
		List<PostCountVO> countLikesByuser = likeService.findCountByPosts(ids);

		// Qtde de comentarios por posts.
		List<PostCountVO> countComentariosByUser = comentarioRep.findCountByPosts(ids);

		// Badges de comentarios
		List<PostCountVO> badgesComentarios = comentarioRep.findBadgesByPosts(user, ids);

		// Configurações notificação
		List<PostUsuarios> postUsuarios = this.findPostUsuarios(user, posts);

		List<PostVO> list = new ArrayList<PostVO>();
		for (Post p : posts) {
			if (p != null) {
				
				PostVO vo = setPost(p, user, idioma);

				vo.setDestaque(p.getPostDestaque());
				
				PostTask postTask = postTaskService.findByUserPost(user, p);
				if(postTask != null) {
					vo.postTask = new PostTaskVO(postTask);
				}

				for (Favorito f : favsOfPosts) {
					if (f.getPost().getId().equals(p.getId())) {
						vo.favorito = (f != null && f.isFavorito()) ? "1" : "0";
						break;
					}
				}

				for (Likes l : likeOfPosts) {
					if (l.getPost().getId().equals(p.getId())) {
						vo.like = (l != null && l.isFavorito()) ? "1" : "0";
						break;
					}
				}

				for (PostCountVO c : countComentariosByUser) {
					if (c.getPost().getId().equals(p.getId())) {
						vo.commentCount = c.getCount();
						break;
					}
				}

				for (PostCountVO c : badgesComentarios) {
					Post post = c.getPost();
					if (post != null && post.getId().equals(p.getId())) {
						vo.badgeComentario = c.getCount();
						break;
					}
				}

				for (PostCountVO c : countLikesByuser) {
					if (c.getPost().getId().equals(p.getId())) {
						vo.likeCount = c.getCount();
						break;
					}
				}
				
				for (PostUsuarios pu : postUsuarios) {
					if (pu.getPost().getId().equals(p.getId())) {
						vo.sendPush = pu.isPush();
						vo.sendNotification = pu.isNotification();
						break;
					}
				}
				
				vo.arquivos = Utils.s3ToEndpoint(vo.arquivos);
				vo.urlFotoUsuario = Utils.s3ToEndpoint(vo.urlFotoUsuario);
				vo.urlFotoUsuarioThumb = Utils.s3ToEndpoint(vo.urlFotoUsuarioThumb);
				vo.urlImagem = Utils.s3ToEndpoint(vo.urlImagem);
				vo.urlVideo = Utils.s3ToCloudfront(vo.urlVideo);
				list.add(vo);
			}
		}

		return list;
	}

	@Override
	public PostVO toVO(Usuario user, Post p) {
		return toVO(user, p, user.getIdioma());
	}
	
	@Override
	public PostVO toVO(Usuario user, Post p, Idioma i) {
		boolean isBjj = "bjj".equals(ParametrosMap.getInstance(user.getEmpresa().getId()).getBuildType());

		PostVO vo = setPost(p, user, i);
		
		PostTask postTask = postTaskService.findByUserPost(user, p);
		if(postTask != null) {
			vo.postTask = new PostTaskVO(postTask);
		}

		vo.setDestaque(p.getPostDestaque());

		if (user != null) {
			Favorito f = favoritoService.findByUserAndPost(user, p);
			vo.favorito = (f != null && f.isFavorito()) ? "1" : "0";

			Likes l = likeService.findByUserAndPost(user, p);
			vo.like = (l != null && l.isFavorito()) ? "1" : "0";

			if (isBjj) {
				Rate r = rateService.findByUserPost(user, p);
				vo.rate = r != null ? String.valueOf(r.getValue()) : "";

				vo.rateMedia = String.valueOf(rateService.findMedia(p));
			}
		}

		Long count = likeService.countByPost(p); // p.getLikeCount();//
		vo.likeCount = count;

		if (isBjj) {
			count = rateService.countByPost(p); // p.getLikeCount();//
			vo.rateCount = count;
		}

		if (p != null) {
			count = comentarioRep.countByPost(p);
			vo.commentCount = count;
		}

		if (isBjj) {
			Playlist playlist = playlistService.findByUserPost(user, p);
			vo.playlist = playlist != null ? 1 : 0;
		}
		
		PostUsuarios postUsuarios = this.getPostUsuarios(user, p);
		if(postUsuarios != null) {
			vo.sendPush = postUsuarios.isPush();
			vo.sendNotification = postUsuarios.isNotification();
		}

		return vo;
	}
	
	@Override
	public PostVO setPost(Post post, Usuario u) {
		return setPost(post, u, u.getIdioma());
	}

	@Override
	public PostVO setPost(Post post, Usuario u, Idioma idioma) {
		PostVO vo = new PostVO();

		if (post != null) {
			vo.setPost(post, u, idioma);

			List<Grupo> gruposet = grupoService.findGruposByPost(post);
			vo.grupos = new ArrayList<GrupoVO>();
			if (gruposet != null && !gruposet.isEmpty()) {
				for (Grupo grupo : gruposet) {
					GrupoVO g = grupoService.setGrupo(grupo);
					vo.grupos.add(g);
				}
			}
			
			Set<Arquivo> list = getArquivos(post);
			if (list != null && list.size() > 0) {
				LinkedList<FileVO> linkedList = new LinkedList<FileVO>();
				for (Arquivo a : list) {
					FileVO f = new FileVO();
					f.setArquivo(a, true);
					f.post = null;
					f.usuario = null;
					f.dimensao = a.getDimensao();
					if(a.isDestaque()) {
						linkedList.addFirst(f);
					} else {
						linkedList.add(f);
					}
				}
				vo.arquivos = linkedList;
			}

			vo.setDestaque(post.getPostDestaque());
		}

		return vo;
	}
	
	@Override
	public Set<Arquivo> getArquivos(Post post) {
		return rep.getArquivos(post); 
	}

	@Override
	public List<Post> findAllPostsByUserAndTitle(BuscaPost b, int page, int maxRows) {
		List<Post> posts = rep.findAllPosts(b, page, maxRows);
		return posts;
	}

	@Override
	public List<Post> findAllByTituloLike(String titulo) {
		return rep.findAllByTituloLike(titulo);
	}

	@Override
	public List<Post> findAllByTituloLikeWithMax(String titulo, int max, Empresa empresa) {
		return rep.findAllByTituloLikeWithMax(titulo, max, empresa);
	}

	@Override
	public long getCountUsuariosByPost(Post p) {
		return rep.getCountUsuariosByPost(p);
	}

	@Override
	public List<Post> findAllRascunhosByUser(Usuario user, int page, int maxSize) {
		// BuscaPost b = new BuscaPost();
		// b.userPost = user;
		// b.rascunhos = true;
		// return findAllPostsByUserAndTitle(b, page, maxRows);

		return rep.findAllRascunhosByUser(user, page, maxSize);
	}

	@Override
	public List<Post> findByCategorias(List<CategoriaPost> categorias) {
		return rep.findByCategorias(categorias);
	}

	@Override
	public List<Long> findAllOwnerByUser(Usuario u) {
		return rep.findAllOwnerByUser(u);
	}

	@Override
	public List<Long> findIdsCriadosByUser(Usuario u) {
		return rep.findAllCriadosByUser(u);
	}

	@Override
	public List<Long> findIdsByCategoria(CategoriaPost c) {
		return rep.findidsByCategoria(c);
	}

	@Override
	public List<Post> findPostsByTag(Tag tag) {
		return rep.findPostsByTag(tag);
	}

	@Override
	public List<Post> findPostsByGrupo(Grupo g, boolean unicosDesteGrupo) {
		return rep.findPostsByGrupo(g, unicosDesteGrupo);
	}

	@Override
	public int publicar() {
		int count = rep.publicar();
		if(count > 0) {
			log.debug("PostService.publicar count: " + count);
		}
		return count;
	}
	
	@Override
	public int expirar() {
		int count = rep.expirar();
		if(count > 0) {
			log.debug("PostService.expirar count: " + count);
		}
		return count;
	}
	
	@Override
	public boolean isVisivel(Usuario u, Post p) {
		if(p == null || u == null) {
			return false;
		}
		
		Long userId = u.getId();
		Long autorId = p.getUsuario().getId();
		Amizade amizade = usuarioService.getAmizade(u, p.getUsuario());
		
		
		if(userId.equals(autorId)) {
			return true;
		}
		
		if(p.getVisibilidade().equals(Visibilidade.SOMENTE_EU)) {
			return userId.equals(autorId);
		}
		
		if(p.getVisibilidade().equals(Visibilidade.MEUS_AMIGOS)) {
			if(amizade != null && amizade.getStatusAmizade().equals(StatusAmizade.APROVADA)) {
				return true;
			}
			return false;
		}
		
		return false;
		
	}

	@Override
	public List<PostUsuarios> findPostUsuarios(Usuario user, List<Post> posts) {
		return rep.findPostUsuarios(user, posts);
	}

	@Override
	public PostUsuarios getPostUsuarios(Usuario user, Post p) {
		return rep.getPostUsuarios(user, p);
	}

	@Override
	public PostUsuarios getPostUsuarios(Long user, Long p) {
		return rep.getPostUsuarios(user, p);
	}

	@Override
	public void saveOrUpdate(PostUsuarios postUsuarios) throws DomainException {
		rep.saveOrUpdate(postUsuarios);
	}

}
