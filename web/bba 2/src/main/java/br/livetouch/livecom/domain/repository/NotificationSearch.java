package br.livetouch.livecom.domain.repository;

import java.util.Date;

import br.livetouch.livecom.domain.Usuario;

public class NotificationSearch {

	public int page = 0;
	public int maxRows = 10;
	public String tipo;
	
	public Usuario user;
	public boolean naoLidas;
	public boolean lidas;
	public Date dataInicial;
	public Date dataFinal;

	public Long novasNotificacoes;
	public String texto;
}
