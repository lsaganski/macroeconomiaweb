package br.livetouch.livecom.domain.vo;

import br.livetouch.livecom.domain.enums.StatusUsuario;

public class StatusAtivacaoVO {

	private String status;
	private Long countByStatus;
	
	public StatusAtivacaoVO(StatusUsuario status, Long countByStatus) {
		super();
		this.status = status != null ? status.getDesc2() : "";
		this.countByStatus = countByStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getCountByStatus() {
		return countByStatus;
	}

	public void setCountByStatus(Long countByStatus) {
		this.countByStatus = countByStatus;
	}

}