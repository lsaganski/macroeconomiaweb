package br.livetouch.livecom.chatAkka;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

	public static final String[] MESES_ABREVIADOS = new String[] { "Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez" };

	public static final String[] MESES_COMPLETOS = new String[] { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };

	public static final int[] ULTIMA_DIA_MES = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	public static final String DATE = "dd/MM/yyyy";

	// nao tem diferen�a entre dia e noite
	public static final String DATE_TIME_AM_PM = "dd/MM/yyyy hh:mm:ss";

	// formato at� 25h
	public static final String DATE_TIME_24h = "dd/MM/yyyy HH:mm:ss";

	public static String getDate() {
		return toString(new Date());
	}

	public static String getDate(String pattern) {
		return getDate(new Date(), pattern);
	}

	public static String getDate(Date date, String pattern) {
		return toString(date, pattern);
	}

	public static String toString(Date date, String pattern) {
		return toString(date, pattern, null);
	}

	public static String toString(Date date, String pattern, Locale locale) {
		if (date == null) {
			return "";
		}

		if (pattern == null) {
			throw new IllegalArgumentException("Null pattern parameter");
		}

		SimpleDateFormat format = locale != null ? new SimpleDateFormat(pattern, locale) : new SimpleDateFormat(pattern);

		return format.format(date);
	}

	public static String toString(Date date) {
		return toString(date, DATE, null);
	}

	public static String toString(Date date, Locale locale) {
		return toString(date, DATE, locale);
	}

	public static Date toDate(String date, String pattern, Locale locale) {
		if (date == null) {
			return null;
		}

		if (pattern == null) {
			throw new IllegalArgumentException("Null pattern parameter");
		}

		SimpleDateFormat format = locale != null ? new SimpleDateFormat(pattern, locale) : new SimpleDateFormat(pattern);

		try {
			return format.parse(date);
		} catch (ParseException e) {
			throw new RuntimeException("Error to parse the Date: " + date + " using the pattern: " + pattern, e);
		}
	}

	public static Date toDate(String date, Locale locale) {
		return toDate(date, DATE, locale);
	}

	public static Date toDate(String date) {
		return toDate(date, DATE, null);
	}

	public static Date toDateTime(String date) {
		return toDate(date, DATE_TIME_24h, null);
	}

	public static Date toDateTime(String date, Locale locale) {
		return toDate(date, DATE_TIME_24h, locale);
	}

	public static int compareTo(Date dateA, Date dateB) {
		return compareTo(dateA, dateB, "yyyyMMdd");
	}

	/**
	 * Compara duas datas
	 *
	 * // - : menor (this < other) // 0 : igual (this = other) // + : maior
	 * (this > other)
	 *
	 * @return
	 */
	public static int compareTo(Date dateA, Date dateB, String pattern) {
		String sA = toString(dateA, pattern);
		String sB = toString(dateB, pattern);

		// - : menor (this < other)
		// 0 : igual (this = other)
		// + : maior (this > other)

		return sA.compareTo(sB);
	}

	public static boolean equals(Date dateA, Date dateB, String pattern) {
		return compareTo(dateA, dateB, pattern) == 0;
	}

	public static boolean equals(Date dateA, Date dateB) {
		return compareTo(dateA, dateB) == 0;
	}

	/**
	 * Retorna o valor do hor�rio minimo para a data de referencia passada. <BR>
	 * <BR>
	 * Por exemplo se a data for "30/01/2009 as 17h:33m:12s e 299ms" a data
	 * retornada por este metodo ser� "30/01/2009 as 00h:00m:00s e 000ms".
	 * 
	 * @param date
	 *            de referencia.
	 * @return {@link java.util.Date} que representa o hor�rio minimo para dia
	 *         informado.
	 */
	public static Date lowDateTime(Date date) {
		Calendar aux = Calendar.getInstance();
		aux.setTime(date);
		toOnlyDate(aux); // zera os parametros de hour,min,sec,milisec
		return aux.getTime();
	}

	/**
	 * Retorna o valor do hor�rio maximo para a data de referencia passada. <BR>
	 * <BR>
	 * Por exemplo se a data for "30/01/2009 as 17h:33m:12s e 299ms" a data
	 * retornada por este metodo ser� "30/01/2009 as 23h:59m:59s e 999ms".
	 * 
	 * @param date
	 *            de referencia.
	 * @return {@link java.util.Date} que representa o hor�rio maximo para dia
	 *         informado.
	 */
	public static Date highDateTime(Date date) {
		Calendar aux = Calendar.getInstance();
		aux.setTime(date);
		toOnlyDate(aux); // zera os parametros de hour,min,sec,milisec
		aux.add(Calendar.DATE, 1); // vai para o dia seguinte
		aux.add(Calendar.MILLISECOND, -1); // reduz 1 milisegundo
		return aux.getTime();
	}

	/**
	 * Zera todas as referencias de hora, minuto, segundo e milesegundo do
	 * {@link java.util.Calendar}.
	 * 
	 * @param date
	 *            a ser modificado.
	 */
	public static void toOnlyDate(Calendar date) {
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
	}

	public static Date setTimeInicioDia(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);

		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		Date d = c.getTime();
		return d;
	}

	public static Date setTimeFimDia(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);

		c.set(Calendar.HOUR, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 0);

		Date d = c.getTime();
		return d;
	}

	public static int getDia(Date date) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);

		int dia = c.get(Calendar.DAY_OF_MONTH);
		return dia;
	}

	public static int getMes(Date date) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);

		int dia = c.get(Calendar.MONTH) + 1;
		return dia;
	}

	public static int getAno(Date date) {

		Calendar c = Calendar.getInstance();
		c.setTime(date);

		int dia = c.get(Calendar.YEAR);
		return dia;
	}

	/**
	 * http://www.exampledepot.com/egs/java.util/CompDates.html
	 *
	 * Retorna a quantidade de horas de diferença entre a data informada e a
	 * data atual
	 *
	 * @param dataInicial
	 * @param dataFinal
	 * @return
	 */
	public static long getDiferencaMillis(Date dataInicial, Date dataFinal) {

		Calendar c1 = Calendar.getInstance();
		c1.setTime(dataInicial);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(dataFinal);

		// Determine which is earlier
		// boolean b = c1.after(c2); // false
		// b = c1.before(c2); // true

		// Get difference in milliseconds
		long diffMillis = c2.getTimeInMillis() - c1.getTimeInMillis();

		return diffMillis;
	}

	/**
	 * http://www.exampledepot.com/egs/java.util/CompDates.html
	 *
	 * Retorna a quantidade de horas de diferença entre a data informada e a
	 * data atual
	 *
	 * @param dataInicial
	 * @param dataFinal
	 * @return
	 */
	public static long getDiferencaSegundos(Date dataInicial, Date dataFinal) {
		long diffMillis = getDiferencaMillis(dataInicial, dataFinal);
		long diffSecs = diffMillis / (1000);
		return diffSecs;
	}

	public static long getDiferencaMinutos(Date dataInicial, Date dataFinal) {
		long diffMillis = getDiferencaMillis(dataInicial, dataFinal);
		long diffMins = diffMillis / (60 * 1000);
		return diffMins;
	}

	/**
	 * http://www.exampledepot.com/egs/java.util/CompDates.html
	 *
	 * Retorna a quantidade de horas de diferença entre a data informada e a
	 * data atual
	 *
	 * @return
	 */
	public static long getDiferencaHoras(Date dataInicial, Date dataFinal) {
		long diffMillis = getDiferencaMillis(dataInicial, dataFinal);
		// Get difference in hours
		long diffHours = diffMillis / (60 * 60 * 1000);
		return diffHours;
	}

	/**
	 * http://www.exampledepot.com/egs/java.util/CompDates.html
	 *
	 * Retorna a quantidade de horas de diferença entre a data informada e a
	 * data atual
	 *
	 * @return
	 */
	public static long getDiferencaDias(Date dataInicial, Date dataFinal) {
		long diffMillis = getDiferencaMillis(dataInicial, dataFinal);

		// Get difference in days
		long diffDays = diffMillis / (24 * 60 * 60 * 1000);

		return diffDays;
	}

	/**
	 * Faz a soma de 1 dia na data especificada, corrigindo o problema com
	 * horário de verão.<br>
	 *
	 * Se por acaso o calendar do Java (ao somar 1 dia), continuar no mesmo dia
	 * se for horário de verão (ex: horario de verão perde 1 hora, e volta para
	 * as 23h) O algoritmo força a troca de dia
	 *
	 * @author Ricardo R. Lecheta
	 * @param date
	 * @return
	 * @since v2.0, 14/1/2009
	 */
	public static Date addDia(Date date, int dias) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);

		int dia1 = c.get(Calendar.DAY_OF_MONTH);
		c.add(Calendar.DATE, dias);
		int dia2 = c.get(Calendar.DAY_OF_MONTH);
		if (dia1 == dia2) {
			// Adiciona novamente o dia porque o Horário de Verão não troca de
			// dia
			// Ex: 10/10/2009, fica para 10/10/2009 - 23:00:00
			c.add(Calendar.DATE, dias);
		}

		// zera as horas
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.HOUR_OF_DAY, 0);

		Date data = c.getTime();
		return data;
	}

	public static String getMesDesc(int mes) {
		return MESES_COMPLETOS[mes - 1];
	}

	public static String getMesDescAbrev(int mes) {
		return MESES_ABREVIADOS[mes - 1];
	}

	public static boolean isMaior(Date dateA, Date dateB, String pattern) {
		return compareTo(dateA, dateB, pattern) > 0;
	}

	public static boolean isIgual(Date dateA, Date dateB, String pattern) {
		return compareTo(dateA, dateB, pattern) == 0;
	}

	public static boolean isMenor(Date dateA, Date dateB, String pattern) {
		return compareTo(dateA, dateB, pattern) < 0;
	}

	public static boolean isMaior(Date dateA, Date dateB) {
		return compareTo(dateA, dateB) > 0;
	}

	public static boolean isIgual(Date dateA, Date dateB) {
		return compareTo(dateA, dateB) == 0;
	}

	public static boolean isMenor(Date dateA, Date dateB) {
		return compareTo(dateA, dateB) < 0;
	}

	/**
	 * Retorna o numero de dias uteis no mes que ocorreram antes desta Data
	 *
	 * @return
	 */
	public static int getDiasUteisAteDia(Date dateFim) {
		int diaFim = getDia(dateFim);
		int mes = getMes(dateFim);

		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, mes - 1);
		int dias = 0;
		for (int i = 1; i < diaFim; i++) {
			c.set(Calendar.DAY_OF_MONTH, i);
			if (isDiaSemana(c.getTime())) {
				dias++;
			}

		}
		return dias;
	}

	/**
	 * Retorna o numero de dias uteis no mes que ainda tem depois desta Data
	 *
	 * @param dataInicio
	 * @return
	 */
	public static int getDiasUteisDepoisDia(Date dataInicio) {
		int diaInicio = getDia(dataInicio);
		int mes = getMes(dataInicio);

		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, mes - 1);
		int dias = 0;
		int diasMes = ULTIMA_DIA_MES[mes - 1];
		for (int i = diaInicio + 1; i <= diasMes; i++) {
			c.set(Calendar.DAY_OF_MONTH, i);
			if (isDiaSemana(c.getTime())) {
				dias++;
			}

		}
		return dias;
	}

	/**
	 * Retorna o numero de dias uteis no mes
	 *
	 * @param mes
	 *            Jan = 1
	 * @return
	 */
	public static int getDiasUteis(int mes) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, mes - 1);
		int dias = 0;
		int diasMes = ULTIMA_DIA_MES[mes - 1];
		for (int i = 1; i <= diasMes; i++) {
			c.set(Calendar.DAY_OF_MONTH, i);
			if (isDiaSemana(c.getTime())) {
				dias++;
			}

		}
		return dias;
	}

	/**
	 * É domingo?
	 *
	 * @param date
	 * @return
	 */
	public static boolean isDomingo(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dia = c.get(Calendar.DAY_OF_WEEK);

		boolean b = dia == Calendar.SUNDAY;

		return b;
	}

	/**
	 * É sábado?
	 *
	 * @param date
	 * @return
	 */
	public static boolean isSabado(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dia = c.get(Calendar.DAY_OF_WEEK);

		boolean b = dia == Calendar.SATURDAY;

		return b;
	}

	/**
	 * Verifica se a data é de 2ª-feira a 6ª-feira
	 *
	 * @param date
	 * @return
	 */
	public static boolean isDiaSemana(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dia = c.get(Calendar.DAY_OF_WEEK);

		switch (dia) {
		case Calendar.MONDAY:
		case Calendar.TUESDAY:
		case Calendar.WEDNESDAY:
		case Calendar.THURSDAY:
		case Calendar.FRIDAY:
			return true;
		}

		return false;
	}

	public static boolean isToday(long time) {
		Calendar c1 = Calendar.getInstance();
		c1.setTime(new Date());

		Calendar c2 = Calendar.getInstance();
		c2.setTime(new Date(time));

		boolean day = c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR);
		boolean month = c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH);
		boolean year = c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR);
		if (day && month && year) {
			return true;
		}

		return false;
	}

	public static boolean isYesterday(long time) {
		// Yesterday
		Calendar c1 = Calendar.getInstance();
		c1.setTime(new Date());
		c1.add(Calendar.DAY_OF_YEAR, -1);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(new Date(time));

		boolean day = c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR);
		boolean month = c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH);
		boolean year = c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR);
		if (day && month && year) {
			return true;
		}

		return false;
	}

	public static String toDateStringHojeOntem(Date dt) {
		String dataStr = "Hoje";
		if (dt != null) {
			long timestamp = dt.getTime();

			if (isToday(timestamp)) {
				dataStr = "Hoje";
			} else if (isYesterday(timestamp)) {
				dataStr = "Ontem";
			} else {
				dataStr = DateUtils.toString(dt, "dd/MMM");
			}
		}
		return dataStr;
	}

	public static String toDateStringHojeOntemHora(Date dt) {
		String dataStr = "Hoje";
		if (dt != null) {
			long timestamp = dt.getTime();

			if (isToday(timestamp)) {
				dataStr = "Hoje";
			} else if (isYesterday(timestamp)) {
				dataStr = "Ontem";
			} else {
				dataStr = DateUtils.toString(dt, "dd MMM 'às' HH'h'mm");
			}
		}
		return dataStr;
	}

	public static int getCurrentYear() {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		return c.get(Calendar.YEAR);
	}

}