package br.livetouch.livecom.web.pages.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;

/**
 * 
 */
@Controller
@Scope("prototype")
public class MensagensPage extends LivecomLogadoPage {
	public MensagemConversa conversa;
	public Long id;
	public Long userTo;
	
	public boolean escondeChat = true;
	
	
	@Override
	public void onInit() {
		super.onInit();
		setRedirect(ChatPage.class);
	}
	
	
	@Override
	public void onRender() {
		
		super.onRender();
		
	}
}
