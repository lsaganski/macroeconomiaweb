package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Agenda;
import br.livetouch.livecom.domain.Evento;
import br.livetouch.livecom.domain.Grupo;
import net.livetouch.extras.util.DateUtils;

public class EventoVO {
	public Long id;
	public String data;
	public String descricao;
	public String nome;
	public String fotos3;
	public List<AgendaVO> agendas;
	public List<GrupoVO> grupos;

	public EventoVO(Evento evento) {
		this.setId(evento.getId());
		this.setData(DateUtils.getDate(evento.getData(), "dd/MM/yyyy"));
		this.setDescricao(evento.getDescricao());
		this.setFotos3(evento.getFotoS3());
		this.setNome(evento.getNome());
		this.setAgendas(setAgendasVo(evento.getAgendas()));
		this.setGrupos(setGruposVo(evento.getGrupos()));

	}

	private List<AgendaVO> setAgendasVo(List<Agenda> agendas) {
		List<AgendaVO> agendaList = new ArrayList<AgendaVO>();
		for (Agenda agenda : agendas) {
			AgendaVO agendaVo = new AgendaVO();
			agendaVo.setAgenda(agenda);
			agendaList.add(agendaVo);
		}

		return agendaList;
	}
	
	private List<GrupoVO> setGruposVo(List<Grupo> grupos) {
		return new ArrayList<GrupoVO>();
//		List<GrupoVO> grupoList = new ArrayList<GrupoVO>();
//		for (Grupo grupo : grupos) {
//			GrupoVO grupoVo = new GrupoVO();
//			grupoVo.setGrupo(grupo);
//			grupoList.add(grupoVo);
//		}

//		return grupoList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getFotos3() {
		return fotos3;
	}

	public void setFotos3(String fotos3) {
		this.fotos3 = fotos3;
	}

	public List<AgendaVO> getAgendas() {
		return agendas;
	}

	public void setAgendas(List<AgendaVO> agendas) {
		this.agendas = agendas;
	}

	public List<GrupoVO> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<GrupoVO> grupos) {
		this.grupos = grupos;
	}
}