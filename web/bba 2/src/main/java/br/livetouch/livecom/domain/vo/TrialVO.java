package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Usuario;

public class TrialVO implements Serializable {

private static final long serialVersionUID = 1524516348542576158L;
	
	private String empresa;
	private String url;
	private String codigo;
	private String usuario;
	private String senha;
	private String status;
	
	public TrialVO() {
	}
	
	public TrialVO(Empresa empresa, Usuario usuario) {
		this.status = "OK";
		this.empresa = empresa.getNome();
		this.url = empresa.getDominio();
		this.codigo = empresa.getTrialCode();
		this.usuario = usuario.getNome();
		this.senha = usuario.getSenha();
	}
	
	public TrialVO(Empresa empresa) {
		this.status = "OK";
		this.empresa = empresa.getNome();
		this.url = empresa.getDominio();
		this.codigo = empresa.getTrialCode();
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
