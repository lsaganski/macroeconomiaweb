package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.PerfilPermissao;
import br.livetouch.livecom.domain.Permissao;

public class PermissaoVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String nome;
	private String codigo;
	private String descricao;
	private boolean isDefault;
	
	private PermissaoCategoriaVO categoria;
	
	public PermissaoVO() {}

	public PermissaoVO(Permissao p) {
		setId(p.getId());
		setNome(p.getNome());
		setCodigo(p.getCodigo());
		setDescricao(p.getDescricao());
		if(p.getCategoria() != null) {
			setCategoria(new PermissaoCategoriaVO(p.getCategoria()));
		}
		isDefault = p.getEmpresa() != null ? false : true;
	}

	public void setPermissao(Permissao p) {
		setId(p.getId());
		setNome(p.getNome());
		setDescricao(p.getDescricao());
		setCodigo(p.getCodigo());
		if(p.getCategoria() != null) {
			setCategoria(new PermissaoCategoriaVO(p.getCategoria()));
		}
        isDefault = p.getEmpresa() != null ? false : true;
	}
	
	public static List<PermissaoVO> fromList(List<Permissao> permissoes) {
		List<PermissaoVO> vos = new ArrayList<>();
		if(permissoes == null) {
			return vos;
		}
		for (Permissao p : permissoes) {
			PermissaoVO permissaoVO = new PermissaoVO();
			permissaoVO.setPermissao(p);
			vos.add(permissaoVO);
		}
		return vos;
	}
	
	public static List<PermissaoVO> fromList(Set<PerfilPermissao> permissoes) {
		List<PermissaoVO> vos = new ArrayList<>();
		if(permissoes == null) {
			return vos;
		}
		for (PerfilPermissao p : permissoes) {
			PermissaoVO permissaoVO = new PermissaoVO();
			permissaoVO.setPermissao(p.getPermissao());
			vos.add(permissaoVO);
		}
		return vos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao != null ? descricao : "";
	}

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean isdefault) {
        this.isDefault = isdefault;
    }

	public PermissaoCategoriaVO getCategoria() {
		return categoria;
	}

	public void setCategoria(PermissaoCategoriaVO categoria) {
		this.categoria = categoria;
	}
}
