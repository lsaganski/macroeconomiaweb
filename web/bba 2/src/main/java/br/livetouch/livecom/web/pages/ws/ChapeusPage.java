package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Chapeu;
import br.livetouch.livecom.domain.vo.ChapeuFilter;
import br.livetouch.livecom.domain.vo.ChapeuVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ChapeusPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private TextField tNome;
	private LongField tCategoriaId;

	@Override
	public void onInit() {
		super.onInit();

		form.add(tNome = new TextField("nome"));
		tNome.setFocus(true);

		form.add(tCategoriaId = new LongField("categoria_id"));

		form.add(new TextField("not_chapeu_ids"));
		form.add(new TextField("page"));
		form.add(new TextField("maxRows"));

		form.add(tMode = new TextField("mode"));

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));

		tMode.setValue("json");

		form.add(new Submit("consultar"));

		// setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {

			String nome = tNome.getValue();
			Long idCategoria = tCategoriaId.getLong();
			CategoriaPost categ = null;
			if (idCategoria != null) {
				categ = categoriaPostService.get(idCategoria);
			}

			String maxRowsParameter = getContext().getRequestParameter("maxRows");
			int maxRows = NumberUtils.toInt(maxRowsParameter, 20);

			ChapeuFilter filter = new ChapeuFilter();
			filter.nome = nome;
			filter.categoria = categ;
			filter.page = page;
			filter.maxRows = maxRows;
			filter.notIds = getListIds("not_chapeu_ids");

			List<Chapeu> list = chapeuService.findAllByFilter(filter);
			List<ChapeuVO> retorno = ChapeuVO.create(list);
			
			if(isWsVersion3()) {
				Response r = Response.ok("OK");
				r.chapeus = retorno;
				return r;
			}

			return retorno;
		}

		return new MensagemResult("NOK", "Erro ao buscar os chapeus.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("chapeu", Chapeu.class);
		super.xstream(x);
	}
}
