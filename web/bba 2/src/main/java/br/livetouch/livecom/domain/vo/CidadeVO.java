package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Cidade;

public class CidadeVO {

	private Long id;
	private String nome;
	private String estado;

	public CidadeVO() {

	}

	public CidadeVO(Cidade cidade) {
		this.id = cidade.getId();
		this.nome = cidade.getNome();
		this.setEstado(cidade.getEstado().getSigla());
	}

	public void setCidade(Cidade cidade) {
		this.id = cidade.getId();
		this.nome = cidade.getNome();
		this.setEstado(cidade.getEstado().getSigla());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public static CidadeVO from(Cidade cidade) {
		return new CidadeVO(cidade);
	}
	
	public static List<CidadeVO> fromList(List<Cidade> cidades) {
		List<CidadeVO> vos = new ArrayList<>();
		if(cidades == null) {
			return vos;
		}
		for (Cidade a : cidades) {
			vos.add(new CidadeVO(a));
		}
		return vos;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
}