package br.livetouch.livecom.web.pages.root;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.vo.EmpresaVO;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.web.pages.LivecomRootPage;
import br.livetouch.livecom.web.pages.cadastro.EmpresaPage;
import net.sf.click.control.Form;
import net.sf.click.control.Option;
import net.sf.click.control.Select;
import net.sf.click.control.Submit;

@Controller
@Scope("prototype")
public class ProjetoPage extends LivecomRootPage {
	public String msg;
	public Form form = new Form();
	public String from;
	private Select projeto;

	@Override
	public void onInit() {
		super.onInit();
		
		form.setMethod("post");
		form.setAttribute("class","selecionaProjeto");
		List<Empresa> empresas = empresaService.findAll();
		comboEmpresa(empresas);
		
		Submit bot = new Submit("concluir");
		bot.setAttribute("class", "botao botao-lg info");
		form.add(bot);
	}

	private void comboEmpresa(List<Empresa> empresas) {
		projeto = new Select("projeto", "Projeto", true);
		for (Empresa empresa : empresas) {
			projeto.add(new Option(empresa.getId(), empresa.getNome()));
		}
		projeto.setAttribute("class", "hasCustomSelect");
		form.add(projeto);
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onPost() {
		String idEmpresa = projeto.getValue();
		if(idEmpresa != null && StringUtils.isNumeric(idEmpresa)) {
			Empresa empresa = empresaService.get(Long.parseLong(idEmpresa));
			if(empresa == null) {
				return;
			}
			UserInfoVO userInfoVO = getUserInfoVO();
			if(userInfoVO != null) {
                Empresa empresaROOT = empresaService.get(empresa.getId());
                EmpresaVO empresaVO = new EmpresaVO(empresaROOT);
                userInfoVO.setEmpresaRoot(empresaVO);
				userInfoVO.setEmpresaId(empresa.getId());
			}
			setRedirect(EmpresaPage.class,"id",idEmpresa);
		}
	}

	@Override
	public String getTemplate() {
		return getPath();
	}
	
}

