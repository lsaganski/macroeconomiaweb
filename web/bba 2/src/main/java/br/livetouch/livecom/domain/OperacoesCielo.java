package br.livetouch.livecom.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.utils.DateUtils;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OperacoesCielo extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = -5621805878507014639L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "OPERACOES_CIELO_SEQ")
	private Long id;
	
	private Date data;
	private Double volumetria;
	private Double picoTps;
	private Double media;
	private Double momento;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getDataString() {
		return DateUtils.toString(this.data ,"dd/MM/yyyy");
	}

	public Double getVolumetria() {
		return volumetria;
	}

	public void setVolumetria(Double volumetria) {
		this.volumetria = volumetria;
	}

	public Double getPicoTps() {
		return picoTps;
	}

	public void setPicoTps(Double picoTps) {
		this.picoTps = picoTps;
	}

	public Double getMedia() {
		return media;
	}

	public void setMedia(Double media) {
		this.media = media;
	}

	public Double getMomento() {
		return momento;
	}

	public void setMomento(Double momento) {
		this.momento = momento;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

}
