package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Faq;
import br.livetouch.livecom.domain.repository.FaqRepository;
import br.livetouch.livecom.domain.service.FaqService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class FaqServiceImpl implements FaqService {
	@Autowired
	private FaqRepository rep;

	@Override
	public Faq get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Faq c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Faq> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void delete(Faq c) throws DomainException {
		rep.delete(c);
	}
}
