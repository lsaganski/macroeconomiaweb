package br.livetouch.livecom.domain.repository.impl;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.infra.util.SQLUtils;
import br.infra.util.Utils;
import br.livetouch.livecom.domain.Amizade;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.UsuarioAcessoMural;
import br.livetouch.livecom.domain.UsuarioAuditoria;
import br.livetouch.livecom.domain.enums.OrigemCadastro;
import br.livetouch.livecom.domain.enums.Semana;
import br.livetouch.livecom.domain.enums.StatusAmizade;
import br.livetouch.livecom.domain.enums.StatusParticipacao;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.enums.TipoSistema;
import br.livetouch.livecom.domain.repository.UsuarioRepository;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVO;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioFuncionaisVO;
import br.livetouch.livecom.domain.vo.StatusAtivacaoVO;
import br.livetouch.livecom.domain.vo.UsuarioAutocompleteFiltro;
import br.livetouch.livecom.domain.vo.UsuarioStatusCountVO;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;

@Repository
@SuppressWarnings("unchecked")
public class UsuarioRepositoryImpl extends LivecomRepository<Usuario> implements UsuarioRepository {

	public UsuarioRepositoryImpl() {
		super(Usuario.class);
	}

	@Override
	public Usuario findByLoginFetch(String login, Empresa empresa, boolean isMobile) {
		StringBuffer sql = new StringBuffer("from Usuario u ");

		sql.append(" inner join fetch u.permissao permissao where 1=1 ");

		sql.append(" and u.login=:login ");
		
		if(ParametrosMap.getInstance(empresa).getBoolean(Params.ITAU_LOGIN_HASHMD5, false) && isMobile) {
			sql.append(" or md5(u.login)=:login ");
		} 
		
		if(empresa != null) {
			sql.append(" and u.empresa.id=:empresa");
		}

		Query q = createQuery(sql.toString());

		q.setString("login", login);
		if (empresa != null) {
			q.setLong("empresa", empresa.getId());
		}

		q.setCacheable(true);
		List<Usuario> list = q.list();
		Usuario usuario = list.isEmpty() ? null : list.get(0);
		return usuario;
	}

	@Override
	public Usuario findByLogin(String login, Empresa empresa) {
		StringBuffer sql = new StringBuffer("from Usuario u where u.login=?");
		if (empresa != null) {
			sql.append(" and (u.empresa.id=? or u.empresa is null)");
		}
		Query q = createQuery(sql.toString());
		q.setString(0, login);
		if (empresa != null) {
			q.setLong(1, empresa.getId());
		}

		q.setCacheable(true);
		List<Usuario> list = q.list();
		Usuario usuario = list.isEmpty() ? null : list.get(0);
		return usuario;
	}

	@Override
	public Usuario findByEmail(String login) {
		Query q = createQuery("from Usuario where email=?").setString(0, login);
		q.setCacheable(true);
		List<Usuario> list = q.list();
		Usuario usuario = list.isEmpty() ? null : list.get(0);
		return usuario;
	}

	@Override
	public List<Usuario> findByFilter(Usuario userInfo, Usuario filtro, int page, int pageSize, Empresa empresa) {
		Query query = getQueryFindByFilter(userInfo, filtro, false, empresa);

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * pageSize;
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(pageSize);
		query.setCacheable(true);

		List<Usuario> list = query.list();

		return list;
	}

	private Query getQueryFindByFilter(Usuario userInfo, Usuario filtro, boolean count, Empresa empresa) {
		StringBuffer sb = new StringBuffer();

		if (count) {
			sb.append("select count(id) from Usuario where 1=1 ");
		} else {
			sb.append("from Usuario u where 1=1 ");
		}

		sb.append(" AND u.empresa.id = :empresaId");
		
		// Nunca exibir o usuário “admin” na lista de usuários
		// sb.append(" and (login is null or login != 'admin') ");

		String nome = null;
		String login = null;
		if (filtro != null) {
			nome = filtro.getNome();
			login = filtro.getLogin();

			if (nome != null) {
				sb.append(" and nome like :nome");
			}
			if (login != null) {
				sb.append(" and (login like :login)");
			}
		}

		if (!count) {
			sb.append(" order by nome ");
		}

		Query query = createQuery(sb.toString());
		query.setLong("empresaId", empresa.getId());
		if (nome != null) {
			query.setParameter("nome", "%" + nome + "%");
		}
		if (login != null) {
			query.setParameter("login", "%" + login + "%");
		}

		query.setCacheable(true);

		return query;
	}

	@Override
	public long getCountByFilter(Usuario usuarioSistema, Usuario filtro) {
		Query query = getQueryFindByFilter(usuarioSistema, filtro, true, usuarioSistema.getEmpresa());

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	@Override
	public void deleteGrupoUsuarios(Usuario u) {
		execute("delete from GrupoUsuarios where usuario.id = ?", u.getId());
	}

	@Override
	public void deleteGrupoUsuarios(Usuario u, List<Grupo> grupos) {
		Long id = u.getId();
		if(grupos.isEmpty()) {
			execute("delete from GrupoUsuarios gu where gu.usuario.id = ?", id);
		} else {
			executeIn("delete from GrupoUsuarios gu where gu.usuario.id = "+id+" and gu.grupo not in (:grupos)", "grupos", grupos);
		}
	}

	@Override
	public void deleteGrupoUsuarios(Grupo g) {
		execute("delete from GrupoUsuarios where grupo.id = ?", g.getId());
	}

	@Override
	public void deleteGrupoUsuarios(Grupo g, Usuario u) {
		execute("delete from GrupoUsuarios where grupo.id = ? and usuario.id = ?", g.getId(),u.getId() );
	}
	
	@Override
	public void deleteGrupoUsuarios(GrupoUsuarios gu) {
		execute("delete from GrupoUsuarios where id = ?", gu.getId() );
	}
	
	@Override
	public void deleteAmizade(Usuario u, Usuario amigo) {
		execute("delete from Amizade where ((usuario.id = ? and amigo.id = ?) or (usuario.id = ? and amigo.id = ?))", u.getId(),amigo.getId(),amigo.getId(),u.getId() );
	}


	/**
	 * Deleta o usuario e seus relacionamentos
	 * 
	 * @see net.livetouch.tiger.ddd.repository.HibernateRepository#delete(java.lang.Object)
	 */
	@Override
	public void delete(Usuario u) {
		// Cascades
		deleteGrupoUsuarios(u);
		super.delete(u);
	}

	@Override
	public List<Usuario> findByFiltro(UsuarioAutocompleteFiltro filtro, Empresa empresa) {
		Query query = getQueryFindByFiltro(filtro, empresa, false);

		int firstResult = 0;
		int page = filtro.getPage();
		int max = filtro.getMaxRows();
		
		if (page != 0) {
			firstResult = page * max;
			
		}
		query.setFirstResult(firstResult);
		
		if(max != 0) {
			query.setMaxResults(max);
		}

		query.setCacheable(true);
		List<Usuario> list = query.list();

		return list;
	}

	@Override
	public long getCountByNomeLike(UsuarioAutocompleteFiltro filtro, Empresa empresa) {
		Query query = getQueryFindByFiltro(filtro, empresa, true);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	private Query getQueryFindByFiltro(UsuarioAutocompleteFiltro filtro, Empresa empresa, boolean count) {
		StringBuffer sb = new StringBuffer();

		String nome = filtro.getNome();
		Long cargoId = filtro.getCargoId();
		List<Long> notUsersIds = filtro.getNotUsersIds();
		List<Long> gruposIds = filtro.getGruposIds();
		String coluna = filtro.getColuna();
		String ordenacao = filtro.getOrdenacao();
		String acesso = filtro.getAcesso();

		OrigemCadastro origem = filtro.getOrigemCadastro();
		StatusUsuario statusUsuario = filtro.getStatusUsuario();
		List<Long> usuariosIds = filtro.getUsuariosIds();
		
		if (count) {
			sb.append("select distinct count(u.id) ");
		} else {
			sb.append("select distinct u ");
		}

		if (gruposIds != null && !gruposIds.isEmpty()) {
			sb.append(" from GrupoUsuarios gu inner join gu.usuario u inner join gu.grupo g where 1=1 ");
			sb.append(" and ( ");
			sb.append(" (gu.statusParticipacao = :aprovada or gu.statusParticipacao is null) ");
			if(filtro.isPodePostar()) {
				sb.append(" and gu.postar = 1 ");
			}
			sb.append(" ) ");
		} else {
			sb.append(" from Usuario u where 1=1 ");
		}

		sb.append(" AND u.empresa = :empresa");
		
		// Nunca exibir o usuário “admin” na lista de usuários
		// sb.append(" and (login is null or login != 'admin') ");

		if(usuariosIds != null && usuariosIds.size() > 0) {
			sb.append(" AND u.id in(:usuarios)");
		}
		
		if (StringUtils.isNotBlank(nome)) {
			sb.append(" and (upper(u.nome) like :nome or upper(u.login) like :nome or upper(u.email) like :nome)");
		}

		if(filtro.isAtivo()) {
			sb.append(" and u.ativo = :ativo");
		}

		if (gruposIds != null && !gruposIds.isEmpty()) {
			sb.append(" and g.id in (:grupos)");
		}

		if (cargoId != null) {
			sb.append(" and u.funcao.id = :cargoId");
		}

		if (statusUsuario != null) {
			sb.append(" and u.status = :statusUsuario");
		}

		if (origem != null) {
			sb.append(" and u.origemCadastro = :origem");
		}
		
		if (StringUtils.isNotEmpty(acesso)) {
			if(StringUtils.equals(acesso, "livecom")) {
				sb.append(" and (u.flags = :acesso or u.flags is null) ");
			} else {
				sb.append(" and u.flags = :acesso ");
			}
		}

		if (notUsersIds != null && notUsersIds.size() > 0) {
			sb.append(" and u.id not in (:usersNotIds) ");
		}
		
		if (!count) {
			if(StringUtils.isNotEmpty(coluna) &&  StringUtils.isNotEmpty(ordenacao)) {
				sb.append(" order by u." + coluna +" "+ ordenacao +" ");
			} else {
				sb.append(" order by u.nome ");
			}
		}

		Query query = createQuery(sb.toString());
		
		if(usuariosIds != null && usuariosIds.size() > 0) {
			query.setParameterList("usuarios", usuariosIds);
		}
		
		if(filtro.isAtivo()) {
			query.setBoolean("ativo", filtro.isAtivo());
		}
			
		if (StringUtils.isNotBlank(nome)) {
			query.setParameter("nome", "%" + nome.toUpperCase() + "%");
		}
		
		if (gruposIds != null && !gruposIds.isEmpty()) {
			query.setParameterList("grupos", gruposIds);
			query.setParameter("aprovada", StatusParticipacao.APROVADA);
		}
		
		if (cargoId != null) {
			query.setLong("cargoId", cargoId);
		}

		if (statusUsuario != null) {
			query.setParameter("statusUsuario", statusUsuario);
		}

		if (origem != null) {
			query.setParameter("origem", origem);
		}
		
		if (StringUtils.isNotEmpty(acesso)) {
			query.setParameter("acesso", acesso);
		}
		
		if (notUsersIds != null && notUsersIds.size() > 0) {
			query.setParameterList("usersNotIds", notUsersIds);
		}
		
		query.setParameter("empresa", empresa);

		query.setCacheable(true);

		return query;
	}

	@Deprecated
	@Override
	public List<Usuario> findAllNaoAtivosByGrupo(Grupo g, int page, int maxRows) {
		StringBuffer sb = new StringBuffer("select gu.usuario from GrupoUsuarios gu where gu.grupo.id=? and (u.status = ? or u.status is null) order by u.nome");
		Query q = createQuery(sb.toString());
		q.setLong(0, g.getId());
		q.setParameter(1, StatusUsuario.NOVO);

		setMaxResults(q, page, maxRows);

		q.setCacheable(true);
		List<Usuario> list = q.list();
		return list;
	}

	@Deprecated
	@Override
	public List<Usuario> findAllAtivosByGrupo(Grupo g, int page, int maxRows) {
		StringBuffer sql = new StringBuffer("select gu.usuario from GrupoUsuarios gu where gu.grupo.id=? and (u.status is not null and u.status = ?) order by u.nome");
		Query q = createQuery(sql.toString());
		q.setLong(0, g.getId());
		q.setParameter(1, StatusUsuario.ATIVO);

		setMaxResults(q, page, maxRows);

		q.setCacheable(true);
		List<Usuario> list = q.list();
		return list;
	}

	@Deprecated
	@Override
	public List<Usuario> findAllEnviadoConviteByGrupo(Grupo g, int page, int maxRows) {
		StringBuffer sql = new StringBuffer("select gu.usuario from GrupoUsuarios gu where gu.grupo.id=? and (u.status is not null and (u.status = ? or u.status = ? or u.status = ?)) order by u.nome");
		Query q = createQuery(sql.toString());
		q.setLong(0, g.getId());
		q.setParameter(1, StatusUsuario.ENVIADO_CONVITE);
		q.setParameter(2, StatusUsuario.ENVIADO_CONVITE_JA_LOGOU);
		q.setParameter(3, StatusUsuario.ENVIADO_CONVITE_JA_LOGOU_MOBILE);

		setMaxResults(q, page, maxRows);

		q.setCacheable(true);
		List<Usuario> list = q.list();
		return list;
	}
	
	/**
	 * "1", "Novo - Cadastro recente, ainda não foi enviado e-mail." "2",
	 * "Enviado Convite - Ainda não fizeram login no sistema" "3",
	 * "Ativo - Já fizeram login no sistema"
	 * 
	 * @see br.livetouch.livecom.domain.repository.UsuarioRepository#findAllByStatusAtivacaoByGrupo(br.livetouch.livecom.domain.Grupo,
	 *      int, int, java.lang.String)
	 */
	@Deprecated
	@Override
	public List<Usuario> findAllByStatusAtivacaoByGrupo(Grupo g, int page, int maxRows, String status) {
		if ("2".equals(status)) {
			return findAllEnviadoConviteByGrupo(g, page, maxRows);
		} else if ("3".equals(status)) {
			return findAllAtivosByGrupo(g, page, maxRows);
		}

		return findAllNaoAtivosByGrupo(g, page, maxRows);

	}

	@Override
	public List<StatusAtivacaoVO> findStatusAtivacao(Empresa empresa) {
		StringBuffer sb = new StringBuffer("select new br.livetouch.livecom.domain.vo.StatusAtivacaoVO(u.status,count(u.status)) from Usuario u	");
		sb.append(" WHERE u.empresa.id = :empresaId group by u.status");

		Query q = createQuery(sb.toString());
		q.setParameter("empresaId", empresa.getId());
		List<StatusAtivacaoVO> list = q.list();

		return list;
	}

	public void saveOrUpdateUsuarioAcesso(UsuarioAcessoMural u) {
		getSession().saveOrUpdate(u);
	}

	@Override
	public UsuarioAcessoMural findUsuarioAcessoMural(Usuario u, Post post) {
		StringBuffer sql = new StringBuffer("from UsuarioAcessoMural a where 1=1 ");

		if (u != null) {
			sql.append(" and a.usuario.id=?");
		}

		if (post != null) {
			sql.append(" and a.post.id=?");
		}

		Query q = createQuery(sql.toString());
		if (u != null) {
			q.setLong(0, u.getId());
		}
		if (post != null) {
			q.setLong(1, post.getId());
		}

		q.setCacheable(true);
		List<UsuarioAcessoMural> list = q.list();
		UsuarioAcessoMural a = list.isEmpty() ? new UsuarioAcessoMural() : list.get(0);
		return a;
	}

	@Override
	public List<Usuario> findAllByKeys(List<? extends Serializable> ids) {
		return createCriteria().add(Restrictions.in("id", ids)).setCacheable(true).list();
		// return super.findAllByKeys(ids);
	}

	@Override
	public void saveOrUpdate(GrupoUsuarios gu) {
		getSession().saveOrUpdate(gu);
	}

	@Override
	public void saveOrUpdate(Amizade a) {
		getSession().saveOrUpdate(a);
	}

	@Override
	public List<Grupo> getGrupos(Usuario u) {
		List<Grupo> list = createQuery("select distinct gu.grupo from GrupoUsuarios gu where gu.usuario.id=?").setLong(0, u.getId()).setCacheable(true).list();
		return list;
	}

	@Override
	public List<Grupo> getGrupos(Usuario u, boolean postar) {
		Query q = createQuery("select gu.grupo from GrupoUsuarios gu where gu.usuario.id=? and gu.postar=?");
		q.setLong(0, u.getId());
		q.setBoolean(1, postar);
		q.setCacheable(true);
		List<Grupo> list = q.list();
		return list;
	}

	@Override
	public List<Grupo> getGruposCriadosPorMim(Usuario u) {
		// Grupo.userCreate
		List<Grupo> list = createQuery("from Grupo g where g.userCreate.id=?").setLong(0, u.getId()).setCacheable(true).list();
		return list;
	}

	@Override
	public GrupoUsuarios getGrupoUsuario(Usuario usuario, Grupo grupo) {
		Query q = createQuery("select gu from GrupoUsuarios gu where gu.usuario.id=? and gu.grupo.id=?");
		q.setLong(0, usuario.getId());
		q.setLong(1, grupo.getId());
		q.setCacheable(true);
		List<GrupoUsuarios> list = q.list();
		return list.size() > 0 ? list.get(0) : null;
	}

	@Override
	public Amizade getAmizade(Usuario usuario, Usuario amigo) {
		
		if(usuario == null || amigo == null) {
			return null;
		}
		
		Query q = createQuery("select a from Amizade a where ((a.usuario.id=? and a.amigo.id=?) or (a.usuario.id=? and a.amigo.id=?))");
		q.setLong(0, usuario.getId());
		q.setLong(1, amigo.getId());
		q.setLong(2, amigo.getId());
		q.setLong(3, usuario.getId());
		q.setCacheable(true);
		List<Amizade> list = q.list();
		return list.size() > 0 ? list.get(0) : null;
	}

	@Override
	public List<Usuario> findUsuariosByGrupo(Grupo g, int page, int maxSize) {
		Query q = createQuery("select distinct gu.usuario from GrupoUsuarios gu where gu.grupo.id=? order by gu.usuario.nome");
		q.setLong(0, g.getId());
		q.setCacheable(true);

		if(page > -1) {
			q.setFirstResult(page);
			q.setMaxResults(maxSize);
		}

		List<Usuario> list = q.list();
		return list;
	}

	@Override
	public List<Usuario> findAll(Usuario user, int page, int max) {
		String sql = "from Usuario u ";
		Query q = createQuery(sql);

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * max;
		}
		q.setFirstResult(firstResult);
		q.setMaxResults(max);

		q.setCacheable(true);
		List<Usuario> list = q.list();
		return list;
	}

	@Override
	public void deleteAcessoMural(Usuario u) {

		StringBuffer sql = new StringBuffer("delete from UsuarioAcessoMural a where a.usuario.id=? ");
		Query q = createQuery(sql.toString());
		if (u != null) {
			q.setLong(0, u.getId());
		}

		q.executeUpdate();

	}

	@Override
	public void deleteAuditoria(Usuario u) {

		if (u != null) {
			// Auditoria antiga
			Query q = createQuery("delete from UsuarioAuditoria a where a.user.id=? ");
			q.setLong(0, u.getId());
			q.executeUpdate();
			
			// Auditoria antiga
			q = createQuery("delete from PostAuditoria a where a.user.id=? ");
			q.setLong(0, u.getId());
			q.executeUpdate();
			
			// Auditoria nova
			q = createQuery("delete from LogAuditoria a where a.usuario.id=? ");
			q.setLong(0, u.getId());
			q.executeUpdate();
		}
	}

	@Override
	public void saveOrUpdate(UsuarioAuditoria a) {
		getSession().saveOrUpdate(a);
	}

	@Override
	public void deleteAllUsersByTipoImportacao() {
		StringBuffer sql = new StringBuffer("delete from GrupoUsuarios g where g.usuario.tipoSistema = '" + TipoSistema.IMPORTACAO + "'");
		Query q = createQuery(sql.toString());
		q.executeUpdate();

		sql = new StringBuffer("delete from Usuario u where u.tipoSistema = '" + TipoSistema.IMPORTACAO + "'");
		q = createQuery(sql.toString());
		q.executeUpdate();
	}

	@Override
	public List<String> findLogins() {
		return createQuery("select login from Usuario").list();
	}

	@Override
	public List<Object[]> findLoginsTipoImportacao() {
		return createQuery("select id,login from Usuario where tipoSistema='IMPORTACAO' and (canDelete is null or canDelete=1)").list();
	}

	@Override
	public boolean isUsuarioDentroDoGrupo(Usuario user, Grupo grupo) {
		Query q = createQuery("select count(gu.id) from GrupoUsuarios gu where gu.usuario.id=? and gu.grupo.id=?");
		q.setLong(0, user.getId());
		q.setLong(1, grupo.getId());
		q.setCacheable(true);
		Long count = getCount(q);
		boolean exists = count > 0;
		return exists;
	}

	@Override
	public void insertGrupoUsuario(Long userId, Long grupoId) throws SQLException {
		String sql = "insert into grupo_usuarios (grupo_id,usuario_id,postar) VALUES(?,?,0)";
		SQLUtils.execSql(getSession(), sql, grupoId, userId);
	}

	@Override
	public Long insertUsuarioImportacao(String login, String senha, String dataNasc, Long permissaoId, Long grupoId) throws SQLException {
		String sql = "insert into usuario (login,nome,senha,data_nasc,permissao_id,grupo_id,tipo_sistema) VALUES(?,?,?,str_to_date(?,'%d%m%Y'),?,?,'IMPORTACAO')";
		Long id = SQLUtils.execInsert(getSession(), sql, login, login, senha, dataNasc, permissaoId, grupoId);
		return id;

		// http://stackoverflow.com/questions/1915166/how-to-get-the-insert-id-in-jdbc
	}

	@Override
	public List<RelatorioFuncionaisVO> getFuncionais(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException {
		Date dataInicio = null;
		Date dataFim = null;

		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		if (count) {
			sb.append("select count(distinct login) ");
		} else {
			sb.append("select distinct new br.livetouch.livecom.domain.vo.RelatorioFuncionaisVO (usuario) ");
		}

		sb.append(" from LoginReport l");

		sb.append(" where 1=1 ");

		sb.append(" AND l.usuario.empresa.id = :empresaId");
		
		if (dataInicio != null) {
			sb.append(" and data>= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and data<= :dataFim");
		}

		// sb.append(" and requestPath = '/ws/login.htm'");

		Query q = createQuery(sb.toString());
		
		q.setLong("empresaId", empresa.getId());
		
		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (count) {
			Object obj = q.uniqueResult();
			RelatorioFuncionaisVO vo = new RelatorioFuncionaisVO();
			vo.setCount(new Long(obj.toString()));
			List<RelatorioFuncionaisVO> funcionais = new ArrayList<RelatorioFuncionaisVO>();
			funcionais.add(vo);
			return funcionais;
		}

		q.setCacheable(true);
		List<RelatorioFuncionaisVO> funcionais = q.list();

		return funcionais;
	}

	@Override
	public long getCountByFilter(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException {
		Query query = getQueryFindByFilter(filtro, count, empresa);

		Object obj = query.list().size();

		return new Long(obj.toString());
	}

	private Query getQueryFindByFilter(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;

		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		sb.append("select distinct new br.livetouch.livecom.domain.vo.RelatorioAcessosVO (DATE_FORMAT(data,'%d/%m/%Y'),count(id),count(distinct login)) ");

		sb.append(" from LoginReport l ");

		sb.append(" where 1=1 AND l.empresa.id = :empresaId");

		if (dataInicio != null) {
			sb.append(" and data >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and data <= :dataFim");
		}

		//if (!count && filtro.isExpandir()) { removida pois não funciona o mssql
			sb.append(" group by DATE_FORMAT(data,'%d/%m/%Y')");
//		}

//		if (!count) { removida pois não funciona o mssql
			sb.append(" order by DATE_FORMAT(data,'%d/%m/%Y') desc");
//		}

		Query q = createQuery(sb.toString());
		q.setLong("empresaId", empresa.getId());
		
		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		q.setCacheable(true);
		return q;
	}

	@Override
	public List<RelatorioAcessosVO> findbyFilter(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException {
		Query query = getQueryFindByFilter(filtro, count, empresa);

		int page = filtro.getPage();

		if (!count) {
			int firstResult = 0;
			if (page != 0) {
				firstResult = page * filtro.getMax();
			}
			query.setFirstResult(firstResult);
			query.setMaxResults(filtro.getMax());
		}

		query.setCacheable(true);
		List<RelatorioAcessosVO> acessos = query.list();

		return acessos;
	}

	public Query queryAcessosByVersao(RelatorioFiltro filtro, boolean count) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;
		String versao = filtro.getVersao();
		Long empresa = filtro.getEmpresaId();

		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		if (count) {
			sb.append("select count(distinct versaoAplicativo) ");
		} else {
			sb.append("select distinct new br.livetouch.livecom.domain.vo.RelatorioAcessosVersaoVO (deviceSo,versaoAplicativo,count(distinct login)) ");
		}
		sb.append(" from LogTransacao ");

		sb.append(" where 1=1 ");

		if (dataInicio != null) {
			sb.append(" and dataFim >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and dataFim <= :dataFim");
		}
		
		if (empresa != null) {
			sb.append(" and empresa.id = :empresa");
		}

		if (StringUtils.isNotEmpty(versao)) {
			sb.append(" and versaoAplicativo = :versao");
		}

		sb.append(" and requestPath = '/ws/login.htm'");

		if (!count) {
			sb.append(" group by deviceSo, versaoAplicativo");
			sb.append(" order by versaoAplicativo desc");
		}

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}
		
		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}

		if (StringUtils.isNotEmpty(versao)) {
			q.setParameter("versao", versao);
		}

		q.setCacheable(true);

		return q;

	}

	@Override
	public long getCountVersaoByFilter(RelatorioFiltro filtro) throws DomainException {
		Query query = queryAcessosByVersao(filtro, true);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	@Override
	public List<RelatorioAcessosVersaoVO> findAcessosByVersao(RelatorioFiltro filtro) throws DomainException {
		Query query = queryAcessosByVersao(filtro, false);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		query.setCacheable(true);
		List<RelatorioAcessosVersaoVO> acessos = query.list();

		return acessos;
	}

	@Override
	public List<Object[]> findIdLoginsUsuario() {
		return createQuery("select id,login,empresa.id from Usuario").list();
	}

	@Override
	public List<Usuario> findByNomeLikeLogin(UsuarioAutocompleteFiltro filtro, Empresa empresa) {
		StringBuffer sb = new StringBuffer();

		sb.append("from Usuario where 1=1 and empresa.id = :empresaId");
		
		String nome = null;
		String login = null;
		if (filtro != null) {
			nome = filtro.getLogin();
			login = filtro.getLogin();

			
			if (StringUtils.isNotEmpty(nome) && StringUtils.isNotEmpty(login)) {
				sb.append(" and (nome like :nome or login like :login)");
			} 
		}
		
		
		Query query = createQuery(sb.toString());
		if (StringUtils.isNotEmpty(nome)) {
			query.setParameter("nome", "%" + nome + "%");
		}
		if (StringUtils.isNotEmpty(login)) {
			query.setParameter("login", "%" + login + "%");
		}
		query.setLong("empresaId", empresa.getId());
		query.setCacheable(true);

		return query.list();
	}

	@Override
	public List<Usuario> findAll(Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Usuario u ");
		if(empresa != null) {
			sb.append(" where u.empresa = :empresa");
		}
		Query q = createQuery(sb.toString());
		if(empresa != null) {
			q.setParameter("empresa", empresa);
		}
		return q.list();
	}

	@Override
	public List<Long> findAllIds(Empresa empresa) {
		StringBuffer sb = new StringBuffer("SELECT u.id FROM Usuario u WHERE u.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setParameter("empresa", empresa);
		return q.list();
	}

	@Override
	public List<String> findLoginsByIds(List<Long> ids) {
		Query q = createQuery("SELECT u.login FROM Usuario u WHERE u.id in(:ids) and u.receivePush = 1");
		q.setParameterList("ids", ids);
		return q.list();
	}

	@Override
	public Long cont(Empresa empresa) {
		Query q = createQuery("SELECT COUNT(id) FROM Usuario u where u.empresa.id = :empresaId");
		q.setParameter("empresaId", empresa.getId());
		return (Long) (q.list().size() > 0 ? q.list().get(0) : 0L);
	}

	@Override
	public void setNullFuncaoByCargos(List<Funcao> cargos) {
		StringBuffer sb = new StringBuffer("update Usuario u set u.funcao = NULL ");
		sb.append(" where u.funcao in(:funcoes)");
		Query q = createQuery(sb.toString());
		q.setParameterList("funcao", cargos);
		q.executeUpdate();
	}

	@Override
	public List<Usuario> findByCargos(List<Funcao> cargos) {
		StringBuffer sb = new StringBuffer("from Usuario u where u.funcao in(:list)");
		Query q = createQuery(sb.toString());
		q.setParameterList("funcao", cargos);
		q.setCacheable(true);
		return q.list();
	}
	
	@Override
	public boolean isUsuarioDentroHorario(Usuario u) {
		if(u == null) {
			return false;
		}
		
		StringBuffer sb = new StringBuffer("select u.id from Usuario u ");

		sb.append(" where u = :usuario ");
		
		// quem nao tem horario cadastrado
		sb.append(" and ((u.horaAbertura is null and u.horaFechamento is null) ");
		
		// esta dentro do horario
		sb.append("or (CONVERT_DATE_TO_TIME(u.horaAbertura) <= CONVERT_DATE_TO_TIME(:data) and CONVERT_DATE_TO_TIME(u.horaFechamento) >= CONVERT_DATE_TO_TIME(:data))");
		sb.append("or (CONVERT_DATE_TO_TIME(u.horaAbertura) = CONVERT_DATE_TO_TIME(u.horaFechamento)))");

		Query q = createQuery(sb.toString());
		
		q.setTime("data", new Date());
		q.setParameter("usuario", u);
		
		// Nao pode fazer cache pois valida horario.
		Long count = getCount(q);
		return count > 0;
	}

	@Override
	public List<Long> usuariosDentroHorarioExpediente(List<Long> idsToFilter) {
		
		Semana dia = Semana.today();
		
		// SEG, TER, QUA, etc
		String diaHoje = dia.getDia();
		
		StringBuffer sb = new StringBuffer("select u.id from Usuario u left join u.expediente e ");

		sb.append(" where u.id in (:users) and u.receivePush = 1");

		// quem nao tem horario cadastrado
		sb.append(" and ((u.horaAbertura is null and u.horaFechamento is null) ");
		
		// quem esta dentro do horario de trabalho
		sb.append(" or (CONVERT_DATE_TO_TIME(u.horaAbertura) <= CONVERT_DATE_TO_TIME(:data) and CONVERT_DATE_TO_TIME(u.horaFechamento) >= CONVERT_DATE_TO_TIME(:data))");
		sb.append(" or (CONVERT_DATE_TO_TIME(u.horaAbertura) = CONVERT_DATE_TO_TIME(u.horaFechamento))) ");
		
		// quem nao tem expediente cadastrado ou trabalha no dia de hoje
		sb.append(" and (e is null or e."+diaHoje+"=1)");

		Query q = createQuery(sb.toString());
		q.setTime("data", new Date());
		q.setParameterList("users", idsToFilter);

		// Nao pode fazer cache pois valida horario.
		List<Long> ids = q.list();
		return ids;
	}

	@Override
	public boolean existsByLogin(Usuario u, Empresa empresa) {
		List<Long> ids = null;
		
		String login = StringUtils.lowerCase(u.getLogin());
		
		StringBuffer sb = new StringBuffer("select id from Usuario u where u.login = ?");
		sb.append(" and u.empresa.id = ?");
		
		if(u.getId() != null) {
			sb.append(" and u.id != ?");
			ids = queryIds(sb.toString(), true, login, empresa.getId(), u.getId());
		} else {
			ids = queryIds(sb.toString(), true, login, empresa.getId());
		}

		return !ids.isEmpty();
	}
	
	@Override
	public boolean existsByEmail(Usuario u, Empresa empresa) {
		List<Long> ids = null;
		
		String email = StringUtils.lowerCase(u.getEmail());
		
		StringBuffer sb = new StringBuffer("select id from Usuario u where u.email = ?");
		sb.append(" and u.empresa.id = ?");
		
		if(u.getId() != null) {
			sb.append(" and u.id != ?");
			ids = queryIds(sb.toString(), true, email, empresa.getId(), u.getId());
		} else {
			ids = queryIds(sb.toString(), true, email, empresa.getId());
		}

		return !ids.isEmpty();
	}

	@Override
	public Long getCountFindByStatus(Grupo g, StatusUsuario statusAtivacao, Empresa empresa) {
		Query query = getQueryFindByStatus(g, statusAtivacao, empresa, true);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}
	
	@Override
	public List<Usuario> findByStatus(Grupo g, StatusUsuario status, Empresa empresa, int page, int maxRows) {
		Query q = getQueryFindByStatus(g, status, empresa, false);

		setMaxResults(q, page, maxRows);

		q.setCacheable(true);
		List<Usuario> list = q.list();
		return list;
	}
	
	private Query getQueryFindByStatus(Grupo g, StatusUsuario status, Empresa empresa, boolean count) {
		StringBuffer sb = new StringBuffer();
		
		if(count) {
			sb.append("select count(distinct u.id) from Usuario u where 1=1");
		} else {
			sb.append("from Usuario u where 1=1");
		}
		
		if(g != null) {
			sb.append(" and u.grupo.id=:grupo ");
		}
		
		if(status != null) {
			sb.append(" and (u.status is not null and u.status = :status) ");
		}
		
		if(empresa != null) {
			sb.append(" and u.empresa = :empresa ");
		}
		
		if(!count) {
			sb.append(" order by u.nome");
		}
		
		Query q = createQuery(sb.toString());
		
		if(g != null) {
			q.setParameter("grupo", g.getId());
		}
		
		if(status != null) {
			q.setParameter("status", status);
		}
		
		if(empresa != null) {
			q.setParameter("empresa", empresa);
		}

		return q;
	}

	@Override
	public UsuarioStatusCountVO getCountUsuariosStatus(Empresa empresa) {
		UsuarioStatusCountVO vo = new UsuarioStatusCountVO();
		
		Query q = queryFindNovos(empresa, true);
		vo.setNovo((Long) q.list().get(0));
		
		q = queryFindAtivos(empresa, true);
		vo.setAtivo((Long) q.list().get(0));
		
		q = queryFindInativos(empresa, true);
		vo.setInativo((Long) q.list().get(0));

		return vo;
	}

	@Override
	public List<Usuario> findNovos(Empresa empresa) {
		Query q = queryFindNovos(empresa, false);
		return q.list();
	}

	@Override
	public List<Usuario> findAtivos(Empresa empresa) {
		Query q = queryFindAtivos(empresa, false);
		return q.list();
	}
	
	@Override
	public List<Usuario> findInativos(Empresa empresa) {
		Query q = queryFindInativos(empresa, false);
		return q.list();
	}
	
	private Query queryFindNovos(Empresa empresa, boolean count) {
		StringBuffer sb = new StringBuffer();
		if(count) {
			sb = new StringBuffer("SELECT count(u.id) FROM Usuario u ");
		} else {
			sb = new StringBuffer("FROM Usuario u ");
		}
		
		sb.append(" where (status = :novo OR status = :enviadoConvite) AND (perfilAtivo = 1 OR perfilAtivo is null) AND u.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setParameter("novo", StatusUsuario.NOVO);
		q.setParameter("enviadoConvite", StatusUsuario.ENVIADO_CONVITE);
		q.setParameter("empresa", empresa);
		return q;
	}
	
	private Query queryFindAtivos(Empresa empresa, boolean count) {
		StringBuffer sb = new StringBuffer();
		if(count) {
			sb = new StringBuffer("SELECT count(u.id) FROM Usuario u ");
		} else {
			sb = new StringBuffer("FROM Usuario u ");
		}
		
		sb.append("where (status = :ativo OR status = :conviteLogou OR status = :conviteLogouMobile) ");
		sb.append("AND (perfilAtivo = 1 OR perfilAtivo is null) AND u.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setParameter("ativo", StatusUsuario.ATIVO);
		q.setParameter("conviteLogou", StatusUsuario.ENVIADO_CONVITE_JA_LOGOU);
		q.setParameter("empresa", empresa);
		q.setParameter("conviteLogouMobile", StatusUsuario.ENVIADO_CONVITE_JA_LOGOU_MOBILE);
		return q;
	}

	private Query queryFindInativos(Empresa empresa, boolean count) {
		StringBuffer sb = new StringBuffer();
		if(count) {
			sb = new StringBuffer("SELECT count(u.id) FROM Usuario u ");
		} else {
			sb = new StringBuffer("FROM Usuario u ");
		}
		sb.append("WHERE (perfilAtivo = 0 OR perfilAtivo is null) AND u.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setParameter("empresa", empresa);
		return q;
	}
	
	@Override
	public List<Amizade> findUsuariosByStatusAmizade(StatusAmizade aprovada, Usuario usuario) {
		return findUsuariosByStatusAmizade(aprovada, usuario, "");
	}
	
	@Override
	public List<Amizade> findUsuariosByStatusAmizade(StatusAmizade status, Usuario u, String nome) {
		StringBuffer sb = new StringBuffer("select a from Amizade a where 1=1 ");
		
		if(u != null) {
			sb.append(" and ((a.amigo = :usuario) or (a.usuario = :usuario))");
		}
		
		if(StringUtils.isNotEmpty(nome)) {
			sb.append(" and ((a.amigo.nome like :nome) or (a.usuario.nome like :nome))");
		}

		if(status != null) {
			sb.append(" and a.statusAmizade = :status");
		}
		
		sb.append(" order by a.amigo.nome");
		
		Query q = createQuery(sb.toString());
		
		if(u != null) {
			q.setParameter("usuario", u);
		}
		
		if(StringUtils.isNotEmpty(nome)) {
			q.setParameter("nome", "%" + nome + "%");
		}

		if(status != null) {
			q.setParameter("status", status);
		}
		
		q.setCacheable(true);
		List<Amizade> list = q.list();
		return list;
	}
	
	@Override
	public List<Long> findUsuariosToPush(Post p, List<Long> amigos, Idioma idioma) {
		Long empresaId = p.getUsuario().getEmpresa().getId();
		String on = ParametrosMap.getInstance(empresaId).get(Params.USUARIO_RESTRICAO_HORARIO_ON, "0");
		
		Semana dia = Semana.today();
		String diaHoje = dia.getDia();
		
		StringBuffer sb = new StringBuffer("select distinct u.id from Usuario u left join u.expediente e where 1=1 ");

		sb.append(" and u.empresa.id=:empresaId ");

		sb.append(" and u.perfilAtivo = :ativo ");

		if(on.equals("1")) {
			sb.append(" and ((u.horaAbertura is null and u.horaFechamento is null) ");
			sb.append(" or (CONVERT_DATE_TO_TIME(u.horaAbertura) <= CONVERT_DATE_TO_TIME(:data) and CONVERT_DATE_TO_TIME(u.horaFechamento) >= CONVERT_DATE_TO_TIME(:data))");
			sb.append(" or (CONVERT_DATE_TO_TIME(u.horaAbertura) = CONVERT_DATE_TO_TIME(u.horaFechamento))) ");
			sb.append(" and (e is null or e."+diaHoje+"=1) ");
		}
		
		// TODO AUGUSTO AMIZADE buscar antes.
		if(!amigos.isEmpty()) {
			sb.append(" and u.id in (:amigos) ");
		}
		
		if(idioma != null) {
			sb.append(" and u.idioma = :idioma ");
		}

		Query q = createQuery(sb.toString());
		q.setParameter("empresaId", empresaId);
		q.setParameter("ativo", true);
		
		if(on.equals("1")) {
			q.setTime("data", p.getDataPublicacao());
		}
		
		if(!amigos.isEmpty()) {
			q.setParameterList("amigos", amigos);
		}
		
		if(idioma != null) {
			q.setParameter("idioma", idioma);
		}

		q.setCacheable(true);
		List<Long> list = q.list();
		return list;
	}

	@Override
	public Date updateDataLastChat(Long userId) {
		Date date = new Date();
		Query q = createQuery("update Usuario set dataLastChat=? where id=?");
		q.setParameter(0, date);
		q.setParameter(1, userId);
		q.executeUpdate();
		return date;
	}

	@Override
	public void updateAllDataSenha(Long empresaId) {
		Date now = new Date();
		Query q = createQuery("update Usuario set dataUpdatedSenha=? where empresa_id=?");
		q.setParameter(0, now);
		q.setParameter(1, empresaId);
		q.executeUpdate();
	}
}