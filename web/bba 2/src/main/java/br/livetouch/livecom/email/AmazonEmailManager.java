package br.livetouch.livecom.email;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;

import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AWSJavaMailTransport;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.ListVerifiedEmailAddressesResult;
import com.amazonaws.services.simpleemail.model.VerifyEmailAddressRequest;

import br.infra.util.Log;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.utils.S3Helper;

public class AmazonEmailManager implements EmailManager {
	protected static Logger log = Log.getLogger(EmailManager.class);
	
	public static void main(String[] args) {
		try {
			AmazonEmailManager a = new AmazonEmailManager();
			a.sendEmail("rlecheta@livetouch.com.br", "rlecheta@gmail.com", "SES", "teste", true);
			System.out.println("go");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean sendEmail(String to, String subject, String body, boolean html) throws IOException {
		ParametrosMap params = ParametrosMap.getInstance();
		String from = params.get(Params.EMAIL_EMAIL_ADMIN,"livecom@livetouch.com.br");

		return sendEmail(from,to, subject, body, html);
	}
	
	public String getCredentials() {
		ParametrosMap params = ParametrosMap.getInstance();
		String credentials = params.get(Params.FILE_MANAGER_AMAZON_CREDENTIALS);
		if(StringUtils.isEmpty(credentials)) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.amazon.credentials]");
		}
		return credentials;
	}

	public boolean sendEmail(String from,String to,String subject, String body, boolean html) throws IOException {
		PropertiesCredentials credentials = new PropertiesCredentials(S3Helper.class.getResourceAsStream(getCredentials()));
		AmazonSimpleEmailService ses = new AmazonSimpleEmailServiceClient(credentials);
		Region r = Region.getRegion(Regions.US_EAST_1);
		ses.setRegion(r);

		// No modo Sandbox, você precisa validar os emails "from" e "to" para confirmar que pertencem a você
		verifyEmailAddress(ses, from);
		verifyEmailAddress(ses, to);

		/*
		 * Configura o JavaMail para utilizar o Amazon SES.
		 */
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "aws");

		// Informa as chaves de acesso da AWS
		props.setProperty("mail.aws.user", credentials.getAWSAccessKeyId());
		props.setProperty("mail.aws.password", credentials.getAWSSecretKey());

		Session session = Session.getInstance(props);

		try {
			// Dados da mensagem
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(from));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			msg.setSubject(subject);
			msg.setText(body);
			msg.saveChanges();

			// Envia o email
			Transport t = new AWSJavaMailTransport(session, null);
			t.connect();
			t.sendMessage(msg, null);

			// Fecha a conexão de envio
			t.close();
			
			return true;
		} catch (MessagingException e) {
			throw new IOException(e.getMessage(), e);
		}
	}

	// Verifica se o e-mail  está validado no modo de acesso Sandbox
	// Se não estiver o e-mail  é enviado com o link de validação
	private static void verifyEmailAddress(AmazonSimpleEmailService ses, String address) {
		ListVerifiedEmailAddressesResult verifiedEmails = ses.listVerifiedEmailAddresses();
		if (verifiedEmails.getVerifiedEmailAddresses().contains(address)) {
			// E-mail está validado
			return;
		}
		// Envia o email de validação
		ses.verifyEmailAddress(new VerifyEmailAddressRequest().withEmailAddress(address));
		System.out.println("Please check the email address " + address + " to verify it");
		System.exit(0);
	}

	@Override
	public boolean sendEmail(String from, String[] to, String subject, String body, boolean html, String[] bcc) throws IOException, EmailException {
		return false;
	}
}
