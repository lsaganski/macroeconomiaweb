package br.livetouch.livecom.utils;

public class TimeUtils {

	private static long timeA;
	private static long timeB;

	public static void start() {
		timeA = System.currentTimeMillis();
	}
	
	public static void end() {
		timeB = System.currentTimeMillis();
	}
	
	public static long getTimeMs() {
		return timeB - timeA;
	}
	
	public static String getTimeMsString() {
		return "Time ms: " + getTimeMs();
	}
	
	public static long getTimeSegundos() {
		return (timeB - timeA)/1000;
	}
	
	public static String getTimeSegundosString() {
		return "Time seg: " + getTimeSegundos();
	}
	
	public static long getTimeMinutos() {
		return (timeB - timeA)/1000/60;
	}
	
	public static String getTimeMinutosString() {
		return "Time min: " + getTimeMinutos();
	}
}
