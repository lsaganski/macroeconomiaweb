package br.livetouch.livecom.domain.repository;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.EmailContato;

@Repository
public interface EmailContatoRepository extends net.livetouch.tiger.ddd.repository.Repository<EmailContato> {
	
}