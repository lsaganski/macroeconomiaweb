package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Palestrante;
import net.livetouch.tiger.ddd.DomainException;

public interface PalestranteService extends Service<Palestrante> {

	List<Palestrante> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Palestrante f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Palestrante f) throws DomainException;

}
