package br.livetouch.livecom.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class TagAuditoria extends Auditoria {
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "TAGAUDITORIA_SEQ")
	private Long id;

	@Column(length=1000)
	private String msg;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = true)
	private Usuario user;
	private Date data;
	
	// 1 update, 2 delete.
	private int tipo;
	
	private Long tagId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;

	@Override
	public Long getId() {
		return id;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

	public Date getDate() {
		return data;
	}

	public void setDate(Date date) {
		this.data = date;
	}
	
	public Long getTagId() {
		return tagId;
	}
	
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public int getTipo() {
		return tipo;
	}
	
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "TagAuditoria [id=" + id + ", msg=" + msg + ", user=" + user + ", date=" + data + ", tagId=" + tagId + "]";
	}

	public static TagAuditoria audit(Tag t,Usuario userInfo, String msg, int tipo) {
		TagAuditoria a = new TagAuditoria();
		a.setDate(new Date());
		a.setUser(userInfo);
		a.setMsg(msg);
		a.setTipo(tipo);
		a.setTagId(t.getId());
		a.setEmpresa(userInfo.getEmpresa());
		return a;
	}
	
	@Override
	public Long getRefId() {
		return tagId;
	}
	
	public String getTipoString() {
		if(tipo == 0) {
			return "update";
		}
		if(tipo == 2) {
			return "delete";
		}
		else 
			return "create";
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
