package br.livetouch.livecom.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import br.infra.util.ServletUtils;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;

public class XSSFilter implements Filter { 
	
	private static final boolean LOG = false;
	  
    @Override 
    public void init(FilterConfig filterConfig) throws ServletException {
    	boolean XSS_ON = ParametrosMap.getInstance().getBoolean(Params.SECURITY_XSS_ON, true);
    	if(!XSS_ON) {
    		return;
    	}
    } 
  
    @Override 
    public void destroy() { 
    } 
  
    @Override 
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
        try {
        	if(LOG) {
        		String url = ServletUtils.getRequestUrl(request);
        	}
    		
			XSSRequestWrapper xssRequest = new XSSRequestWrapper((HttpServletRequest) request);
			chain.doFilter(xssRequest, response);
		} catch (Exception e) {
			e.printStackTrace();
			chain.doFilter(request, response);
		}
    } 
  
} 
