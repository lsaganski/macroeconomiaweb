package br.livetouch.livecom.web.pages.admin;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;

import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.RelatorioConversaFiltro;
import br.livetouch.livecom.web.pages.report.RelatorioPage;



@Controller
@Scope("prototype")
public class DetalhesRelatorioConversaPage extends RelatorioPage {

	public Long conversa;
	public String filtroJson;
	public RelatorioConversaFiltro filtro;
	public List<MensagemVO> vos;
	
	public boolean isGrupo = false;

	@Override
	public void onInit() {
		
		RelatorioConversaFiltro filtro = (RelatorioConversaFiltro) getContext().getSessionAttribute(RelatorioConversaFiltro.SESSION_FILTRO_KEY);
		if(filtro == null) {
			setRedirect(RelatorioConversaPage.class);
		}
		
		if(filtro != null) {
			filtro.setGroup(null);
			filtro.setUser(null);
			if(conversa != null) {
				filtro.setId(conversa);
				MensagemConversa c = mensagemService.getMensagemConversa(conversa);
				if(c != null) {
					isGrupo = c.getGrupo() != null;
				}
			}
			filtroJson = new Gson().toJson(filtro);
		}
		
	}
	
	@Override
	public String getContentType() {
		return super.getContentType();
	}
	
	@Override
	public String getTemplate() {
		return super.getTemplate();
	}
	
//	private void table() {
//		Column c = new Column("dataUpdated", getMessage("data.label"));
//		c.setAttribute("align", "left");
//		table.addColumn(c);
//		
//		c = new Column("from", getMessage("from.label"));
//		c.setAttribute("align", "left");
//		table.addColumn(c);
//
//		c = new Column("to", getMessage("to.label"));
//		c.setAttribute("align", "left");
//		table.addColumn(c);
//		
//		c = new Column("msg", getMessage("coluna.mensagem.label"));
//		c.setAttribute("align", "left");
//		table.addColumn(c);
//		
//		if(filtro.getGroup() == null) {
//		
//			c = new Column(getMessage("status.conversa.label"));
//			c.setDecorator(new Decorator() {
//				
//				@Override
//				public String render(Object object, Context context) {
//					MensagemVO vo = (MensagemVO) object;
//					if(vo.getLida() == 1) {
//						//icon lida
//						return "<i class='icones lida'></i>";
//					} else if (vo.getEntregue() == 1) {
//						//icon entregue
//						return "<i class='icones entregue'></i>";
//					} else {				
//						//icon enviada
//						return "<i class='icones enviada'></i>";
//					}
//					
//				}
//			});
//			c.setAttribute("align", "center");
//			table.addColumn(c);
//		} else {
//			c = new Column("detalhes", getMessage("detalhes.label"));
//			c.setTextAlign("center");
//			c.setAttribute("align", "center");
//			c.setDecorator(new LinkDecorator(table, detalhes, "id"));
//			table.addColumn(c);
//		}
//		
//	}
//	
//	public boolean detalhes() {
//		setRedirect(DetalhesStatusRelatorioConversaPage.class, "mensagemId", detalhes.getValue());
//		return false;
//	}
	
}
