package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Playlist;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.PlaylistRepository;
import br.livetouch.livecom.domain.service.PlaylistService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class PlaylistServiceImpl implements PlaylistService {
	@Autowired
	private PlaylistRepository rep;

	@Override
	public Playlist get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Playlist c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Playlist> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void delete(Playlist c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public List<Playlist> findAllByUser(Usuario u) {
		return rep.findAllByUser(u);
	}

	@Override
	public List<Post> findAllPostsByUser(Usuario u, int page, int max) {
		return rep.findAllPostsByUser(u,page,max);
	}

	@Override
	public Playlist findByUserPost(Usuario u, Post p) {
		return rep.findByUserPost(u, p);
	}
}
