package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.PostDestaque;
import br.livetouch.livecom.domain.vo.FiltroLink;

@Repository
public interface PostDestaqueRepository extends net.livetouch.tiger.ddd.repository.Repository<PostDestaque> {

	List<PostDestaque> findAllLinks(FiltroLink filtro);

}