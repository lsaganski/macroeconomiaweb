package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Historico;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.HistoricoRepository;
import br.livetouch.livecom.domain.service.HistoricoService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class HistoricoServiceImpl implements HistoricoService {
	@Autowired
	private HistoricoRepository rep;

	@Override
	public Historico get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Historico c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Historico> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void delete(Historico c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public List<Historico> findAllByUser(Usuario u) {
		return rep.findAllByUser(u);
	}
	
	@Override
	public List<Post> findAllPostsByUser(Usuario u, int page, int max) {
		return rep.findAllPostsByUser(u,page,max);
	}

	@Override
	public Historico findByUserPost(Usuario u, Post p) {
		return rep.findByUserPost(u,p);
	}
}
