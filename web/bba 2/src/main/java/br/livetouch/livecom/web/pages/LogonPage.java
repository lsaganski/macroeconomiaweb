
package br.livetouch.livecom.web.pages;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.Log;
import br.infra.util.ServletUtil;
import br.infra.util.Utils;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LoginReport;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.Plataforma;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.exception.HorarioGrupoException;
import br.livetouch.livecom.domain.exception.HorarioUsuarioException;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.rest.oauth.LivecomAuthorizationFlow;
import br.livetouch.livecom.utils.LoginCount;
import br.livetouch.livecom.web.pages.admin.LivecomPage;
import br.livetouch.livecom.web.pages.cadastro.EmpresaPage;
import br.livetouch.livecom.web.pages.cadastro.UsuarioPage;
import br.livetouch.livecom.web.pages.download.IndexPage;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import br.livetouch.livecom.web.pages.root.ProjetoPage;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;
import net.sf.click.control.HiddenField;
import net.sf.click.control.PasswordField;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class LogonPage extends LivecomPage {

	private static Logger logger = Log.getLogger(LogonPage.class);
	public Long id;
	
	public String msg;

	public Form form = new Form();

	private TextField tLogin;

	private PasswordField tSenha;

	public String background;
	public String favicon;
	
	public String from;
	public String fromParams;
	private Checkbox checkRemenber;
	private Usuario u;
	public boolean isCaptcha = false;
	
	@Autowired
	LivecomAuthorizationFlow livecomAuthorizationFlow;
	
	public boolean byPassSecurity;
	public int loginCount = 0;

	/**
	 * @see br.livetouch.livecom.web.pages.admin.LivecomAdminPage#onSecurityCheck()
	 */
	@Override
	public boolean onSecurityCheck() {
		
		
		if(isMobile()) {
			setRedirect(IndexPage.class);
			return false;
		}
		
		if(byPassSecurity) {
			boolean get = getContext().isGet();
			if(get) {
				return true;
			} else {
				String login = getParam("login");
				if("root@livetouch.com.br".equals(login)) {
					// Deixa o root entrar.
					return true;
				}
			}
		}

//		if(isBuildBJJ()) {
//			setRedirect(LoginPage.class);
//			return false;
//		}

		UserInfoVO info = UserInfoVO.getHttpSession(getContext());
		if(info != null) {
			setRedirect(MuralPage.class);
			return false;
		}

		if(getContext().isGet()) {
			String login = getCookieValue(COOKIE_LOGIN);
			if(StringUtils.isNotEmpty(login) && usuarioService != null) {
				Usuario user = usuarioService.findByLogin(login);
				if(user != null) {
					boolean ok = saveUserSessionAfterLoginAndRedirect(login, user);
					return ok;
				}
			}
		}
		
		return validateSecurity();
	}

	@Override
	public void onInit() {
		super.onInit();

		Empresa e = getEmpresa();
		background = e != null ? e.getUrlBg() : "";
		favicon = e != null ? e.getUrlFav() : "";
		
		form.setMethod("post");
		if(StringUtils.isNotEmpty(from)) {
			form.add(new HiddenField("from",from));
		}
		if(StringUtils.isNotEmpty(fromParams)) {
			form.add(new HiddenField("fromParams",fromParams));
		}
		
		form.add(new HiddenField("byPassSecurity", byPassSecurity));
		form.add(tLogin = new TextField("login",true));
		tSenha = new PasswordField("senha",true);
		tSenha.setAttribute("autocomplete","off");
		form.add(tSenha);
		
		form.add(checkRemenber = new Checkbox("remember"));
		
//		List<Empresa> empresas = empresaService.findAll();
//		if(empresas.size() > 1) {
//			form.add(tComboEmpresa = new ComboEmpresa(empresaService));
//		}

		tLogin.setFocus(true);
		
		if(id != null) {
			Usuario u = usuarioService.get(id);
			if(u != null) {
				tLogin.setValue(u.getLogin());
			}
		}
		
		Submit btlogon = new Submit("logon", "Entrar", this, "logon");
		btlogon.setAttribute("class", "botao salvar");
		form.add(btlogon);
	}

	@Override
	public String getTemplate() {
		return super.getPath();
	}

	@Override
	public void onGet() {
		super.onGet();
	}
	
	@Override
	public void onPost() {
	}

	public boolean logon() {
		if(form.isValid()) {
			
			String login = tLogin.getValue();
			String senha = tSenha.getValue();
			
			LoginReport report = new LoginReport();
			report.setPlataforma(Plataforma.WEB);
			report.setData(new Date());
			report.setLogin(login);
			
			log("Livecom login [" + login + "]");
			log("Livecom password [**********]");
			log("Livecom host [" + getHost() + "]");
			log("Livecom empresa [" + getEmpresa() + "]");
			
			try {
				
				boolean remember = checkRemenber.isChecked();

				if("livetouch".equals(login) && "livetouch@2013com".equals(senha)) {
					Usuario u = new Usuario();
					u.setLogin("admin");
					u.setNome("Admin");

					UserInfoVO userInfo = UserInfoVO.create(u);
					userInfo.addSession("web");
					UserInfoVO.setHttpSession(getContext(),userInfo);

					// OK
					setRedirect(HomePage.class);

				} else {
					
					if(usuarioService != null) {
						
						boolean isMobile = false;
						u = usuarioService.login(login, senha, getEmpresa(), isMobile);
						
						if(!u.isRoot() && !u.isAdmin()) {
							String modoRestricao = ParametrosMap.getInstance(getEmpresaId()).get(Params.RESTRICAO_HORARIO_MODO,"");
							
							if(modoRestricao.equals("grupo")) {
								boolean isGrupoDentroHorario = grupoService.isGruposDentroHorario(u);
	
								if(!isGrupoDentroHorario) {
									String msgGrupo = ParametrosMap.getInstance(u.getEmpresa().getId()).get("grupo.fora.horario.msg", "O sistema não é acessível fora do seu horário de trabalho.");
									throw new HorarioGrupoException(msgGrupo);
								}
							} else {
								boolean isUsuarioDentroHorario = true;
								if("1".equals(ParametrosMap.getInstance(getEmpresaId()).get(Params.USUARIO_RESTRICAO_HORARIO_ON,"0"))) {
									isUsuarioDentroHorario = usuarioService.isUsuarioDentroHorario(u) && Utils.isExpedienteValido(u.getExpediente());
								} 
								
								if(u != null && !isUsuarioDentroHorario) {
									String msgUsuario = ParametrosMap.getInstance(getEmpresaId()).get(Params.MSG_HORARIO_USUARIO,"O sistema não é acessível fora do seu horário de trabalho.");
									throw new HorarioUsuarioException(msgUsuario);
								}
							}
						}
						
						report.setUsuario(u);
						report.setStatus(true);
						report.setEmpresa(u.getEmpresa());
						if(u.isPerfilAtivo() == null || !u.isPerfilAtivo()){
							throw new DomainException("Perfil inativo");
						}
						
						if(StringUtils.isEmpty(u.getChave()) || u.getChave() == null) {
							usuarioService.gerarChave(u);
							usuarioService.saveOrUpdate(getUsuario(),u);
						}

						saveUserSessionAfterLoginAndRedirect(login, u);
						
						if(remember) {
							addCookie(COOKIE_LOGIN,login);
							logger.debug("Cookie para " + login + " adicionado.");
						}
						
						// OK
						//livecomAuthorizationFlow.accessToken(u, getContext().getRequest());
						this.isCaptcha = false;
						LoginCount.getInstance().clear(login);

					} else {
						form.setError("Servidor inDisponivel");
					}
				}
			}
			catch (DomainException e) {
				// Error
				loginCount = LoginCount.getInstance().increment(login);
				this.isCaptcha = this.isCaptcha();
				report.setErro(e.getMessage());
				report.setStatus(false);
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}catch (Exception e) {
				// Error
				loginCount = LoginCount.getInstance().increment(login);
				this.isCaptcha = this.isCaptcha();
				report.setErro(e.getMessage());
				report.setStatus(false);
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
			try {
				loginReportService.saveOrUpdate(report);
			} catch (DomainException e) {
				e.printStackTrace();
			}
		} else {
			log("Form is invalid: " + form.getError());
		}
		return true;
	}

	@Override
	protected boolean loginOk(Usuario u) {
		if(!u.getAtivo()) {
			u.setStatus(StatusUsuario.ATIVO);
			try {
				usuarioService.saveOrUpdate(getUsuario(),u);
			} catch (DomainException e) {
				logError(e.getMessage(), e);
			}
		} 
		
		if(u.isRoot()) {
			return redirectRoot();
		} else if(u.isPreCadastro()) {
			setRedirect(UsuarioPage.class, "id", u.getId().toString());
			return true;
		} else {
			setRedirect(MuralPage.class);
			return true;
		}
	}
	
	private boolean redirectRoot() {
		List<Empresa> empresas = empresaService.findAll();
		
		if(empresas.size() == 1) {
			Empresa empresa = empresas.get(0);
			if(empresa == null) {
				return false;
			}
			
			setRedirect(EmpresaPage.class, "id", empresa.getId().toString());
			return true;
		}
		
		setRedirect(ProjetoPage.class);
		return true;
	}

	protected boolean redirectAfterLogin() {
		if(StringUtils.isNotEmpty(from)) {
			if(StringUtils.isEmpty(fromParams)) {
				setRedirect(from);
				return true;
			} else {
				setRedirect(from+"?"+fromParams);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void onRender() {
		super.onRender();
	}

	public static void redirect(LivecomPage page) {
		Map<String, String> params = new HashMap<String, String>();
		String from = page.getPath();
		if(!"/index.htm".equals(from)) {
			params.put("from",from);
			HttpServletRequest request = page.getContext().getRequest();
			String s = ServletUtil.getRequestParams(request);
			if (StringUtils.isNotEmpty(s)) {
				params.put("fromParams", s);
			}
		}
		page.setRedirect(LogonPage.class,params);
	}
	
	protected void log(String string) {
		String host = getHost();
		logger.debug(host + ": " + string);
	}
	
	protected void logError(Object message, Throwable t) {
		String host = getHost();
		logger.error(host + ": " + message, t);
	}
	
	protected void logError(Object message) {
		String host = getHost();
		logger.error(host + ": " + message);
	}
	
	protected boolean isCaptcha() {
		boolean isCaptchaOn = ParametrosMap.getInstance(getEmpresa()).getBoolean(Params.LOGIN_FORM_CAPTCHA_ON, false);
		if(!isCaptchaOn) {
			return false;
		}
		
		int count = ParametrosMap.getInstance(getEmpresa()).getInt(Params.LOGIN_FORM_CAPTCHA_QTD, 3);
		if(count == 0) {
			return true;
		}
		
		return loginCount >= count;
	}
}
