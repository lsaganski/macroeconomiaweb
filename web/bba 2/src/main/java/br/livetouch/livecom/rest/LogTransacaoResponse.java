package br.livetouch.livecom.rest;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import com.google.gson.Gson;

@Provider
public class LogTransacaoResponse extends LogTransacaoUtil implements ContainerResponseFilter {
	 
	@Override
	public void filter(ContainerRequestContext req, ContainerResponseContext resp) throws IOException {
		Object entity = resp.getEntity();
		if(entity != null) {
			Gson gson = new Gson();
			String json = gson.toJson(entity);
			saveLogTransacao(json);
		}
	}
}

