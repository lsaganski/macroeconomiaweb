package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.utils.DateUtils;

public class NotificationVO implements Serializable {
	private static final long serialVersionUID = 1L;

	public Long id;
	public String tipo;
	public String texto;
	public String textoNotificacao;
	public String titulo;
	public Long postId;

	public Long parentId;

	public Long grupoId;
	public String grupo;
	public String urlThumbGrupo;

	public Long idUsuario;
	public String loginUsuario;
	public String nomeUsuario;
	public String urlThumbUsuario;
	public String hora;

	public String data;
	public String dataPush;
	public Long timestamp;
	
	public String msg;
	public Long conversaId;
	
	public Long badge;
	
	public boolean lida;
	public boolean sendPush;

	private String dataHora;
	
	private ComentarioVO comentario;
	private PostSimpleVO post;

	public List<NotificationVO> notifications;
	private List<UsuarioSimpleVO> usuarios;
	
	public NotificationVO() {
			
	}

	public NotificationVO(Notification n) {
		report(n);
	}

	public NotificationVO(NotificationUsuario n) {
		id = n.getId();
		lida = n.isLida();
		Usuario u = n.getUsuario();
		nomeUsuario = u.getNome();
		idUsuario = u.getId();
		loginUsuario = u.getLogin();
		dataPush = DateUtils.toString(n.getDataPush(), "dd/MM/yyyy HH:mm");
	}

	public void copyFrom(NotificationUsuario notUsuario) {
		Notification n = notUsuario.getNotification();
		Usuario from = n.getUsuario();
		this.id = notUsuario.getId();
		this.tipo = n.getTipo().toString();
		this.titulo = n.getTitulo();
		this.texto = n.getTexto();
		

		this.idUsuario = from.getId();
		this.loginUsuario = from.getLogin();
		this.nomeUsuario = from.getNome();
		this.urlThumbUsuario = from.getUrlThumb();

		Post post = n.getPost();
		if(post != null) {
			this.postId = post.getId();
		}

		Grupo grupo = n.getGrupo();
		if(grupo != null) {
			this.grupoId = grupo.getId();
			this.grupo = grupo.getNome();
			this.urlThumbGrupo = grupo.getUrlFotoCapa();
		}

		this.textoNotificacao = n.getTextoNotificacao();

		if(n.getMensagem() != null) {
			this.msg = n.getMensagem().getMsg();
		}
		
		if(n.getTipo() == TipoNotificacao.COMENTARIO) {
			Comentario c = n.getComentario();
			if(c != null) {
				this.comentario = new ComentarioVO();
				this.comentario.setComentario(c);
			}
		}

		if(n.getTipo() == TipoNotificacao.NEW_POST) {
			Post p = n.getPost();
			if(p != null) {
				this.post = new PostSimpleVO();
				this.post.setPost(p);
			}
		}

		if(n.getData() != null) {
			this.data = n.getDataStringHojeOntem();
			boolean isHojeOrOntem = StringUtils.equalsIgnoreCase("hoje", StringUtils.trim(this.data)) || StringUtils.equalsIgnoreCase("ontem", StringUtils.trim(this.data));
			this.setDataHora("");
			if(isHojeOrOntem) {
				this.setDataHora(this.data + " " + DateUtils.toString(n.getData(), "H:mm")); 
			}
			this.timestamp = n.getData().getTime();
		}

		this.lida = notUsuario.isLida();
	}
	
	public void report(Notification n) {
		// TODO ajustar report notification
//		this.id = n.getId();
//		this.tipo = n.getTipo().toString();
//
//		this.idUsuario = n.getTo().getId();
//		this.loginUsuario = n.getTo().getLogin();
//		this.nomeUsuario = n.getTo().getNome();
//		this.urlThumbUsuario = n.getTo().getUrlThumb();
//
//		this.postId = n.getPost().getId();
//		this.titulo = n.getTitulo();
//		this.texto = n.getTexto();
//
//		if(n.getMensagem() != null) {
//			this.msg = n.getMensagem().getMsg();
//		}
//
//		if(n.getParent() != null) {
//			this.parentId = n.getParent().getId();
//		}
//
//		if(n.getData() != null) {
//			this.data = n.getDataStringHojeOntem();
//			this.timestamp = n.getData().getTime();
//		}
//
//		if(n.getDataPush() != null) {
//			this.dataPush = DateUtils.toString(n.getDataPush(), "dd/MM/yyyy HH:mm");
//		}
//
//		this.sendPush = n.isSendPush();
//		this.lida = n.isLida();
	}

	public String getTituloDesc() {
		if(StringUtils.isNotEmpty(titulo)) {
			return titulo;
		}
		if(StringUtils.isNotEmpty(msg)) {
			if(msg.length() > 40) {
				return msg.substring(0,40)+"...";
			}
			return msg;
		}
		return null;
	}

	@Override
	public String toString() {
		return "NotificationVO [id=" + id + ", tipo=" + tipo + ", nomeUsuario=" + nomeUsuario + ", urlFotoUsuario=" + urlThumbUsuario + ", msg=" + msg + ", data=" + data + "]";
	}

	public String getDataHora() {
		return dataHora;
	}

	public void setDataHora(String dataHora) {
		this.dataHora = dataHora;
	}

	public List<NotificationVO> getNotifications() {
		if(notifications == null) {
			return new ArrayList<NotificationVO>();
		}
		return notifications;
	}

	public void setNotifications(List<NotificationVO> notifications) {
		this.notifications = notifications;
	}

	public List<UsuarioSimpleVO> getUsuarios() {
		if(usuarios == null) {
			return new ArrayList<UsuarioSimpleVO>();
		}
		return usuarios;
	}

	public void setUsuarios(List<UsuarioSimpleVO> usuarios) {
		this.usuarios = usuarios;
	}
}
