package br.livetouch.livecom.rest.resource;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.enums.TipoTemplate;
import br.livetouch.livecom.domain.vo.TemplateVO;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/template")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class TemplateResource extends MainResource {
	protected static final Logger log = Log.getLogger(TemplateResource.class);
	
	@POST
	public Response create(TemplateEmail template) {

		try {
			
			if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.ENVIAR_EMAILS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Long empresaId = getUserInfoVO().getEmpresaId();
			Empresa empresa = empresaService.get(empresaId);
			templateEmailService.saveOrUpdate(template, empresa);
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", new TemplateVO(template))).build();
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel efetuar o cadastro do template")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@PUT
	public Response update(TemplateEmail template) {
		try {
			
			if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.ENVIAR_EMAILS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Long empresaId = getUserInfoVO().getEmpresaId();
			Empresa empresa = empresaService.get(empresaId);
			templateEmailService.saveOrUpdate(template, empresa);
			return Response.ok(MessageResult.ok("Template atualizado com sucesso", new TemplateVO(template))).build();
		}  catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar o template")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@GET
	public Response findAll() {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.ENVIAR_EMAILS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Long empresaId = getUserInfoVO().getEmpresaId();
		Empresa empresa = empresaService.get(empresaId);
		List<TemplateEmail> templates = templateEmailService.findAll(empresa);
		List<TemplateVO> vos = TemplateVO.fromList(templates);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/tipo")
	public Response findByTipo(@QueryParam("tipo") String tipo) {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.ENVIAR_EMAILS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		UserInfoVO userInfoVO = getUserInfoVO();
		if(userInfoVO == null) {
			return null;
		}
		Long empresaId = userInfoVO.getEmpresaId();
		Empresa empresa = empresaService.get(empresaId);
		List<TemplateEmail> templates = templateEmailService.findByTipo(empresa, TipoTemplate.valueOf(tipo));
		List<TemplateVO> vos = TemplateVO.fromList(templates);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.ENVIAR_EMAILS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		TemplateEmail template = templateEmailService.get(id);
		if(template == null) {
			return Response.ok(MessageResult.error("Template não encontrado")).build();
		}
		TemplateVO vo = new TemplateVO(template);
		return Response.ok().entity(vo).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			
			if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.ENVIAR_EMAILS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			TemplateEmail template = templateEmailService.get(id);
			if(template == null) {
				return Response.ok(MessageResult.error("Template não encontrado")).build();
			}
			templateEmailService.delete(template);
			return Response.ok().entity(MessageResult.ok("Template deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Template  " + id)).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Template  " + id)).build();
		}
	}
	
	@GET
	@Path("/preview/{id}")
	public Response preview(@PathParam("id") Long id) {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.ENVIAR_EMAILS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		String html = "";
		TemplateEmail template = templateEmailService.get(id);
		
		if(template == null) {
			return Response.ok(MessageResult.error("Template não encontrado")).build();
		}

		html = template.getHtml();
		html = html.replace("\n", "").replace("\r", "").replace(System.lineSeparator(), "");
		HashMap<String,String> map = new HashMap<>();
		map.put("html", html);
		return Response.ok().entity(map).build();
	}
	
}
