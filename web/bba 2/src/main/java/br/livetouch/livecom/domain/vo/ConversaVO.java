package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;

public class ConversaVO {

	public Long id;
	private Long conversaId;
	
	private Long fromDateOnlineChat;
	private Long fromId;
	private String from;
	private String fromNome;
	private String fromUrlFoto;
	private String fromUrlFotoThumb;
	private boolean fromChatOn;
	
	private Long toId;
	private Long toDateOnlineChat;
	private String to;
	private String toNome;
	private String toUrlFoto;
	private String toUrlFotoThumb;
	private boolean toChatOn;
	
	private String dataUpdated;
	private String dataUpdatedString;
	private Long postId;
	private Long grupoId;
	private String nomeGrupo;
	private FileVO fotoGrupo;
	private LocationVO loc;
	private Long identifier;
	private boolean isAdmin;
	private String dataString;

	private boolean isLida;
	private boolean isEntregue;
	
	private String dataLida;
	private String dataLidaString;
	private String dataEntregue;
	private String dataEntregueString;
	
	private String lastMsg;

	private List<MensagemVO> mensagens;
	private BadgeChatVO badge;
	private boolean isGroup;
	private int grupoQtdeUsers;
	private NotificationChatVO notifications;
	private Boolean fromChatAway;
	private Boolean toChatAway;
	
	public ConversaVO(MensagemConversa mc) {
		this.conversaId = mc.getId();
		this.dataUpdated = mc.getDataUpdatedString();
		this.dataString = mc.getDataString();
	
		
		Mensagem msg = mc.getLastMensagem();
		if(msg != null) {
			this.lastMsg = msg.getMsg();
			this.id = msg.getId();
			if(msg.getArquivos() != null && msg.getArquivos().size() > 0) {
				this.lastMsg = "Enviou um Arquivo";
			}
			
			Grupo grupo = mc.getGrupo();
			if(msg.getFrom() != null) {
				this.fromNome = msg.getFrom() != null ? msg.getFrom().getNome() : "";
			} else if (grupo != null) {
				this.fromNome = grupo != null ? grupo.getNome() : "";
			} else {
				this.fromNome = "";
			}
			
			//to
			if(msg.getTo() != null) {
				this.toNome = msg.getTo() != null ? "[Usuário] " + msg.getTo().getNome() : "";
			} else if (grupo != null) {
				this.toNome = grupo != null ? "[Grupo] " + grupo.getNome() : "";
			} else {
				this.toNome = "";
			}
			
			if(msg != null) {
				dataLidaString = msg.getDataLidaString();
				dataEntregueString = msg.getDataEntregueString();
				isLida = msg.isLida();
				isEntregue = msg.isEntregue();
			}
			
			if(grupo != null) {
				nomeGrupo = grupo.getNome();
				grupoId = grupo.getId();
				
				Set<GrupoUsuarios> users = grupo.getUsuarios();
				if(users != null) {
					this.grupoQtdeUsers = users.size();
				}
			}
			
		}
	}
	
	public ConversaVO() {
		
	}
	
	public void setConversa(Usuario userInfo, MensagemConversa c) {
		List<Mensagem> msgs = new ArrayList<Mensagem>();

		if(c != null) {
			Mensagem msg = c.getLastMensagem();
			if(msg != null) {	
				msgs.add(msg);
			}
		}
		
		setConversa(userInfo, c, msgs);
	}
	
	public void setConversa(Usuario userInfo, MensagemConversa c, List<Mensagem> msgs) {
		if (c != null) {
			this.id = c.getId();
			if (c.getPost() != null) {
				this.postId = c.getPost().getId();
			}
			if (c.getFrom() != null) {
				this.fromId = c.getFrom().getId();
				this.from = c.getFrom().getLogin();
				this.fromNome = c.getFrom().getNome();
				this.fromUrlFoto = c.getFrom().getUrlFoto();
				this.fromUrlFotoThumb = c.getFrom().getUrlThumb();
				this.fromChatOn = Livecom.getInstance().isUsuarioLogadoChat(fromId);
				this.setFromDateOnlineChat(c.getFrom().getDataOnlineChat());
				this.fromChatAway = Livecom.getInstance().isUsuarioAwayChat(fromId);
			}
			Grupo grupo = c.getGrupo();
			this.isGroup = grupo != null;
			if (c.getTo() != null) {
				this.toId = c.getTo().getId();
				this.to = c.getTo().getLogin();
				this.toNome = c.getTo().getNome();
				this.toUrlFoto = c.getTo().getUrlFoto();
				this.toUrlFotoThumb = c.getTo().getUrlThumb();
				this.toChatOn = Livecom.getInstance().isUsuarioLogadoChat(toId);
				this.setToDateOnlineChat(c.getTo().getDataOnlineChat());
				this.toChatAway = Livecom.getInstance().isUsuarioAwayChat(toId);
			}
			
			if (grupo != null) {
				this.grupoId = grupo.getId();				
				this.nomeGrupo = grupo.getNome();
				Arquivo foto = grupo.getFoto();
				if (foto != null) {
					this.fotoGrupo = new FileVO();
					this.fotoGrupo.setArquivo(foto, false);
					this.fotoGrupo.resumir();
				}
				Set<GrupoUsuarios> users = grupo.getUsuarios();
				if(users != null) {
					this.grupoQtdeUsers = users.size();
				}
			}
			
			if(c.isGrupo()) {
				this.isAdmin = c.isAdminGrupo(userInfo);
			}
			
			this.dataUpdated = c.getDataUpdatedString();
			this.dataUpdatedString = c.getDataUpdatedStringChat();
			this.dataString = c.getDataString();
			
			// Mensagens e arquivos
			if (msgs != null && msgs.size() > 0) {
				mensagens = new ArrayList<MensagemVO>();
				for (Mensagem m : msgs) {

					if(m != null){
						boolean excluida = m.isExcluidaFrom(userInfo);				
						if (!excluida) {
							MensagemVO f = new MensagemVO();
							f.setMensagem(m, userInfo);
							mensagens.add(f);
						}
						
					}
				}
			}
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public Long getFromId() {
		return fromId;
	}

	public void setFromId(Long fromId) {
		this.fromId = fromId;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getFromUrlFoto() {
		return fromUrlFoto;
	}

	public void setFromUrlFoto(String fromUrlFoto) {
		this.fromUrlFoto = fromUrlFoto;
	}

	public Long getToId() {
		return toId;
	}

	public void setToId(Long toId) {
		this.toId = toId;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getToUrlFoto() {
		return toUrlFoto;
	}

	public void setToUrlFoto(String toUrlFoto) {
		this.toUrlFoto = toUrlFoto;
	}

	public String getDataUpdated() {
		return dataUpdated;
	}

	public void setDataUpdated(String dataUpdated) {
		this.dataUpdated = dataUpdated;
	}

	public void setMensagens(List<MensagemVO> mensagens) {
		this.mensagens = mensagens;
	}

	public List<MensagemVO> getMensagens() {
		return mensagens;
	}
	

	public Long getConversaId() {
		return conversaId;
	}

	public void setConversaId(Long conversaId) {
		this.conversaId = conversaId;
	}

	public String getDataUpdatedString() {
		return dataUpdatedString;
	}

	public String getFromNome() {
		return fromNome;
	}

	public void setFromNome(String fromNome) {
		this.fromNome = fromNome;
	}

	public String getToNome() {
		return toNome;
	}

	public void setToNome(String toNome) {
		this.toNome = toNome;
	}

	public String getToUrlFotoThumb() {
		return toUrlFotoThumb;
	}

	public void setToUrlFotoThumb(String toUrlFotoThumb) {
		this.toUrlFotoThumb = toUrlFotoThumb;
	}

	public String getFromUrlFotoThumb() {
		return fromUrlFotoThumb;
	}

	public void setFromUrlFotoThumb(String fromUrlFotoThumb) {
		this.fromUrlFotoThumb = fromUrlFotoThumb;
	}
	
	public Long getGrupoId() {
		return grupoId;
	}

	public void setGrupoId(Long grupoId) {
		this.grupoId = grupoId;
	}

	
	
	public String getNomeGrupo() {
		return nomeGrupo;
	}

	public void setNomeGrupo(String nomeGrupo) {
		this.nomeGrupo = nomeGrupo;
	}

	public FileVO getFotoGrupo() {
		return fotoGrupo;
	}

	public void setFotoGrupo(FileVO fotoGrupo) {
		this.fotoGrupo = fotoGrupo;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean admin) {
		this.isAdmin = admin;
	}
	
	public boolean isFromChatOn() {
		return fromChatOn;
	}
	public boolean isToChatOn() {
		return toChatOn;
	}

	public String getLastMsg() {
		return lastMsg;
	}

	public void setLastMsg(String lastMsg) {
		this.lastMsg = lastMsg;
	}

	public void setBadge(BadgeChatVO badge) {
		this.badge = badge;
	}
	
	public BadgeChatVO getBadge() {
		return this.badge;
	}

	public LocationVO getLoc() {
		return loc;
	}

	public void setLoc(LocationVO loc) {
		this.loc = loc;
	}

	public Long getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}
	

	public boolean isLida() {
		return isLida;
	}

	public void setLida(boolean isLida) {
		this.isLida = isLida;
	}

	public String getDataLida() {
		return dataLida;
	}

	public void setDataLida(String dataLida) {
		this.dataLida = dataLida;
	}

	public String getDataLidaString() {
		return dataLidaString;
	}

	public void setDataLidaString(String dataLidaString) {
		this.dataLidaString = dataLidaString;
	}

	public String getDataEntregue() {
		return dataEntregue;
	}

	public void setDataEntregue(String dataEntregue) {
		this.dataEntregue = dataEntregue;
	}

	public String getDataEntregueString() {
		return dataEntregueString;
	}

	public void setDataEntregueString(String dataEntregueString) {
		this.dataEntregueString = dataEntregueString;
	}
	
	public boolean isGroup() {
		return this.isGroup;
	}

	public void setGroup(boolean isGroup) {
		this.isGroup = isGroup;
	}

	public int getGrupoQtdeUsers() {
		return grupoQtdeUsers;
	}

	public void setGrupoQtdeUsers(int grupoQtdeUsers) {
		this.grupoQtdeUsers = grupoQtdeUsers;
	}

	public NotificationChatVO getNotifications() {
		return notifications;
	}

	public void setNotifications(NotificationChatVO notifications) {
		this.notifications = notifications;
	}

	public boolean isEntregue() {
		return isEntregue;
	}

	public void setEntregue(boolean isEntregue) {
		this.isEntregue = isEntregue;
	}

	public String getDataString() {
		return dataString;
	}

	public void setDataString(String dataString) {
		this.dataString = dataString;
	}

	public Long getFromDateOnlineChat() {
		return fromDateOnlineChat;
	}

	public void setFromDateOnlineChat(Long fromDateOnlineChat) {
		this.fromDateOnlineChat = fromDateOnlineChat;
	}

	public Long getToDateOnlineChat() {
		return toDateOnlineChat;
	}

	public void setToDateOnlineChat(Long toDateOnlineChat) {
		this.toDateOnlineChat = toDateOnlineChat;
	}
}
