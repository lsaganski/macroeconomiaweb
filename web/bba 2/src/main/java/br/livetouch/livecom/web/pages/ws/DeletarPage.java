package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.apache.commons.validator.routines.UrlValidator;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.StatusMensagemUsuario;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.ComentarioVO;
import br.livetouch.livecom.files.FileManagerFactory;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class DeletarPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tTipo;
	private TextField tMode;
	public TextField tId;
	private UsuarioLoginField tUser;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tUser = new UsuarioLoginField("user_id", false, usuarioService, getEmpresa()));
		form.add(tId =new TextField("id"));
		form.add(tTipo = new TextField("tipo", "tipo = comentario,post,mensagem,conversa,arquivo"));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		form.add(new Submit("Deletar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			
			String tipo = tTipo.getValue();

			String[] schemes = {"http","https"}; // DEFAULT schemes = "http", "https", "ftp"
			UrlValidator urlValidator = new UrlValidator(schemes);
			String url = tId.getValue();
			if(urlValidator.isValid(url) && "s3".equals(tipo)) {
				FileManagerFactory.getFileManager(getParametrosMap()).delete(url);
				return new MensagemResult("OK", "Arquivo excluído com sucesso do S3.");
			}
			
			List<Long> ids = getListIds("id");
			Usuario user = tUser.getEntity();

			try {
				if(ids != null) {
					for (Long id : ids) {
						if ("comentario".equals(tipo)) {
							if (id != null) {
								Comentario c = comentarioService.get(id);
								if (c != null) {
									comentarioService.delete(getUsuario(), c, false);
									return new MensagemResult("OK", "Comentário excluído com sucesso.");
								}
							}
						} else if ("post".equals(tipo)) {
							if (id != null) {
								Post post = postService.get(id);
								if (post != null) {
									postService.delete(getUsuario(),post, false);

									return new MensagemResult("OK", "Post excluído com sucesso.");
								}
							}
						} else if ("mensagem".equals(tipo)) {
							if (id != null && user != null ) {
								Mensagem mensagem = mensagemService.get(id);
								if (mensagem != null) {
									if (mensagem.getTo().getId() == user.getId()) {
										mensagem.setExcluida_to(1);
									} else if (mensagem.getFrom().getId() == user.getId()) {
										mensagem.setExcluida_from(1);
									}
									mensagemService.saveOrUpdate(mensagem);
									if (mensagem.getExcluida_from() ==1 && mensagem.getExpluida_to() == 1) {
										mensagemService.delete(mensagem);
									}
									return new MensagemResult("OK", "Mensagem excluída com sucesso.");
								}
							}
						} else if ("conversa".equals(tipo)) {
							if (id != null) {
								MensagemConversa c = mensagemService.getMensagemConversa(id);
								if (c != null) {
									
									if(c.getGrupo() != null) {
										return new MensagemResult("NOK", "Ainda não é possivel excluir grupos.");
									}
									
									List<StatusMensagemUsuario> smus = statusMensagemUsuarioService.findAllByConversa(c);
									for (StatusMensagemUsuario smu : smus) {
										statusMensagemUsuarioService.delete(user, smu);
									}
									
									mensagemService.delete(c);

									
									return new MensagemResult("OK", "Conversa excluída com sucesso.");
								}
							}
						} else if ("arquivo".equals(tipo)) {
							if (id != null) {
								Arquivo arquivo = arquivoService.get(id);
								if (arquivo != null) {
									arquivoService.delete(arquivo);

									return new MensagemResult("OK", "Arquivo excluído com sucesso.");
								}
							}
						} else if ("s3".equals(tipo)) {
							if (id != null) {
								Arquivo arquivo = arquivoService.get(id);
								if (arquivo != null) {
									arquivoService.delete(arquivo);

									return new MensagemResult("OK", "Arquivo excluído com sucesso.");
								}
							}
						}
					}
				}
			} catch (DomainException e) {
				logError(e.getMessage(), e);
				return new MensagemResult("NOK", e.getMessage());
			} catch (ConstraintViolationException e) {
				logError(e.getMessage(), e);
				return new MensagemResult("NOK", "Não foi possível excluir este registro. Verifique os relacionamentos.");
			} catch (Exception e) {
				logError(e.getMessage(), e);
				return new MensagemResult("NOK", "Não foi possível excluir este registro.");
			}

		}

		return new MensagemResult("NOK", "Nenhum registro excluído.");

	}

	@Override
	protected void xstream(XStream x) {
		x.alias("comentario", ComentarioVO.class);
		super.xstream(x);
	}
}
