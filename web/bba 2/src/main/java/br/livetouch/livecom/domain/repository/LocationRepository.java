package br.livetouch.livecom.domain.repository;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Location;

@Repository
public interface LocationRepository extends net.livetouch.tiger.ddd.repository.Repository<Location> {

}