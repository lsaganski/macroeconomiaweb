package br.livetouch.livecom.rest.resource;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import br.livetouch.domain.exception.DomainMessageException;
import br.livetouch.livecom.chatAkka.ChatLogMessages;
import br.livetouch.livecom.chatAkka.LivecomChatInterface;
import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.chatAkka.router.ChatVO;
import br.livetouch.livecom.domain.ConversaNotification;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.MensagemService;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.NotificationChatVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.rest.domain.ChatMessageFileVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import br.livetouch.livecom.rest.request.MarkAsReadRequest;
import br.livetouch.livecom.web.pages.ws.MensagemResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/chat")
public class ChatResource extends MainResource { 
	@Autowired
	LivecomChatInterface livecomChatInterface;

	@Autowired
	MensagemService mensagemService;

	@POST
	@Path("/markAs2C")
	public Response markAsRead(MarkAsReadRequest mark) throws DomainMessageException {
		Long fromId = mark.from;
		List<Long> cIds = mark.cIds;
		try {
			if(fromId == null) {
				throw new DomainException("User from is null");
			}
			
			if(cIds == null) {
				throw new DomainException("cIds from is null");
			}
			
			livecomChatInterface.markConversasReceivedMessages(cIds, fromId);
			return javax.ws.rs.core.Response.ok(MessageResult.ok("Conversas atulizadas com sucesso")).build();
		} catch (DomainException e) {
			return javax.ws.rs.core.Response.ok(MessageResult.error(e.getMessage())).build();
		} catch (JSONException e) {
			return javax.ws.rs.core.Response.ok(MessageResult.error("Falha ao atulizar as conversas")).build();
		} 
		
	}

	@GET
	@Path("/nova-conversa")
	public javax.ws.rs.core.Response novaConversa(@QueryParam("userFrom") Long userFrom,
			@QueryParam("userTo") Long userTo) {
		if (userFrom == null || userTo == null) {
			return javax.ws.rs.core.Response.ok(MessageResult.error("Desculpe, ocorreu um erro ao tentar iniciar está conversa")).build();
		}
		Usuario usuarioFrom = usuarioService.get(userFrom);
		Usuario usuarioTo = usuarioService.get(userTo);
		try {
			MensagemConversa conversa = mensagemService.getMensagemConversaByUser(usuarioFrom, usuarioTo, true);
			ConversaVO conversaVO = new ConversaVO();
			conversaVO.setConversa(usuarioFrom, conversa);
			return javax.ws.rs.core.Response.status(Status.OK).entity(conversaVO).build();
		} catch (DomainException e) {
			return javax.ws.rs.core.Response.ok(MessageResult.error("Desculpe, ocorreu um erro ao tentar iniciar está conversa")).build();
		}
	}

	@GET
	@Path("/log")
	public Response log(@QueryParam("index") Integer index) {
		if (index != null) {
			ChatLogMessages instance = ChatLogMessages.getInstance();
			try {
				JSONObject json = instance.getLogAfterIndex(index);
				if(json != null) {
					return Response.ok(json).build();
				}		
			} catch (JSONException e1) {
				
			}
		}
		return Response.ok().build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendMessageFile(ChatMessageFileVO mensagem) {
		try {
			if (mensagem == null) {
				return Response.ok(MessageResult.error("Mensagem não existe ou em branco")).build();
			}
			ChatVO chatVO = new ChatVO();
			chatVO.conversaId = mensagem.conversationID;
			chatVO.from = mensagem.fromUserID;
			chatVO.msg = mensagem.msg;
			chatVO.filesBase64 = mensagem.arquivos;
			chatVO.identifier = mensagem.identifier;
			MensagemVO msg = livecomChatInterface.saveMessage(chatVO);
			List<UsuarioVO> usuarios = livecomChatInterface.findUsersFromConversation(mensagem.conversationID, mensagem.fromUserID, false);
			
			if(usuarios != null) {
				AkkaHelper.tellMessageToUsers(msg, usuarios);
			}
			
			return Response.ok(msg).build();
			
		} catch (Exception e) {
			return Response.ok(MessageResult.error("A mensagem não pode ser enviada")).build();
		}
	}
	
	
	@POST
	@Path("/excluirGrupo")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response excluirGrupoMensagem(@FormParam("id") Long id) {
		try {
			if(id == null) {
				return Response.ok(MensagemResult.erro("Id do grupo não informado")).build();
			}
			
			// TODO CHAT GRUPO - o que fazer ao excluir grupo pelo mobile?
			// Importante limpar msgs, sem apagar grupo.
			
			/*GrupoMensagem g = grupoMensagemService.get(id);
			if(g == null) {
				return Response.ok(MensagemResult.erro("Grupo não localizado " + id)).build();
			}
			grupoMensagemService.delete(getUserInfo(), g);*/
			
			return Response.ok(MensagemResult.ok("Mensagens do grupo excluídas com sucesso.")).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.ok(MessageResult.error("Grupo não pode ser excluido")).build();
		}
	}
	
	@GET
	@Path("/usersOnline/user/{id}")
	/**
	 * http://livecom.livetouchdev.com.br/rest/chat/usersOnline/user/2524
	 */
	public br.livetouch.livecom.web.pages.ws.Response getOnlines(@PathParam("id") long id) {
		Map<Long,Boolean> map = new TreeMap<>();
		List<Long> ids = mensagemService.findUsuariosComQuemTemConversa(id);
		for (Long userId : ids) {
			boolean chatOn = Livecom.getInstance().isUsuarioLogadoChat(userId);
			if(chatOn) {
				map.put(userId, chatOn);
			}
		}

		// Remove o user.
		map.remove(id);

		br.livetouch.livecom.web.pages.ws.Response r = br.livetouch.livecom.web.pages.ws.Response.ok();
		r.chatOnline = map;
		return r;
	}
	
	@POST
	@Path("/notifications")
	public br.livetouch.livecom.web.pages.ws.Response changeNotification(
			@FormParam("conversationId") Long conversationId, @FormParam("userId") Long userId,
			@FormParam("tipo") String tipo, @FormParam("status") Boolean status) {
			
		MensagemConversa m = mensagemService.getMensagemConversa(conversationId);
		if(m == null) {
			return br.livetouch.livecom.web.pages.ws.Response.error("Conversa não encontrada");
		}
		
		Usuario usuario = usuarioService.get(userId);
		if(usuario == null) {
			return br.livetouch.livecom.web.pages.ws.Response.error("Usuário não encontrado");
		}
		ConversaNotification c = conversaNotificationService.findByConversationUser(conversationId, userId);
		if(c == null) {
			c = new ConversaNotification();			
		}
		c.setUsuario(usuario);
		c.setConversa(m);
		
		switch (tipo) {
		case "sound":
			c.setSound(status);
			break;
		case "notification":
			c.setNotification(status);
			break;
		case "push":
			c.setSendPush(status);
			break;
		default:
			return br.livetouch.livecom.web.pages.ws.Response.error("Tipo de notificação não existe. Utilize os tipos: sound, notification, push");
		}

		try {
			conversaNotificationService.saveOrUpdate(c);
			return br.livetouch.livecom.web.pages.ws.Response.ok("Status alterado com sucesso");
		} catch (DomainException e) {
			return br.livetouch.livecom.web.pages.ws.Response.error("Erro ao alterar ou salvar os status da notificação");
		}
	}
	
	@GET
	public br.livetouch.livecom.web.pages.ws.Response getNotifications(@QueryParam("userId") Long userId, @QueryParam("conversationId") Long conversationId) {
		MensagemConversa m = mensagemService.getMensagemConversa(conversationId);
		if(m == null) {
			return br.livetouch.livecom.web.pages.ws.Response.error("Conversa não encontrada");
		}
		
		Usuario usuario = usuarioService.get(userId);
		if(usuario == null) {
			return br.livetouch.livecom.web.pages.ws.Response.error("Usuário não encontrado");
		}
		
		
		ConversaNotification c = conversaNotificationService.findByConversationUser(conversationId, userId);
		NotificationChatVO notificationChatVO = new NotificationChatVO(c);
		br.livetouch.livecom.web.pages.ws.Response r = br.livetouch.livecom.web.pages.ws.Response.ok();
		r.notificationsChat = notificationChatVO;
		return r;
	}
}
