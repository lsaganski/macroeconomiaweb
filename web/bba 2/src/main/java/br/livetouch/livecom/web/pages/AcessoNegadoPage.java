package br.livetouch.livecom.web.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.admin.LivecomPage;

@Controller
@Scope("prototype")
public class AcessoNegadoPage extends LivecomPage {

}
