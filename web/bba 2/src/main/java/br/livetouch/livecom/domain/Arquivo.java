package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.utils.FileExtensionUtils;
import br.livetouch.livecom.utils.UploadHelper;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Arquivo extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "ARQUIVO_SEQ")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;

	@Column(length=255)
	private String nome;
	
	// tipo no mobile, audio, video, etc
	private String tipo;

	// dimensões thumb, crop, original
	private String dimensao;
	
	@Column(length=1000)
	private String descricao;
	
	private Date dataCreate;
	private Date dataUpdate;
	
	private String mimeType;
	private long length;
	private String metadata;
	
	private String url;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario usuario;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categoria_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private CategoriaPost categoria;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id", nullable = true)
	private Post post;

	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.REMOVE)
	@JoinTable(name = "arquivos_tags", joinColumns = { @JoinColumn(name = "arquivo_id") }, inverseJoinColumns = { @JoinColumn(name = "tag_id") })
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Tag> tags;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "comentario_id", nullable = true)
	private Comentario comentario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mensagem_id", nullable = true)
	private Mensagem mensagem;

	private String urlThumb;
	
	@OneToMany(mappedBy = "arquivo", fetch = FetchType.LAZY)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	@OrderBy("id asc")
	private Set<ArquivoThumb> thumbs;
	
	@ManyToOne
	@JoinColumn(name = "fk_arquivo_ref", nullable = true)
	private Arquivo arquivoRef;
	
	private int width;
	private int height;

	private int ordem;

	private Boolean destaque;
	
	// 245, 584, 960
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataCreate() {
		return dataCreate;
	}

	public void setDataCreate(Date dataCreate) {
		this.dataCreate = dataCreate;
	}

	public Date getDataUpdate() {
		return dataUpdate;
	}

	public void setDataUpdate(Date dataUpdate) {
		this.dataUpdate = dataUpdate;
	}

	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Usuario getUsuario() {
		if(usuario == null && arquivoRef != null) {
			return arquivoRef.getUsuario();
		}
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public Comentario getComentario() {
		if(arquivoRef != null && comentario == null) {
			return arquivoRef.getComentario();
		}
		return comentario;
	}

	public void setComentario(Comentario comentario) {
		this.comentario = comentario;
	}

	public Mensagem getMensagem() {
		if(mensagem == null && arquivoRef != null) {
			return arquivoRef.getMensagem();
		}
		return mensagem;
	}

	public void setMensagem(Mensagem mensagemInbox) {
		this.mensagem = mensagemInbox;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getExtensao() {
		String ext = FileExtensionUtils.getExtensao(nome);
		return ext;
	}

	public void setUrlThumb(String urlThumb) {
		this.urlThumb = urlThumb;
	}
	public String getUrlThumb() {
		if(StringUtils.isEmpty(urlThumb)) {
			return url;
		}
		return urlThumb;
	}
	
	@Deprecated
	public String getUrlThumbMural() {
		if(thumbs != null) {
			for (ArquivoThumb t : thumbs) {
				if(t.isThumbMural() || t.hasSize(UploadHelper._584)) {
					return t.getUrl();
				}
			}
		}
		return url;
	}
	
	public boolean isImagem() {
		return FileExtensionUtils.isImage(nome);
		
	}
	
	public Set<Tag> getTags() {
		if(tags == null && arquivoRef != null) {
			return arquivoRef.getTags();
		}
		return tags;
	}
	
	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}
	
	public Empresa getEmpresa() {
		if(empresa == null && arquivoRef != null) {
			return arquivoRef.getEmpresa();
		}
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Set<ArquivoThumb> getThumbs() {
		if(thumbs == null && arquivoRef != null) {
			return arquivoRef.getThumbs();
		}
		return thumbs;
	}
	
	public void setThumbs(Set<ArquivoThumb> thumbs) {
		this.thumbs = thumbs;
	}

	@Override
	public String toString() {
		return "Arquivo [id=" + id + ", nome=" + nome + ", url=" + url + "]";
	}
	
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}

	public List<ThumbVO> getThumbsVO() {
		List<ThumbVO> l=  new ArrayList<ThumbVO>();
		Set<ArquivoThumb> at = thumbs;
		
		if(at == null && arquivoRef != null) {
			at = arquivoRef.getThumbs();
		}
		
		if (at != null) {
			try {
				for (ArquivoThumb t : at) {
					ThumbVO vo = new ThumbVO();
					vo.setArquivoThumb(t);
					l.add(vo);
				}
			} catch (Exception e) {
				// FIXME Hibernate: erro lazy
				e.printStackTrace();
			}
		}
		return l;
	}
	
	public String toStringDesc() {
		return getId()+":"+getNome();
	}

	public Arquivo getArquivoRef() {
		return arquivoRef;
	}

	public void setArquivoRef(Arquivo arquivoRef) {
		this.arquivoRef = arquivoRef;
	}

	public void copyArquivo(Arquivo a) {
		this.setArquivoRef(a);
		this.setDataCreate(new Date());
		this.setDataUpdate(new Date());
		this.setHeight(a.getHeight());
		this.setLength(a.getLength());
		this.setMetadata(a.getMetadata());
		this.setMimeType(a.getMimeType());
		this.setNome(a.getNome());
		this.setTipo(a.getTipo());
		this.setDimensao(a.getDimensao());
		this.setUrl(a.getUrl());
		this.setUrlThumb(a.getUrlThumb());
		this.setWidth(a.getWidth());
		this.setDescricao(a.getDescricao());
	}
	
	public static List<Long> getIds(Collection<? extends net.livetouch.tiger.ddd.Entity> list) {
		List<Long> ids = new ArrayList<Long>();
		if(list == null || list.size() == 0) {
			return ids;
		}
		for (net.livetouch.tiger.ddd.Entity e : list) {
			ids.add(e.getId());
		}
		return ids;
	}

	public String getDimensao() {
		return dimensao;
	}

	public void setDimensao(String dimensao) {
		this.dimensao = dimensao;
	}

	public void setDestaque(Boolean destaque) {
		this.destaque = destaque;
	}
	
	public Boolean destaque() {
		return destaque;
	}

	public boolean isDestaque() {
		return destaque != null ? destaque : false;
	}

	public CategoriaPost getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaPost categoria) {
		this.categoria = categoria;
	}

	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	public String getHumanReadableLength() {
		return Utils.sizeToHumanReadable(length);
	}
}
