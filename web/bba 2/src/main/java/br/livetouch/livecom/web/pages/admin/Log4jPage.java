package br.livetouch.livecom.web.pages.admin;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.extras.util.ExceptionUtil;
import net.sf.click.control.Form;
import net.sf.click.control.Label;
import net.sf.click.control.Option;
import net.sf.click.control.Select;
import net.sf.click.control.Submit;


@Controller
@Scope("prototype")
public class Log4jPage extends LivecomAdminPage {

	public String msg;
	public Form form = new Form();
	public Label t1;
	private Select combo;
	public String now;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissoes(true, ParamsPermissao.VIZUALIZAR_RELATORIOS, ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();

		try {
			combo = new Select("appenders");
			List<String> loggers = br.infra.util.Log.getLoggers();
			for (String logger : loggers) {
				combo.add(new Option(logger, logger));
			}
			combo.setWidth("300px;");
			combo.setAttribute("onChange", "document.form.submit()");
			combo.bindRequestValue();
			if(StringUtils.isEmpty(combo.getValue())) {
				String value = (String) getContext().getSessionAttribute("value");
				combo.setValue(value);
			}
			form.add(combo);

			form.add(t1 = new Label("t1", "Logs"));

			form.add(new Submit("stop", this, "stop"));
//			form.add(new Submit("clear", this, "clear"));
			form.add(new Submit("start", this, "start"));

			t1.setReadonly(true);

			if(autoRefresh == -1) {
				start();
			}
		} catch (Exception e) {
			Log.error(e.getMessage(), e);
			t1.setValue(ExceptionUtil.getStackTrace(e));
		}
		
	}
	
	public boolean stop() {
		autoRefresh = 0;
		return false;
	}
	
	public boolean start() {
		autoRefresh = 5;
		return false;
	}
	
	public boolean clear() {
		br.infra.util.Log.clearBuffer();
		return false;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void onRender() {
		super.onRender();
		
		try {
			now = DateUtils.toString(new Date(),"dd/MM/yyyy HH:mm:ss");
			
			String value = combo.getValue();
			if(StringUtils.isEmpty(value)) {
				List optionList = combo.getOptionList();
				if(optionList != null && optionList.size() > 0) {
					Option op = (Option) optionList.get(0);
					value = op.getValue();
				}
			}
			
			if(StringUtils.isNotEmpty(value)) {
				getContext().setSessionAttribute("value", value);
				
				String logs = br.infra.util.Log.getLogs(value);
				if(logs != null && logs.length() > 25000) {
					logs = logs.substring(logs.length() - 25000, logs.length());
				}
					
				String msg= logs  +"";

				t1.setLabel("<pre>"+msg+"</pre>");
			}
			
		} catch (Exception e) {
			logError(e.getMessage(),e);
			t1.setValue(ExceptionUtil.getStackTrace(e));
		}
	}
}
