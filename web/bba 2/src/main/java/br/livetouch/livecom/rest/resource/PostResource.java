package br.livetouch.livecom.rest.resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostDestaque;
import br.livetouch.livecom.domain.PostUsuarios;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.Visibilidade;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.PerfilService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.vo.PostInfoVO;
import br.livetouch.livecom.domain.vo.PostUsuarioVO;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/post")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class PostResource extends MainResource{

	protected static final Logger log = Log.getLogger(PostResource.class);

	@Context
	private SecurityContext securityContext;

	@Autowired
	protected PostService postService;

	@Autowired
	PerfilService permissaoService;

	@Autowired
	protected GrupoService grupoService;

	@GET
	@Path("/titulo")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public List<PostVO> getPosts(@QueryParam("titulo") String titulo, @QueryParam("max") int max) {
		List<Post> list = postService.findAllByTituloLikeWithMax(titulo, max, getEmpresa());
		List<PostVO> vos = new ArrayList<PostVO>();
		for (Post post : list) {
			PostVO e = postService.setPost(post, getUserInfo());
			vos.add(e);
		}
		return vos;
	}
	
	@POST
	public Response create(PostInfoVO info) throws DomainException, IOException {
		
		Usuario user = getUsuarioRequest();
		if (user == null) {
			return Response.ok(MessageResult.error("Usuario nao encontrado")).build();
		}

		Post p = info.post;

		Date publicacao = p.getDataPublicacao();
		
		if (p.getVisibilidade() == null) {
			p.setVisibilidade(Visibilidade.GRUPOS);
		}
		
		if(publicacao != null) {
			p.setDataPublicacao(publicacao);
		}

		List<CategoriaPost> categorias = p.getCategorias();
		if (categorias == null) {
			
			categorias = new ArrayList<CategoriaPost>();
			CategoriaPost categoria = null;
			
			String codigo = info.codigoCategoria;
			if(StringUtils.isNotEmpty(codigo)) {
				categoria = categoriaPostService.findByCodigo(getEmpresa(), codigo);
				
				if(categoria == null) {
					return Response.ok(MessageResult.error("Favor selecione uma categoria para este post.")).build();
				}
			}

			if (categoria == null) {
				categoria = categoriaPostService.findAllDefault(getEmpresa()).get(0);
			}
			
			if (categoria == null) {
				return Response.ok(MessageResult.error("Favor selecione uma categoria para este post.")).build();
			}
			
			categorias.add(categoria);
		} else {
			LinkedList<CategoriaPost> categs = new LinkedList<CategoriaPost>();
			for (CategoriaPost categoria : categorias) {
				categoria = categoriaPostService.get(categoria.getId());
				categs.add(categoria);
			}
			p.setCategorias(categs);
		}
		
		// Destaque
		PostDestaque destaque = p.getPostDestaque();
		if (destaque == null) {
			destaque = new PostDestaque();
		}
		destaque.setUrlSite(p.getUrlSite());
		destaque.setUrlImagem(p.getUrlImagem());
		destaque.setDescricaoUrl(info.destaqueDescricaoUrl);
		destaque.setTituloUrl(info.destaqueTituloUrl);
		destaque.setTipo(info.destaqueTipo);
		
		info.destaque = destaque;
		
		//Rascunho
		boolean isRascunho = p.isRascunho();
		if(p.isRascunho()) {
			p.setDataPublicacao(new Date());
		}
		
		// Post
		p = postService.post(p, user, info, isRascunho);
		
		PostVO vo = postService.setPost(p, user);
		
		Response r = Response.ok(vo).build();
		return r;
		
	}
	
	@POST
	@Path("/notification")
	public Response options(@FormParam("post") Long postId, @FormParam("user") Long userId, @FormParam("status") boolean status) throws DomainException {
		
		try {
			if(postId == null || userId == null) {
				log.debug("Não foi possivel alterar as preferencias deste post");
				return Response.ok(MessageResult.error("Não foi possivel alterar as preferencias deste post")).build();
			}
			
			Usuario usuario = usuarioService.get(userId);
			Post post = postService.get(postId);
			
			if(post == null || usuario == null) {
				log.debug("Não foi possivel alterar as preferencias deste post");
				return Response.ok(MessageResult.error("Não foi possivel alterar as preferencias deste post")).build();
			}
			
			PostUsuarios postUsuario = postService.getPostUsuarios(usuario, post);
			if(postUsuario == null) {
				postUsuario = new PostUsuarios();
				postUsuario.setPost(post);
				postUsuario.setUsuario(usuario);
				postUsuario.setPush(true);
				postUsuario.setNotification(true);
			}
			
			postUsuario.setPush(status);
			postUsuario.setNotification(status);
			
			postService.saveOrUpdate(postUsuario);
			PostUsuarioVO pu = new PostUsuarioVO(postUsuario);
			
			if(status) {
				log.debug("Notificações do Post ativadas com sucesso.");
				return Response.ok(MessageResult.ok("Notificações do Post ativadas com sucesso.", pu)).build();
			}
			
			log.debug("Post silenciado com sucesso.");
			return Response.ok(MessageResult.ok("Post silenciado com sucesso.", pu)).build();
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar as preferencias do post")).build();
		}
	}

}
