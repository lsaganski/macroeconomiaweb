package br.livetouch.livecom.web.pages.admin;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.livetouch.click.control.link.ActionDeleteLink;
import net.livetouch.click.table.TablePagination;
import net.sf.click.Context;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Decorator;
import net.sf.click.extras.control.LinkDecorator;

@Controller
@Scope("prototype")
public class UsuariosLogadosV2Page extends LivecomAdminPage {
	public TablePagination table = new TablePagination();

	public ActionLink detalhes = new ActionLink("detalhes", getMessage("detalhes.label"), this,"detalhes");
	public ActionDeleteLink logout = new ActionDeleteLink("logout", getMessage("logout.label"), this,"logout");

	public Long id;

	public UserInfoVO user;
	
	public String value;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}
	
	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
		
		if(clear) {
			getContext().removeSessionAttribute(Usuario.SESSION_FILTRO_KEY);
		}
		
		table();

		logout.setConfirmMessage(getMessage("msg.deslogar.usuario.confirm"));

//		autoRefresh = 60;
	}

	public void table() {
		Column c = new Column("login", getMessage("coluna.login.label"));
		table.addColumn(c);

//		c = new Column("nome");
//		c.setAttribute("align", "center");
//		table.addColumn(c);
		
		c = new Column("tipoLogin", getMessage("tipo.login.label"));
		table.addColumn(c);
		
		c = new Column("chat", "Chat On");
		c.setDecorator(new Decorator() {
			@Override
			public String render(Object object, Context context) {
				UserInfoVO user = (UserInfoVO) object;
				boolean chatOn = Livecom.getInstance().isUsuarioLogadoChat(user.id);
				String imgSrc = "<img src='"+context.getRequest().getContextPath()+"/img/ok.png' width='12px' height='12px' >";
				imgSrc = chatOn ? imgSrc : "";
				return imgSrc;
//				return chatOn ? "on" : ""; 
			}
		});
		table.addColumn(c);
		
		c = new Column("dataLoginString", getMessage("data.login.label"));
		table.addColumn(c);
		
		c = new Column("dataUltTransacaoString", getMessage("ultimo.acesso.label"));
		table.addColumn(c);

		c = new Column("tempo (min)", getMessage("tempo.min.label"));
		c.setDecorator(new Decorator() {
			@Override
			public String render(Object object, Context context) {
				UserInfoVO user = (UserInfoVO) object;
				long seg = user.getTempoLogadoMinutos();
				String minutos = String.valueOf(seg);
				return minutos;
			}
		});
		table.addColumn(c);

		c = new Column("Ação", getMessage("acao.label"));
		c.setSortable(false);
		ActionLink[] links = new ActionLink[] { detalhes,logout };
		c.setDecorator(new LinkDecorator(table, links, "login"));
		c.setTextAlign("center");
		table.addColumn(c);
	}
	
	public boolean detalhes() {
		try {
			String login = detalhes.getValue();
			user = Livecom.getInstance().getUserInfo(login);
			
		} catch (Exception e) {
			this.msg = e.getMessage();
		}
		return true;
	}

	public boolean logout() {
		try {
			String login = detalhes.getValue();
			if(login == null) {
				login = value;
			}
			user = Livecom.getInstance().getUserInfo(login);

			Livecom.getInstance().removeUserInfo(user);

			setFlashAttribute("msg", getMessage("msg.usuario.deslogado.success"));

			refresh();

		} catch (Exception e) {
			this.msg = e.getMessage();
		}
		return true;
	}
	
	@Override
	public void onRender() {
		super.onRender();
		
		// Usuários logados
		Empresa empresa = getEmpresa();
		
		List<UserInfoVO> users = Livecom.getInstance().getUsuarios(empresa);
		
		Collections.sort(users,new Comparator<UserInfoVO>() {
			@Override
			public int compare(UserInfoVO o1, UserInfoVO o2) {
				boolean b1 = o1 != null && o2 != null && o1.getDataUltTransacao() != null && o2.getDataUltTransacao() != null;
				boolean b2 = o1 != null && o2 != null && o1.getDataLogin() != null && o2.getDataLogin() != null;
				int compareTo = 0;
				if(b1) {
					compareTo = o2.getDataUltTransacao().compareTo(o1.getDataUltTransacao());
				} else if(b2) {
					compareTo = o2.getDataLogin().compareTo(o1.getDataLogin());
				}
				return compareTo;
			}
		});
		
		table.setRowList(users);
	}
}
