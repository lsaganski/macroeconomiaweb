package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Idioma;

public class IdiomaVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public Long id;
	public String nome;
	public String codigo;
	public String icone;
	
	public IdiomaVO() {
		
	}

	public IdiomaVO(Idioma idioma) {
		this.id = idioma.getId();
		this.nome = idioma.getNome();
		this.codigo = idioma.getCodigo();
		this.icone = idioma.getIcone();
	}
	
	public static List<IdiomaVO> fromList(List<Idioma> idiomas) {
		List<IdiomaVO> vos = new ArrayList<>();
		if(idiomas == null) {
			return vos;
		}
		for (Idioma i : idiomas) {
			vos.add(new IdiomaVO(i));
		}
		return vos;
	}

}
