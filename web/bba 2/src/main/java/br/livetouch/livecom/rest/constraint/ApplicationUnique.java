package br.livetouch.livecom.rest.constraint;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.springframework.beans.factory.annotation.Autowired;

import br.livetouch.livecom.domain.Application;
import br.livetouch.livecom.domain.service.ApplicationService;

@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ApplicationUnique.ApplicationUniqueValidator.class})
public @interface ApplicationUnique {
	
	String message() default "{app.exists}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
	
	public class ApplicationUniqueValidator implements ConstraintValidator<ApplicationUnique, Application>  {
		
		@Autowired
		ApplicationService applicationService;
		
        @Override
        public void initialize(final ApplicationUnique applicationUnique) {
        }
        @Override
        public boolean isValid(final Application app, final ConstraintValidatorContext constraintValidatorContext) {
            return  !applicationService.exists(app);
        }
    }
	
}
