package br.livetouch.livecom.web.pages.admin;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.vo.RelatorioAcessosVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Option;
import net.sf.click.control.Select;
import net.sf.click.control.Submit;
import net.sf.click.control.Table;
import net.sf.click.control.TextArea;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.DateField;
import net.sf.click.extras.control.IntegerField;

/**
 * Logs das auditorias.
 * 
 */
@Controller
@Scope("prototype")
public class NumeroDeAcessosPage extends RelatorioPage {

	private RelatorioFiltro filtro;
	public PaginacaoTable table = new PaginacaoTable();
	public Table tableNaoExpandido = new Table();
	public int page;
	private IntegerField tMax;
	public Form form = new Form();
	public TextArea textarea = new TextArea();
	public Select expandir = new Select();
	public DateField tDataInicio = new DateField("dataInicial", getMessage("dataInicio.label"));
	public DateField tDataFim = new DateField("dataFinal", getMessage("dataFim.label"));
	public String dataInicio;
	public String dataFim;
	public TextField tUsuariosLogados;
	public TextField tUsuariosLogadosNaoRepetidos;

	@Override
	public void onInit() {
		super.onInit();
		
		bootstrap_on = true;
		escondeChat = true;

		if (clear) {
			getContext().removeSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		}

		table();

		tableNaoExpandido();

		form();
	}

	public void form() {

		tDataInicio.setAttribute("class", "input data datepicker");
		tDataFim.setAttribute("class", "input data datepicker");
		tDataInicio.setFormatPattern("dd/MM/yyyy");
		tDataFim.setFormatPattern("dd/MM/yyyy");

		tDataInicio.setValue(DateUtils.toString(DateUtils.addDia(new Date(),-7), "dd/MM/yyyy"));
		tDataFim.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));

		tDataInicio.setMaxLength(10);
		tDataFim.setMaxLength(10);

		form.add(tDataInicio);
		form.add(tDataFim);

		expandir.add(new Option("consolidado", "Consolidado"));
		expandir.add(new Option("expandido", "Expandido"));
		form.add(expandir);

		tUsuariosLogados = new TextField("usuarioLogado", getMessage("usuarios.logados.label"));
		tUsuariosLogados.setAttribute("class", "input menor");
		tUsuariosLogados.setReadonly(true);
		form.add(tUsuariosLogados);

		tUsuariosLogadosNaoRepetidos = new TextField("usuarioNaoRepetido", getMessage("usuarios.distinct.logados.label"));
		tUsuariosLogadosNaoRepetidos.setAttribute("class", "input menor");
		tUsuariosLogadosNaoRepetidos.setReadonly(true);
		form.add(tUsuariosLogadosNaoRepetidos);

		Submit tfiltrar = new Submit("filtrar", getMessage("filtrar.label"), this, "filtrar");
		tfiltrar.setAttribute("class", "btn btn-primary min-width");
		form.add(tfiltrar);

		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "btn btn-outline blue min-width");
		form.add(tExportar);

		tMax = new IntegerField("max");
		tMax.setAttribute("class", "input");
		form.add(tMax);

		this.textarea.setCols(100);
		this.textarea.setRows(4);
		this.textarea.setReadonly(true);
	}

	private void table() {
		Column c = new Column("date", getMessage("data.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("logados", getMessage("usuarios.logados.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("distinctLogados", getMessage("usuarios.distinct.logados.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);
	}

	private void tableNaoExpandido() {
		Column c = new Column("logados", getMessage("usuarios.logados.label"));
		c.setAttribute("align", "left");
		tableNaoExpandido.addColumn(c);

		c = new Column("distinctLogados", getMessage("usuarios.distinct.logados.label"));
		c.setAttribute("align", "left");
		tableNaoExpandido.addColumn(c);

	}

	@Override
	public void onGet() {
		super.onGet();

		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro != null) {
			form.copyFrom(filtro);

			if (expandir.getValue().equals("expandido")) {
				filtro.setExpandir(true);
			} else {
				filtro.setExpandir(false);
				expandir.setValue("expandido");
			}
			filtro.setPage(page);
		}
	}

	public boolean filtrarResumido() {
		List<RelatorioAcessosVO> acessos;

		try {
			acessos = usuarioService.findbyFilter(filtro, true, getEmpresa());
			// String result = getMessage("usuarios.logados.label") + " = ";
			tUsuariosLogados.setValue(String.valueOf(acessos.get(0).getLogados()));
			// result += String.valueOf(acessos.get(0).getLogados());
			tUsuariosLogadosNaoRepetidos.setValue(String.valueOf(acessos.get(0).getDistinctLogados()));
			// result += "\n\n" + getMessage("usuarios.distinct.logados.label")
			// + " = ";
			// result += String.valueOf(acessos.get(0).getDistinctLogados());

			// this.textarea.setValue(result);
			tableNaoExpandido.setRowList(acessos);
		} catch (Exception e) {
			this.msg = e.getMessage();
		}
		return true;
	}

	public boolean filtrar() {
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new RelatorioFiltro();
			getContext().setSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);

		if (!expandir.getValue().equals("expandido")) {
			filtrarResumido();
			return true;
		}

		return true;
	}

	public boolean exportar() throws DomainException {
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new RelatorioFiltro();
			getContext().setSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);
		filtro.setExpandir(true);

		String csv = logService.csvAcessos(filtro, getEmpresa());
		download(csv, "relatorio-acessos.csv");

		return true;
	}

	@Override
	public String getContentType() {
		return super.getContentType();
	}

	@Override
	public String getTemplate() {
		return super.getTemplate();
	}

	@Override
	public void onRender() {
		super.onRender();

		if (filtro == null) {
			filtro = new RelatorioFiltro();
			form.copyTo(filtro);
		}

		Integer max = tMax.getInteger();
		if (max == null) {
			max = 20;
			filtro.setMax(max);
		}
		filtrarResumido();
		List<RelatorioAcessosVO> acessos;
		try {
			acessos = usuarioService.findbyFilter(filtro, false, getEmpresa());
			table.setPageSize(max);
			long acessosCount = usuarioService.getCountByFilter(filtro, false, getEmpresa());
			table.setCount(acessosCount);
		} catch (DomainException e) {
			form.setError(e.getMessage());
			return;
		}

		table.setRowList(acessos);

	}
}
