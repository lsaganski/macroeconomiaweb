package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Card;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.Location;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.StatusMensagemUsuario;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.MensagemConversaRepository;
import br.livetouch.livecom.domain.repository.MensagemRepository;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.CardService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.LocationService;
import br.livetouch.livecom.domain.service.MensagemService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.StatusMensagemUsuarioService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.RelatorioConversaFiltro;
import br.livetouch.livecom.domain.vo.StatusMensagemUsuarioVO;
import br.livetouch.livecom.domain.vo.UpdateStatusMensagemResponseVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class MensagemServiceImpl extends LivecomService<Mensagem> implements MensagemService {
	protected static final Logger log = Log.getLogger(MensagemService.class);

	@Autowired
	private MensagemRepository rep;

	@Autowired
	private MensagemConversaRepository repConversa;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private ArquivoService arquivoService;
	
	@Autowired
	protected NotificationService notificationService;

	@Autowired
	private GrupoService grupoService;

	@Autowired
	private StatusMensagemUsuarioService statusMensagemUsuarioService;

	@Autowired
	private LocationService locationService;
	
	@Autowired private CardService cardService;

	@Override
	public Mensagem get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Mensagem msg) throws DomainException {
		try {
			if(msg == null) {
				System.err.println("erro msg is null");
				return;
			}
			if(rep == null) {
				System.err.println("erro rep is null");
			}
			rep.saveOrUpdate(msg);
			MensagemConversa c = msg.getConversa();
			if (c != null) {
				c.setLastMensagem(msg);
				repConversa.saveOrUpdate(c);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public List<Mensagem> findAll() {
		return rep.findAll();
	}

	@Override
	public List<MensagemConversa> findAllConversas() {
		return rep.findAllConversas();
	}

	@Override
	public void delete(Mensagem msg) throws DomainException {
//		if (userInfo != null) {
//			String msg = "Usuário " + userInfo.getNome() + " deletou um comentário " + c.getComentario();
//			LogAuditoria.log(userInfo, c, AuditoriaEntidade.COMENTARIO, AuditoriaAcao.DELETAR, msg);
//		}

		List<Long> ids = new ArrayList<>();
		ids.add(msg.getId());
		deleteMensagens(ids);
/*
		try {
			Set<Arquivo> arquivos = msg.getArquivos();
			for (Arquivo a : arquivos) {
				a.setPost(null);
				a.setMensagem(null);
				arquivoService.saveOrUpdate(a);

				// Deleta da amazon
				arquivoService.delete(null,a, true);
			}

			execute("update Mensagem set location = null where id = ? ", msg.getId());
			execute("delete from Location l where l.mensagem.id = ? ", msg.getId());

			execute("update MensagemConversa c set c.lastMensagem = null where c.lastMensagem.id = ? ", msg.getId());

			execute("delete from StatusMensagemUsuario where mensagem.id = ? ", msg.getId());

			notificationService.deleteBy(msg);

			rep.delete(msg);
		} catch (Exception e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if(root == null) {
				root = e;
			}
			log.error("Erro ao excluir a msg [" + msg.toStringDesc() + "]: " + root.getMessage(), root);
			
			throw new DomainException("Não foi possível excluir a mensagem [" + msg.toStringDesc() + "]");
		}
*/
	}

	@Override
	public void deleteMensagens(List<Long> ids) throws DomainException {

		if (ids == null || ids.isEmpty()) {
			return;
		}

		Mensagem c = null;

		try {
			// Arquivos
			List<Long> idsArquivos = queryIdsIn("select id from Arquivo where mensagem.id in (:ids)",false,"ids",ids);
			arquivoService.delete(null, idsArquivos, false);
			
			// Notifications
			notificationService.deleteBy(Mensagem.class, ids);

			List<Long> cardIds =  queryIdsIn("select id from Card c where c.mensagem.id in(:ids)", false, "ids", ids);

			// Mensagens
//			executeIn("update Mensagem set location = null where id in (:ids)","ids",ids);
            executeIn("delete from CardButton c where c.mensagem.id in (:ids)","ids",ids);
            executeIn("delete from CardButton c where c.card.id in (:ids)","ids",cardIds);
            executeIn("delete from Card c where c.mensagem.id in (:ids)","ids",ids);
			execute("delete from Location l where l.id in (select m.location.id from Mensagem m)");
			executeIn("delete from StatusMensagemUsuario where mensagem.id in (:ids)","ids",ids);
//			executeIn("delete from Location l where l.mensagem.id in (:ids)","ids",ids);

			// Conversas
			executeIn("update MensagemConversa c set c.lastMensagem = null where c.lastMensagem.id in (:ids)","ids",ids);

			// Delete
			executeIn("delete Mensagem where id in (:ids)","ids", ids);
			
		}catch (Exception e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if (root == null) {
				root = e;
			}
			log.error("Erro ao excluir a mensagem [" + c + "]: " + root.getMessage(), root);

			throw new DomainException("Não foi possível excluir a mensagem [" + c + "]", e);
		}
	}
	
	@Override
	public void delete(MensagemConversa c) throws DomainException {
		List<Long> ids = new ArrayList<>();
		if(c != null) {
			ids.add(c.getId());
			deleteConversas(ids);
		}
	}

	@Override
	public void deleteConversas(List<Long> ids) throws DomainException {

		if (ids == null || ids.isEmpty()) {
			return;
		}


		try {
			executeIn("update MensagemConversa c set c.lastMensagem = null where c.id in (:ids)","ids",ids);
			
			executeIn("delete ConversaNotification where conversa.id in (:ids)","ids", ids);
			
			// Mensagens
			List<Long> idsMsgs = queryIdsIn("select id from Mensagem where conversa.id in (:ids)",false,"ids",ids);
			deleteMensagens(idsMsgs);
			
			// Delete
			executeIn("delete MensagemConversa where id in (:ids)","ids", ids);
			
		}catch (Exception e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if (root == null) {
				root = e;
			}
			log.error("Erro ao excluir a conversa: " + root.getMessage(), root);

			throw new DomainException("Não foi possível excluir a conversa", e);
		}
	}

	@Override
	public List<Mensagem> findAllByUser(Usuario userFrom, Usuario userTo, int tipo, int page, int maxRows) {
		return rep.findAllByUser(userFrom, userTo, tipo, page, maxRows);
	}

	@Override
	public void saveOrUpdate(MensagemConversa c) throws DomainException {
		repConversa.saveOrUpdate(c);
	}

	@Override
	public MensagemConversa getMensagemConversa(Long id) {
		return repConversa.get(id);
	}

	@Override
	public List<MensagemConversa> findAllConversasByUser(Usuario user, String search, int page, int maxRows) {
		return rep.findAllConversasByUser(user, search, page, maxRows);
	}

	@Override
	public MensagemConversa getMensagemConversaByUser(Usuario userFrom, Usuario userTo) throws DomainException {
		return getMensagemConversaByUser(userFrom, userTo, true);
	}

	@Override
	public MensagemConversa getMensagemConversaByUser(Usuario userFrom, Usuario userTo, boolean create) throws DomainException {
		MensagemConversa c = rep.getMensagemConversaByUser(userFrom, userTo);
		if (c != null) {
			// Existe
			return c;
		}

		if (create) {
			c = getNewConversa(userFrom, userTo, null);
			saveOrUpdate(c);
			c = getMensagemConversa(c.getId());
			return c;
		}

		return null;
	}
	
	@Override
	public MensagemConversa getMensagemConversaByUserGrupo(Usuario user, Grupo grupo, boolean create) throws DomainException {
		MensagemConversa c = rep.getMensagemConversaByUserGrupo(user, grupo);
		if (c != null) {
			// Existe
			return c;
		}

		if (create) {
			c = getNewConversa(user, null,grupo);
			saveOrUpdate(c);
			c = getMensagemConversa(c.getId());
			return c;
		}

		return null;
	}

	private MensagemConversa getNewConversa(Usuario userFrom, Usuario userTo, Grupo grupo) {
		MensagemConversa c = new MensagemConversa();
		c.setDataCreated(new Date());
		c.setTo(userTo);
		c.setFrom(userFrom);
		c.setGrupo(grupo);
		c.setPost(null);
		c.setDataUpdated(new Date());
		return c;
	}
	

	@Override
	public List<Mensagem> getMensagensByConversa(MensagemConversa conversa, Usuario user, int page, int maxRows, boolean orderAsc) {
		List<Mensagem> mensagens = rep.getMensagensByConversa(conversa, user, page, maxRows, orderAsc);
		if(conversa.isGrupo()) {
			for (Mensagem mensagem : mensagens) {
				mensagem.setLida(mensagem, conversa, user, statusMensagemUsuarioService);
			}
		}
		return mensagens;
	}

	@Override
	public br.livetouch.livecom.domain.Mensagem getMensagem(MensagemConversa conversa, Long id, Usuario userFrom, Usuario userTo, 
			String msg) {
		br.livetouch.livecom.domain.Mensagem m = id != null && id > 0 ? get(new Long(id)) : null;
		if (m == null) {
			m = new br.livetouch.livecom.domain.Mensagem();
			m.setDataCreated(new Date());
		}
		m.setDataUpdated(new Date());
		m.setMsg(msg);
		m.setTo(userTo);
		m.setFrom(userFrom);

		m.setConversa(conversa);

		return m;
	}

	@Override
	public br.livetouch.livecom.domain.MensagemConversa getMensagemConversa(Long id, Usuario userFrom, Usuario userTo, String msg) throws DomainException {
		br.livetouch.livecom.domain.MensagemConversa c = id != null && id > 0 ? getMensagemConversa(new Long(id)) : null;
		if (c == null) {

			// boolean isWhatsApp = "1".equals(whatsapp);
			// if(isWhatsApp) {
			// throw new
			// DomainException("Conversa inválida, selecione o usuário
			// corretamente.");
			// }

			c = new br.livetouch.livecom.domain.MensagemConversa();
			c.setDataCreated(new Date());
			c.setTo(userTo);
			c.setFrom(userFrom);
			c.setPost(null);
		}
		c.setDataUpdated(new Date());
		return c;
	}


	@Override
	public Mensagem saveMensagem(Long msgId, Long conversaId, Long userFromLogin, Long userToLogin, String msg, 
			List<Long> arquivoIds, Location location, Long identifier, Set<Card> cards) throws DomainException {
		log.debug(">> saveMensagem from[" + userFromLogin + "] to [" + userToLogin + "] > " + msg);

		Usuario userFrom = usuarioService.get(userFromLogin);
		if (userFrom == null) {
			throw new DomainException("Uusário [" + userFromLogin + "] não encontrado");
		}

		Usuario userTo = null;
		if(userToLogin != null && userToLogin != 0L) {
			userTo = usuarioService.get(userToLogin);
			if (userTo == null) {
				throw new DomainException("Usuário [" + userTo + "] não encontrado");
			}
		}

		// Conversa
		MensagemConversa msgConversa = getMensagemConversa(conversaId, userFrom, userTo, msg);
		saveOrUpdate(msgConversa);

		// Mensagem
		br.livetouch.livecom.domain.Mensagem mensagem = getMensagem(msgConversa, msgId, userFrom, userTo, msg);

		// identifier (id) no mobile.
		mensagem.setIdentifier(identifier);

		// save
		saveOrUpdate(mensagem);

		if (arquivoIds != null && arquivoIds.size() > 0) {
			// Arquivos
			List<Arquivo> arquivos = arquivoService.findAllByIds(arquivoIds);
			if (arquivos != null && arquivoIds.size() > 0) {
				for (Arquivo arquivo : arquivos) {
					arquivo.setMensagem(mensagem);
					arquivoService.saveOrUpdate(arquivo);
				}
				mensagem.setArquivos(new HashSet<Arquivo>(arquivos));

				// save
				saveOrUpdate(mensagem);
			}
		}
		
		if(cards != null && cards.size() > 0) {
			for (Card card : cards) {
				card.setMensagem(mensagem);
				cardService.saveOrUpdate(card);
			}
			mensagem.setCards(cards);
			saveOrUpdate(mensagem);
		}

		if (location != null) {
			locationService.saveOrUpdate(location);
			mensagem.setLocation(location);
			saveOrUpdate(mensagem);
		}

		return mensagem;
	}

	
	@Override
	public Mensagem saveMensagemGrupo(Long msgId, Long conversaId, Long grupoId, Long userFromLogin, String msg, List<Long> arquivoIds, Location location, Long identifier, Set<Card> cards)
			throws DomainException {
		log.debug(">> postMensagem from[" + userFromLogin + "] to group [" + grupoId + "] > " + msg);

		Usuario userFrom = usuarioService.get(userFromLogin);
		if (userFrom == null) {
			throw new DomainException("Uusário [" + userFromLogin + "] não encontrado");
		}

		// Conversa
		MensagemConversa msgConversa = getMensagemConversa(conversaId, userFrom, null, msg);
		saveOrUpdate(msgConversa);

		// Grupo
		Grupo grupo = msgConversa.getGrupo();
		List<Usuario> usuarios = usuarioService.findUsuariosByGrupo(grupo, -1, -1);

		// Mensagem
		br.livetouch.livecom.domain.Mensagem mensagem = getMensagem(msgConversa, msgId, userFrom, null, msg);
		
		// identifier (id) no mobile.
		mensagem.setIdentifier(identifier);

		// save
		saveOrUpdate(mensagem);

		// Status Mensagem
		for (Usuario u : usuarios) {
			StatusMensagemUsuario statusMensagemUsuario = new StatusMensagemUsuario();
			statusMensagemUsuario.setConversa(msgConversa);
			statusMensagemUsuario.setUsuario(u);
			statusMensagemUsuario.setMensagem(mensagem);

			boolean owner = u.getId().equals(userFrom.getId());
			if (owner) {
				statusMensagemUsuario.setDataEntregue(new Date());
				statusMensagemUsuario.setDataLida(new Date());
				statusMensagemUsuario.setLida(true);
				statusMensagemUsuario.setEntregue(true);
			}

			statusMensagemUsuarioService.saveOrUpdate(u, statusMensagemUsuario);
		}

		if (arquivoIds != null) {
			// Arquivos
			List<Arquivo> arquivos = arquivoService.findAllByIds(arquivoIds);
			if (arquivos != null) {
				for (Arquivo arquivo : arquivos) {
					arquivo.setMensagem(mensagem);
					arquivoService.saveOrUpdate(arquivo);
				}
				mensagem.setArquivos(new HashSet<Arquivo>(arquivos));

				// save
				saveOrUpdate(mensagem);
			}
		}
		
		if(cards != null) {
			for (Card card : cards) {
				card.setMensagem(mensagem);
				cardService.saveOrUpdate(card);
			}
			mensagem.setCards(cards);
			saveOrUpdate(mensagem);
		}

		if (location != null) {
			locationService.saveOrUpdate(location);
			mensagem.setLocation(location);
			saveOrUpdate(mensagem);
		}

		return mensagem;
	}

	@Override
	public List<Long> findUsuariosComQuemTemConversa(long userId) {
		return rep.findUsuariosComQuemTemConversa(userId);
	}

	@Override
	public List<ConversaVO> findConversasByFilter(RelatorioConversaFiltro filtro) throws DomainMessageException, DomainException {
		ArrayList<ConversaVO> list = new ArrayList<>();
		List<MensagemConversa> findConversasByFilter = repConversa.findConversasByFilter(filtro);
		for (MensagemConversa conversa : findConversasByFilter) {
			if(conversa.getLastMensagem() != null) {
				list.add(new ConversaVO(conversa));
			}
		}
		return list;
	}

	@Override
	public long getCountConversasByFilter(RelatorioConversaFiltro filtro) throws DomainException {
		return repConversa.getCountVersaoByFilter(filtro);
	}

	@Override
	public List<MensagemVO> findDetalhesByConversa(RelatorioConversaFiltro filtro) {
		ArrayList<MensagemVO> list = new ArrayList<>();
		List<Mensagem> mensagens = rep.findDetalhesByConversa(filtro);
		for (Mensagem mensagem : mensagens) {
			MensagemVO vo = new MensagemVO();
			vo.setMensagem(mensagem, null);
			list.add(vo);
		}
		return list;
	}

	@Override
	public List<StatusMensagemUsuarioVO> findDetalhesByMensagem(Long id) {
		return rep.findDetalhesByMensagem(id);
	}

	@Override
	public long getCountDetalhesByConversa(RelatorioConversaFiltro filtro) {
		return rep.getCountDetalhesByConversa(filtro);
	}

	/**
	 * status: 1 - recebida, 2 lida
	 * 
	 * @return
	 */
	@Override
	public int markConversationAsRead(Long userId, Long conversaId) throws DomainException {
		MensagemConversa conversa = repConversa.get(conversaId);
		int qtdModified = 0;
		if(conversa != null) {
			boolean isGrupo = conversa.isGrupo();
			if (isGrupo) {
				// FIXME CHAT - ver se precisa juntar esse update com o updateStatusMensagemLida abaixo.
				qtdModified += statusMensagemUsuarioService.updateStatusConversaGrupo(userId, conversaId, 1);
				qtdModified += statusMensagemUsuarioService.updateStatusConversaGrupo(userId, conversaId, 2);
				
				// Passa nulo no usuario, para nao fazer join com status_mensagem
				List<Mensagem> msgs = findMensagensNaoLidas(conversa);
				for (Mensagem msg : msgs) {
					Long msgId = msg.getId();
					updateMensagemEntregueLida(msgId, msg);
				}
				
			} else {
				qtdModified += rep.updateStatusConversa(userId, conversaId, 1);
				qtdModified += rep.updateStatusConversa(userId, conversaId, 2);			
			}
		}
		return qtdModified;
	}
	
	/**
	 * status: 1 - recebida, 2 lida
	 * 
	 * @throws DomainMessageException 
	 */
	@Override
	public UpdateStatusMensagemResponseVO updateStatusMensagemLida(Usuario u, long msgId, int status) throws DomainMessageException {
		if (msgId == 0) {
			throw new DomainMessageException("Informe o id da mensagem.");
		}
		if (status != 0 && status != 1 && status != 2) {
			throw new DomainMessageException("Status inválido.");
		}

		Mensagem msg = rep.get(msgId);

		if(msg != null) {
			boolean isGrupo = msg.isGrupo();
			if(isGrupo) {

				// Atualiza Status Mensagem
				int update = statusMensagemUsuarioService.updateStatusMensagemGrupo(u.getId(), msgId, status);

				// Atualiza Mensagem para ver se todos Status Mensagem foram lidos.
				UpdateStatusMensagemResponseVO s = updateMensagemEntregueLida(msgId, msg);
				if(s != null) {
					// Busca todos usuarios do grupo
					List<UsuarioVO> usuarios = rep.findUsuariosGrupo(msg);

					MensagemVO mensagemVO = new MensagemVO();
					mensagemVO.setMensagem(msg, u);
					s.usuarios = usuarios;
					s.mensagem = mensagemVO;
					s.qtdeModified = update;
				}
				
				return s;
			} else {
				int update = rep.updateStatusMensagem(u.getId(), msgId, status);
				UpdateStatusMensagemResponseVO s = new UpdateStatusMensagemResponseVO();
				s.qtdeModified = update;
				return s;
			}
		} else {
			log.error("Mensagem nao encontrada id: " + msgId);
		}
		
		return null;
	}

	/**
	 * Para mensagens de Grupo, depois de setar o Status Mensagem de cada msg..
	 * Faz a verificacao se todos os Status Mensagem foram entregues/lidos..
	 * Se sim, atualiza o flag entregue/lida na Mensagem.
	 * 
	 * @param msgId
	 * @param msg
	 * @return
	 */
	private UpdateStatusMensagemResponseVO updateMensagemEntregueLida(long msgId, Mensagem msg) {
		UpdateStatusMensagemResponseVO s = rep.countStatusMensagemGrupo(msgId);
		if(s != null) {
			if(s.isAllRecebida()) {
				if(!msg.isEntregue()) {
					msg.setEntregue(true);
					msg.setDataEntregue(new Date());
					rep.save(msg);
				}
			}
			if(s.isAllLida()) {
				if(!msg.isLida()) {
					msg.setLida(true);
					msg.setDataLida(new Date());
					rep.save(msg);
				}
			}
		}
		return s;
	}

	@Override
	public void checkHibernateProxy(Mensagem mensagem) {
		rep.checkHibernateProxy(mensagem);
	}

	@Override
	public List<Usuario> findUsersFromConversation(Long conversaId, Long userId, boolean includeUser) {
		List<Usuario> usuarios = new ArrayList<Usuario>();

		MensagemConversa conversa = getMensagemConversa(conversaId);

		if (conversa == null) {
			return null;
		}

		if (conversa.isGrupo()) {
			Grupo grupo = conversa.getGrupo();

			usuarios = grupoService.findUsuarios(grupo);
			// Remover usuario logado
			for (Usuario u : usuarios) {
				
				if(!includeUser) {
					if(u.getId().equals(userId)) {
						usuarios.remove(u);
						break;
					}	
				}
			}
		} else {
			if (conversa.getFrom().getId().equals(userId)) {
				usuarios.add(conversa.getTo());
			} else {
				usuarios.add(conversa.getFrom());
			}
			
			if(includeUser) {
				Usuario user = usuarioService.get(userId);
				usuarios.add(user);
			}
		}

		return usuarios;
	}

	@Override
	public MensagemConversa findByGrupo(Grupo g) {
		return repConversa.findByGrupo(g);
	}

	@Override
	public Boolean isUltimaMensagem(Long mensagemId) {
		Long id = rep.findConversaByMensagem(mensagemId);
		if(id != null && id.equals(mensagemId)) {
			return true;
		}
		return false;
	}

	@Override
	public Long countMensagensNaoLidas(Usuario u) {
		if(!Livecom.getInstance().isChatOn()) {
			return 0L;
		}
		return rep.getBadges(u);
	}

	@Override
	public Long countMensagensNaoLidas() {
		if(!Livecom.getInstance().isChatOn()) {
			return 0L;
		}
		return rep.countMensagensNaoLidas();
	}

	@Override
	public List<Long> usersToSilent(Empresa e) {
		return rep.usersToSilent(e);
	}

	@Override
	public List<Long> findAllIdsConversasByUser(Usuario u) {
		return rep.findAllIdsConversasByUser(u);
	}

	@Override
	public List<Long> findAllMensagensIdsOwnerByUser(Usuario u) {
		return rep.findAllMensagensIdsOwnerByUser(u);
	}

	@Override
	public List<MensagemConversa> findAllConversasByUser(long userId) {
		return rep.findAllConversasByUser(userId);
	}

	@Override
	public boolean isTodasMensagensLidas(Long conversaId, Long userId, boolean isValidateGroup) {
		return repConversa.isTodasMensagensLidas(conversaId, userId, isValidateGroup);
	}
	
	@Override
	public boolean isTodasEntregue(Long conversaId, Long userId) {
		return repConversa.isTodasEntrege(conversaId, userId);
	}

	@Override
	public List<Mensagem> findMensagensCallback(MensagemConversa c, Usuario user, Long lastReceivedMessageID) {
		return rep.findMensagensCallback(c, user,lastReceivedMessageID);
	}

	@Override
	public void updateDataPushMsg(Long msgId) {
		rep.updateDataPushMsg(msgId);
	}

	@Override
	public void updateCallbackMensagem(long conversaId,Long msgId,boolean callback1cinza, boolean callback2cinza, boolean callback2azul) {
		rep.updateCallbackMensagem(conversaId,msgId,callback1cinza,callback2cinza,callback2azul);
	}

	@Override
	public Mensagem findByIdentifier(Long conversaId, Long idUserFrom, Long identifier) {
		return rep.findByIdentifier(conversaId, idUserFrom, identifier);
	}

	@Override
	public Long getBadge(MensagemConversa c, Usuario user) {
		return rep.getBadges(c, user);
	}

	@Override
	public List<Mensagem> findMensagensMobile(Usuario user, Long lastMsgId) {
		return rep.findMensagensMobile(user, lastMsgId);
	}
	
	@Override
	public List<Mensagem> findMensagensById(List<Long> msgIds) {
		return rep.findMensagensById(msgIds);
	}
	
	@Override
	public List<Long> findIdsConversasLidasById(List<Long> ids, Long userId) {
		return repConversa.findIdsConversasLidasById(ids, userId);
	}

	@Override
	public List<Mensagem> findMensagensNaoLidas(MensagemConversa conversa) {
		return rep.findMensagensNaoLidas(conversa);
	}

	@Override
	public void forward(Mensagem msg, MensagemConversa c) {
		rep.forward(msg,c);
	}
	
	
	@Override
	public boolean markMessagesWasReceived(MensagemConversa conversa, Usuario from) throws DomainMessageException {
		if(conversa != null) {
			Long userId = from.getId();
			Long conversaId = conversa.getId();
			boolean isGrupo = conversa.isGrupo();
			if (isGrupo) {
				statusMensagemUsuarioService.updateStatusConversaGrupo(userId, conversaId, 1);
				List<Mensagem> msgs = findMensagensNaoLidas(conversa);
				for (Mensagem msg : msgs) {
					Long msgId = msg.getId();
					updateMensagemEntregueLida(msgId, msg);
				}
			} else {
				rep.updateStatusConversa(userId, conversaId, 1);
			}
			
			
			conversa = getMensagemConversa(conversa.getId());

			if(conversa != null && conversa.isGrupo()) {
				/**
				 * Se for grupo só retorna o conversationWasRead (c2A) se todos leram a msg.
				 */
				boolean todasEntrege = isTodasEntregue(conversaId, userId);
				return todasEntrege;
			} else {
				return true;
			}	
		}
		return false;
	}
}
