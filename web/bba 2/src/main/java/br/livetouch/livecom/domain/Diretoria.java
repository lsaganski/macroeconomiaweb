package br.livetouch.livecom.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;

import br.livetouch.livecom.domain.vo.DiretoriaVO;
import net.livetouch.tiger.ddd.DomainException;

@Entity
public class Diretoria extends JsonEntity{
	private static final long serialVersionUID = 691495770535792242L;
	
	@Id
	@Column(name="id", unique=true, nullable=false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "DIRETORIA_SEQ")
	private Long id;
	private String nome;
	private String codigo;
	private boolean padrao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "diretoria_executiva_id")
	private Diretoria diretoriaExecutiva;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigo() {
		return codigo;
	}
	
	public String getCodigoDesc() {
		return codigo != null ? codigo : id.toString();
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Diretoria getDiretoriaExecutiva() {
		return diretoriaExecutiva;
	}

	public void setDiretoriaExecutiva(Diretoria diretoriaExecutiva) {
		this.diretoriaExecutiva = diretoriaExecutiva;
	}

	public void isValidFormCreate() throws DomainException {
		
	}

	public static void isFormValid(Diretoria diretoria, boolean isUpdate) throws DomainException {
		if(diretoria == null) {
			throw new DomainException("Parametros inválidos");
		}
		
		if(StringUtils.isEmpty(diretoria.nome)) {
			throw new DomainException("Campo nome não pode ser vazio");
		}
		
		if(isUpdate && diretoria.getId() == null) {
			throw new DomainException("Diretoria não localizada");
		}
		
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}

	public boolean isPadrao() {
		return padrao;
	}

	public void setPadrao(boolean padrao) {
		this.padrao = padrao;
	}

	@Override
	public String toJson() {
		DiretoriaVO vo = new DiretoriaVO(this);
		return new Gson().toJson(vo);
	}
	
	@Override
	public String toString() {
		return toStringIdDesc();
	}
	
	public String toStringIdDesc() {
		return id + ": " + nome;
	}
}
