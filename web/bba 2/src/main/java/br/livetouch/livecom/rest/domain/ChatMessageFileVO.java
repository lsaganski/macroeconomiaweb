package br.livetouch.livecom.rest.domain;

import java.util.HashSet;

import br.livetouch.livecom.chatAkka.ChatFile;

public class ChatMessageFileVO {
	public String code;
	public Long conversationID;
	public Long identifier;
	public HashSet<ChatFile> arquivos;
	public String msg;
	public Long fromUserID;
	public String fromNome;
	public String from;
}
