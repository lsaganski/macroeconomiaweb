package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.Filtro;

@Repository
public interface EmpresaRepository extends net.livetouch.tiger.ddd.repository.Repository<Empresa> {

	List<Empresa> filterEmpresa(Filtro filtroEmpresa);

	Empresa findDominio(String dominio);

	Empresa findTrial(String codigo);

	Empresa findByName(Empresa empresa);

	Empresa findByAdmin(Usuario u);

	
}