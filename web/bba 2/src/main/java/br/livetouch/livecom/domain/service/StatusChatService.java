package br.livetouch.livecom.domain.service;


import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.StatusChat;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

public interface StatusChatService extends br.livetouch.livecom.domain.service.Service<StatusChat> {

	List<StatusChat> findAll(Usuario user);
	List<StatusChat> findAll();
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(StatusChat f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(StatusChat f) throws DomainException;
	
	List<StatusChat> getStatusUsuario();
}