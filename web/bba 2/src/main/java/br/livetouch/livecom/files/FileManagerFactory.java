package br.livetouch.livecom.files;

import java.io.IOException;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;

public class FileManagerFactory {

	public static FileManager getFileManager(ParametrosMap params) throws IOException {
		String ftpOn = params.get(Params.FTP_ON, "0");
		if("1".equals(ftpOn)) {
			return new FTPFileManager(params);
		} else {
			return  new AmazonS3FileManager(params);
		}
	}
}
