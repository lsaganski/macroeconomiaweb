package br.livetouch.livecom.domain.enums;

/**
 * @author Ricardo Lecheta
 * @created 14/03/2013
 */
public enum StatusContato {

	A_PENDENTE,
	B_RESOLVENDO,
	C_RESOLVIDO
}
