package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.OperacoesCielo;
import br.livetouch.livecom.domain.repository.OperacoesCieloRepository;
import br.livetouch.livecom.domain.vo.OperacoesCieloVO;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class OperacoesCieloRepositoryImpl extends StringHibernateRepository<OperacoesCielo> implements OperacoesCieloRepository {

	public OperacoesCieloRepositoryImpl() {
		super(OperacoesCielo.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findIdDataTransacao(Long idEmpresa) {
		Query query = createQuery("select id,data from OperacoesCielo where empresa.id = :idEmpresa");
		query.setLong("idEmpresa", idEmpresa);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OperacoesCielo> transacoesByEmpresa(Empresa empresa, int page, Integer max, boolean count) {

		Query query = getQueryFindAll(empresa, page, max, count);

		if (!count) {
			int firstResult = 0;
			if (page != 0) {
				firstResult = (page * max);
			}
			query.setFirstResult(firstResult);
			query.setMaxResults(max);
		}

		query.setCacheable(true);
		List<OperacoesCielo> transacoes = query.list();

		return transacoes;

	}

	@Override
	public long getCountTransacoesByEmpresa(Empresa empresa, int page, Integer max, boolean count) {
		Query query = getQueryFindAll(empresa, page, max, count);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	private Query getQueryFindAll(Empresa empresa, int page, Integer max, boolean count) {

		StringBuffer sb = new StringBuffer();

		if (count) {
			sb.append("select count(distinct t.id) ");
		}

		sb.append("FROM OperacoesCielo t WHERE t.empresa = :empresa");

		sb.append(" order by t.data desc");

		Query q = createQuery(sb.toString());
		q.setParameter("empresa", empresa);
		return q;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OperacoesCielo> findAll(Empresa empresa) {
		Query q = createQuery("FROM OperacoesCielo t where  t.empresa = :empresa");
		q.setParameter("empresa", empresa);
		List<OperacoesCielo> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OperacoesCieloVO> graficoPicoTps() {

		StringBuffer sb = new StringBuffer();

		sb.append("select new br.livetouch.livecom.domain.vo.OperacoesCieloVO(MAX(t.picoTps), DATE_FORMAT(t.data,'%Y')) ");
		sb.append("from OperacoesCielo t ");
		sb.append("group by DATE_FORMAT(t.data,'%Y') ");
		sb.append("order by DATE_FORMAT(t.data,'%Y') asc");

		Query q = createQuery(sb.toString());

		List<OperacoesCieloVO> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OperacoesCieloVO> graficoMomento() {

		StringBuffer sb = new StringBuffer();

		sb.append("select new br.livetouch.livecom.domain.vo.OperacoesCieloVO(MAX(t1.volumetria), DATE_FORMAT(data,'%Y')) ");
		sb.append("from OperacoesCielo t1 ");
		sb.append("where t1.volumetria in (select MAX(volumetria) from OperacoesCielo group by DATE_FORMAT(data,'%Y')) ");
		sb.append("group by DATE_FORMAT(t1.data,'%Y') ");
		sb.append("order by min(t1.data)");

		Query q = createQuery(sb.toString());

		List<OperacoesCieloVO> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OperacoesCieloVO> graficoTransacao() {

		StringBuffer sb = new StringBuffer();

		sb.append(
				"select new br.livetouch.livecom.domain.vo.OperacoesCieloVO(SUM(t.volumetria), DATE_FORMAT(t.data,'%Y')) ");
		sb.append("from OperacoesCielo t ");
		sb.append("group by DATE_FORMAT(t.data,'%Y') ");
		sb.append("order by min(t.data)");

		Query q = createQuery(sb.toString());

		List<OperacoesCieloVO> list = q.list();
		return list;
	}

	public OperacoesCielo getByData(Date data){

		StringBuffer sb = new StringBuffer();
		
		data = DateUtils.setTimeInicioDia(data);

		sb.append("from OperacoesCielo t where t.data = :data");

		Query q = createQuery(sb.toString());
		
		q.setParameter("data", data);

		OperacoesCielo transacao = (OperacoesCielo) q.uniqueResult();
		return transacao;
	}

	@Override
	public void deleteAll(Long empresaId) {
		Query q = createQuery("delete from OperacoesCielo oc where oc.empresa.id = :empresaId");
		q.setParameter("empresaId", empresaId);
		q.executeUpdate();
	}

}