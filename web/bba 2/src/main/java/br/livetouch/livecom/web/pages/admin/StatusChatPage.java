package br.livetouch.livecom.web.pages.admin;


import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.StatusChat;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.Table;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LinkDecorator;

/**
 * 
 */
@Controller
@Scope("prototype")
public class StatusChatPage extends LivecomAdminPage {

	public Table table = new Table();
	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", this,"editar");
	public Link deleteLink = new Link("deletar", this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public StatusChat statusChat;
	public Long usuario;
	private TextField tUsuario;
	private TextField tNome;
	

	public List<StatusChat> statusChats;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}
	
	@Override
	public void onInit() {
		super.onInit();

		form();
		
		table();

		if (id != null) {
			statusChat = statusChatService.get(id);
		} else {
			statusChat = new StatusChat();
		}
		form.copyFrom(statusChat);
	}
	
	public void form(){
		form.add(new IdField());

		tNome = new TextField("nome", getMessage("coluna.status.label"));
		tNome.setFocus(true);
		tNome.setAttribute("class", "input inputClean");
		form.add(tNome);
		
		tUsuario = new TextField("usuario");
		tUsuario.setAttribute("class", "inputClean");
		tUsuario.setId("usuario");
		form.add(tUsuario);
		
		Submit tsalvar = new Submit("salvar", this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);
		
		Submit cancelar = new Submit("novo",this,"novo");
		cancelar.setAttribute("class", "botao");
		form.add(cancelar);

	}
	
	private void table() {
		Column c = new Column("nome", getMessage("coluna.status.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);
		
		c = new Column("usuario.nome", getMessage("usuario.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);
		
		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes");
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				statusChat = id != null ? statusChatService.get(id) : new StatusChat();
				form.copyTo(statusChat);
				
				Usuario u = usuario != null ? usuarioService.get(usuario) : new Usuario();
				statusChat.setUsuario(u);

				statusChatService.saveOrUpdate(statusChat);

				setFlashAttribute("msg",  "StatusChat ["+statusChat.getNome()+"] salvo com sucesso.");
				setRedirect(getClass());

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(getClass());
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		statusChat = statusChatService.get(id);
		form.copyFrom(statusChat);
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			StatusChat e = statusChatService.get(id);
			statusChatService.delete(e);
			setRedirect(getClass());
			setFlashAttribute("msg", "StatusChat deletado com sucesso.");
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = "StatusChat possui relacionamentos. Não foi possível excluir.";
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		statusChats = statusChatService.getStatusUsuario();

		// Count(*)
		int pageSize = 100;

		table.setPageSize(pageSize);
		table.setRowList(statusChats);
	}
}
