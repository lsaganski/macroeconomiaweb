package br.livetouch.livecom.web.pages.cadastro;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

@Controller
@Scope("prototype")
public class DiretoriasPage extends CadastrosPage {
	public boolean escondeChat = true;
	public Long id;
	public Form form = new Form();

	@Override
	public void onInit() {
		super.onInit();
		form();
	}

	public void form() {
		
		form.add(new Checkbox("padrao", getMessage("padrao.label")));
		
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip botao export");
		form.add(tExportar);
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();
	}
	
	public boolean exportar(){
		String csv = diretoriaService.csvDiretoria(getEmpresa());
		download(csv, "diretorias.csv");
		return true;
	}
}
