package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.util.Utils;
import br.infra.web.click.EntityField;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.LikeVO;
import br.livetouch.pushserver.lib.PushService;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class PushPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMsg;
	private TextField tTitulo;
	private TextField tTituloOriginal;
	private TextField tMode;
	private EntityField<Usuario> tUser;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		
		form.add(tUser = new EntityField<Usuario>("user_id", false, usuarioService));
		form.add(tMsg = new TextField("msg", true));
		form.add(tTitulo = new TextField("titulo", true));
		form.add(tTituloOriginal = new TextField("titulo_original", true));
		
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");
		
		form.add(new Submit("enviar"));

		setFormTextWidth(form, 400);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			String msg 		= tMsg.getValue();
			String titulo 	= tTitulo.getValue();
			Usuario usuario = tUser.getEntity();
			String tituloOriginal = tTituloOriginal.getValue();
			
			ParametrosMap params = ParametrosMap.getInstance(getEmpresaId());
			
			String modoRestricao = ParametrosMap.getInstance(getEmpresaId()).get(Params.RESTRICAO_HORARIO_MODO,"");
			
			if(modoRestricao.equals("grupo")) {
				boolean isGrupoDentroHorario = grupoService.isGruposDentroHorario(usuario);

				if(!isGrupoDentroHorario) {
					String msgGrupo = ParametrosMap.getInstance(usuario.getEmpresa().getId()).get("grupo.fora.horario.msg", "O sistema não é acessível fora do seu horário de trabalho.");
					throw new DomainException(msgGrupo);
				}
			} else {
				boolean isUsuarioDentroHorario = true;
				if("1".equals(ParametrosMap.getInstance(getEmpresaId()).get(Params.USUARIO_RESTRICAO_HORARIO_ON,"0"))) {
					isUsuarioDentroHorario = usuarioService.isUsuarioDentroHorario(usuario) && Utils.isExpedienteValido(usuario.getExpediente());
				} 
				
				if(usuario != null && !isUsuarioDentroHorario) {
					String msgUsuario = ParametrosMap.getInstance(getEmpresaId()).get(Params.MSG_HORARIO_USUARIO,"O sistema não é acessível fora do seu horário de trabalho.");
					throw new DomainException(msgUsuario);
				}
			}
			
			PushService service = Livecom.getPushService();
			String projeto = params.get(Params.PUSH_SERVER_PROJECT, "Livecom");

			service.push(projeto, usuario.getLogin(), 1, titulo, tituloOriginal,null, null, "", msg, 0, mode, "push", false);

			return new MensagemResult("OK","Mensagem push enviada.");
		}

		return new MensagemResult("NOK","Mensagem push não enviada.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("like", LikeVO.class);
		super.xstream(x);
	}
}
