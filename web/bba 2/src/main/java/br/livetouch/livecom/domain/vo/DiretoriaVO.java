package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Diretoria;

public class DiretoriaVO implements Serializable{
	private static final long serialVersionUID = 5891520266760664729L;
	public Long id;
	public String nome;
	public String codigo;
	public String codigoDesc;
	public boolean padrao;
	public DiretoriaVO diretoriaExecutiva;
	
	
	public DiretoriaVO() {
	}
	
	public DiretoriaVO(Diretoria d, Boolean isFindDirExc) {
		setDiretoria(d, isFindDirExc);
	}

	private void setDiretoria(Diretoria d, Boolean isFindDirExc) {
		this.id = d.getId();
		this.nome = d.getNome();
		this.codigo = d.getCodigo();
		this.padrao = d.isPadrao();
		this.codigoDesc = codigo != null ? codigo : id.toString();
	}

	public DiretoriaVO(Diretoria d) {
		this.id = d.getId();
		this.nome = d.getNome();
		this.codigo = d.getCodigo();
		this.padrao = d.isPadrao();
		this.codigoDesc = codigo != null ? codigo : id.toString();
	}


	public static List<DiretoriaVO> setFromList(List<Diretoria> diretorias) {
		List<DiretoriaVO> list = new ArrayList<DiretoriaVO>();
		if(diretorias == null) {
			return list;
		}
		
		for (Diretoria d : diretorias) {
			DiretoriaVO diretoriaVO = new DiretoriaVO(d);
			if(d.getDiretoriaExecutiva() != null) {
				diretoriaVO.diretoriaExecutiva = new DiretoriaVO(d.getDiretoriaExecutiva());
			}
			list.add(diretoriaVO);
		}
		
		return list;
	}
	
	public static List<DiretoriaVO> fromList(List<Diretoria> diretorias) {
		return setFromList(diretorias);
	}
	
	
}
