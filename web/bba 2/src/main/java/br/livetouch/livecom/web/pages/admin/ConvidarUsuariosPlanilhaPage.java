package br.livetouch.livecom.web.pages.admin;

import java.io.File;
import java.io.InputStream;
import java.util.Date;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.enums.TipoLogSistema;
import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;
import br.livetouch.livecom.jobs.importacao.JobImportacaoUtil;
import br.livetouch.livecom.jobs.info.LogSistemaJob;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.sf.click.control.FileField;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

/**
 * 
 */
@Controller
@Scope("prototype")
public class ConvidarUsuariosPlanilhaPage extends LivecomAdminPage {
	protected static final Logger logger = Log.getLogger(ConvidarUsuariosPlanilhaPage.class);

	public Form formImportacao = new Form();
	
	public Form form = new Form();
	private FileField fileField;
	public String entrada;
	public String count;
	public String countOk;
	public String countError;
	public String countInsert;
	public String countUpdate;
	JobInfo jobInfo;
	
	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
		
		formImportacao();

	}
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.ENVIAR_EMAILS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	public void formImportacao() {
		jobInfo = JobInfo.getInstance(getEmpresaId());

		formImportacao.add(fileField = new FileField("file", true));
		//fileField.setAttribute("style", "display:none;");

		entrada = ParametrosMap.getInstance(getEmpresaId()).get(Params.PASTAENTRADA);

		Submit tImportar = new Submit("upload", "Importar", this, "upload");
		tImportar.setAttribute("class", "btn btn-primary");
		formImportacao.add(tImportar);
	}

	public boolean upload() {
		super.onRender();
		LogSistema logSistema = new LogSistema();
		if (form.isValid()) {
			Usuario usuario = getUsuario();
			String userImportou = usuario.getNome();
			FileItem fileItem = fileField.getFileItem();
			try {

				ParametrosMap map = ParametrosMap.getInstance(getEmpresaId());
				InputStream in = fileItem.getInputStream();

				if (in == null) {
					setMessageError(getMessage("msg.arquivo.invalido.error"));
					return false;
				}

				String importOn = ParametrosMap.getInstance(getEmpresaId()).get(Params.IMPORTACAO_ARQUIVO_ON, "0");
				if (!StringUtils.equals(importOn, "1")) {
					setMessageError("Parametro importacao.arquivo.on está desativado");
					return false;
				}

				byte[] bytes = fileItem.get();
				if (StringUtils.isEmpty(entrada)) {
					setMessageError(getMessage("pastaDeEntrada.error"));
					return false;
				}

				String[] split = StringUtils.split(StringUtils.trim(fileItem.getName()), ".");
				if (split == null || split.length == 0) {
					setMessageError("Não foi possível fazer o parse do arquivo, formato não identificado");
					return false;
				}

				Empresa empresa = getEmpresa();
				if (empresa == null) {
					setMessageError("Empresa não identificada");
					return true;
				}
				String nome = JobImportacaoUtil.normalizeNomeArquivo(split[0]);
				String ext = split[1];
				jobInfo.arquivo = nome;

				nome += "." + ext;
				
				logSistema.setMsg("Importou o arquivo " + nome);
				if (!JobImportacaoUtil.isImportFile(nome, map)) {
					setMessageError("O arquivo <b>" + nome + "</b> não é reconhecido pelo sistema");
					return false;
				}

				jobInfo.arquivo += "_" + empresa.getId() + "." + ext;

				File f = new File(StringUtils.trim(entrada) + "/" + jobInfo.arquivo);
				logger.debug("Importando arquivo: " + f);

				FileUtils.writeByteArrayToFile(f, bytes);

				
				
				String msgLog = "[" + userImportou + "]" + " importou o arquivo ["+ nome + "]";
				LogAuditoria.log(getUsuario(), null, AuditoriaEntidade.IMPORTACAO, AuditoriaAcao.IMPORTAR, msgLog);
				
				jobInfo.temArquivo = true;
				setFlashAttribute("msg", getMessage("msg.importacao.arquivo.salvar.sucess"));
				refresh();

				return true;
			} catch (Exception e) {
				String msgLog = userImportou + " Tentou importar o arquivo [" + fileItem.getName() + "]";
				LogAuditoria.log(getUsuario(), null, AuditoriaEntidade.IMPORTACAO, AuditoriaAcao.IMPORTAR, msgLog, e);
				
				logSistema.setTipo(TipoLogSistema.ERRO);
				logSistema.setException(e.getMessage() + " - " + e);
				logError(e.getMessage(), e);
				return false;
			} finally {
				logSistema.setTipo(TipoLogSistema.INFORMACAO);
				logSistema.setUsuario(getUsuario());
				logSistema.setEmpresa(getEmpresa());
				logSistema.setData(new Date());
				LogSistemaJob.getInstance().addLogSistema(logSistema);
			}
		}
		setMessageError(getMessage("msg.importacao.arquivo.salvar.error"));
		return false;
	}
}
