package br.livetouch.livecom.domain.repository;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.LoginReport;

@Repository
public interface LoginReportRepository extends net.livetouch.tiger.ddd.repository.Repository<LoginReport> {

	
	
}