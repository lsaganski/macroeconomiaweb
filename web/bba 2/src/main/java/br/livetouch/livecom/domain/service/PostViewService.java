package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostView;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

public interface PostViewService extends Service<PostView> {

	List<PostView> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(PostView f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(PostView f) throws DomainException;

	List<PostView> findAllByUser(Usuario u);

	long getCount();

	PostView findByUserPost(Usuario u, Post p);
	
	List<PostView> findAllByPost(Post post);

	void visualizarPosts(List<Post> posts, Usuario user);
	
}