package br.livetouch.livecom.web.pages.pages;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class PostPage extends LivecomLogadoPage {

	public Long id;
	public Long notId;
	public boolean isShowThumbCrop = true;

	@Override
	public void onInit() {
		super.onInit();
		if(id != null) {
			Post p = postService.get(id);
			if(p != null) {
				
				if(!hasPermissao(ParamsPermissao.VISUALIZAR_TODOS_OS_POSTS)) {
					
					Set<Grupo> grupos = new HashSet<Grupo>();
					List<Grupo> subgrupos = grupoService.findSubgruposInGrupos(p.getGrupos());
					grupos.addAll(subgrupos);
					grupos.addAll(p.getGrupos());
					
					if (Collections.disjoint(usuarioService.getGrupos(getUserInfo()), grupos)) {
						boolean visivel = postService.isVisivel(getUsuario(), p);
						if(!visivel && !grupoService.hasAberto(grupos)) {
							setRedirect(MuralPage.class);
						}
					}
				
				}
			} else {
				setRedirect(MuralPage.class);
			}
		} else {
			setRedirect(MuralPage.class);
		}
		
	}
	
	@Override
	public void onRender() {
		super.onRender();
		this.web_layout_sideBox = ParametrosMap.getInstance(getEmpresa()).get("web.layout.sideBox", "0");
	}
}
