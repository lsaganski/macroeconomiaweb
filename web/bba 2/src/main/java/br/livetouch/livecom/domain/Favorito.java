package br.livetouch.livecom.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.utils.DateUtils;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Favorito extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "FAVORITO_SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id", nullable = true)
	private Post post;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "arquivo_id", nullable = true)
	private Arquivo arquivo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario usuario;
	
	private boolean favorito;

	private Boolean lidaNotification;
	
	private Date data;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Boolean getLidaNotification() {
		if(lidaNotification == null) {
			return false;
		}
		return lidaNotification;
	}
	public void setLidaNotification(Boolean lidaNotification) {
		this.lidaNotification = lidaNotification;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Post getPost() {
		return post;
	}
	
	public void setPost(Post post) {
		this.post = post;
	}
	
	public void setFavorito(boolean favorito) {
		this.favorito = favorito;
	}
	
	public boolean isFavorito() {
		return favorito;
	}

	@Override
	public String toString() {
		return "Favorito [post=" + post + ", usuario=" + usuario +", arquivo=" + arquivo + ", favorito=" + favorito + "]";
	}
	

	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public String getDataStringHojeOntem() {
		return DateUtils.toDateStringHojeOntem(getData());
	}
	
	public String getDataString() {
		return net.livetouch.extras.util.DateUtils.toString(data, "dd/MM/yyyy HH:mm:ss");
	}
	
	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

}
