package br.livetouch.livecom.domain.service.impl;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import br.infra.util.Log;

@SuppressWarnings("unchecked")
@Service
public abstract class LivecomService<T> implements br.livetouch.livecom.domain.service.Service<T> {
	protected static final Logger log = Log.getLogger(LivecomService.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	protected ApplicationContext applicationContext;
	
	protected Query createQuery(String query){
		return getSession().createQuery(query);
	}
	
	public int executeNative(String sql,Long... ids) {
		try {
			SQLQuery q = getSession().createSQLQuery(sql);
			for (int i = 0; i < ids.length; i++) {
				q.setLong(i, ids[i]);
			}
			int count = executeUpdate(q);
			log.debug("Delete [" + count + "]");
			return count;
		} catch (Exception e) {
			return -1;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public int executeNativeIn(String sql,String param,Collection list) {
		if(list == null || list.isEmpty()) {
			return 0;
		}
		try {
			SQLQuery q = getSession().createSQLQuery(sql);
			q.setParameterList(param, list);
			int count = executeUpdate(q);
			log.debug("Delete [" + count + "]");
			return count;
		} catch (Exception e) {
			return -1;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public int executeIn(String hql,String param,Collection list) {
		if(list == null || list.isEmpty()) {
			return 0;
		}

		Query q = getSession().createQuery(hql);
		q.setParameterList(param, list);

		int count  = executeUpdate(q);
		log.debug("Rows updated [" + count + "]");

		return count;
	}
	
	public int execute(String hql,List<Long> ids) {
		Query q = getSession().createQuery(hql);
		if(ids != null) {
			for (int i = 0; i < ids.size(); i++) {
				q.setLong(i, ids.get(i));
			}
		}

		int count  = executeUpdate(q);
		log.debug("Rows updated [" + count + "]");

		return count;
	}
	
	public int execute(String hql) {
		Query q = getSession().createQuery(hql);
		int count  = executeUpdate(q);
		log.debug("Rows updated [" + count + "]");

		return count;
	}
	
	public int execute(String hql,Long... ids) {
		Query q = getSession().createQuery(hql);
		for (int i = 0; i < ids.length; i++) {
			q.setLong(i, ids[i]);
		}

		int count  = executeUpdate(q);
		log.debug("Rows updated [" + count + "]");

		return count;
	}
	
	protected int executeUpdate(Query q) {
		try {
			return q.executeUpdate();
		} catch(RuntimeException e) {
			e.printStackTrace();
			log.error(e.getMessage(), e);
			throw e;
		}
	}
	
	public List<Long> queryIds(String hql, boolean cache, Object... params) {
		Query q = createQuery(hql);
		for (int i = 0; i < params.length; i++) {
			q.setParameter(i, params[i]);
		}
		q.setCacheable(cache);
		List<Long> list = (List<Long>) q.list();
		return list;
	}
	
	@SuppressWarnings("rawtypes")
	public List<Long> queryIdsIn(String hql, boolean cache,String param,Collection list) {
		Query q = createQuery(hql);
		q.setParameterList(param, list);
		q.setCacheable(cache);
		List<Long> ids = (List<Long>) q.list();
		return ids;
	}

	public List<T> query(String hql, boolean cache, Object... params) {
		return query(hql, cache, -1, -1, params);
	}

	public List<T> query(String hql, int page, int maxSize, Object... params) {
		return query(hql, false, page, maxSize, params);
	}

	public List<T> query(String hql, boolean cache, int page, int maxSize, Object... params) {
		Query q = createQuery(hql);
		for (int i = 0; i < params.length; i++) {
			q.setParameter(i, params[i]);
		}

		if (maxSize > 0) {
			int firstResult = 0;
			if (page != 0) {
				firstResult = page * maxSize;
			}
			q.setFirstResult(firstResult);
			q.setMaxResults(maxSize);
		}

		q.setCacheable(cache);
		
		List<T> list = (List<T>) q.list();
		return list;
	}

	/**
	 * Exemplos:
	 * 
	 * 1) Ao excluir categ reassocia os seus posts com a categ padrao
	 * 2) Ao excluir grupo, reassocia o grupo origem dos usuários com o grupo padrão.
	 * 
	 * update Post   set categoria.id = ? where categoria.id=?
	 * update Chapeu set categoria.id = ? where categoria.id=?
	 * 
	 * @param string
	 * @param string2
	 * @param id
	 * @param id2
	 */
	protected int executeReassociate(String table, String atributo, Long idPadrao, Long idToBeDeleted) {
		int count = execute("update "+table+" set "+atributo+".id = ? where "+atributo+".id=?", idPadrao, idToBeDeleted);
		return count;
	}
	
	/**
	 * update Chapeu set categoria = null where categoria.id=?
	 * 
	 * @param table
	 * @param atributo
	 * @param idToBeDeleted
	 * @return
	 */
	protected int executeSetNull(String table, String atributo, Long idToBeDeleted) {
		int count = execute("update "+table+" set "+atributo+" = null where "+atributo+".id=?", idToBeDeleted);
		return count;
	}

	protected void executeCascadeUpdate(Long id,String table, String child) {
		List<Long> ids = queryIds("select id from "+table+" where empresa.id=?", false, id);
		executeIn("update "+table+" set "+child+" = null where id in (:ids)","ids", ids);
		execute("delete from "+table+" where empresa.id=?", id);		
	}
	
	protected void executeCascadeDelete(Long id,String table, String table2) {
		List<Long> ids = queryIds("select id from "+table+" where empresa.id=?", false, id);
		executeIn("delete from "+table2+" where "+StringUtils.uncapitalize(table)+".id in (:ids)","ids", ids);
		execute("delete from "+table+" where empresa.id=?", id);
	}
	
	protected Session getSession() {
		Session session = sessionFactory.getCurrentSession();
		return session;
	}
}
