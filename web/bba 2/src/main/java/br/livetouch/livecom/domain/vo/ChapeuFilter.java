package br.livetouch.livecom.domain.vo;

import java.util.List;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.spring.SearchInfo;

public class ChapeuFilter extends SearchInfo {

	public String nome;
	public List<Long> notIds;
	public CategoriaPost categoria;
}