package br.livetouch.livecom.web.pages.admin;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class ConvidarUsuariosPage extends LivecomAdminPage {
	protected static final Logger logger = Log.getLogger(ConvidarUsuariosPage.class);

	public Form form = new Form();
	public Long id;
	public String status;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.ENVIAR_EMAILS)) {
			return true;
		}
		
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;

		form();
	}

	public void form() {
		form.setMethod("post");
		TextField tAssunto = new TextField("assunto");
		String assunto = getParametrosMap().get(Params.EMAIL_SUBJECT_CONVITE, "Convite Livecom");
		tAssunto.setValue(assunto);
		tAssunto.setAttribute("class", "form-control");
		form.add(tAssunto);

		Submit tconsultar = new Submit("enviar", getMessage("enviar.label"), this, "enviar");
		tconsultar.setAttribute("class", "botao btSalvar");
		form.add(tconsultar);
	}
}
