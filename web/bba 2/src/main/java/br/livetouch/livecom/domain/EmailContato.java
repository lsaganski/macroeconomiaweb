package br.livetouch.livecom.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.livetouch.livecom.domain.enums.StatusContato;
import br.livetouch.livecom.domain.enums.TipoEmailContato;

/**
 * @author Ricardo Lecheta
 * @created 14/03/2013
 */
@Entity
public class EmailContato extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 354202716668751815L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "EMAILCONTATO_SEQ")
	private Long id;

	@Column(name="nome", nullable=false, length=150)
	private String nome;
	
	@Column(name="email", nullable=false, length=100)
	private String email;
	
	@Column(name="mensagem", nullable=false)
	private String mensagem;
	
	@Enumerated(EnumType.STRING)
	private StatusContato status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	@Enumerated(EnumType.STRING)
	private TipoEmailContato tipo;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoEmailContato getTipo() {
		return tipo;
	}
	
	public void setTipo(TipoEmailContato motivo) {
		this.tipo = motivo;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMensagem() {
		return this.mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}


	public StatusContato getStatus() {
		return this.status;
	}

	public void setStatus(StatusContato status) {
		this.status = status;
	}


	public Date getData() {
		return data;
	}

	public void setData(Date dtEnvio) {
		this.data = dtEnvio;
	}
}
