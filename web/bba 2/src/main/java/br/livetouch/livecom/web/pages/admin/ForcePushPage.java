package br.livetouch.livecom.web.pages.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.jobs.ForcePushJob;
import br.livetouch.livecom.web.pages.pages.MuralPage;

/**
 * Relatório de acessos
 * 
 */
@Controller
@Scope("prototype")
public class ForcePushPage extends LivecomAdminPage {
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}
	
	@Override
	public void onGet() {
		super.onGet();
		
		msg = "Push disparado com sucesso";
		
		executeJob(ForcePushJob.class, null);
	}
}
