package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Evento;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.repository.EventoRepository;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class EventoRepositoryImpl extends StringHibernateRepository<Evento> implements EventoRepository {

	public EventoRepositoryImpl() {
		super(Evento.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Evento> findByAtivo(List<Grupo> grupos) {

		Date today = new Date();
		today = DateUtils.setTimeInicioDia(today);
		
		StringBuffer sb = new StringBuffer();
		sb.append("select distinct e from Evento e inner join e.grupos g where g in :grupos ");

		if (today != null) {
			sb.append(" and e.data >= :today");
		}
		
		sb.append(" and e.ativo = '1' order by data DESC");

		Query q = createQuery(sb.toString());
		
		if (today != null) {
			q.setParameter("today", today);
		}

		if (grupos != null) {
			q.setParameterList("grupos", grupos);
		}

		q.setCacheable(true);
		List<Evento> eventos = (List<Evento>) q.list();
		return eventos;

	}
}