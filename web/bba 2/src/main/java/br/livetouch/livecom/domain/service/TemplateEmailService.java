package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.enums.TipoTemplate;
import net.livetouch.tiger.ddd.DomainException;

public interface TemplateEmailService extends Service<TemplateEmail> {

	List<TemplateEmail> findAll(Empresa e);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(TemplateEmail t, Empresa e) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(TemplateEmail t) throws DomainException;

	TemplateEmail findByType(Empresa empresa, TipoTemplate tipo);

	TemplateEmail existe(Empresa empresa, TemplateEmail templateEmail);

	List<TemplateEmail> findByTipo(Empresa empresa, TipoTemplate tipo);

}
