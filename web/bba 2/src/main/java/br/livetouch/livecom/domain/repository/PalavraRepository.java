package br.livetouch.livecom.domain.repository;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Palavra;

@Repository
public interface PalavraRepository extends net.livetouch.tiger.ddd.repository.Repository<Palavra> {
	
	
}