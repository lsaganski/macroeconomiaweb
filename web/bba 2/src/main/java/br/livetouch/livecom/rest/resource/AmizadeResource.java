package br.livetouch.livecom.rest.resource;


import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Amizade;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusAmizade;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.PerfilService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/amizade")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class AmizadeResource extends MainResource{

	protected static final Logger log = Log.getLogger(AmizadeResource.class);
	
	@Context
	private SecurityContext securityContext;
	
	@Autowired
	protected UsuarioService usuarioService;
	
	@Autowired
	PerfilService permissaoService;
	
	@Autowired
	protected GrupoService grupoService;
	
	@Context
	protected ServletContext context;
	
		
	@PUT
	@Path("/solicitacao")
	public Response participar(@FormParam("usuarioId") Long usuarioId, @FormParam("amigoId") Long amigoId, @FormParam("solicitacao") boolean solicitacao) throws DomainException {
		
		try {
			if(amigoId == null || usuarioId == null) {
				log.debug("Não foi possivel solicitar/desfazer conexão, ids null");
				setError("Não foi possivel solicitar/desfazer conexão");
				return Response.ok(MessageResult.error("Não foi possivel solicitar/desfazer conexão")).build();
			}
			
			Usuario usuario = usuarioService.get(usuarioId);
			Usuario amigo = usuarioService.get(amigoId);
			
			//Valida se o usuario não está tentando ser amigo de si mesmo
			if(usuarioId.equals(amigoId)) {
				log.debug("[" + usuario.getNome() + "] é si mesmo ["+ amigo.getNome() +"]");
				setError("Você não pode solicitar/recusar conexão com si mesmo");
				return Response.ok(MessageResult.error("Você não pode solicitar/recusar conexão com si mesmo")).build();
			}
			
			Amizade amigos = usuarioService.solicitar(usuario, amigo, solicitacao);
			
			if(amigos == null) {
				log.debug("[" + usuario.getNome() + "] não é mais conexão de ["+ amigo.getNome() +"]");
				setMsg(amigo.getNome() + " não é mais uma conexão");
				return Response.ok(MessageResult.ok(amigo.getNome() + " não é mais uma conexão")).build();
			}
			
			log.debug("[" + usuario.getNome() + "] solicitou a conexão de ["+ amigo.getNome() +"]");
			setMsg("Você solicitou a conexão de " + amigo.getNome());
			return Response.ok(MessageResult.ok("Você solicitou a conexão de " + amigo.getNome())).build();
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			setError("Não foi possivel solicitar/desfazer conexão");
			return Response.ok(MessageResult.error("Não foi possivel solicitar/desfazer conexão")).build();
		}
	}

	@PUT
	@Path("/solicitacao/aprovar")
	public Response aprovacao(@FormParam("usuarioId") Long usuarioId, @FormParam("amigoId") Long amigoId, @FormParam("aprovar") boolean aprovar) throws DomainException {
		
		try {
			if(amigoId == null || usuarioId == null) {
				log.debug("Não foi possivel aprovar/recusar conexão, ids null");
				return Response.ok(MessageResult.error("Não foi possivel aprovar/recusar conexão")).build();
			}
			
			Usuario usuario = usuarioService.get(usuarioId);
			Usuario amigo = usuarioService.get(amigoId);
			
			Amizade amizade = usuarioService.getAmizade(usuario, amigo);
			
			if(amizade == null) {
				log.debug("Não foi possivel aprovar/recusar a conexão do usuario [" + amigo.getNome() + "]");
				return Response.ok(MessageResult.error("Não foi possivel aprovar/recusar a conexão do usuario [" + amigo.getNome() + "]")).build();
			}
			
			if(aprovar) {
				amizade.setStatusAmizade(StatusAmizade.APROVADA);
				usuarioService.criarNotificacao(amizade, TipoNotificacao.USUARIO_ACEITAR_AMIZADE);
			} else {
				// não aprovar amizade do usuario
				usuarioService.deleteAmizade(usuario, amigo);
				List<UsuarioVO> users = usuarioService.findUsuariosByStatusAmizade(StatusAmizade.SOLICITADA, usuario);
				
				notificationService.markSolicitacaoAmizadeAsRead(amigo, usuario);

				usuarioService.criarNotificacao(amizade, TipoNotificacao.USUARIO_RECUSAR_AMIZADE);
				log.debug("Você recusou a conexão do usuario [" + amigo.getNome() + "]");
				return Response.ok(MessageResult.ok("Você recusou a conexão de " + amigo.getNome(), users)).build();
			}
			
			usuarioService.saveOrUpdate(amizade);
			notificationService.markSolicitacaoAmizadeAsRead(amigo, usuario);
			
			List<UsuarioVO> users = usuarioService.findUsuariosByStatusAmizade(StatusAmizade.SOLICITADA, usuario);
			log.debug("Você aprovou a solicitação de conexão do usuario [" + amigo.getNome() + "]");
			return Response.ok(MessageResult.ok("Agora você está conectado com " + amigo.getNome(), users)).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel aprovar/recusar a solicitação de conexão usuario")).build();
		}
	}
	
	@GET
	@Path("/solicitacao/aprovar/amigos/{id}")
	public Response amigos(@PathParam("id") Long id) throws DomainException {
		
		try {
			if(id == null) {
				return Response.ok(MessageResult.error("Não foi possivel verificar os usuarios que solicitaram sua conexão")).build();
			}
			
			Usuario usuario = usuarioService.get(id);
			
			List<UsuarioVO> users = usuarioService.findByStatusAmizade(usuario);
			
			return Response.ok(MessageResult.ok(users)).build();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel verificar os usuarios que solicitaram sua conexão")).build();
		}
	}
	
	@GET
	@Path("/solicitacao/amigos/{id}")
	public Response solicitacoes(@PathParam("id") Long id, @QueryParam("nome") String nome) throws DomainException {
		
		try {
			if(id == null) {
				return Response.ok(MessageResult.error("Não foi possivel verificar os usuarios que são suas conexões")).build();
			}
			
			Usuario usuario = usuarioService.get(id);
			
			List<UsuarioVO> users = usuarioService.findByAmigos(usuario, nome);
			
			return Response.ok(MessageResult.ok(users)).build();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel verificar os usuarios que são suas conexões")).build();
		}
	}
	
	@GET
	@Path("/solicitacao/pendentes/{id}")
	public Response pendentes(@PathParam("id") Long id) throws DomainException {
		
		try {
			if(id == null) {
				return Response.ok(MessageResult.error("Não foi possivel verificar os usuarios que solicitaram sua conexão")).build();
			}
			
			Usuario usuario = usuarioService.get(id);
			
			List<UsuarioVO> users = usuarioService.findByPendentes(usuario);
			
			return Response.ok(MessageResult.ok(users)).build();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel verificar os usuarios que solicitaram sua conexão")).build();
		}
	}
	
}
	
