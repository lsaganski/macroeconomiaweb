package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Formacao;
import net.livetouch.tiger.ddd.DomainException;

public interface FormacaoService extends Service<Formacao> {

	List<Formacao> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Formacao f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Formacao f) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void deleteAll();

	String csvFormacao(Empresa empresa);

	List<Formacao> findAll(Empresa empresa);

}
