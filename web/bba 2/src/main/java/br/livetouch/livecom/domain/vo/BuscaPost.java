package br.livetouch.livecom.domain.vo;

import java.util.Date;
import java.util.List;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Chapeu;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.StatusPost;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.JSONUtils;

public class BuscaPost {

	public Usuario userPost;
	public Usuario user;

	public String texto;
	public List<Tag> tags;

	public List<Grupo> grupos;

	public CategoriaPost categoria;
	public Chapeu chapeu;

	public Date dataInicial;
	public Date dataFinal;

	public boolean comAnexo;
	public boolean destaque;
	public boolean notDestaques;
	public boolean favoritos;
	public boolean rascunhos;
	public boolean buscaGrupos;

	public boolean somenteEu;
	public boolean meusAmigos;

	public List<Long> categoriaIds;
	public List<Long> categoriasNotIn;
	public List<String> categoriasCodigo;
	public List<String> categoriasCodigoNotIn;
	
	public List<Long> amigosIds;

	public Long novosPosts;
	public Long excludePostId;

	public Empresa empresa;
	public Arquivo arquivo;
	
	public StatusPost statusPost;

	public boolean prioritario;
	public boolean tasks;
	public boolean reminders;

	public Idioma idioma;

	@Override
	public String toString() {
		try {
			String json = JSONUtils.toJSON(this);
			return json;
		} catch (Exception e) {
			return "BuscaPost [userPost=" + userPost + ", user=" + user + ", texto=" + texto + ", tags=" + tags + ", grupos=" + grupos + ", categoria=" + categoria + ", chapeu=" + chapeu
					+ ", dataInicial=" + dataInicial + ", dataFinal=" + dataFinal + ", comAnexo=" + comAnexo + ", destaque=" + destaque + ", notDestaques=" + notDestaques + ", favoritos=" + favoritos
					+ ", rascunhos=" + rascunhos + ", buscaGrupos=" + buscaGrupos + " , somenteEu=" + somenteEu + ", categoriaIds=" + categoriaIds + ", categoriasNotIn=" + categoriasNotIn + ", categoriasCodigo=" + categoriasCodigo
					+ ", categoriasCodigoNotIn=" + categoriasCodigoNotIn + ", statusPost=" + statusPost + "]";
		}
	}

}