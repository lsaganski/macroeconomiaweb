package br.livetouch.livecom.domain.exception;

import br.infra.util.MessageUtil;
import net.livetouch.tiger.ddd.DomainException;

/**
 * Exception para regras de negocio, mas com o objetivo de exibir a mensagem final ao usuario (celular)
 * 
 * @author ricardo
 *
 */
public class DomainMessageException extends DomainException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5571798052715248656L;

	public DomainMessageException(String message) {
		super(message);
	}

	@Override
	public String getMessage() {
		String key = super.getMessage();
		String s = MessageUtil.getString(key );
		return s;
	}
}
