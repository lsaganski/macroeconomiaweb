package br.livetouch.livecom.web.pages.cadastro;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import br.livetouch.livecom.web.pages.pages.ArquivosPage;

/**
 * Page mae para usar o Autowired do Spring
 * 
 * @author ricardo
 * 
 */
@Controller
@Scope("prototype")
public class ArquivoPage extends LivecomLogadoPage {
	
	public Long id;
	public Arquivo arquivo;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissoes(false, ParamsPermissao.ARQUIVOS, ParamsPermissao.ACESSO_CADASTROS)) {
			return true;
		}
			
		setRedirect(ArquivosPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();
		
		if(id != null) {
			arquivo = arquivoService.get(id);
		}
	}

	@Override
	public void onRender() {
		super.onRender();
	}

}
