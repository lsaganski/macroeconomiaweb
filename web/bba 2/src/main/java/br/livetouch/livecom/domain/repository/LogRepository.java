package br.livetouch.livecom.domain.repository;

import java.util.List;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.LogTransacao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.DeviceVO;
import br.livetouch.livecom.domain.vo.LogSistemaFiltro;
import br.livetouch.livecom.domain.vo.LogTransacaoFiltro;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.tiger.ddd.DomainException;
import net.livetouch.tiger.ddd.repository.Repository;



public interface LogRepository extends Repository<LogSistema>{

	int MAX = 20;

	List<LogSistema> findLogsSistema(int page, Empresa empresa);

	List<LogTransacao> findLogsTransacao(int page, Empresa empresa);

	public List<LogSistema> findbyFilter(LogSistemaFiltro log, int page, int max, Empresa empresa) throws DomainException;
	List<LogTransacao> findbyFilter(LogTransacaoFiltro log, Usuario usuarioSistema, int page, int max, Empresa empresa) throws DomainException;

	LogTransacao getLogTransacao(Long id);
	LogSistema getLogSistema(Long id);

	void saveOrUpdate(LogTransacao log, Empresa empresa);

	void delete(LogTransacao log);

	long getLogTransacaoMaxId(Empresa empresa);


	LogTransacao findByOtp(Usuario usuario, String otp, Empresa empresa);

	long getCountByFilter(LogTransacaoFiltro filtro, Usuario usuarioSistema, Empresa empresa) throws DomainException;

	long getCountByFilter(LogSistemaFiltro filtro, Empresa empresa) throws DomainException;

	List<DeviceVO> getRegistrationIds(Empresa empresa);
	
	List<LogTransacao> findAllLogTransacao(Empresa empresa);
	
	List<RelatorioAcessosVersaoVO> findLogsByVersao(RelatorioFiltro filtro, Empresa empresa) throws DomainException;

	long getCountLogsByVersao(RelatorioFiltro filtro, Empresa empresa) throws DomainException;
	
	List<String> findAllVersoes(Empresa empresa);

	List<LogSistema> findAll(Empresa empresa);

}