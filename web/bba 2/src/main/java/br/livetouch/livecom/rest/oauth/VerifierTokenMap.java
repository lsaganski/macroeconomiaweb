package br.livetouch.livecom.rest.oauth;

import java.util.concurrent.ConcurrentHashMap;

public class VerifierTokenMap {

	private static final VerifierTokenMap instance = new VerifierTokenMap();
	private ConcurrentHashMap<String, String> verifierToken;
	
	private VerifierTokenMap() {
		verifierToken = new ConcurrentHashMap<>();
	}
	
	public static VerifierTokenMap getInstance() {
		return instance;
	}
	
	public String remove(String token) {
		return verifierToken.remove(token);
	}

	public void put(String token, String verifier) {
		verifierToken.put(token, verifier);
	}
	
}
