package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Agenda;
import net.livetouch.tiger.ddd.DomainException;

public interface AgendaService extends Service<Agenda> {

	List<Agenda> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Agenda f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Agenda f) throws DomainException;

}
