/**
 * 
 */
package br.livetouch.livecom.domain.enums;

public enum Status {
	A("Aprovado"), 
	P("Pendente");

	private final String label;

	Status(String label){
		this.label = label;
	}

	@Override
	public String toString() {
		return label != null ? label : "?";
	}
}