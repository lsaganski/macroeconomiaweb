package br.livetouch.livecom.web.pages.cadastro;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("prototype")
public class LinkConhecidoPage extends CadastrosPage {

	public Long id;
	
	@Override
	public void onInit() {
		super.onInit();
	}

}
