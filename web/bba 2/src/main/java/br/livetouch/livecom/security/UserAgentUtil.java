package br.livetouch.livecom.security;

import org.apache.commons.lang.StringUtils;

import net.sf.click.Context;

public class UserAgentUtil {

	public static String getUserAgent(Context context) {
		return StringUtils.lowerCase(context.getRequest().getHeader("user-agent"));
	}
	
	/**
	 * web, android, ios
	 */
	public static String getUserAgentSO(Context context) {
		String userAgent = context.getRequest().getHeader("user-agent");
		if(StringUtils.isEmpty(userAgent)) {
			return "web";
		}

		if("livecom-android".equals(userAgent)) {
			return "android";
		}

		if("livecom-ios".equals(userAgent)) {
			return "ios";
		}

		userAgent = StringUtils.lowerCase(userAgent);
		
		if(userAgent.contains("android")) {
			return "android";
		}
		
		if(userAgent.contains("iphone")) {
			return "iphone";
		}

		return "web";
	}
	
	/**
	 * web, android, ios
	 */
	public static boolean isMobile(Context c) {
		return isAndroid(c) || isIos(c);
	}
	
	public static boolean isAndroid(Context c) {
		String userAgent = getUserAgentSO(c);
		
		if("android".equals(userAgent)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * web, android, ios
	 */
	public static boolean isIos(Context c) {
		String userAgent = getUserAgentSO(c);
		
		if("ios".equals(userAgent)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * web, android, ios
	 */
	public static boolean isWeb(Context c) {
		String userAgent = getUserAgentSO(c);
		
		if("web".equals(userAgent)) {
			return true;
		}
		
		return !isMobile(c);
	}
}
