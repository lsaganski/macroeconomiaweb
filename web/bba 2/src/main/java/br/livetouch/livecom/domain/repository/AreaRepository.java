package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Area;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.vo.FiltroArea;

@Repository
public interface AreaRepository extends net.livetouch.tiger.ddd.repository.Repository<Area> {

	List<Object[]> findIdCodGrupos(Long idEmpresa);
	List<Area> filterArea(FiltroArea filtro, Empresa empresa);
	List<Area> findAll(Empresa empresa);
	Area findDefaultByEmpresa(Long empresaId);
	List<Object[]> findIdNomeAreas(Long empresaId);
	Area findByName(Area area, Empresa empresa);
	Area findByCodigoValid(Area area, Empresa empresa);

}