
package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostAuditoria;
import br.livetouch.livecom.domain.PostIdioma;
import br.livetouch.livecom.domain.PostUsuarios;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusParticipacao;
import br.livetouch.livecom.domain.enums.StatusPublicacao;
import br.livetouch.livecom.domain.enums.Visibilidade;
import br.livetouch.livecom.domain.repository.PostRepository;
import br.livetouch.livecom.domain.repository.UsuarioRepository;
import br.livetouch.livecom.domain.vo.BuscaPost;
import net.livetouch.extras.util.DateUtils;

@Repository
public class PostRepositoryImpl extends LivecomRepository<Post> implements PostRepository {

	@Autowired
	UsuarioRepository usuarioRep;

	public PostRepositoryImpl() {
		super(Post.class);
	}
	
	@Override
	public Post get(Long id, Idioma idioma) {

		StringBuffer sb = new StringBuffer();
		
		sb.append("select p, pi from Post p inner join p.idiomas pi ");
		sb.append("where p.id = :id ");
		
		if(idioma != null) {
			sb.append("and pi.idioma = :idioma");
		}
		
		Query q = createQuery(sb.toString());

		q.setParameter("id", id);
		
		if(idioma != null) {
			q.setParameter("idioma", idioma);
		}
		
		if(q.iterate() != null && !q.iterate().hasNext()) {
			return null;
		}
		
		Object[] o = (Object[]) q.iterate().next();
		Post p = (Post) o[0];
		PostIdioma pi = (PostIdioma) o[1];
		p.translate(pi);
		return p;

		
	}

	@Override
	public List<Long> findAllOwnerByUser(Usuario u) {
		
		// Posts do usuario
		// Nao precisa do  or usuarioUpdate.id=?, pois marco como update.
		List<Long> list = queryIds("select p.id from Post p where usuario.id = ?",true,u.getId());
		
		// Posts feitos por outros usuários aonde o user é admin.
		List<Long> list2 = queryIds("select p.id from Post p inner join p.grupos g where g.id in (select gu.grupo.id from GrupoUsuarios gu where gu.usuario.id = ? AND gu.grupo.usuario.id = ?)",true,u.getId(), u.getId());
		list.addAll(list2);

		return list;
	}

	@Override
	public List<Long> findAllCriadosByUser(Usuario u) {
		return queryIds("select p.id from Post p where usuario.id = ?",true,u.getId());
	}

	// query busca posts web
	@Override
	@SuppressWarnings("unchecked")
	public List<Post> findAllPosts(BuscaPost b, int page, int pageSize) {

		ParametrosMap params = ParametrosMap.getInstance(b.empresa);

		String sql = "select p, p.dataPublicacao, pi from Post p left join p.grupos g left join g.subgrupos gs ";
		sql += " inner join p.idiomas pi ";

		boolean isFazerBusca = StringUtils.isNotEmpty(b.texto);
		if (isFazerBusca) {
			sql += " left join p.chapeu chapeu ";
		}
		
		sql += " left join p.categorias categoria ";
		
		if(b.arquivo != null) {
			sql += " left join p.arquivos arquivo ";
		}
		
		boolean favoritos = b.favoritos;
		boolean rascunhos = b.rascunhos;

		List<Long> gruposIds = Grupo.getIds(b.grupos);

		List<Long> tags = Tag.getIds(b.tags);
		if (tags != null || isFazerBusca) {
			sql += " left join p.tagsList tags";
		}

		if (favoritos) {
			sql += " inner join p.favoritos favorito ";
		}

		sql += " where 1=1 ";
		if (isFazerBusca) {
			sql += " and ( (pi.titulo like :titulo) ";
			sql += " or (pi.resumo like :titulo) ";
			sql += " or (chapeu.nome like :titulo) ";
			sql += " or (categoria.nome like :titulo) )";
		}
		
		if (b.categoriaIds != null && b.categoriaIds.size() > 0) {
			sql += " and categoria.id in(:categoriaId)";
		}
		
		if(b.categoriasNotIn != null && b.categoriasNotIn.size() > 0) {
			sql += " and categoria.id not in(:categoriasNotIn)";
		}
		
		if(b.categoriasCodigo != null && b.categoriasCodigo.size() > 0) {
			sql += " and categoria.codigo in(:categoriasCodigo)";
		}
		
		if(b.categoriasCodigoNotIn != null && b.categoriasCodigoNotIn.size() > 0) {
			sql += " and (categoria.codigo not in(:categoriasCodigoNotIn) or categoria.codigo is null)";
		}
		
		if(b.idioma != null) {
			sql += " and pi.idioma = :idioma ";
		}
		
		if (b.dataInicial != null) {
			sql += " and p.data >= :dataInicial";
		}
		if (b.dataFinal != null) {
			sql += " and p.data <= :dataFinal";
		}
		if (tags != null) {
			sql += " and tags.id in (:tagsIds)";
		}
		if (gruposIds != null && gruposIds.size() > 0) {
			sql += " and g.id in (:gruposIds)";
		}
		
		if(b.prioritario) {
			sql += " and p.prioritario = 1";
		}

		if(b.tasks) {
			sql += " and p.id in (select pt.post.id from PostTask pt where pt.usuario.id = :userId)";
		}

		if(b.reminders) {
			sql += " and p.id in (select n.post.id from Notification n inner join n.notifications nu where n.tipo = 'REMINDER' and nu.usuario.id = :userId) ";
		}
		
		//Usuario que acessa todos os posts
		boolean verTodos = Livecom.getInstance().hasPermissao(ParamsPermissao.VISUALIZAR_TODOS_OS_POSTS, b.user);
		if(!verTodos) {

			sql += " and (";
			
			// TODO AUGUSTO AMIZADE buscar antes. Buscar no texto por from Amizade e evitar sub-select disso.
		
			//Query para buscar os posts dos amigos com visibilidade MEUS_AMIGOS
			if(!b.amigosIds.isEmpty()) {
				sql += " (p.usuario.id in (:amigosIds) ";
				if(b.meusAmigos) {	
					sql += " and p.visibilidade = :amigos) ";
					sql += " and ";
				} else {
					sql += " and p.visibilidade = :amigos or (p.visibilidade = :privado and p.usuario.id = :userId)) ";
					sql += " or ";
				}
			}
		
			//Query para buscar os posts dos grupos que o usuario possui acesso e visibilidade 'GRUPOS'
			boolean isGrupoHorarioPost = "1".equals(params.get(Params.GRUPO_HORARIO_POST,"0"));
			if(isGrupoHorarioPost) {
				// somente posts de horario permitido
	//			sql += " (g.id in ((select gu.grupo.id from GrupoUsuarios gu where gu.usuario.id =:userId ";
				sql += " (COALESCE(gs.subgrupo.id, g.id) in ((select gu.grupo.id from GrupoUsuarios gu where gu.usuario.id =:userId ";
				sql += " and ((gu.grupo.horaAbertura is null and gu.grupo.horaFechamento is null) ";
				sql += " or (gu.grupo.horaAbertura <= cast(p.dataPublicacao as time) and gu.grupo.horaFechamento >= cast(p.dataPublicacao as time))";
				sql += " or (gu.grupo.horaAbertura = gu.grupo.horaFechamento))";
				sql += " and gu.statusParticipacao = :aprovada)) ";
				sql += " or (g is null and p.usuario.id = :userId) ";
				if(b.meusAmigos && !b.amigosIds.isEmpty()) {
					sql += " or (g is null and p.visibilidade = :amigos) ";
				} else {
					sql += " and (p.visibilidade = 'GRUPOS' or p.usuario.id = :userId) ";
				}
				sql += " ) ";
			} else {
				// somente grupos do usuario
	//			sql += " (g.id in (select gu.grupo.id from GrupoUsuarios gu where gu.usuario.id =:userId ";
				sql += " (COALESCE(gs.subgrupo.id, g.id) in (select gu.grupo.id from GrupoUsuarios gu where gu.usuario.id =:userId ";
				if(b.grupos == null || b.grupos.size() == 0) {
					sql += " and gu.mostraMural = 1 ";
				}
				sql += " and gu.statusParticipacao = :aprovada)";
				sql += " or (g is null and p.usuario.id = :userId) ";
				
				if(b.meusAmigos && !b.amigosIds.isEmpty()) {
					sql += " or (g is null and p.visibilidade = :amigos) ";
				} else {
					sql += " and (p.visibilidade = 'GRUPOS' or p.usuario.id = :userId) ";
				}
				
				sql += " ) ";
			}
		
			sql += " ) ";
		} else {
			if(b.meusAmigos) {	
				sql += " and p.visibilidade = :amigos ";
			}
		}

		if(b.user.getEmpresa() != null) {
			sql += " and p.usuario.empresa = :empresa ";
		}

		if(b.somenteEu) {
			sql += " and (p.visibilidade = :privado and p.usuario.id = :userId) ";
		} 
		
		if (b.comAnexo) {
			sql += " and p.arquivos is not empty";
		}

		if (favoritos) {
			sql += " and favorito.favorito=1 and favorito.usuario.id = :userId";
		}
		
		if (rascunhos) {
			sql += " and p.rascunho = 1";
		} else {
			sql += " and p.rascunho = 0";
		}
		
		if (b.destaque) {
			sql += " and p.destaque = 1";
		}
		if (b.notDestaques) {
			sql += " and (p.destaque is null or p.destaque <> 1)";
		}
		if (b.chapeu != null) {
			sql += " and p.chapeu.id = :chapeuId";
		}

		sql += " and ( (p.statusPublicacao = :publicado) or p.usuario.id=:userId ) ";
		
		if(b.novosPosts != null) {
			sql += " and p.id > :novosPosts";
		}

		if(b.excludePostId != null) {
			sql += " and p.id != :excludePostId";
		}

		if(b.arquivo != null) {
			sql += " and arquivo = :arquivo";
		}
		
		if (b.userPost != null) {
			sql += " and p.usuario.id = :userPostId ";
		}

		if (b.statusPost != null) {
			sql += " and p.statusPost = :statusPost ";
		}
		
		// Group By e Order By para ordenação e SQL SERVER
		sql += " group by p, p.dataPublicacao";
		sql += " order by p.dataPublicacao desc";

		Query q = createQuery(sql);

		q.setParameter("publicado", StatusPublicacao.PUBLICADO);

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * pageSize;
		}
		q.setFirstResult(firstResult);
		q.setMaxResults(pageSize);
		q.setCacheable(true);
		q.setCacheRegion("BUSCA_NOTICAS");

		if(!verTodos) {
			q.setParameter("aprovada", StatusParticipacao.APROVADA);
			if(b.somenteEu) {
				q.setParameter("privado", Visibilidade.SOMENTE_EU);
			}
			
			if(!b.amigosIds.isEmpty()) {
				if(!b.meusAmigos) {
					q.setParameter("privado", Visibilidade.SOMENTE_EU);
				}
				q.setParameter("amigos", Visibilidade.MEUS_AMIGOS);
				q.setParameterList("amigosIds", b.amigosIds);
			}
		} else {
			if(b.meusAmigos) {	
				q.setParameter("amigos", Visibilidade.MEUS_AMIGOS);
			}
		}
		
		if(b.user != null) {
			q.setParameter("userId", b.user.getId());
		}

		if(b.user.getEmpresa() != null) {
			q.setParameter("empresa", b.user.getEmpresa());
		}
		
		if (b.userPost != null) {
			q.setParameter("userPostId", b.userPost.getId());
		}

		if (b.statusPost != null) {
			q.setParameter("statusPost", b.statusPost);
		}
		
		if (isFazerBusca) {
			q.setParameter("titulo", "%" + b.texto + "%");
		}

		if (b.categoriaIds != null && b.categoriaIds.size() > 0) {
			q.setParameterList("categoriaId", b.categoriaIds);
		}
		
		if(b.categoriasNotIn != null && b.categoriasNotIn.size() > 0) {
			q.setParameterList("categoriasNotIn", b.categoriasNotIn);
		}
		
		if(b.categoriasCodigo != null && b.categoriasCodigo.size() > 0) {
			q.setParameterList("categoriasCodigo", b.categoriasCodigo);
		}
		
		if(b.categoriasCodigoNotIn != null && b.categoriasCodigoNotIn.size() > 0) {
			q.setParameterList("categoriasCodigoNotIn", b.categoriasCodigoNotIn);
		}
		
		if (tags != null) {
			q.setParameterList("tagsIds", tags);
		}
		if (gruposIds != null && gruposIds.size() > 0) {
			q.setParameterList("gruposIds", gruposIds);
		}

		
		if(b.idioma != null) {
			q.setParameter("idioma", b.idioma);
		}
		
		if (b.dataInicial != null) {
			q.setParameter("dataInicial", DateUtils.setTimeInicioDia(b.dataInicial));
		}
		if (b.dataFinal != null) {
			q.setParameter("dataFinal", DateUtils.setTimeFimDia(b.dataFinal));
		}

		if (b.chapeu != null) {
			q.setParameter("chapeuId", b.chapeu.getId());
		}

		if (b.novosPosts != null) {
			q.setParameter("novosPosts", b.novosPosts);
		}

		if (b.excludePostId != null) {
			q.setParameter("excludePostId", b.excludePostId);
		}
		
		if(b.arquivo != null) {
			q.setParameter("arquivo", b.arquivo);
		}
		
		if(!isFazerBusca) {
			q.setCacheable(true);
		}
		
		List<Post> posts = new LinkedList<>();
		
		// Le deste jeito para o SQL SERVER
		for (Iterator<Object> iter = q.iterate(); iter.hasNext(); ) { 
			Object[] o = (Object[]) iter.next();
			Post p = (Post) o[0];
			PostIdioma pi = (PostIdioma) o[2];
			p.translate(pi);
			posts.add(p);
		}

		return posts;
	}
	
	// query busca posts web
	@Override
	@SuppressWarnings("unchecked")
	public List<Post> findAllByDate(Date data) {
		
		StringBuffer sb = new StringBuffer("from Post p where p.dataPublicacao >= :data");
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("data", data);
		
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> findAllByTituloLike(String titulo) {
		String sql = "select p from Post p where p.titulo like :titulo";

		Query q = createQuery(sql);
		q.setParameter("titulo", "%" + titulo + "%");
		List<Post> posts = (List<Post>) q.list();
		return posts;
	}

	@Override
	public void saveOrUpdate(PostAuditoria a) {
		getSession().saveOrUpdate(a);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Post> findAllByTituloLikeWithMax(String titulo, int max, Empresa empresa) {
		String sql = "select p from Post p where p.titulo like :titulo AND p.usuario.empresa.id = :empresaId order by data desc";
		
		Query q = createQuery(sql);
		q.setParameter("titulo", "%" + titulo + "%");
		q.setLong("empresaId", empresa.getId());
		q.setMaxResults(max);
		List<Post> posts = (List<Post>) q.list();
		return posts;
	}

	@Override
	public Post findByIdWordpress(Long id) {
		String sql = "from Post p where p.idWordpress = :id";
		Query q = createQuery(sql);
		q.setParameter("id", id);
		if (q.list().size() > 0)
			return (Post) q.list().get(0);
		else
			return null;
	}
	
	@Override
	public long getCountUsuariosByPost(Post p) {
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("select count(distinct u.id) ");
		sb.append("from post p ");
		sb.append("inner join post_grupos pg on pg.post_id = p.id ");
		sb.append("inner join grupo_usuarios gu on pg.grupo_id = gu.grupo_id ");
		sb.append("inner join usuario u on gu.usuario_id = u.id ");
		sb.append("where 1=1 ");
		sb.append("and p.id = :post ");
			
		Query q = getSession().createSQLQuery(sb.toString());
		q.setParameter("post", p);
		
		/**
		 * java.lang.IllegalStateException: aliases expected length is 0; actual length is 1
        at org.hibernate.transform.CacheableResultTransformer.transformTuple(CacheableResultTransformer.java:172)

		 */
		// A FAZER Melhoria: se colocar cache deu erro (ver isso)
//		q.setCacheable(true);
		
		Long count = getCount(q);
		
		return count;
	}

	@Override
	public List<Post> findAllRascunhosByUser(Usuario user, int page, int maxSize) {
		String hql = "from Post p where usuario.id = ? and p.rascunho = ? order by p.id desc";
		boolean isRascunho = true;
		List<Post> posts = query(hql,page,maxSize,user.getId(), isRascunho);
		return posts;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Post> findByCategorias(List<CategoriaPost> categorias) {
		StringBuffer sb = new StringBuffer("FROM Post p where p.categoria in(:list)");
		Query q = createQuery(sb.toString());
		q.setCacheable(true);
		q.setParameterList("list", categorias);
		return q.list();
	}

	@Override
	public void delete(List<Post> posts) {
		List<Long> ids = Post.getIds(posts);
		int i = executeIn("delete from Post p where p.id in(:ids)", "ids",ids);
		System.out.println(i);
//		StringBuffer sb = new StringBuffer("delete from Post p where p in(:list)");
//		Query q = createQuery(sb.toString());
//		q.setParameterList("list", posts);
//		q.executeUpdate();
	}
	
	@Override
	public List<Long> findidsByCategoria(CategoriaPost c) {
		Long id = c.getId();
		String hql = "select id from Post where categoria.id=? or chapeu.id in (select id from Chapeu where categoria.id=?)";
		List<Long> list = queryIds(hql,false,id,id);
		return list;
	}
	
	@Override
	public List<Post> findPostsByTag(Tag tag) {
		String hql = "from Post p inner join p.tagsList tag where tag.id=?";
		List<Post> list = query(hql,false,tag.getId());
		return list;
	}

	@Override
	public List<Post> findPostsByGrupo(Grupo g, boolean unicosDesteGrupo) {
//		if(unicosDesteGrupo) {
//			// Posts que tem apenas este grupo. Isso significa que ao deletar o Grupo precisa excluir o post.
//			String hql = "select distinct p from Post p inner join p.grupos g where g.id in (?) group by p having count(g) = 1";
//			List<Post> list = query(hql,false,g.getId());
//			return list;
//		} else {
			String hql = "select p from Post p inner join p.grupos g where g.id in (?)";
			List<Post> list = query(hql,false,g.getId());
			return list;
//		}
	}

	/**
	 * Chamado pelo LivecomJob.
	 */
	@Override
	public int publicar() {
		// Para fazer cache de 1 minuto (pois a HQL fica com a hora:minuto)
		Date now = getDateTrimMinute();

		// Todos Posts que precisam ser publicados (e ainda não foram expirados)
		String hql = "select id from Post where dataPublicacao <= ? and statusPublicacao <> ? and statusPublicacao <> ?";
		List<Long> ids = queryIds(hql,true,now,StatusPublicacao.PUBLICADO,StatusPublicacao.EXPIRADO);
		if(ids.isEmpty()) {
			return 0;
		}

		// update post set status_publicacao=1 where (status_publicacao is null or status_publicacao=0) and data_publicacao > now()
		Query q = createQuery("update Post set statusPublicacao = :publicado where id in (:ids)");
		q.setParameter("publicado", StatusPublicacao.PUBLICADO);
		q.setParameterList("ids", ids);
		int count = q.executeUpdate();
		return count;
	}

	@Override
	public int expirar() {
		// Para fazer cache de 1 minuto (pois a HQL fica com a hora:minuto)
		// Sera que podemos expirar por hora?
		Date now = getDateTrimMinute();

		// Para expirar
		String hql = "select id from Post where dataExpiracao is not null and dataExpiracao <= ? and statusPublicacao <> ?";
		List<Long> ids = queryIds(hql,true,now,StatusPublicacao.EXPIRADO);
		if(ids.isEmpty()) {
			return 0;
		}

		// Update to EXPIRADO
		Query q = createQuery("update Post set statusPublicacao = :expirado where id in (:ids)");
		q.setParameter("expirado", StatusPublicacao.EXPIRADO);
		q.setParameterList("ids", ids);
		int count = q.executeUpdate();
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PostUsuarios> findPostUsuarios(Usuario user, List<Post> posts) {
		StringBuffer sql = new StringBuffer("from PostUsuarios pu where pu.usuario = :user ");
		
		if(!posts.isEmpty()) {
			sql.append(" and pu.post in (:posts)");
		}
		
		Query q = createQuery(sql.toString());
		
		q.setParameter("user", user);
		
		if(!posts.isEmpty()) {
			q.setParameterList("posts", posts);
		}
		
		return q.list();
	}

	@Override
	public PostUsuarios getPostUsuarios(Usuario user, Post p) {
		String sql = "from PostUsuarios pu where pu.usuario = :user and pu.post = :post";
		Query q = createQuery(sql);
		
		q.setParameter("user", user);
		q.setParameter("post", p);
		
		if (q.list().size() > 0)
			return (PostUsuarios) q.list().get(0);
		else
			return null;
	}

	@Override
	public PostUsuarios getPostUsuarios(Long user, Long p) {
		String sql = "from PostUsuarios pu where pu.usuario.id = :user and pu.post.id = :post";
		Query q = createQuery(sql);
		
		q.setParameter("user", user);
		q.setParameter("post", p);
		
		if (q.list().size() > 0)
			return (PostUsuarios) q.list().get(0);
		else
			return null;
	}

	@Override
	public void saveOrUpdate(PostUsuarios postUsuarios) {
		getSession().saveOrUpdate(postUsuarios);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Arquivo> getArquivos(Post post) {
		
		if(post == null) {
			return new HashSet<Arquivo>();			
		}
		
		StringBuffer sql = new StringBuffer("from Arquivo a where a.post = :post order by a.ordem ");
		
		Query q = createQuery(sql.toString());
		
		q.setParameter("post", post);
		
		return new HashSet<Arquivo>(q.list());
	}

}
