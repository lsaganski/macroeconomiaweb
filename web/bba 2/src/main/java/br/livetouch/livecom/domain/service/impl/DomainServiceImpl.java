package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Formacao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.FormacaoRepository;
import br.livetouch.livecom.domain.repository.UsuarioRepository;
import br.livetouch.livecom.domain.service.DomainService;
import br.livetouch.livecom.domain.service.TagService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class DomainServiceImpl implements DomainService {
	protected static final Logger log = Log.getLogger(TagService.class);
	
	@Autowired
	private UsuarioRepository usuarioRep;
	
	@Autowired
	private FormacaoRepository formacaoRep;

	@Override
	public Usuario get(Long id) {
		return usuarioRep.get(id);
	}

	@Override
	public List<Formacao> findAllFormacao(Usuario userInfo) {
		return formacaoRep.findAllFormacao(userInfo);
	}

	@Override
	public void saveOrUpdate(Formacao f,Usuario userInfo) throws DomainException {
		f.setEmpresa(userInfo.getEmpresa());
		formacaoRep.saveOrUpdate(f);	
	}

	@Override
	public void delete(Formacao f) throws DomainException {
		formacaoRep.delete(f);
	}

	@Override
	public Formacao getFormacao(Long id) {
		return formacaoRep.get(id);
	}

}
