package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostTask;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.PostTaskRepository;
import br.livetouch.livecom.domain.service.PostTaskService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class PostTaskServiceImpl  extends LivecomService<PostTask> implements PostTaskService {
	@Autowired
	private PostTaskRepository rep;

	@Override
	public PostTask get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(PostTask c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<PostTask> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void delete(PostTask c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public List<PostTask> findAllByUser(Usuario u) {
		return rep.findAllByUser(u);
	}

	@Override
	public long getCount() {
		return rep.getCount();
	}

	@Override
	public PostTask findByUserPost(Usuario u, Post p) {
		return rep.findByUserPost(u, p);
	}

	@Override
	public List<PostTask> findAllByPost(Post post) {
		return rep.findAllByPost(post);
	}

}