package br.livetouch.livecom.rest.resource;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.PostDestaque;
import br.livetouch.livecom.domain.vo.FiltroLink;
import br.livetouch.livecom.domain.vo.PostDestaqueVO;

@Path("/v1/links")
public class LinksResource extends MainResource{
	
	protected static final Logger log = Log.getLogger(LinksResource.class);
	
	@GET
	public Response all(
			@QueryParam("site") String site,
			@QueryParam("userId") Long userId, 
			@QueryParam("texto") String titulo) {
		FiltroLink filtro = new FiltroLink();
		filtro.titulo = titulo;
		filtro.userId = userId;
		filtro.site = site;
		List<PostDestaque> all = postDestaqueRepository.findAllLinks(filtro);
		List<PostDestaqueVO> vo = PostDestaqueVO.fromList(all);
		return Response.ok().entity(vo).build();
	}
}
