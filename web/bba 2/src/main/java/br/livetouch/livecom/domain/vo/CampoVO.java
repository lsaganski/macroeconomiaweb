package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Campo;

public class CampoVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String nome;
	private String dependeCampo;
	private String dependeCampoValor;

	private boolean visivel;
	private boolean publico;
	private boolean editavel;
	private boolean obrigatorioCadastroAdmin;

	public void setCampo(Campo c) {
		setId(c.getId());
		setNome(c.getNome());
		setDependeCampo(c.getDependeCampo() != null ? c.getDependeCampo() : "");
		setDependeCampoValor(c.getDependeCampoValor() != null ? c.getDependeCampoValor() : "");
		setVisivel(c.getVisivel() == 1 ? true : false);
		setPublico(c.getPublico() == 1 ? true : false);
		setEditavel(c.getEditavel() == 1 ? true : false);
		setObrigatorioCadastroAdmin(c.getObrigatorioCadastroAdmin() == 1 ? true : false);
	}

	public static List<CampoVO> fromList(List<Campo> campos) {
		List<CampoVO> vos = new ArrayList<>();
		if (campos == null) {
			return vos;
		}
		for (Campo c : campos) {
			CampoVO campoVO = new CampoVO();
			campoVO.setCampo(c);
			vos.add(campoVO);
		}
		return vos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDependeCampo() {
		return dependeCampo;
	}

	public void setDependeCampo(String dependeCampo) {
		this.dependeCampo = dependeCampo;
	}

	public String getDependeCampoValor() {
		return dependeCampoValor;
	}

	public void setDependeCampoValor(String dependeCampoValor) {
		this.dependeCampoValor = dependeCampoValor;
	}

	public boolean isVisivel() {
		return visivel;
	}

	public void setVisivel(boolean visivel) {
		this.visivel = visivel;
	}

	public boolean isPublico() {
		return publico;
	}

	public void setPublico(boolean publico) {
		this.publico = publico;
	}

	public boolean isEditavel() {
		return editavel;
	}

	public void setEditavel(boolean editavel) {
		this.editavel = editavel;
	}

	public boolean isObrigatorioCadastroAdmin() {
		return obrigatorioCadastroAdmin;
	}

	public void setObrigatorioCadastroAdmin(boolean obrigatorioCadastroAdmin) {
		this.obrigatorioCadastroAdmin = obrigatorioCadastroAdmin;
	}

}
