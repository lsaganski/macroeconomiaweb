package br.livetouch.livecom.rest.resource;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Area;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.AreaVO;
import br.livetouch.livecom.domain.vo.FiltroArea;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/area")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class AreaResource extends MainResource {
	protected static final Logger log = Log.getLogger(AreaResource.class);
	
	@POST
	public Response create(Area area) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}

		Area.isFormValid(area, false);
		
		//Validar codigo
		Response messageResult = existe(area);
		if(messageResult != null){
			return messageResult;
		}
		
		Usuario userInfo = getUserInfo();
		areaService.saveOrUpdate(userInfo, area, getEmpresa());
		if(area.getDiretoria() != null) {
			area.setDiretoria(diretoriaService.get(area.getDiretoria().getId()));
		}
		return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", AreaVO.from(area))).build();
	}
	
	@PUT
	public Response update(Area area) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Area.isFormValid(area, true);
		
		//Validar codigo
		Response messageResult = existe(area);
		if(messageResult != null){
			return messageResult;
		}
		
		Usuario userInfo = getUserInfo();
		areaService.saveOrUpdate(userInfo, area, getEmpresa());
		if(area.getDiretoria() != null) {
			area.setDiretoria(diretoriaService.get(area.getDiretoria().getId()));
		}
		
		return Response.ok(MessageResult.ok("Área atualiza com sucesso", AreaVO.from(area))).build();
	}
	
	@GET
	public Response findAll() {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		List<Area> areas = areaService.findAll(getEmpresa());
		List<AreaVO> vos = AreaVO.fromList(areas);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Area area = areaService.get(id);
		if(area == null) {
			return Response.ok(MessageResult.error("Área não encontrada")).build();
		}
		AreaVO vo = new AreaVO(area);
		return Response.ok().entity(vo).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		try {
			Area area = areaService.get(id);
			if(area == null) {
				return Response.ok(MessageResult.error("Área não encontrada")).build();
			}
			Usuario userInfo = getUserInfo();
			areaService.delete(userInfo, area);
			return Response.ok().entity(MessageResult.ok("Área deletada com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error(e.getMessage())).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir a Area  " + id)).build();
		}
	}
	
	@GET
	@Path("/filtro")
	public Response findByName(@BeanParam FiltroArea filtro) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		List<Area> areas = areaService.filterArea(filtro, getEmpresa());
		List<AreaVO> vo = AreaVO.fromList(areas);
		return Response.status(Status.OK).entity(vo).build();
	}
	
	@GET
	@Path("/exportar")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response exportar() throws IOException {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Empresa empresa = getEmpresa();
		String csv = areaService.csvArea(empresa);
		
		String charset = ParametrosMap.getInstance(empresa).get(Params.EXPORTACAO_ARQUIVO_CHARSET, "ISO-8859-1");

		String attachament = "attachment; filename=" + URLEncoder.encode("areas.csv", "UTF-8");
		ResponseBuilder b = Response.status(Status.OK);
		b.encoding(charset);
		b.header("Content-Disposition", attachament);
		b.header("Content-Type", "application/octet-stream"+";charset="+charset);
		b.entity(csv);
		Response r = b.build();
		return r;
	}
	

	public Response existe(Area area) {
		
		if(areaService.findByName(area, getEmpresa()) != null) {
			log.debug("Ja existe uma area com este nome");
			return Response.ok(MessageResult.error("Ja existe uma area com este nome")).build();
		}
		if(areaService.findByCodigoValid(area, getEmpresa()) != null){
			log.debug("Ja existe uma permissão com este codigo");
			return Response.ok(MessageResult.error("Ja existe uma permissão com este codigo")).build();
			
		}

		return null;
	}
	
}
