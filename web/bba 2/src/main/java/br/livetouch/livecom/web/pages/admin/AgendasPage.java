package br.livetouch.livecom.web.pages.admin;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Agenda;
import br.livetouch.livecom.domain.Evento;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Table;
import net.sf.click.extras.control.LinkDecorator;

/**
 * 
 */
@Controller
@Scope("prototype")
public class AgendasPage extends LivecomPage {

	public Table table = new Table();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public Agenda agenda;
	public Evento evento;

	public List<Agenda> agendas;
	public Map<String, String> params = new HashMap<String, String>();
	
	@Override
	public void onInit() {
		super.onInit();

		table();

		if (id != null) {
			agenda = agendaService.get(id);
			params.put("id", String.valueOf(id));
		} else {
			agenda = new Agenda();
		}
		
		String eventoId = getParam("evento");
		if(eventoId != null && StringUtils.isNotEmpty(eventoId)) {
			evento = eventoService.get(Long.valueOf(eventoId));
			params.put("evento", eventoId);
			getContext().setSessionAttribute("eventoId", String.valueOf(evento.getId()));
		}
	}
	
	private void table() {
		Column c = null;
		
		c = new Column("titulo", getMessage("titulo.label"));
		c.setWidth("150px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("dataInicio", getMessage("dataInicio.label"));
		c.setFormat("{0,date,dd/MM/yyyy}"); 
		c.setWidth("20px");
		c.setTextAlign("center");
		table.addColumn(c);
		
		c = new Column("dataFim", getMessage("dataFim.label"));
		c.setFormat("{0,date,dd/MM/yyyy}"); 
		c.setWidth("20px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("evento.nome", getMessage("coluna.evento.label"));
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("palestrante.nome", getMessage("coluna.palestrante.label"));
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes", getMessage("detalhes.label"));
		c.setTextAlign("center");
		c.setWidth("100px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);
		table.setAttribute("class", "tableLists");

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean novo() {
		setRedirect(AgendasPage.class, params);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		params.put("id", String.valueOf(id));
		setRedirect(AgendaPage.class, params);
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Agenda e = agendaService.get(id);
			agendaService.delete(e);
			setRedirect(getClass());
			setFlashAttribute("msg", getMessage("msg.agenda.deletar.success"));
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.agenda.possuirelacionamento.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		if(evento != null) {
			agendas = evento.getAgendas();
		}

		// Count(*)
		int pageSize = 100;

		table.setPageSize(pageSize);
		table.setRowList(agendas);
	}
}
