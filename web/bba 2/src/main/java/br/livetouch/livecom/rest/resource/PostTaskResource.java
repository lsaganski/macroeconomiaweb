 package br.livetouch.livecom.rest.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostTask;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.PostTaskVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/postTask")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class PostTaskResource extends MainResource {
	protected static final Logger log = Log.getLogger(PostTaskResource.class);
	
	@POST
	public Response create(PostTask postTask) {
		
		if(postTask == null) {
			log.debug("Não foi possível agendar a tarefa para este post");
			return Response.ok(MessageResult.error("Não foi possível agendar a tarefa para este post")).build();
		}
		
		Post post = postTask.getPost();
		Usuario usuario = postTask.getUsuario();
		
		if(post == null || usuario == null) {
			log.debug("Não foi possível agendar a tarefa para este post");
			return Response.ok(MessageResult.error("Não foi possível agendar a tarefa para este post")).build();
		}
		
		PostTask pt = postTaskService.findByUserPost(usuario, post);
		if(pt != null) {
			postTask.setId(pt.getId());
		}
		
		try {
			postTaskService.saveOrUpdate(postTask);
			PostTaskVO postTaskVO = new PostTaskVO(postTask);
			return Response.ok(MessageResult.ok("Tarefa adicionada com sucesso", postTaskVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel agendar a tarefa para este post " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível agendar a tarefa para este post")).build();
		}
	}
	
	@PUT
	public Response update(PostTask postTask) {
		
		if(postTask == null) {
			log.debug("Não foi possivel agendar a tarefa para este post");
			return Response.ok(MessageResult.error("Não foi possivel agendar a tarefa para este post")).build();
		}
		
		Post post = postTask.getPost();
		Usuario usuario = postTask.getUsuario();
		
		if(post == null || usuario == null) {
			log.debug("Não foi possivel agendar a tarefa para este post");
			return Response.ok(MessageResult.error("Não foi possivel agendar a tarefa para este post")).build();
		}
		
		PostTask pt = postTaskService.findByUserPost(usuario, post);
		if(pt != null) {
			postTask.setId(pt.getId());
		}
		
		try {
			postTaskService.saveOrUpdate(postTask);
			PostTaskVO postTaskVO = new PostTaskVO(postTask);
			return Response.ok(MessageResult.ok("Tarefa adicionada com sucesso", postTaskVO)).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel agendar a tarefa para este post " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel agendar a tarefa para este post")).build();
		}
	}
	
	@GET
	public Response findAll() {
		List<PostTask> postTasks = postTaskService.findAll();
		List<PostTaskVO> vos = PostTaskVO.fromList(postTasks);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		PostTask postTask = postTaskService.get(id);
		if(postTask == null) {
			return Response.ok(MessageResult.error("Tarefa não encontrada")).build();
		}
		PostTaskVO postTaskVO = new PostTaskVO(postTask);
		return Response.ok().entity(postTaskVO).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		
		PostTask postTask = postTaskService.get(id);
		if(postTask == null) {
			return Response.ok(MessageResult.error("Tarefa não encontrada")).build();
		}
		try {
			postTaskService.delete(postTask);
			return Response.ok().entity(MessageResult.ok("Tarefa removida com sucesso")).build();
		} catch (DomainException e) {
			log.error("Não foi possível excluir a tarefa id: " + id + " ERRO:" + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível remover a tarefa  " + id)).build();
		}
	}
	
}
