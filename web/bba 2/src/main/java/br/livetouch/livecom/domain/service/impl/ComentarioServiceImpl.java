package br.livetouch.livecom.domain.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Likes;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.UsuarioMarcado;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.enums.GrupoNotification;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.repository.ComentarioRepository;
import br.livetouch.livecom.domain.repository.LikeRepository;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.ComentarioService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.UsuarioMarcadoService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.ComentarioVO;
import br.livetouch.livecom.domain.vo.UserToPushVO;
import br.livetouch.livecom.jobs.WorkDispatcherService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ComentarioServiceImpl extends LivecomService<Comentario> implements ComentarioService {
	protected static final Logger log = Log.getLogger(ComentarioService.class);

	@Autowired
	private ComentarioRepository rep;

	@Autowired
	private ArquivoService arquivoService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private UsuarioMarcadoService usuarioMarcadoService;

	@Autowired
	private PostService postService;

	@Autowired
	private LikeRepository likesRep;

	@Autowired
	private GrupoService grupoService;

	@Autowired
	protected NotificationService notificationService;
	
	@Autowired SessionFactory sessionFactory;
	
	@Autowired
	WorkDispatcherService workDispatcherService;

	@Override
	public Comentario get(Long id) {
		return rep.get(id);
	}

	@Override
	public Comentario comentar(Long id, Usuario u, Post post, String texto, List<Arquivo> arquivos, String usuariosMarcados) throws DomainException, IOException {
		// Comentario
		Boolean insert = false;
		Comentario c = id != null && id > 0 ? get(new Long(id)) : null;
		if (c == null) {
			c = new Comentario();
			insert = true;
		}
		c.setUsuario(u);
		c.setPost(post);
		c.setData(new Date());
		c.setComentario(texto);

		log.debug("Salvando comentario: " + c);

		// Comentario
		saveOrUpdate(c);

		auditSave(u, insert, c);

		// Marcar usuarios
		marcarUsuarios(c, usuariosMarcados);

		// inc++ qtde comentario
		post.comment();
		postService.saveOrUpdate(null, post);

		// Arquivos
		if (arquivos != null) {
			for (Arquivo arquivo : arquivos) {
				arquivo.setComentario(c);
				arquivoService.saveOrUpdate(arquivo);
			}
			c.setArquivos(new HashSet<Arquivo>(arquivos));
		}

		// Push
		push(c, insert);

		return c;
	}

	protected void auditSave(Usuario u, Boolean insert, Comentario c) {
		// Audit
		AuditoriaAcao acao = insert ? AuditoriaAcao.INSERIR : AuditoriaAcao.EDITAR;
		String msg = null;
		if (insert) {
			msg = "Usuário " + u.getNome() + " comentou: " + c.getComentario();
		} else {
			msg = "Usuário " + u.getNome() + " editou um comentário: " + c.getComentario();
		}
		LogAuditoria.log(u, c, AuditoriaEntidade.COMENTARIO, acao, msg);
	}

	private void push(Comentario c, boolean insert) throws DomainException, IOException {
		Post post = c.getPost();
		Usuario u = c.getUsuario();
		Empresa e = u.getEmpresa();

		ParametrosMap params = ParametrosMap.getInstance(e);
		boolean pushOn = params.isPushOn();
		boolean pushComentarioOn = "1".equals(params.get(Params.PUSH_ON_COMENTAR, "1"));
		boolean isSendPush = pushOn && pushComentarioOn;
		
		// Notification
		String texto = null;
		String tituloNot = null;
		String subTituloNot = null;
		String textoNotificacao = null;
		if (insert) {
			texto = params.get(Params.MSG_PUSH_COMENTARIO, "[%user%] comentou o post [%titulo%]");
			tituloNot = params.get(Params.PUSH_COMENTARIO_TITULO_NOTIFICATION, "Novo Comentário");
			subTituloNot = params.get(Params.PUSH_COMENTARIO_SUB_TITULO_NOTIFICATION, "[%user%] comentou o post [%titulo%]");
			textoNotificacao = u.getNome() + " comentou na publicação ";
			textoNotificacao += post.getTitulo() != null ? post.getTitulo() : "";
			
			
		} else {
			texto = params.get(Params.MSG_PUSH_UPDATE_COMENTARIO, "[%user%] editou o comentario no post [%titulo%]");
			tituloNot = params.get(Params.PUSH_COMENTARIO_UPDATE_TITULO_NOTIFICATION, "Comentário Editado");
			subTituloNot = params.get(Params.PUSH_COMENTARIO_SUB_UPDATE_TITULO_NOTIFICATION, "[%user%] editou o comentario no post [%titulo%]");
			textoNotificacao = u.getNome() + " editou o comentario no post ";
			textoNotificacao += post.getTitulo() != null ? post.getTitulo() : "";
		}
		
		
		if (StringUtils.isNotEmpty(texto)) {
			texto = StringUtils.replace(texto, "%user%", u.getNome());
			texto = StringUtils.replace(texto, "%titulo%", post.getTituloDesc());
		}

		if (StringUtils.isNotEmpty(subTituloNot)) {
			subTituloNot = StringUtils.replace(subTituloNot, "%user%", u.getNome());
			subTituloNot = StringUtils.replace(subTituloNot, "%titulo%", post.getTituloDesc());
		}

		Notification n = new Notification();
		n.setData(new Date());
		n.setDataPublicacao(c.getData());
		n.setTipo(TipoNotificacao.COMENTARIO);
		n.setGrupoNotification(GrupoNotification.MURAL);
		n.setTitulo(post.getTitulo());
		n.setTexto(texto);
		n.setTituloNot(tituloNot);
		n.setSubTituloNot(subTituloNot);
		n.setTextoNotificacao(textoNotificacao);
		n.setPost(post);
		n.setComentario(c);
		n.setUsuario(u);
		n.setSendPush(isSendPush);
		notificationService.save(n);
		
		List<UserToPushVO> usersToPush = getUsuariosPushComentario(u, post);

		// Salva todas as notifications: InsertNotificationsWork
		if(usersToPush.size() > 0) {
			workDispatcherService.insertNotificationUsuariosWork(n, usersToPush, null, true);
		}
	}

	/**
	 * push.comentario.mode = 1: Todos que comentaram e dono do post.
	 * push.comentario.mode = 2: Todos do grupo.
	 */
	public List<UserToPushVO> getUsuariosPushComentario(Usuario userInfo, Post post) {
		ParametrosMap params = ParametrosMap.getInstance(userInfo.getEmpresa());
		String mode = params.get(Params.PUSH_COMENTARIO_MODE, "1");
		boolean autorReceivePush = params.getBoolean(Params.PUSH_COMENTARIO_AUTOR_RECEBE_ON, false);
		
		Map<Long, UserToPushVO> map = new HashMap<Long, UserToPushVO>();

		if(post.getGrupos().isEmpty()) {
			mode = "1";
			Usuario usuario = post.getUsuario();
			Perfil permissao = usuario.getPermissao();
			if(permissao != null && Livecom.getInstance().hasPermissao(ParamsPermissao.VIZUALIZAR_COMENTARIOS, usuario)) {
				map.put(usuario.getId(), new UserToPushVO(usuario.getId(),usuario.getLogin(),usuario.getEmail(), false, false));
			}
		}
		
		if ("2".equals(mode)) {
			/**
			 * Vai para todos do grupo
			 */
			Set<Grupo> grupos = new HashSet<Grupo>();
			grupos.addAll(post.getGrupos());
			List<Grupo> subgrupos = grupoService.findSubgruposInGrupos(post.getGrupos());
			if(subgrupos != null && subgrupos.size() > 0) {
				grupos.addAll(subgrupos);
			}
			for (Grupo g : grupos) {
				List<UserToPushVO> users = grupoService.findUsers(g, post, TipoNotificacao.COMENTARIO);
				for (UserToPushVO usuario : users) {
					Long userId = usuario.getId();
					if (!userId.equals(userInfo.getId()) || autorReceivePush) {
						map.put(userId, usuario);
					}
				}
			}
		} else {

			/**
			 * Todos que comentaram + dono do post
			 */
			List<UserToPushVO> users = findAllUsersQueComentaramPost(post, userInfo);
			for (UserToPushVO usuario : users) {
				Long userId = usuario.getId();

				if (!userId.equals(userInfo.getId())) {
					map.put(userId, usuario);
				} else if(autorReceivePush) {
					map.put(userId, usuario);
				}
			}
		}

		return new ArrayList<>(map.values());
	}

	private void marcarUsuarios(Comentario c, String usuariosMarcados) throws DomainException {
		String[] split = StringUtils.split(usuariosMarcados, ",");
		List<UsuarioMarcado> usuariosMarcado = new ArrayList<>();
		List<Long> idsMarcados = new ArrayList<>();
		if (split != null && split.length > 0) {

			for (String s : split) {
				idsMarcados.add(Long.parseLong(s));
			}

			List<Usuario> usuarios = usuarioService.findAllByIds(idsMarcados);
			if (usuarios != null && usuarios.size() > 0) {
				for (Usuario usuario : usuarios) {
					UsuarioMarcado usuarioMarcado = usuarioMarcadoService.find(c, usuario);
					if (usuarioMarcado == null) {
						usuarioMarcado = new UsuarioMarcado();
						usuarioMarcado.setUsuario(usuario);
						usuarioMarcado.setComentario(c);
						usuarioMarcadoService.saveOrUpdate(usuarioMarcado);
					}
					usuariosMarcado.add(usuarioMarcado);
				}
			}
		}

		if (idsMarcados.size() == 0) {
			idsMarcados.add(0L);
		}
		c.setUsuariosMarcado(usuariosMarcado);

		usuarioMarcadoService.delete(idsMarcados, c);
	}

	@Override
	public void saveOrUpdate(Comentario c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Comentario> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void delete(Usuario userInfo, Comentario c,boolean deleteArquivosAmazon) throws DomainException {

		if (userInfo != null) {
			String msg = "Usuário " + userInfo.getNome() + " deletou um comentário " + c.getComentario();
			LogAuditoria.log(userInfo, c, AuditoriaEntidade.COMENTARIO, AuditoriaAcao.DELETAR, msg);
		}

		List<Long> ids = new ArrayList<>();
		ids.add(c.getId());
		delete(userInfo, ids,deleteArquivosAmazon);
	}

	@Override
	public void delete(Usuario userInfo, List<Long> ids,boolean deleteArquivosAmazon) throws DomainException {
		if (ids == null || ids.isEmpty()) {
			return;
		}

		Comentario c = null;

		try {
			// Em Batch
			executeIn("delete from PushReport where comentario.id in (:ids)","ids", ids);
			executeIn("delete from UsuarioMarcado where comentario.id in (:ids)","ids", ids);
			executeIn("delete from Likes where comentario.id in (:ids)","ids", ids);
			
			// Nofications
			notificationService.deleteBy(Comentario.class,ids);
			
			// Arquivos
			List<Long> idsArquivos = queryIdsIn("select id from Arquivo where comentario.id in (:ids)",false,"ids", ids);
			arquivoService.delete(userInfo, idsArquivos, true);
			
			// Delete
			executeIn("delete Comentario where id in (:ids)","ids", ids);
			
		} catch (HibernateOptimisticLockingFailureException e) {
			// ok
			e.printStackTrace();
		} catch (Exception e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if (root == null) {
				root = e;
			}
			log.error("Erro ao excluir o comentário [" + c + "]: " + root.getMessage(), root);

			throw new DomainException("Não foi possível excluir o comentário [" + c + "]", e);
		}
	}

	@Override
	public List<Comentario> findAllByUserAndPost(long userId, long postId, int page, int maxRows) {
		return rep.findAllByUserAndPost(userId, postId, page, maxRows);
	}

	@Override
	public List<ComentarioVO> toListVo(Usuario user, List<Comentario> list) {
		Collections.reverse(list);

		List<ComentarioVO> comentarios = new ArrayList<ComentarioVO>();
		for (Comentario c : list) {
			ComentarioVO vo = new ComentarioVO();
			vo.setComentario(c);

			Long count = likesRep.countByComentario(c);// c.getLikeCount();
			vo.likeCount = count;

			Likes l = likesRep.findLikeByUserAndComentario(user, c);
			vo.like = (l != null && l.isFavorito()) ? "1" : "0";

			comentarios.add(vo);
		}
		return comentarios;
	}

	@Override
	public List<Comentario> findAllComentariosNotificationByUser(Usuario user) {
		if (user == null) {
			return new ArrayList<Comentario>();
		}
		return rep.findAllComentariosNotificationByUser(user);
	}

	@Override
	public void clearNotification(Long id) throws DomainException {
		Comentario f = get(id);
		f.setLidaNotification(true);
		saveOrUpdate(f);
	}

	@Override
	public List<Long> findAllIdsByUser(Usuario u) {
		return rep.findAllIdsByUser(u);
	}

	@Override
	public List<Long> findAllByPost(Post p) {
		return rep.findAllByPost(p);
	}
	
	@Override
	public List<Comentario> findByPost(Post p) {
		return rep.findByPost(p);
	}

	@Override
	public List<Usuario> findAllUsuariosQueComentaramPost(Post p, Usuario u) {
		return rep.findAllUsuariosQueComentaramPost(p, u);
	}

	@Override
	public List<UserToPushVO> findAllUsersQueComentaramPost(Post p, Usuario u) {
		return rep.findAllUsersQueComentaramPost(p, u);
	}

	@Override
	public List<Comentario> findAllByDate(Date data) {
		return rep.findAllByDate(data);
	}
}
