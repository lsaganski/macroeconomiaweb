package br.livetouch.livecom.web.pages.cadastro;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.root.RootPage;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;

@Controller
@Scope("prototype")
public class MobilePage extends RootPage {

	
	public ActionLink deleteCustomer = new ActionLink("delete", getMessage("excluir.label"), this, "onDeleteClick");
	public ActionLink editarParam = new ActionLink("editar", "Editar", this, "onEditarClick");
	
	public String tab;
	public Parametro parametro;
	
	public Usuario adminEmpresa;
	public Empresa empresa;
	
	/// configuracoes corretas
	public boolean loginAutomatico;
	public boolean permiteLogout;
	public boolean podePostar;
	public boolean chatOn;
	public boolean showWhatsNews;
	public boolean showAudienciaPost;
	public boolean optionsMenu;
	public boolean menuCategoria;
	public String corMenu;
	public boolean loginOffline;
	public boolean loginByPass;

	@Override
	public void onGet() {
		super.onGet();
        Long empresaId = getUserInfoVO().getEmpresaId();
        empresa = empresaService.get(empresaId);
        empresaService.setParametrosDefaultEmpresa(empresa);

        ParametrosMap param = ParametrosMap.getInstance(empresaId);

        loginAutomatico = param.getBoolean(Params.APP_LOGIN_AUTOMATICO, true);
        permiteLogout = param.getBoolean(Params.APP_PERMITE_LOGOUT, true);
        podePostar = param.getBoolean(Params.APP_PODE_POSTAR, true);
        chatOn = param.getBoolean(Params.APP_CHAT_ON, true);
        showWhatsNews = param.getBoolean(Params.APP_SHOW_WHATS_NEWS, true);
        showAudienciaPost = param.getBoolean(Params.APP_SHOW_AUDIENCIA_POST, true);
        optionsMenu = param.getBoolean(Params.APP_OPTIONS_MENU, true);
        menuCategoria = param.getBoolean(Params.APP_MENU_CATEGORIA, false);
        loginOffline = param.getBoolean(Params.APP_LOGIN_OFFLINE, false);
        loginByPass = param.getBoolean(Params.APP_BY_PASS_LOGIN, false);
        adminEmpresa = empresa.getAdmin();
        corMenu = param.get(Params.COR_MENU, "#06377b");
	}
	
	public boolean onDeleteClick() throws DomainException {
		if(StringUtils.isNotEmpty(deleteCustomer.getValue())&& StringUtils.isNumeric(deleteCustomer.getValue())) {
			long id = deleteCustomer.getValueLong();
		    Parametro parametro =  parametroService.get(id);
		    if (parametro != null) {
		    	parametroService.delete(parametro);
		    	setMessageSesssion("Parâmetro deletado com sucesso");
		    	
		    }
		}
		setRedirect(MobilePage.class, "tab", "parametros");
	    return true;
	}
	
	public boolean onEditarClick() throws DomainException {
		 long id = editarParam.getValueLong();
	     parametro =  parametroService.get(id);
	     return true;
	}
	
}
