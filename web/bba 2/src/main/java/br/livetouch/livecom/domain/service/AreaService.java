package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Area;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.FiltroArea;
import net.livetouch.tiger.ddd.DomainException;

public interface AreaService extends Service<Area> {

	List<Area> findAll(Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Usuario userInfo, Area f, Empresa empresa) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo,Area f) throws DomainException;

	List<Object[]> findIdCodAreas(Long idEmpresa);

	List<Area> filterArea(FiltroArea filtro, Empresa empresa);

	Area findDefaultByEmpresa(Long empresaId);

	String csvArea(Empresa empresa);

	List<Object[]> findIdNomeAreas(Long empresaId);

	Area findByName(Area area, Empresa empresa);
	
	Area findByCodigoValid(Area area, Empresa empresa);

}
