package br.livetouch.livecom.domain.exception;

import net.livetouch.tiger.ddd.DomainException;

public class HorarioGrupoException extends DomainException{
	private static final long serialVersionUID = 3579513859913195559L;

	public HorarioGrupoException(String message) {
		super(message);
	}
}
