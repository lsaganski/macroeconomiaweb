package br.livetouch.livecom.web.pages.ws;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.util.TempExceptionUtils;
import br.infra.util.Utils;
import br.livetouch.infra.util.XStreamUtil;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogTransacao;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusTransacao;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.exception.HorarioGrupoException;
import br.livetouch.livecom.domain.exception.HorarioUsuarioException;
import br.livetouch.livecom.domain.exception.SenhaInvalidaException;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.jobs.info.LogTransacaoJob;
import br.livetouch.livecom.security.LivecomSecurity;
import br.livetouch.livecom.security.UserAgentUtil;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import net.sf.click.control.Field;
import net.sf.click.control.Form;

/**
 * Pagina para o Mobile, retorna um XML ou JSON
 * 
 * @author ricardo
 * 
 */
@Controller
@Scope("prototype")
public abstract class WebServiceXmlJsonPage extends LivecomLogadoPage {

	private static Logger logger = Logger.getLogger(WebServiceXmlJsonPage.class);
	public String mode;
	public String wstoken;
	public int wsVersion;

	protected static final String WSVERSION_DEFAULT = "3";
	private static final String WS_CRYPT_ON = "WS_CRYPT_ON";
	
	@Autowired
	UsuarioService service;

	@Override
	public boolean onSecurityCheck() {
		boolean isGet = getContext().isGet();
		if(isGet) {

			boolean localhost = isLocalhost();
			if(!localhost) {
				Empresa e = getEmpresa();
				
				boolean ok = LivecomSecurity.isWsGetAllowed(e);
				if(!ok) {
					notFound();
					return false;
				}
				
				ok = LivecomSecurity.validate(getContext(),e);
				if(!ok) {
					notFound();
					return false;
				}
			}
		} else {
			try {
				boolean ok;
				if (isWsTokenOn()) {
					UserInfoVO userInfo = getUserInfoVO();
					String so = UserAgentUtil.getUserAgentSO(getContext());
					ok = LivecomSecurity.validateWsToken(userInfo, so, wstoken, tokenService);
					if(!ok) {
						forbidden();
						return false;
					}
				}
				
				if(isOTPOn()) {
					String otp = getParam("wsotp");
					ok = LivecomSecurity.validateOTP(otp, wstoken);
					if(!ok) {
						forbidden();
						return false;
					}
				}
			} catch (DomainMessageException ex) {
				logger.error(ex.getMessage(), ex);
				forbidden();
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void onInit() {
		super.onInit();
	}

	@Override
	public String getTemplate() {
		if (isExecuteWebService()) {
			return getPath();
		}
		return getPath();
	}

	@Override
	public void onPost() {
		super.onPost();
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	/**
	 * @see net.sf.click.Page#onRender()
	 */
	@Override
	public void onRender() {
		super.onRender();
		if (isExecuteWebService()) {
			executeWebService();
		}
	}

	public boolean isExecuteWebService() {
		return getContext().isPost();
	}
	
	protected boolean validateSecurity() {
		return super.validateSecurity();
	}

	public void executeWebService() {
		HttpServletResponse response = getContext().getResponse();
		response.setContentType("text/plain; charset=UTF-8");

		response.setHeader("Pragma", "no-cache");

		LogTransacao log = new LogTransacao();
		log.setDataInicio(new Date());
		log.setEmpresa(getEmpresa());

		PrintWriter writer = null;

		try {
			
			Object obj = null;
			
			boolean securityOk = validateSecurity();
			if(securityOk) {
				obj = templateExecute(log);
			} else {
				obj = new MensagemResult("ERROR", "Not Found");
			}

			if(obj instanceof MensagemResult) {
				MensagemResult r = (MensagemResult) obj;
				if(r.isError()) {
					log.setStatus(StatusTransacao.ERRO);
				}
			}
			
			// User
			Usuario user = getUsuario();
		
			log.setTransacaoRequest(getContext().getRequest(), user,getHost() + getPath(), getRequestParams());

			boolean showXmlOuJson = isXmlJson() && !isHtml();

			if (showXmlOuJson) {
				writer = response.getWriter();

				setPath(null);

				String json = toXmlJson(obj);

				if (StringUtils.isEmpty(json)) {
					json = toXmlJsonMessage("Retorne algo");
				}

				log.setXml(json);

				if(isWsCryptOn()) {
					json = LivecomSecurity.encryptJson(json, getCryptKey());
					if(StringUtils.isEmpty(json)) {
						json = toXmlJson(unauthorized(wsVersion));
					} else {
						response.setHeader(WS_CRYPT_ON, "1");
					}
				}

				response.setContentType(getContentType());

				writer.print(json);
				writer.flush();
				writer.close();

				printXmlJson(json);
				
				if (logger.isTraceEnabled()) {
					log("> " + log.getLogin() + ":" + log.getRequestPath() + "?" + log.getRequestParams());
				}
			}

		} catch (Throwable e) {
			log.setException(e);
			logError("executeWebService(): Ocorreu um erro na transação [" + getPath() + ", params:[" + getRequestParams() + "], por favor tente novamente mais tarde: " + e.getMessage(), e);
			if (writer != null) {
				writer.print("Ocorreu um erro na transação, por favor tente novamente mais tarde.");
			}
		} finally {
			saveLogTransacao(log);
		}
	}

	protected boolean isWsCryptOn() {
		return LivecomSecurity.isWsCryptOn(getEmpresa(),wsVersion);
	}

	private void saveLogTransacao(LogTransacao log) {
		boolean saveLogTransacao = isSaveLogTransacao();
		if (saveLogTransacao) {
			
			log.finalizar();
			if (logService != null) {
				try {
					LogTransacaoJob.getInstance().addLogTransacao(log);
//						logService.saveOrUpdate(log);
				} catch (Throwable e) {

					if (e instanceof NullPointerException) {
						// logError("Erro ao salvar o log: " +
						// e.getMessage() + " - " + e.getClass(), e);
					} else {
						logError("Erro ao salvar o log para ["+getPath()+"]: " + e.getMessage() + " - " + e.getClass(), e);
					}
				}
			}
		}
	}
	
	/**
	 * Este método serve para outros ws fazerem override
	 * @return
	 */
	protected boolean isWsTokenOn() {
		boolean b = LivecomSecurity.isWsTokenOn(getEmpresa(),wsVersion);
		return b;
	}

	/**
	 * Este método serve para outros ws fazerem override
	 * @return
	 */
	protected boolean isOTPOn() {
		if(UserAgentUtil.isMobile(getContext())) {
			boolean b = LivecomSecurity.isOTPOn(getEmpresa());
			return b;
		}
		return false;
	}

	private Object templateExecute(LogTransacao log) {

		try {

			validaHorario(getUserInfoVO());

			Object o = null;

			o = execute();

			return o;
		} catch(HorarioGrupoException e) {
			log.setException(e);
			return MensagemResult.erro(e.getMessage(), "horario");
		} catch(HorarioUsuarioException e) {
			log.setException(e);
			return MensagemResult.erro(e.getMessage(), "horario");
		} catch(SenhaInvalidaException e) {
			log.setException(e);
			return MensagemResult.erro(e.getMessage(), "senha");
		} catch (DomainMessageException e) {
			log.setException(e);
			return MensagemResult.erro(e.getMessage());
		} catch (DataIntegrityViolationException e) {
			log.setException(e);
			if(TempExceptionUtils.isErrorTruncate(e)) {
				return new MensagemResult("Exception","Não foi possível salvar o registro (tamanho excedido).");
			} else if(TempExceptionUtils.isErrorContraint(e)) {
				return new MensagemResult("Exception","Não foi possível excluir este registro porque ele possui relacionamentos.");
			}
			
			return new MensagemResult("Exception","Não foi possível salvar o registro.");
		} catch (Throwable e) {
			log.setException(e);

			logError("Erro ao executar o web service: " + e.getMessage(), e);
			boolean devMode = ParametrosMap.getInstance(getEmpresaId()).isDevModeOn();
			if(devMode) {
				return new MensagemResult("Exception", e.getMessage() + " - " + e.getClass());
			} else {
				return new MensagemResult("Exception","Ocorreu um erro ao fazer a consulta");
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void printParams() {
		Enumeration<String> enuma = getContext().getRequest().getHeaderNames();
		while (enuma.hasMoreElements()) {
			String string = (String) enuma.nextElement();
			System.out.println(string);
			System.out.println("header ("+string+"): " + getContext().getRequest().getHeader(string));
		}
		
		Map map = getContext().getRequest().getParameterMap();
		Set<String> keys = map.keySet();
		for (String key : keys) {
			System.out.println("req ("+key+"): " + getContext().getRequest().getParameter(key));
		}
	}
	public boolean isSaveLogTransacao() {
		ParametrosMap params = ParametrosMap.getInstance(getEmpresaId());
		boolean b = params.getBoolean(Params.INFRA_SAVE_LOG_TRANSACAO_WS, true);
		boolean saveLogTransacaoUserAgentWeb = params.getBoolean(Params.INFRA_SAVE_LOG_TRANSACAO_WEB_WS, false);
		if(!b) {
			return false;
		}
		boolean web = UserAgentUtil.isWeb(getContext());
		if(!saveLogTransacaoUserAgentWeb && web) {
			// Se for user-agent web e nao é para salvar da web.
			return false;
		}
		return true;
	}

	protected void printXmlJson(String xml) {
		// System.out.println(xml);
		// log(xml);
	}

	protected boolean isHtml() {
		return false;
	}

	protected boolean isValidaHorario() {
		String onGrupos = ParametrosMap.getInstance(getEmpresaId()).get(Params.GRUPO_RESTRICAO_HORARIO_ON, "0");
		String onUsuarios = ParametrosMap.getInstance(getEmpresaId()).get(Params.USUARIO_RESTRICAO_HORARIO_ON, "0");
		if("1".equals(onGrupos) || "1".equals(onUsuarios)) {
			return true;
		}
		return false;
	}
	
	protected void validaHorario(UserInfoVO userInfo) throws HorarioGrupoException, DomainMessageException, HorarioUsuarioException {
		if(userInfo == null) {
			return;
		}
		
		validaHorario(userInfo.getId());
	}
	
	
	protected void validaHorario(Long id) throws HorarioGrupoException, DomainMessageException, HorarioUsuarioException {
		if(id == null) {
			return;
		}
		
		if(!isValidaHorario()) {
			return;
		}
		
		Usuario usuario = usuarioService.get(id);
		if(usuario != null && !usuario.isAdmin()) {
			
			String modoRestricao = ParametrosMap.getInstance(getEmpresaId()).get(Params.RESTRICAO_HORARIO_MODO,"");
			
			if(modoRestricao.equals("grupo")) {
				boolean isGrupoDentroHorario = grupoService.isGruposDentroHorario(usuario);

				if(!isGrupoDentroHorario) {
					String msgGrupo = ParametrosMap.getInstance(usuario.getEmpresa().getId()).get("grupo.fora.horario.msg", "O sistema não é acessível fora do seu horário de trabalho.");
					throw new HorarioGrupoException(msgGrupo);
				}
			} else {
				boolean isUsuarioDentroHorario = true;
				if("1".equals(ParametrosMap.getInstance(getEmpresaId()).get(Params.USUARIO_RESTRICAO_HORARIO_ON,"0"))) {
					isUsuarioDentroHorario = usuarioService.isUsuarioDentroHorario(usuario) && Utils.isExpedienteValido(usuario.getExpediente());
				} 
				
				if(usuario != null && !isUsuarioDentroHorario) {
					String msgUsuario = ParametrosMap.getInstance(getEmpresaId()).get(Params.MSG_HORARIO_USUARIO,"O sistema não é acessível fora do seu horário de trabalho.");
					throw new HorarioUsuarioException(msgUsuario);
				}
			}
		}
	}
	
	

	/**
	 * Utilize o isXml() e isJson() para saber o tipo do conteúdo par retornar
	 * 
	 * @throws IOException
	 */
	protected abstract Object execute() throws Exception;

	protected String toXmlJson(Object object) {
		try {
			if (object != null) {
				
				if(isJson() && isResponseGSON()) {
					boolean web = UserAgentUtil.isWeb(getContext());
					String json = br.livetouch.livecom.utils.JSONUtils.toJSON(object, web);
					return json;
				}

				boolean formatJson = isWebRequest();
				String jsonFormatParam = getParam("json.format");
				if (jsonFormatParam != null) {
					formatJson = "1".equals(jsonFormatParam);
				}

				XStream x = XStreamUtil.getXStream(isJson(), true);
				// XStream x = new XStream(new JettisonMappedXmlDriver(){});
				x.alias("mensagem", MensagemResult.class);
				xstream(x);
				String xml = x.toXML(object);

				if (!formatJson) {
					xml = xml.replaceAll("\n\\s+", "" + "");
				}

				if (StringUtils.isNotEmpty(xml)) {
					xml = xml.trim();
					xml = xml.replaceAll("&amp", "");
					if (isXml()) {
						String header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

						xml = header + "\n" + xml;
					}
				}

				return xml;
			}
		} catch (Exception e) {
			logError(e.getMessage(), e);
		}
		return toXmlJsonMessage("Informe um objeto para gerar o XML.");
	}

	protected boolean isResponseGSON() {
		return isWsVersion3();
	}
	
	/**
	 * GSON
	 * 
	 * @return
	 */
	protected boolean isWsVersion3() {
		return wsVersion > 2;
	}
	
	/**
	 * Crypt
	 * 
	 * @return
	 */
	protected boolean isWsVersion4() {
		return wsVersion >= 4;
	}

	/**
	 * Coloque mapeamentos "alias" adicionais aqui, exemplo:
	 * 
	 * <pre>
	 * x.alias(&quot;mensagem&quot;, Mensagem.class);
	 * x.alias(&quot;aluno&quot;, Aluno.class);
	 * </pre>
	 * 
	 * @param x
	 */
	protected void xstream(XStream x) {
		x.alias("mensagem", MensagemStatus.class);
	}

	protected String toXmlJsonMessage(String s) {
		MensagemStatus msg = new MensagemStatus(s, null);
		String xml = toXmlJson(msg);
		return xml;
	}

	protected boolean isXmlJson() {
		boolean isXml = isXml();
		boolean isJson = isJson();
		return isXml || isJson;
	}

	@Override
	public String getContentType() {
		if (isXml()) {
			return "text/xml";
		} else if (isJson()) {
			return "application/json; charset=UTF-8";
		} /*
		 * else if (isTxt()) { return "text/plain"; }
		 */
		return "text/html";
	}

	protected boolean isJson() {
		boolean postGson =  isResponseGSON() && getContext().isPost(); 
		return "json".equalsIgnoreCase(mode) || postGson;
	}

	protected boolean isXml() {
		return "xml".equalsIgnoreCase(mode);
	}

	protected boolean isTxt() {
		return "txt".equalsIgnoreCase(mode);
	}

	@SuppressWarnings("unchecked")
	protected Object getFormErrorMensagemResult(Form form) {
		StringBuffer sb = new StringBuffer();
		if (StringUtils.isNotEmpty(form.getError())) {
			sb.append("form error: " + form.getError());
		}
		List<Field> errorFields = form.getErrorFields();
		if (errorFields != null) {
			for (Field f : errorFields) {
				sb.append("field " + f.getName() + ": " + f.getError()).append("\n");
			}
		}
		return new MensagemResult("ERROR", "Form invalido: " + sb.toString());
	}

	protected long getLong(Long l) {
		return l == null ? 0 : l;
	}

	protected boolean isWebRequest() {
		return !isMobileRequest();
	}

	protected boolean isMobileRequest() {
		boolean android = "android".equalsIgnoreCase(getContext().getRequestParameter("device.so"));
		boolean ios = "ios".equalsIgnoreCase(getContext().getRequestParameter("device.so"));
		return android || ios;
	}

	@Override
	protected void initWeb() {
		// nada
	}
	
	protected String getCryptKey() {
		return wstoken;
	}
	
	public String getRequestPath() {
		return getContext().getRequest().getRequestURI();
	}

	protected String getRequestKey() {
		HttpServletRequest request = getContext().getRequest();

		StringBuffer sb = null;
		Map<String, String[]> parameterMap = request.getParameterMap();
		for (String key : parameterMap.keySet()) {
			if (sb != null) {
				sb.append("&");
			} else {
				sb = new StringBuffer();
			}
			String value = request.getParameter(key);
			if(!"form_name".equals(key) && !"mode".equals(key)
					 && !"wsVersion".equals(key)  && !"wstoken".equals(key))
			 {
				sb.append(key).append("=").append(value);
			 }
		}

		// String s = null;
		// s.length();
		if(sb != null) {
			return sb.toString();
		}
		return "";
	}
	
	public boolean isWordPress() {
		return wordpress != null && wordpress.isWordpressOn();
	}
	
	protected void log(String string) {
		String host = getHost();
		logger.debug(host + ": " + string);
	}
	
	protected void logError(Object message, Throwable t) {
		String host = getHost();
		logger.error(host + ": " + message, t);
	}
	
	protected void logError(Object message) {
		String host = getHost();
		logger.error(host + ": " + message);
	}
}
