package br.livetouch.livecom.domain.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.StatusMensagemUsuario;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.StatusMensagemUsuarioRepository;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.domain.service.StatusMensagemUsuarioService;
import net.livetouch.extras.util.ExceptionUtil;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class StatusMensagemUsuarioServiceImpl implements StatusMensagemUsuarioService {
	@Autowired
	private StatusMensagemUsuarioRepository rep;

	@Autowired
	private LogService logService;
	
	@Override
	public StatusMensagemUsuario get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Usuario userInfo, StatusMensagemUsuario status)
			throws DomainException {
		try {
			Validate.notNull(status);
			Validate.notNull(status.getUsuario());
			Validate.notNull(status.getConversa());
			Validate.notNull(status.getMensagem());


			boolean create = status.getId() == null;

			
			auditSave(userInfo, status, create);
			
		} catch (Exception e) {
			// Qualquer erro decorrente desta rotina deverá ser gravado no “Log
			// do Sistema”.
			LogSistema l = LogSistema.logError(userInfo, ExceptionUtil
					.getStackTrace(e));
			logService.saveOrUpdateNewTransaction(l, userInfo.getEmpresa());
			throw new RuntimeException(e);
		}
	}
	
	protected void auditSave(Usuario userInfo, StatusMensagemUsuario t, boolean create) {
		if (t.isExcluida() && t.getDataExcluida() == null) {
			t.setDataExcluida(new Date());
		}
		
		rep.saveOrUpdate(t);
		System.out.println("Salvou ["+t.getUsuario().getLogin()+"] > " + t.isLida() + " > " + t);

		if (userInfo != null) {
			// audit
			//StringBuffer sb = new StringBuffer("Grupo " + (create ? "criado" : "atualizado") + ": " + t.toStringAll());
			//String msgAudit = StringUtils.abbreviate(sb.toString(), 1000);
			//GrupoAuditoria a = GrupoAuditoria.audit(t, userInfo, msgAudit, 1);
			//rep.saveOrUpdate(a);
		}
	}

	@Override
	public void delete(Usuario userInfo, StatusMensagemUsuario g) throws DomainException {	
		
		g.setConversa(null);
		g.setMensagem(null);
		g.setUsuario(null);
		
		rep.delete(g);
	}

	@Override
	public List<StatusMensagemUsuario> findAllByUserConversa(Usuario u,
			MensagemConversa c) {
		return rep.findAllByUserConversa(u, c);
	}

	@Override
	public List<StatusMensagemUsuario> findAllByConversa(MensagemConversa c) {
		return rep.findAllByConversa(c);
	}

	@Override
	public int updateStatusConversaGrupo(Long userId, Long conversaId, int status) {
		return rep.updateStatusMensagem(userId, conversaId, status);
	}
	
	@Override
	public int updateStatusMensagemGrupo(Long userId, Long mensagemId, Integer status) {
		return rep.updateStatusMensagemGrupo(userId,mensagemId,status);
	}

	@Override
	public List<StatusMensagemUsuario> findAllByMensagem(Mensagem m) {
		return rep.findAllByMensagem(m);
	}

	@Override
	public void delete(Usuario u, Grupo grupo) {
		rep.delete(u, grupo);
		
	}
	
	@Override
	public StatusMensagemUsuario findByMsgUsuario(Mensagem msg, Usuario u) {
		return rep.findByMsgUsuario(msg, u);
	}
	
}
