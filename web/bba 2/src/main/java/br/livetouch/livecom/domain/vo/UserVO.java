package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

public class UserVO implements Serializable {

	private static final long serialVersionUID = 1L;
	public Long id;
	public String codigo;
	public String nome;
	public String email;
	public int badge;
	
}