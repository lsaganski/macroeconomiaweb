package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.PerfilPermissao;

public class PerfilVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	public String nome;
	
	private String descricao;
	
	public boolean publicar;
	public boolean excluirPostagem;
	public boolean editarPostagem;
	public boolean comentar;
	public boolean visualizarComentario;
	public boolean visualizarConteudo;
	public boolean visualizarRelatorio;
	public boolean curtir;
	public boolean enviarMensagem;
	public boolean cadastrarUsuarios;
	public boolean arquivos;
	public boolean padrao;
	public boolean cadastrarTabelas;
	public boolean codigo;
	
	private Map<String, Boolean> permissoes;
	
	public void setMapPermissoes(Set<PerfilPermissao> permissao) {
		
		if(permissoes == null) {
			permissoes = new HashMap<String, Boolean>();
		}
		
		for (PerfilPermissao p : permissao) {
			if(p.getPermissao() != null) {
				permissoes.put(p.getPermissao().getCodigo(), p.isLigado());
			}
		}
		
		this.publicar = isPermissao(ParamsPermissao.PUBLICAR);
		this.excluirPostagem = isPermissao(ParamsPermissao.EXCLUIR_PUBLICACAO);
		this.editarPostagem = isPermissao(ParamsPermissao.EDITAR_PUBLICACAO);
		this.comentar = isPermissao(ParamsPermissao.COMENTAR);
		this.visualizarComentario = isPermissao(ParamsPermissao.VIZUALIZAR_COMENTARIOS);
		this.visualizarConteudo = isPermissao(ParamsPermissao.VIZUALIZAR_CONTEUDO);
		this.visualizarRelatorio = isPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS);
		this.curtir = isPermissao(ParamsPermissao.CURTIR_PUBLICACAO);
		this.enviarMensagem = isPermissao(ParamsPermissao.ENVIAR_MENSAGEM);
		this.cadastrarUsuarios = isPermissao(ParamsPermissao.CADASTRAR_USUARIOS);
		this.arquivos = isPermissao(ParamsPermissao.ARQUIVOS);
		this.padrao = isPermissao(ParamsPermissao.PADRAO);
		this.cadastrarTabelas = isPermissao(ParamsPermissao.ACESSO_CADASTROS);
		this.codigo = isPermissao(ParamsPermissao.CADASTRAR_CODIGOS);
		
	}

	public void setMapPermissoesVO(List<PerfilPermissaoVO> permissao) {
		
		if(permissoes == null) {
			permissoes = new HashMap<String, Boolean>();
		}
		
		for (PerfilPermissaoVO p : permissao) {
			if(p.getPermissao() != null) {
				permissoes.put(p.getPermissao().getCodigo(), p.isLigado());
			}
		}
	}

	public PerfilVO() {
		
	}

	public PerfilVO(Perfil p) {
		setId(p.getId());
		setNome(p.getNome());
		setDescricao(p.getDescricao());
	}

	public void setPerfil(Perfil p) {
		setId(p.getId());
		setNome(p.getNome());
		setDescricao(p.getDescricao());
	}
	
	public static List<PerfilVO> fromList(List<Perfil> perfis) {
		List<PerfilVO> vos = new ArrayList<>();
		if(perfis == null) {
			return vos;
		}
		for (Perfil p : perfis) {
			PerfilVO perfilVO = new PerfilVO();
			perfilVO.setPerfil(p);
			vos.add(perfilVO);
		}
		return vos;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Map<String, Boolean> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Map<String, Boolean> permissoes) {
		this.permissoes = permissoes;
	}
	
	public boolean isPermissao(String key) {
		if(permissoes == null) {
			return false;
		}
		
		return permissoes.get(key) != null ? permissoes.get(key) : false;
	}

	public boolean isPublicar() {
		return isPermissao(ParamsPermissao.PUBLICAR);
	}

	public void setPublicar(boolean publicar) {
		this.publicar = publicar;
	}

	public boolean isExcluirPostagem() {
		return isPermissao(ParamsPermissao.EXCLUIR_PUBLICACAO);
	}

	public void setExcluirPostagem(boolean excluirPostagem) {
		this.excluirPostagem = excluirPostagem;
	}

	public boolean isEditarPostagem() {
		return isPermissao(ParamsPermissao.EDITAR_PUBLICACAO);
	}

	public void setEditarPostagem(boolean editarPostagem) {
		this.editarPostagem = editarPostagem;
	}

	public boolean isComentar() {
		return isPermissao(ParamsPermissao.COMENTAR);
	}

	public void setComentar(boolean comentar) {
		this.comentar = comentar;
	}

	public boolean isVisualizarComentario() {
		return isPermissao(ParamsPermissao.VIZUALIZAR_COMENTARIOS);
	}

	public void setVisualizarComentario(boolean visualizarComentario) {
		this.visualizarComentario = visualizarComentario;
	}

	public boolean isVisualizarConteudo() {
		return isPermissao(ParamsPermissao.VIZUALIZAR_CONTEUDO);
	}

	public void setVisualizarConteudo(boolean visualizarConteudo) {
		this.visualizarConteudo = visualizarConteudo;
	}

	public boolean isVisualizarRelatorio() {
		return isPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS);
	}

	public void setVisualizarRelatorio(boolean visualizarRelatorio) {
		this.visualizarRelatorio = visualizarRelatorio;
	}

	public boolean isCurtir() {
		return isPermissao(ParamsPermissao.CURTIR_PUBLICACAO);
	}

	public void setCurtir(boolean curtir) {
		this.curtir = curtir;
	}

	public boolean isEnviarMensagem() {
		return isPermissao(ParamsPermissao.ENVIAR_MENSAGEM);
	}

	public void setEnviarMensagem(boolean enviarMensagem) {
		this.enviarMensagem = enviarMensagem;
	}

	public boolean isCadastrarUsuarios() {
		return isPermissao(ParamsPermissao.CADASTRAR_USUARIOS);
	}

	public void setCadastrarUsuarios(boolean cadastrarUsuarios) {
		this.cadastrarUsuarios = cadastrarUsuarios;
	}

	public boolean isArquivos() {
		return isPermissao(ParamsPermissao.ARQUIVOS);
	}

	public void setArquivos(boolean arquivos) {
		this.arquivos = arquivos;
	}

	public boolean isPadrao() {
		return isPermissao(ParamsPermissao.PADRAO);
	}

	public void setPadrao(boolean padrao) {
		this.padrao = padrao;
	}

	public boolean isCadastrarTabelas() {
		return isPermissao(ParamsPermissao.ACESSO_CADASTROS);
	}

	public void setCadastrarTabelas(boolean cadastrarTabelas) {
		this.cadastrarTabelas = cadastrarTabelas;
	}

	public boolean isCodigo() {
		return isPermissao(ParamsPermissao.CADASTRAR_CODIGOS);
	}

	public void setCodigo(boolean codigo) {
		this.codigo = codigo;
	}

}
