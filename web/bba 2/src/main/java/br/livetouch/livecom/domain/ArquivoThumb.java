package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.livetouch.livecom.domain.vo.ThumbVO;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ArquivoThumb extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "ARQUIVOSTHUMB_SEQ")
	private Long id;

	private String url;

	private int width;
	private int height;
	private int cod;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "arquivo_id", nullable = true)
	@XStreamOmitField
	private Arquivo arquivo;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String nome) {
		this.url = nome;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	@Override
	public String toString() {
		return "Formacao [id=" + id + ", nome=" + url + "]";
	}

	public void setThumbVo(ThumbVO t) {
		this.cod = t.getCod();
		this.width = t.getWidth();
		this.height = t.getHeight();
		this.url = t.getUrl();
	}

	public static List<ArquivoThumb> toList(List<ThumbVO> thumbsVo) {
		List<ArquivoThumb> l=  new ArrayList<ArquivoThumb>();
		if (thumbsVo != null) {
			for (ThumbVO t : thumbsVo) {
				ArquivoThumb vo = new ArquivoThumb();
				vo.setThumbVo(t);
				l.add(vo);
			}
		}
		return l;
	
	}
	
	public int getCod() {
		return cod;
	}
	public void setCod(int cod) {
		this.cod = cod;
	}

	public boolean hasSize(int size) {
		return width == size || height == size;
	}
	
	public boolean isThumbMural() {
		boolean a = width < 580 && height == 325;
		boolean b = width == 580 && height < 325;
		return a || b;
	}
}
