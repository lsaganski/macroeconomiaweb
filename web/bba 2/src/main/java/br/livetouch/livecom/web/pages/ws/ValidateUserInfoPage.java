package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.Usuario;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ValidateUserInfoPage extends WebServiceXmlJsonPage {

	public Form form = new Form();

	public String status;
	public String mensagem;
	public String errorCode;
	private UsuarioLoginField tUser;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		form.add(tUser = new UsuarioLoginField("user_id", false, usuarioService, getEmpresa()));
		
		form.add(new Submit("Test"));
		
		tUser.setFocus(true);

		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {

			Usuario user = tUser.getEntity();
			if (user == null) {
				return Response.error("Usuário não encontrado");
			}

			Long userId = user.getId();

			boolean a = Livecom.getInstance().getUserInfo(userId) != null;
			boolean b = Livecom.getInstance().isUsuarioLogadoChat(userId);

			ValidateUserInfo v = new ValidateUserInfo();
			v.loginLivecomOk = a;
			v.loginChatOk = b;
			v.status = a && b ? "OK" : "ERROR";
			
			return v;
		}
		
		return new MensagemResult("NOK","Erro ao buscar notificações.");
	}
	
	public static class ValidateUserInfo {
		public String status;
		public boolean loginLivecomOk;
		public boolean loginChatOk;
	}
	
	@Override
	protected boolean isResponseGSON() {
		return true;
	}
}
