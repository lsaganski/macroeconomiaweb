package br.livetouch.livecom.web.pages.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * Relatório de acessos
 * 
 */
@Controller
@Scope("prototype")
public class EventosMenuPage extends LivecomAdminPage {

	@Override
	public void onInit() {
		super.onInit();
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
