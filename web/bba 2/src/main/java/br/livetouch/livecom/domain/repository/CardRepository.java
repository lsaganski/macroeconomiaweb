package br.livetouch.livecom.domain.repository;

import br.livetouch.livecom.domain.Card;
import net.livetouch.tiger.ddd.repository.Repository;

public interface CardRepository extends Repository<Card>{

}
