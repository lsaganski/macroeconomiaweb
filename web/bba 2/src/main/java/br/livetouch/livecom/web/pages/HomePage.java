package br.livetouch.livecom.web.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;

@Controller
@Scope("prototype")
public class HomePage extends LivecomLogadoPage {

	public String login;

	@Override
	public void onGet() {
		super.onGet();
	}
}
