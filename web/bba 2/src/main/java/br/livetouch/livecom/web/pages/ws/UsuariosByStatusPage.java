package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.domain.vo.UsuariosVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class UsuariosByStatusPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;

	private LongField tGrupoId;
	public Integer maxRows;
	
	public String status;

	@Override
	public boolean onSecurityCheck() {
		return true;
	}

	@Override
	public void onInit() {
		form.setMethod("post");

		form.add(tGrupoId = new LongField("grupo_id"));
		form.add(new TextField("maxRows"));
		form.add(new TextField("page"));

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		form.add(new Submit("buscar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if(form.isValid()) {
			Long id = tGrupoId.getLong();
			
			Grupo g = null;
			if(id != null) {
				g = grupoService.get(id);
				if(g == null) {
					return new MensagemResult("NOK", "Grupo não encontrado.");
				}
			}

			if(maxRows == null) {
				maxRows = 10;
			}
			
			StatusUsuario statusAtivacao = null;
			if(StringUtils.isNotEmpty(status)) {
				statusAtivacao = StatusUsuario.values()[Integer.valueOf(status)];
			}

			List<Usuario> usuarios = usuarioService.findByStatus(g, statusAtivacao, getEmpresa(), page, maxRows);
			List<UsuarioVO> usersVO = UsuarioVO.toList(usuarios, usuarioService, grupoService);
			
			Long count = usuarioService.getCountFindByStatus(g, statusAtivacao, getEmpresa());
			
			UsuariosVO vo = new UsuariosVO();
			vo.total = count;
			vo.usuarios = usersVO;

			return vo;
		}
		
		return new MensagemResult("NOK", "Chamada incorreta.");
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("list", UsuariosVO.class);
		super.xstream(x);
	}
}
