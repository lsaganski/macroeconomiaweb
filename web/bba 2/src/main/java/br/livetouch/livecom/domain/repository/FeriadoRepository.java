package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Feriado;

@Repository
public interface FeriadoRepository extends net.livetouch.tiger.ddd.repository.Repository<Feriado> {


	List<Feriado> findAll(Empresa empresa);
	
}