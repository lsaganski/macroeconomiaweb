package br.livetouch.livecom.web.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.enums.TipoGrupo;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import br.livetouch.livecom.web.pages.cadastro.GruposPage;


@Controller
@Scope("prototype")
public class GrupoPage extends LivecomLogadoPage {

	public boolean escondeChat = true;
	public Long id;
	public Grupo grupo;
	public String tab;
	
	public Long notId;

	public boolean favorito = false;
	public boolean sair = false;
	public boolean isAdmin  = false;
	public boolean isSubadmin = false;
	public GrupoUsuarios gu;
	
	public boolean tipoGrupo = false;
	
	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
		
		tipoGrupo = getParametrosMap().getBoolean(Params.CADASTRO_GRUPO_TIPO_ON, false);
		
		if(id != null) {
			grupo = grupoService.get(id);
			if(grupo == null) {
				setRedirect(GruposPage.class);
				return;
			}
			
			isAdmin = grupoService.isAdminOrSubadminGrupo(getUsuario(), grupo);
			isSubadmin = isAdmin;
			validateAcessoGrupo(isAdmin, tipoGrupo, false, grupo);
			
			if(!grupo.isGrupoVirtual()) {
				TipoGrupo tipoGrupo = grupo.getTipoGrupo() != null ? grupo.getTipoGrupo() : TipoGrupo.ABERTO;
				boolean isPrivado = tipoGrupo.equals(TipoGrupo.PRIVADO);
				
				gu = usuarioService.getGrupoUsuario(getUsuario(), grupo);
				
				if(gu != null) {
					if(!isPrivado && !isAdmin) {
						sair = true;
					}
					favorito = gu.isFavorito();
				}
			}
			
			if(notId != null) {
				NotificationUsuario not = notificationService.getNotificationUsuario(notId);
				if(not != null) {
					notificationService.markAsRead(not);
				}
				
			}
		}
		
	}

	@Override
	public void onGet() {
		super.onGet();
	}


	@Override
	public void onRender() {
		super.onRender();
	}
}
