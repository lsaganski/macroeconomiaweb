package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.TagFilter;
import net.livetouch.tiger.ddd.DomainException;

public interface TagService extends Service<Tag> {

	List<Tag> findAll(Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Usuario userInfo,Tag c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Usuario userInfo,Tag c, boolean force) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void deleteAll(Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	List<Tag> getTagsAndSave(String tags, Empresa empresa);

	List<Tag> getTags(String tags, Empresa empresa);

	List<Tag> findTagsByNome(String s, Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	void saveTags(List<Tag> list, Empresa empresa);

	List<Tag> findAllByFilter(TagFilter filter, Empresa empresa);

	List<Tag> findAllByIds(List<Long> listIds, Empresa empresa);

	String csvTag(Empresa empresa);

	Tag findByName(Tag tag, Empresa empresa);
}
