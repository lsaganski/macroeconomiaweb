package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.JSONUtils;

public class MensagemInfoVO {
	public Long id;
	public Long conversaId;
    public String data;

    public String dataPush;
    public String dataEntregue;
    public String dataLida;

    // Se for arquivo, vem a URL
    public String fileUrl;
    
    public boolean isGrupo;

    public String msg;
    public Long fromId;
	public Long toId;
	
	public MensagemInfoVO () {
			
	}

	public void setMensagem(Mensagem msg, Usuario u) {
		if (msg == null) {
			return;
		}
		this.id = msg.getId();
		
		MensagemConversa c = msg.getConversa();
		this.conversaId = c.getId(); 

		this.data = msg.getDataLidaStringHojeOntem();
		this.dataLida = msg.getDataLidaStringHojeOntem();
		this.dataEntregue = msg.getDataEntregueStringHojeOntem();
		this.dataPush = msg.getDataPushStringHojeOntem();
		
		this.fromId = c.getFrom() != null ? msg.getFrom().getId() : null;
		this.toId = c.getTo() != null ? msg.getTo().getId() : null;
		
		// arquivos
		List<Arquivo> list = new ArrayList<Arquivo>(msg.getArquivos());
		if(list != null && list.size() > 0) {
			Arquivo a = list.get(0);
			this.fileUrl = a.getUrlThumb();
		}

		this.isGrupo = c.getGrupo() != null;
		
		this.msg = msg.getMsg();

	}
	
	@Override
	public String toString() {
		return JSONUtils.toJSON(this);
	}
}
