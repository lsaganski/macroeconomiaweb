package br.livetouch.livecom.utils;

import br.livetouch.livecom.domain.ParametrosMap;

public class SQLUtil {

	/**
	 * MySQL:
	 * 
	 * DATE_FORMAT(t1.data,'%d/%m/%Y')
	 * 
	 * MS SQL SERVER:
	 * 
	 * 
	 * 
	 * @return
	 */
	public static String getDateFormat(String date){
		String database = ParametrosMap.getInstance().get("infra.database", "mysql");
		
		if("mssqlserver".equals(database)) {
			return String.format("CONVERT(%s,'%dd/%mm/%YY')", date);
		}
		
		// MySQL default
		return String.format("DATE_FORMAT(%s,'%d/%m/%Y')", date);
	}	
	
	public static String format() {
		System.out.println("teste");
		return "";
	}
}
