package br.livetouch.livecom.web.pages.training;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import net.sf.click.Context;

public class PostFilter implements Serializable {

	private static final String KEY = "filter";
	private static final long serialVersionUID = -1030496972151085256L;
	
	public String filter;
	public String search;
	public String tags;
	public Long categId;
	public String sort;

	public void init(Context context) {
		String s = context.getRequestParameter("filter");
		if(StringUtils.isNotEmpty(s)) {
			this.filter = s;
		}
		
		s = context.getRequestParameter("search");
		if(StringUtils.isNotEmpty(s)) {
			this.search = s;
		}
		
		s = context.getRequestParameter("tags");
		if(StringUtils.isNotEmpty(s)) {
			this.tags = s;
		}
		
		s = context.getRequestParameter("categId");
		if(StringUtils.isNotEmpty(s)) {
			this.categId = NumberUtils.toLong(s,0);
			this.search = null;
		}
		
		s = context.getRequestParameter("sort");
		if(StringUtils.isNotEmpty(s)) {
			this.sort = s;
		}
		
		s = context.getRequestParameter("clear");
		if(StringUtils.isNotEmpty(s)) {
			clear();
		}
		
		if(context.getRequest().getParameterMap().isEmpty()) {
			clear();
		}
	}

	public static void set(Context context, PostFilter f) {
		context.setSessionAttribute(KEY, f);
	}

	public static PostFilter get(Context context) {
		PostFilter f = (PostFilter) context.getSessionAttribute(KEY);
		if(f == null) {
			f = new PostFilter();
			PostFilter.set(context, f);
		}
		return f;
	}

	public void clear() {
		this.sort = null;
		this.filter = null;
		this.search = null;
		this.tags = null;
		this.categId = null;		
	}

	public Long getCategId() {
		return categId;
	}
	public String getFilter() {
		return filter;
	}
	public String getSearch() {
		return search;
	}
	public String getSort() {
		return sort;
	}
	public String getTags() {
		return tags;
	}
	public static String getKey() {
		return KEY;
	}
}
