package br.livetouch.livecom.web.pages.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoGrupo;
import br.livetouch.livecom.utils.FileExtensionUtils;
import br.livetouch.livecom.web.pages.LogonPage;
import br.livetouch.livecom.web.pages.cadastro.GruposPage;
import br.livetouch.livecom.web.pages.pages.AlterarSenhaExpiradaPage;

@Controller
public class LivecomLogadoPage extends LivecomPage {

	/**
	 * @see br.livetouch.livecom.web.pages.admin.LivecomPage#onSecurityCheck()
	 */
	@Override
	public boolean onSecurityCheck() {
		boolean b = super.onSecurityCheck();
		if(!b) {
			return b;
		}
		
		Usuario u = getUsuario();
		boolean logado = u != null;
		if (logado) {
			boolean senhaExpirada = getUserInfoVO().getSenhaExpirada();
			if(senhaExpirada){
				setRedirect(AlterarSenhaExpiradaPage.class);
				return false;
			}
			return true;
		}

		redirectLogin();
		return onSecurityCheckNotOk();
	}

		

	protected void redirectLogin() {
		LogonPage.redirect(this);
	}

	@Override
	public void onRender() {
		super.onRender();
	}

	protected void downloadFile(File f) {
		try {
			byte bytes[]  = IOUtils.toByteArray(new FileInputStream(f));
			downloadFile(bytes, f.getName());
		} catch (Exception e) {
			logError(e.getMessage(), e);
		}
	}

	/**
	 * Se precisar de excel:
	 * response.setCharacterEncoding("ISO-8859-1");
	 * response.setContentType("application/ms-excel");
	 *
	 * @param bytes
	 * @param fileName
	 */
	protected void downloadFile(byte bytes[], String fileName) {
		try {
			String charSet = "UTF-8";
			HttpServletResponse response = getContext().getResponse();
			String contentType = FileExtensionUtils.getMimeType(fileName);
			response.setContentType(contentType+"; charset="+charSet);
			response.setCharacterEncoding(charSet);
			response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName, charSet));

			// response.setContentType(getContentType());

			setPath(null);

			OutputStream out = response.getOutputStream();

			out.write(bytes);
			out.flush();
			out.close();
		} catch (IOException e) {
			logError(e.getMessage(), e);
		}
	}

	protected void download(String text, String fileName) {
		try {

			String charset = getParams().get(Params.EXPORTACAO_ARQUIVO_CHARSET, "UTF-8");

			if(StringUtils.isEmpty(charset)) {
				charset = "UTF-8";
			}

			HttpServletResponse response = getContext().getResponse();
			response.setContentType("application/csv; charset=" + charset);
			response.setCharacterEncoding(charset);
			response.setContentType("application/ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8"));

			setPath(null);
			PrintWriter writer = response.getWriter();

			writer.print(text);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			logError(e.getMessage(), e);
		}
	}

	protected void validateAcessoGrupo(boolean isAdmin, boolean tipoGrupo, boolean edit, Grupo grupo) {
		if(tipoGrupo) {
			if(grupo.isGrupoVirtual()) {
				if(!isAdmin && edit) {
					setRedirect(GruposPage.class);
					return;
				}
			} else {

				if(edit) {
					if(!isAdmin) {
						setRedirect(GruposPage.class);
						return;
					}
				}

				TipoGrupo tipo = grupo.getTipoGrupo() != null ? grupo.getTipoGrupo() : TipoGrupo.ABERTO;
				boolean isPrivado = tipo.equals(TipoGrupo.PRIVADO);

				GrupoUsuarios gu = usuarioService.getGrupoUsuario(getUsuario(), grupo);

				//Apenas o admin de um grupo privado pode acessar
				if(isPrivado && !isAdmin && gu == null)  {
					setRedirect(GruposPage.class);
					return;
				} else {
					boolean isPrivadoOpcional = tipo.equals(TipoGrupo.PRIVADO_OPCIONAL);
					if(isPrivadoOpcional && gu == null) {
						setRedirect(GruposPage.class);
						return;
					}
				}
			}

		} else {

			GrupoUsuarios gu = usuarioService.getGrupoUsuario(getUsuario(), grupo);
			if((!grupo.isAdmin(getUsuario()) && gu == null) && !hasPermissao(ParamsPermissao.VISUALIZAR_TODOS_OS_GRUPOS)) {
				setRedirect(GruposPage.class);
				return;
			}
		}
	}
}
