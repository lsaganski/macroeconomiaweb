package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.BuscaPost;
import br.livetouch.livecom.domain.vo.PostVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;

/**
 * @deprecated usar sempre MuralPage
 * 
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class BuscaPostsPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tUserId;
	private TextField tTexto;
	private TextField tMode;
	private IntegerField tFavoritos;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tUserId = new TextField("user_id"));
		form.add(tTexto = new TextField("texto"));
		
		form.add(new TextField("maxRows"));
		form.add(new TextField("page"));
		
		form.add(tFavoritos = new IntegerField("favoritos","Somente favoritos"));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		tUserId.setFocus(true);

		form.add(new Submit("consultar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			String userIdText = tUserId.getValue();
			String texto = tTexto.getValue();
			long userId = -1;

//			Usuario u = usuarioService.get(userId);
			if(StringUtils.isNotEmpty(userIdText)) {
				if(NumberUtils.isNumber(userIdText)) {
					userId = new Long(userIdText);
				}
				else {
					return new MensagemResult("ERROR","Usuário incorreto.");
				}
			}

			Usuario u = usuarioService.get(userId);
			if(u == null) {
				return new MensagemResult("ERROR","Usuário inválido.");
			}

			if(StringUtils.isEmpty(userIdText) && StringUtils.isEmpty(texto)) {
				return new MensagemResult("ERROR","Informe usuário ou título do post.");
			}
			
			int maxRows = NumberUtils.toInt(getContext().getRequestParameter("maxRows"),20);

			boolean favoritos = "1".equals(tFavoritos.getValue());

			BuscaPost b = new BuscaPost();
			b.empresa = getEmpresa();
			b.favoritos = favoritos;
			b.user = u;
			b.texto = texto;
			List<PostVO> list = getPosts(u, maxRows, b);

			return list;
		}
		
		return new MensagemResult("NOK","Erro ao buscar comentarios.");
	}

	private List<PostVO> getPosts(Usuario u, int maxRows, BuscaPost b) {
		List<PostVO> list = null;
		if(isWordPress()) {
			list = wordpress.getPosts(u, maxRows, page);
		} else {
			List<Post> posts = postService.findAllPostsByUserAndTitle(b, page, maxRows);
			list = postService.toListVo(u, posts);
		}
		return list;
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("post", PostVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
