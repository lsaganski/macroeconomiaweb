package br.livetouch.livecom.jobs.importacao;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.dialect.Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Service;

import br.infra.livetouch.dialect.OracleDialect;
import br.infra.util.Log;
import br.infra.util.SQLUtils;
import br.infra.util.Utils;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.ImportarArquivoResponse;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.LinhaUsuarioVO;
import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;
import net.livetouch.extras.util.CripUtil;
import net.livetouch.hibernate.HibernateUtil;
import net.livetouch.tiger.ddd.DomainException;

/**
 * 22 min 87 mil
 * 
 * @author Usuário
 *
 */
@Service
public class ImportarArquivoItauRevistaWork extends ImportacaoArquivoWork {

	protected static final Logger log = Log.getLogger("importacao_usuario");
	protected static final Logger logError = Log.getLogger("importacao_usuario_error");
	private File file;
	private Scanner scanner;
	private ImportarArquivoResponse response;
	private Long idGrupo;
	private Long idPermissao;
	
	private int linhas;
	private double porcentagem;

	@Autowired
	protected UsuarioService usuarioService;
	private HashMap<String, Long> mapLogins;
	private Session session;
	private Long idEmpresa;
	private Dialect dialect;
	
	@Override
	public void init(Session session,File file, Dialect dialect) throws DomainException, IOException {
		this.session = session;
		this.file = file;
		this.dialect = dialect;
		idEmpresa = JobImportacaoUtil.getIdEmpresa(file);
		
		linhas = countLines(file.getAbsolutePath());
		JobInfo jobInfo = JobInfo.getInstance(idEmpresa);
		jobInfo.setFim(linhas);
		jobInfo.setTemArquivo(true);
		jobInfo.setStatus("");
		

		idGrupo = new Long(ParametrosMap.getInstance().get(Params.IMPORTACAO_GRUPO_ID, "5"));
		idPermissao = new Long(ParametrosMap.getInstance().get(Params.IMPORTACAO_PERMISSAO_ID, "3"));

		Grupo grupo = (Grupo) session.get(Grupo.class,idGrupo);
		if (grupo == null) {
			log.debug("Grupo não encontrado no banco de dados, id [" + idGrupo + "]");
			throw new DomainException("Grupo não encontrado no banco de dados, id [" + idGrupo + "]");
		}

		Perfil permissao = (Perfil) session.get(Perfil.class,idPermissao);
		if (permissao == null) {
			log.debug("Permissão não encontrada no banco de dados, id [" + idPermissao + "]");
			throw new DomainException("Permissão não encontrada no banco de dados, id [" + idPermissao + "]");
		}

		// Salva map com todos os logins
		// Os logins que que nao estiverem no csv
		// E estiverem no map, serao excluidos no final.
		log.debug("Carregando logins");
		List<Object[]> logins = usuarioService.findIdLoginsTipoImportacao();
		mapLogins = new HashMap<String,Long>();
		for (Object[] user : logins) {
			Long id = (Long) user[0];
			String login = (String) user[1];
			mapLogins.put(login, id);
		}
		log.debug("Existem ["+mapLogins.size()+"] logins na base.");
	}
	
	@Override
	public void execute(Connection conn) throws SQLException {
		if(file == null) {
			throw new IllegalArgumentException("Cha,ou i init()?");
		}
		
		response = new ImportarArquivoResponse();
		int countOk = 0;
		int countError = 0;
		
		long timeA = System.currentTimeMillis();
		
		Date inicio = new Date();
		final Charset ENCODING = StandardCharsets.UTF_8;
		Path path = Paths.get(file.getAbsolutePath());

		PreparedStatement stmtInsert = createStatementInsert(dialect, conn);
		PreparedStatement stmtUpdate = conn.prepareStatement("update usuario set login=?,nome=?,senha=?,data_nasc=str_to_date(?,'%d%m%Y'),permissao_id=?,grupo_id=? where id=?");		
		PreparedStatement stmtInsertUsuarioGrupo = conn.prepareStatement("insert into grupo_usuarios (grupo_id,usuario_id,postar) VALUES(?,?,0)");
		PreparedStatement stmtExisteUsuarioGrupoIn = null;
		
		
		int row = 0;
		JobInfo jobInfo = JobInfo.getInstance(idEmpresa);
		try {
			log.debug("Iniciou ImportarArquivoWork com Batch ...");

			scanner = new Scanner(path, ENCODING.name());

			List<Long> idsUsuario = new ArrayList<>();
			StringBuffer sbIn = null;
			
			boolean insert = false;
			
			jobInfo.setStatus("Inserindo usuarios");
			while (scanner.hasNextLine()) {
				try {
					row++;
					String linha = scanner.nextLine();
					String[] split = StringUtils.split(linha, ";");
					LinhaUsuarioVO vo = new LinhaUsuarioVO();
					
					vo = (LinhaUsuarioVO) vo.parser(linha, split, row);
					if(vo == null) {
						continue;
					}

					String login = vo.getLogin();
					String dataNasc = StringUtils.replace(vo.getDataNascimento(), "/", "");
					String senhaMD5 = CripUtil.criptToMD5(dataNasc);
					
					insert = false;
					
					Long idUser = mapLogins.get(login);
					if (idUser == null) {
						
						insert = true;
						
						// insert into usuario
						log("insert work2 row["+row+"] " + login);
						stmtInsert.setString(1, login);
						stmtInsert.setString(2, login);
						stmtInsert.setString(3, senhaMD5);
						stmtInsert.setString(4, dataNasc);
						stmtInsert.setLong(5, idPermissao);
						stmtInsert.setLong(6, idGrupo);
						stmtInsert.execute();

						idUser = SQLUtils.getGeneratedId(stmtInsert);
						if(idUser == null) {
							logError.error("Erro row ["+row+"] ao inserir user: " + login);
							continue;
						}
					} else {
						// update
						//PreparedStatement stmtUpdate = conn.prepareStatement("update usuario set login=?,nome=?,senha=?,data_nasc=str_to_date(?,'%d%m%Y'),permissao_id=?,grupo_id=? where id=?");
						stmtUpdate.setString(1, login);
						stmtUpdate.setString(2, login);
						stmtUpdate.setString(3, senhaMD5);
						stmtUpdate.setString(4, dataNasc);
						stmtUpdate.setLong(5, idPermissao);
						stmtUpdate.setLong(6, idGrupo);
						stmtUpdate.setLong(7, idUser);
						stmtUpdate.execute();
						log("update row["+row+"] " + login);
					}
					
					if (idUser != null) {
						idsUsuario.add(idUser);
						
						if(sbIn == null) {
							sbIn = new StringBuffer("(");
							sbIn.append(idUser);
						} else {
							sbIn.append(",").append(idUser);
						}
						
						// count
						if(insert) {
							insert = false;
							response.countInsert++;
						} else {
							response.countUpdate++;
						}
					}

					countOk++;
					response.list.add(vo);

					// Remove do map
					mapLogins.remove(login);
					
					porcentagem = ((double)row * 100) / (double)linhas;
					jobInfo.setPorcentagem(porcentagem);
					jobInfo.setAtual(row);
					
				} catch (Exception e) {
					countError++;
					log.error("Erro usuario linha [" + row + "]: " + e.getMessage(), e);
				}
			}

			log.debug("idsUsuario: " + idsUsuario.size());

			sbIn.append(")");
			
			// Existe Grupo?
			log.debug("Select in para ["+idsUsuario.size()+"] users para descobrir grupo_usuarios a inserir.");
			String sql = "select u.id from grupo_usuarios gu,usuario u where u.id=gu.usuario_id and u.tipo_sistema = 'IMPORTACAO' and  gu.grupo_id=? and gu.usuario_id in " + sbIn.toString();
			stmtExisteUsuarioGrupoIn = conn.prepareStatement(sql);
			stmtExisteUsuarioGrupoIn.setLong(1,idGrupo);
			ResultSet rs2 = stmtExisteUsuarioGrupoIn.executeQuery();
			while (rs2.next()) {
				Long userId = rs2.getLong(1);
				idsUsuario.remove(userId);
			}
			JdbcUtils.closeResultSet(rs2);
			log.debug("Select in OK, vai inserir: " + idsUsuario.size() + " users.");

			row = 0;
			porcentagem = 0.0;
			linhas = idsUsuario.size();
			jobInfo.setFim(idsUsuario.size());
			jobInfo.setPorcentagem(porcentagem);
			
			// Insert: grupo_usuarios
			jobInfo.setStatus("Associando usuarios aos grupos");
			for (Long idUser : idsUsuario) {
				row++;
				// Inserir usuario grupo
				System.out.println(idGrupo + ", " + idUser);
				stmtInsertUsuarioGrupo.setLong(1, idGrupo);
				stmtInsertUsuarioGrupo.setLong(2, idUser);
				stmtInsertUsuarioGrupo.execute();
				
				porcentagem = ((double)row * 100) / (double)linhas;
				jobInfo.setPorcentagem(porcentagem);
				jobInfo.setAtual(row);
			}
			log.debug("grupo_usuarios Insert OK.");

			long timeB = System.currentTimeMillis();
			log("TimeB min: " + (timeB-timeA)/1000/60);
			
			// Deletar usuarios
			// Todos logins que nao estavam no csv serao excluidos
			boolean local = Utils.isLocalMachine();
			if(!local) {
				deleteLoginsQueNaoEstavamNoCsv(response,mapLogins);
			}
			
		} catch (Exception e) {
			jobInfo.setSuccessful(false);
			String msgError = "Erro importar areas cargos[" + row + "] " + e.getMessage();
			logError.error(msgError, e);
			throw new SQLException(msgError, e);
		} finally {
			
			JdbcUtils.closeStatement(stmtExisteUsuarioGrupoIn);
			JdbcUtils.closeStatement(stmtInsert);
			JdbcUtils.closeStatement(stmtInsertUsuarioGrupo);

			HibernateUtil.clearCache(session);
			
			response.countOk = countOk;
			response.countError = countError;
			
			finish(jobInfo);
			
			log.debug("Início: " + inicio);
			log.debug("Final: " + new Date());
			long timeC = System.currentTimeMillis();
			log("TimeC min: " + (timeC-timeA)/1000/60);
		}
	}

	private void finish(JobInfo jobInfo) {
		jobInfo.setResponse(response);
		jobInfo.setStatus("Fim da Importação");
		jobInfo.setTemArquivo(false);
		jobInfo.setFim(0);
		jobInfo.setAtual(0);
		jobInfo.setAtual(0);
	}
	
	private void log(String string) {
		JobInfo.getInstance(idEmpresa).lastMessageImportacao = string;
		log.debug(string);
	}
	
	public ImportarArquivoResponse getResponse() {
		return response;
	}
	
	protected void deleteLoginsQueNaoEstavamNoCsv(ImportarArquivoResponse response, Map<String, Long> mapLogins) {
		Collection<Long> idsToDelete = mapLogins.values();
		
		int count = 0;
		int countError = 0;
		
		if(idsToDelete.size() > 0) {
			
			int total = idsToDelete.size();
			
			log.debug("Deletando " + total + " usuários que não estavam no csv");
			log.debug("Keys: ["+idsToDelete+"]");
			
			int row = 0;
			porcentagem = 0.0;
			linhas = idsToDelete.size();
			JobInfo jobInfo = JobInfo.getInstance(idEmpresa);
			jobInfo.setFim(idsToDelete.size());
			jobInfo.setPorcentagem(porcentagem);

			jobInfo.setStatus("Verificando Usuários");
			for (Long id : idsToDelete) {
				// Deleta usuario que nao estava no csv
				row++;
				try {
					log.debug("Deletando user " + id);
					//Tentava deletar assim
//					usuarioService.deleteById(id);
					Usuario u = usuarioService.get(id);
					if(u != null) {
						usuarioService.delete(null, u);
					}
					log.debug("User deletado com sucesso");
					
					count++;
					
					porcentagem = ((double)row * 100) / (double)linhas;
					jobInfo.setPorcentagem(porcentagem);
					jobInfo.setAtual(row);
					
					log.debug("Count["+count+"], Total ["+total+"]");
					
				} catch (RuntimeException e) {
					countError++;
					log.error("Não foi possível excluir usuário ["+id+"] ao importar: " + e.getMessage(), e);
				} catch (DomainException e) {
					log.error("Não foi possível excluir usuário ["+id+"] ao importar: " + e.getMessage(), e);
				}
			}
		}
		
		response.countDeleteOk = count;
		response.countDeleteError = countError;
	}
	
	private PreparedStatement createStatementInsert(Dialect dialect, Connection conn) throws SQLException {
		
		if(dialect instanceof OracleDialect) {
			return conn.prepareStatement("insert into usuario (id,login,nome,senha,data_nasc,permissao_id,grupo_id,tipo_sistema,ativo,perfil_ativo,status,empresa_id,pre_cadastro) VALUES(USUARIO_SEQ.nextval,?,?,?,str_to_date(?,'%d%m%Y'),?,?,'IMPORTACAO',1,1,'NOVO',1,1)", new String [] {"id"});
		}

		return conn.prepareStatement("insert into usuario (login,nome,senha,data_nasc,permissao_id,grupo_id,tipo_sistema,ativo,perfil_ativo,status,empresa_id,pre_cadastro) VALUES(?,?,?,str_to_date(?,'%d%m%Y'),?,?,'IMPORTACAO',1,1,'NOVO',1,1)",Statement.RETURN_GENERATED_KEYS);
		
	}
}
