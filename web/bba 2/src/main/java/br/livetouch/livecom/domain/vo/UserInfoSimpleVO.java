package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.Livecom;

public class UserInfoSimpleVO implements Serializable {
	public static final String USER_INFO_KEY = "userInfo";

	private static final long serialVersionUID = 1L;
	public Long id;
	public String nome;
	public String login;
	public String tipoLogin;
	public String dataUltChat;
	public String dataLoginChat;
	public String dataUltTransacao;
//	public String tokenLogin;
	public long tempoLogadoMin;
	public boolean chatOn;
	public String dataLogin;
	
	public UserInfoSimpleVO(UserInfoVO user) {
		this.id = user.getId();
		this.nome = user.getNome();
		this.login = user.getLogin();
		this.tipoLogin = user.getTipoLogin();
		this.dataUltTransacao = user.getDataUltTransacaoString();
		this.dataLogin = user.getDataLoginString();
		this.dataLoginChat= user.getDataLoginChatString();
		this.dataUltChat = user.getDataUltChatString();
//		this.tokenLogin = user.wstoken
		this.chatOn = Livecom.getInstance().isUsuarioLogadoChat(user.id);
		
		this.tempoLogadoMin = user.getTempoLogadoMinutos();
	}
}