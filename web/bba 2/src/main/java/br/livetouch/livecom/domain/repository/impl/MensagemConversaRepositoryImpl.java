package br.livetouch.livecom.domain.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.repository.MensagemConversaRepository;
import br.livetouch.livecom.domain.vo.RelatorioConversaFiltro;
import br.livetouch.livecom.utils.ListUtils;
import br.livetouch.spring.StringHibernateRepository;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public class MensagemConversaRepositoryImpl extends StringHibernateRepository<MensagemConversa> implements MensagemConversaRepository {

	public MensagemConversaRepositoryImpl() {
		super(MensagemConversa.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MensagemConversa> findConversasByFilter(RelatorioConversaFiltro filtro) throws DomainException {
		Query query = queryConversasByFilter(filtro, false);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		query.setCacheable(false);
		List<MensagemConversa> conversas = query.list();

		return conversas;
	}

	private Query queryConversasByFilter(RelatorioConversaFiltro filtro, boolean count) throws DomainException {

		Long id = filtro.getId();
		Long usuario = filtro.getUsuario();
		Long grupo = filtro.getGrupo();

		StringBuffer sb = new StringBuffer();
		if (count) {
			sb.append("select count(distinct c) ");
		} else {
			sb.append("select distinct c ");
		}
		sb.append(" from MensagemConversa c");

		sb.append(" where 1=1 ");
		
		if(id != null) {
			sb.append(" and c.id = :id");
		}

		if (usuario != null) {
			sb.append(" and (c.from.id = :usuario or c.to.id = :usuario)");
		}

		if (grupo != null) {
			sb.append(" and c.grupo.id = :grupo");
		}

		if (!count) {
			sb.append(" group by c");
			sb.append(" order by c.dataUpdated desc");
		}

		Query q = createQuery(sb.toString());

		if (id != null) {
			q.setParameter("id", id);
		}

		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}

		if (grupo != null) {
			q.setParameter("grupo", grupo);
		}

		return q;
	}

	@Override
	public long getCountVersaoByFilter(RelatorioConversaFiltro filtro) throws DomainException {
		Query query = queryConversasByFilter(filtro, true);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}

	@Override
	@SuppressWarnings("unchecked")
	public MensagemConversa findByGrupo(Grupo g) {
		Query q = createQuery("from MensagemConversa c where c.grupo.id = ?");
		q.setCacheable(true);
		q.setLong(0, g.getId());

		List<MensagemConversa> list = q.list();
		
		return list.size() > 0 ? list.get(0) : null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isTodasMensagensLidas(Long conversaId, Long from, boolean isValidateGroup) {
		StringBuffer sb = new StringBuffer("select distinct m.lida, status.lida from Mensagem m inner join m.conversa c");

		sb.append(" left join m.statusUsuarios status ");
		
		sb.append(" where c.id = :conversaId and (status.usuario.id = :userId or m.to.id = :userId)");

		Query q = createQuery(sb.toString());
		q.setLong("conversaId", conversaId);
		q.setLong("userId", from);
		List<Object[]> list = q.list();
		Boolean todasLidas = true;
		for (Object[] obj : list) {
			Boolean statusLida = (Boolean) obj[1];
			Boolean msgLida = (Boolean) obj[0];
			if(statusLida == null) {
				if(!msgLida) {
					todasLidas = false;
					break;
				}
			} else if(!statusLida && !isValidateGroup) {
				todasLidas = false;
				break;
			} else if(isValidateGroup && !msgLida) {
				todasLidas = false;
				break;
			}
		}
		return todasLidas;
		
	}
	
	
	@Override
	public boolean isTodasEntrege(Long conversaId, Long userId) {
		StringBuffer sb = new StringBuffer("select distinct m.entregue, status.entregue from Mensagem m inner join m.conversa c");

		sb.append(" left join m.statusUsuarios status ");

		sb.append(" where c.id = :conversaId and (status.usuario.id = :userId or m.to.id = :userId)");

		Query q = createQuery(sb.toString());
		q.setLong("conversaId", conversaId);
		q.setLong("userId", userId);
		List<Object[]> list = q.list();
		Boolean todasEntregue = true;
		for (Object[] obj : list) {
			Boolean statusEntregue = (Boolean) obj[1];
			if(statusEntregue == null) {
				Boolean msgEntregue = (Boolean) obj[0];
				if(!msgEntregue) {
					todasEntregue = false;
					break;
				}
			} else if(!statusEntregue) {
				todasEntregue = false;
				break;
			}
		}
		return todasEntregue;
	}

	@Override
	public List<Long> findIdsConversasLidasById(List<Long> ids, Long userId) {
		if(ListUtils.isEmpty(ids)) {
			return new ArrayList<>();
		}
		
		List<Long> cidsLidas = new ArrayList<>();

		for (Long cId : ids) {
			// TODO daria de melhorar e buscar tudo de uma vez.
			boolean lida = isTodasMensagensLidas(cId, userId, false);
			if(lida) {
				cidsLidas.add(cId);
			}
		}

		return cidsLidas;
	}
}