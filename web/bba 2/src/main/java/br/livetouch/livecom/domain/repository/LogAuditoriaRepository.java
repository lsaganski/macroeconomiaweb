package br.livetouch.livecom.domain.repository;

import java.util.List;

import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.tiger.ddd.DomainException;
import net.livetouch.tiger.ddd.repository.Repository;

public interface LogAuditoriaRepository extends Repository<LogAuditoria>{

	List<LogAuditoria> reportAuditoria(RelatorioFiltro filtro) throws DomainException;

	List<LogAuditoria> envioPosts(RelatorioFiltro filtro) throws DomainException;
	Long countEnvioPosts(RelatorioFiltro filtro) throws DomainException;

	LogAuditoria findFirstReport(LogAuditoria a);

	List<LogAuditoria> getAuditoriasByEntity(Long id, AuditoriaEntidade entidade);


}