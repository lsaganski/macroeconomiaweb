package br.livetouch.livecom.web.pages.admin;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Complemento;
import net.livetouch.click.control.link.Link;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LinkDecorator;
import net.sf.click.extras.control.LongField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class ComplementoPage extends LivecomAdminPage {

	public PaginacaoTable table = new PaginacaoTable();
	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public Complemento complemento;

	public List<Complemento> complementos;
	
	@Override
	public void onInit() {
		super.onInit();

		form();
		
		table();
	}
	
	private void table() {
		Column c = new Column("id",getMessage("codigo.label"));
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("nome", getMessage("nome.label"));
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes",getMessage("coluna.detalhes.label"));
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	public void form(){
//		form.add(new IdField());
		LongField lId = new LongField("id");
		form.add(lId);

		TextField tNome = new TextField("nome", getMessage("nome.label"));
		tNome.setAttribute("autocomplete", "off");
		tNome.setAttribute("rel", "Adicionar complemento");
		tNome.setAttribute("class", "mascara");
		form.add(tNome);
		
		if (id != null) {
			complemento = complementoService.get(id);
		} else {
			complemento = new Complemento();
		}
		
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip botao export");
		form.add(tExportar);

		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);
		
		Submit cancelar = new Submit("novo", getMessage("novo.label"), this,"novo");
		cancelar.setAttribute("class", "botao btSalvar");
		form.add(cancelar);

		form.copyFrom(complemento);
		
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				complemento = id != null ? complementoService.get(id) : new Complemento();
				form.copyTo(complemento);

				complementoService.saveOrUpdate(complemento, getEmpresa());

				setMessageSesssion(getMessage("msg.complemento.salvar.sucess",complemento.getNome()), getMessage("msg.complemento.salvar.label.sucess"));
				
				setRedirect(ComplementoPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(ComplementoPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		complemento = complementoService.get(id);
		form.copyFrom(complemento);
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Complemento e = complementoService.get(id);
			if(id.equals(1L) && "livecom".equalsIgnoreCase(e.getNome())) {
				throw new DomainException(getMessage("msg.complemento.livecom.excluir.error"));
			}
			complementoService.delete(e);
			setRedirect(ComplementoPage.class);
			setMessageSesssion(getMessage("msg.complemento.excluir.sucess"), getMessage("msg.complemento.excluir.label.sucess"));
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.complemento.excluir.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		complementos = complementoService.findAll(getEmpresa());

		// Count(*)
		int pageSize = 100;
		long count = complementos.size();

		createPaginator(page, count, pageSize);

		table.setPageSize(pageSize);
		table.setRowList(complementos);
	}
	
	public boolean exportar(){
		String csv = complementoService.csvComplemento(getEmpresa());
		download(csv, "complementos.csv");
		return true;
	}
}
