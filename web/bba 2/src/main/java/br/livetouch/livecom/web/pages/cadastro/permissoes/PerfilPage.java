package br.livetouch.livecom.web.pages.cadastro.permissoes;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.LivecomRootEmpresaPage;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;


/**
 * 
 * @author ricardo
 *
 */
@Controller
@Scope("prototype")
public class PerfilPage extends LivecomRootEmpresaPage {

		public Form form = new Form();
		
		@Override
		public void onInit() {
			super.onInit();
			escondeChat = true;
			bootstrap_on = true;
			form();
		}
		
		public void form() {

			form.add(getCheck("padrao"));
			form.add(getCheck("cadastrarTabelas"));
			form.add(getCheck("cadastrarUsuarios"));
			form.add(getCheck("publicar"));
			form.add(getCheck("editarPostagem"));
			form.add(getCheck("excluirPostagem"));
			form.add(getCheck("comentar"));
			form.add(getCheck("visualizarConteudo"));
			form.add(getCheck("visualizarComentario"));
			form.add(getCheck("curtir"));
			form.add(getCheck("arquivos"));
			form.add(getCheck("enviarMensagem"));
			form.add(getCheck("codigo"));
			form.add(getCheck("visualizarRelatorio"));
			
		}
		
		private Checkbox getCheck(String nome){
			Checkbox check = new Checkbox(nome);
			check.setAttribute("class", "bootstrap");
			check.setAttribute("data-off-color", "danger");
			check.setAttribute("data-off-text", "Não");
			check.setAttribute("data-on-text", "Sim");
			check.setId(nome);
			return check;
			
		}
}
