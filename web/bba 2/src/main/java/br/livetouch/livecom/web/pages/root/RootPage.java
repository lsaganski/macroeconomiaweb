package br.livetouch.livecom.web.pages.root;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.LivecomRootPage;

@Controller
@Scope("prototype")
public class RootPage extends LivecomRootPage {
	@Override
	public void onInit() {
		super.onInit();
	}
}
