package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Playlist;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;

public interface PlaylistService extends Service<Playlist> {

	List<Playlist> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Playlist f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Playlist f) throws DomainException;

	List<Playlist> findAllByUser(Usuario u);

	List<Post> findAllPostsByUser(Usuario u, int page, int max);
	
	Playlist findByUserPost(Usuario u, Post p);
}
