package br.livetouch.livecom.web.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.admin.LivecomAdminPage;

@Controller
@Scope("prototype")
public class ConvitePage extends LivecomAdminPage {

	public Long id;
	
	@Override
	public void onInit() {
		super.onInit();
		
		logoutUserInfo();

		if(id != null) {
			setRedirect(LogonPage.class,"id",id.toString());
		} else {
			setRedirect(LogonPage.class);
		}
	}
}
