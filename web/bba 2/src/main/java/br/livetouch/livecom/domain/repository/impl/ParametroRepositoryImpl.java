package br.livetouch.livecom.domain.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.enums.TipoParametro;
import br.livetouch.livecom.domain.repository.ParametroRepository;
import br.livetouch.livecom.domain.vo.ParametroVO;
import br.livetouch.spring.StringHibernateRepository;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public class ParametroRepositoryImpl extends StringHibernateRepository<Parametro> implements ParametroRepository {

	public ParametroRepositoryImpl() {
		super(Parametro.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Parametro> findAll() {
		StringBuffer sb = new StringBuffer("FROM Parametro p WHERE 1=1 and p.empresa is not null");
		sb.append(" ORDER BY p.empresa.id");
		Query q = createQuery(sb.toString());
		q.setCacheable(true);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Parametro> findDefaultByNomeNotIn(List<String> nomes) {
		Query q = createQuery("FROM Parametro p WHERE p.nome not in(:nomes) AND p.empresa.id = 1");
		q.setParameterList("nomes", nomes);
		q.setCacheable(true);
		return q.list();
	}

	@Override
	public Parametro findByNome(String nome, Empresa empresa) {
		StringBuffer sb = new StringBuffer("FROM Parametro p where p.nome = :nome AND p.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setParameter("nome", nome);
		q.setParameter("empresa", empresa);
		Parametro p = (Parametro) (q.list().size() > 0 ? q.list().get(0) : null);
		return p;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Parametro> findAllByEmpresa(Long id) {
		
		if(id == null) {
			return new ArrayList<Parametro>();
		}
		
		StringBuffer sb = new StringBuffer("FROM Parametro p where p.empresa.id = :id");
		Query q = createQuery(sb.toString());
		q.setParameter("id", id);
		q.setCacheable(true);
		return q.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ParametroVO> parametrosAutocomplete(Long id, String nome) {
		
		StringBuffer sb = new StringBuffer("SELECT new br.livetouch.livecom.domain.vo.ParametroVO(p.nome, p.valor) FROM Parametro p where 1=1");
		sb.append(" and p.empresa.id = 1 ");
		
		if(id != null) {
			sb.append(" and p.nome not in (select param.nome from Parametro param where param.empresa.id = :id)");
		}
		
		sb.append(" and p.nome like :nome");
		
		Query q = createQuery(sb.toString());

		if(id != null) {
			q.setParameter("id", id);
		}
		
		q.setParameter("nome", "%" + nome + "%");
		q.setCacheable(true);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Parametro> findAllParametros(Empresa e, Integer page, Integer max, boolean count) throws DomainException {
		Query query = getQueryFindByFilter(e, count);

		if (!count) {
			int firstResult = 0;
			if (page != 0) {
				firstResult = page * max;
			}
			query.setFirstResult(firstResult);
			query.setMaxResults(max);
		}

		query.setCacheable(true);
		List<Parametro> parametros = query.list();

		return parametros;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Parametro> findAllParametros(Empresa e) throws DomainException {
		Query query = getQueryFindByFilter(e, false);
		query.setCacheable(true);
		List<Parametro> parametros = query.list();

		return parametros;
	}
	
	@Override
	public long getCount(Empresa e, int page, int max, boolean count) throws DomainException {
		Query query = getQueryFindByFilter(e, count);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}
	
	private Query getQueryFindByFilter(Empresa e, boolean count) throws DomainException {

		StringBuffer sb = new StringBuffer();
		if (count) {
			sb.append("select count(distinct id) from Parametro p");
		} else {
			sb.append("from Parametro p");
		}
		
		sb.append(" WHERE p.empresa.id = :empresaId");

		Query q = createQuery(sb.toString());
		q.setParameter("empresaId", e.getId());
		q.setCacheable(true);
		return q;
	}
	
	@Override
	public long getCount() {
		String q = ParametrosMap.getInstance().get("ping.query","select count(*) from Parametro");
		Query query = createQuery(q);
		query.setCacheable(false);
		
		return (long) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Parametro> findAllParametrosByTipo(TipoParametro tipo, Empresa empresa) {
		StringBuffer sb = new StringBuffer("FROM Parametro p where 1=1");
		
		if(tipo != null) {
			sb.append(" and p.tipoParametro = :tipoParametro ");
			
			if(tipo.equals(TipoParametro.SERVER)) {
				sb.append(" or p.tipoParametro IS NULL ");
				
			}
		}

		if(empresa != null) {
			sb.append(" and p.empresa = :empresa");
		}

		Query q = createQuery(sb.toString());

		if(tipo != null) {
			q.setParameter("tipoParametro", tipo);
		}

		if(empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		q.setCacheable(true);
		return q.list();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Parametro> findAll(Long empresaId, TipoParametro tipoParametro) {
		Query q = createQuery("FROM Parametro p where p.empresa.id = :empresaId and p.tipoParametro = :tipo");
		q.setParameter("empresaId", empresaId);
		q.setParameter("tipo", tipoParametro);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Parametro> findParametrosMobile(Empresa empresa) {
		StringBuffer sb = new StringBuffer("FROM Parametro p where p.tipoParametro = :tipo and p.empresa = :empresa");
		Query q = createQuery(sb.toString());
		q.setParameter("empresa", empresa);
		q.setParameter("tipo", TipoParametro.MOBILE);
		return q.list();
	}

    @Override
    public List<Parametro> findMobileDefault() {
        StringBuffer sb = new StringBuffer("FROM Parametro p where p.tipoParametro = :tipo and p.empresa is null");
        Query q = createQuery(sb.toString());
        q.setParameter("tipo", TipoParametro.MOBILE);
        return q.list();
    }
}