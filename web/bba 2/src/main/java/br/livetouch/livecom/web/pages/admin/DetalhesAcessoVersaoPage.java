package br.livetouch.livecom.web.pages.admin;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.LogTransacao;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.Context;
import net.sf.click.control.Column;
import net.sf.click.control.Decorator;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

@Controller
@Scope("prototype")
public class DetalhesAcessoVersaoPage extends LivecomAdminPage {

	public String versao;
	public LogTransacao log;
	public RelatorioFiltro filtro;
	public List<RelatorioAcessosVersaoVO> vos;
	public PaginacaoTable table = new PaginacaoTable();
	public int page;
	public Form form = new Form();
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onGet() {
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro != null) {
			filtro.setPage(page);
		}
	}

	@Override
	public void onInit() {

		if (StringUtils.isNotEmpty(versao)) {
			filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			if (filtro != null) {
				filtro.setVersao(versao);
				filtro.setPage(page);
			}
		}

		form();
		table();
	}

	@Override
	public String getContentType() {
		return super.getContentType();
	}

	@Override
	public String getTemplate() {
		return super.getTemplate();
	}

	private void table() {

		Column c = new Column("data", getMessage("data.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);
		
		c = new Column("login", getMessage("coluna.login.label"));
		c.setDecorator(new Decorator() {
			@Override
			public String render(Object object, Context context) {
				RelatorioAcessosVersaoVO rel = (RelatorioAcessosVersaoVO) object;
				if (rel != null) {
					Usuario user = usuarioService.findByLogin(rel.getLogin());
					if (user != null) {
						String link = String.format("<a href=\"%s/usuario.htm?id=%s\">%s</a>", getContextPath(), user.getId(), rel.getLogin());
						return link;
					}
				}
				return rel.getLogin();
			}

		});
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("versao", getMessage("coluna.versao.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("deviceSo", "Device SO");
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("deviceSoVersion", "Device Versão");
		c.setAttribute("align", "left");
		table.addColumn(c);

	}
	
	public void form() {
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "btn btn-outline blue min-width");
		form.add(tExportar);
	}

	@Override
	public void onRender() {
		super.onRender();

		if (filtro != null) {
			filtro.setMax(20);

			long acessosCount = 20;
			List<RelatorioAcessosVersaoVO> acessos = null;
			try {
				acessos = logService.findLogsByVersao(filtro, getEmpresa());
				acessosCount = logService.getCountLogsByVersao(filtro, getEmpresa());
			} catch (DomainException e) {
				setFlashAttribute(getMessage("erro.label"), e.toString());
			}
			table.setCount(acessosCount);
			table.setPageSize(filtro.getMax());

			table.setRowList(acessos);
		}

	}
	
	public boolean exportar() throws DomainException {
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		String csv = logService.csvDetalhesAcessoVersao(filtro, getEmpresa());
		download(csv, "detalhes-acesso-versao.csv");

		return true;
	}
}
