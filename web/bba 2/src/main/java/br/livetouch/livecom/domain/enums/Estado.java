package br.livetouch.livecom.domain.enums;

import org.apache.commons.lang.StringUtils;

/**
 * @author Ricardo Lecheta
 * @created 22/02/2013
 */
public enum Estado {
	NA(0,"NENHUM"),				
	AC(1,"Acre"),				
	AL(2,"Alagoas"),			
	AM(3,"Amazonas"),			
	AP(4,"Amapá"),				
	BA(5,"Bahia"),				
	CE(6,"Ceará"),				
	DF(7,"Distrito Federal"),	
	ES(8,"Espírito Santo"),		
	GO(9,"Goiás"),				
	MA(10,"Maranhão"),				
	MG(11,"Minas Gerais"),			
	MS(12,"Mato Grosso do Sul"),	
	MT(13,"Mato Grosso"),			
	PA(14,"Pará"),				
	PB(15,"Paraíba"),			
	PE(16,"Pernambuco"),		
	PI(17,"Piauí"),				
	PR(18,"Paraná"),			
	RJ(19,"Rio de Janeiro"),	
	RN(20,"Rio Grande do Norte"),
	RO(21,"Rondônia"),			
	RR(22,"Roraima"),			
	RS(23,"Rio Grande do Sul"),	
	SC(24,"Santa Catarina"),	
	SE(25,"Sergipe"),			
	SP(26,"São Paulo"),			
	TO(27,"Tocantins");			

	private int ordinal;
	private final String nome;

	private Estado(int ordinal, String nome) {
		this.setOrdinal(ordinal);
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public String getSigla() {
		return name();
	}
	
	public static Estado from(String s) {
		Estado[] values = Estado.values();
		for (Estado e : values) {
			if(StringUtils.equalsIgnoreCase(s, e.name())) {
				return e;
			}
		}
		return Estado.SP;
	}

	public int getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(int ordinal) {
		this.ordinal = ordinal;
	}
}
