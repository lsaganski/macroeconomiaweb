package br.livetouch.livecom.domain.service;


import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostIdioma;
import net.livetouch.tiger.ddd.DomainException;

public interface PostIdiomaService extends br.livetouch.livecom.domain.service.Service<PostIdioma> {

	List<PostIdioma> findAll();
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(PostIdioma pi) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(PostIdioma pi) throws DomainException;

	Set<PostIdioma> findByPost(Post p);
	
}