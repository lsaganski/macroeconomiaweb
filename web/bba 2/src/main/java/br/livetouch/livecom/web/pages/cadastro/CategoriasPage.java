package br.livetouch.livecom.web.pages.cadastro;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboCategoriaPost;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.service.CategoriaPostService;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;
import net.sf.click.control.HiddenField;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class CategoriasPage extends CadastrosPage {

	@Autowired
	CategoriaPostService categoriaPostService;

	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this, "editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public boolean escondeChat = true;
	public boolean isShowThumbCrop = false;

	public int page;
	// public int lastPage;

	public CategoriaPost categoriaPost;

	public List<CategoriaPost> categorias;

	private ComboCategoriaPost comboCategoria;
	
	
	private HiddenField tCor;

	private TextField tCodigo;
	
	@Override
	public void onInit() {
		super.onInit();
		
		boolean subcategorias = getParametrosMap().getBoolean(Params.SUBCATEGORIAS_ON, false);
		if(subcategorias) {
			setRedirect(CategoriasTreePage.class);
		}
		
		form();
	}

	public void form() {
		IdField field = new IdField();
		field.setId("idCategoria");
		form.add(field);

		TextField tNome = new TextField("nome", getMessage("nome.label"), true);
		tNome.setAttribute("autocomplete", "off");
		tNome.setAttribute("rel", "Adicionar categoria");
		tNome.setAttribute("class", "placeholder");
		form.add(tNome);
		
		tCodigo = new TextField("codigo", false);
		tCodigo.setAttribute("autocomplete", "off");
		tCodigo.setAttribute("class", "placeholder input");
		form.add(tCodigo);
		
		form.add(new Checkbox("padrao", getMessage("padrao.label")));
		form.add(new Checkbox("menu", "menu"));
		form.add(new Checkbox("likeable", "likeable"));
		form.add(new Checkbox("hideData", "hideData"));
		

		form.add(comboCategoria = new ComboCategoriaPost(categoriaPostService, true, false, getEmpresa()));
		comboCategoria.addStyleClass("form-control input-sm");
		
		
		tCor = new HiddenField("cor", getMessage("cor.label"));
		form.add(tCor);

		if (id != null) {
			categoriaPost = categoriaPostService.get(id);
		} else {
			categoriaPost = new CategoriaPost();
		}

		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip botao export");
		form.add(tExportar);
		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao salvar popup_ok");
		form.add(tsalvar);

		form.add(new Submit("novo", getMessage("novo.label"), this, "novo"));

		form.copyFrom(categoriaPost);

		// setFormTextWidth(form);

	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				form.copyTo(categoriaPost);
				if(categoriaPost.getId() == null){
					categoriaPost.setOrdem(99999);
				}

				CategoriaPost categ = comboCategoria.getCategoriaPost();
				if (categ != null) {
					categoriaPost.setCategoriaParent(categ);
				} else {
					categoriaPost.setCategoriaParent(null);
				}

				categoriaPostService.saveOrUpdate(getUsuario(), categoriaPost);

				setMessageSesssion(getMessage("msg.salvar.categoria.sucess", categoriaPost.getNome()), getMessage("categoria.salva.sucess"));

				setRedirect(CategoriasPage.class);

				return false;

			} catch (Exception e) {
				logError(e.getMessage(), e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(CategoriasPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		categoriaPost = categoriaPostService.get(id);
		form.copyFrom(categoriaPost);

		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			CategoriaPost categoriaPost = categoriaPostService.get(id);
			if(categoriaPost == null) {
				setMessageSesssion("Categoria não encontrada");
				return true;
			}
			categoriaPostService.delete(getUsuario(), categoriaPost);
			setRedirect(CategoriasPage.class);
			setMessageSesssion(getMessage("msg.remover.categoria.sucess", categoriaPost.getNome()), getMessage("msg.excluir.categoria.sucess"));
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(), e);
			this.msgErro = getMessage("msg.excluir.categoria.error");
		} catch (DomainException e) {
			logError(e.getMessage(), e);
			this.msgErro = e.getMessage();
		} catch (Exception e) {
			logError(e.getMessage(), e);
			this.msgErro = getMessage("msg.excluir.categoria.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		categorias = categoriaPostService.findAll(getEmpresa());

		// Count(*)
		int pageSize = 10;
		long count = categorias.size();

		createPaginator(page, count, pageSize);
	}
	public boolean exportar(){
		String csv = categoriaPostService.csvCategoria(getEmpresa());
		download(csv, "categorias.csv");
		return true;
	}
}
