package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

public class UsuarioStatusCountVO implements Serializable{
	private static final long serialVersionUID = -2382934870408092352L;
	private Long ativo;
	private Long inativo;
	private Long novo;
	private Long total = 0L;
	
	public Long getAtivo() {
		return ativo;
	}
	public void setAtivo(Long ativo) {
		this.setTotal(this.getTotal() + ativo);
		this.ativo = ativo;
	}
	public Long getInativo() {
		return inativo;
	}
	public void setInativo(Long inativo) {
		this.setTotal(this.getTotal() + inativo);
		this.inativo = inativo;
	}
	public Long getNovo() {
		return novo;
	}
	public void setNovo(Long novo) {
		this.setTotal(this.getTotal() + novo);
		this.novo = novo;
	}
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
}
