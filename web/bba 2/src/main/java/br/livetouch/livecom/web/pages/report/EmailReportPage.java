package br.livetouch.livecom.web.pages.report;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboTipoTemplate;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.extras.util.DateUtils;
import net.sf.click.control.Form;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

@Controller
@Scope("prototype")
public class EmailReportPage extends RelatorioPage {

	public Form form = new Form();
	public RelatorioFiltro filtro;
	public int page;
	
	public Select status = new Select();
	public ComboTipoTemplate comboTipo;
	
	@Override
	public void onInit() {
		super.onInit();
		
		if (clear) {
			getContext().removeSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			filtro = new RelatorioFiltro();
			filtro.setDataIni(DateUtils.toString(br.livetouch.livecom.utils.DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
			filtro.setDataFim(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		} else {
			filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			if (filtro != null) {
				if (filtro.getUsuarioId() != null) {
					Usuario usuario = usuarioService.get(filtro.getUsuarioId());
					if (usuario != null) {
						filtro.setUsuario(usuario);
					}
				}
			}
		}
		
		form();
	}
	
	public void form() {
		form.add(comboTipo = new ComboTipoTemplate());
		status.add(new Option(true, "Enviado"));
		status.add(new Option(false, "Não Enviado"));
		form.add(status);
	}

}
