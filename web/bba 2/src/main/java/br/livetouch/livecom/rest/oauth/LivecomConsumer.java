package br.livetouch.livecom.rest.oauth;

import java.security.Principal;

import javax.ws.rs.core.MultivaluedMap;

import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap;
import org.glassfish.jersey.server.oauth1.OAuth1Consumer;

public class LivecomConsumer implements OAuth1Consumer {
	private String key;
	private String secret;
	private String owner;
	private MultivaluedMap<String, String> attributes;

	public LivecomConsumer(String key, String secret, String owner) {
		this.key = key;
		this.secret = secret;
		this.owner = owner;
		this.attributes = new MultivaluedStringMap();
	}

	public LivecomConsumer(String key, String secret, String owner,
			MultivaluedMap<String, String> attributes) {
		this.key = key;
		this.secret = secret;
		this.owner = owner;
		this.attributes = attributes;
	}

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getSecret() {
		return secret;
	}

	public String getOwner() {
		return owner;
	}

	public MultivaluedMap<String, String> getAttributes() {
		return attributes;
	}

	@Override
	public Principal getPrincipal() {
		return null;
	}

	@Override
	public boolean isInRole(final String role) {
		return false;
	}

}
