package br.livetouch.livecom.web.pages.training.pagseguro;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.Log;
import br.livetouch.livecom.utils.DummyUtils;
import br.livetouch.payment.MessageResponseException;
import br.livetouch.payment.PaymentResponse;
import br.livetouch.payment.PaymentService;


/**
 * @author Ricardo Lecheta
 * @created 20/03/2013
 */
@Controller
@Scope("prototype")
public class ConfirmationPage extends NotificationPage {
	protected static final Logger log = Log.getLogger(ConfirmationPage.class);

	public String compraId;
	public String transactionId;
	public String msgError;

	public PaymentResponse paymentResponse;
	public boolean pago;
	
	@Override
	public void onInit() {
		if(org.apache.commons.lang.StringUtils.isNotEmpty(compraId)) {
			boolean localhost = false;
			String host = localhost ? "localhost:8080": "54.207.75.112";
			PaymentService s = new PaymentService(host);

			try {
				System.out.println("ConfirmationPage transactionId ["+transactionId+"], compraId ["+compraId+"]");
				paymentResponse = s.confirmPagSeguro(transactionId, compraId);
				
				if(paymentResponse != null) {
					
					pago = paymentResponse.isPago();
					
					addModel("urlPagamento", paymentResponse.getUrl_payment());
					
					Long compraId = paymentResponse.getId();
					String numeroPedido = DummyUtils.toRight(compraId.toString(), 8, "0");
					addModel("numeroPedido", numeroPedido);
				}
				
			} catch (MessageResponseException e) {
				msgError = e.getMessage();
			}
		}
		
	}
}
