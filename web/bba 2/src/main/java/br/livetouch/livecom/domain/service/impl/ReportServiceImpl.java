package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.ReportRepository;
import br.livetouch.livecom.domain.repository.UsuarioRepository;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.ReportService;
import br.livetouch.livecom.domain.vo.QuantidadeVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioAudienciaVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioLoginVO;
import br.livetouch.livecom.domain.vo.RelatorioNotificationsVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoFiltro;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ReportServiceImpl implements ReportService {

	@Autowired
	private ReportRepository rep;

	@Autowired
	private UsuarioRepository repUsuario;
	
	@Autowired
	private NotificationService notificationService;

	@Override
	public List<RelatorioLoginVO> loginsResumido(RelatorioFiltro filtro) throws DomainException {
		return rep.loginsResumido(filtro);
	}

	@Override
	public long getCountloginsResumido(RelatorioFiltro filtro) throws DomainException {
		return rep.getCountloginsResumido(filtro);
	}

	@Override
	public List<RelatorioLoginVO> loginsExpandido(RelatorioFiltro filtro) throws DomainException {
		return rep.loginsExpandido(filtro);
	}

	@Override
	public List<RelatorioLoginVO> reportVersao(RelatorioFiltro filtro) throws DomainException {
		return rep.reportVersao(filtro);
	}

	public Map<String, List<QuantidadeVersaoVO>> reportVersaoGrafico(RelatorioFiltro filtro) throws DomainException{
		
		List<QuantidadeVersaoVO> versoes = rep.reportVersaoGrafico(filtro);
		
		Map<String, List<QuantidadeVersaoVO>> map = new HashMap<String, List<QuantidadeVersaoVO>>();
		for (QuantidadeVersaoVO vo : versoes) {
			String version = vo.getDeviceAppVersion();
			if(StringUtils.isNotEmpty(version)) {
				List<QuantidadeVersaoVO> list = map.get(version);
				if(list == null) {
					list = new ArrayList<QuantidadeVersaoVO>();
				}
				list.add(vo);
				map.put(version, list);
			}
			
		}
		
		return map;
		
		
	}

	@Override
	public String csvVisualizacao(RelatorioVisualizacaoFiltro filtro) {
		if (StringUtils.equals(filtro.getModo(), "consolidado")) {
			List<RelatorioVisualizacaoVO> vos = relVisualizacaoConsolidado(filtro);
			CSVGenerator generator = new CSVGenerator();
			CSVHeader header = generator.getHeader();
			CSVBody body = generator.getBody();
			header.add("Data").add("Autor").add("Comunicado").add("Quantidade");
			for (RelatorioVisualizacaoVO a : vos) {
				body.add(a.getData()).add(a.getNomeAutor()).add(a.getTitulo()).add(String.valueOf(a.getTotalUsers()));
				body.end();
			}
			return generator.toString();
		} else {
			List<RelatorioVisualizacaoVO> vos = relVisualizacao(filtro);
			CSVGenerator generator = new CSVGenerator();
			CSVHeader header = generator.getHeader();
			CSVBody body = generator.getBody();
			header.add("Data").add("Usuario").add("Post");
			for (RelatorioVisualizacaoVO a : vos) {
				body.add(a.getData()).add(a.getLoginAutor()).add(a.getTitulo());
				body.end();
			}
			return generator.toString();
		}
	}

	@Override
	public List<RelatorioVisualizacaoVO> relVisualizacao(RelatorioVisualizacaoFiltro filtro) {
		List<Notification> allByFiltro = notificationService.findViewsByFilter(filtro);
		List<RelatorioVisualizacaoVO> vos = new ArrayList<RelatorioVisualizacaoVO>();
		for (Notification not : allByFiltro) {
			RelatorioVisualizacaoVO vo = new RelatorioVisualizacaoVO(not);
			vos.add(vo);
		}
		return vos;
	}

	@Override
	public String csvDetalhesVisualizacao(Usuario usuario, Post post) {
//		List<PostViewVO> list = new ArrayList<PostViewVO>();
//		if (post != null) {
//			list = repPostView.findAllGroupUserByPostDate(usuario, post);
//		}
//		CSVGenerator generator = new CSVGenerator();
//		CSVHeader header = generator.getHeader();
//		CSVBody body = generator.getBody();
//		header.add("Data").add("Usuario").add("Post");
//		for (PostViewVO a : list) {
//			body.add(a.getData()).add(a.getUserLogin()).add(a.getPostTitulo());
//			body.end();
//		}
//		return generator.toString();
		//  RELATORIO CSV
		return "";
	}

	@Override
	public HashMap<String, HashMap<String, Long>> buildMapLogin(List<RelatorioLoginVO> logins) {

		HashMap<String, HashMap<String, Long>> map = new LinkedHashMap<String, HashMap<String, Long>>();

		for (RelatorioLoginVO vo : logins) {
			map.put(vo.getData(), new HashMap<String, Long>());
		}

		for (RelatorioLoginVO vo : logins) {
			map.get(vo.getData()).put(vo.getPlataforma(), vo.getCount());
		}

		return map;
	}

	@Override
	public List<RelatorioLoginVO> loginsAgrupado(RelatorioFiltro filtro) throws DomainException {
		return rep.loginsAgrupado(filtro);
	}

	@Override
	public String csvLoginReport(RelatorioFiltro filtro) throws DomainException {
		if (filtro.getUsuario() == null && filtro.getUsuarioId() != null) {
			filtro.setUsuario(repUsuario.get(filtro.getUsuarioId()));
		}
		if (filtro.getModo().equals("consolidado")) {
			List<RelatorioLoginVO> logins = loginsResumido(filtro);
			HashMap<String, HashMap<String, Long>> map = buildMapLogin(logins);
			CSVGenerator generator = new CSVGenerator();
			CSVHeader header = generator.getHeader();
			CSVBody body = generator.getBody();
			header.add("Data").add("Web").add("Android").add("IOS").add("Total");
			for (Entry<String, HashMap<String, Long>> a : map.entrySet())			{
				HashMap<String, Long> map2 = map.get(a.getKey());
				Long web = map2.containsKey("WEB") ? map2.get("WEB") : 0l;
				Long android = map2.containsKey("ANDROID") ? map2.get("ANDROID") : 0l;
				Long ios = map2.containsKey("IOS") ? map2.get("IOS") : 0l;
				body.add(a.getKey().toString()).add(web.toString()).add(android.toString()).add(ios.toString()).add(String.valueOf(web + android + ios));
				body.end();
			}
			return generator.toString();
		} else {
			List<RelatorioLoginVO> logins = loginsExpandido(filtro);
			CSVGenerator generator = new CSVGenerator();
			CSVHeader header = generator.getHeader();
			CSVBody body = generator.getBody();
			header.add("Data").add("Login").add("Plataforma");
			for (RelatorioLoginVO a : logins) {
				body.add(a.getData()).add(a.getLogin()).add(a.getPlataforma());
				body.end();
			}
			return generator.toString();
		}
	}

	@Override
	public String csvDetalhesLoginReport(RelatorioFiltro filtro) throws DomainException {
		if (!filtro.isAgrupar()) {

			List<RelatorioLoginVO> logins = loginsExpandido(filtro);
			CSVGenerator generator = new CSVGenerator();
			CSVHeader header = generator.getHeader();
			CSVBody body = generator.getBody();
			header.add("Data").add("Login").add("Plataforma");
			for (RelatorioLoginVO a : logins) {
				body.add(a.getData()).add(a.getLogin()).add(a.getPlataforma());
				body.end();
			}
			return generator.toString();
		} else {
			List<RelatorioLoginVO> logins = loginsAgrupado(filtro);
			CSVGenerator generator = new CSVGenerator();
			CSVHeader header = generator.getHeader();
			CSVBody body = generator.getBody();
			header.add("Data").add("Login").add("Plataforma").add("Qtde");
			for (RelatorioLoginVO a : logins) {
				body.add(a.getData()).add(a.getLogin()).add(a.getPlataforma()).add(String.valueOf(a.getCount()));
				body.end();
			}
			return generator.toString();
		}
	}

	@Override
	public String csvDetalhesAuditoriaPostReport(List<LogAuditoria> audits) throws DomainException {
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("Data").add("Entidade").add("Ação").add("Usuário").add("Mensagem").add("Titulo").add("Conteudo").add("Grupos").add("Categorias").add("json");
		for (LogAuditoria a : audits) {
			body.add(a.getValue4())
			.add(a.getEntidade().toString())
			.add(a.getAcao().toString())
			.add(a.getUsuario() != null ? a.getUsuario().getLogin() : a.getValue8())
			.add(StringUtils.defaultString(a.getValue3()))
			.add(StringUtils.defaultString(a.getValue2()))
			.add(StringUtils.isEmpty(a.getValue3()) ? "" : "\"" + a.getValue3() + "\"")
			.add(StringUtils.defaultString(a.getValue10(), a.getValue7()))
			.add(StringUtils.defaultString(a.getValue9(), a.getValue6()))
			.add(StringUtils.defaultString(a.getValue1()));
			body.end();
		}
		return generator.toString();
	}

	@Override
	public List<RelatorioVisualizacaoVO> relVisualizacaoConsolidado(RelatorioVisualizacaoFiltro filtro) {
		return rep.relVisualizacaoConsolidado(filtro);
	}

	@Override
	public String csvFavoritos(RelatorioFiltro filtro) throws DomainException {
		if (StringUtils.equals(filtro.getModo(), "consolidado")) {
			List<RelatorioNotificationsVO> favs = notificationService.reportNotificationsByType(filtro);
			CSVGenerator generator = new CSVGenerator();
			CSVHeader header = generator.getHeader();
			CSVBody body = generator.getBody();
			header.add("Data").add("Login").add("Qtde");
			for (RelatorioNotificationsVO a : favs) {
				body.add(a.getData()).add(a.getTitulo()).add(String.valueOf(a.getTotalUsers()));
				body.end();
			}
			return generator.toString();
		} else {
			List<RelatorioNotificationsVO> favs = notificationService.reportNotificationsByTypeDetalhes(filtro);
			CSVGenerator generator = new CSVGenerator();
			CSVHeader header = generator.getHeader();
			CSVBody body = generator.getBody();
			header.add("Data").add("Post").add("Login");
			for (RelatorioNotificationsVO a : favs) {
				body.add(a.getData()).add(a.getTitulo()).add(a.getNomeAutor());
				body.end();
			}
			return generator.toString();
		}
	}

	@Override
	public String csvDetalhesFavoritos(RelatorioFiltro filtro) throws DomainException {
		List<RelatorioNotificationsVO> favs = notificationService.reportNotificationsByTypeDetalhes(filtro);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("Data").add("Post").add("Login");
		for (RelatorioNotificationsVO a : favs) {
			body.add(a.getData()).add(a.getTitulo()).add(a.getNomeAutor());
			body.end();
		}
		return generator.toString();
	}
	
	@Override
	public List<RelatorioAudienciaVO> findAudiencia(RelatorioFiltro filtro) throws DomainException {
		return rep.findAudiencia(filtro);
	}

	@Override
	public long getCountAudiencia(RelatorioFiltro filtro) throws DomainException {
		return rep.getCountAudiencia(filtro);
	}
	
	@Override
	public List<RelatorioAudienciaVO> audienciaDetalhes(RelatorioFiltro filtro) throws DomainException {
		List<RelatorioAudienciaVO> audiencias = rep.audienciaDetalhes(filtro);
		for (RelatorioAudienciaVO vo : audiencias) {
			Usuario usuario = repUsuario.get(vo.getUsuarioId());
			if(usuario != null) {
				vo.setFotoUser(usuario.getUrlThumb());
			}
		}
		return audiencias;
	}

}
