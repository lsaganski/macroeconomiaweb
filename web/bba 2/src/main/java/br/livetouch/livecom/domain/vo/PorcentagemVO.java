package br.livetouch.livecom.domain.vo;

import java.util.HashMap;


public class PorcentagemVO {

	private String porcentagem;
	private String temArquivo;
	private String atual;
	private String fim;
	private String status;
	private String idImportacao;

	public PorcentagemVO (HashMap<String, String> map) {
		this.porcentagem = map.get("porcentagem");
		this.temArquivo = map.get("temArquivo");
		this.atual = map.get("atual");
		this.fim = map.get("fim");
		this.status = map.get("status");
		this.idImportacao = map.get("idImportacao");
	}

	public String getPorcentagem() {
		return porcentagem;
	}

	public void setPorcentagem(String porcentagem) {
		this.porcentagem = porcentagem;
	}

	public String getTemArquivo() {
		return temArquivo;
	}

	public void setTemArquivo(String temArquivo) {
		this.temArquivo = temArquivo;
	}

	public String getAtual() {
		return atual;
	}

	public void setAtual(String atual) {
		this.atual = atual;
	}

	public String getFim() {
		return fim;
	}

	public void setFim(String fim) {
		this.fim = fim;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIdImportacao() {
		return idImportacao;
	}

	public void setIdImportacao(String idImportacao) {
		this.idImportacao = idImportacao;
	}

}