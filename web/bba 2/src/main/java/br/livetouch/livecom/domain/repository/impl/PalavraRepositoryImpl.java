package br.livetouch.livecom.domain.repository.impl;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Palavra;
import br.livetouch.livecom.domain.repository.PalavraRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class PalavraRepositoryImpl extends StringHibernateRepository<Palavra> implements PalavraRepository {

	public PalavraRepositoryImpl() {
		super(Palavra.class);
	}	

}