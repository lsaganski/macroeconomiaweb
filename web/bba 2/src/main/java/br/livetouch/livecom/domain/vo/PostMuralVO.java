package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Favorito;
import br.livetouch.livecom.domain.Post;

public class PostMuralVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1137789963958503817L;
	public String like;
	public String favorito;

	public Long likeCount;
	public Long commentCount;
	private final Post post;

	public PostMuralVO(Post post,String like, Favorito f, Object likeCount, Object commentCount) {
		super();
		this.post = post;
		this.like = like;
		if(f != null) {
			this.favorito = (f != null && f.isFavorito()) ? "1" : "0";
		}
		this.likeCount = NumberUtils.toLong(likeCount.toString());
		this.commentCount = NumberUtils.toLong(commentCount.toString());
	}

	public String getLike() {
		return like;
	}

	public void setLike(String like) {
		this.like = like;
	}

	public String getFavorito() {
		return favorito;
	}

	public void setFavorito(String favorito) {
		this.favorito = favorito;
	}

	public Long getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(Long likeCount) {
		this.likeCount = likeCount;
	}

	public Long getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(Long commentCount) {
		this.commentCount = commentCount;
	}
	
	public Post getPost() {
		return post;
	}
	
	@Override
	public String toString() {
		return "post="+post.getTituloDesc()+ ", like=" + like + ", favorito=" + favorito + ", likeCount=" + likeCount + ", commentCount=" + commentCount;
	}

	
}
