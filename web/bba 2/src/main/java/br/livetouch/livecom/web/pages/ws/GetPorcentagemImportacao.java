package br.livetouch.livecom.web.pages.ws;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.vo.PorcentagemVO;
import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;

@Controller
@Scope("prototype")
public class GetPorcentagemImportacao extends WebServiceFormPage {
	public JobInfo jobInfo;

	@Override
	protected void form() {
	}

	@Override
	protected Object go() throws Exception {
		jobInfo = JobInfo.getInstance(getEmpresaId());
		HashMap<String, String> porcentagem = new HashMap<String, String>();
		String importOn = ParametrosMap.getInstance(getEmpresaId()).get(Params.IMPORTACAO_ARQUIVO_ON, "0");
		if(!StringUtils.equals(importOn, "1")){
			porcentagem.put("porcentagem", "0,00");
			porcentagem.put("temArquivo", "false");
			porcentagem.put("atual", "0");
			porcentagem.put("fim", "0");
			porcentagem.put("status", "importacao.arquivo.on");
			if(jobInfo != null && jobInfo.getResponse() != null && jobInfo.getResponse().idArquivo != null) {
				porcentagem.put("idImportacao", jobInfo.getResponse().idArquivo.toString());
			}
			return new PorcentagemVO(porcentagem);
		}

		porcentagem.put("porcentagem", String.format("%.2f", jobInfo.porcentagem));
		porcentagem.put("temArquivo", String.valueOf(jobInfo.temArquivo));
		porcentagem.put("atual", String.valueOf(jobInfo.atual));
		porcentagem.put("fim", String.valueOf(jobInfo.fim));
		if(jobInfo.getResponse() != null && jobInfo.getResponse().idArquivo != null) {
			porcentagem.put("idImportacao", jobInfo.getResponse().idArquivo.toString()); 
		}
		porcentagem.put("status", jobInfo.status);
		
		PorcentagemVO vo = new PorcentagemVO(porcentagem);
		
		return vo;
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("porcentagem", PorcentagemVO.class);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return false;
	}
}
