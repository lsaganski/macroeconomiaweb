package br.livetouch.livecom.web.pages.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.pages.MuralPage;

/**
 * 
 * @author ricardo
 *
 */
@Controller
@Scope("prototype")
public class SistemaPage extends LivecomLogadoPage {
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
		Usuario u = getUsuario();
		if (u != null) {
			
			if(hasPermissoes(false, ParamsPermissao.CONFIGURACOES_AVANCADAS, ParamsPermissao.VIZUALIZAR_RELATORIOS, ParamsPermissao.IMPORTACAO, ParamsPermissao.EXPORTACAO)) {
				return true;
			}
		}
		
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
	}
}
