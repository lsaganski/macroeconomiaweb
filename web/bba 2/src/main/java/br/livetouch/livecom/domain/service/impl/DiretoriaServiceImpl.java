package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Diretoria;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.repository.DiretoriaRepository;
import br.livetouch.livecom.domain.service.DiretoriaService;
import br.livetouch.livecom.domain.vo.Filtro;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class DiretoriaServiceImpl extends LivecomService<Diretoria> implements DiretoriaService {
	@Autowired
	private DiretoriaRepository rep;

	@Override
	public Diretoria get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Usuario userInfo, Diretoria d, Empresa empresa) throws DomainException {
		d.setEmpresa(empresa);
		rep.saveOrUpdate(d);
		
		auditSave(userInfo, d);
	}

	protected void auditSave(Usuario userInfo, Diretoria d) {
		boolean insert = d.getId() == null;
		AuditoriaAcao acao = insert ? AuditoriaAcao.INSERIR : AuditoriaAcao.EDITAR;
		
		String msg = null;
		if(insert) {
			msg = "Diretoria " + d.getNome() + " foi cadastrada";
		} else {
			msg = "Diretoria " + d.getNome() + " foi editada";
		}
		
		LogAuditoria.log(userInfo, d, AuditoriaEntidade.DIRETORIA, acao, msg);
	}

	@Override
	public List<Diretoria> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	@Override
	public void delete(Usuario userInfo, Diretoria d) throws DomainException {
		AuditoriaAcao acao = AuditoriaAcao.DELETAR;
		
		Empresa empresa = null;
		LogAuditoria auditoria = null;
		try {
			
			// Auditoria
			if(userInfo != null) {
				String msg = "Diretoria " + d.getNome() + " deletada";
				auditoria = LogAuditoria.log(userInfo, d, AuditoriaEntidade.DIRETORIA, acao, msg,false);
				empresa = userInfo.getEmpresa();
			}

			// Nao pode excluir diretoria com usuarios.
			boolean r1 = ParametrosMap.getInstance(empresa).getBoolean(Params.DELETAR_DIRETORIA_R1_ON,true);
			// Nao pode excluir diretoria com areas.
			boolean r2 = ParametrosMap.getInstance(empresa).getBoolean(Params.DELETAR_DIRETORIA_R2_ON,true);
			// Nao pode excluir diretoria com diretoria executiva.
			boolean r3 = ParametrosMap.getInstance(empresa).getBoolean(Params.DELETAR_DIRETORIA_R3_ON,true);

			if(r1) {
				List<Long> userIds = queryIds("select u.id from Usuario u where u.diretoria.id = ?", false, d.getId());
				if (userIds.size() > 0) {
					throw new DomainException("Não foi possivel excluir esta diretoria, pois existem " + userIds.size() + " usuários associados.");
				}
			}

			if(r2) {
				List<Long> areasIds = queryIds("select a.id from Area a where a.diretoria.id = ?", false, d.getId());
				if (areasIds.size() > 0) {
					throw new DomainException("Não foi possivel excluir esta diretoria, pois existem " + areasIds.size() + " áreas associadas.");
				}
			}

			if(r3) {
				List<Long> diretoriasIds = queryIds("select d.id from Diretoria d where d.diretoriaExecutiva.id = ?", false, d.getId());
				if (diretoriasIds.size() > 0) {
					throw new DomainException("Não foi possivel excluir esta diretoria, pois existem " + diretoriasIds.size() + " diretorias executiva associadas.");
				}
			}

			// Delete
			rep.delete(d);
			
			// OK
			if(auditoria != null) {
				auditoria.finish();
			}
		} catch (DomainException e) {
			throw e;
		} catch (Exception e) {
			throw new DomainException("Não foi possível excluir a diretoria [" + d + "]", e);
		}
	}

	@Override
	public List<Diretoria> filterDiretoria(Filtro filtroDiretoria, Empresa empresa) {
		return rep.filterDiretoria(filtroDiretoria, empresa);
	}

	@Override
	public List<Object[]> findIdCodDiretorias(Long idEmpresa) {
		return rep.findIdCodDiretorias(idEmpresa);
	}

	@Override
	public Diretoria findDefaultByEmpresa(Long empresaId) {
		return rep.findDiretoriaDefaultByEmpresa(empresaId);
	}

	@Override
	public String csvDiretoria(Empresa empresa) {
		List<Diretoria> findAll = findAll(empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("CODIGO").add("NOME").add("DIRETORIA_EXECUTIVA");
		for (Diretoria d : findAll) {
			body.add(d.getCodigoDesc()).add(d.getNome()).add(d.getDiretoriaExecutiva() != null ? d.getDiretoriaExecutiva().getCodigoDesc() : "");
			body.end();
		}
		return generator.toString();
	}

	@Override
	public List<Object[]> findIdNomeDiretorias(Long empresaId) {
		return rep.findIdNomeDiretorias(empresaId);
	}

	@Override
	public List<Diretoria> findByNome(String nome, int page, int maxRows, Empresa empresa) {
		return rep.findByNome(nome, page, maxRows, empresa);
	}

	@Override
	public Long countByNome(String nome, int page, int maxRows, Empresa empresa) {
		return rep.countByNome(nome, page, maxRows, empresa);
	}

	@Override
	public Diretoria findByName(Diretoria diretoria, Empresa empresa) {
		return rep.findByName(diretoria, empresa);
	}

	@Override
	public Diretoria findByCodigo(Diretoria diretoria, Empresa empresa){
		return rep.findByCodigo(diretoria, empresa);
	}
	
		
	
	
}
