package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Set;

import br.livetouch.livecom.domain.Favorito;
import br.livetouch.livecom.domain.Likes;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.DateUtils;

public class RelatorioVisualizacaoVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String data;
	private String titulo;
	private String texto;
	private String dataPush;
	private Long idAutor;
	private String nomeAutor;
	private String loginAutor;
	private int totalUsers;
	private Long postId;
	private Long count;
	private Long countLida;

	public RelatorioVisualizacaoVO() {

	}

	public RelatorioVisualizacaoVO(Notification notification) {
		this.id = notification.getId();
		this.titulo = notification.getTitulo();
		this.texto = notification.getTexto();
		this.data = DateUtils.toString(notification.getData(), "dd/MM/yyyy HH:mm");
		this.dataPush = DateUtils.toString(notification.getDataPush(), "dd/MM/yyyy HH:mm");
		Usuario u = notification.getUsuario();
		this.idAutor = u.getId();
		this.nomeAutor = u.getNome();
		this.loginAutor = u.getLogin();
		this.postId = notification.getPost() != null ? notification.getPost().getId() : null;
		Set<NotificationUsuario> notifications = notification.getNotifications();
		if(notifications != null) {
			this.totalUsers = notifications.size();
		}
	}
	
	/*
	 * Utilizado para o report de favoritos 
	 */
	public RelatorioVisualizacaoVO(String data, Long count, Post post, Favorito fav) {
		this.data = data;
		this.count = count;
		if(post != null) {
			this.titulo = post.getTitulo();
			this.id = post.getId();
		} else {
			this.titulo = " - ";
			this.id = 0L;
		}
		
		if(fav != null) {
			this.idAutor = fav.getUsuario() != null ? fav.getUsuario().getId() : 0L;
			this.nomeAutor = fav.getUsuario() != null ? fav.getUsuario().getNome() : " - ";
			this.loginAutor = fav.getUsuario() != null ? fav.getUsuario().getLogin() : " - ";
		} else {
			this.idAutor = 0L;
			this.nomeAutor = " - ";
			this.loginAutor = " - ";
		}
	}
	
	/*
	 * Utilizado para o report de likes 
	 */
	public RelatorioVisualizacaoVO(String data, Long count, Post post, Likes l) {
		this.data = data;
		this.count = count;
		if(post != null) {
			this.titulo = post.getTitulo();
			this.id = post.getId();
		} else {
			this.titulo = " - ";
			this.id = 0L;
		}
		
		if(l != null) {
			this.idAutor = l.getUsuario() != null ? l.getUsuario().getId() : 0L;
			this.nomeAutor = l.getUsuario() != null ? l.getUsuario().getNome() : " - ";
			this.loginAutor = l.getUsuario() != null ? l.getUsuario().getLogin() : " - ";
		} else {
			this.idAutor = 0L;
			this.nomeAutor = " - ";
			this.loginAutor = " - ";
		}
	}
	
	/*
	 * Utilizado para o report de visualização 
	 */
	public RelatorioVisualizacaoVO(String date, Post post, Long count, Long countLida) {
		this.data = date;
		this.count = count;
		this.countLida = countLida;
		if(post != null) {
			this.titulo = post.getTitulo();
			this.id = post.getId();
			this.idAutor = post.getUsuario() != null ? post.getUsuario().getId() : 0L;
			this.nomeAutor = post.getUsuario() != null ? post.getUsuario().getNome() : " - ";
			this.loginAutor = post.getUsuario() != null ? post.getUsuario().getLogin() : " - ";
		} else {
			this.titulo = " - ";
			this.id = 0L;
			this.idAutor = 0L;
			this.nomeAutor = " - ";
			this.loginAutor = " - ";
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getDataPush() {
		return dataPush;
	}

	public void setDataPush(String dataPush) {
		this.dataPush = dataPush;
	}

	public Long getIdAutor() {
		return idAutor;
	}

	public void setIdAutor(Long idAutor) {
		this.idAutor = idAutor;
	}

	public String getNomeAutor() {
		return nomeAutor;
	}

	public void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}

	public int getTotalUsers() {
		return totalUsers;
	}

	public void setTotalUsers(int totalUsers) {
		this.totalUsers = totalUsers;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getLoginAutor() {
		return loginAutor;
	}

	public void setLoginAutor(String loginAutor) {
		this.loginAutor = loginAutor;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getCountLida() {
		return countLida;
	}

	public void setCountLida(Long countLida) {
		this.countLida = countLida;
	}

}