package br.livetouch.livecom.web.pages.training.pagseguro;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.Log;
import br.livetouch.payment.MessageResponseException;
import br.livetouch.payment.PaymentResponse;
import br.livetouch.payment.PaymentService;

/**
 * @author Ricardo Lecheta
 * @created 03/05/2013
 */
@Controller
@Scope("prototype")
public class NotificationPage extends PaymentPage {
	protected static final Logger log = Log.getLogger(NotificationPage.class);

	public String compraId;
	public String notificationCode;
	public String msgError;
	
	public PaymentResponse response;
	public boolean pago;

	@Override
	public void onInit() {

		boolean localhost = false;
		String host = localhost ? "localhost:8080": "54.207.75.112";
		PaymentService s = new PaymentService(host);

		try {
			System.out.println("ConfirmationPage notificationCode ["+notificationCode+"], compraId ["+compraId+"]");
			response = s.notifyPagSeguro(notificationCode, compraId);
			
			if(response.isPago()) {
				pago = true;
			}
			
		} catch (MessageResponseException e) {
			msgError = e.getMessage();
		}
	}
}