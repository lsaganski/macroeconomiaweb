package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.List;

public class UsuariosVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public List<UsuarioVO> usuarios;
	public Long total;
	
}
