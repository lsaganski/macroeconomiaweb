package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import br.livetouch.livecom.domain.Campo;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Usuario;

public class CamposMap implements Serializable {

	private static final long serialVersionUID = 2720810798933945542L;
	private Map<String,Campo> camposMap = new HashMap<String, Campo>();
	private boolean admin;
	private final Usuario userInfo;
	
	private CamposMap(Usuario userInfo) {
		this.userInfo = userInfo;
		if(userInfo != null) {
			this.admin = userInfo.isAdmin();
		}
	}
	
	public static CamposMap getInstance(Usuario userInfo) {
		CamposMap c = new CamposMap(userInfo);
		Empresa e = null;
		if(userInfo != null) {
			e = userInfo.getEmpresa();
		}
		
		if(e != null) {
			c.camposMap = CamposMapCache.getInstance(e).getMap();
		} else {
			c.camposMap = CamposMapCache.getInstance().getMap();
		}
		
		return c;
	}
	
	public Campo getCampo(String nome) {
		Campo c = camposMap.get(nome);
		
		if(c == null) {
			return c = CamposMapCache.getInstance().getMap().get(nome);
		}
		
		return c;
	}
	
	public boolean isCampoReadOnly(String campo) {
		if (userInfo != null) {
			Perfil permissao = userInfo.getPermissao();
			if (permissao != null && Livecom.getInstance().hasPermissao(ParamsPermissao.CADASTRAR_USUARIOS, userInfo)) {
				return false;
			}
			if (admin) {
				return false;
			}
			if (camposMap != null) {
				Campo c = getCampo(campo);
				if (c != null) {
					return c.getEditavel() == 0;
				} else {
					return false;
				}
			}
		}
		return false;
	}
	
	public boolean isCampoPublic(String campo) {
		return isCampoPublic(campo, null);
	}
	
	public boolean isCampoPublic(String campo, Long idUserLogado) {
		if (userInfo != null) {
			Perfil p = userInfo.getPermissao();
			if (p != null && Livecom.getInstance().hasPermissao(ParamsPermissao.CADASTRAR_USUARIOS, userInfo)) {
				return true;
			}
			if (admin) {
				return true;
			}
			if (userInfo != null && idUserLogado != null && userInfo.getId().equals(idUserLogado)) {
				return true;
			}
			if (camposMap != null) {
				Campo c = getCampo(campo);
				if (c != null) {
					return c.getPublico() == 1;
				} else {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean isCampoObrigatorioCadastroAdmin(String campo, boolean defaultValue) {
		if(camposMap != null) {
			Campo c = getCampo(campo);
			if(c != null) {
				
				if(isCampoVisivel(campo)) {
					return c.getObrigatorioCadastroAdmin() == 1;
				} else {
					return false;
				}
				
			}else{
				return defaultValue;
			}	
		}
		return defaultValue;
	}
	
	public boolean isCampoVisivel(String campo) {
		if(camposMap != null) {
			Campo c = getCampo(campo);
			if(c != null) {
				return c.getVisivel() == 1;
			}else{
				return false;
			}	
		}
		return true;
	}


	@Override
	public String toString() {
		return camposMap != null ? camposMap.toString() : "";
	}

	public boolean isCampoObrigatorioCadastroAdmin(String campo) {
		return isCampoObrigatorioCadastroAdmin(campo, false);
	}
}
