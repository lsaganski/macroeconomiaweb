package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Cidade;
import br.livetouch.livecom.domain.enums.Estado;
import br.livetouch.livecom.domain.vo.CidadeVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class CidadesPage extends WebServiceXmlJsonPage {

	public Form form = new Form();

	private TextField tMode;
	public List<CidadeVO> list;
	private TextField tEstado;
	private TextField tCidade;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		form.add(tEstado = new TextField("estado"));
		form.add(tCidade = new TextField("cidade"));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));
		form.add(new TextField("maxRows"));
		form.add(new TextField("page"));
		
		tMode.setValue("json");
		tEstado.setFocus(true);

		form.add(new Submit("Consultar"));

	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			String s = tEstado.getValue();
			int maxRows = NumberUtils.toInt(getContext().getRequestParameter("maxRows"), 20);
			
			Estado estado = null;
			if(StringUtils.isNotEmpty(s)) {
				estado = Estado.from(s);
			}

			list = new ArrayList<CidadeVO>();

			List<Cidade> cidades = cidadeService.findByNome(tCidade.getValue(), estado, page, maxRows);
			for (Cidade c : cidades) {
				CidadeVO vo  = new CidadeVO();
				vo.setCidade(c);
				list.add(vo);
			}

			return list;
		}

		return new MensagemResult("NOK","Erro ao listar mensagens.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("cidade", CidadeVO.class);
		x.alias("msg", MensagemVO.class);
		super.xstream(x);
	}
}
