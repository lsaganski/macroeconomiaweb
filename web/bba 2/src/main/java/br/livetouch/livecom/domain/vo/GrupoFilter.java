package br.livetouch.livecom.domain.vo;

import java.util.List;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.spring.SearchInfo;


public class GrupoFilter extends SearchInfo {
	public Long id;
	public Usuario usuario;
	public Empresa empresa;
	public String nome;
	public String tipo;
	public String nomeAdmin;
	public List<Long> notIds;
	public List<String> cods;
	/**
	 * Somente grupos que tenho acesso, e que o flag postar=1 
	 */
	public boolean podePostar;
	/**
	 * Somente grupos que criei
	 */
	public boolean gruposQueCriei;
	/**
	 * Somente grupos que participo
	 */
	public boolean gruposQueParticipo;
	/**
	 * Se for admin, retorna todos os grupos
	 */
	public boolean adminAll;
	
	public boolean tipoOn;

	public boolean fisicos = true;

	public boolean padrao = false;

	@Override
	public String toString() {
		return "GrupoFilter [nome=" + nome + ", notIds=" + notIds + ", podePostar=" + podePostar + ", gruposQueCriei=" + gruposQueCriei + ", tipoOn=" + tipoOn + ", virtuais=" + fisicos + ", cods=" + cods + "]";
	}
	
	
}