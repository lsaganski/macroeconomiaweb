package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Feriado;
import net.livetouch.tiger.ddd.DomainException;

public interface FeriadoService extends Service<Feriado> {

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Feriado f, Empresa empresa) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Feriado f) throws DomainException;

	List<Feriado> findAll(Empresa empresa);

	String csvFeriado(Empresa empresa);

}
