package br.livetouch.livecom.web.pages.admin;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import au.com.flyingkite.mobiledetect.UAgentInfo;
import br.infra.util.Log;
import br.infra.util.LongOperation;
import br.infra.util.LongOperation.LongOperationRunner;
import br.infra.util.ServletUtil;
import br.infra.util.ServletUtils;
import br.livetouch.livecom.chatAkka.LivecomChatInterface;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.RequestMap;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.exception.SenhaExpiradaException;
import br.livetouch.livecom.domain.service.AgendaService;
import br.livetouch.livecom.domain.service.ApplicationService;
import br.livetouch.livecom.domain.service.AreaService;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.CampoService;
import br.livetouch.livecom.domain.service.CanalService;
import br.livetouch.livecom.domain.service.CategoriaPostService;
import br.livetouch.livecom.domain.service.ChapeuService;
import br.livetouch.livecom.domain.service.CidadeService;
import br.livetouch.livecom.domain.service.ComentarioService;
import br.livetouch.livecom.domain.service.ComplementoService;
import br.livetouch.livecom.domain.service.ConversaNotificationService;
import br.livetouch.livecom.domain.service.DiretoriaService;
import br.livetouch.livecom.domain.service.DomainService;
import br.livetouch.livecom.domain.service.EmailContatoService;
import br.livetouch.livecom.domain.service.EmailReportService;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.EventoService;
import br.livetouch.livecom.domain.service.FaqService;
import br.livetouch.livecom.domain.service.FavoritoService;
import br.livetouch.livecom.domain.service.FeriadoService;
import br.livetouch.livecom.domain.service.FormacaoService;
import br.livetouch.livecom.domain.service.FuncaoService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.HistoricoService;
import br.livetouch.livecom.domain.service.IdiomaService;
import br.livetouch.livecom.domain.service.LikeService;
import br.livetouch.livecom.domain.service.LinhaImportacaoService;
import br.livetouch.livecom.domain.service.LogAuditoriaService;
import br.livetouch.livecom.domain.service.LogImportacaoService;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.domain.service.LoginReportService;
import br.livetouch.livecom.domain.service.MensagemService;
import br.livetouch.livecom.domain.service.MenuService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.OperacoesCieloService;
import br.livetouch.livecom.domain.service.PalavraService;
import br.livetouch.livecom.domain.service.PalestranteService;
import br.livetouch.livecom.domain.service.ParametroService;
import br.livetouch.livecom.domain.service.PerfilService;
import br.livetouch.livecom.domain.service.PermissaoService;
import br.livetouch.livecom.domain.service.PlaylistService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.PostViewService;
import br.livetouch.livecom.domain.service.PushReportService;
import br.livetouch.livecom.domain.service.RateService;
import br.livetouch.livecom.domain.service.ReportService;
import br.livetouch.livecom.domain.service.StatusChatService;
import br.livetouch.livecom.domain.service.StatusMensagemUsuarioService;
import br.livetouch.livecom.domain.service.StatusPostService;
import br.livetouch.livecom.domain.service.TagService;
import br.livetouch.livecom.domain.service.TemplateEmailService;
import br.livetouch.livecom.domain.service.TokenService;
import br.livetouch.livecom.domain.service.UploadService;
import br.livetouch.livecom.domain.service.UsuarioMarcadoService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.CamposMap;
import br.livetouch.livecom.domain.vo.MenuVO;
import br.livetouch.livecom.domain.vo.NotificationBadge;
import br.livetouch.livecom.domain.vo.UserInfoSession;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.jobs.JobUtil;
import br.livetouch.livecom.push.PushLivecomService;
import br.livetouch.livecom.security.LivecomSecurity;
import br.livetouch.livecom.security.UserAgentUtil;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.livecom.utils.HostUtil;
import br.livetouch.livecom.web.pages.download.IndexPage;
import br.livetouch.livecom.web.pages.pages.AlterarSenhaExpiradaPage;
import br.livetouch.livecom.web.pages.pages.WaitPage;
import br.livetouch.livecom.web.pages.ws.MensagemResult;
import br.livetouch.livecom.web.pages.ws.Response;
import br.livetouch.livecom.wordpress.WordPress;
import net.livetouch.click.page.BorderPage;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Field;
import net.sf.click.control.FieldSet;
import net.sf.click.control.Form;
import net.sf.click.extras.control.Menu;

/**
 * Page mae para usar o Autowired do Spring
 * 
 * @author ricardo
 * 
 */
@Controller
@Scope("prototype")
public class LivecomPage extends BorderPage {
	public String web_layout_sideBox = "0";
	protected static final String COOKIE_LOGIN = "livecom.login3";
	
	private static final Logger logger = Log.getLogger(LivecomPage.class);
	private static final Logger loggerTimer = Log.getLogger("timer");
	
	protected static final String SIZE_300_NOME = "300px;";
	protected static final String SIZE_150_COMBOS = "150px;";

	public Menu rootMenu = Menu.getRootMenu();

	public RequestMap requestMap = RequestMap.getInstance();

	/**
	 * Indica que o admin da empresa esta acessando as paginas de root
	 */
	public boolean showMenuRootEmpresa = false;
	
	public boolean menu = true;
	/**
	 * indica para zerar a session
	 */
	public boolean clear;

	public int currentYear;
	
	public String pt;
	public String es;
	
	@Autowired
	protected ApplicationContext applicationContext;

	@Autowired
	protected LogService logService;

	@Autowired
	protected UsuarioService usuarioService;

	@Autowired
	protected StatusPostService statusPostService;
	
	@Autowired
	protected GrupoService grupoService;

	@Autowired
	protected TokenService tokenService;
	
	@Autowired
	protected PerfilService perfilService;

	@Autowired
	protected CategoriaPostService categoriaPostService;
	@Autowired
	protected PostService postService;

	@Autowired
	protected FavoritoService favoritoService;
	@Autowired
	protected LikeService likeService;

	@Autowired
	protected ComentarioService comentarioService;

	@Autowired
	protected TagService tagService;

	@Autowired
	protected DomainService domainService;

	@Autowired
	protected IdiomaService idiomaService;

	@Autowired
	protected FormacaoService formacaoService;

	@Autowired
	protected MensagemService mensagemService;

	@Autowired
	protected ArquivoService arquivoService;

	@Autowired
	protected UploadService uploadService;
	
	@Autowired
	protected ParametroService parametroService;

	@Autowired
	protected CampoService campoService;

	@Autowired
	protected EmpresaService empresaService;

	@Autowired
	protected ComplementoService complementoService;

	@Autowired
	protected CanalService canalService;

	@Autowired
	protected FuncaoService funcaoService;

	@Autowired
	protected CidadeService cidadeService;

	@Autowired
	protected HistoricoService historicoService;

	@Autowired
	protected RateService rateService;

	@Autowired
	protected PlaylistService playlistService;

	@Autowired
	protected FaqService faqService;

	@Autowired
	protected EmailContatoService emailContatoService;

	@Autowired
	protected NotificationService notificationService;
	
	@Autowired
	protected StatusMensagemUsuarioService statusMensagemUsuarioService;
	
	@Autowired
	protected ChapeuService chapeuService;

	@Autowired
	protected EventoService eventoService;

	@Autowired
	protected PalestranteService palestranteService;

	@Autowired
	protected AgendaService agendaService;
	
	@Autowired
	protected PalavraService palavraService;
	
	@Autowired
	protected LogImportacaoService logImportacaoService;
	
	@Autowired
	protected LinhaImportacaoService linhaImportacaoService;

	@Autowired
	protected StatusChatService statusChatService;

	@Autowired
	protected ApplicationService applicationService;
	
	@Autowired
	protected WordPress wordpress;

	@Autowired
	protected ReportService reportService;
	
	@Autowired
	protected AreaService areaService;
	
	@Autowired
	protected DiretoriaService diretoriaService;
	
	@Autowired
	protected LoginReportService loginReportService;
	
	@Autowired
	protected UsuarioMarcadoService usuarioMarcadoService;
	
	@Autowired
	protected PushLivecomService pushLivecomService;
	
	@Autowired
	protected LivecomChatInterface livecomChatInterface;
	
	@Autowired
	protected PushReportService pushReportService;
	
	@Autowired
	protected FeriadoService feriadoService;
	
	@Autowired
	protected OperacoesCieloService transacaoService;

	@Autowired
	protected TemplateEmailService templateEmailService;
	
	@Autowired
	protected LogAuditoriaService logAuditoriaService;

	@Autowired
	protected EmailReportService emailReportService;

	@Autowired
	protected MenuService menuService;

	@Autowired
	protected PostViewService postViewService;

	@Autowired
	protected PermissaoService permissaoService;
	
	@Autowired
	protected ConversaNotificationService conversaNotificationService;
	
	public int autoRefresh = -1;
	public String msg;
	public String msgTitle;
	public String msgError;

	public int page;
	
	public boolean escondeChat = false;
	public boolean bootstrap_on = false;

	/**
	 * @see br.livetouch.livecom.web.pages.admin.LivecomAdminPage#onSecurityCheck()
	 */
	@Override
	public boolean onSecurityCheck() {
		
		if(isMobile()) {
			setRedirect(IndexPage.class);
			return false;
		}
		
		boolean b = validateSecurity();
		return b;
	}

	protected boolean onSecurityCheckNotOk() {
		return false;
	}

	protected boolean validateSecurity() {
		
		if(StringUtils.containsIgnoreCase(getContext().getResourcePath(),"/download")) {
			ParametrosMap map = ParametrosMap.getInstance(getEmpresa());
			if(map.getBoolean(Params.THIRD_PARTY_VERSION_CONTROL_ON, false) && StringUtils.isNotEmpty(map.get(Params.THIRD_PARTY_VERSION_CONTROL_URL, ""))) {
				setRedirect(map.get(Params.THIRD_PARTY_VERSION_CONTROL_URL));
			}
			return true;
		}
		
		if(validateRedirectDownload()) {
			return true;
		}
		
		boolean b = LivecomSecurity.validate(getContext(), getEmpresa());
		if(!b) {
			boolean get = getContext().isGet();
			if(get) {
				notFound();
			} else {
				forbidden();
			}
		}
		
		return b;
	}
	
	protected boolean validateRedirectDownload() {
		
		if(LivecomSecurity.isShareLink(getContext())) {
			redirectDownload();
			return true;
		}
		
		boolean mobile = UserAgentUtil.isMobile(getContext());
		if(mobile) {
			redirectDownload();
			return true;
		}
		
		return false;
		
	}

	private void redirectDownload() {
		getContext().getResponse().setStatus(HttpServletResponse.SC_OK);
		setRedirect("/download");
	}
	
	protected void notFound() {
		getContext().getResponse().setStatus(HttpServletResponse.SC_NOT_FOUND);
		// Por segurança não retorna nenhuma página
		// Deixa o browser mostrar erro padrão
		setPath(null);
//		setPath("/error404.htm");
	}

	protected void forbidden() {
		getContext().getResponse().setStatus(HttpServletResponse.SC_FORBIDDEN);
		setPath(null);
		//setPath("/error403.htm");		
	}

	protected Object unauthorized(int wsVersion) {
		if(wsVersion > 2) {
			Response r = Response.error("Ocorreu um erro na transação, por favor tente novamente mais tarde.");
			return r;
		}
		return new MensagemResult("ERROR", "Ocorreu um erro na transação, por favor tente novamente mais tarde.");
	}

	@Override
	public String getTemplate() {
//		HttpServletRequest req = getContext().getRequest();
//		StringBuffer url = req.getRequestURL();
//		String uri = req.getRequestURI();
//		String ctx = req.getContextPath();
//		String requestUri = url.substring(0, url.length() - uri.length() + ctx.length());
//		log("Renderizando a page " + getPath() + " no caminho " + requestUri + getPath());
		
		if("/error403.htm".equals(getPath())) {
			return getPath();
		}
		
		if("/blank.htm".equals(getPath())) {
			return getPath();
		}
		
//		if (isBuildBJJ()) {
//			return TrainingLogadoPage.TEMPLATE;
//		}
		ParametrosMap param = ParametrosMap.getInstance(getEmpresaId());
		String isNovo = param.get("layout.novo", "0");
		if(StringUtils.isNotEmpty(isNovo) && isNovo.equals("1")) {
			return "layout/border.htm";
		}
		return "border.htm";
	}

	@Override
	public void onInit() {
		if(loggerTimer.isTraceEnabled()) {
			loggerTimer.trace(new Date() + ":" + getPath() + ".onInit1()");
		}
		super.onInit();
		pt = es = "";
		if ("pt".equals(getContext().getLocale().getLanguage())){
			pt="ativo";
		}else if("es".equals(getContext().getLocale().getLanguage())){
			es="ativo";
		}
		
		init();
		if(loggerTimer.isTraceEnabled()) {
			loggerTimer.trace(new Date() + ":" + getPath() + ".onInit2()");
		}
		
		if(HostUtil.isLocalhost()) {
			ParametrosMap.getInstance(getEmpresaId()).put(Params.CHAT_SERVICE_LOCATION, "ws://localhost:8080/livecom/chat/");
		}
		
		if(HostUtil.isLocalhost()) {
			ParametrosMap.getInstance(getEmpresaId()).put(Params.CHAT_SERVICE_LOCATION_NOTIFICATION, "ws://localhost:8080/livecom/notification/");
		}
	}

	protected void init() {
		if(campoService == null) {
			throw new IllegalArgumentException("Verifique se a page possui o @Controller @Scope(prototype)");
		}
		
		initWeb();

		setDataUltTransacao();
	}

	private void setDataUltTransacao() {
		UserInfoVO userInfo = getUserInfoVO();
		if (userInfo != null) {

			String so = UserAgentUtil.getUserAgentSO(getContext());

			UserInfoSession session = userInfo.getUserSession(so);

			if(session != null) {
				session.setDataUltTransacao(new Date());
				session.setPathUltimoWebService(getPath());
				session.setPathUltimoWebServiceFull(getPath() + "?" + getRequestParams());
			}
		}
	}

	protected void initWeb() {

		if ("invictus".equalsIgnoreCase(getbuildType())) {
			addModel("buildInvictus", new Boolean(true));
		} else {
			addModel("buildLivecom", new Boolean(true));
		}
		addModel("buildType", getbuildType());
	}

	protected CamposMap getCampos() {
		return CamposMap.getInstance(getUsuario());
		
//		CamposMap campos = new CamposMap(getUserInfo(), isAdmin());
//		campos.setCampos(campoService.findAll());
//		return campos;
	}

	
	protected boolean isMobile() {
		String userAgent = getContext().getRequest().getHeader("user-agent");
		// setup the class with the user agent
		UAgentInfo agentInfo = new UAgentInfo(userAgent, null);
		    
		// check if mobile device
		boolean isMobileDevice = agentInfo.detectMobileQuick();
		    
		//or perhaps check if it is a tablet
		boolean isTabletDevice = agentInfo.detectTierTablet();
		
		
		return isMobileDevice || isTabletDevice;
	}
	
	@Override
	public void onGet() {
		super.onGet();
		if(loggerTimer.isTraceEnabled()) {
			loggerTimer.trace(new Date() + ":" + getPath() + ".onGet()");
		}
	}

	@Override
	public void onPost() {
		super.onPost();
		if(loggerTimer.isTraceEnabled()) {
			loggerTimer.trace(new Date() + ":" + getPath() + ".onPost()");
		}
	}

	protected void setIncludeHeader(boolean include) {
		if (include) {
			// String pathHeader = StringUtils.replace(getPath(), ".htm",
			// "_header.htm");
			// addModel("header", pathHeader);
		}
	}

	protected void hideMenu() {
		menu = false;
	}

	protected boolean isAdmin() {
		UserInfoVO u = UserInfoVO.getHttpSession(getContext());
		return u != null ? u.isAdmin() : false;
	}
	
	protected boolean isRoot() {
		UserInfoVO u = UserInfoVO.getHttpSession(getContext());
		return u != null ? u.isRoot() : false;
	}

	protected Usuario getUsuarioRequest() {
		String id = getContext().getRequestParameter("user_id");
		if(id == null) {
			id = getContext().getRequestParameter("user");
		}
		
		if(id == null) {
			return null;
		}
		Usuario usuario = null;
		if(StringUtils.isNotEmpty(id)) {
			if(StringUtils.isNumeric(id)) {
				usuario = usuarioService.get(Long.parseLong(id));
			} else {
				usuario = usuarioService.findByLogin(id);
			}
		}
		
		return usuario;
	}
	
	protected Long getLongParam(String key) {
		String s = getContext().getRequestParameter(key);
		if(StringUtils.isEmpty(s)) {
			return null;
		}
		if(NumberUtils.isNumber(s)) {
			return new Long(s);
		}
		return null;
	}

	protected Usuario getUsuario() {
		if(usuarioService == null) {
			logError("Configure o spring @Controller na página "+getClass().getName()+": " + getHost() + ":"+getPath()+">"+ServletUtils.getRequestUrl(getContext().getRequest()));
			return null;
		}
		Usuario u = getUserInfo();
		if(u == null) {
			u = getUsuarioRequest();
		}
		
		return u;
	}
	
	protected Usuario getUserInfo() {
		UserInfoVO userInfo = getUserInfoVO();
		if(userInfo != null && userInfo.getId() != null) {
			Usuario user = usuarioService.get(userInfo.getId());
			return user;
		}
		return null;
	}

	/**
	 * Pega da http session.
	 * Se nao encontra, pega dos params da request
	 * 
	 * @return
	 */
	protected UserInfoVO getUserInfoVO() {
		String id = null;
		
		// Pega da session se logado na web.
		UserInfoVO info = getUserInfoByWsToken();
		if(info != null) {
			id = String.valueOf(info.id);
		}
		
		// Pega da session se logado na web.
		if (info == null) {
			// Long id
			id = getContext().getRequestParameter("user_logado_id");
			
			if(id == null) {
				// tenta autenticar com Token
				info = UserInfoVO.getHttpSession(getContext());
				if(info != null) {
					id = String.valueOf(info.id);
				}
			}
		} else {
			id = String.valueOf(info.id);
		}
		
		if(StringUtils.isNotEmpty(id)) {
			if(StringUtils.isNumeric(id)) {
				UserInfoVO user = Livecom.getInstance().getUserInfo(Long.parseLong(id));
				return user != null ? user : info;
			} else {
				UserInfoVO user = Livecom.getInstance().getUserInfo(id);
				return user;
			}
		}

		return null;
	}

	/**
	 * Retorna o user que enviou o wstoken.
	 * 
	 * @return
	 */
	private UserInfoVO getUserInfoByWsToken() {
		// caso nao tenha user ou user_id na request, procurar se o token
		// passado é válido
		String wstoken = getContext().getRequestParameter("wstoken");

		String so = UserAgentUtil.getUserAgentSO(getContext());

		if (StringUtils.isNotEmpty(wstoken)) {
			List<UserInfoVO> usuarios = Livecom.getInstance().getUsuarios();
			for (UserInfoVO userInfoVO : usuarios) {
				
				UserInfoSession session = userInfoVO.getUserSession(so);
				
				if (session != null && StringUtils.equals(session.getWstoken(), wstoken)) {
					return userInfoVO;
				}
			}
		}
		
		return null;
	}

	@SuppressWarnings("unchecked")
	public void setFormTextWidth(Form form, int width) {
		/**
		 * Seta todos os campos do Form para 400px
		 */
		Iterator<Field> iterator = form.getFieldList().iterator();
		while (iterator.hasNext()) {
			Field f = (Field) iterator.next();
			if (f instanceof FieldSet) {
				FieldSet set2 = (FieldSet) f;
				for (Iterator<Field> iterator2 = set2.getFieldList().iterator(); iterator2.hasNext();) {
					Field f2 = (Field) iterator2.next();
					f2.setWidth(width + "px;");
				}
			} else {
				f.setWidth(width + "px;");
			}
		}
	}

	public void setFormTextWidth(Form form) {
		setFormTextWidth(form, 350);
	}

	@SuppressWarnings("unchecked")
	public void setFormSubmitWidth(Form form) {
		/**
		 * Seta todos os campos do Form para 160px
		 */
		Iterator<Field> iterator = form.getButtonList().iterator();
		while (iterator.hasNext()) {
			Field f = (Field) iterator.next();
			if (f instanceof FieldSet) {
				FieldSet set2 = (FieldSet) f;
				for (Iterator<Field> iterator2 = set2.getFieldList().iterator(); iterator2.hasNext();) {
					Field f2 = (Field) iterator2.next();
					f2.setWidth("");
				}
			} else {
				f.setWidth("");
			}
		}
	}

	public void setMessage(String msg) {
		this.msg = msg;
		addModel("msg", msg);
	}

	public void setMessage(String msg, String title) {
		this.msg = msg;
		this.msgTitle = msg;
		addModel("msg", msg);
		addModel("msgTitle", title);
	}

	public void setMessageSesssion(String msg) {
		setFlashAttribute("msg", msg);
	}

	public void setMessageSesssion(String msg, String title) {
		setFlashAttribute("msg", msg);
		setFlashAttribute("msgTitle", title);
	}

	public void setMessageKey(String key) {
		String s = getMessage(key);
		if (StringUtils.isNotEmpty(s)) {
			this.msg = s;
		}
		this.msg = key;
	}

	public void setMessageError(String msg) {
		this.msgError = msg;
	}

	public void setMessageError(Exception e, String msg) {
		logError(e.getMessage(), e);
		this.msgError = msg;
	}

	public void setMessageError(Exception e) {
		logError(e.getMessage(), e);
		this.msgError = e.getClass() + ": " + e.getMessage();
	}

	public String getRequestParams() {
		HttpServletRequest request = getContext().getRequest();
		return ServletUtil.getRequestParams(request);
	}

	// Paginacao
	public class LivecomPaginator {
		public int pageSize;
		public int pageCount;
		public int pageFirst;
		public int pageLast;
		public long count;
		public List<Integer> pagesArray = new ArrayList<Integer>();
		public int page;

		public int getPageCount() {
			return pageCount;
		}

		public List<Integer> getPagesArray() {
			return pagesArray;
		}

		public int getPageSize() {
			return pageSize;
		}

		public long getCount() {
			return count;
		}

		public int getPageFirst() {
			return pageFirst;
		}

		public int getPageLast() {
			return pageLast;
		}

		@Override
		public String toString() {
			return "LivecomPaginator [pageSize=" + pageSize + ", pageCount=" + pageCount + ", pageFirst=" + pageFirst + ", pageLast=" + pageLast + ", count=" + count + ", pagesArray=" + pagesArray + ", page=" + page + "]";
		}
	}

	public LivecomPaginator paginator = new LivecomPaginator();

	protected void createPaginator(int page, long count, int pageSize) {
		this.paginator.page = page;
		this.paginator.count = count;
		this.paginator.pageSize = 10;

		this.paginator.pageCount = (int) Math.ceil((float) paginator.count / (float) paginator.pageSize);

		this.paginator.pageFirst = page == 0 ? 1 : page * paginator.pageSize + 1;
		this.paginator.pageLast = page == 0 ? paginator.pageSize : page * paginator.pageSize + paginator.pageSize;
		if (this.paginator.pageLast > paginator.count) {
			this.paginator.pageLast = (int) paginator.count;
		}
		for (int i = 0; i < paginator.pageCount; i++) {
			paginator.pagesArray.add(i);
		}
	}

	protected List<Long> getListIds(String param) {
		Set<Long> ids = new HashSet<Long>();
		String s = getContext().getRequestParameter(param);
		String[] split = StringUtils.split(s, ",");
		if (split != null) {
			for (String id : split) {
				id = StringUtils.trim(id);
				if (NumberUtils.isNumber(id)) {
					ids.add(new Long(id));
				}
			}
		}
		return new ArrayList<Long>(ids);
	}

	protected List<String> getListCods(String param) {
		Set<String> cods = new HashSet<String>();
		String s = getContext().getRequestParameter(param);
		String[] split = StringUtils.split(s, ",");
		if (split != null) {
			for (String cod : split) {
				cod = StringUtils.trim(cod);
				cods.add(cod);
			}
		}
		return new ArrayList<String>(cods);
	}

	protected void writeResponseText(String text, String charset) {
		HttpServletResponse response = getContext().getResponse();
		response.setContentType(charset);
		response.setHeader("Pragma", "no-cache");
		PrintWriter writer = null;
		try {
			writer = response.getWriter();

			setPath(null);

			response.setContentType(getContentType());

			writer.print(text);
			writer.flush();
			writer.close();

		} catch (Throwable e) {
			logError(e.getMessage(), e);
		} finally {

		}
	}

	protected void writeResponseBytes(byte bytes[], String charset) {
		HttpServletResponse response = getContext().getResponse();
		response.setContentType(charset);
		response.setHeader("Pragma", "no-cache");
		OutputStream out = null;
		try {
			out = response.getOutputStream();

			setPath(null);

			response.setContentType(getContentType());

			out.write(bytes);
			out.flush();
			out.close();

		} catch (Throwable e) {
			logError(e.getMessage(), e);
		} finally {

		}
	}

	@SuppressWarnings("rawtypes")
	protected String printFormErrors(Form form) {
		StringBuffer sb = new StringBuffer();
		if (form.getError() != null) {
			sb.append("form error: " + form.getError());
		}
		List errorFields = form.getErrorFields();
		for (Object object : errorFields) {
			if (object instanceof Field) {
				Field f = (Field) object;
				sb.append(f.getError() + "</br>");
			}
		}
		return sb.toString();
	}
	
	@SuppressWarnings("rawtypes")
	protected String getFormError(Form form) {
		if (form.getError() != null) {
			return form.getError();
		}
		List errorFields = form.getErrorFields();
		for (Object object : errorFields) {
			if (object instanceof Field) {
				Field f = (Field) object;
				return f.getError();
			}
		}
		return null;
	}

	@Override
	public void onRender() {
		super.onRender();
		if(loggerTimer.isTraceEnabled()) {
			loggerTimer.trace(new Date() + ":" + getPath() + ".onGet()");
		}
		
		addModel("logo_empresa", getLogoEmpresa());
		addModel("favicon", getFaviconEmpresa());
		
		addParams();
		currentYear = DateUtils.getCurrentYear();
	}

	/**
	 * Coloca os parametros do root na web.
	 * 
	 * Ex: push.on fica push_on
	 */
	protected void addParams() {
		List<String> inEmpresa = params();
		
		if(inEmpresa.size() > 0) {
			List<Parametro> paramsDefaults = parametroService.findDefaultByNomeNotIn(inEmpresa);
			if(paramsDefaults != null && paramsDefaults.size() > 0) {
				for (Parametro p : paramsDefaults) {
					String s = org.apache.commons.lang.StringUtils.replace(p.getNome(), ".", "_");
					if(s != null && p != null && p.getValor() != null) {
						addModel(s, p.getValor());
					}
				}
			}
		}
	}


	/**
	 * Parametros da empresa
	 * 
	 * @return
	 */
	private List<String> params() {
		ParametrosMap params = ParametrosMap.getInstance(getEmpresa());
		List<Parametro> parametros = params.getAll();
		addModel("params", parametros);
		List<String> inEmpresa = new ArrayList<>();
		for (Parametro parametro : parametros) {
			inEmpresa.add(parametro.getNome());
			String s = org.apache.commons.lang.StringUtils.replace(parametro.getNome(), ".", "_");
			if(s != null && parametro != null && parametro.getValor() != null) {
				addModel(s, parametro.getValor());
			}
		}
		return inEmpresa;
	}

	private String getLogoEmpresa() {
		Empresa empresa = getEmpresa();
		if(empresa == null) {
			return null;
		}
		if(empresa.getUrlLogo() == null) {
			return null;
		}
		return empresa.getUrlLogo();
	}
	
	private String getFaviconEmpresa() {
		Empresa empresa = getEmpresa();
		if(empresa == null) {
			return null;
		}
		if(empresa.getUrlFav() == null) {
			return null;
		}
		return empresa.getUrlFav();
	}

	protected void executeLongOperation(final String cod, final LongOperationRunner runnable) {
		final Usuario userInfo = getUserInfo();
		LongOperation.start(cod);
		new Thread(){
			public void run() {
				try {
					runnable.run(userInfo);
				} catch(DomainException e) {
					String message = e.getMessage();
					logError(message, e);
					LongOperation.setErro(cod,message);
				} catch(Exception e) {
					String message = "LongOperation ["+cod+"]: " + e.getMessage();
					logError(message, e);
					LongOperation.setErro(cod,message);
				} finally {
					LongOperation.finish(cod);
				}
			};
		}.start();
		
		setRedirect(WaitPage.class,"cod",cod);
	}
	
	protected void executeJob(final Class<? extends Job> cls,String theadName,final Map<String, Object> params) {
		JobUtil.executeJob(applicationContext, cls,theadName, params);
	}
	
	/**
	 * Executa um Job do Quartz
	 * 
	 * @param jobName
	 */
	protected void executeJob(final Class<? extends Job> cls,final Map<String, Object> params) {
		JobUtil.executeJob(applicationContext, cls, params);
	}

	public boolean isCampoReadOnly(CamposMap campos,String campo) {
		Usuario userInfo = getUsuario();
		if (userInfo != null) {
			Perfil permissao = userInfo.getPermissao();
			if (permissao != null && Livecom.getInstance().hasPermissao(ParamsPermissao.CADASTRAR_USUARIOS, userInfo)) {
				return false;
			}
			if (isAdmin()) {
				return false;
			}
			if (campos != null) {
				return campos.isCampoReadOnly(campo);
			}
		}
		return false;
	}

	public boolean isCampoPublic(CamposMap campos,String campo, Long idUserLogado) {
		Usuario userInfo = getUsuario();
		if (userInfo != null) {
			Perfil p = userInfo.getPermissao();
			if (p != null && Livecom.getInstance().hasPermissao(ParamsPermissao.CADASTRAR_USUARIOS, userInfo)) {
				return true;
			}
			if (isAdmin()) {
				return true;
			}
			if (campos != null) {
				return campos.isCampoPublic(campo, idUserLogado);
			}
		}
		return false;
	}

	public boolean isCampoObrigatorioCadastroAdmin(CamposMap campos,String campo, boolean defaultValue) {
		if (campos != null) {
			return campos.isCampoObrigatorioCadastroAdmin(campo, defaultValue);
		}
		return false;
	}
	
	public boolean isCampoObrigatorioCadastroAdmin(CamposMap campos,String campo) {
		if (campos != null) {
			return campos.isCampoObrigatorioCadastroAdmin(campo, false);
		}
		return false;
	}
	
	public boolean isCampoVisivel(CamposMap campos,String campo) {
		if (campos != null) {
			return campos.isCampoVisivel(campo);
		}
		return false;
	}

	protected void reportException(Throwable e) {
		e.printStackTrace();
		logError(e.getMessage(), e);
	}

	protected String error(Form form) {
		setFlashAttribute("msgError", form.getError() != null ? form.getError() : form.getErrorFields().toString());
		setFlashAttribute("msg", form.getError() != null ? form.getError() : form.getErrorFields().toString());
		String msgErro = printFormErrors(form);
		logError(msgErro);
		return msgErro;
	}

	protected void logoutUserInfo() {
		UserInfoVO info = UserInfoVO.getHttpSession(getContext());
		Livecom.getInstance().removeUserInfo(info,"web");
		
		UserInfoVO.removeHttpSession(getContext());
		HttpSession session = getContext().getSession();
		session.invalidate();
	}

	protected boolean isBuildBJJ() {
		return "bjj".equals(getbuildType());
	}

	protected String getbuildType() {
		String string = ParametrosMap.getInstance(getEmpresa().getId()).getBuildType();
		if ("bjj".equals(string)) {
			String hostName = "";
			try {
				hostName = Inet4Address.getLocalHost().getHostName();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}

			boolean allowed = br.infra.util.StringUtils.containsAny(hostName, new String[] { "RicardoLecheta" + "", "iMac-de-livetouch-2.local", "Wagner-PC", "ip-172-31-22-123" });

			if (!allowed) {
				throw new RuntimeException("Invalid configuration, host [" + hostName + "]. No web.xml configure o spring.xml.");
			}
		}
		return string;
	}

	protected String getCss() {
		String css = getbuildType();
		return css;
	}

	protected void setError(Form f, String msg) {
		f.setError(msg);
		// this.msg = msg;
		addModel("msgError", msg);
		// setMessage(msg);
	}
	
	protected boolean isPushOn() {
		Long empresaId = getEmpresaId();
		boolean pushOn = ParametrosMap.getInstance(empresaId).isPushOn();
		return pushOn;
	}

	protected Empresa getEmpresa() {
		String domain = getHost();
		Empresa e = empresaService.findDominio(domain);
		
		//Empresa não existe, bloquear request
//		if("livecom.livetouchdev.com.br".contains("domain") && e == null) {
//			e = empresaService.get(1L);
//		}
		
		if((isLocalhost()  || HostUtil.isLocalhost()) && e == null) {
			e = empresaService.get(1L);
		}
		
		if(e == null) {
			Usuario u = getUsuarioRequest();
			if(u != null) {
				e = u.getEmpresa();
			} else {
				//Empresa não existe, bloquear request
				//e = empresaService.get(1L);
			}
		}
		return e;
	}

	protected ParametrosMap getParametrosMap() {
		Long empresaId = getEmpresaId();
		return ParametrosMap.getInstance(empresaId);
	}
	
	protected Long getEmpresaId() {
		Empresa e = getEmpresa();
		return e != null ? e.getId() : null;
	}

	protected boolean isLocalhost() {
		return LivecomSecurity.isLocalhost(getContext());
	}

	protected String getHost() {
		return LivecomSecurity.getHost(getContext());
	}
	
	@SuppressWarnings("unchecked")
	public void setAutoCompleteOff(Form form) {
		List<Field> fields = form.getFieldList();

		for (Field field : fields) {
			field.setAttribute("autocomplete", "off");
		}
	}

	protected boolean saveUserSessionAfterLoginAndRedirect(String login, Usuario u)
	{
		// User Info
		UserInfoVO userInfo = UserInfoVO.create(u);
		userInfo.addSession("web");
		Boolean senhaExpirada = isExpiredPassword(u);
		userInfo.setSenhaExpirada(senhaExpirada);
		UserInfoVO.setHttpSession(getContext(),userInfo);
		
		if(getParametrosMap().getBoolean(Params.MENU_DINAMICO_ON, false)) {
			List<MenuVO> menus = generateMenu(u);
			userInfo.setMenus(menus);
		}

		log("Login: " + login);
		
		boolean redirect = redirectAfterLogin(); 
		if(senhaExpirada){
			setRedirect(AlterarSenhaExpiradaPage.class);
			return false;
		}
		if(u.isStatusNovo() || u.isStatusEnviadoConvite() || u.isPreCadastro()) {
			redirect = loginOk(u);
			setRedirect(br.livetouch.livecom.web.pages.cadastro.UsuarioPage.class, "id", u.getId().toString());
			return redirect;
		} else {
			if(!redirect) {
				redirect = loginOk(u);
			}
		}

		return redirect;
	}
	
	private Boolean isExpiredPassword(Usuario u) {
		try{
			Long empresaId = u.getEmpresa().getId();
			LivecomSecurity.checkExpiredPasswordByInactivity(u, empresaId);
			LivecomSecurity.checkExpiredPasswordByPeriodicity(u, empresaId, usuarioService);
			return false;
		}catch(SenhaExpiradaException e){
			return true;
		}
	}

	private List<MenuVO> generateMenu(Usuario u) {
		Perfil perfil = u.getPermissao();
		
		List<MenuVO> menusVO = new ArrayList<MenuVO>();
		if(perfil != null) {
			List<br.livetouch.livecom.domain.Menu> menus = menuService.paisByPerfil(perfil, getEmpresa());
			List<br.livetouch.livecom.domain.Menu> menusFilho = menuService.filhosByPerfil(perfil, getEmpresa());
			
			for (br.livetouch.livecom.domain.Menu menu : menus) {
				MenuVO menuVO = new MenuVO(menu);
				for (br.livetouch.livecom.domain.Menu filho : menusFilho) {
					if(filho.getParent().getId().equals(menu.getId())) {
						MenuVO filhoVO = new MenuVO(filho);
						menuVO.addFilho(filhoVO);
					}
				}
				menusVO.add(menuVO);
			}
			
		}
		
		return menusVO;
		
	}


	protected boolean loginOk(Usuario u) {
		return false;
	}
	
	protected boolean redirectAfterLogin() {
		return false;
	}

	public Map<String, Cookie> getCookies() {
		Map<String, Cookie> map = new HashMap<String,Cookie>();
		// http://tutorials.jenkov.com/java-servlets/cookies.html
		Cookie[] cookies = getContext().getRequest().getCookies();
		if(cookies != null) {
			for(Cookie c : cookies) {
			    log("Cookie: " + c.getName() + "("+c.getMaxAge()+")"  +": " + c.getValue());
			    map.put(c.getName() , c);
			}
		}
		return map;
	}
	
	public Cookie getCookie(String key) {
		return getCookies().get(key);
	}
	
	public String getCookieValue(String key) {
		Cookie c = getCookies().get(key);
		if(c == null || c.getMaxAge() == 0) {
	    	return null;
	    }
		return c != null ? c.getValue() : null;
	}

	protected void addCookie(String key, String value) {
		
		Cookie c = getCookie(key);
		int maxAge = ParametrosMap.getInstance(getEmpresaId()).getInt(Params.WEB_COOKIE_MAXAGE,24 * 60 * 60);
		if(c != null) {
			c.setMaxAge(maxAge);
			getContext().getResponse().addCookie(c);
			getCookies();
			return;
		}
		
		c = new Cookie(key, value);
		// 1 dia: 60 sec x 60 min x 24 hours (24 x 60 x 60)
		c.setMaxAge(maxAge);
		getContext().getResponse().addCookie(c);
	}
	
	/**
	 * If you set the expiration time to 0 the cookie will be removed immediately 
	 * from the browser. 
	 * If you set the expiration time to -1 the cookie will be deleted 
	 * when the browser shuts down.
	 * 
	 * @param key
	 */
	protected void removeCookie(String key) {
		Cookie c = getCookies().get(key);
		if(c != null) {
			c.setMaxAge(0);
			getContext().getResponse().addCookie(c);
		}
	}
	
	
	
	public String[] getParameterValues(String param) {
		return getContext().getRequest().getParameterValues(param);
	}
	
	protected void log(String string) {
		String host = getHost();
		logger.debug(host + ": " + string);
	}
	
	protected void logError(Object message, Throwable t) {
		String host = getHost();
		logger.error(host + ": " + message, t);
	}
	
	protected void logError(Object message) {
		String host = getHost();
		logger.error(host + ": " + message);
	}
	
	protected ParametrosMap getParams() {
		return ParametrosMap.getInstance(getEmpresa());
	}
	
	protected boolean hasPermissao(String permissao) {
		UserInfoVO user = getUserInfoVO();
		if(user != null) {
			return user.isPermissao(permissao);
		}
		
		return false;
	}

	protected boolean hasPermissoes(boolean checkAll, String... permissoes) {
		UserInfoVO user = getUserInfoVO();
		if(user != null) {
			if(checkAll) {
				for (String permissao : permissoes) {
					if(!user.isPermissao(permissao)) {
						return false;
					}
				}
				return true;
			} else {
				for (String permissao : permissoes) {
					if(user.isPermissao(permissao)) {
						return true;
					}
				}
				return false;
			}
		}
		
		return false;
	}

	protected NotificationBadge getBadges(Usuario user) {
		NotificationBadge badges = notificationService.findBadges(user);
		// Chat
		if(badges != null) {
			Long count = mensagemService.countMensagensNaoLidas(user);
			badges.mensagens = count;
		}
		return badges;
	}
	
	protected DataSource getDataSource() {
		Object o = (Object) applicationContext.getBean("dataSource");
		if (o != null && o instanceof DataSource) {
			DataSource ds = (DataSource) o;
			return ds;
		}
		return null;
	}

	protected Idioma getIdioma() {
		Usuario usuario = getUsuario();
		if(usuario == null) {
			return null;
		}
		
		return usuario.getIdioma();
		
	}
}
