package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Palavra;
import br.livetouch.livecom.domain.Post;
import net.livetouch.tiger.ddd.DomainException;

public interface PalavraService extends Service<Palavra> {

	List<String> censurar(Post p);

	boolean isCensurado(String msg);
	
	List<Palavra> findAll();
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Palavra f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Palavra f) throws DomainException;

}
