package br.livetouch.livecom.web.pages.cielo;


import java.util.Date;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.infra.web.click.DateField;
import br.livetouch.livecom.domain.OperacoesCielo;
import br.livetouch.livecom.web.pages.admin.LivecomAdminPage;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.Context;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Decorator;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.extras.control.DoubleField;
import net.sf.click.extras.control.LinkDecorator;

/**
 * 
 */
@Controller
@Scope("prototype")
public class OperacoesCieloPage extends LivecomAdminPage {

	public PaginacaoTable table = new PaginacaoTable();
	public Form form = new Form();
	public DateField tData = new DateField("data", getMessage("data.label"));
	public ActionLink editLink = new ActionLink("editar", this,"editar");
	public Link deleteLink = new Link("deletar", this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public OperacoesCielo transacao;

	public List<OperacoesCielo> transacoes;
	
	@Override
	public void onInit() {
		super.onInit();

		form();
		
		table();

		if (id != null) {
			transacao = transacaoService.get(id);
		} else {
			transacao = new OperacoesCielo();
		}
		form.copyFrom(transacao);
	}
	
	public void form(){
		form.add(new IdField());

		tData.setAttribute("class", "input data datepicker");
		tData.setFormatPattern("dd/MM/yyyy");
		tData.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		tData.setMaxLength(10);
		form.add(tData);
		
		DoubleField tVolumetria = new DoubleField("volumetria", true);
		tVolumetria.setAttribute("class", "input");
		form.add(tVolumetria);
		
		DoubleField tPicoTps = new DoubleField("picoTps", true);
		tPicoTps.setAttribute("class", "input");
		form.add(tPicoTps);
		
		DoubleField tMedia = new DoubleField("media");
		tMedia.setAttribute("class", "input");
		form.add(tMedia);
		
		DoubleField tMomento = new DoubleField("momento");
		tMomento.setAttribute("class", "input");
		form.add(tMomento);
		
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip botao btIcone marginFloat");
		form.add(tExportar);
		
		Submit tsalvar = new Submit("salvar", this, "salvar");
		tsalvar.setAttribute("class", "botao");
		form.add(tsalvar);
		
		Submit cancelar = new Submit("novo",this,"novo");
		cancelar.setAttribute("class", "botao");
		form.add(cancelar);

	}
	
	private void table() {
		
		Column c = new Column("data");
		c.setTextAlign("center");
		c.setDecorator(new Decorator() {
			@Override
			public String render(Object object, Context context) {
				OperacoesCielo transacao = (OperacoesCielo) object;
				if (transacao != null && transacao.getData() != null) {

					String date = transacao.getDataString();
					return date;
				}
				return "-";
			}

		});
		table.addColumn(c);

		c = new Column("volumetria");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		c = new Column("picoTps");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		c = new Column("media");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		c = new Column("momento");
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes");
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				transacao = id != null ? transacaoService.get(id) : new OperacoesCielo();
				form.copyTo(transacao);

				transacaoService.saveOrUpdate(transacao, getEmpresa());

				setFlashAttribute("msg",  "Transacao ["+transacao.getDataString()+"] salvo com sucesso.");
				setRedirect(getClass());

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(getClass());
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		transacao = transacaoService.get(id);
		form.copyFrom(transacao);
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			OperacoesCielo e = transacaoService.get(id);
			transacaoService.delete(e);
			setRedirect(getClass());
			setFlashAttribute("msg", "Transacao deletado com sucesso.");
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = "Transacao possui relacionamentos. Não foi possível excluir.";
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		Integer max = 50;
		transacoes = transacaoService.transacoesByEmpresa(getEmpresa(), page, max);
		table.setPageSize(max);
		long count = transacaoService.getCountTransacoesByEmpresa(getEmpresa(), page, max);
		table.setCount(count);

		table.setRowList(transacoes);
	}
	
	public boolean exportar(){
		String csv = transacaoService.csvTransacao(getEmpresa());
		download(csv, "transacoes.csv");
		return true;
	}
	
}
