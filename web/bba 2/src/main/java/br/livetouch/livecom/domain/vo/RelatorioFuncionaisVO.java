package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.Usuario;

public class RelatorioFuncionaisVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6564680148644503377L;
	
	private String login;
	private Long count;
	private Long userId;
	
	public RelatorioFuncionaisVO(String login) {
		this.login = login;
	}
	
	public RelatorioFuncionaisVO(Usuario u) {
		this.login = u.getLogin();
		this.userId = u.getId();
	}
	
	public RelatorioFuncionaisVO() {
		
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
