package br.livetouch.livecom.jobs;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import akka.actor.ActorSelection;
import br.infra.util.Log;
import br.livetouch.livecom.chatAkka.actor.AkkaFactory;
import br.livetouch.livecom.chatAkka.protocol.ChatUtils;
import br.livetouch.livecom.chatAkka.protocol.JsonRawMessage;
import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.vo.UserInfoSession;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.utils.DateUtils;

/**
 * Fica enviando keep alive para manter o socket do Android chat aberto para users conectados.
 * 
 * {"code":"keepAlive"}
 * 
 * @author rlech
 *
 */
@Service
public class KeepAliveChatJob extends SpringJob {

	protected static final Logger log = Log.getLogger(KeepAliveChatJob.class);
	
	@Override
	protected void execute(Map<String, Object> params) throws Exception {
		if(Livecom.getInstance().isChatOn()) {
			keepAliveAndroidChat();
		}
	}

	private void keepAliveAndroidChat() {
		try {
			
			boolean keepAliveOn = ParametrosMap.getInstance().getBoolean("chat.sendKeepAlive.on", true);
			if(!keepAliveOn) {
				return;
			}

			List<UserInfoVO> usuariosLogados = Livecom.getInstance().getUsuarios();
			if(usuariosLogados != null && usuariosLogados.size() > 0) {

				for (UserInfoVO userInfo : usuariosLogados) {

					boolean isLogado = Livecom.getInstance().isUsuarioLogadoChat(userInfo.getId());
					if(isLogado) {

						/**
						 * Envia keep alive para o user info
						 */
						sendKeepAlive(userInfo);
						
						/**
						 * Valida se cada session do userInfo respondeu ao keepAlive
						 * Se não respondeu, remove a session (web, ios, android).
						 */
						boolean checkKeepAliveOn = ParametrosMap.getInstance().getBoolean("chat.checkKeepAlive.on", true);
						if(!checkKeepAliveOn) {
							return;
						}
						checkKeepAlive(userInfo);						
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public void checkKeepAlive(UserInfoVO userInfo) {
		long now = System.currentTimeMillis();
		
		/**
		 * Percorre as sessions para ver quem respondeu.
		 */
		List<UserInfoSession> sessions = userInfo.getListSession();
		for (UserInfoSession session : sessions) {
			
			if("web".equals(session.getTipo())) {
				continue;
			}

			/**
			 * Se ainda não respondeu o keepAlive...
			 */
			boolean keepAliveOk = session.isKeepAliveOk();
			if(! keepAliveOk) {
				
				String sessionKey = userInfo.getLogin()+"/"+session.getTipo();
				
				// Resposta KeepAlive
				long lastKeepAliveOk = session.getTimestampUltChat();
				long diffKeepAliveOk = now - lastKeepAliveOk;

				log("Check Session ["+sessionKey+"] diff: " + (diffKeepAliveOk/1000) + " seg");
				
				/**
				 * chat.checkKeepAlive.timeSeconds = 30
				 * 
				 * Tempo resposta after last keep alive, 30 segundos
				 */
				long TIME_RESPONSE = ParametrosMap.getInstance().getInt("chat.checkKeepAlive.timeSeconds", 30 * 1000);
				
				/**
				 * Se ainda não respondeu verifica se deu timeout.
				 */
				if(diffKeepAliveOk > TIME_RESPONSE) {

					log(">> *** User ("+sessionKey+") NAO RESPONDEU AO KEEP ALIVE ***");

					String so = session.getTipo();
					Livecom.getInstance().removeUserInfo(userInfo, so);
				}
			} else {
				String sessionKey = userInfo.getLogin()+"/"+session.getTipo();
				log("Session ["+sessionKey+"] keepAlive OK.");
			}
		}
	}

	public void sendKeepAlive(UserInfoVO userInfo) {
		long now = System.currentTimeMillis();
		
		final Long userId = userInfo.getId();

		// Envia KeepAlive
		Date lastKeepAliveChat = userInfo.getLastKeepAliveChat();
		long lastKeepAlive = lastKeepAliveChat != null ? lastKeepAliveChat.getTime() : 0;
		long diffKeepAlive = now - lastKeepAlive;

		/**
		 * Tempo para enviar keep alive (30 seg default).
		 */
		long TIME = ParametrosMap.getInstance().getInt("chat.sendKeepAlive.timeSeconds", 30 * 1000);
		
		if(diffKeepAlive > TIME) {

			log(">> send keepAlive ("+userInfo.getLogin()+") " + new Date());
			ActorSelection userActor = AkkaFactory.getUserActor(userId);
			// manda msg pro ator (se online)
			JsonRawMessage jsonKeepAlive = ChatUtils.getJsonKeepAlive(userId);
			AkkaHelper.tellWithFuture(userActor, jsonKeepAlive, null);
			userInfo.setLastKeepAliveChat(DateUtils.toDate(now));
			
			List<UserInfoSession> sessions = userInfo.getListSession();
			for (UserInfoSession session : sessions) {
				// Zera status do keep alive
				// Cada session deve responder, caso contrário será eliminada.
				session.setKeepAliveOk(false);
			}
			return;
		}
	}

	private void log(String string) {
		log.debug(string);
		System.out.println(string);
	}
}
