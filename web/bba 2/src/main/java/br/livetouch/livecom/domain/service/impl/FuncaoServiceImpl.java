package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.repository.FuncaoRepository;
import br.livetouch.livecom.domain.service.FuncaoService;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class FuncaoServiceImpl extends LivecomService<Funcao> implements FuncaoService {
	@Autowired
	private FuncaoRepository rep;
	
	@Override
	public Funcao get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Funcao c,Usuario userInfo) throws DomainException {
		if(c.getEmpresa() == null) {
			c.setEmpresa(userInfo.getEmpresa()); 
		}
		rep.saveOrUpdate(c);
		
		auditSave(c, userInfo);
		
	}

	protected void auditSave(Funcao c, Usuario userInfo) {
		boolean insert = c.getId() == null;
		AuditoriaAcao acao = insert ? AuditoriaAcao.INSERIR : AuditoriaAcao.EDITAR;
		
		String msg = null;
		if(userInfo != null) {
			msg = "Cargo " + c.getNome() + " cadastrado";
			if(!insert) {
				msg = "Cargo " + c.getNome() + " editado";
			}
			
		}
		LogAuditoria.log(userInfo, c, AuditoriaEntidade.CARGO, acao, msg);
	}

	@Override
	public List<Funcao> findAll(Usuario userInfo) {
		return rep.findAll(userInfo);
	}

	@Override
	public void delete(Usuario userInfo, Funcao c) throws DomainException {
		AuditoriaAcao acao = AuditoriaAcao.DELETAR;
		
		LogAuditoria auditoria = null;
		try {
			String msg = null;
			if(userInfo != null) {
				msg = "Cargo "+ c.getNome() +" deletado";
				auditoria = LogAuditoria.log(userInfo, c, AuditoriaEntidade.CARGO, acao, msg, false);
			}
			rep.delete(c);
			
			// OK
			if(auditoria != null) {
				auditoria.finish();
			}
		} catch (DataIntegrityViolationException e) {
			LogAuditoria.log(userInfo, c, AuditoriaEntidade.CARGO, acao, e.getMessage(), e);
			throw new DomainMessageException("validate.cargo.delete.possui.relacionamentos");
		}
	}

	@Override
	public List<Object[]> findIdNomeFuncao(Long idEmpresa) {
		return rep.findIdNomeFuncao(idEmpresa);
	}

	@Override
	public Funcao findByNome(String nome, Long idEmpresa) {
		return rep.findByNome(nome, idEmpresa);
	}

	@Override
	public Funcao findDefaultByEmpresa(Long empresaId) {
		return rep.findDefaultByEmpresa(empresaId);
	}

	@Override
	public String csvCargo(Empresa empresa) {
		List<Funcao> findAll = findAll(empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("CODIGO").add("NOME");
		for (Funcao f : findAll) {
			body.add(f.getCodigoDesc()).add(f.getNome());
			body.end();
		}
		return generator.toString();
	}
	@Override
	public List<Funcao> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	@Override
	public List<Funcao> findByNome(String nome, int page, int maxRows, Empresa empresa) {
		return rep.findByNome(nome, page, maxRows, empresa);
	}
	
	@Override
	public Long countByNome(String nome, int page, int maxRows, Empresa empresa) {
		return rep.countByNome(nome, page, maxRows, empresa);
	}

	@Override
	public Funcao findByName(Funcao funcao, Empresa empresa) {
		return rep.findByName(funcao, empresa);
	}

	@Override
	public List<Object[]> findIdCodFuncao(Long idEmpresa) {
		return rep.findIdCodFuncao(idEmpresa);
	}

}
