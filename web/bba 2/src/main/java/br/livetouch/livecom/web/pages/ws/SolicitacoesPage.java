package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.BadgeChatVO;
import br.livetouch.livecom.domain.vo.NotificationBadge;
import br.livetouch.livecom.domain.vo.NotificationResponseVO;
import br.livetouch.livecom.domain.vo.NotificationVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.DateField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class SolicitacoesPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private UsuarioLoginField tUser;
	public int maxRows;
	public int page;
	private LongField tNotificationId;
	private LongField tNovasNotificationsId;
	private DateField tDataInicial;
	private DateField tDataFinal;
	
	public TextField tTextoBusca;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		form.add(tUser = new UsuarioLoginField("user_id","user_id", false,usuarioService, getEmpresa()));
		form.add(tTextoBusca =new TextField("texto","Busca por palavras-chave"));
		form.add(tNotificationId = new LongField("id_notification","id_notification"));

		form.add(tNovasNotificationsId = new LongField("novas_notifications_id","novas_notifications_id"));
		
		tDataInicial = new DateField("dataInicial","dataInicial", false);
		tDataFinal = new DateField("dataFinal","dataFinal", false);
		tDataInicial.setFormatPattern("dd/MM/yyyy");
		tDataFinal.setFormatPattern("dd/MM/yyyy");
		form.add(tDataInicial);
		form.add(tDataFinal);

		tDataInicial.setMaxLength(10);
		tDataFinal.setMaxLength(10);
		
		form.add(new IntegerField("page","page"));
		form.add(new IntegerField("maxRows","maxRows"));
		form.add(new TextField("wsVersion"));
		form.add(tMode = new TextField("mode","mode"));
		
		tMode.setValue("");

		tUser.setFocus(true);

		tMode.setValue("json");

		form.add(new Submit("consultar"));

		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario u = tUser.getEntity();
			if(u == null) {
				return new MensagemResult("ERROR" ,"Usuário inválido.");
			}
			
			NotificationResponseVO notifications = getNotifications(u);
			
			if(isWsVersion3()) {
				Response r = Response.ok("OK");
				r.notifications = notifications;
				return r;
			}
			
			return notifications;
		}
		
		return new MensagemResult("NOK","Erro ao buscar solicitações.");
	}

	private NotificationResponseVO getNotifications(Usuario u) {
		NotificationResponseVO r = new NotificationResponseVO();
		return r;
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("notification", NotificationResponseVO.class);
		x.alias("notification", NotificationVO.class);
		x.alias("badges", NotificationBadge.class);
		x.alias("chatBadges", BadgeChatVO.class);
		super.xstream(x);
	}
	
	@Override
	protected boolean isResponseGSON() {
		return true;
	}
}
