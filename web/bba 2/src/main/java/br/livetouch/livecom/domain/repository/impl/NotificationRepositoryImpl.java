package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusAmizade;
import br.livetouch.livecom.domain.enums.StatusParticipacao;
import br.livetouch.livecom.domain.enums.StatusPublicacao;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.repository.NotificationRepository;
import br.livetouch.livecom.domain.repository.NotificationSearch;
import br.livetouch.livecom.domain.vo.AudienciaVO;
import br.livetouch.livecom.domain.vo.NotificationBadge;
import br.livetouch.livecom.domain.vo.NotificationVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioNotificationsVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoFiltro;
import br.livetouch.livecom.domain.vo.UserToPushVO;
import br.livetouch.livecom.utils.DateUtils;
import net.livetouch.tiger.ddd.DomainException;

@Repository
@SuppressWarnings("unchecked")
public class NotificationRepositoryImpl extends LivecomRepository<Notification> implements NotificationRepository {

	public NotificationRepositoryImpl() {
		super(Notification.class);
	}

	@Override
	public List<NotificationUsuario> findAll(NotificationSearch search){
		StringBuffer sb = new StringBuffer("select distinct notUser from NotificationUsuario notUser inner join notUser.notification n left join n.post p where 1=1 ");

		boolean cache = true;

		if(search.user != null) {
			sb.append(" and notUser.usuario.id = :userId ");
		}
		
		if(StringUtils.isNotEmpty(search.tipo)) {
			cache = false;
			sb.append(" and n.tipo = :tipo");
		} else {
			sb.append(" and n.tipo != :reminder");
		}

		if(search.naoLidas) {
			sb.append(" and notUser.lida=0 ");
		} else if (search.lidas) {
			sb.append(" and notUser.lida=1 ");
		}
		
		if(StringUtils.isNotEmpty(search.texto)) {
			cache = false;
			sb.append(" and n.textoNotificacao like :texto");
		}
		
		if (search.dataInicial != null) {
			cache = false;
			sb.append(" and n.data >= :dataInicial");
		}
		
		if (search.dataFinal != null) {
			cache = false;
			sb.append(" and n.data <= :dataFinal");
		}
		
		if (search.novasNotificacoes != null) {
			cache = false;
			sb.append(" and notUser.id > :novasNotificacoes");
		}

		sb.append(" and (p.statusPublicacao = :publicado or n.publicado=1)");
		
		sb.append(" and notUser.visivel = 1 ");

		sb.append(" order by notUser.id desc");
		
		Query q = createQuery(sb.toString());
		
		if(search.user != null) {
			q.setLong("userId", search.user.getId());
		}
		
		if(StringUtils.isNotEmpty(search.tipo)) {
			q.setString("tipo", search.tipo.toUpperCase());
		}else {
			q.setParameter("reminder", TipoNotificacao.REMINDER);
		}
		
		
		if (search.dataInicial != null) {
			q.setParameter("dataInicial", DateUtils.setTimeInicioDia(search.dataInicial));
		}
		if (search.dataFinal != null) {
			q.setParameter("dataFinal", DateUtils.setTimeFimDia(search.dataFinal));
		}
		
		if (search.novasNotificacoes != null) {
			q.setParameter("novasNotificacoes", search.novasNotificacoes);
		}
		
		if(StringUtils.isNotEmpty(search.texto)) {
			q.setParameter("texto", "%" + search.texto + "%");
		}

		q.setParameter("publicado", StatusPublicacao.PUBLICADO);
		
		if (search.maxRows > 0) {
			int firstResult = search.page * search.maxRows;
			q.setFirstResult(firstResult);
			q.setMaxResults(search.maxRows);
		}
		
		q.setCacheable(cache);
		List<NotificationUsuario> list = q.list();

		return list;
	}

	@Override
	public List<NotificationUsuario> findAllAgrupada(NotificationSearch search){
		StringBuffer sb = new StringBuffer("select max(notUser) from NotificationUsuario notUser inner join notUser.notification n left join n.post p where 1=1 ");

		boolean cache = true;

		if(search.user != null) {
			sb.append(" and notUser.usuario.id = :userId ");
		}

		if(StringUtils.isNotEmpty(search.tipo)) {
			cache = false;
			sb.append(" and n.tipo = :tipo");
		} else {
			sb.append(" and n.tipo != :reminder");
		}

		if(search.naoLidas) {
			sb.append(" and notUser.lida=0 ");
		} else if (search.lidas) {
			sb.append(" and notUser.lida=1 ");
		}

		if(StringUtils.isNotEmpty(search.texto)) {
			cache = false;
			sb.append(" and n.textoNotificacao like :texto");
		}

		if (search.dataInicial != null) {
			cache = false;
			sb.append(" and n.data >= :dataInicial");
		}

		if (search.dataFinal != null) {
			cache = false;
			sb.append(" and n.data <= :dataFinal");
		}

		if (search.novasNotificacoes != null) {
			cache = false;
			sb.append(" and notUser.id > :novasNotificacoes");
		}

		sb.append(" and (p.statusPublicacao = :publicado or n.publicado=1)");
		
		sb.append(" and notUser.visivel = 1 ");
		
//		sb.append(" group by CASE WHEN notUser.lida = 0 THEN 'p, n.tipo' ELSE notUser.id END ");
		sb.append(" group by p, n.tipo,  CASE WHEN notUser.lida = 1 THEN notUser.id ELSE '' END ");
		
		sb.append(" order by notUser.lida, notUser.id desc");
		
		Query q = createQuery(sb.toString());
		
		if(search.user != null) {
			q.setLong("userId", search.user.getId());
		}
		
		if(StringUtils.isNotEmpty(search.tipo)) {
			q.setString("tipo", search.tipo.toUpperCase());
		} else {
			q.setParameter("reminder", TipoNotificacao.REMINDER);
		}
		
		if (search.dataInicial != null) {
			q.setParameter("dataInicial", DateUtils.setTimeInicioDia(search.dataInicial));
		}
		if (search.dataFinal != null) {
			q.setParameter("dataFinal", DateUtils.setTimeFimDia(search.dataFinal));
		}

		if (search.novasNotificacoes != null) {
			q.setParameter("novasNotificacoes", search.novasNotificacoes);
		}

		if(StringUtils.isNotEmpty(search.texto)) {
			q.setParameter("texto", "%" + search.texto + "%");
		}
		
		q.setParameter("publicado", StatusPublicacao.PUBLICADO);
		
		if (search.maxRows > 0) {
			int firstResult = search.page * search.maxRows;
			q.setFirstResult(firstResult);
			q.setMaxResults(search.maxRows);
		}
		
		q.setCacheable(cache);
		List<NotificationUsuario> list = q.list();
		
		return list;
	}
	
	@Override
	public List<Usuario> findNaoLidasByNotification(Notification n, NotificationSearch search){
		
		Post post = n.getPost() != null ? n.getPost() : n.getComentario() != null ? n.getComentario().getPost() : null;
		
		StringBuffer sb = new StringBuffer("select distinct n.usuario from NotificationUsuario notUser inner join notUser.notification n where 1=1 ");

		sb.append(" and notUser.lida=0 ");

		if(search.user != null) {
			sb.append(" and notUser.usuario.id = :userId");
		}
		
		if(post != null) {
			sb.append(" and n.post = :post ");
		}
		
		if(n.getTipo() != null) {
			sb.append(" and n.tipo = :tipo ");
		}
		
		sb.append(" order by notUser.id desc");
		
		Query q = createQuery(sb.toString());
		
		if(n.getTipo() != null) {
			q.setParameter("tipo", n.getTipo());	
		}
		
		if(post != null) {
			q.setParameter("post", post);
		}
		
		if(search.user != null) {
			q.setLong("userId", search.user.getId());
		}
		
		List<Usuario> list = q.list();
		
		return list;
	}

	@Override
	public List<NotificationUsuario> findSolicitacoes(NotificationSearch search){
		StringBuffer sb = new StringBuffer("select max(notUsuario) from NotificationUsuario notUsuario inner join notUsuario.notification n left join n.grupo g where 1=1 ");

		boolean cache = true;
		
		if(search.user != null) {
			sb.append(" and notUsuario.usuario.id = :userId");
		}
		
		if(search.naoLidas) {
			sb.append(" and notUsuario.lida=0 ");
		}
		
		if(StringUtils.isNotEmpty(search.texto)) {
			cache = false;
			sb.append(" and n.textoNotificacao like :texto");
		}
		
		if (search.dataInicial != null) {
			cache = false;
			sb.append(" and n.data >= :dataInicial");
		}
		
		if (search.dataFinal != null) {
			cache = false;
			sb.append(" and n.data <= :dataFinal");
		}
		
		if (search.novasNotificacoes != null) {
			cache = false;
			sb.append(" and notUsuario.id > :novasNotificacoes");
		}

		sb.append(" and n.publicado=1");
		
		sb.append(" and ((n.usuario.id in (select gu.usuario.id from GrupoUsuarios gu where gu.statusParticipacao = :solicitada and gu.grupo = g) and n.tipo = :tipo) ");

		sb.append(" or (n.usuario.id in (select a.usuario.id from Amizade a where a.statusAmizade = :amizadeSolicitada and a.amigo = notUsuario.usuario ) and n.tipo = :tipoAmizade)) ");

		sb.append(" group by notUsuario.usuario, n.usuario, g");
		sb.append(" order by n.tipo, notUsuario.id desc");
		
		Query q = createQuery(sb.toString());
		
		if(search.user != null) {
			q.setLong("userId", search.user.getId());
		}
		
		q.setParameter("tipoAmizade", TipoNotificacao.USUARIO_SOLICITAR_AMIZADE);
		q.setParameter("tipo", TipoNotificacao.GRUPO_SOLICITAR_PARTICIPAR);
		
		if (search.dataInicial != null) {
			q.setParameter("dataInicial", DateUtils.setTimeInicioDia(search.dataInicial));
		}
		if (search.dataFinal != null) {
			q.setParameter("dataFinal", DateUtils.setTimeFimDia(search.dataFinal));
		}
		
		if (search.novasNotificacoes != null) {
			q.setParameter("novasNotificacoes", search.novasNotificacoes);
		}
		
		if(StringUtils.isNotEmpty(search.texto)) {
			q.setParameter("texto", "%" + search.texto + "%");
		}
		
		q.setParameter("amizadeSolicitada", StatusAmizade.SOLICITADA);
		q.setParameter("solicitada", StatusParticipacao.SOLICITADA);
		
		if (search.maxRows > 0) {
			int firstResult = search.page * search.maxRows;
			q.setFirstResult(firstResult);
			q.setMaxResults(search.maxRows);
		}
		
		q.setCacheable(cache);
		List<NotificationUsuario> list = q.list();
		
		return list;
	}

	@Override
	public List<Notification> findAllToSendPush() {
		Query q = createQuery("from Notification n where n.sendPush=1 and n.dataPush is null and n.parent is null and ((n.tipo = :newPost and n.post.statusPublicacao = :publicado) or n.tipo = :like or n.tipo = :favorito or n.tipo = :comentario)  ");
		
		q.setParameter("newPost", TipoNotificacao.NEW_POST);
		q.setParameter("like", TipoNotificacao.LIKE);
		q.setParameter("favorito", TipoNotificacao.FAVORITO);
		q.setParameter("comentario", TipoNotificacao.COMENTARIO);
		q.setParameter("publicado", StatusPublicacao.PUBLICADO);
		
		// cache para job de push
		q.setCacheable(true);
		List<Notification> list = q.list();
		return list;
	}

	@Override
	public List<Long> findIdUsersFrom(Notification parent) {
		Query q = createQuery("select notUser.usuario.id from NotificationUsuario notUser where notUser.notification.id = ? and notUser.notification.sendPush = 1");
		q.setLong(0, parent.getId());
		q.setCacheable(true);
		List<Long> list = q.list();

		return list;
	}

	@Override
	public List<UserToPushVO> findUsersFrom(Notification parent) {
		StringBuffer sb = new StringBuffer("select u.id, u.login, u.email, n.sendPush, notUser.visivel from NotificationUsuario notUser  ");
		sb.append("inner join notUser.notification n ");
		sb.append("inner join notUser.usuario u ");
		sb.append("where  notUser.notification.id = ? ");
		Query q = createQuery(sb.toString());
		q.setLong(0, parent.getId());
		List<Object[]> array = q.list();
		
		List<UserToPushVO> list = UserToPushVO.hibernate(array);
		
		return list;
	}

	@Override
	public Notification findByPost(Post p, TipoNotificacao tipo) {
		Query q = createQuery("from Notification where post.id = ? and tipo = ?");

		q.setLong(0, p.getId());
		q.setString(1, tipo.name());

		q.setCacheable(true);
		List<Notification> list = q.list();

		Notification n = list.isEmpty() ? null : list.get(0);

		return n;
	}

	@Override
	public int markAllAsRead(Usuario u) {
		StringBuffer sb = new StringBuffer("select notUser.id from NotificationUsuario notUser inner join notUser.notification n where 1=1 ");
		sb.append(" and (notUser.lida is null or notUser.lida = 0) ");
		sb.append(" and n.tipo != :tipoGrupo ");
		sb.append(" and n.tipo != :tipoAmizade ");
		sb.append(" and n.tipo != :reminder ");
		sb.append(" and notUser.usuario.id = :user ");
		sb.append(" and notUser.visivel = 1 ");
		
		Query q = createQuery(sb.toString());
		q.setParameter("tipoGrupo", TipoNotificacao.GRUPO_SOLICITAR_PARTICIPAR);
		q.setParameter("tipoAmizade", TipoNotificacao.USUARIO_SOLICITAR_AMIZADE);
		q.setParameter("reminder", TipoNotificacao.REMINDER);
		q.setParameter("user", u.getId());
		
		List<Long> idsUpdate = q.list();
		
		if(idsUpdate.size() > 0) {
			q = createQuery("update NotificationUsuario notUser set notUser.lida=1, notUser.dataLida=:data where notUser.id in(:ids) and notUser.usuario.id = :user");
			q.setParameter("data", new Date());
			q.setParameterList("ids", idsUpdate);
			q.setParameter("user", u.getId());
			return q.executeUpdate();
		}
		return 0;
	}
	
	@Override
	public List<NotificationUsuario> findPostVisivel(Usuario u){
		StringBuffer sb = new StringBuffer("from NotificationUsuario notUser inner join notUser.notification n where 1=1 ");
		sb.append(" and (notUser.lida is null or notUser.lida = 0) ");
		sb.append(" and n.tipo != :tipoGrupo ");
		sb.append(" and n.tipo != :tipoAmizade ");
		sb.append(" and notUser.usuario = :user ");
		sb.append(" and notUser.visivel = 1 ");
		
		Query q = createQuery(sb.toString());
		q.setParameter("tipoGrupo", TipoNotificacao.GRUPO_SOLICITAR_PARTICIPAR);
		q.setParameter("tipoAmizade", TipoNotificacao.USUARIO_SOLICITAR_AMIZADE);
		q.setParameter("user", u);
		
		return (List<NotificationUsuario>) q.list();
		
	}
	
	
	


	@Override
	public int markAsSent(Long id) {
		Query q = createQuery("update Notification n set n.sendPush=0, n.dataPush=:data where n.id = :id");
		
		q.setParameter("data", new Date());
		q.setParameter("id", id);
		
		int count  = q.executeUpdate();
		
		return count;
	}

	@Override
	public void markNotificationsAsRead(NotificationUsuario notUser, Post p, TipoNotificacao tipo) {
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("update NotificationUsuario notUser ");
		sb.append(" set notUser.dataLida=:data, notUser.lida=:lida where 1=1 ");

		if(p != null && tipo != null && notUser.getUsuario() != null) {
			sb.append(" and notUser.notification in (select n from Notification n where n.post = :post and n.tipo = :tipo and notUser.usuario = :user) ");
		} else {
			sb.append(" and notUser.id = :id ");
		}
		
		Query q = createQuery(sb.toString());
		
		if(p != null && tipo != null && notUser.getUsuario() != null) {
			q.setParameter("post", p);
			q.setParameter("tipo", tipo);
			q.setParameter("user", notUser.getUsuario());
		} else {
			q.setParameter("id", notUser.getId());
		}
		
		q.setParameter("data", new Date());
		q.setParameter("lida", Boolean.TRUE);
		
		q.executeUpdate();
	}
	
	@Override
	public List<Object[]> findUsersWithoutBadgesToPush(List<Long> usersId) {
		/**
		 * array[0] = Long id
		 * array[1] = String login
		 * array[2] = 0
		 * array[3] = nome do usuario
		 * array[4] = email do usuario
		 */
		String sql = "select id, login, 0, nome, email from Usuario where id in (:users)";

		Query q = createQuery(sql);
		q.setParameterList("users", usersId);

		q.setCacheable(true);

		List<Object[]> list = q.list();
		return list;
	}
	
	@Override
	public List<RelatorioNotificationsVO> reportNotifications(RelatorioFiltro filtro) throws DomainException {
		Query query = queryReportNotifications(filtro);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		// report nao precisa cache
		query.setCacheable(false);
		List<RelatorioNotificationsVO> notifications = query.list();

		return notifications;
	}

	public Query queryReportNotifications(RelatorioFiltro filtro) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;

		if (StringUtils.isNotEmpty(filtro.getDataIni())) {
			filtro.setDataInicial(DateUtils.toDate(filtro.getDataIni()));
		}
		if (StringUtils.isNotEmpty(filtro.getDataFim())) {
			filtro.setDataFinal(DateUtils.toDate(filtro.getDataFim()));
		}
		
		Long empresa = filtro.getEmpresaId();

		Usuario usuario = filtro.getUsuario();
		Post post = filtro.getPost();
		CategoriaPost categoria = filtro.getCategoria();
		TipoNotificacao tipo = filtro.getTipoEnum();
		boolean lida = filtro.isLida();

		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}
		
		StringBuffer sb = new StringBuffer();
		sb.append("select new br.livetouch.livecom.domain.vo.RelatorioNotificationsVO(n) ");

		sb.append(" from Notification n");

		sb.append(" where 1=1 ");
		
		if (usuario != null) {
			sb.append(" and (n.usuario = :usuario or n.id in(select notUser.notification.id from NotificationUsuario notUser where notUser.notification.id = n.id AND notUser.usuario = :usuario)) ");
		}
		
		if(lida) {
			sb.append(" and n.lida = 1 ");
		}

		if (dataInicio != null) {
			sb.append(" and n.data >= :dataIni");
		}
		if (dataFim != null) {
			sb.append(" and n.data <= :dataFim");
		}

		if (tipo != null) {
			sb.append(" and n.tipo = :tipo");
		}

		if (post != null) {
			sb.append(" and n.post = :post");
		}
		
		if(empresa != null) {
			sb.append(" and n.usuario.empresa.id = :empresa");
		}
		
		sb.append(" order by n.data desc");

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}

		if (post != null) {
			q.setParameter("post", post);
		}
		
		if (categoria != null) {
			q.setParameter("categoria", categoria);
		}
		
		if (tipo != null) {
			q.setParameter("tipo", tipo);
		}
		
		if(empresa != null) {
			q.setParameter("empresa", empresa);
		}

		return q;

	}
	
	@Override
	public List<RelatorioNotificationsVO> reportNotificationsByType(RelatorioFiltro filtro) throws DomainException {
		Query query = queryReportNotificationsByType(filtro);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		query.setCacheable(true);
		List<RelatorioNotificationsVO> notifications = query.list();

		return notifications;
	}

	public Query queryReportNotificationsByType(RelatorioFiltro filtro) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;

		if (StringUtils.isNotEmpty(filtro.getDataIni())) {
			filtro.setDataInicial(DateUtils.toDate(filtro.getDataIni()));
		}
		if (StringUtils.isNotEmpty(filtro.getDataFim())) {
			filtro.setDataFinal(DateUtils.toDate(filtro.getDataFim()));
		}

		Long empresa = filtro.getEmpresaId();
		
		Usuario usuario = filtro.getUsuario();
		Post post = filtro.getPost();
		CategoriaPost categoria = filtro.getCategoria();
		TipoNotificacao tipo = TipoNotificacao.valueOf(filtro.getTipo());
		
		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		sb.append("select new br.livetouch.livecom.domain.vo.RelatorioNotificationsVO(count(distinct n.from),p) ");

		sb.append(" from Notification n left join n.post p");
		
		if (tipo != null) {
			if (TipoNotificacao.FAVORITO.equals(tipo)) {
				sb.append(" inner join p.favoritos fav");
			} else {
				sb.append(" inner join p.likes fav");
			}
		}

		sb.append(" where 1=1 ");
		
		if (tipo != null) {
			sb.append(" and n.tipo = :tipo");
			sb.append(" and fav.favorito = 1");
		}

		sb.append(" and n.comentario is null");

		if (dataInicio != null) {
			sb.append(" and p.dataPublicacao >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and p.dataPublicacao <= :dataFim");
		}

		if (usuario != null) {
			sb.append(" and (n.to = :usuario or n.from = :usuario)");
		}

		if (post != null) {
			sb.append(" and p = :post");
		}

		//Incluir empresa na notificacao
		if (empresa != null) {
			sb.append(" and n.from.empresa.id = :empresa");
		}
		
		if (categoria != null) {
			sb.append(" and p.categoria = :categoria");
		}

		sb.append(" group by p, p.dataPublicacao");
		sb.append(" order by p.dataPublicacao desc");

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}

		if (post != null) {
			q.setParameter("post", post);
		}
		
		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		if (categoria != null) {
			q.setParameter("categoria", categoria);
		}
		
		if (tipo != null) {
			q.setParameter("tipo", tipo);
		}

		return q;

	}

	@Override
	public List<RelatorioNotificationsVO> reportNotificationsByTypeDetalhes(RelatorioFiltro filtro) throws DomainException {

		Query query = queryReportNotificationsByTypeDetalhes(filtro);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		if (filtro.getMax() != 0) {
			query.setFirstResult(firstResult);
			query.setMaxResults(filtro.getMax());
		}

		// report sem cache
		query.setCacheable(false);
		List<RelatorioNotificationsVO> notifications = query.list();

		return notifications;

	}

	public Query queryReportNotificationsByTypeDetalhes(RelatorioFiltro filtro) throws DomainException {

		Long post = filtro.getPostId();
		TipoNotificacao tipo = TipoNotificacao.valueOf(filtro.getTipo());

		StringBuffer sb = new StringBuffer();
		sb.append("select new br.livetouch.livecom.domain.vo.RelatorioNotificationsVO(n.data, n.post, n.from) ");

		sb.append(" from Notification n");

		sb.append(" where 1=1 ");
		
		if (tipo != null) {
			sb.append(" and n.tipo = :tipo");
		}
		
		sb.append(" and n.comentario is null");

		if (post != null) {
			sb.append(" and n.post.id = :post");
		}

		sb.append(" order by n.data desc");

		Query q = createQuery(sb.toString());

		if (post != null) {
			q.setParameter("post", post);
		}
		
		if (tipo != null) {
			q.setParameter("tipo", tipo);
		}

		q.setCacheable(true);

		return q;

	}

	@Override
	public int markPostAsRead(Post p, Usuario u) {
		StringBuffer sb = new StringBuffer();
		
		sb.append("update NotificationUsuario nu set nu.lida=1, nu.dataLida=:data where 1=1 ");
		sb.append(" and (nu.lida is null or nu.lida = 0) ");
		
		sb.append(" AND nu.notification.id in (");
		sb.append(" select distinct n.id from Notification n " +
                " where (n.tipo = :like or n.tipo = :favorito or n.tipo = :newPost) " +
                " and n.post.id = :postId and nu.usuario.id = :user");
		sb.append(") and nu.usuario.id = :user");

		Query q = createQuery(sb.toString());
		
		q.setParameter("data", new Date());
		q.setParameter("user", u.getId());
		q.setParameter("like", TipoNotificacao.LIKE);
		q.setParameter("favorito", TipoNotificacao.FAVORITO);
		q.setParameter("newPost", TipoNotificacao.NEW_POST);
		q.setParameter("postId", p.getId());
		
		return q.executeUpdate();
	}

	@Override
	public int markGrupoAsRead(Grupo g, Usuario u) {
		StringBuffer sb = new StringBuffer("SELECT notUser.id FROM NotificationUsuario notUser inner join notUser.notification n where 1=1");
		sb.append(" and (notUser.lida is null or notUser.lida = 0)");
		sb.append(" and (n.tipo = :participar or n.tipo = :sair) ");
		sb.append(" and n.grupo.id = :grupo ");
		sb.append(" and notUser.usuario.id = :usuario ");
		Query q = createQuery(sb.toString());
		q.setParameter("participar", TipoNotificacao.GRUPO_PARTICIPAR);
		q.setParameter("sair", TipoNotificacao.GRUPO_SAIR);
		q.setParameter("grupo", g.getId());
		q.setParameter("usuario", u.getId());
		List<Long> ids = q.list();
		
		if(ids.size() > 0) {
			sb = new StringBuffer("update NotificationUsuario n set n.lida=1, n.dataLida=:data ");
			sb.append("where 1=1 and n.id in(:ids)");
			q = createQuery(sb.toString());
			q.setDate("data", DateUtils.toSqlDate(new Date()));
			q.setParameterList("ids", ids);
			int count  = q.executeUpdate();
			return count;
		}
		return 0;
	}
	
	@Override
	public int markSolicitacaoAsRead(Grupo g, Usuario u) {
		StringBuffer sb = new StringBuffer("SELECT notUser.id FROM NotificationUsuario notUser inner join notUser.notification n ");
		sb.append(" where 1=1 ");
		sb.append(" and (notUser.lida is null or notUser.lida = 0) ");
		sb.append(" and n.tipo = :tipo ");
		sb.append(" and n.grupo.id = :grupo ");
		sb.append(" and notUser.usuario.id = :usuario ");
		Query q = createQuery(sb.toString());
		q.setParameter("tipo", TipoNotificacao.GRUPO_SOLICITAR_PARTICIPAR);
		q.setParameter("grupo", g.getId());
		q.setParameter("usuario", u.getId());
		List<Long> ids = q.list();
		
		if(ids.size() > 0) {
			sb = new StringBuffer("UPDATE NotificationUsuario notUser set notUser.dataLida = :data, lida = 1 where notUser.id in(:ids)");
			q = createQuery(sb.toString());
			q.setParameter("data", new Date());
			q.setParameterList("ids", ids);
			return q.executeUpdate();
		}
		return 0;
	}

	@Override
	public int markSolicitacaoAmizadeAsRead(Usuario u, Usuario amigo) {
		Long usuarioId = u.getId();
		Long amigoId = amigo.getId();
		
		
		StringBuffer sb = new StringBuffer("select notUser.id from NotificationUsuario notUser inner join notUser.notification n where notUser.usuario.id = :amigo and n.usuario.id = :usuario and n.tipo = :tipo");
		Query q = createQuery(sb.toString());
		q.setParameter("usuario", usuarioId);
		q.setParameter("tipo", TipoNotificacao.USUARIO_SOLICITAR_AMIZADE);
		q.setParameter("amigo", amigoId);
		List<Long> ids = q.list();
		if(ids.size() > 0) {
			sb = new StringBuffer("update NotificationUsuario n set n.lida=1, n.dataLida=:data ");
			sb.append("where 1=1 and (n.lida is null or n.lida = 0) ");
			sb.append(" and n.id in(:ids)");
			q = createQuery(sb.toString());
			q.setDate("data", DateUtils.toSqlDate(new Date()));
			q.setParameterList("ids", ids);
			int count  = q.executeUpdate();
			return count;
		}
		return 0;
	}
	
	@Override
	public int markComentarioAsRead(Long p, Long u) {
		StringBuffer sb = new StringBuffer("SELECT n.id from NotificationUsuario notUser inner join notUser.notification n where n.tipo = :comentario and n.post.id = :postId and notUser.usuario.id = :usuario and (notUser.lida is null or notUser.lida = 0)");
		Query q = createQuery(sb.toString());
		q.setParameter("comentario", TipoNotificacao.COMENTARIO);
		q.setParameter("postId", p);
		q.setParameter("usuario", u);
		List<Long> notifications = q.list();
		if(notifications.size() > 0) {
			sb = new StringBuffer("update NotificationUsuario n set n.lida=1, n.dataLida=:data where 1=1 ");
			sb.append(" and (n.lida is null or n.lida = 0) ");
			sb.append(" AND n.notification.id in(:notifications)");
			sb.append(" AND n.usuario.id = :usuario");
			q = createQuery(sb.toString());
			q.setDate("data", DateUtils.toSqlDate(new Date()));
			q.setParameter("data", new Date());
			q.setParameterList("notifications", notifications);
			q.setParameter("usuario", u);
			int count  = q.executeUpdate();
			return count;
		}
		return 0;
	}
	
	@Override
	public void delete(List<Notification> notifications) {
		StringBuffer sb = new StringBuffer("delete from Notification n where n.parent in(:list)");
		Query q = createQuery(sb.toString());
		q.setParameterList("list", notifications);
		q.executeUpdate();
		
		sb = new StringBuffer("delete from Notification n where n in(:list)");
		q = createQuery(sb.toString());
		q.setParameterList("list", notifications);
		q.executeUpdate();
	}

	@Override
	public List<Notification> findViewsByFilter(RelatorioVisualizacaoFiltro filtro) {
		Date inicial = null;
		Date dFinal = null;
		
		Long empresa = filtro.getEmpresaId();
		
		if (filtro.getDataInicial() != null) {
			inicial = DateUtils.setTimeInicioDia(filtro.getDataInicial());
		}
		if (filtro.getDataFinal() != null) {
			dFinal = DateUtils.setTimeFimDia(filtro.getDataFinal());
		}

		StringBuffer sb = new StringBuffer("select n from Notification n ");
		sb.append(" where 1=1 ");
		if (inicial != null) {
			sb.append(" and n.data >= :dataIni ");
		}
		if (dFinal != null) {
			sb.append(" and n.data <= :dataFim ");
		}
		if (filtro.getUsuarioId() != null) {
			sb.append(" and (n.to.id= :usuario or n.from.id= :usuario) ");
		}
		if (filtro.getPostId() != null) {
			sb.append(" and n.post.id= :post ");
		}
		
		if (empresa != null) {
			sb.append(" and (n.to.empresa.id = :empresa or n.from.empresa.id = :empresa)");
		}
		
		if (filtro.getCategoriaId() != null) {
			sb.append(" and n.post.categoria.id= :categoria ");
		}
		sb.append("order by n.post.dataPublicacao DESC, p.data DESC");
		Query q = createQuery(sb.toString());

		if (inicial != null) {
			q.setParameter("dataIni", inicial);
		}
		if (dFinal != null) {
			q.setParameter("dataFim", dFinal);
		}
		if (filtro.getUsuarioId() != null) {
			q.setLong("usuario", filtro.getUsuarioId());
		}
		if (filtro.getPostId() != null) {
			q.setLong("post", filtro.getPostId());
		}
		
		if (empresa != null) {
			q.setLong("empresa", empresa);
		}
		
		if (filtro.getCategoriaId() != null) {
			q.setLong("categoria", filtro.getCategoriaId());
		}

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		q.setFirstResult(firstResult);
		q.setMaxResults(filtro.getMax());

		// report sem cache
		q.setCacheable(false);
		List<Notification> list = q.list();
		return list;
	}

	@Override
	public List<NotificationVO> findAllGroupUserByPost(Usuario usuario, Post post) {
		StringBuffer sb = new StringBuffer("select distinct new br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO (DATE_FORMAT(p.dataPublicacao,'%d/%m/%Y %H:%m:%s'), p, SUM(coalesce(pv.contador,0)), SUM(coalesce(pv.contadorLido,0))) ");
		
		sb.append(" from PostView pv inner join pv.post p ");

		sb.append(" where 1=1 ");
		
		if (post != null) {
			sb.append(" and p= :post");
		}
		if (usuario != null) {
			sb.append(" and pv.usuario = :usuario)");
		}
		
		sb.append(" group by pv.data, pv.id");
		sb.append(" order by pv.data desc");
		
		Query q = createQuery(sb.toString());

		if (post != null) {
			q.setParameter("post", post);
		}
		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}
		
		List<NotificationVO> list = q.list();
		return list;
	}
	
	@Override
	public AudienciaVO getCountAudienciaByPost(Post post) {

		Long postId = post.getId();

		StringBuffer sb = new StringBuffer();
// aumento exponencial de tempo de consulta em posts com muita audiencia
//		sb.append("select count(*) as iteracao ");
//		sb.append("from (select distinct u.id ");
//		sb.append("from post p ");
//		sb.append("inner join post_grupos pg on pg.post_id = p.id ");
//		sb.append("inner join grupo_usuarios gu on pg.grupo_id = gu.grupo_id ");
//		sb.append("inner join usuario u on gu.usuario_id = u.id ");
//		sb.append("inner join post_view pv on pv.post_id = p.id and (pv.lido = 1 or pv.visualizado = 1)");
//		sb.append("left join comentario c on c.post_id = p.id and c.usuario_id = u.id ");
//		sb.append("left join favorito f on f.post_id = p.id and f.favorito = 1 ");
//		sb.append("left join likes l on l.post_id = p.id and l.comentario_id is null and l.favorito = 1 ");
//		sb.append("where 1=1 ");
//		sb.append("and p.id = :post and (u.id in(l.usuario_id) OR u.id in(f.usuario_id) OR u.id in(c.usuario_id) OR u.id in(pv.usuario_id))) as iter");
		
		/** Otimização de consultas com LEFT JOIN
		 * http://www.postgresql-archive.org/Otimizar-consulta-com-LEFT-JOIN-td2035417.html **/
		
		sb.append("SELECT COUNT(DISTINCT id) AS iteracao FROM (");
		sb.append("SELECT DISTINCT u.id FROM post p INNER JOIN "
				+ "post_grupos pg ON pg.post_id = p.id INNER JOIN "
				+ "grupo_usuarios gu ON pg.grupo_id = gu.grupo_id "
				+ "INNER JOIN usuario u ON gu.usuario_id = u.id "
				+ "INNER JOIN post_view pv ON pv.post_id = p.id "
				+ "AND (pv.lido = 1 OR pv.visualizado = 1) WHERE "
				+ "p.id = :post AND (u.id IN (pv.usuario_id)) ");
		sb.append("UNION ALL ");
		sb.append("SELECT DISTINCT u.id FROM post p INNER JOIN "
				+ "post_grupos pg ON pg.post_id = p.id INNER JOIN "
				+ "grupo_usuarios gu ON pg.grupo_id = gu.grupo_id "
				+ "INNER JOIN usuario u ON gu.usuario_id = u.id "
				+ "INNER JOIN comentario c ON c.post_id = p.id "
				+ "AND c.usuario_id = u.id WHERE p.id = :post "
				+ "AND (u.id IN (c.usuario_id)) ");
		sb.append("UNION ALL ");
		sb.append("SELECT DISTINCT u.id FROM post p INNER JOIN "
				+ "post_grupos pg ON pg.post_id = p.id INNER JOIN "
				+ "grupo_usuarios gu ON pg.grupo_id = gu.grupo_id "
				+ "INNER JOIN usuario u ON gu.usuario_id = u.id "
				+ "INNER JOIN favorito f ON f.post_id = p.id AND "
				+ "f.favorito = 1 WHERE p.id = :post AND (u.id IN "
				+ "(f.usuario_id)) ");
		sb.append("UNION ALL ");
		sb.append("SELECT DISTINCT u.id FROM post p INNER JOIN "
				+ "post_grupos pg ON pg.post_id = p.id INNER JOIN "
				+ "grupo_usuarios gu ON pg.grupo_id = gu.grupo_id "
				+ "INNER JOIN usuario u ON gu.usuario_id = u.id "
				+ "INNER JOIN likes l ON l.post_id = p.id AND "
				+ "l.comentario_id IS NULL AND l.favorito = 1 WHERE "
				+ "p.id = :post AND (u.id IN (l.usuario_id)) ");
		sb.append(") AS iter");
		
		
		Query q = getSession().createSQLQuery(sb.toString())
				.addScalar("iteracao",StandardBasicTypes.LONG)
		        .setResultTransformer(new AliasToBeanResultTransformer(AudienciaVO.class));

		if (post != null) {
			q.setParameter("post", postId);
		}

		AudienciaVO audiencia = (AudienciaVO) q.uniqueResult();

		return audiencia;

	}
	
	/**
	 * array[0] = Long userId
	 * array[1] = String login
	 * array[2] = badge post
	 * array[3] = nome do usuario
	 * array[4] = email do usuario
	 */
	@Override
	public List<Object[]> findBadgesToPush(TipoNotificacao tipo, List<Long> usersId) {
		StringBuffer sb = new StringBuffer("select u.id, u.login, count(notUser.id), u.nome, u.email from NotificationUsuario notUser  ");
		sb.append("inner join notUser.notification n ");
		sb.append("inner join notUser.usuario u ");
		sb.append("where  notUser.lida = 0 and u.id in(:usersId) ");
		sb.append("and n.tipo = :tipo and (n.dataPublicacao is null OR n.dataPublicacao <= :now)");
		sb.append("group by u.id, u.login, u.nome, u.email");
		
		Query q = createQuery(sb.toString());
		q.setParameter("tipo", tipo);
		q.setParameterList("usersId", usersId);
		q.setParameter("now", new Date());
		// no cache
		List<Object[]> list = q.list();
		return list;
	}
	
	@Override
	public NotificationBadge findBadges(Usuario u) {
		NotificationBadge badges = new NotificationBadge();
		if(u != null) {
			ParametrosMap params = ParametrosMap.getInstance(u.getEmpresa());
			
			boolean amizadeOn = params.getBoolean(Params.MURAL_AMIZADE_ON,false);
			boolean gruposParticipacao = params.getBoolean(Params.MURAL_GRUPO_PARTICIPACAO_ON,false);

			Long totalBadges = getTotalBadges(u);
			Long usuarioAmizadeSolicitada = 0L;
			Long grupoSolicitarParticipar = 0L;
			Long grupos = 0L;
			Long reminders = 0L;
			
			 if(amizadeOn) {
				 usuarioAmizadeSolicitada = getCountByTipoNotificacao(u,TipoNotificacao.USUARIO_SOLICITAR_AMIZADE);
			} 
			if(gruposParticipacao) {
				grupoSolicitarParticipar = getCountByTipoNotificacao(u, TipoNotificacao.GRUPO_SOLICITAR_PARTICIPAR);
				grupos = getCountByTipoNotificacao(u, TipoNotificacao.GRUPO_PARTICIPAR, TipoNotificacao.GRUPO_SAIR,
						TipoNotificacao.GRUPO_USUARIO_ACEITO, TipoNotificacao.GRUPO_USUARIO_ADICIONADO, TipoNotificacao.GRUPO_USUARIO_RECUSADO, TipoNotificacao.GRUPO_USUARIO_REMOVIDO);
			}
			
			reminders = getCountByTipoNotificacao(u, TipoNotificacao.REMINDER);

			badges.grupoSolicitarParticipar = grupoSolicitarParticipar;
			badges.usuarioAmizadeSolicitada = usuarioAmizadeSolicitada;
			badges.reminders = reminders;
			badges.total = totalBadges + grupoSolicitarParticipar + usuarioAmizadeSolicitada + grupos;
		}
		return badges;
	}

	private Long getCountByTipoNotificacao(Usuario u, TipoNotificacao...tipoNotificacoes) {
		StringBuffer sb = new StringBuffer("SELECT count(notUser.id) FROM NotificationUsuario notUser inner join notUser.notification n");
		sb.append(" where notUser.lida = 0 AND notUser.usuario.id = :usuarioId ");
		if(tipoNotificacoes.length > 0) {
			sb.append(" AND n.tipo in(:tipos) ");
		}
		Query q = createQuery(sb.toString());
		q.setParameter("usuarioId", u.getId());
		if(tipoNotificacoes.length > 0) {
			q.setParameterList("tipos", tipoNotificacoes);
		}
		q.setCacheable(true);
		return (Long) (q.list().size() > 0 ? q.list().get(0) : 0L);
	}

	@Override
	public Long getTotalBadges(Usuario u) {
		StringBuffer sb = new StringBuffer("select count(n.id) from NotificationUsuario n inner join n.notification notification inner join notification.post p");
		sb.append(" WHERE n.lida = 0 AND n.visivel = 1 AND n.usuario.id = :usuarioId AND p.statusPublicacao = :statusPublicacao AND notification.tipo != :reminder");
		Query q = createQuery(sb.toString());
		q.setParameter("statusPublicacao", StatusPublicacao.PUBLICADO);
		q.setParameter("reminder", TipoNotificacao.REMINDER);
		q.setParameter("usuarioId", u.getId());
		q.setCacheable(true);
		Long total = (Long) (q.list().size() > 0 ? q.list().get(0) : 0L);
		return total;
	}
	
	@Override
	public List<Notification> findByGrupo(Grupo g){
		StringBuffer sb = new StringBuffer("select n from Notification n inner join n.grupo g where g = :grupo ");

		sb.append(" order by n.id desc");
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("grupo", g);
		
		// no cache: Query usada para deletar notifications do grupo ao excluir grupo
		q.setCacheable(false);
		List<Notification> list = q.list();
		return list;
	}

	@Override
	public List<NotificationUsuario> findNotificationsFilhas(Notification notification) {
		StringBuffer sb = new StringBuffer("from NotificationUsuario notUsuario where notUsuario.notification = :parent ");

		sb.append(" order by notUsuario.id desc");
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("parent", notification);
		
		// no cache: usada para report
		q.setCacheable(false);
		List<NotificationUsuario> list = q.list();
		return list;
	}

	@Override
	public void deleteFilhas(Notification n) {
		execute("delete from NotificationUsuario n where n.notification.id = ?", n.getId());
	}

}