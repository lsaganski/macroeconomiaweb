package br.livetouch.livecom.domain.vo;

import java.util.List;

/**
 * Classe que retorna a qtde de registros atualizados para recebida/lida do chat.
 * 
 * Tambem retorna se todos os usuarios do grupo leram a mensagem.
 * 
 * @author rlech
 */
public class UpdateStatusMensagemResponseVO {
	/**
	 * qtde update no banco
	 */
	public int qtdeModified;

	// qtde de usuarios no grupo
	public long qtdeUsers;
	// entregue: qtde de usuarios com status 2 check cinza
	public long qtdeStatus1;
	// lida: qtde de usuarios com status 2 check azul
	public long qtdeStatus2;

	public MensagemVO mensagem;

	/**
	 * Usuarios para avisar do update status
	 */
	public List<UsuarioVO> usuarios;

	public UpdateStatusMensagemResponseVO() {
		
	}

	public UpdateStatusMensagemResponseVO(long qtde, long qtdeStatus1, long qtdeStatus2) {
		super();
		this.qtdeUsers = qtde;
		this.qtdeStatus1 = qtdeStatus1; // entregue
		this.qtdeStatus2 = qtdeStatus2; // lida
	}

	public boolean isAllRecebida() {
		return qtdeUsers == qtdeStatus1;
	}
	
	public boolean isAllLida() {
		return qtdeUsers == qtdeStatus2;
	}

	@Override
	public String toString() {
		return "qtdeModified=" + qtdeModified + ", qtde=" + qtdeUsers + ", qtdeStatus1=" + qtdeStatus1 + ", qtdeStatus2=" + qtdeStatus2 + "]";
	}
	
	
}