package br.livetouch.livecom.push;

import java.io.IOException;
import java.util.List;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.pushserver.lib.DeviceFirebaseVO;


public interface PushLivecomService  {

	/**
	 * Push para 1 usuario
	 */
	String push(long pushId, Usuario to, PushNotificationVO notification) throws IOException;

	/**
	 * Salva apenas a msg para enviar em batch
	 */
	String push(long pushId, Empresa empresa, PushNotificationVO notification) throws IOException;

	/**
	 * Push para (n) usuarios
	 */
	String push(long pushId, List<Long> userIds, PushNotificationVO notification) throws IOException;
	
	/**
	 * Ler todos os pushs
	 */
	String readAllPushs(Usuario to) throws IOException;

	String sendToAll(ParametrosMap params, long pushId, PushNotificationVO notification) throws IOException;
	
	String unregister(Empresa empresa, DeviceFirebaseVO device) throws IOException;
	
	/**
	 * Push silent
	 */
	String silent(List<Long> userIds, PushNotificationVO notification) throws IOException;

}
