package br.livetouch.livecom.domain.enums;

public enum BBAWebview {
	sobre("Sobre este Aplicativo"),
	termos("Termos de Uso");
	
	private String label;
	
	BBAWebview(String label) {
		this.label = label;
	}
	
	@Override
	public String toString() {
		return this.label != null ? this.label : "";
	}
	
}
