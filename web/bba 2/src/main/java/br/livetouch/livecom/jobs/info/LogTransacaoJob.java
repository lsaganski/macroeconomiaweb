package br.livetouch.livecom.jobs.info;

import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.LogTransacao;

/**
 * Os logs adicionados aqui serao processados pelo LivecomJob
 * 
 * @author live
 *
 */
public class LogTransacaoJob {
	
	protected List<LogTransacao> list;
	protected static LogTransacaoJob instance = null;
	
	public LogTransacaoJob() {
		list = new ArrayList<>();
	}
	
	public static LogTransacaoJob getInstance() {
		if (instance == null) {
			instance = new LogTransacaoJob();
		}		
		return instance;
	}

	public void addLogTransacao(LogTransacao logTransacao) {
		list.add(logTransacao);
	}
	
	public List<LogTransacao> getList() {
		return list;
	}
	
	public ArrayList<LogTransacao> getListAndDelete() {
		ArrayList<LogTransacao> logsList = new ArrayList<>(this.list);
		this.list.removeAll(this.list);
		
		return logsList;
	}
	
	
}
