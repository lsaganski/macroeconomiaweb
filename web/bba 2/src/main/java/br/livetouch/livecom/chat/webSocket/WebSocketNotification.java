package br.livetouch.livecom.chat.webSocket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.chatAkka.protocol.RawMessage;
import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.utils.JSONUtils;
 
@ServerEndpoint(value="/notification/{idUsuario}", encoders=WebSocketNotificationEncoder.class, decoders=WebSocketNotificationDecoder.class)
public class WebSocketNotification {

	private static Logger log = Log.getLogger(WebSocketNotification.class);
	
	public static Map<Long, List<Session>> webConnections = new HashMap<Long, List<Session>>();
	
	@OnOpen
	public void open(final Session wsSession, @PathParam("idUsuario") String idUsuario) {
		log("open wsSession userId: " + idUsuario);
		wsSession.getUserProperties().put("idUsuario", idUsuario);
		
		/**
		 * TUDO IGUAL CHAT, sem web actor
		 */
//		AkkaFactory.createWebConnectionActor(wsSession, Long.parseLong(idUsuario));
		
		addConnection(Long.parseLong(idUsuario), wsSession);
		
	}
 
	@OnMessage
	public void onMessage(final Session wsSession, final RawMessage rawMessage) {
		Long userId = WebSocketNotification.getUserId(wsSession);
		
		/**
		 * Receber msgs do WebSocket do Chat
		 */
		log("WebSocket ChatEndpoint onMessage wsSession userId: " + userId + ", msg: " + rawMessage);
		
		/**
		 * Ao receber msg, manda pro web socket da notification
		 */
		String json = "json q vem do chat";
		
		sendMsg(wsSession, userId, json);
	}
	
	@OnClose
	public void closedConnection(Session wsSession, @PathParam("idUsuario") String idUsuario) {
		log("closedConnection: " + wsSession + ", idUsuario: " + idUsuario);
		
//		AkkaHelper.stopWebConnectionActor(wsSession);
		
		Long userId = NumberUtils.toLong(idUsuario);

		revemeConnection(userId, wsSession);
	}

	@OnError
	public void error(Session wsSession, Throwable t, @PathParam("idUsuario") String idUsuario) {
		Long userId = NumberUtils.toLong(idUsuario);

		AkkaHelper.stopWebConnectionActor(wsSession);

		revemeConnection(userId, wsSession);
	}
	
	public static void addConnection(long userId, Session wsSession) {
		List<Session> sessions =  webConnections.get(userId);
		if (sessions == null) {
			sessions = new ArrayList<Session>();
			webConnections.put(userId, sessions);
		}
		
		sessions.add(wsSession);
	}
	
	public static void revemeConnection(long userId, Session wsSession ) {
		List<Session> sessions =  webConnections.get(userId);
		if (sessions != null) {
			sessions.remove(wsSession);
		}
	}
	
	public static void removeAllConnections(long userId) {
		webConnections.remove(userId);
	}
	
	public static void sendMsg(long userId, Object obj) {
		sendMsg(userId, JSONUtils.toJSON(obj));
	}
	
	public static void sendMsg(List<Long> userIds, Object obj) {
		sendMsg(userIds, JSONUtils.toJSON(obj));
	}
	
	public static void sendMsg(List<Long> userIds, String json) {
		if(userIds != null) {
			for (Long userId : userIds) {
				sendMsg(userId, json);
			}
		}
	}
	
	public static Long getUserId(Session wsSession) {
		String s = (String) wsSession.getUserProperties().get("idUsuario");
		Long userId = NumberUtils.toLong(s,0);
		return userId;
	}

	/**
	 * Envia mensagem pelo WebSocket para o usuario
	 * 
	 * @param userId
	 * @param json
	 */
	public static void sendMsg(long userId, String json) {
		List<Session> webSessions =  webConnections.get(userId);
		if (webSessions != null && !webSessions.isEmpty()) {
			log("webSocket msg to user ["+userId+"]: " + json);

			for (Session wsSession : webSessions) {
				sendMsg(wsSession,userId, json);
			}
		}
	}

	public static void sendMsg(Session wsSession,long userId, String json) {
		if (wsSession != null && wsSession.isOpen()) {
			try {
				log("webSocket.sendMsg to[" + userId + "], json: " + json);
				wsSession.getBasicRemote().sendText(json);
			}catch (Exception e) {
				log.error("erro ao enviar msg por webSocket par ao user " + userId, e);
			}
		}
	}
	
	private static void log(String s) {
		log.debug(s);
	}
}
