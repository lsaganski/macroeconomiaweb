package br.livetouch.livecom.chatAkka.actor;

import java.util.concurrent.TimeUnit;

import javax.websocket.Session;

import akka.actor.ActorIdentity;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Identify;
import akka.actor.UntypedActorContext;
import akka.dispatch.OnComplete;
import akka.pattern.AskableActorSelection;
import akka.util.Timeout;
import br.livetouch.livecom.chatAkka.AkkaChatServer;
import br.livetouch.livecom.chatAkka.protocol.JsonRawMessage;
import scala.concurrent.Await;
import scala.concurrent.Future;

/**
 * Factor para encontrar actors
 * 
 * @author rlecheta
 *
 */
public class AkkaFactory {
	private static Timeout timeout = new Timeout(3, TimeUnit.SECONDS);

	public static ActorSystem getActorySystem() {
		return AkkaChatServer.actorSystem;
	}
	
	public static ActorSelection getUserActor(ActorSystem context, String userId) {
		return context.actorSelection("akka://ChatLivecom/user/mainActor/userActor_" + userId);
	}
	
	public static ActorSelection getUserActor(Long userId) {
		return getUserActor(userId.toString());
	}
	
	public static ActorSelection getUserActor(String userId) {
		return getUserActor(getActorySystem(), userId);
	}
	
	public static ActorSelection getUserActor(UntypedActorContext context, Long userId) {
		return getUserActor(context, userId.toString());
	}
	
	public static ActorSelection getUserActor(UntypedActorContext context, String userId) {
		return context.actorSelection("akka://ChatLivecom/user/mainActor/userActor_" + userId);
	}

	public static ActorSelection getUserMessageRouter(UntypedActorContext context) {
		ActorSelection userMessageRouter = context.actorSelection("akka://ChatLivecom/user/mainActor/userMessageRouter");
		return userMessageRouter;
	}

	public static ActorSelection getMainActor() {
		return getActorySystem().actorSelection("akka://ChatLivecom/user/mainActor");
	}

	public static ActorSelection getMainActor(UntypedActorContext context) {
		return context.actorSelection("akka://ChatLivecom/user/mainActor");
	}

	public static ActorSelection getLivecomRouter(UntypedActorContext context) {
		return context.actorSelection("akka://ChatLivecom/user/mainActor/livecomRouter");
	}

	public static ActorRef createWebConnectionActor(final Session wsSession, final long userId) {
		if (getActorySystem() != null) {
	
			final String name = "webConnection_" + wsSession.getId();

			ActorSelection actorSelection = AkkaFactory.getWebConnectionActor(wsSession);

			AkkaFactory.findActorAsync(actorSelection, new FindActorCallback() {
				@Override
				public void actorNotFound() {
					// Nao existe, entao cria.
					ActorRef webUserActor = WebConnectionActor.create(getActorySystem(),wsSession,name);

					ActorSelection mainActor = AkkaFactory.getMainActor();
					
					// Cria o UserActor se precisar.
					JsonRawMessage json = new JsonRawMessage(webUserActor, null, null,"web", userId);
					MainActor.CreateUserActor create = new MainActor.CreateUserActor(json,"web", String.valueOf(userId));
					mainActor.tell(create, ActorRef.noSender());
					
					// Futuramente ao logar no chat, este ator Web é usado.
					
					// O WebConnectionActor vai criar um UserActor ao fazer login com json.

					// O UserActor será associado ao WebConnectionActor via AssociateWithUser.
				}
				
				@Override
				public void actorFound(ActorRef actorRef) {
					// Já existe, então Ok.
					System.out.println("found");
				}
			});
		}

		return null;
	}
	
	public static ActorSelection getWebConnectionActor(Session wsSession) {
		String id = wsSession.getId();
		String path = "akka://ChatLivecom/user/webConnection_" + id;
		ActorSelection a = getActorySystem().actorSelection(path);
		return a;
	}
	
	public static interface FindActorCallback {
		public void actorFound(ActorRef actorRef);
		public void actorNotFound();
	}
	
	/**
	 * Busca um ator
	 * 
	 * @param context
	 * @param from
	 * @param callback
	 */
	public static void findActorAsync(final ActorSelection actor, final FindActorCallback callback) {
		if(getActorySystem() != null) {
			Future<ActorRef> future = actor.resolveOne(timeout);
			if(future != null) {
				future.onComplete(new OnComplete<ActorRef>() {
					@Override
					public void onComplete(Throwable failure, ActorRef existingActor) throws Throwable {
						boolean actorAlreadyExists = failure == null;

						boolean found = actorAlreadyExists && existingActor != null;
						if (found) {
							callback.actorFound(existingActor);
						} else {
							callback.actorNotFound();
						}
					}
				}, getActorySystem().dispatcher());
			}
		}
		
	}
	
	public static Future<ActorRef> getUserActorFuture(UntypedActorContext context, String userId, FindActorCallback callback) {
		ActorSelection userActor = getUserActor(context, userId);
		Future<ActorRef> actorRef = userActor.resolveOne(timeout);
		return actorRef;
	}

	/**
	 * Busca o ActorRef de forma sincrona
	 * 
	 * @param actorSelection
	 * @return
	 */
	public static ActorRef findActorSync(ActorSelection actorSelection, int timeoutSeconds) {
		Timeout t = new Timeout(timeoutSeconds, TimeUnit.SECONDS);
	    AskableActorSelection asker = new AskableActorSelection(actorSelection);
	    Future<Object> fut = asker.ask(new Identify(1), t);
		try {
			ActorIdentity ident = (ActorIdentity)Await.result(fut, t.duration());
			ActorRef ref = ident.getRef();
			return ref;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
