package br.livetouch.livecom.web.pages.ws;


import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.vo.TagFilter;
import br.livetouch.livecom.domain.vo.TagVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class TagsPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private TextField tNome;

	public Long id;
	
	@Override
	public void onInit() {
		super.onInit();

		form.add(tNome = new TextField("nome"));
		tNome.setFocus(true);

		form.add(new TextField("not_tag_ids"));
		form.add(new TextField("page"));
		form.add(new TextField("maxRows"));
		
		form.add(tMode = new TextField("mode"));
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		tMode.setValue("json");

		form.add(new Submit("consultar"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			
			if(id != null) {
				Tag tag = tagService.get(id);
				TagVO tagVO = new TagVO();
				tagVO.setTag(tag);
				
				if(isWsVersion3()) {
					Response r = Response.ok("OK");
					r.tag = tagVO;
					return r;
				}
				return tagVO;
			}
			
			String nome = tNome.getValue();
			
			String maxRowsParameter = getContext().getRequestParameter("maxRows");
			int maxRows = NumberUtils.toInt(maxRowsParameter,40);

			TagFilter filter = new TagFilter();
			filter.nome = nome;
			filter.page = page;
			filter.maxRows = maxRows;
			filter.notIds = getListIds("not_tag_ids");

			List<Tag> list = tagService.findAllByFilter(filter,getEmpresa());
			List<TagVO> vo = TagVO.createByList(list);

			if(isWsVersion3()) {
				Response r = Response.ok("OK");
				r.tags = vo;
				return r;
			}
			
			return vo;
		}
		
		return new MensagemResult("NOK","Erro ao buscar as tags.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("tag", TagVO.class);
		super.xstream(x);
	}
}
