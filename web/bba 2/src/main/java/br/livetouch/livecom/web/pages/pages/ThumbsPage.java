package br.livetouch.livecom.web.pages.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;

@Controller
@Scope("prototype")
public class ThumbsPage extends LivecomLogadoPage {
	public Long arquivoId;

	public Arquivo arquivo;
	
	@Override
	public void onGet() {
		super.onGet();
		
		if(arquivoId != null) {
			arquivo  = arquivoService.get(arquivoId);
		}
	}
}
