	package br.livetouch.livecom.web.pages.admin;


import java.io.IOException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.Palestrante;
import br.livetouch.livecom.domain.service.PalestranteService;
import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.utils.UploadHelper;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.ImageField;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextArea;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class PalestrantePage extends LivecomAdminPage {
	
	@Autowired
	protected PalestranteService palestranteService;

	public Form form = new Form();
	public String msgErro;
	public Long id;

	public int page;	
	private TextArea curriculo;
	public Palestrante palestrante;
	public Long arquivoFotoId;
	private boolean novoPalestrante;
	
	@Override
	public void onInit() {
		super.onInit();

		initGetUsuario();
		
		form();
		
		if (id != null) {
			palestrante = palestranteService.get(id);
		} else {
			palestrante = new Palestrante();
		}
		form.copyFrom(palestrante);
	}
	
	public void form(){
		form.add(new IdField());

		TextField t = null;
		
		t = new TextField("nome", getMessage("nome.label"), true);
		t.setAttribute("class", "input");
		form.add(t);
		
		t = new TextField("email", getMessage("email.label"), true);
		t.setAttribute("class", "email input");
		form.add(t);
		
		curriculo = new TextArea("curriculo", getMessage("curriculo.label"), true);
		curriculo.setRows(15);
		form.add(curriculo);
		
		form.add(new ImageField("foto", getMessage("foto.label")));
		
		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);
		
		if (id != null) {
			Submit tDeletar = new Submit("deletar", getMessage("deletar.label"), this, "deletar");
			tDeletar.setAttribute("class", "botao btSalvar");
			form.add(tDeletar);
		}
		Submit cancelar = new Submit("cancelar", getMessage("cancelar.label"), this,"cancelar");
		cancelar.setAttribute("class", "botao");
		form.add(cancelar);

		//setFormTextWidth(form);
	}
	
	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				palestrante = id != null ? palestranteService.get(id) : new Palestrante();
				form.copyTo(palestrante);
				
				setFotoPalestrante();

				palestranteService.saveOrUpdate(palestrante);

				setFlashAttribute("msg",  getMessage("msg.palestrante.salvar.sucess", palestrante.getNome()));
				setRedirect(PalestrantesPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		} else {
			setFlashAttribute("msg",  getMessage("msg.campos.obrigatorio.error"));
		}
		return true;
	}	
		
	protected void createThumbsArquivoFoto(Arquivo a) throws IOException {
		palestrante.setFotoS3(a.getUrl());
		
		UploadHelper.log.debug("Arquivo: " + a.getId());
		
		UploadHelper.log.debug("Gerando thumbs arquivo: " + a.getNome());
		ThumbVO voThumb = UploadHelper.createThumbFotoUsuario(getParametrosMap(), "palestrante/fotoProfile", a.getUrl(), a.getNome());
		ArquivoThumb t = new ArquivoThumb();
		t.setThumbVo(voThumb);
		t.setArquivo(a);
		arquivoService.saveOrUpdate(t);

		palestrante.setFoto(a);
	}


	/**
	 * Se o web service enviar este id, atualiza a foto com o arquivo.
	 * @throws IOException 
	 */
	private void setFotoPalestrante() throws IOException {
		if(arquivoFotoId != null) {
			Arquivo a = arquivoService.get(arquivoFotoId);
			
			if(a != null) {
				createThumbsArquivoFoto(a);
			}
		}
	}

	public boolean cancelar() {
		setRedirect(PalestrantesPage.class);
		return true;
	}

	public boolean deletar() {
		try {
			try {
				palestrante = id != null ? palestranteService.get(id) : new Palestrante();

				palestranteService.delete(palestrante);

				setFlashAttribute("msg",  getMessage("msg.palestrante.excluir.sucess"));
				setRedirect(PalestrantesPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (ConstraintViolationException e) {
				logError(e.getMessage(),e);
				this.msgErro = getMessage("msg.palestrante.excluir.error");
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.palestrante.excluir.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();
	}
	
	protected void initGetUsuario() {
		if (id != null) {
			palestrante = palestranteService.get(id);
		} else {
			novoPalestrante = true;
		}
	}
}
