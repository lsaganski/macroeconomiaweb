package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Usuario;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class ManageAdminGrupoMensagemPage extends WebServiceFormPage {

	public Long user;
	public Long grupo;
	public Long usuario;
	public String action;

	@Override
	protected void form() {
		form.add(new TextField("user", true));
		form.add(new TextField("grupo", true));
		form.add(new TextField("usuario", true));
		form.add(new TextField("action", true));
	}

	@Override
	protected Object go() throws Exception {
		if (form.isValid()) {
			
			//verificar permissão para conceder admins no grupo
			Usuario admin = null;
			if (user != null && user > 0) {
				admin = usuarioService.get(user);
				if (admin == null) {
					return new MensagemResult("NOK", "Admin do grupo não encontrado com o id [" + user + "]");
				}
			}

			Grupo g = null;
			if (grupo != null && grupo > 0) {
				g = grupoService.get(grupo);
				if (g == null) {
					return new MensagemResult("NOK", "Grupo não encontrado com o id [" + grupo + "]");
				}
			}

			Usuario u = null;
			if (usuario != null && usuario > 0) {
				u = usuarioService.get(usuario);
				if (u == null) {
					return new MensagemResult("NOK", "Usuario não encontrado com o id [" + usuario + "]");
				}
			}
			
			// TODO CHAT GRUPO - refazer este ws
			
			/*GrupoMensagemUsuarios gu = null;
			gu = grupoMensagemUsuariosService.findByGrupoEUsuario(g, admin);
			if (gu == null) {
				return new MensagemResult("NOK", "Usuario [" + admin + "] não possui permissão para acessar o grupo");
			} else if (!gu.isAdminGrupo()) {
				return new MensagemResult("NOK", "Usuario [" + admin + "] não possui permissão para conceder admins ao grupo");
			}
			
			gu = null;
			gu = grupoMensagemUsuariosService.findByGrupoEUsuario(g, u);
			if (gu == null) {
				gu = new GrupoMensagemUsuarios();
				gu.setGrupo(g);
				gu.setUsuario(u);
				gu.setPostar(true);
			}
			
			if (action.equals("adicionar")) {
				try {
					gu.setAdminGrupo(true);
					grupoMensagemUsuariosService.saveOrUpdate(gu);
					return new MensagemResult("OK","Usuario [" + gu.getUsuario().getNome() +"] adicionado como administrador ao grupo de mensagens");
				} catch (Exception e) {
					return new MensagemResult("NOK", "Não foi possivel adicionar o administrador ao grupo [" + g.getNome() + "]. " + e);
				}
			} else if (action.equals("remover")) {
				try {
					gu.setAdminGrupo(false);
					grupoMensagemUsuariosService.saveOrUpdate(gu);
					return new MensagemResult("OK","Usuario [" + gu.getUsuario().getNome() +"] removido como administrador do grupo de mensagens");
				} catch (Exception e) {
					return new MensagemResult("NOK", "Não foi possivel remover o administrador do grupo [" + g.getNome() + "]. " + e);
				}
			} else {
				return new MensagemResult("NOK", "Solicitação [" + mode + "] não reconhecida");
			}*/
			
			return new MensagemResult("NOK", "Refazer este ws");
		}

		return new MensagemResult("NOK", "Preencha o formulario");

	}

	@Override
	protected void xstream(XStream x) {
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
