package br.livetouch.livecom.domain.service;

import java.util.List;

import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.vo.RelatorioEnvioPostVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import net.livetouch.tiger.ddd.DomainException;

public interface LogAuditoriaService extends br.livetouch.livecom.domain.service.Service<LogAuditoria> {
	
	public void saveOrUpdate(LogAuditoria audi);

	public List<LogAuditoria> reportAuditoria(RelatorioFiltro filtro) throws DomainException;

	public List<RelatorioEnvioPostVO> envioPosts(RelatorioFiltro filtro) throws DomainException;
	public Long countEnvioPosts(RelatorioFiltro filtro) throws DomainException;

	public String csvEnvioPostReport(RelatorioFiltro filtro) throws DomainException;

	public List<LogAuditoria> getAuditoriasByEntity(Long id, AuditoriaEntidade entidade);

}
