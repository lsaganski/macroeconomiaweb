package br.livetouch.livecom.web.pages.report.push;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboTipoPush;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
import net.livetouch.extras.util.DateUtils;
import net.sf.click.control.Form;

@Controller
@Scope("prototype")
public class GeralPushReportPage extends RelatorioPage {

	public Form form = new Form();
	
	public RelatorioFiltro filtro;
	public String empresa;
	public String url;
	public int page;
	
	public ComboTipoPush comboTipo;
	
	@Override
	public void onInit() {
		super.onInit();
		
		form();
		
		empresa = ParametrosMap.getInstance(getEmpresa()).get(Params.PUSH_SERVER_PROJECT, "");
		url = ParametrosMap.getInstance(getEmpresa()).get(Params.PUSH_SERVER_HOST, "");

		if (clear) {
			getContext().removeSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			filtro = new RelatorioFiltro();
			filtro.setDataIni(DateUtils.toString(br.livetouch.livecom.utils.DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
			filtro.setDataFim(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		} else {
			filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			if (filtro != null) {
				if (filtro.getPostId() != null) {
					Post post = postService.get(filtro.getPostId());
					if (post != null) {
						filtro.setPost(post);
					}
				}
			}
		}
		
	}
	
	public void form() {
		
		form.add(comboTipo = new ComboTipoPush());
		
	}
}
