package br.livetouch.livecom.web.pages.cadastro;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboEstado;
import br.livetouch.livecom.domain.Cidade;
import net.livetouch.click.control.link.Link;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LinkDecorator;
import net.sf.click.extras.control.LongField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class CidadesPage extends CadastrosPage {
	public boolean escondeChat = true;
	
	public PaginacaoTable table = new PaginacaoTable();
	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public Cidade cidade;

	public List<Cidade> cidades;
	
	public ComboEstado comboEstado = new ComboEstado("estado");
	public ComboEstado comboEstado2 = new ComboEstado("estado2");
	
	@Override
	public void onInit() {
		super.onInit();

		form();
		
		table();
	}
	
	private void table() {
		Column c = new Column("id",getMessage("codigo.label"));
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("nome", getMessage("nome.label"));
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes",getMessage("coluna.detalhes.label"));
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	public void form(){
//		form.add(new IdField());
		LongField lId = new LongField("id");
		form.add(lId);

		comboEstado.addStyleClass("form-control inpupt-sm");
		
		
		TextField tNome = new TextField("nome", getMessage("nome.label"));
		tNome.setAttribute("autocomplete", "off");
		tNome.setAttribute("rel", "Adicionar cidade");
		tNome.setAttribute("class", "placeholder");
		form.add(tNome);
		form.add(comboEstado);
		
		comboEstado.setId("estados");
		
		form.add(comboEstado2);
		
		if (id != null) {
			cidade = cidadeService.get(id);
		} else {
			cidade = new Cidade();
		}
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip botao export");
		form.add(tExportar);
		
		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);
		
		Submit cancelar = new Submit("novo", getMessage("novo.label"), this,"novo");
		cancelar.setAttribute("class", "botao btSalvar");
		form.add(cancelar);

		form.copyFrom(cidade);
		
//		setFormTextWidth(form);
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				cidade = id != null ? cidadeService.get(id) : new Cidade();
				form.copyTo(cidade);
				
				cidade.setEstado(comboEstado.getEstado());
				cidadeService.saveOrUpdate(cidade);

				setMessageSesssion(getMessage("msg.cidade.salvar.sucess", cidade.getEstado() + " - " + cidade.getNome()), getMessage("msg.cidade.salvar.label.sucess"));
				setRedirect(CidadesPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(CidadesPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		cidade = cidadeService.get(id);
		form.copyFrom(cidade);
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Cidade c = cidadeService.get(id);
			if(id.equals(1L) && "livecom".equalsIgnoreCase(c.getNome())) {
				throw new DomainException(getMessage("msg.cidade.livecom.excluir.error"));
			}
			cidadeService.delete(c);
			setRedirect(CidadesPage.class);
			setMessageSesssion(getMessage("msg.cidade.excluir.sucess"), getMessage("msg.cidade.excluir.label.sucess"));
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.cidade.excluir.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		cidades = cidadeService.findAll();

		// Count(*)
		int pageSize = 100;
		long count = cidades.size();

		createPaginator(page, count, pageSize);

		table.setPageSize(pageSize);
		table.setRowList(cidades);
	}
	public boolean exportar(){
		String csv = cidadeService.csvCidade();
		download(csv, "cidades.csv");
		return true;
	}
}
