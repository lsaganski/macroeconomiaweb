package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.enums.TipoTemplate;
import br.livetouch.livecom.domain.repository.TemplateEmailRepository;
import br.livetouch.livecom.domain.service.TemplateEmailService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class TemplateEmailServiceImpl implements TemplateEmailService {
	@Autowired
	private TemplateEmailRepository rep;

	@Override
	public TemplateEmail get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(TemplateEmail t, Empresa e) throws DomainException {
		t.setEmpresa(e);
		rep.saveOrUpdate(t);
	}

	@Override
	public List<TemplateEmail> findAll(Empresa e) {
		return rep.findAll(e);
	}

	@Override
	public void delete(TemplateEmail c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public TemplateEmail findByType(Empresa empresa, TipoTemplate tipo) {
		return rep.findByType(empresa, tipo);
	}
	
	@Override
	public List<TemplateEmail> findByTipo(Empresa empresa, TipoTemplate tipo) {
		return rep.findByTipo(empresa, tipo);
	}


	@Override
	public TemplateEmail existe(Empresa empresa, TemplateEmail templateEmail) {
		return rep.existe(empresa, templateEmail);
	}
}
