package br.livetouch.livecom.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Perfil extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "PERFIL_SEQ")
	private Long id;

	private String nome;

	private String descricao;

	private boolean publicar;
	private boolean excluirPostagem;
	private boolean editarPostagem;
	private boolean comentar;
	private boolean visualizarComentario;
	private boolean visualizarConteudo;
	private boolean curtir;
	private boolean enviarMensagem;
	private boolean arquivos;
	private boolean cadastrarUsuarios;
	private boolean codigo;
	
	private boolean padrao;
	private boolean cadastrarTabelas;
	private boolean visualizarRelatorio;
	
	@OneToMany(mappedBy = "permissao", fetch=FetchType.LAZY)
	@XStreamOmitField
	private Set<Usuario> usuarios;
	
	@OneToMany(mappedBy = "perfil", fetch = FetchType.LAZY)
	@XStreamOmitField
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<PerfilPermissao> permissoes;
	
	@OneToMany(mappedBy = "perfil", fetch = FetchType.LAZY)
	private Set<MenuPerfil> menus;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String desc) {
		this.descricao = desc;
	}

	public boolean isPublicar() {
		return publicar;
	}

	public void setPublicar(boolean publicar) {
		this.publicar = publicar;
	}

	public boolean isComentar() {
		return comentar;
	}

	public void setComentar(boolean comentar) {
		this.comentar = comentar;
	}

	public boolean isCurtir() {
		return curtir;
	}

	public void setCurtir(boolean curtir) {
		this.curtir = curtir;
	}

	public boolean isEnviarMensagem() {
		return enviarMensagem;
	}

	public void setEnviarMensagem(boolean enviarMensagem) {
		this.enviarMensagem = enviarMensagem;
	}
	
	public Set<Usuario> getUsuarios() {
		return usuarios;
	}
	
	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	@Override
	public String toString() {
		return "Permissao [id=" + id + ", padrao=" + padrao + ", nome=" + nome + ", descricao=" + descricao + ", publicar=" + publicar + ", excluirPostagem=" + excluirPostagem + ", comentar=" + comentar + ", curtir=" + curtir + ", enviarMensagem=" + enviarMensagem + ", cadastrarUsuarios=" + cadastrarUsuarios
				+ "]";
	}

	public boolean isExcluirPostagem() {
		return excluirPostagem;
	}

	public void setExcluirPostagem(boolean excluirPostagem) {
		this.excluirPostagem = excluirPostagem;
	}

	public boolean isCadastrarUsuarios() {
		return cadastrarUsuarios;
	}

	public void setCadastrarUsuarios(boolean cadastrarUsuarios) {
		this.cadastrarUsuarios = cadastrarUsuarios;
	}

	public void setEditarPostagem(boolean editarPostagem) {
		this.editarPostagem = editarPostagem;
	}
	public boolean isEditarPostagem() {
		return editarPostagem;
	}

	public boolean isArquivos() {
		return arquivos;
	}
	public void setArquivos(boolean arquivos) {
		this.arquivos = arquivos;
	}

	public void setPadrao(boolean padrao) {
		this.padrao = padrao;
	}

	public boolean isPadrao() {
		return padrao;
	}

	public String getNomeDescricao() {
		return nome + ": "+ descricao;
	}
	
	public boolean isCadastrarTabelas() {
		return cadastrarTabelas;
	}
	
	public void setCadastrarTabelas(boolean cadastrarTabelas) {
		this.cadastrarTabelas = cadastrarTabelas;
	}
	
	public boolean isAdmin() {
		return StringUtils.startsWithIgnoreCase(nome, "admin");
	}

	public boolean isRoot() {
		return StringUtils.startsWithIgnoreCase(nome, "root");
	}

	public boolean isCodigo() {
		return codigo;
	}

	public void setCodigo(boolean codigo) {
		this.codigo = codigo;
	}

	public boolean isVisualizarComentario() {
		return visualizarComentario;
	}

	public void setVisualizarComentario(boolean visualizarComentario) {
		this.visualizarComentario = visualizarComentario;
	}

	public boolean isVisualizarRelatorio() {
		return visualizarRelatorio;
	}

	public void setVisualizarRelatorio(boolean visualizarRelatorio) {
		this.visualizarRelatorio = visualizarRelatorio;
	}

	public Set<PerfilPermissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Set<PerfilPermissao> permissoes) {
		this.permissoes = permissoes;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Set<MenuPerfil> getMenus() {
		return menus;
	}

	public void setMenus(Set<MenuPerfil> menus) {
		this.menus = menus;
	}

	public boolean isVisualizarConteudo() {
		return visualizarConteudo;
	}

	public void setVisualizarConteudo(boolean visualizarConteudo) {
		this.visualizarConteudo = visualizarConteudo;
	}
	
}
