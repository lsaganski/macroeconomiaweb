package br.livetouch.livecom.web.pages.admin;


import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboCategoriaPost;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Chapeu;
import br.livetouch.livecom.domain.service.CategoriaPostService;
import br.livetouch.livecom.domain.service.ChapeuService;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class ChapeusPage extends LivecomAdminPage {

	@Autowired
	ChapeuService chapeuService;

	@Autowired
	CategoriaPostService categoriaPostService;

	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this, "editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public boolean escondeChat = true;

	public int page;

	public Chapeu chapeu;

	public List<Chapeu> chapeus;

	private ComboCategoriaPost comboCategoria;

	@Override
	public void onInit() {
		super.onInit();

		form();
	}

	public void form() {
		form.add(new IdField());

		TextField tNome = new TextField("nome", getMessage("nome.label"));
		tNome.setAttribute("autocomplete", "off");
		tNome.setAttribute("rel", "Adicionar categoria");
		tNome.setAttribute("class", "mascara");
		form.add(tNome);

		form.add(comboCategoria = new ComboCategoriaPost(categoriaPostService, getEmpresa()));

		if (id != null) {
			chapeu = chapeuService.get(id);
		} else {
			chapeu = new Chapeu();
		}

		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);

		form.add(new Submit("novo", getMessage("novo.label"), this, "novo"));

		form.copyFrom(chapeu);

		// setFormTextWidth(form);

	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				chapeu = id != null ? chapeuService.get(id) : new Chapeu();
				form.copyTo(chapeu);

				CategoriaPost categ = comboCategoria.getCategoriaPost();
				if (categ != null) {
					chapeu.setCategoria(categ);
				}

				chapeuService.saveOrUpdate(chapeu);

				setMessageSesssion(getMessage("msg.chapeu.salvar.sucess", chapeu.getNome()), getMessage("msg.chapeu.salvar.label.sucess"));

				setRedirect(ChapeusPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(), e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		chapeu = chapeuService.get(id);
		form.copyFrom(chapeu);

		return true;
	}

	public boolean deletar() throws SQLIntegrityConstraintViolationException {
		try {
			Long id = deleteLink.getValueLong();
			Chapeu chapeu = chapeuService.get(id);
			chapeuService.delete(chapeu);
			setRedirect(ChapeusPage.class);
			setMessageSesssion(getMessage("msg.chapeu.excluir.sucess", chapeu.getNome()), getMessage("msg.chapeu.excluir.label.sucess"));
		} catch (DomainException e) {
			logError(e.getMessage(), e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(), e);
			this.msgErro = getMessage("msg.chapeu.excluir.error");
		} catch (Exception e) {
			logError(e.getMessage(), e);
			this.msgErro = getMessage("msg.chapeu.excluir.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		// categorias = categoriaPostService.findAll();
		chapeus = chapeuService.findAll();

		// Count(*)
		int pageSize = 10;
		long count = chapeus.size();

		createPaginator(page, count, pageSize);
	}
}
