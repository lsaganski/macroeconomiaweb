package br.livetouch.livecom.sender;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.services.kinesis.model.InvalidArgumentException;

import br.infra.util.Log;
import br.infra.util.VelocityUtil;
import br.livetouch.livecom.domain.EmailReport;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoTemplate;
import br.livetouch.livecom.domain.service.EmailReportService;
import br.livetouch.livecom.domain.service.TemplateEmailService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.email.EmailFactory;
import br.livetouch.livecom.email.EmailManager;
import br.livetouch.livecom.email.EmailUtils;
import br.livetouch.pushserver.lib.PushNotification;
import br.livetouch.pushserver.lib.PushService;
import br.livetouch.pushserver.lib.UsuarioToPush;
import net.livetouch.tiger.ddd.DomainException;

/**
 * Classe responsável por realizar envio de mensagens
 * para outras plataformas como 
 * <ul>
 * <li>(SMTP) Envio de e-mail</li>
 * <li>(PUSH) notificações para mobile</li>
 * </ul>
 * @author Juillian
 *
 */
public abstract class Sender {
	protected static Logger log = Log.getLogger(EmailUtils.class);
	
	@Autowired
	protected EmailReportService emailReportService;
	
	@Autowired
	protected TemplateEmailService templateEmailService;
	
	@Autowired
	protected UsuarioService usuarioService;

	/**
	 * Assunto da mensagem
	 */
	protected String subject;

	/**
	 * Empresa que está enviando o e-mail
	 */
	private Empresa empresa;
	
	/**
	 * Define o tipo de template que sera usado
	 * caso não seja setado nenhum template na notificação
	 * @return
	 */
	protected abstract TipoTemplate getTipoTemplate();
	
	/**
	 * Define o nome do arquivo do template, caso não setado nenhum template na notificação
	 * exe: email_recuperar_senha.htm
	 * @return
	 */
	protected abstract String getNameFileTemplate();
	
	/**
	 * Enviar uma notificação para o push ou pelo SMTP padrão
	 * dependendo da configuração do cliente
	 * 
	 * @param empresa
	 * @param not
	 */
	public void send(Empresa empresa, PushNotification not) {
		this.empresa = empresa;
		
		if(not == null) {
			throw new InvalidArgumentException("A notificação não pode ser nula");
		}
		
		String template = not.getTemplate();
		boolean isSendMail = not.isSendMail();
		boolean isSendPush = not.isSendPush();
		
		if(StringUtils.isEmpty(not.getTitulo())) {
			not.setTitulo(getSubject());
		}
		
		if(isSendPush) {
			log.debug("Ainda não foi implementado o envio de push pelo sender");
			return;
		}
		
		if(isSendMail) {
			template = template != null ? template : getTemplate(empresa, getTipoTemplate());
			
			if(StringUtils.isEmpty(template)) {
				template = getDefaultTemplate();
			}
			not.setTemplate(template);
			
			String from = not.getFrom() != null ? not.getFrom() : getEmailAdmin();
			not.setFrom(from);
		}
		
		/**
		 * Caso necessário todas as classes podem implementar esta factory
		 * assim decidindo quem envia pelo push ou outra plataforma de envio
		 * 
		 */
		String factory = getParamMap(Params.EMAIL_FACTORY, "smtp");
		if("push".equals(factory)) {
			sendPush(empresa, not);
		} else {
			sendSMTP(empresa, not, null);
		}
	}
	
	/**
	 * Envia uma notificação por e-mail ou push
	 * @param empresa
	 * @param not
	 * @param idTemplate
	 */
	public void send(Empresa empresa, PushNotification not, Long idTemplate) {
		this.empresa = empresa;
		TemplateEmail template = templateEmailService.get(idTemplate);
		send(empresa, not, template);
	}
	
	/**
	 * Envia uma notificação por e-mail ou push
	 * @param empresa
	 * @param not
	 * @param template
	 */
	public void send(Empresa empresa, PushNotification not, TemplateEmail template) {
		this.empresa = empresa;
		
		String body = template != null ? template.getHtml() : null;
		not.setTemplate(body);
		send(empresa, not);
	}
	
	/**
	 * Envia uma notificação por e-mail ou push
	 * @param empresa
	 * @param not
	 */
	private void sendPush(Empresa empresa, PushNotification not) {
		this.empresa = empresa;
		
		not.setProjeto(getProjetoPush());

		PushService service = getPushService();
		service.sendNotification(not);
		List<UsuarioToPush> usuarios = not.getUsuarios();
		for (UsuarioToPush u : usuarios) {
			String template = not.getTemplate();
			Map<String, Object> params = u.getParams();
			if(params != null && params.size() > 0) {
				template = VelocityUtil.fromString(template, params);
			}
			report(empresa, not, u, true,template, null);
		}
		
	}

	/**
	 * Envia uma notificação por e-mail ou push
	 * @param empresa
	 * @param not
	 */
	private void sendSMTP(Empresa empresa, PushNotification not, String[] bcc) {
		log.debug("----- sendSMTP ------ ");
		List<UsuarioToPush> usuarios = not.getUsuarios();
		for (UsuarioToPush usuario : usuarios) {
			String template = not.getTemplate();
			Map<String, Object> params = usuario.getParams();
			log.debug("Enviando para o " + usuario.codigo + " com os parametros " + params);
			if(params != null && params.size() > 0) {
				template = VelocityUtil.fromString(template, params);
			}
			try {
				EmailManager manager =  EmailFactory.getEmailManager(empresa);
				log.debug("Enviando e-mail para: [" + usuario.getNome() + "]");
				boolean isSend;
				String from = ParametrosMap.getInstance().get(Params.EMAIL_CONTATO_LIVETOUCH, "contato@livetouch.com.br");
				if(bcc != null){
					isSend = manager.sendEmail(from, new String[]{usuario.getEmail()}, not.getTitulo(), template, true, bcc);
					
				}else{
					isSend = manager.sendEmail(from, usuario.getEmail(), not.getTitulo(), template, true);
				}
				log.debug("E-mail enviado com sucesso para: [" + usuario.getNome() + "]");
				report(empresa, not, usuario, isSend, template, null);
			} catch (IOException | EmailException e) {
				report(empresa, not, usuario, false, template, e);
				
				
				
				log.error(">>> Falha no envio de E-mail " + e.getMessage(), e);
			}
		}
	}
	
	/**
	 * Pega o template default definido por arquivo
	 * @return
	 */
	protected String getDefaultTemplate() {
		String fileName = getNameFileTemplate();
		if(StringUtils.isNotEmpty(fileName)) {
			URL url = this.getClass().getResource("/templates/" + fileName);
			if(url != null) {
				try {
					File file = new File(url.toURI());
					String template = FileUtils.readFileToString(file, "UTF-8");
					return template;
				} catch (URISyntaxException | IOException e) {
					log.debug("ERROR: ao  criar o arquivo do template: [" + fileName + "], MESSAGE: " + e.getMessage(), e);
				}
			} else {
				log.debug("Template [" + fileName + "] não encontrado na pasta template do classpath");
			}
		}
		return null;
	}
	
	/**
	 * Retorna o conteudo de um template pela sua empresa e tipo
	 * 
	 * @param empresa
	 * @param tipoTemplate
	 * @return
	 */
	protected String getTemplate(Empresa empresa, TipoTemplate tipoTemplate) {
		TemplateEmail template = templateEmailService.findByType(empresa, tipoTemplate);
		if(template != null) {
			return template.getHtml();
		}
		return null;
	}

	/**
	 * Retorna o nome do projeto da empresa cadastrado no push
	 * @return
	 */
	private String getProjetoPush() {
		String projeto = getParamMap(Params.PUSH_SERVER_PROJECT, "Livecom");
		return projeto;
	}
	
	/**
	 * Retorna o e-mail do admin da empresa que será o remetente
	 * @return
	 */
	protected String getEmailAdmin() {
		return getParamMap(Params.EMAIL_SMTP_USER,"admin@livetouch.com.br");
	}
	
	/**
	 * Seta Titulo ou assunto do e-mail
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	/**
	 * Return o assunto ou titutlo do e-mail
	 * @return
	 */
	protected String getSubject() {
		return "Novo comunicado";
	}
	
	/**
	 * Loga a transação do envio de e-mail
	 * @param empresa
	 * @param not
	 * @param u
	 * @param status
	 * @param template
	 * @param e
	 */
	private void report(Empresa empresa, PushNotification not, UsuarioToPush u, Boolean status, String template, Exception e) {
		Boolean reportOn = getParamMap(Params.EMAIL_REPORT_ON, true);
		log.debug("EMAIL_REPORT_ON:" + reportOn);
		if(reportOn) {
			Long idLivecom = u.getIdLivecom();
			Usuario usuario = usuarioService.get(idLivecom);
			if(usuario != null) {
				log.debug("salvando e-mail report para " + u.getNome());
				String error = e != null ? "Falha no envio do e-mail: " + e.getMessage() : null;
				EmailReport emailReport = new EmailReport();
				emailReport.setData(new Date());
				emailReport.setStatus(status);
				emailReport.setEmpresa(empresa);
				emailReport.setUsuario(usuario);
				emailReport.setErro(error);
				emailReport.setMsg(template);
				emailReport.setAssunto(not.getTitulo());
				emailReport.setDe(not.getFrom());
				emailReport.setTipoTemplate(getTipoTemplate());
				if(e != null) {
					emailReport.setErro(error);
				}
				try {
					emailReportService.saveOrUpdate(emailReport);
					log.debug("E-mail report salvo com sucesso");
				} catch (DomainException e1) {
					log.error("Erro ao salvar e-mail report " + error, e);
				}
			}
		} else {
			log.debug("EMAIL REPORT NÂO ATIVADO");
		}
	}
	
	protected String getParamMap(String key, String defaultValue) {
		ParametrosMap param = ParametrosMap.getInstance(empresa);
		defaultValue = param.get(key, defaultValue);
		return defaultValue;
	}
	
	protected Boolean getParamMap(String key, Boolean defaultValue) {
		ParametrosMap param = ParametrosMap.getInstance(empresa);
		defaultValue = param.getBoolean(key, defaultValue);
		return defaultValue;
	}
	
	private PushService getPushService() {
		ParametrosMap params = ParametrosMap.getInstance(empresa);
		PushService pushService = Livecom.getPushService(params);
		return pushService;
	}

	public void sendTrial(Empresa empresa, PushNotification not) {
		this.empresa = empresa;
		
		if(not == null) {
			throw new InvalidArgumentException("A notificação não pode ser nula");
		}
		
		String template = not.getTemplate();
		boolean isSendMail = not.isSendMail();
		boolean isSendPush = not.isSendPush();
		
		if(StringUtils.isEmpty(not.getTitulo())) {
			not.setTitulo(getSubject());
		}
		
		if(isSendPush) {
			log.debug("Ainda não foi implementado o envio de push pelo sender");
			return;
		}
		
		if(isSendMail) {
			template = template != null ? template : getTemplate(empresa, getTipoTemplate());
			
			if(StringUtils.isEmpty(template)) {
				template = getDefaultTemplate();
			}
			not.setTemplate(template);
			
			String from = not.getFrom() != null ? not.getFrom() : getEmailAdmin();
			not.setFrom(from);
		}
		
		/**
		 * Caso necessário todas as classes podem implementar esta factory
		 * assim decidindo quem envia pelo push ou outra plataforma de envio
		 * 
		 */
		String factory = getParamMap(Params.EMAIL_FACTORY, "smtp");
		if("push".equals(factory)) {
			sendPush(empresa, not);
		} else {
			String bccTrial = ParametrosMap.getInstance().get(Params.EMAIL_BCC_TRIAL, "");
			String[] bcc = StringUtils.isNotEmpty(bccTrial) ? bccTrial.split(",") : null;
			sendSMTP(empresa, not, bcc);
		}
	}
}
