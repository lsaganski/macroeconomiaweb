package br.livetouch.livecom.domain.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.StatusMensagemUsuario;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.LivecomDateUtils;

public class StatusMensagemUsuarioVO {

	public boolean lida;
	public String dataLida;
	public String dataLidaString;
	
	public boolean entregue;
	public String dataEntregue;
	public String dataEntregueString;
	
	public Long userId;
	public String userLogin;
	public String userNome;
	public String userUrlFoto;
	
	
	/**
	 * Construtor para Hibernate.
	 * 
	 * 
	 * @param status
	 */
	public StatusMensagemUsuarioVO(StatusMensagemUsuario status) {
		this.lida = status.isLida();
		this.dataLida = LivecomDateUtils.getDateString(status.getDataLida());
		this.dataLidaString = LivecomDateUtils.getDataStringHojeOntem(status.getDataLida());
		
		this.entregue = status.isEntregue();
		this.dataEntregue = LivecomDateUtils.getDateString(status.getDataEntregue());
		this.dataEntregueString = LivecomDateUtils.getDataStringHojeOntem(status.getDataEntregue());
		
		this.userId = status.getUsuario().getId();
		this.userLogin = status.getUsuario().getLogin();
		this.userNome = status.getUsuario().getNome();
		this.userUrlFoto = status.getUsuario().getUrlThumb();
	}

	public StatusMensagemUsuarioVO(Mensagem mensagem) {
		this.lida = mensagem.isLida();
		this.dataLida = LivecomDateUtils.getDateString(mensagem.getDataLida());
		this.dataLidaString = LivecomDateUtils.getDataStringHojeOntem(mensagem.getDataLida());
		
		this.entregue = mensagem.isEntregue();
		this.dataEntregue = LivecomDateUtils.getDateString(mensagem.getDataEntregue());
		this.dataEntregueString = LivecomDateUtils.getDataStringHojeOntem(mensagem.getDataEntregue());
		
		this.userId = mensagem.getTo().getId();
		this.userLogin = mensagem.getTo().getLogin();
		this.userNome = mensagem.getTo().getNome();
		this.userUrlFoto = mensagem.getTo().getUrlThumb();
	}

	public boolean isLida() {
		return lida;
	}

	public void setLida(boolean lida) {
		this.lida = lida;
	}

	public String getDataLida() {
		return dataLida;
	}

	public void setDataLida(String dataLida) {
		this.dataLida = dataLida;
	}

	public String getDataLidaString() {
		return dataLidaString;
	}

	public void setDataLidaString(String dataLidaString) {
		this.dataLidaString = dataLidaString;
	}

	public boolean isEntregue() {
		return entregue;
	}

	public void setEntregue(boolean entregue) {
		this.entregue = entregue;
	}

	public String getDataEntregue() {
		return dataEntregue;
	}

	public void setDataEntregue(String dataEntregue) {
		this.dataEntregue = dataEntregue;
	}

	public String getDataEntregueString() {
		return dataEntregueString;
	}

	public void setDataEntregueString(String dataEntregueString) {
		this.dataEntregueString = dataEntregueString;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	public String getUserNome() {
		return userNome;
	}

	public void setUserNome(String userNome) {
		this.userNome = userNome;
	}

	public String getUserUrlFoto() {
		return userUrlFoto;
	}

	public void setUserUrlFoto(String userUrlFoto) {
		this.userUrlFoto = userUrlFoto;
	}

	public static List<StatusMensagemUsuarioVO> toList(Set<StatusMensagemUsuario> statusUsuarios, Usuario user) {
		List<StatusMensagemUsuarioVO> listVO = new ArrayList<StatusMensagemUsuarioVO>();
		
		if(statusUsuarios != null && statusUsuarios.size() > 0) {
			for (StatusMensagemUsuario status : statusUsuarios) {
				if(!status.getUsuario().equals(user)) {
					StatusMensagemUsuarioVO vo = new StatusMensagemUsuarioVO(status);
					listVO.add(vo);
				}
			}
		}

		return listVO;
	}
	
}
