package br.livetouch.livecom.domain.vo;

import java.util.List;

import javax.ws.rs.QueryParam;

public class Filtro {
	@QueryParam("nome")
	public String nome;
	@QueryParam("codigo")
	public String codigo;
	@QueryParam("page")
	public Integer page;
	@QueryParam("max")
	public Integer max;
	
	@QueryParam("not_ids")
	public String notIds;
	
	@QueryParam("dir_exec")
	public String dirExecId;
	
	public boolean executiva;
	public boolean diretoriaNaoExecutiva;
	public List<Long> ids;
}
