package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.ConversaNotification;
import net.livetouch.tiger.ddd.DomainException;

public interface ConversaNotificationService extends Service<ConversaNotification> {

	List<ConversaNotification> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(ConversaNotification c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(ConversaNotification c) throws DomainException;

	ConversaNotification findByConversationUser(Long conversationId, Long userId);

}
