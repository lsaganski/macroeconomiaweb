package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Chapeu;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.vo.ChapeuFilter;

@Repository
public interface ChapeuRepository extends net.livetouch.tiger.ddd.repository.Repository<Chapeu> {

	List<Chapeu> findAllByFilter(ChapeuFilter filter);

	Chapeu findByName(Chapeu chapeu, Empresa empresa);
	
}