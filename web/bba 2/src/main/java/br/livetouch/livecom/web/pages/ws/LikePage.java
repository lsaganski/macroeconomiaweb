package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Likes;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.vo.LikeVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;
import net.sf.click.extras.control.NumberField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class LikePage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private LongField tUserId;
	private LongField tPostId;
	private NumberField tFavorito;
	private TextField tMode;
	private LongField tComentarioId;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tUserId = new LongField("user_id", true));
		form.add(tPostId = new LongField("post_id"));
		form.add(tComentarioId = new LongField("comentario_id"));
		form.add(tFavorito = new NumberField("favorito", "favorito 0 ou 1"));

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));

		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		tFavorito.setValue("1");

		tUserId.setFocus(true);

		form.add(new Submit("enviar"));
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Long userId = tUserId.getLong();
			Long postId = tPostId.getLong();
			Long comentarioId = tComentarioId.getLong();
			boolean liked = "1".equals(tFavorito.getValue());

			Usuario u = usuarioService.get(userId);
			if (u == null) {
				return new MensagemResult("ERROR", "Usuário inválido.");
			}

			if ((postId == null || postId <= 0) && (comentarioId == null || comentarioId <= 0)) {
				return new MensagemResult("ERROR", "Informe o post ou comentario.");
			}

			Post post = null;
			Comentario comentario = null;

			if (postId != null && postId > 0) {
				post = postService.get(postId);
				if (post == null) {
					return new MensagemResult("ERROR", "Post inválido.");
				}
			}
			if (comentarioId != null && comentarioId > 0) {
				comentario = comentarioService.get(comentarioId);
				if (comentario == null) {
					return new MensagemResult("ERROR", "Comentário inválido.");
				}
				post = comentario.getPost();
			}

			try {
				Likes l = likeService.like(u, post, comentario, liked);

				// JSON
				LikeVO fav = new LikeVO();
				fav.setLikes(l);
				
				if(liked) {
					notificationService.markPostAsRead(post, u);
				}

//				NotificationBadge badge = notificationService.findBadges(post.getUsuario());
//				WebSocket.sendMsg(post.getUsuario().getId(), badge);

				return fav;
			} catch (DomainMessageException e) {
				return new MensagemResult("ERROR", e.getMessage());
			}
		}

		return new MensagemResult("NOK", "Erro ao fazer like do post.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("like", LikeVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
