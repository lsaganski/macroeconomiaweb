package br.livetouch.livecom.domain.vo;

import java.io.Serializable;


/**
 * Value Object
 * 
 * @author Ricardo Lecheta
 *
 */
public class HelloVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String nome;
	private String cidade;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
