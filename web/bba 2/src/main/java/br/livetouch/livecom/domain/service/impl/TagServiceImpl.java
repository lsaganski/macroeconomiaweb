package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.repository.TagRepository;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.TagService;
import br.livetouch.livecom.domain.vo.TagFilter;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class TagServiceImpl extends LivecomService<Tag> implements TagService {
	protected static final Logger log = Log.getLogger(TagService.class);
	
	@Autowired
	private TagRepository rep;
	
	@Autowired
	private PostService postService;
	
	@Autowired
	private ArquivoService arquivoService;

	@Override
	public Tag get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Usuario userInfo,Tag t) throws DomainException {
		boolean insert = t.getId() == null;
		t.setEmpresa(userInfo.getEmpresa());
		if(insert) {
			boolean existe = rep.exists(t.getNome(), userInfo.getEmpresa());
			if (existe) {
				throw new DomainMessageException("validate.tag.ja.existe");
			}
		}
		
		if(t.getDateCreate() == null) {
			t.setDateCreate(new Date());
			t.setUserCreate(userInfo);
		}
		
		t.setDateUpdate(new Date());
		t.setUserUpdate(userInfo);

		rep.saveOrUpdate(t);
		
		auditSave(userInfo, t, insert);
	}

	protected void auditSave(Usuario userInfo, Tag t, boolean insert) {
		AuditoriaAcao acao = insert ? AuditoriaAcao.INSERIR : AuditoriaAcao.EDITAR;
		String msg = null;
		if(insert) {
			msg = "Tag " + t.getNome() + " cadastrada";
		} else {
			msg = "Tag " + t.getNome() + " editada";
		}
		
		LogAuditoria.log(userInfo, t, AuditoriaEntidade.TAG, acao, msg);
	}

	@Override
	public List<Tag> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	/**
<pre>

(1) Regras:

(r1) A tag não pode ser excluída se estiver associada com Posts.
(r2) A tag não pode ser excluída se estiver associada com Arquivos.

(2) Cascade ao excluir:

(c1) Remove associação com Posts e Arquivos.

</pre>
	 */
	@Override
	public void delete(Usuario userInfo,Tag tag, boolean force) throws DomainException {
		
		boolean r1 = ParametrosMap.getInstance(tag.getEmpresa()).getBoolean(Params.DELETAR_TAG_R1_ON,false);
		boolean r2 = ParametrosMap.getInstance(tag.getEmpresa()).getBoolean(Params.DELETAR_TAG_R2_ON,false);
		boolean c1 = ParametrosMap.getInstance(tag.getEmpresa()).getBoolean(Params.DELETAR_TAG_C1_ON,false);
		
		if(!force) {
			if(r1) {
				List<Post> posts = postService.findPostsByTag(tag);
				if(posts != null && posts.size() > 0) {
					throw new DomainException("Esta tag está vinculada a posts e não pode ser excluída.");
				}
			}
			
			if(r2) {
				List<Arquivo> arquivos = arquivoService.findAllByTag(tag);
				if(arquivos != null && arquivos.size() > 0) {
					throw new DomainException("Esta tag está vinculada a arquivos e não pode ser excluída.");
				}
			}
		}
		
		LogAuditoria auditoria = null;
		
		try {
			// Auditoria
			if(userInfo != null) {
				String msg = "Tag " + tag.getNome() + " deletada";
				auditoria = LogAuditoria.log(userInfo, tag, AuditoriaEntidade.TAG, AuditoriaAcao.DELETAR, msg, false);
			}
			
			if(force || c1) {
				executeNative("delete from post_tags where tag_id = ?", tag.getId());
				executeNative("delete from arquivos_tags where tag_id = ?", tag.getId());
			}
			
			// Deleta tag
			rep.delete(tag);
			
			// OK
			if(auditoria != null) {
				auditoria.finish();
			}
			
		} catch (Exception e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if(root == null) {
				root = e;
			}
			log.error("Erro ao excluir a Tag [" + tag.toString() + "]: " + root.getMessage(), root);
			
			LogSistema.logError(userInfo, e);

			throw new DomainException("Não foi possível excluir a tag [" + tag.toString() + "]", e);
		} 
	}

	@Override
	public void deleteAll(Empresa empresa) {
		rep.deleteAll(empresa);
		//rep.delete("delete from Tag");
	}
	
	@Override
	public List<Tag> getTagsAndSave(String tags, Empresa empresa) {
		List<Tag> list = new ArrayList<Tag>();
		if(StringUtils.isEmpty(tags)) {
			return list;
		}
		String[] split = StringUtils.split(tags, ",");
		if(split != null) {
			for (String s : split) {
				List<Tag> find = findTagsByNome(s, empresa);
				Tag tag = (find != null && find.size() > 0) ? find.get(0) : null; 
				if(tag != null) {
					list.add(tag);
				} else {
					tag = new Tag();
					tag.setNome(s);
					tag.setEmpresa(empresa);
					rep.saveOrUpdate(tag);
					list.add(tag);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<Tag> getTags(String tags, Empresa empresa) {
		List<Tag> list = new ArrayList<Tag>();
		if(StringUtils.isEmpty(tags)) {
			return list;
		}
		String[] split = StringUtils.split(tags, ",");
		if(split != null) {
			for (String s : split) {
				List<Tag> find = findTagsByNome(s,  empresa);
				Tag tag = (find != null && find.size() > 0) ? find.get(0) : null; 
				if(tag != null) {
					list.add(tag);
				} else {
					log.error("Tag ["+s+"] não encontrada");
				}
			}
		}
		return list;
	}

	@Override
	public List<Tag> findTagsByNome(String nome, Empresa empresa) {
		return rep.findByNome(nome, empresa);
	}

	@Override
	public void saveTags(List<Tag> list, Empresa empresa) {
		if(list != null) {
			for (Tag tag : list) {
				tag.setEmpresa(empresa);
				rep.saveOrUpdate(tag);
			}
		}
	}

	@Override
	public List<Tag> findAllByFilter(TagFilter filter, Empresa empresa) {
		return rep.findAllByFilter(filter, empresa);
	}

	@Override
	public List<Tag> findAllByIds(List<Long> ids, Empresa empresa) {
		if(ids == null || ids.isEmpty()) {
			return new ArrayList<Tag>();
		}
		return rep.findAllByKeys(ids, empresa);
	}

	@Override
	public String csvTag(Empresa empresa) {
		List<Tag> findAll = findAll(empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("CODIGO").add("NOME");
		for (Tag tag : findAll) {
			body.add(tag.getCodigoDesc()).add(tag.getNome());
			body.end();
		}
		return generator.toString();
	}

	@Override
	public Tag findByName(Tag tag, Empresa empresa) {
		return rep.findByName(tag, empresa);
	}

}
