package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.DateUtils;

public class RelatorioFavoritosVO implements Serializable {

	private static final long serialVersionUID = 1524516347642576158L;
	
	private String data;
	private Long count;
	private Long countDistinct;
	private Long id;
	private String titulo;
	private Long postId;
	private String usuario;
	private Long usuarioId;
	
	public RelatorioFavoritosVO(Long count, Post post) {
		this.count = count;
		this.titulo = post.getTitulo();
		this.postId = post.getId();
		this.data = DateUtils.toString(post.getDataPublicacao(), "dd/MM/yyyy");
		
		Usuario user = post.getUsuario();
		this.usuario = user.getLogin();
		this.usuarioId = user.getId();
	}
	
	public RelatorioFavoritosVO(Date data, Post post, Usuario usuario) {
		this.data = DateUtils.toString(data, "dd/MM/yyyy HH:mm");
		this.titulo = post.getTitulo();
		this.postId = post.getId();
		this.usuario = usuario.getLogin();
		this.usuarioId = usuario.getId();
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getCountDistinct() {
		return countDistinct;
	}

	public void setCountDistinct(Long countDistinct) {
		this.countDistinct = countDistinct;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Long getPostId() {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}
	
	
}
