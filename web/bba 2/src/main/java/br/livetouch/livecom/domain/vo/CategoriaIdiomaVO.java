package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.CategoriaIdioma;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Idioma;

public class CategoriaIdiomaVO implements Serializable {

	private static final long serialVersionUID = 1L;
	public Long id;
	public String nome;

	public CategoriaVO categoria;
	public IdiomaVO idioma;
	
	public CategoriaIdiomaVO() {

	}

	public CategoriaIdiomaVO(CategoriaIdioma categoriaIdioma) {
		this.id = categoriaIdioma.getId();
		this.nome = categoriaIdioma.getNome();

		CategoriaPost c = categoriaIdioma.getCategoria();
		if (c != null) {
			this.categoria = new CategoriaVO(c);
		}
		
		Idioma i = categoriaIdioma.getIdioma();
		if (i != null) {
			this.idioma = new IdiomaVO(i);
		}

	}

	public static List<CategoriaIdiomaVO> fromList(Set<CategoriaIdioma> categoriaIdioma) {
		List<CategoriaIdiomaVO> vos = new ArrayList<>();
		if (categoriaIdioma == null) {
			return vos;
		}
		for (CategoriaIdioma ci : categoriaIdioma) {
			vos.add(new CategoriaIdiomaVO(ci));
		}
		return vos;
	}

}
