package br.livetouch.livecom.web.pages.root;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.OnOffZeroUmDecorator;
import br.livetouch.livecom.domain.Campo;
import br.livetouch.livecom.web.pages.LivecomRootPage;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LinkDecorator;

/**
 * 
 */
@Controller
@Scope("prototype")
public class CamposPage extends LivecomRootPage {

	public PaginacaoTable table = new PaginacaoTable();
	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public int page;
//	public int lastPage;

	public Campo campo;

	public List<Campo> campos;
	private Checkbox checkVisivel;
	private Checkbox checkEditavel;
	private Checkbox checkPublico;
	private Checkbox checkObrigatorio;

	@Override
	public void onInit() {
		super.onInit();

		form();
		
		table();
	}
	
	private void table() {
		Column c = new Column("id",getMessage("codigo.label"));
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("nome", getMessage("nome.label"));
		c.setHeaderStyle("text-align", "center");
		table.addColumn(c);
		
		c = new Column("visivel", getMessage("check.visivel.label"));
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		c.setDecorator(new OnOffZeroUmDecorator("visivel"));
		
		c = new Column("publico", getMessage("check.publico.label"));
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		c.setDecorator(new OnOffZeroUmDecorator("publico"));
		
		c = new Column("dependeCampo", getMessage("dependeCampo.label"));
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		c.setDecorator(new OnOffZeroUmDecorator("dependeCampo"));
		table.addColumn(c);
		
		c = new Column("dependeCampoValor", getMessage("dependeCampoValor.label"));
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		c.setDecorator(new OnOffZeroUmDecorator("dependeCampoValor"));
		table.addColumn(c);
		
		c = new Column("editavel", getMessage("check.editavel.label"));
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		c.setDecorator(new OnOffZeroUmDecorator("editavel"));
		table.addColumn(c);
		
		c = new Column("obrigatorioCadastroAdmin", getMessage("check.obrigatorio.admin.label"));
		c.setHeaderStyle("text-align", "center");
		c.setDecorator(new OnOffZeroUmDecorator("obrigatorioCadastroAdmin"));
		c.setTextAlign("center");
		table.addColumn(c);
		
		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes",getMessage("coluna.detalhes.label"));
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	public void form(){
		form.add(new IdField());

		TextField t = new TextField("nome", getMessage("nome.label"), true);
		t.setAttribute("class", "input");
		t.setAttribute("autocomplete", "off");
		form.add(t);
		
		checkVisivel = new Checkbox("visivel", getMessage("check.visivel.label"));
		form.add(checkVisivel);
		
		checkPublico = new Checkbox("publico",getMessage("check.publico.label"));
		form.add(checkPublico);
		
		t = new TextField("dependeCampo", getMessage("dependeCampo.label"));
		t.setAttribute("class", "input");
		t.setAttribute("autocomplete", "off");
		form.add(t);
		
		t = new TextField("dependeCampoValor", getMessage("dependeCampoValor.label"));
		t.setAttribute("class", "input");
		t.setAttribute("autocomplete", "off");
		form.add(t);
		
		checkEditavel = new Checkbox("editavel", getMessage("check.editavel.label"));
		form.add(checkEditavel);
		
		checkObrigatorio = new Checkbox("obrigatorioCadastroAdmin", getMessage("check.obrigatorio.admin.label"));
		form.add(checkObrigatorio);

		if (id != null) {
			campo = campoService.get(id);
		} else {
			campo = new Campo();
		}

		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao salvar");
		form.add(tsalvar);
		
		Submit cancelar = new Submit("novo", getMessage("novo.label"), this,"novo");
		cancelar.setAttribute("class", "botao branco info");
		form.add(cancelar);

		form.copyFrom(campo);
		
//		setFormTextWidth(form);
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				campo = id != null ? campoService.get(id) : new Campo();
				form.copyTo(campo);
				
				campo.setVisivel(checkVisivel.isChecked() ? 1 : 0);
				campo.setEditavel(checkEditavel.isChecked() ? 1 : 0);
				campo.setPublico(checkPublico.isChecked() ? 1 : 0);
				campo.setObrigatorioCadastroAdmin(checkObrigatorio.isChecked() ? 1 : 0);
				
				campoService.saveOrUpdate(campo, getEmpresa());

				setFlashAttribute("msg",  getMessage("msg.campo.salvar.sucess", campo.getNome()));
				setRedirect(CamposPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(CamposPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		campo = campoService.get(id);
		form.copyFrom(campo);
		
		checkVisivel.setChecked(1 == campo.getVisivel());
		checkEditavel.setChecked(1 == campo.getEditavel());
		checkPublico.setChecked(1 == campo.getPublico());
		checkObrigatorio.setChecked(1 == campo.getObrigatorioCadastroAdmin());
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Campo Campo = campoService.get(id);
			campoService.delete(Campo, getEmpresa());
			setRedirect(CamposPage.class);
			setFlashAttribute("msg", getMessage("msg.campo.deletar.sucess"));
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.campo.deletar.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		campos = campoService.findAll(getEmpresa());

		// Count(*)
		int pageSize = 100;
		long count = campos.size();

		createPaginator(page, count, pageSize);

		table.setPageSize(pageSize);
		table.setRowList(campos);
	}
}
