package br.livetouch.livecom.rest.oauth;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RequestTokenMap {
	
	private static final RequestTokenMap instance = new RequestTokenMap();
	private ConcurrentHashMap<String, LivecomToken> requestToken;
	
	
	private RequestTokenMap() {
		requestToken = new ConcurrentHashMap<>();
	}
	
	public static RequestTokenMap getInstance() {
		return instance;
	}
	
	public LivecomToken get(String token) {
		return requestToken.get(token);
	}
	
	public void put(String token, LivecomToken livecomToken) {
		requestToken.put(token, livecomToken);
	}
	
	public LivecomToken remove(String token) {
		return requestToken.remove(token);
	}

	public LivecomToken newRequestToken(String consumerKey, String callbackUrl, Map<String, List<String>> attributes) {
		LivecomToken rt = new LivecomToken();
		rt.newRequestToken(consumerKey, callbackUrl, attributes);
		requestToken.put(rt.getToken(), rt);
		return rt;
	}
	
}
