package br.livetouch.livecom.domain.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.util.CollectionUtils;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.CategoriasMap;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.repository.CategoriaPostRepository;
import br.livetouch.livecom.domain.service.CategoriaPostService;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.vo.CategoriaVO;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class CategoriaPostServiceImpl extends LivecomService<CategoriaPost> implements CategoriaPostService {
	@Autowired
	private CategoriaPostRepository rep;

	@Autowired
	private PostService postService;

	@Autowired
	public EmpresaService empresaService;

	@Override
	public CategoriaPost get(Long id) {
		return rep.get(id);
	}

	@Override
	@Transactional
	public void saveOrUpdate(Usuario userInfo, CategoriaPost c) {
		try {
			c.setEmpresa(userInfo.getEmpresa());

			boolean insert = c.getId() == null;
			String nome = c.getNome();
			if (insert) {
				boolean existe = rep.exists(nome, userInfo.getEmpresa());
				if (existe) {

					throw new DomainMessageException("validate.categoria.ja.existe");

				}
			} else {
				CategoriaPost categoria = get(c.getId());
				categoria.setNome(c.getNome());
				categoria.setCodigo(c.getCodigo());
				categoria.setUrlIcone(c.getUrlIcone());
				categoria.setCategoriaParent(c.getCategoriaParent());
				categoria.setPadrao(c.isPadrao());
				categoria.setCor(c.getCor());
				c = categoria;
			}

			if (c.getDateCreate() == null) {
				c.setDateCreate(new Date());
				c.setUserCreate(userInfo);
			}

			c.setDateUpdate(new Date());
			c.setUserUpdate(userInfo);

			rep.saveOrUpdate(c);

			String msg = null;
			if (insert) {
				msg = "Categoria [" + nome + "] cadastrada";
			} else {
				msg = "Categoria [" + nome + "] editada";
			}

			AuditoriaAcao acao = insert ? AuditoriaAcao.INSERIR : AuditoriaAcao.EDITAR;
			LogAuditoria.log(userInfo, c, AuditoriaEntidade.CATEGORIA, acao, msg);
			init();

		} catch (Exception e) {

		}
	}

	@Override
	public List<CategoriaPost> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	@Override
	public List<CategoriaVO> findByIdioma(Empresa empresa, Idioma idioma) {
		
		if(idioma == null) {
			List<CategoriaPost> categorias = rep.findAll(empresa);
			List<CategoriaVO> vos = CategoriaVO.create(categorias);
			return vos;
		}
		
		return rep.findByIdioma(empresa, idioma);
	}

	@Override
	public List<CategoriaPost> findAllParent(Empresa empresa) {
		return rep.findAllParent(empresa);
	}

	@Override
	public List<CategoriaPost> findAllParentByIdioma(Empresa empresa, Idioma idioma) {
		
		List<Idioma> list = new ArrayList<Idioma>();
		if(idioma != null && idioma.getId() != null) {
			list.add(idioma);
		}
		
		return rep.findAllParentByIdioma(empresa, list);
	}

	@Override
	public List<CategoriaPost> findAllParentByIdioma(Empresa empresa, List<Idioma> idiomas) {
		return rep.findAllParentByIdioma(empresa, idiomas);
	}

	@Override
	public List<CategoriaPost> findAllChildren(Empresa empresa) {
		return rep.findAllChildren(empresa);
	}

	@Override
	public List<CategoriaPost> findAllFetchChildren(Empresa empresa) {
		return rep.findAllFetchChildren(empresa);
	}

	@Override
	public List<CategoriaPost> findAllDefault(Empresa empresa) {
		return rep.findAllDefault(empresa);
	}

	public void delete(Usuario userInfo, CategoriaPost c) throws DomainException {
		delete(userInfo, c, false);
		init();
	}

	/**
	 * <pre>
	Regras:
	
	(r1) Não permite excluir a Categoria padrão.
	(r2) Não permite excluir a Categoria caso tenha posts associados.
	
	Cascades:
	
	(c1) Exclui a categoria e transfere os posts para a Categoria padrão.
	(c2) Exclui a Categoria e todos os seus Posts.
	
	Obs: Ao excluir uma categoria vai excluir suas sub-categorias. Pode dar cascade ou precisa ter uma regra disso?
	 * 
	 * </pre>
	 */
	@Override
	public void delete(Usuario userInfo, CategoriaPost c, boolean force) throws DomainException {

		if (c != null) {

			CategoriaPost categPadrao = rep.findPadrao(c.getEmpresa());

			boolean r1 = true;
			boolean r2 = ParametrosMap.getInstance(c.getEmpresa()).getBoolean(Params.DELETAR_CATEGORIA_R2_ON, true);

			boolean c1 = ParametrosMap.getInstance(c.getEmpresa()).getBoolean(Params.DELETAR_CATEGORIA_C1_ON, false);
			boolean c2 = true;

			LogAuditoria auditoria = null;
			try {
				// Auditoria
				String msg = "Categoria [" + c.getNome() + " deletada]";
				auditoria = LogAuditoria.log(userInfo, c, AuditoriaEntidade.CATEGORIA, AuditoriaAcao.DELETAR, msg,
						false);

				if (!force) {
					// (r1) Aplica regras
					if (r1) {
						if (c.isPadrao()) {
							throw new DomainException("A Categoria padrão não pode ser excluída.");
						}
					}

					// (r2)
					if (r2) {
						List<Long> ids = postService.findIdsByCategoria(c);
						if (ids != null && ids.size() > 0) {
							throw new DomainException("Esta categoria possui comunicados e não pode ser excluída.");
						}
					}

					if (c1) {
						// Transfere posts para categ padrao
						// (b)
						if (categPadrao == null) {
							throw new DomainException("Categoria padrão não encontrada");
						}

						executeReassociate("Chapeu", "categoria", categPadrao.getId(), c.getId());
						executeReassociate("Post", "categoria", categPadrao.getId(), c.getId());
					}
				}

				// Sempre executa c2.
				if (c2 || force) {
					// Exclui categ e posts e arquivos
					List<Long> postIds = postService.findIdsByCategoria(c);
					log.debug("delete [" + postIds.size() + "] posts from categoria[" + c.getId() + "]");
					postService.delete(userInfo, postIds, false, force);

					execute("delete from Chapeu where categoria.id = ?", c.getId());
					execute("delete from ArquivoThumb a where a.arquivo.id in (select id from Arquivo where categoria.id = ?)", c.getId());
					execute("delete from Arquivo a where a.categoria.id = ?", c.getId());
					execute("delete from CategoriaIdioma c where c.categoria.id = ?", c.getId());
				}

				if (force) {
					// Se nao transferiu os relacionamentos, seta nulo
					executeSetNull("Chapeu", "categoria", c.getId());
					executeSetNull("Post", "categoria", c.getId());
					executeSetNull("Grupo", "categoria", c.getId());
					if (c.getCategoriaParent() != null) {
						executeSetNull("CategoriaPost", "categoriaParent", c.getCategoriaParent().getId());
					}
				}

				// Children
				Set<CategoriaPost> chidren = c.getCategorias();
				for (CategoriaPost child : chidren) {
					delete(null, child, force);
				}

				rep.delete(c);

				if (auditoria != null) {
					auditoria.finish();
				}

			} catch (DomainException e) {
				log.error("Erro ao excluir a Categoria [" + c.toStringIdDesc() + "]: " + e.getMessage(), e);
				LogSistema.logError(userInfo, e);
				throw new DomainException(e.getMessage());
			} catch (Exception e) {
				Throwable root = ExceptionUtils.getRootCause(e);
				if (root == null) {
					root = e;
				}
				log.error("Erro ao excluir a Categoria [" + c + "]: " + root.getMessage(), root);

				LogSistema.logError(userInfo, e);

				e.printStackTrace();
				System.err.println(e.getMessage());
				throw new DomainException("Não foi possível excluir a categoria [" + c + "]", e);
			}
		}
	}

	@Override
	public void updateOrdemByIds(Usuario user, String ids) {
		try {
			if (StringUtils.isNotEmpty(ids)) {
				if (StringUtils.contains(ids, ",")) {
					String[] split = StringUtils.split(ids, ",");
					for (int i = 0; i < split.length; i++) {
						String id = split[i];
						id = StringUtils.trim(id);
						CategoriaPost categoriaPost = get(new Long(id));
						if (categoriaPost != null) {
							categoriaPost.setOrdem(i);

							saveOrUpdate(user, categoriaPost);

						}
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public List<CategoriaPost> findAllByNomeLikeWithMax(String nome, int max, Empresa empresa) {

		return rep.findAllByNomeLikeWithMax(nome, max, empresa);

	}

	@Override
	public String csvCategoria(Empresa empresa) {

		List<CategoriaPost> findAll = findAll(empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("CODIGO").add("NOME");
		for (CategoriaPost categoriaPost : findAll) {
			body.add(categoriaPost.getCodigoDesc()).add(categoriaPost.getNome());
			body.end();
		}
		return generator.toString();
	}

	@Override
	public CategoriaPost findByName(CategoriaPost categoria, Empresa empresa) {

		return rep.findByName(categoria, empresa);

	}

	@Override
	public CategoriaPost findByCodigo(Empresa empresa, String codigo) {

		return rep.findByCodigo(empresa, codigo);

	}

	@Override
	public List<CategoriaPost> findMenuCategoria(Empresa emp) {

		return rep.findMenuCategoria(emp);

	}

	@Override
	public void deleteArquivos(Long categoriaId, List<Long> arquivosIds) {
		rep.deleteArquivos(categoriaId, arquivosIds);
	}

	@Override
	public List<CategoriaPost> findByParent(CategoriaPost pai) {
		return rep.findByParent(pai);
	}

	@Override
	public List<CategoriaPost> findByParentAndIdiomas(CategoriaPost pai, List<Idioma> idiomas) {
		return rep.findByParentAndIdiomas(pai, idiomas);
	}
	
	@Override
	public List<CategoriaPost> findByNameAndIdioma(String nome, Idioma idioma, Empresa empresa) {
		return rep.findByNameAndIdioma(nome, idioma, empresa);
	}

	@Override
	public List<CategoriaPost> findParentByIdiomas(List<Idioma> idiomas) {
		return rep.findParentByIdiomas(idiomas);
	}

	@Override
	public List<CategoriaPost> findByParent(CategoriaVO pai) {
		return rep.findByParent(pai);
	}

	@Override
	public HashMap<Long, Set<Long>> mountMap(Empresa empresa) {

		HashMap<Long, Set<Long>> map = new HashMap<Long, Set<Long>>();
		
		List<CategoriaPost> categorias = this.findAll(empresa);
		for (CategoriaPost c : categorias) {
			HashSet<Long> set = new HashSet<Long>();
			
			set = recursive(c, set);
			map.put(c.getId(), set);
		}
		
		return map;
		
	}
	
	public HashSet<Long> recursive(CategoriaPost pai, HashSet<Long> set) {
		
		List<CategoriaPost> filhos = this.findByParent(pai);
		
		if(filhos.isEmpty()) {
			return set;
		}
		
		
		for (CategoriaPost filho : filhos) {
			set.add(filho.getId());
			recursive(filho, set);
		}
		
		return set;
	}
	
	@Override
	public List<CategoriaVO> mountCategoriasVO(List<CategoriaPost> pais, Idioma i) {

		List<CategoriaVO> list = new ArrayList<CategoriaVO>();
		
		for (CategoriaPost c : pais) {
			CategoriaVO vo = new CategoriaVO(c, i);
			recursiveCategoriasVO(vo, i);
			list.add(vo);
		}
		
		return list;
		
	}
	public void recursiveCategoriasVO(CategoriaVO pai, Idioma i) {
		
		List<CategoriaPost> filhos = this.findByParent(pai);
		
		if(filhos.isEmpty()) {
			return;
		}
		
		List<CategoriaVO> subcategorias = CategoriaVO.create(filhos, i);
		pai.setSubcategorias(subcategorias);
		
		for (CategoriaVO filho : subcategorias) {
			recursiveCategoriasVO(filho, i);
		}
		
		return;
	}
	
	@Override
	public CategoriasMap init() {
		
		List<Empresa> empresas = empresaService.findAll();
		CategoriasMap.clear();
		for (Empresa empresa : empresas) {
			HashMap<Long, Set<Long>> map = mountMap(empresa);
			CategoriasMap.setCategorias(empresa.getId(), map);
		}
		
		return CategoriasMap.getInstance();
	}

	@Override
	public List<CategoriaPost> findByIdsAndIdioma(Idioma idioma, List<Long> categoriaIds) {
		List<CategoriaPost> categorias = rep.findByIdsAndIdioma(idioma, categoriaIds);
		if(!CollectionUtils.isNullOrEmpty(categorias)) {
			return orderByOriginalList(categoriaIds, categorias);
		}
		return categorias;
	}

	private List<CategoriaPost> orderByOriginalList(List<Long> categoriaIds, List<CategoriaPost> categorias) {
		Map<Long, CategoriaPost> map = new LinkedHashMap<Long, CategoriaPost>();
		for (Long id : categoriaIds) {
			map.put(id, null);
		}
		for (CategoriaPost categoriaPost : categorias) {
			map.put(categoriaPost.getId(), categoriaPost);
		}
		return new ArrayList<>(map.values());
	}

}