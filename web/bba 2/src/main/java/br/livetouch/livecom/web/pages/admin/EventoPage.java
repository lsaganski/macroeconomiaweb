package br.livetouch.livecom.web.pages.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.RadioStatus;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.Evento;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.utils.UploadHelper;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.ImageField;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextArea;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.DateField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class EventoPage extends LivecomAdminPage {

	public Form form = new Form();
	public String msgErro;
	public String endereco;
	public String numero;
	public String cidade;
	public Long id;
	public int page;
	public Evento evento;
	public Long arquivoFotoId;
	public RadioStatus ativo;

	public List<Grupo> gruposDefault;

	@Override
	public void onInit() {
		super.onInit();

		form();

		if (id != null) {
			evento = eventoService.get(id);
		} else {
			evento = new Evento();
		}
		form.copyFrom(evento);
	}

	public void form() {
		form.add(new IdField());

		TextField t = null;

		t = new TextField("nome", getMessage("nome.label"), true);
		t.setAttribute("class", "input");
		form.add(t);
		TextArea descricao = new TextArea("descricao", getMessage("descricao.label"), true);
		descricao.setAttribute("class", "input");
		descricao.setRows(15);
		form.add(descricao);
		
		t = new TextField("endereco", getMessage("endereco.label"), true);
		t.setAttribute("class", "input");
		form.add(t);
		
		t = new TextField("numero", getMessage("numero.label"), true);
		t.setAttribute("class", "input");
		form.add(t);
		
		t = new TextField("estado", getMessage("estado.label"), true);
		t.setAttribute("class", "hasCustomSelect");
		t.setAttribute("id", "estado");
		t.setSize(0);
		form.add(t);

		t = new TextField("cidade", getMessage("cidade.label"), true);
		t.setAttribute("class", "hasCustomSelect");
		t.setAttribute("id", "cidade");
		t.setSize(0);
		form.add(t);

		t = new TextField("complemento", getMessage("complemento.label"), true);
		t.setAttribute("class", "input");
		form.add(t);

		DateField data = new DateField("data", getMessage("data.label"), true);
		data.setAttribute("class", "input");
		data.setAttribute("id", "form_dataPublicacao");
		data.setFormatPattern("dd/MM/yyyy");
		form.add(data);
		
		form.add(new ImageField("foto", getMessage("foto.label")));
		
		ativo = new RadioStatus();
		ativo.setVerticalLayout(false);
		ativo.setRequired(true);
		ativo.setLabel(getMessage("ativo.label"));
		form.add(ativo);

		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);

		if (id != null) {
			Submit tDeletar = new Submit("deletar", getMessage("deletar.label"), this, "deletar");
			tDeletar.setAttribute("class", "botao btSalvar");
			form.add(tDeletar);
		}
		Submit cancelar = new Submit("cancelar", getMessage("cancelar.label"), this, "cancelar");
		cancelar.setAttribute("class", "botao");
		form.add(cancelar);

		//setFormTextWidth(form);
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if(form.isValid()){
		try {
			evento = id != null ? eventoService.get(id) : new Evento();
			form.copyTo(evento);
			
			boolean editar = id != null;
			
			List<Long> grupoIds = new ArrayList<Long>();
			
			String [] idsGrupo = this.getParam("idsGrupos").split(",");
			if(idsGrupo.length > 0) {
				for (String id : idsGrupo) {
					if(StringUtils.isNotEmpty(id.trim())) {
						grupoIds.add(Long.valueOf(id.trim()));
					}
				}
			}
			
			List<Grupo> grupos = grupoService.findAllByIds(grupoIds);
			evento.setGrupos(grupos);
			
			setFotoEvento();
			eventoService.saveOrUpdate(evento);
			
			getContext().setSessionAttribute("eventoId", evento.getId());
			if(editar) {
				setRedirect(EventosPage.class);
			} else {
				setRedirect(AgendaPage.class, "evento", String.valueOf(evento.getId()));
			}

			return false;

		} catch (DomainException e) {
			form.setError(e.getMessage());
		} catch (Exception e) {
			logError(e.getMessage(), e);
			form.setError(e.getMessage());
		}
		} else {
			setFlashAttribute("msg", getMessage("msg.campos.obrigatorio.error"));
		}
		return true;
	}

	public boolean cancelar() {
		setRedirect(EventosPage.class);
		return true;
	}

	public boolean deletar() {
		try {
			try {
				evento = id != null ? eventoService.get(id) : new Evento();

				eventoService.delete(evento);

				setFlashAttribute("msg", getMessage("msg.evento.excluir.sucess"));
				setRedirect(EventosPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (ConstraintViolationException e) {
				logError(e.getMessage(), e);
				this.msgErro = getMessage("msg.evento.excluir.error");
			} catch (Exception e) {
				logError(e.getMessage(), e);
				form.setError(e.getMessage());
			}
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(), e);
			this.msgErro = getMessage("msg.evento.excluir.error");
		}
		return true;
	}
	
	/**
	 * Se o web service enviar este id, atualiza a foto com o arquivo.
	 * @throws IOException 
	 */
	private void setFotoEvento() throws IOException {
		if(arquivoFotoId != null) {
			Arquivo a = arquivoService.get(arquivoFotoId);
			
			if(a != null) {
				createThumbsArquivoFoto(a);
			}
		}
	}
	
	protected void createThumbsArquivoFoto(Arquivo a) throws IOException {
		evento.setFotoS3(a.getUrl());
		
		UploadHelper.log.debug("Arquivo: " + a.getId());
		
		UploadHelper.log.debug("Gerando thumbs arquivo: " + a.getNome());
		ThumbVO voThumb = UploadHelper.createThumbFotoUsuario(getParametrosMap(), "evento/fotoProfile", a.getUrl(), a.getNome());
		ArquivoThumb t = new ArquivoThumb();
		t.setThumbVo(voThumb);
		t.setArquivo(a);
		arquivoService.saveOrUpdate(t);

		evento.setFoto(a);
	}

	

	@Override
	public void onRender() {
		super.onRender();

		gruposDefault = grupoService.findAllDefault(getUsuario());
	}
}
