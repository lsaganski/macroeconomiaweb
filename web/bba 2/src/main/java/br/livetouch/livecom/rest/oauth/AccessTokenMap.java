package br.livetouch.livecom.rest.oauth;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.glassfish.jersey.server.oauth1.OAuth1Token;

import br.livetouch.livecom.domain.Application;

public class AccessTokenMap {
	
	public static final AccessTokenMap instance = new AccessTokenMap();
	private ConcurrentHashMap<String, LivecomToken> accessToken;
	
	private AccessTokenMap() {
		accessToken = new ConcurrentHashMap<>();
	}
	
	public static AccessTokenMap getInstance() {
		return instance;
	}
	
	
	public void setAcessTokens(List<Application> apps) {
		accessToken.clear();
	}
	
	public LivecomToken get(String key) {
		LivecomToken livecomToken = accessToken.get(key);
		return livecomToken != null ? livecomToken : null;
	}
	
	public void put(String key, LivecomToken value) {
		accessToken.put(key, value);
	}
	
	public void revokeAccessToken(String token, String principalName) {
		LivecomToken t = get(token);
		if (t != null && t.getPrincipal().getName().equals(principalName)) {
			accessToken.remove(token);
		}
	}

	public OAuth1Token newAccessToken(OAuth1Token requestToken, String verifier) {
		String tokenVerifier = VerifierTokenMap.getInstance().remove(requestToken.getToken());
		boolean isValid = !verifier.equals(tokenVerifier);
		if (verifier == null || requestToken == null || isValid) {
			return null;
		}
		LivecomToken token = RequestTokenMap.getInstance().remove(requestToken.getToken());
		
		if (token == null) {
			return null;
		}
		
		LivecomToken at = new LivecomToken();
		at.newAccessToken(token);
		put(at.getToken(), at);
		return at;
	}
}
