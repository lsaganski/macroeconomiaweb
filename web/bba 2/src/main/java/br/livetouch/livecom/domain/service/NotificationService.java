package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.repository.NotificationSearch;
import br.livetouch.livecom.domain.vo.AudienciaVO;
import br.livetouch.livecom.domain.vo.NotificationBadge;
import br.livetouch.livecom.domain.vo.NotificationVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioNotificationsVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoFiltro;
import br.livetouch.livecom.domain.vo.UserToPushVO;
import net.livetouch.tiger.ddd.DomainException;
import net.livetouch.tiger.ddd.Entity;

public interface NotificationService extends Service<Notification> {

	List<NotificationUsuario> findAll(NotificationSearch search);
	
	List<NotificationUsuario> findAllAgrupada(NotificationSearch search);
	List<Usuario> findNaoLidasByNotification(Notification notification, NotificationSearch search);
	
	List<NotificationUsuario> findSolicitacoes(NotificationSearch search);
	
	List<Notification> findAllToSendPush();
	
	List<Long> findIdUsersFrom(Notification parent);
	List<UserToPushVO> findUsersFrom(Notification parent);

	@Transactional(rollbackFor=Exception.class)
	void save(Notification n) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Notification n) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void deleteFilhas(Notification n) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void read(NotificationUsuario notUser) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void readAgrupada(NotificationUsuario notUser, Post p, TipoNotificacao tipo) throws DomainException;
	
	Notification findByPost(Post p, TipoNotificacao tipo);
	
	@Transactional(rollbackFor=Exception.class)
	int markAllAsRead(Usuario u);

	List<RelatorioNotificationsVO> reportNotifications(RelatorioFiltro filtro) throws DomainException;

	List<RelatorioNotificationsVO> reportNotificationsByType(RelatorioFiltro filtro) throws DomainException;

	List<RelatorioNotificationsVO> reportNotificationsByTypeDetalhes(RelatorioFiltro filtro) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	int markPostAsRead(Post p, Usuario u);

	@Transactional(rollbackFor=Exception.class)
	int markGrupoAsRead(Grupo g, Usuario u);

	@Transactional(rollbackFor=Exception.class)
	int markSolicitacaoAsRead(Grupo g, Usuario u);

	@Transactional(rollbackFor=Exception.class)
	int markSolicitacaoAmizadeAsRead(Usuario usuario, Usuario amigo);
	
	@Transactional(rollbackFor=Exception.class)
	int markComentarioAsRead(Long p, Long u);

	void delete(List<Notification> notifications);
	
	/**
	 * user[0] = Long id
	 * user[1] = String login
	 * user[2] = Long badge
	 */
	List<Object[]> findBadgesToPush(TipoNotificacao tipo,List<Long> ids);
	/**
	 * user[0] = Long id
	 * user[1] = String login
	 * user[2] = Long badge = 0
	 */
	List<Object[]> findUsersWithoutBadgesToPush(List<Long> ids);
	NotificationBadge findBadges(Usuario u);
	
	List<Notification> findViewsByFilter(RelatorioVisualizacaoFiltro filtro);
	
	List<NotificationVO> findAllGroupUserByPostDate(Usuario usuario, Post post);
	AudienciaVO getCountAudienciaByPost(Post post);
	
	void deleteBy(Object p);
	void deleteBy(Class<? extends Entity> cls, List<Long> ids);
	List<Notification> findAll();
	List<Notification> findByGrupo(Grupo g);
	
	List<NotificationUsuario> findNotificationsFilhas(Notification notification);
	
	/**
	 * Marca todas as Notifications como lidas mas na contagem não soma as que estão como invisiveis
	 */
	
	List<NotificationUsuario> findPostVisivel(Usuario u);
	
	
	void saveOrUpdate(NotificationUsuario notification);
	int updateSendPushNotification(Long id, List<Long> userIds);
	NotificationUsuario getNotificationUsuario(Long id);
	
	@Transactional(rollbackFor=Exception.class)
	int markAsRead(NotificationUsuario notificationUsuario);
	
	@Transactional(rollbackFor=Exception.class)
	void markAllAsRead(Long id);
	
}
