package br.livetouch.livecom.web.pages.ws;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoGrupo;
import br.livetouch.livecom.domain.vo.ConversaVO;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class NovoGrupoMensagemPage extends WebServiceFormPage {

	private UsuarioLoginField tUser;
	
	public Long arquivo_id;
	public String nome;

	@Override
	protected void form() {
		form.add(tUser = new UsuarioLoginField("user", true,usuarioService, getEmpresa()));
		form.add(new TextField("nome",true));
		form.add(new TextField("usuario_ids",true));
		form.add(new TextField("arquivo_id",false));
	}

	@Override
	protected Object go() throws Exception {
		
		if (!form.isValid()) {
			if(isWsVersion3()) {
				Response r = Response.error(getFormErrorMensagemResult(form).toString());
				return r;
			}
			return getFormErrorMensagemResult(form);
		}
		Usuario u = tUser.getEntity();
		if(u == null) {
			if(isWsVersion3()) {
				Response r = Response.error("Usuario nao encontrado");
				return r;
			}
			return new MensagemResult("NOK" ,"Usuario nao encontrado");
		}

		Arquivo a = null;
		if (arquivo_id != null && arquivo_id > 0) {
			a = arquivoService.get(arquivo_id);
//			if(a == null) {
//				return new MensagemResult("NOK" ,"Arquivo inválido.");
//			}
		}
		
		List<Long> usuarioIds = getListIds("usuario_ids");
		List<Usuario> usuarios = usuarioService.findAllByIds(usuarioIds);
		
		if (usuarios == null || usuarios.size() == 0) {
			if(isWsVersion3()) {
				Response r = Response.error("Usuários inválidos");
				return r;
			}
			return new MensagemResult("NOK" ,"Usuários inválidos");
		}
		
//		GrupoMensagem g = grupoMensagemService.findByNome(nome);
//		if(g != null) {
//			return new MensagemResult("OK" ,"Já existe um grupo com esse nome.");
//		}
		
		Grupo g = createGrupo(u, a);
		
		for (Usuario user : usuarios) {
			usuarioService.addGrupoToUser(user, g, true);
		}

		MensagemConversa c = new MensagemConversa();
		c.setDataCreated(new Date());
		c.setPost(null);
		c.setGrupo(g);
		c.setDataUpdated(new Date());
		c.setFrom(u);
		
		mensagemService.saveOrUpdate(c);
		
		criarPrimeiraMsg(u, c);
		
		
		ConversaVO vo  = new ConversaVO();
		vo.setConversa(u,c);
		
		if(isWsVersion3()) {
			Response r = Response.ok("OK");
			r.conversa = vo;
			return r;
		}

		return vo;
	}

	private void criarPrimeiraMsg(Usuario u, MensagemConversa c) {
		Grupo grupo = c.getGrupo();
		try {
			Mensagem mensagem = mensagemService.saveMensagemGrupo(null, c.getId(), grupo.getId(), u.getId(), "Bem vindo ao Grupo", null, null, null, null);
			Set<Mensagem> mensagens = new HashSet<>();
			mensagens.add(mensagem);
			c.setMensagens(mensagens);
		} catch (DomainException e) {
			log.debug("Erro ao salvar a mensagem para o Grupo " + e.getMessage(), e);
		}
	}

	private Grupo createGrupo(Usuario u, Arquivo a) throws DomainException {
		Grupo g = new Grupo();
		g.setNome(nome);
		g.setEmpresa(getEmpresa());
		g.setPadrao(false);
		g.setTipoGrupo(TipoGrupo.PRIVADO_OPCIONAL);
		g.setFoto(a);
		grupoService.saveOrUpdate(u, g);
		usuarioService.addGrupoToUser(u, g, true);
		return g;
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("conversa", ConversaVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
