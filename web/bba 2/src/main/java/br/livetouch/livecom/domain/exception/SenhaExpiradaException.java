package br.livetouch.livecom.domain.exception;

import br.infra.util.MessageUtil;
import net.livetouch.tiger.ddd.DomainException;

public class SenhaExpiradaException extends DomainException {

	private static final long serialVersionUID = 5571798052715248656L;

	public SenhaExpiradaException(String message) {
		super(message);
	}

	@Override
	public String getMessage() {
		String key = super.getMessage();
		String s = MessageUtil.getString(key );
		return s;
	}
}
