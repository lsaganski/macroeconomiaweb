package br.livetouch.livecom.web.pages.training.admin;

import javax.servlet.http.HttpSession;

import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.web.pages.HomePage;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;

/**
 * @author Ricardo Lecheta
 *
 */
public class TrainingAdminPage extends LivecomLogadoPage {

	public static final String TEMPLATE = "/training/border.htm";

	@Override
	public String getTemplate() {
		return TEMPLATE;
	}

	@Override
	public boolean onSecurityCheck() {
		if(!isBuildBJJ()) {
			// livecom
			setRedirect(HomePage.class);
			return false;
		}
		return onSecurityCheckAdmin();
	}

	public boolean onSecurityCheckAdmin() {
		UserInfoVO u = UserInfoVO.getHttpSession(getContext());
		boolean logado = u != null;
		if (logado) {
			if(u.isAdmin()) {
				return true;
			}
		}

		UserInfoVO userInfo = getUserInfoVO();
		Livecom.getInstance().removeUserInfo(userInfo,"web");

		HttpSession session = getContext().getSession();
		session.invalidate();

		LoginPage.redirect(this);

		return false;
	}
	
}
