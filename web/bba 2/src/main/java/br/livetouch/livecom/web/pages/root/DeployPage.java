package br.livetouch.livecom.web.pages.root;

import java.io.IOException;

import org.jfree.util.Log;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.LivecomRootPage;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class DeployPage extends LivecomRootPage {

	public Form form = new Form();
	private TextField tCmd;

	@Override
	public void onInit() {
		super.onInit();
		
		form.add(tCmd = new TextField("cmd",true));
		
		form.add(new Submit("Executar",this,"executar"));
		form.add(new Submit("Deploy",this,"deploy"));
	}
	
	public boolean executar() {
		
		if(form.isValid()) {
			String path = tCmd.getValue();
			
			Log.debug("Runtime.execute: " + path);

			try {
				Runtime.getRuntime().exec(path);
				
				msg = "Runtime.execute OK: " + path;

				log(msg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return true;
	}
	
	public boolean deploy() {
		
		try {
			
			Runtime.getRuntime().exec("sh /root/deploy-livecom.sh");
			msg = "Runtime.execute Deploy OK";

			log(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return true;
	}
}
