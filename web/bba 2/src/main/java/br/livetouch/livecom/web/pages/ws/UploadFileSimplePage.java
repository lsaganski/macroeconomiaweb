package br.livetouch.livecom.web.pages.ws;

import java.io.File;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.vo.FileVO;
import net.sf.click.control.FileField;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class UploadFileSimplePage extends WebServiceXmlJsonPage {

	public Form form = new Form();

	public String dir;
	public String url;
	public String contentType;
	private TextField tMode;

	private FileField fileField;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		form.add(fileField = new FileField("fileField"));

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode", "mode: xml, json"));
		tMode.setValue("json");

		form.add(new Submit("Upload File"));

//		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {

			FileItem item = fileField.getFileItem();
			System.out.println("Upload: " + item);
			log("upload: " + item);
			
			log("upload ["+item.getName()+"]: " + getContext().getRequest().getParameterMap());
			
			File f = new File("/var/lib/tomcat7/webapps/livecom/temp",item.getName());
			FileUtils.writeByteArrayToFile(f, item.get());

			return new MensagemResult("OK", "http://livetouchdev.com.br/livecom/temp/" + item.getName());
		}

		return new MensagemResult("ERRO", "Form inválido.");
	}


	@Override
	protected void xstream(XStream x) {
		x.alias("file", FileVO.class);
		super.xstream(x);
	}
}
