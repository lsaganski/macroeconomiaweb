package br.livetouch.livecom.chat.webSocket;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import br.livetouch.livecom.chatAkka.protocol.RawMessage;
 
public class WebSocketNotificationEncoder implements Encoder.Text<RawMessage> {
	
	@Override
	public void init(final EndpointConfig config) {
	}
 
	@Override
	public void destroy() {
	}
 
	@Override
	public String encode(final RawMessage raw) throws EncodeException {
		String json = raw.getText();
		return json;
	}
}