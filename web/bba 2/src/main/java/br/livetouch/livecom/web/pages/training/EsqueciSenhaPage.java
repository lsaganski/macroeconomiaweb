package br.livetouch.livecom.web.pages.training;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.EmailReportService;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

/**
 * Alterar senha do usuario
 * 
 * @author ricardo
 *
 */
@Controller
@Scope("prototype")
public class EsqueciSenhaPage extends TrainingPage {

	public Form form = new Form();
	
	private UsuarioLoginField tLogin;
	
	@Autowired
	protected EmailReportService emailReportService;

	@Override
	public void onInit() {
		super.onInit();
		
		logoutUserInfo();

		form.add(tLogin = new UsuarioLoginField("login","login",true , usuarioService, getEmpresa()));
		
		tLogin.setFocus(true);

		form.add(new Submit("enviar", this, "enviar"));
		
		TrainingUtil.setTrainingStyle(form);
	}

	public boolean enviar() throws DomainException {
		try {
			if(form.isValid()) {
				Usuario u = tLogin.getEntity();
				if(u == null) {
					form.setError("Login não encontrado");
				} else {
					usuarioService.recuperarSenha(getEmpresa(), getUsuario(), u);
					msg = "A nova senha foi enviada para o e-mail";
				}
			}
		} catch (Exception e) {
			form.setError(e.getMessage());
		}

		return true;
	}

}
