package br.livetouch.livecom.web.pages.ws;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Amizade;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.OrigemCadastro;
import br.livetouch.livecom.domain.enums.StatusAmizade;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.vo.UsuarioAutocompleteFiltro;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.domain.vo.UsuariosVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LongField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class UsuariosAutoCompletePage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	
	public boolean get;
	public List<UsuarioVO> users;
	private TextField tNaoAtivo;
	
	private UsuarioAutocompleteFiltro filtro;
	
	@Override
	public void onInit() {
		
		filtro = new UsuarioAutocompleteFiltro();
		
		form.setMethod("post");

		TextField tQ = new TextField("q");
		form.add(tQ);
		tQ.setFocus(true);
		form.add(new TextField("grupoId"));
		form.add(new TextField("not_users_ids"));
		form.add(tNaoAtivo = new TextField("naoAtivo"));
		form.add(new TextField("usuarios"));
		form.add(new TextField("page"));
		form.add(new TextField("maxRows"));
		form.add(new TextField("buscaTotal"));
		form.add(new TextField("podePostar"));
		form.add(new TextField("coluna"));
		form.add(new TextField("ordenacao"));
		form.add(new LongField("cargoId"));
		form.add(new TextField("acesso"));
		form.add(new TextField("origem"));
		form.add(new IntegerField("statusAtivacao"));

		tNaoAtivo.setValue("-1");
		
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		
		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		form.add(new Submit("buscar"));

//		setFormTextWidth(form);
	}

	@Override
	public void onGet() {
		super.onGet();
		
		get = true;
	}

	@Override
	protected Object execute() throws Exception {
		
		if(form.isValid()) {
			form.copyTo(filtro);
			
			List<Long> usuariosIds = getListIds("usuarios");
			filtro.setUsuariosIds(usuariosIds);
			
			List<Long> gruposIds = getListIds("grupoId");
			List<Long> notIds = getListIds("not_users_ids");
			
			filtro.setGruposIds(gruposIds);
			filtro.setNotUsersIds(notIds);
			
			if(filtro.getStatusAtivacao() != null) {
				StatusUsuario statusUsuario = StatusUsuario.values()[Integer.valueOf(filtro.getStatusAtivacao())];
				filtro.setStatusUsuario(statusUsuario);
			}

			if(StringUtils.isNotEmpty(filtro.getOrigem())) {
				OrigemCadastro origemCadastro = OrigemCadastro.valueOf(filtro.getOrigem());
				filtro.setOrigemCadastro(origemCadastro);
			}
			
			filtro.setNome(filtro.getQ());
			
			users = new ArrayList<UsuarioVO>();
			List<Usuario> list = usuarioService.findByFiltro(filtro, getEmpresa());
			for (Usuario usuario : list) {
				UsuarioVO vo = new UsuarioVO();
				vo.setUsuario(usuario, usuarioService, grupoService);
				isAmigo(usuario, vo);
				users.add(vo);
			}
			
			if(filtro.isBuscaTotal()) {
				Long count = usuarioService.getCountByNomeLike(filtro, getEmpresa());
				UsuariosVO vo = new UsuariosVO();
				vo.total = count;
				vo.usuarios = users;
				
				if(isWsVersion3()) {
					Response r = Response.ok("OK");
					r.usuarios = vo;
					return r;
				}
				
				return vo;
			}
			
			if(isWsVersion3()) {
				Response r = Response.ok("OK");
				r.users = users;
				return r;
			}
			
			return users;
		}
		return new MensagemResult("NOK", "Erro ao efetuar busca");
	}
	
	private void isAmigo(Usuario usuario, UsuarioVO vo) {
		Amizade amizade = usuarioService.getAmizade(getUsuario(), usuario);
		
		String status = "";
		if(amizade != null) {
			StatusAmizade statusAmizade = amizade.getStatusAmizade();
			
			if(StatusAmizade.APROVADA.equals(statusAmizade)) {
				status = "AMIGOS";
			}
			
			if(StatusAmizade.SOLICITADA.equals(statusAmizade)) {
				if(usuario.getId().equals(amizade.getAmigo().getId())){
					status = "AGUARDANDO_APROVACAO";
				} else{
					status = "SOLICITADO";
				}
			}
		}
		vo.amigo = status;
		
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("list", UsuariosVO.class);
		super.xstream(x);
	}
}
