package br.livetouch.livecom.domain.vo;


public class LocationVO {
	private Double lat;
	private Double lng;
	
	public LocationVO(Double lat, Double lng) {
		this.lat = lat;
		this.lng = lng;
	}

	public Double getLat() {
		return lat;
	}
	
	public Double getLng() {
		return lng;
	}
}
