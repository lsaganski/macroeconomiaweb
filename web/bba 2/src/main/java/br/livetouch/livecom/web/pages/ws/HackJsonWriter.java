package br.livetouch.livecom.web.pages.ws;

import java.io.Writer;

import com.thoughtworks.xstream.io.json.AbstractJsonWriter;
import com.thoughtworks.xstream.io.json.JsonWriter;

/**
 * @author Ricardo Lecheta
 *
 */
public class HackJsonWriter extends JsonWriter {
	private boolean dropQuotes = false;

	public HackJsonWriter(Writer writer) {
		super(writer);
	}

	public void dropQuotes() {
		dropQuotes = true;
	}

	public void allowQuotes() {
		dropQuotes = false;
	}

	@Override
	protected void addValue(String value, AbstractJsonWriter.Type type) {
		if (dropQuotes) {
			super.addValue(value, AbstractJsonWriter.Type.NULL);
		} else {
			super.addValue(value, type);
		}
	}
}
