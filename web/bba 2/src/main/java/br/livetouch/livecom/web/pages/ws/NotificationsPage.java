package br.livetouch.livecom.web.pages.ws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.repository.NotificationSearch;
import br.livetouch.livecom.domain.vo.BadgeChatVO;
import br.livetouch.livecom.domain.vo.NotificationBadge;
import br.livetouch.livecom.domain.vo.NotificationResponseVO;
import br.livetouch.livecom.domain.vo.NotificationVO;
import br.livetouch.livecom.domain.vo.UsuarioSimpleVO;
import br.livetouch.livecom.security.UserAgentUtil;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.DateField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class NotificationsPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private UsuarioLoginField tUser;
	public int maxRows;
	public int page;
	private LongField tNotificationId;
	
	private LongField tNovasNotificationsId;
	private DateField tDataInicial;
	private DateField tDataFinal;
	
	public String tipo;
	public TextField tSolicitacoes;
	
	public String list;
	public String count;
	public String markAllAsRead;
	public String markAsRead;
	public TextField tTextoBusca;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		form.add(tUser = new UsuarioLoginField("user_id","user_id", false,usuarioService, getEmpresa()));
		form.add(new TextField("tipo","new_post,like,favorito,comentario,mensagem,chat"));
		form.add(tTextoBusca =new TextField("texto","Busca por palavras-chave"));
		form.add(tSolicitacoes = new TextField("solicitacoes","1 - Busca as solicitacoes | 0 - retorna lista vazia"));
		form.add(tNotificationId = new LongField("id_notification","id_notification"));

		form.add(tNovasNotificationsId = new LongField("novas_notifications_id","novas_notifications_id"));
		
		form.add(new IntegerField("list","list (1  = listar todas, 2 = listar nao lidas, 3 = listar lidas)"));
		form.add(new IntegerField("count","count (1  = retorna apenas badge)"));
		form.add(new IntegerField("markAsRead","markAsRead (1 = marcar como lida)"));
		form.add(new IntegerField("markAllAsRead","markAllAsRead (1 = marcar todas como lidas)"));
		
		tDataInicial = new DateField("dataInicial","dataInicial", false);
		tDataFinal = new DateField("dataFinal","dataFinal", false);
		tDataInicial.setFormatPattern("dd/MM/yyyy");
		tDataFinal.setFormatPattern("dd/MM/yyyy");
		form.add(tDataInicial);
		form.add(tDataFinal);
		
//		tDataInicial.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));
//		tDataFinal.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));

		tDataInicial.setMaxLength(10);
		tDataFinal.setMaxLength(10);
		
		form.getField("list").setValue("1");
		
		form.add(new IntegerField("page","page"));
		form.add(new IntegerField("maxRows","maxRows"));
		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));
		form.add(tMode = new TextField("mode","mode"));
		
		tMode.setValue("");

		tUser.setFocus(true);

		tMode.setValue("json");

		form.add(new Submit("consultar"));

		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario u = tUser.getEntity();
			if(u == null) {
				return new MensagemResult("ERROR" ,"Usuário inválido.");
			}
			
			if(StringUtils.equals(tipo, "chat")) {
				/**
				 * Badge do chat na web
				 */
				Long count = mensagemService.countMensagensNaoLidas(u);
				BadgeChatVO badgeVo = new BadgeChatVO();
				badgeVo.mensagens = count;
				
				if(isWsVersion3()) {
					Response r = Response.ok("OK");
					r.chatBadges = badgeVo;
					return r;
				}
				
				return badgeVo;
			} else {
				String sideBoxNotificationOn = ParametrosMap.getInstance(u.getEmpresa()).get(Params.SIDEBOX_NOTIFICATION_ON, "1");
				if("0".equals(sideBoxNotificationOn)) {
					return new NotificationResponseVO();
				}
				
				/**
				 * Badge do sino na web
				 */
				Long notificationId = tNotificationId.getLong();

				if(StringUtils.isEmpty(list)) {	
					if(notificationId != null && "1".equals(markAsRead)) {
						// Marcar como lida
						return markAsRead(u, notificationId);
					}

					if("1".equals(markAllAsRead)) {
						return markAllAsRead(u);
					}
				}
				
				// primeiro busca as badges do usuario
				NotificationResponseVO r = new NotificationResponseVO();
				NotificationBadge badges = getBadges(u);
				r.badges = badges;
				
				// agora busca a notificacoes
				boolean getBadges = "1".equals(count);
				if(!getBadges) {
					boolean isSolicitacoes = "1".equals(tSolicitacoes.getValue());
					if(isSolicitacoes) {
						List<NotificationVO> solicitacoes = getSolicitacoes(u);
						r.solicitacoes = solicitacoes;
					} else {
						List<NotificationVO> notifications = getNotifications(u);
						r.list = notifications;
					}
				}
				
				r.loginLivecomOk = Livecom.getInstance().getUserInfo(u.getId()) != null;
				if(isWsVersion3()) {
					Response response = Response.ok("OK");
					response.notifications = r;
					return response;
				}
				
				return r;
			}
		}
		
		return new MensagemResult("NOK","Erro ao buscar notificações.");
	}

	private List<NotificationVO> getSolicitacoes(Usuario u) {
		if(maxRows == 0) {
			maxRows = 10;
		}

		Long novasNotificationId = tNovasNotificationsId.getLong();

		// Search
		NotificationSearch search = new NotificationSearch();
		search.page = page;
		search.maxRows = maxRows;
		search.user = u;
		search.texto = tTextoBusca.getValue();
		search.novasNotificacoes = novasNotificationId;
		search.naoLidas = false;
		search.dataInicial = StringUtils.isEmpty(tDataFinal.getValue()) ? null : tDataInicial.getDate();
		search.dataFinal = StringUtils.isEmpty(tDataFinal.getValue()) ? null : tDataFinal.getDate();
		List<NotificationUsuario> notifications = notificationService.findSolicitacoes(search);
		List<NotificationVO> solicitacoes = new ArrayList<NotificationVO>();
		if(notifications != null) {
			for (NotificationUsuario n : notifications) {
				NotificationVO vo = new NotificationVO();
				vo.copyFrom(n);
				solicitacoes.add(vo);
			}
		}

		return solicitacoes;
	}
	
	private List<NotificationVO> getNotifications(Usuario u) {
		if(maxRows == 0) {
			maxRows = 10;
		}

		Long novasNotificationId = tNovasNotificationsId.getLong();
		// Search
		NotificationSearch search = new NotificationSearch();
		search.page = page;
		search.maxRows = maxRows;
		search.tipo = tipo;
		search.user = u;
		search.texto = tTextoBusca.getValue();
		search.novasNotificacoes = novasNotificationId;
		search.naoLidas = "2".equals(list);
		search.lidas = "3".equals(list);
		search.dataInicial = StringUtils.isEmpty(tDataFinal.getValue()) ? null : tDataInicial.getDate();
		search.dataFinal = StringUtils.isEmpty(tDataFinal.getValue()) ? null : tDataFinal.getDate();

		List<NotificationUsuario> notifications;
		Boolean agruparOn = ParametrosMap.getInstance(u.getEmpresa()).getBoolean(Params.SIDEBOX_NOTIFICATION_AGRUPAR_ON, false);
		List<NotificationVO> notificacoes = new ArrayList<NotificationVO>();
		
		if(agruparOn) {
			notifications = notificationService.findAllAgrupada(search);
			
			if(notifications != null) {
				for (NotificationUsuario n : notifications) {
					NotificationVO vo = new NotificationVO();
					vo.copyFrom(n);
					List<UsuarioSimpleVO> users = vo.getUsuarios();
					
					if(!n.isLida() && !n.getNotification().getTipo().equals(TipoNotificacao.NEW_POST)) {
						List<Usuario> notRead = notificationService.findNaoLidasByNotification(n.getNotification(), search);
						for (Usuario usuario : notRead) {
							
							if(usuario != null) { 
								
								if(vo.idUsuario != null && !vo.idUsuario.equals(usuario.getId())) {
									UsuarioSimpleVO user = new UsuarioSimpleVO();
									user.setUsuario(usuario);
									users.add(user);
								}
							}
						}
						
						if(users.size() > 0) {
							vo.setUsuarios(users);
						}
					}
					
					notificacoes.add(vo);
				}
			}
			
		} else {
			
			notifications = notificationService.findAll(search);
			if(notifications != null) {
				for (NotificationUsuario n : notifications) {
					NotificationVO vo = new NotificationVO();
					vo.copyFrom(n);
					notificacoes.add(vo);
				}
			}
		}
		
		return notificacoes;
	}

	private Object markAllAsRead(Usuario u) throws IOException {
		int count = notificationService.markAllAsRead(u);
		
		Long id = u.getId();
		if(AkkaHelper.isOnline(id)) {
			AkkaHelper.sendNotificationReadAll(id);
		} else {
			//send push read all
			if(StringUtils.equals(UserAgentUtil.getUserAgentSO(getContext()), "web")) {
				pushLivecomService.readAllPushs(u);
			}
		}
		
		if(isWsVersion3()) {
			Response r = Response.ok(count + " notificações marcadas como lida.");
			return r;
		}
		return new MensagemResult("OK" ,count + " notificações marcadas como lida.");
	}

	private Object markAsRead(Usuario u, Long id) throws DomainException {
		NotificationUsuario notUser = notificationService.getNotificationUsuario(id);
		if(notUser == null) {
			return new MensagemResult("NOK" ,"Notificação não encontrada");
		}
		
		if(!notUser.getUsuario().equals(u)) {
			return new MensagemResult("NOK" ,"Permissão negada");
		}
		
		Notification n = notUser.getNotification();
		TipoNotificacao type = n.getTipo();
		Post post = n.getPost();

		if(type.equals(TipoNotificacao.GRUPO_SOLICITAR_PARTICIPAR) || type.equals(TipoNotificacao.USUARIO_SOLICITAR_AMIZADE)) {
			return new MensagemResult("NOK" ,"Solicitações não podem ser marcadas como lida");
		}
		
		if(post == null) {
			post = n.getComentario() != null ? n.getComentario().getPost() : null;
		}
		
		notificationService.readAgrupada(notUser, post, type);

        if(post != null) {
        	AkkaHelper.sendNotificationWasRead(u.getId(), post.getId());
        }

        log("Notification ["+n+"] marcada como lida");
		System.err.println("markAsRead - Notification ["+n+"] marcada como lida");
		
		if(isWsVersion3()) {
			Response r = Response.ok("Notificação marcada como lida.");
			return r;
		}

		return new MensagemResult("OK" ,"Notificação marcada como lida.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("notification", NotificationResponseVO.class);
		x.alias("notification", NotificationVO.class);
		x.alias("badges", NotificationBadge.class);
		x.alias("chatBadges", BadgeChatVO.class);
		super.xstream(x);
	}
	
	@Override
	protected boolean isResponseGSON() {
		return true;
	}
}
