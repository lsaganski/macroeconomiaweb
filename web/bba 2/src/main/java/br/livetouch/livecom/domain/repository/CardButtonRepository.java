package br.livetouch.livecom.domain.repository;

import br.livetouch.livecom.domain.CardButton;
import net.livetouch.tiger.ddd.repository.Repository;

public interface CardButtonRepository extends Repository<CardButton>{

}
