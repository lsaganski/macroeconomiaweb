package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.EmpresaRepository;
import br.livetouch.livecom.domain.vo.Filtro;

@Repository
public class EmpresaRepositoryImpl extends LivecomRepository<Empresa> implements EmpresaRepository {

	public EmpresaRepositoryImpl() {
		super(Empresa.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Empresa> filterEmpresa(Filtro filtroEmpresa) {
		StringBuffer sb = new StringBuffer("FROM Empresa e where 1=1 ");
		
		if(filtroEmpresa == null) {
			return createQuery(sb.toString()).list();
		}
		if(filtroEmpresa.nome != null) {
			sb.append(" AND lower(e.nome) like :nome");
		}
		
		Query q = createQuery(sb.toString());
		if(filtroEmpresa.nome != null) {
			q.setParameter("nome", "%" + StringUtils.lowerCase(filtroEmpresa.nome) +"%");
		}
		
		setCache(q);
		
		return q.list();
	}

	@Override
	public Empresa findDominio(String dominio) {
		Query q = createQuery("FROM Empresa e WHERE e.dominio like :dominio");
		q.setParameter("dominio", "%" + dominio + "%");
		setCache(q);
		Empresa e = getEntityFromQuery(q);
		if(e == null && dominio.contains("http")) {
			dominio = dominio.replaceFirst("^(http://www\\.|http://|www\\.)","");
			if(StringUtils.isNotEmpty(dominio)) {
				q = createQuery("FROM Empresa e WHERE e.dominio like :dominio");
				q.setParameter("dominio", "%" + dominio + "%");
				setCache(q);
				e = getEntityFromQuery(q);
			}
		}
		return e;
	}
	
	@Override
	public Empresa findTrial(String codigo) {
		Query q = createQuery("FROM Empresa e WHERE e.trialCode like :codigo");
		q.setParameter("codigo", codigo);
		setCache(q);
		Empresa e = getEntityFromQuery(q);
		return e;
	}

	@Override
	public Empresa findByName(Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Empresa e where e.nome = :nome");
		
		if(empresa.getId() != null) {
			sb.append(" and e.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("nome", empresa.getNome());
		
		if(empresa.getId() != null) {
			q.setParameter("id", empresa.getId());
		}
		
		setCache(q);

		Empresa e = getEntityFromQuery(q);
		return e;
	}

	@Override
	public Empresa findByAdmin(Usuario u) {
		List<Empresa> list = query("from Empresa where admin.id=?",true,u.getId());
		return list.isEmpty() ? null : list.get(0);
	}
}