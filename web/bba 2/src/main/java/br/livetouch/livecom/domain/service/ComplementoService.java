package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Complemento;
import br.livetouch.livecom.domain.Empresa;
import net.livetouch.tiger.ddd.DomainException;

public interface ComplementoService extends Service<Complemento> {

	List<Complemento> findAll(Empresa empresa);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Complemento f, Empresa empresa) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Complemento f) throws DomainException;

	String csvComplemento(Empresa empresa);

}
