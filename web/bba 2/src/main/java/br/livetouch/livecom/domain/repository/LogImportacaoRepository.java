package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LogImportacao;
import br.livetouch.livecom.domain.vo.LogImportacaoFiltro;

@Repository
public interface LogImportacaoRepository extends net.livetouch.tiger.ddd.repository.Repository<LogImportacao> {

	List<String> findAllTiposCsv(Empresa empresa);

	List<LogImportacao> findAllByFiltro(LogImportacaoFiltro filtro, Empresa empresa);

	long countByFiltro(LogImportacaoFiltro filtro, Empresa empresa);

	List<LogImportacao> findAll(Empresa empresa);
	
	
}