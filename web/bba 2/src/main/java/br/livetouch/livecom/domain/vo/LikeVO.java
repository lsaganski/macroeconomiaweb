package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Likes;
import br.livetouch.livecom.utils.DateUtils;

public class LikeVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public long userId;
	public String urlFotoUsuario;
	public String userNome;
	public String userCargo;
	public String userLogin;
	public String postTitulo;
	public String comentario;
	public long postId;
	public boolean lidaNotification;
	public String favorito;
	public Long comentarioId;
	public String data;

	public void setLikes(Likes f) {
		this.userId = f.getUsuario().getId();
		this.setUserNome(f.getUsuario().getNome());
		this.userCargo = f.getUsuario().getCargo();
		this.userLogin = f.getUsuario().getLogin();
		this.urlFotoUsuario = f.getUsuario().getUrlFoto();
		if (f.getPost() != null) {
			this.postId = f.getPost().getId();
			this.setPostTitulo(f.getPost().getTitulo());
		}
		if (f.getComentario() != null) {
			this.comentarioId = f.getComentario().getId();
			this.setComentario(f.getComentario().getComentario());
		}
		this.favorito = f.isFavorito() ? "1" : "0";
		this.lidaNotification = f.getLidaNotification();
		this.setData(DateUtils.toString(f.getData(), "dd/MM/yyyy HH:mm"));
	}

	public static List<LikeVO> toListVO(List<Likes> likes) {
		List<LikeVO> likesVO = new ArrayList<LikeVO>();
		for (Likes l : likes) {
			LikeVO vo = new LikeVO();
			vo.setLikes(l);
			likesVO.add(vo);
		}
		return likesVO;
	}

	@Override
	public String toString() {
		return "LikeVO [userId=" + userId + ", postId=" + postId + ", favorito=" + favorito + "]";
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getUserNome() {
		return userNome;
	}

	public void setUserNome(String userNome) {
		this.userNome = userNome;
	}

	public String getPostTitulo() {
		return postTitulo;
	}

	public void setPostTitulo(String postTitulo) {
		this.postTitulo = postTitulo;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
}
