package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.enums.TipoTemplate;
import br.livetouch.livecom.domain.repository.TemplateEmailRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class TemplateEmailRepositoryImpl extends StringHibernateRepository<TemplateEmail> implements TemplateEmailRepository {

	public TemplateEmailRepositoryImpl() {
		super(TemplateEmail.class);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TemplateEmail> findAll(Empresa empresa) {
		StringBuffer sb = new StringBuffer("FROM TemplateEmail t WHERE t.empresa.id = :empresaId");
		Query q = createQuery(sb.toString());
		q.setParameter("empresaId", empresa.getId());
		q.setCacheable(true);
		return q.list();
	}
	
	@Override
	public TemplateEmail findByType(Empresa empresa, TipoTemplate tipo) {
		StringBuffer sb = new StringBuffer("FROM TemplateEmail t WHERE t.empresa.id = :empresaId and t.tipoTemplate = :tipo");
		Query q = createQuery(sb.toString());
		q.setParameter("empresaId", empresa.getId());
		q.setParameter("tipo", tipo);
		q.setCacheable(true);
		return q.list().size() > 0 ? (TemplateEmail) q.list().get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TemplateEmail> findByTipo(Empresa empresa, TipoTemplate tipo) {
		StringBuffer sb = new StringBuffer("FROM TemplateEmail t WHERE t.empresa.id = :empresaId and t.tipoTemplate = :tipo");
		Query q = createQuery(sb.toString());
		q.setParameter("empresaId", empresa.getId());
		q.setParameter("tipo", tipo);
		q.setCacheable(true);
		return q.list();
	}

	@Override
	public TemplateEmail existe(Empresa empresa, TemplateEmail templateEmail) {
		
		TipoTemplate tipo = templateEmail.getTipoTemplate();
		
		StringBuffer sb = new StringBuffer("FROM TemplateEmail t WHERE 1=1");
		
		if(empresa != null) {
			sb.append(" and t.empresa.id = :empresaId");
		}
		
		if(tipo != null) {
			sb.append(" and (t.tipoTemplate = :tipo and t.tipoTemplate != :mkt and t.tipoTemplate != :convite)");
		}
		
		if(templateEmail.getId() != null) {
			sb.append(" and t.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		if(empresa != null) {
			q.setParameter("empresaId", empresa.getId());
		}
		
		if(tipo != null) {
			q.setParameter("tipo", templateEmail.getTipoTemplate());
			q.setParameter("mkt", TipoTemplate.emailMarketing);
			q.setParameter("convite", TipoTemplate.convite);
		}
		
		if(templateEmail.getId() != null) {
			q.setParameter("id", templateEmail.getId());
		}
			
		q.setCacheable(true);
		return q.list().size() > 0 ? (TemplateEmail) q.list().get(0) : null;
	}

}