package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Rate;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.RateRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class RateRepositoryImpl extends StringHibernateRepository<Rate> implements RateRepository {

	public RateRepositoryImpl() {
		super(Rate.class);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Rate> findAllByUser(Usuario u) {
		StringBuffer sb = new StringBuffer("from Rate h where h.usuario.id=?");

		Query q = createQuery(sb.toString());

		q.setLong(0, u.getId());
		List<Rate> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Rate findByUserPost(Usuario u, Post p) {
		StringBuffer sb = new StringBuffer("from Rate h where h.usuario.id=? and h.post.id=?");

		Query q = createQuery(sb.toString());
		
		q.setLong(0, u.getId());
		q.setLong(1, p.getId());
		List<Rate> list = q.list();
		Rate r = list.size() > 0 ? list.get(0) : null;
		return r;
	}

	@Override
	public int findMedia(Post p) {
		StringBuffer sb = new StringBuffer("select avg(r.value) from Rate r where r.post.id=?");

		Query q = createQuery(sb.toString());
		
		q.setLong(0, p.getId());
		
		Object o = q.uniqueResult();
		
		int i = 0;
		
		if(o != null) {
			i = (int) NumberUtils.toDouble(o.toString());
		}
		
		return i;
	}

	@Override
	public Long countByPost(Post p) {
		Criteria c = createCriteria();
		if(p != null) {
			c.add(Restrictions.eq("post.id", p.getId()));
		}
		c.setCacheable(true);
		c.setProjection(Projections.rowCount());
		long count = getCount(c);
		return count;
	}

}