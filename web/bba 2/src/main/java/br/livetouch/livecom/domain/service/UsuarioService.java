package br.livetouch.livecom.domain.service;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Amizade;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.UsuarioAcessoMural;
import br.livetouch.livecom.domain.enums.StatusAmizade;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.vo.AlterarSenhaVO;
import br.livetouch.livecom.domain.vo.ControleVersaoVO;
import br.livetouch.livecom.domain.vo.ExportarUsuariosFiltro;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVO;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioFuncionaisVO;
import br.livetouch.livecom.domain.vo.StatusAtivacaoVO;
import br.livetouch.livecom.domain.vo.UsuarioAutocompleteFiltro;
import br.livetouch.livecom.domain.vo.UsuarioStatusCountVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import net.livetouch.tiger.ddd.DomainException;

public interface UsuarioService extends br.livetouch.livecom.domain.service.Service<Usuario> {

	Usuario load(Long id);

	List<Usuario> findAll(Empresa empresa);

	List<Long> findAllIds(Empresa empresa);

	List<Usuario> findAll(Usuario user, int page, int max);

	@Transactional(rollbackFor = Exception.class)
	void delete(Usuario userInfo, Usuario c) throws DomainException;
	
	/**
	 * Force para deletar mesmo se for admin.
	 * 
	 * Acontece ao excluir empresa
	 * 
	 * @param userInfo
	 * @param u
	 * @param force
	 * @throws DomainException
	 */
	@Transactional(rollbackFor = Exception.class)
	void delete(Usuario userInfo, Usuario u, boolean force) throws DomainException;

	Usuario findByLogin(String login, Empresa empresa );

	@Transactional(rollbackFor = Exception.class)
	Usuario findByLogin(String login);

	Usuario findByEmail(String email);

	/**
	 * @param usuario
	 *            Usuario para salvar
	 * @param usuarioLogado
	 *            Usuario que esta utilizando o sistema. Seu login fica salvo
	 *            nos logs
	 * 
	 * @throws DomainException
	 */
	@Transactional(rollbackFor = Exception.class)
	boolean saveOrUpdate(Usuario userInfo, Usuario usuario) throws DomainException;
	
	@Transactional(rollbackFor = Exception.class)
	boolean saveAudit(Usuario userInfo, Usuario usuario) throws DomainException;

	@Transactional(rollbackFor = Exception.class)
	String createPassword(Usuario usuario) throws DomainException;

	List<Usuario> findByFilter(Usuario userIInfo, Usuario filtro, int page, int pageSize);

	List<Usuario> findByFiltro(UsuarioAutocompleteFiltro filtro, Empresa empresa);

	long getCountByFilter(Usuario userIInfo, Usuario filtro);

	/**
	 * Altera a senha do usuario root
	 * 
	 * @param usuario
	 * @param alterar
	 * @throws DomainException
	 */
	@Transactional(rollbackFor = Exception.class)
	void alterarSenha(Usuario usuario, AlterarSenhaVO alterar) throws DomainException;

	@Transactional(rollbackFor = Exception.class)
	void esqueciSenha(Usuario usuario, String novaSenha, String confirmaNovaSenha) throws DomainException;

	Usuario login(String login, String senha) throws DomainException;

	Usuario login(String login, String senha, Empresa empresa, boolean isMobile) throws DomainException;

	long count();

	List<Usuario> findAllByIds(List<Long> usersIds);

	@Transactional(rollbackFor = Exception.class)
	void gerarChave(Usuario u) throws DomainException;

	List<StatusAtivacaoVO> findStatusAtivacao(Empresa empresa);

	UsuarioAcessoMural findUsuarioAcessoMural(Usuario u);

	@Transactional(rollbackFor = Exception.class)
	UsuarioAcessoMural saveUsuarioAcessoMural(Usuario u, Post lastPost);

	@Transactional(rollbackFor = Exception.class)
	void saveOrUpdate(GrupoUsuarios gu);

	@Transactional(rollbackFor = Exception.class)
	void saveOrUpdate(Amizade a);

	@Transactional(rollbackFor = Exception.class)
	void saveGruposToUser(Usuario u, List<Grupo> grupos);

	@Transactional(rollbackFor = Exception.class)
	void saveGruposToUser(Usuario u, Set<GrupoUsuarios> grupos, List<Grupo> list);

	@Transactional(rollbackFor = Exception.class)
	GrupoUsuarios addGrupoToUser(Usuario u, Grupo g, boolean postar);

	@Transactional(rollbackFor = Exception.class)
	GrupoUsuarios addSubadmin(Usuario u, Grupo g, boolean admin);

	@Transactional(rollbackFor = Exception.class)
	GrupoUsuarios addGrupoToUserSemSalvar(Usuario u, Grupo g, boolean postar);

	@Transactional(rollbackFor = Exception.class)
	void saveUsersToGrupo(Usuario userInfo, Grupo g, List<Usuario> users) throws DomainException;

	List<Grupo> getGrupos(Usuario u);

	List<Grupo> getGrupos(Usuario u, boolean postar);

	@Transactional(rollbackFor = Exception.class)
	void deleteGrupoUsuarios(Grupo grupo, Usuario usuario);

	@Transactional(rollbackFor = Exception.class)
	void deleteGrupoUsuarios(GrupoUsuarios grupoUsuarios);

	@Transactional(rollbackFor = Exception.class)
	void deleteGrupoUsuarios(Usuario usuario, List<Grupo> grupos);
	
	@Transactional(rollbackFor = Exception.class)
	void deleteAmizade(Usuario usuario, Usuario amigo);

	GrupoUsuarios getGrupoUsuario(Usuario usuario, Grupo grupo);

	Amizade getAmizade(Usuario usuario, Usuario amigo);

	List<Usuario> findUsuariosByGrupo(Grupo grupo, int page, int maxSize);

	@Deprecated
	List<Usuario> findAllByStatusAtivacaoByGrupo(Grupo g, int page, Integer maxRows, String status);

	List<Usuario> findByStatus(Grupo g, StatusUsuario status, Empresa empresa, int page, Integer maxRows);

	@Transactional(rollbackFor = Exception.class)
	void deleteAllUsersByTipoImportacao();

	List<String> findLogins();

	boolean isUsuarioDentroDoGrupo(Usuario user, Grupo grupo);

	void insertGrupoUsuario(Long userId, Long grupoId) throws SQLException;

	Long insertUsuarioImportacao(String login, String senha, String dataNasc, Long permissaoId, Long grupoId) throws SQLException;

	List<Object[]> findIdLoginsTipoImportacao();

	void deleteById(Long id);

	List<RelatorioFuncionaisVO> getFuncionais(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException;

	long getCountByFilter(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException;

	List<RelatorioAcessosVO> findbyFilter(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException;

	List<RelatorioAcessosVersaoVO> findAcessosByVersao(RelatorioFiltro filtro) throws DomainException;

	long getCountVersaoByFilter(RelatorioFiltro filtro) throws DomainException;

	List<Object[]> findIdLoginsUsuario();

	List<Usuario> findByNomeLikeLogin(UsuarioAutocompleteFiltro filtro, Empresa empresa);

	String csvUsuarios(Empresa empresa);

	String csvUsuariosByFilter(ExportarUsuariosFiltro filtro, Empresa empresa);

	List<Usuario> findAllByFilter(ExportarUsuariosFiltro filtro, Empresa empresa);

	List<String> findLoginsByIds(List<Long> ids);

	Long count(Empresa empresa);
	
	long getCountByNomeLike(UsuarioAutocompleteFiltro filtro, Empresa empresa);

	boolean isUsuarioDentroHorario(Usuario u);

	void deleteUserToGrupo(Grupo g);

	void revemoUsersToGrupo(Usuario userInfo, Grupo grupo, List<Usuario> users) throws DomainException;

	void recuperarSenha(Empresa empresa, Usuario userInfo, Usuario u) throws DomainException;

	void alterarSenha(Empresa empresa, Usuario userInfo, Usuario u, String senha) throws DomainException;

	void setNullFuncaoByFuncoes(List<Funcao> funcoes);

	List<Usuario> findByFuncoes(List<Funcao> funcoes);

	/**
	 * Recebe lista de ids de usuarios e retorna apenas usuarios dentro do horário.
	 * 
	 * @param idsToFilter
	 * @return
	 */
	List<Long> findAllUsuarioIdsDentroHorario(List<Long> idsToFilter);

	boolean existsByLogin(Usuario u, Empresa empresa);

	boolean existsByEmail(Usuario u, Empresa empresa);

	Long getCountFindByStatus(Grupo g, StatusUsuario statusAtivacao, Empresa empresa);

	void convidarUsuarios(Empresa empresa, Usuario userInfo, String contextPath, List<Long> usersId, TemplateEmail template, String assunto);

	UsuarioStatusCountVO getCountUsuariosStatus(Empresa empresa);

	List<Usuario> findNovos(Empresa empresa);

	List<Usuario> findInativos(Empresa empresa);

	List<Usuario> findAtivos(Empresa empresa);

	/**
	 * Coloca este usuario dentro deste grupo
	 * @param go
	 * @throws DomainException 
	 */
	@Transactional(rollbackFor = Exception.class)
	void addGrupo(Usuario userInfo, Usuario u,Grupo go) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	Amizade solicitar(Usuario usuario, Usuario amigo, boolean solicitacao) throws DomainException;
	
	void criarNotificacao(Usuario usuario, Usuario amigo, TipoNotificacao tipo) throws DomainException;
	void criarNotificacao(Amizade amizade, TipoNotificacao tipo) throws DomainException;
	
	List<UsuarioVO> findUsuariosByStatusAmizade(StatusAmizade status, Usuario u);

	List<UsuarioVO> findByStatusAmizade(Usuario usuario);

	List<UsuarioVO> findByAmigos(Usuario usuario, String nome);

	List<Usuario> findAmigos(Usuario usuario);

	List<Long> findIdsAmigos(Usuario usuario);

	List<UsuarioVO> findByPendentes(Usuario usuario);
	
	List<Long> findUsuariosToPush(Post p, List<Long> idsAmigos, Idioma idioma);
	
	@Transactional
	void saveOrUpdate(Usuario usuario);

	ControleVersaoVO validateVersion(Long empresaId, String appVersion, Integer appVersionCode, String userAgentSO);

	@Transactional
	Date updateDataLastChat(Long userId);

	void updateAllDataSenha(Long empresaId);

	void alterarSenhaExpirada(Usuario usuario, AlterarSenhaVO alterar) throws DomainException;

}