/**
 * 
 */
package br.livetouch.livecom.domain.enums;

public enum GrupoNotification {
	MURAL("MURAL"),
	AMIZADE("AMIZADE"),
	GRUPO("GRUPO");
	private final String desc;

	GrupoNotification(String s){
		this.desc = s;
	}

	@Override
	public String toString() {
		return desc != null ? desc : "?";
	}
	
	public String getDesc() {
		return desc;
	}
}