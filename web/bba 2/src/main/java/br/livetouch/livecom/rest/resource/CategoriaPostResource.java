package br.livetouch.livecom.rest.resource;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.infra.util.Utils;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.CategoriaVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import br.livetouch.livecom.rest.request.CategoriaRequest;
import io.swagger.annotations.ApiOperation;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/categoria")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class CategoriaPostResource extends MainResource {

	protected static final Logger log = Log.getLogger(CategoriaPostResource.class);

	@GET
	@Path("/autoComplete")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public List<CategoriaVO> getPosts(@QueryParam("nome") String nome, @QueryParam("max") int max) throws DomainException {
		List<CategoriaPost> list = categoriaPostService.findAllByNomeLikeWithMax(nome, max, getEmpresa());
		List<CategoriaVO> vos = CategoriaVO.create(list);
		return vos;
	}
	
	@POST
	public Response create(CategoriaRequest request) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		if(request == null) {
			return Response.ok(MessageResult.error("Categoria invalida")).build();
		}
		
		CategoriaPost c = request.toCategoria();
		
		//Validar se ja existe uma categoria com o mesmo nome
		boolean existe = categoriaPostService.findByName(c, getEmpresa()) != null;
		if(existe) {
			log.debug("Ja existe uma categoria com este nome");
			return Response.ok(MessageResult.error("Ja existe uma categoria com este nome")).build();
		}
		
		//Validar se ja existe uma categoria com o mesmo codigo
		existe = categoriaPostService.findByCodigo(getEmpresa(), c.getCodigo()) != null;
		if(existe && !c.getCodigo().equals("")) {
			log.debug("Ja existe uma categoria com este nome");
			return Response.ok(MessageResult.error("Ja existe uma categoria com este codigo")).build();
		}
		
		categoriaPostService.saveOrUpdate(getUserInfo(), c);
		if(c.getCategoriaParent() != null) {
			c.setCategoriaParent(categoriaPostService.get(c.getCategoriaParent().getId()));
		}
		
		String arquivos_ids = request.arquivo_ids;
		List<Long> arquivosIds = Utils.getIds(arquivos_ids.split(","));
		if(arquivos_ids != null && arquivosIds.size() > 0) {
			Set<Arquivo> arquivos = new HashSet<Arquivo>();
			for (Long id : arquivosIds) {
				Arquivo arquivo = arquivoService.get(id);
				if(arquivos != null) {
					arquivo.setCategoria(c);
					arquivos.add(arquivo);
				}
			}
			c.setArquivos(arquivos);
		}
		
		categoriaPostService.saveOrUpdate(getUserInfo(), c);
		return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", new CategoriaVO(c))).build();

	}
	
	@PUT
	public Response update(CategoriaRequest request) throws DomainException  {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		if(request == null || request.id == null) {
			return Response.ok(MessageResult.error("Categoria invalida")).build();
		}
		
		CategoriaPost categoria = request.toCategoria();
		
		//Validar se ja existe uma categoria com o mesmo nome
		boolean existe = categoriaPostService.findByName(categoria, getEmpresa()) != null;
		if(existe) {
			log.debug("Ja existe uma categoria com este nome");
			return Response.ok(MessageResult.error("Ja existe uma categoria com este nome")).build();
		}
		
		String cor = categoria.getCor().replace("#", "");
		categoria.setCor(cor);
		
		String arquivos_ids = request.arquivo_ids;
		List<Long> arquivosIds = Utils.getIds(arquivos_ids.split(","));
		arquivoService.deleteNotInCategorias(arquivosIds, categoria);
		
		if(arquivos_ids != null && arquivosIds.size() > 0) {
			Set<Arquivo> arquivos = new HashSet<Arquivo>();
			for (Long id : arquivosIds) {
				Arquivo arquivo = arquivoService.get(id);
				if(arquivos != null) {
					arquivo.setCategoria(categoria);
					arquivos.add(arquivo);
				}
			}
			categoria.setArquivos(arquivos);
		}
		
		
		categoriaPostService.saveOrUpdate(getUserInfo(), categoria);
		CategoriaVO vo = new CategoriaVO(categoria);
		CategoriaPost categoriaPai = categoria.getCategoriaParent();
		if(categoriaPai != null) {
			categoriaPai = categoriaPostService.get(categoriaPai.getId());
			CategoriaVO categoriaPaiVO = new CategoriaVO(categoriaPai);
			vo.setCategoriaPai(categoriaPaiVO);
		}
		
		return Response.ok(MessageResult.ok("Categoria atualizada com sucesso", vo)).build();

	}
	
	@GET
	public Response findAll() throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Idioma idioma = getUserInfo().getIdioma();
		List<CategoriaPost> categorias = categoriaPostService.findAll(getEmpresa());
		
		List<CategoriaVO> vos = CategoriaVO.create(categorias, idioma);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) { 
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		CategoriaPost categoria = categoriaPostService.get(id);
		if(categoria == null) {
			return Response.ok(MessageResult.error("Categoria não encontrada")).build();
		}
		
		Idioma idioma = getUserInfo().getIdioma();
		
		CategoriaVO vo = new CategoriaVO(categoria, idioma);
		System.out.println("arquivos vo original: " + vo.getArquivos().size());
		if(categoria.getCategoriaParent() != null) {
			CategoriaVO pai = new CategoriaVO(categoria.getCategoriaParent(), idioma);
			vo.setCategoriaPai(pai);
		}
		System.out.println("arquivos vo depois pai: " + vo.getArquivos().size());
		return Response.ok().entity(vo).build();
	}

	@GET
	@Path("/pai/{id}")
	public Response findByPai(@PathParam("id") Long id) {
		
		CategoriaPost pai = categoriaPostService.get(id);
		if(pai == null) {
			return Response.ok(MessageResult.error("Categoria pai não encontrada")).build();
		}
		
		List<CategoriaPost> categorias = categoriaPostService.findByParent(pai);
		
		Usuario user = getUserInfo();
		
		List<CategoriaVO> vos = CategoriaVO.createTree(categorias, user.getIdioma());
		return Response.ok().entity(vos).build();
		
	}
	
	@POST
	@Path("/pai")
	public Response findPais(Idioma i) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}

		if(i != null && i.getId() != null) {
			i = idiomaService.get(i.getId());
		}
		
		List<CategoriaPost> categorias = categoriaPostService.findAllParentByIdioma(getEmpresa(), i);
		List<CategoriaVO> vos = CategoriaVO.createTree(categorias, i);
		return Response.ok().entity(vos).build();
	}

	@POST
	@Path("/pai/{id}")
	public Response findPai(@PathParam("id") Long id, Idioma i) throws DomainException {
		CategoriaPost pai = categoriaPostService.get(id);
		if(pai == null) {
			return Response.ok(MessageResult.error("Categoria pai não encontrada")).build();
		}
		
		List<CategoriaPost> categorias = categoriaPostService.findByParent(pai);
		
		List<CategoriaVO> vos = CategoriaVO.create(categorias, i);
		return Response.ok().entity(vos).build();
	}

	@POST
	@Path("/pai/idiomas")
	public Response findPaiByIdiomas(List<Idioma> idiomas) throws DomainException {
		List<CategoriaPost> categorias = categoriaPostService.findParentByIdiomas(idiomas);
		
		List<CategoriaVO> vos = CategoriaVO.createTree(categorias, null);
		return Response.ok().entity(vos).build();
	}
	
	@POST
	@Path("/pai/idiomas/{id}")
	public Response findByPaiAndIdiomas(@PathParam("id") Long id, List<Idioma> idiomas) throws DomainException {
		CategoriaPost pai = categoriaPostService.get(id);
		if(pai == null) {
			return Response.ok(MessageResult.error("Categoria pai não encontrada")).build();
		}
		
		List<CategoriaPost> categorias = categoriaPostService.findByParentAndIdiomas(pai, idiomas);
		
		List<CategoriaVO> vos = CategoriaVO.createTree(categorias, null);
		return Response.ok().entity(vos).build();
	}
	
	@POST
	@Path("/idiomas/{nome}")
	public Response findByPaisAndIdiomas(@PathParam("nome") String nome, Idioma idioma) throws DomainException {
		
		List<CategoriaPost> categorias = categoriaPostService.findByNameAndIdioma(nome, idioma, getEmpresa());
		if(categorias == null) {
			return Response.ok(MessageResult.error("Categorias não encontrada")).build();
		}
		
		List<CategoriaVO> vos = CategoriaVO.createTree(categorias, idioma);
		return Response.ok().entity(vos).build();
	}
	
	@POST
	@Path("/idioma/{id}")
	public Response findByIdioma(@PathParam("id") Long id, List<Long> categoriaIds) throws DomainException {
		
		Idioma idioma = idiomaService.get(id);
		if(idioma == null) {
			return Response.ok(MessageResult.error("Idioma não encontrado")).build();
		}
		
		List<CategoriaPost> categorias = categoriaPostService.findByIdsAndIdioma(idioma, categoriaIds);
		if(categorias == null) {
			return Response.ok(MessageResult.error("Categorias não encontrada")).build();
		}
		
		List<CategoriaVO> vos = CategoriaVO.createTree(categorias, idioma);
		return Response.ok().entity(vos).build();
	}

	@POST
	@Path("/idioma")
	public Response findByIdioma(List<Idioma> idiomas) throws DomainException {
		
		List<CategoriaPost> categorias = categoriaPostService.findAllParentByIdioma(getEmpresa(), idiomas);
		List<CategoriaVO> vos = CategoriaVO.create(categorias);
		return Response.ok().entity(vos).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		CategoriaPost categoria = categoriaPostService.get(id);
		if(categoria == null) {
			return Response.ok(MessageResult.error("Categoria não encontrada")).build();
		}
		categoriaPostService.delete(getUserInfo(), categoria);
		return Response.ok().entity(MessageResult.ok("Categoria deletada com sucesso")).build();
	}
	
	@GET
	@Path("/exportar")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response exportar() throws IOException, DomainException {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Empresa empresa = getEmpresa();
		String csv = categoriaPostService.csvCategoria(empresa);
		
		String charset = ParametrosMap.getInstance(empresa).get(Params.EXPORTACAO_ARQUIVO_CHARSET, "ISO-8859-1");

		String attachament = "attachment; filename=" + URLEncoder.encode("categorias.csv", "UTF-8");
		ResponseBuilder b = Response.status(Status.OK);
		b.encoding(charset);
		b.header("Content-Disposition", attachament);
		b.header("Content-Type", "application/octet-stream"+";charset="+charset);
		b.entity(csv);
		Response r = b.build();
		return r;
	}
	
	@POST
	@Path("/ordem")
	@ApiOperation(value = "salva/atualiza ordem dos capitulos.", notes = "WS salvar ordem.")
	public Response post(List<CategoriaVO> vos) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			for (CategoriaVO vo : vos) {
				CategoriaPost categoria = categoriaPostService.get(vo.getId());
				categoria.setOrdem(vo.getOrdem());
				categoriaPostService.saveOrUpdate(getUserInfo(), categoria);
			}
			Response response = Response.ok(MessageResult.ok("Ordem de [" + vos.size() + "] categorias(s) atualizada com sucesso.")).build();
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			String erro = "Não foi possível salvar a ordem das categorias.";
			log.error(erro , e);
			return Response.ok(MessageResult.error(erro)).build();
		}
	}	
	
}
