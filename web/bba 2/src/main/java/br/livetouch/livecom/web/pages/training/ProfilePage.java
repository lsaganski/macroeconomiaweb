package br.livetouch.livecom.web.pages.training;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class ProfilePage extends TrainingLogadoPage {
	
	public Form form = new Form();
	public Usuario usuario;
	
	@Override
	public void onInit() {
		super.onInit();
		TextField t;
		form.add(t = new TextField("nome",true));
		form.add(new TextField("login",true));

		t.setFocus(true);

		form.add(new Submit("salvar",this,"salvar"));

		TrainingUtil.setTrainingStyle(form);
		setAutoCompleteOff(form);
	}
	
	@Override
	public void onGet() {
		super.onGet();
		
		usuario = getUsuario();
		if(usuario != null) {
			form.copyFrom(usuario);
		}
	}
	
	@Override
	public void onPost() {
		super.onPost();
	}

	public boolean salvar() {
		if(form.isValid()) {


			String nome = getParam("nome");
			String login = getParam("login");

			if(StringUtils.isBlank(nome) || StringUtils.isBlank(login)) {
				setMessageError("Digite todos os campos");
				return true;
			}

			usuario = getUsuario();

			form.copyTo(usuario);

			try {
				if(!usuario.isValidNomeSobrenome()) {
					form.setError("Informe o nome e sobrenome");
					return false;
				}

				usuarioService.saveOrUpdate(getUsuario(),usuario);

				// User Info
				setUserInfo(usuario);
				
				msg = "Dados atualizados com sucesso";
			}
			catch (Exception e) {
				setMessageError(e);
			}

			return false;
		
		}
		return false;
	}

	protected void setUserInfo(Usuario usuario) {
		UserInfoVO userInfo = UserInfoVO.create(usuario);
		userInfo.addSession("web");
		UserInfoVO.setHttpSession(getContext(),userInfo);
	}
}
