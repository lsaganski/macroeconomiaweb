package br.livetouch.livecom.domain.repository;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Canal;

@Repository
public interface CanalRepository extends net.livetouch.tiger.ddd.repository.Repository<Canal> {
	
}