package br.livetouch.livecom.web.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.admin.LivecomPage;

@Controller
@Scope("prototype")
public class RoadmapChatPage extends LivecomPage {

	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
	}
	
	@Override
	public String getTemplate() {
		return getPath();
	}
	
}
