package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

import br.livetouch.livecom.domain.Grupo;

public class GrupoMensagemVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public String nome;
	
	public GrupoMensagemVO(Grupo gm) {
		this.id = gm.getId();
		this.nome = gm.getNome();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}
}
