package br.livetouch.livecom.web.pages.ws;

import java.util.List;
import java.util.Map;

import br.livetouch.livecom.domain.vo.AudienciaQuantidadeVO;
import br.livetouch.livecom.domain.vo.BadgeChatVO;
import br.livetouch.livecom.domain.vo.CategoriaVO;
import br.livetouch.livecom.domain.vo.ChapeuVO;
import br.livetouch.livecom.domain.vo.ChatStatusVO;
import br.livetouch.livecom.domain.vo.ComentarioVO;
import br.livetouch.livecom.domain.vo.ControleVersaoVO;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.GrupoUsuariosCountVO;
import br.livetouch.livecom.domain.vo.GrupoVO;
import br.livetouch.livecom.domain.vo.IdiomaVO;
import br.livetouch.livecom.domain.vo.LikeVO;
import br.livetouch.livecom.domain.vo.MensagemInfoVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.NotificationBadge;
import br.livetouch.livecom.domain.vo.NotificationChatVO;
import br.livetouch.livecom.domain.vo.NotificationResponseVO;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.livecom.domain.vo.RelatorioEnvioPostVO;
import br.livetouch.livecom.domain.vo.StatusMensagemUsuarioVO;
import br.livetouch.livecom.domain.vo.StatusPostVO;
import br.livetouch.livecom.domain.vo.TagVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.domain.vo.UsuariosVO;
import br.livetouch.livecom.utils.JSONUtils;
import br.livetouch.livecom.web.pages.ws.ValidateUserInfoPage.ValidateUserInfo;

public class Response {
	public String status;
	public String errorCode;
	public String mensagem;
	
	public Long count;

	public NotificationBadge badges;
	public List<UsuarioVO> users;
	public PostVO post;
	public List<PostVO> posts;
	public List<GrupoUsuariosCountVO> grupos;
	public List<GrupoVO> groups;
	public List<ComentarioVO> comentarios;
	public UsuarioVO user;
	public UsuariosVO usuarios;
	public ConversaVO conversa;
	public MensagemVO msg;
	public MensagemInfoVO msgInfo;
	public List<MensagemVO> mensagens;
	public NotificationResponseVO notifications;
	public BadgeChatVO chatBadges;
	public ComentarioVO comentario;
	public List<LikeVO> likes;
	public AudienciaQuantidadeVO audienciaQuantidade;
	public List<FileVO> listFiles;
	public TagVO tag;
	public List<TagVO> tags;
	public ChatStatusVO chatStatus;
	public Map<Long,Boolean> chatOnline;
	public List<CategoriaVO> categorias;
	public ValidateUserInfo validateUserInfo;
	public NotificationChatVO notificationsChat;
	public List<ChapeuVO> chapeus;
	public List<StatusMensagemUsuarioVO> statusMensagens;
	public ControleVersaoVO controleVersao;
	public List<StatusPostVO> statusPost;
	public List<IdiomaVO> idiomas;
	public List<RelatorioEnvioPostVO> relatorioPosts;

	public static Response ok() {
		Response m = new Response();
		m.status = "OK";
		return m;
	}
	
	public static Response ok(String msg) {
		Response m = new Response();
		m.status = "OK";
		m.mensagem = msg;
		return m;
	}
	
	public static Response error(String msg) {
		Response m = new Response();
		m.status = "ERROR";
		m.mensagem = msg;
		return m;
	}
	
	@Override
	public String toString() {
		return JSONUtils.toJSON(this);
	}
}
