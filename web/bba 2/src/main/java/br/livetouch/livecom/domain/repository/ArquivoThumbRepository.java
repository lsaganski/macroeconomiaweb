package br.livetouch.livecom.domain.repository;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.ArquivoThumb;

@Repository
public interface ArquivoThumbRepository extends net.livetouch.tiger.ddd.repository.Repository<ArquivoThumb> {

	
}