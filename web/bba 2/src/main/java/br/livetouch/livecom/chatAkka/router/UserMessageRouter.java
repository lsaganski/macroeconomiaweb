package br.livetouch.livecom.chatAkka.router;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ai.api.model.AIResponse;
import ai.api.model.ResponseMessage;
import ai.api.model.ResponseMessage.ResponsePayload;
import ai.api.model.ResponseMessage.ResponseSpeech;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.actor.UntypedActorContext;
import akka.routing.RoundRobinPool;
import br.infra.util.ApiAiHelper;
import br.infra.util.Log;
import br.livetouch.livecom.chatAkka.AkkaChatServer;
import br.livetouch.livecom.chatAkka.LivecomChatInterface;
import br.livetouch.livecom.chatAkka.actor.AkkaFactory;
import br.livetouch.livecom.chatAkka.actor.MainActor;
import br.livetouch.livecom.chatAkka.protocol.ChatUtils;
import br.livetouch.livecom.chatAkka.protocol.JsonRawMessage;
import br.livetouch.livecom.chatAkka.protocol.Toast;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.vo.ControleVersaoVO;
import br.livetouch.livecom.domain.vo.ConversaVO;
import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.UpdateStatusMensagemResponseVO;
import br.livetouch.livecom.domain.vo.UserInfoSession;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.livecom.utils.ListUtils;
import net.livetouch.tiger.ddd.DomainException;

/**
 * Recebe mensagens do TcpConnectionActor (Android e iOS) e WebConnectionActor
 * (WebSocket)
 * 
 * @author rlecheta
 *
 */
public class UserMessageRouter extends UntypedActor {

	private static Logger logError = Log.getLogger("chat_error");
	
	private static final int POOL_SIZE = 5;
	private static Logger log = Log.getLogger("UserMessageRouter");
	
	private static final ExecutorService executor = Executors.newSingleThreadExecutor();
    private static final ExecutorService executorMsg = Executors.newSingleThreadExecutor();

	private LivecomChatInterface getLivecomInterface() {
		LivecomChatInterface livecomInterface = AkkaChatServer.getLivecomInterface();
		if(livecomInterface == null) {
			throw new IllegalArgumentException("SpringError: LivecomChatInterface is null.");
		}
		return livecomInterface;
	}

	public static ActorRef create(UntypedActorContext context) {
//		http://doc.akka.io/docs/akka/snapshot/scala/routing.html
		Props create = Props.create(UserMessageRouter.class);
		ActorRef ref = context.actorOf(new RoundRobinPool(POOL_SIZE).props(create), "userMessageRouter");
		return ref;
	}

	@Override
	public void preStart() throws Exception {
	}

	@Override
	public void onReceive(Object msg) throws Exception {
//		System.err.println("UserMessageRouter.onReceive, msg: " + msg);

		if (msg instanceof Toast) {
			Toast toast = (Toast) msg;
			toast(toast.getActor(), toast.getMsg());
		}
		else if (msg instanceof JsonRawMessage) {

			JsonRawMessage raw = (JsonRawMessage) msg;
            ChatVO chatVO = new ChatVO(raw);
            if(StringUtils.contains(raw.toString(), "console")) {
                sendCustomMessage(chatVO, raw);
			    return;
            }

			ChatVO c = null;
			
			try {
				
				c = new ChatVO(raw);
				
				String code = c.code;
				
				updateDataLastChat(c);
				
				checkUserLogado(c);

				if (code.equals("login")) {
					login(c);
				} else if (code.equals("getAll")) {
					get(c);
				} else if (code.equals("msg")) {
					msg(c);
				} else if (code.equals("msg1CC")) {
					// server received callback
					msg1CC(c);
				} else if (code.equals("msg2C")) {
					// reach destination
					msg2C_2A(c, false, 1);
				} else if (code.equals("msg2CC")) {
					// reach destination callback
					callback(c,true, 2);
				} else if (code.equals("msg2A")) {
					// mensagem read callback
					msg2C_2A(c, false, 2);
				} else if (code.equals("msg2AC")) {
					// mensagem read callback
					callback(c,true, 3);
				} else if(code.equals("c2CC")) {
					callback(c, true, 2);
				} else if (code.equals("c2A")) {
					// conversa read
					c2A(c, false);
				} else if (code.equals("c2AC")) {
					// conversa read callback
					c2A(c, true);
				} else if (code.equals("typing")) {
					typing(c);
				} else if (code.equals("ping")) {
					ping(c);
				} else if (code.equals("keepAliveOk")) {
					// Nao precisa tratar pq updateDataLastChat(c) ja faz o trabalho.
				} else if (code.equals("notifyUser")) {
					notifyUser(c);
				} else if (code.equals("forward")) {
					forward(c);
				} else if (code.equals("userStatus")) {
					/**
					 * Usamos para o away/alive
					 */
					userStatus(c);
				} else {
					log.error("UserMessageRouter - code:" + code + " unknow");
				}
				
			} catch (JSONException e) {
				System.err.println("JSON Error: " + raw);
				e.printStackTrace();
				exception(c, e);
			} catch (Exception e) {
				exception(c, e);
			}
		} else {
			log.error("JsonRawMessage - invalid message: " + msg);
		}
	}

    private void sendCustomMessage(ChatVO c, JsonRawMessage raw) {
        long fromUserId = c.from;
        long conversaId = c.conversaId;
        String msg = c.msg;
        List<UsuarioVO> usuarios = getLivecomInterface().findUsersFromConversation(conversaId, fromUserId, false);
        sendJsonToUsers(usuarios,raw, null, true, false,new InterceptorBeforeSendMsgToUser(){
            @Override
            public void intercept(UsuarioVO u, JsonRawMessage raw) {
                UserInfoVO userInfo = Livecom.getInstance().getUserInfo(u.getId());
                if(userInfo != null) {
                    boolean webConnected = userInfo.isWebOnline();
                    if(webConnected) {
                        try {
                            JSONObject jsonObj = new JSONObject(raw.getJson());
                            jsonObj.put("web", 1);
                            String json = jsonObj.toString();
                            raw.apply(json);
                        } catch (JSONException e) {
                            log.error(e.getMessage(), e);
                        }
                    }
                }
            }
        });
    }

    /**
	 * Metodo copia de UserActor.tellFriendsStatusChanged.
	 * 
	 * Usado para mandar status "away / alive", ao inves de que esta online.
	 * 
	 * @param c
	 */
	private void userStatus(ChatVO c) {
		long fromUserId = c.from;

		boolean away = c.away;
		
		String so = c.so;

		UserInfoSession session = UserInfoVO.getUserSession(c.raw);
		if(session != null) {
			// Deixa como away.
			session.setAway(away);
		}

		ChatUtils.sendUserStatus(getLivecomInterface(),fromUserId,true,away, so);
	}

	private void exception(ChatVO c, Exception e) {
		e.printStackTrace();
		log.error("UserMessageRouter exception: " + e.getMessage(), e);
		logError.error("UserMessageRouter exception: " + e.getMessage(), e);
	}

	/**
	 * Se o ator enviou msg pelo chat e por algum motivo nao esta logado no chat Livecom,
	 * corrige o problema.
	 * 
	 * @param raw
	 */
	private void checkUserLogado(ChatVO c) {
		JsonRawMessage raw = c.raw;
		if(raw.isUserActorOk()) {
			ActorRef tcpWeb = raw.actor;
			Long userId = raw.userId;
			String so = raw.userSo;
			
			// Fix Logado Chat
			boolean online = Livecom.getInstance().isUsuarioLogadoChat(userId);
			if(!online) {
				log.error("FIX AKKA WebConnectorActor online: " + userId);
				UserInfoVO userInfo = Livecom.getInstance().getUserInfo(userId);
				if(userInfo != null) {
					UserInfoSession session = userInfo.addSession(so);
					if(session != null) {
						session.setActorRef(tcpWeb.toString());
					}
				}
			}	
		}
	}

	/**
	 * Busca as mensagens pendentes do usuário.
	 *
	 * @param chat
	 * @param all - Se true retorna a lista de todas as mensagens, caso contrário retorna uma a uma.
	 * @throws JSONException
	 */
	private void get(ChatVO chat) throws JSONException {

		Long from = chat.from;
		// last msg id salva no device
		Long lastMsgId = chat.lastMsgId;

		/**
		 * Msg nao lidas ou ainda nao recebidas
		 */
		List<MensagemVO> msgs = getLivecomInterface().findMensagensMobile(from,lastMsgId);

		// Ator que enviou a msg.
		final ActorRef sender = chat.raw.actor;
		
		String flag = chat.flag;

		if(ListUtils.isEmpty(msgs)) {
			// Envia flag com 0.
			AkkaHelper.tellTcpJson(getSelf(), sender, ChatUtils.getJsonMsgBatch(false,0,flag));
		} else {

			int qtdeMsgs = msgs.size();
			int limit = ParametrosMap.getInstance().getInt("chat.batchSize.limit", 500);
			
			if(qtdeMsgs > limit) {
				// Forca refresh
				AkkaHelper.tellTcpJson(getSelf(), sender, ChatUtils.getJsonRefreshChat());
				return;
			}
			
			/**
			 * Envia no modo batch
			 */
			// Start batch
			if(qtdeMsgs > 1) {
				AkkaHelper.tellTcpJson(getSelf(), sender, ChatUtils.getJsonMsgBatch(true,qtdeMsgs,flag));
			}
			// Envia todas msgs
			for (int i = 0; i < msgs.size(); i++) {
				MensagemVO msg = msgs.get(i);
				// Envia a msg nao recebida para quem pediu
				JSONObject jsonMsg = ChatUtils.getJsonMsg(msg, false, i);
				JsonRawMessage raw = new JsonRawMessage(jsonMsg);
				AkkaHelper.tellTcpJson(getSelf(), sender, raw);
			}
			// Fim batch
			if(qtdeMsgs > 1) {
				AkkaHelper.tellTcpJson(getSelf(), sender, ChatUtils.getJsonMsgBatch(false,-1,flag));
			}
		}

		/**
		 * Msgs pendentes que device ainda não recebeu 2C, 2A
		 */
		sendMensagensCallback(chat,from, sender);
		
		/**
		 * Manda users online/away
		 */
		sendUsersOnline(from, sender);
	}

	/**
	 * Envia status das msgs que ainda não foram lidas.
	 */
	public void sendMensagensCallback(ChatVO chat,Long from, final ActorRef sender) {

		// msgs sem resposta
		List<Long> msgIds1C = chat.msgIds1C;
		List<Long> msgIds2C = chat.msgIds2C;
		List<Long> cIds = chat.cIds;

		log("sendMensagensCallback");
		log("msgIds1C: " + msgIds1C);
		log("msgIds2C: " + msgIds2C);
		log("cIds: " + cIds);

		/**
		 * Pega status das conversas nao lidas
		 */
		List<Long> cIds2A = getLivecomInterface().findIdsConversasLidasById(cIds, from);
		Long fromId = chat.from;
		for(Long cId : cIds2A) {
			// Envia c2A para conversas ja lidas para sincronizar mobile.
			JsonRawMessage raw = ChatUtils.getJsonC2A(fromId, cId);
			AkkaHelper.tellTcpJson(getSelf(), sender, raw);
		}
		
		/**
		 * Pega status das mensagens enviadas
		 */
		// Lista com todas msgs 1C e 2C
		List<Long> msgIdsAll = new ArrayList<>();
		msgIdsAll.addAll(msgIds1C);
		msgIdsAll.addAll(msgIds2C);
		List<MensagemVO> msgsCallback = getLivecomInterface().findMensagensById(msgIdsAll);

		if(ListUtils.isNotEmpty(msgsCallback)) {
			for (MensagemVO msg : msgsCallback) {
				// Envia a msg nao recebida para quem pediu

				Long id = msg.getId();

				boolean isFrom = msg.isFrom(from);

				JsonRawMessage raw = null;

				/**
				 * Enviadas por mim
				 */
				if(msg.isLida()) {
					
					// Envia 2A
					raw = ChatUtils.getJsonMessageMsg2A_2C("msg2A",msg.getConversaId(), msg.getId(),msg.getAmigoId(from));
				} else {
					if(msg.isEntregue()) {
						// Envia 2C apenas se status anterior não era 2C.
						boolean jaEra2C = msgIds2C.contains(id); 
						if(!jaEra2C) {
							raw = ChatUtils.getJsonMessageMsg2A_2C("msg2C",msg.getConversaId(), msg.getId(),msg.getAmigoId(from));
						}
					}
				}

				AkkaHelper.tellTcpJson(getSelf(), sender, raw);
			}
		}
	}

	public void sendUsersOnline(Long from, final ActorRef sender) throws JSONException {
		/**
		 * Usuarios Online
		 */
		List<Long> idsOnline = ChatUtils.getIdsUsersOnline(getLivecomInterface(),from);
		List<Long> idsAway = ChatUtils.getIdsUsersAway(getLivecomInterface(),from);
		
		if(ListUtils.isNotEmpty(idsOnline)) {
			JsonRawMessage rawOnline = ChatUtils.getJsonUsersOnline(idsOnline, idsAway);
			AkkaHelper.tellTcpJson(getSelf(), sender, rawOnline);
		}
	}

	private void updateDataLastChat(ChatVO c) throws JSONException {
		UserInfoSession session = UserInfoVO.getUserSession(c.raw);
		if(session != null) {
			// Ultimo json socket no chat.
			session.setDataUltChat(new Date());
			session.setKeepAliveOk(true);
		}
	}

	/**
	 * Recebeu a mensagem. Salva no livecom. Depois envia pro usuario pelo chat
	 * ou push.
	 * 
	 * {"code":"sendMessage","content":{"identifier":64,"conversationID":1596,"fromUserID":2,"msg":"oi"}}
	 * 
	 * @param raw
	 * @throws JSONException 
	 */
	private void msg(final ChatVO c) throws JSONException {
		/**
		 * Salvar a msg no banco
		 */


        executorMsg.execute(new Runnable() {
            @Override
            public void run() {

                long identifier = c.identifier;
                long conversaId = c.conversaId;

                // Ator que enviou a msg
                final ActorRef sender = c.raw.actor;

                try {

                    long gId = c.grupoId;
                    if(gId > 0) {
                        boolean isDentroGrupo = getLivecomInterface().isUsuarioDentroDoGrupo(c.from, gId);
                        if(!isDentroGrupo) {
                            sendMessageDenied(c, conversaId, sender, gId);
                            return;
                        }
                    }

                    MensagemVO msg = getLivecomInterface().saveMessage(c);

                    if (msg != null) {
                        /**
                         * Salvou
                         */
                        /**
                         * Msg OK: Manda "serverReceivedMessage" pro usuario que
                         * enviou a mensagem
                         */

                        // 1) Avisa quem enviou
                        sendServerReceivedMessage(c, msg);

                        // 2) Manda msg pro user ou grupo.
                        JsonRawMessage raw = sendMessageToUsers(c, msg);

                        // 3) Manda msg para você mesmo. Para replicar nas outras connections.
                        ChatUtils.sendReplicateMessage(c, raw);
                    } else {
                        /**
                         * Error
                         */
                        JsonRawMessage raw = ChatUtils.getJsonMsg1C(null,identifier,conversaId, false);
                        AkkaHelper.tellTcpJson(getSelf(), sender, raw);
                    }
                } catch (Exception e) {
                    ChatUtils.logError("Erro ao salvar a mensagem " + e.getMessage(), e);
                    exception(c, e);

                    /**
                     * Error
                     */
                    JsonRawMessage rawServerReceivedMessage = ChatUtils.getJsonMsg1C(null,identifier,conversaId, false);
                    AkkaHelper.tellTcpJson(getSelf(), sender, rawServerReceivedMessage);
                }
            }
        });
		

	}

	private void sendMessageDenied(ChatVO c, long conversaId, final ActorRef sender, long gId) throws JSONException {
		// manda o acesso denied ao grupo
		JsonRawMessage raw = ChatUtils.getJsonDenied(conversaId, gId);
		AkkaHelper.tellTcpJson(getSelf(), sender, raw);
		
		//replica para as outras conexoes
		ChatUtils.sendReplicateMessage(c, raw);
		return;
	}

	private JsonRawMessage sendMessageToUsers(ChatVO c, MensagemVO msg) {
		long fromUserId = c.from;
		long conversaId = c.conversaId;
		List<UsuarioVO> usuarios = getLivecomInterface().findUsersFromConversation(conversaId, fromUserId, false);
		JSONObject jsonMsg = ChatUtils.getJsonMsg(msg);
		JsonRawMessage raw = new JsonRawMessage(jsonMsg);
		sendJsonToUsers(usuarios,raw, msg, true, true,new InterceptorBeforeSendMsgToUser(){
			@Override
			public void intercept(UsuarioVO u, JsonRawMessage raw) {
				/**
				 * Envia web=1 se usuario esta logado na web e online
				 */
				UserInfoVO userInfo = Livecom.getInstance().getUserInfo(u.getId());
				if(userInfo != null) {
					boolean webConnected = userInfo.isWebOnline();
					if(webConnected) {
						try {
							JSONObject jsonObj = new JSONObject(raw.getJson());
							jsonObj.put("web", 1);
							String json = jsonObj.toString();
							raw.apply(json);
						} catch (JSONException e) {
							log.error(e.getMessage(), e);
						}
					}
				}
			}
		});
		return raw;
	}

	private void sendServerReceivedMessage(ChatVO c, MensagemVO msg) {
		long identifier = c.identifier;
		long conversaId = c.conversaId;
		final ActorRef sender = c.raw.actor;
		JsonRawMessage raw = ChatUtils.getJsonMsg1C(msg,identifier,conversaId, true);
		AkkaHelper.tellTcpJson(getSelf(), sender, raw);
	}

	/**
	 * 
    	Recebe:
    	{"code": "notifyUser","content": {"fromUserID": 2525,"toUserID": 2525}}
    	
    	Devolve Push:
    	
    	{"notificacao": {
  "tipo": "mensagem",
  "conversaId": 1986,
  "mensagemId": 27991,
  "title": "Admin Livetouch: hey",
  "msg": "hey",
  "data": "25/05/2016 15:55:15",
  "timestamp": 1464202515155,
  "tituloOriginal": "hey"
}} 
    	
	 * @param raw
	 */
	private void notifyUser(ChatVO c) {
		
		LivecomChatInterface livecomInterface = getLivecomInterface();
		
		Long fromUserId = c.from;
		Long toUserId = c.to;
		if(toUserId != null && fromUserId != null) {
			ConversaVO msgConversa = livecomInterface.findConversaUsers(fromUserId,toUserId);
			UsuarioVO userFrom = livecomInterface.getUsuario(fromUserId);
			UsuarioVO userTo = livecomInterface.getUsuario(toUserId);
			if(msgConversa != null) {
				MensagemVO msg = new MensagemVO();

				String fromNome = msgConversa.getFromNome();
				
				msg.setId(1L);
				msg.setMsg(fromNome + " está lhe chamando a atenção.");
				msg.setConversaId(msgConversa.getConversaId());
				msg.setFromNome(fromNome);
				livecomInterface.sendPush(msg, userTo);
				log("receiveNotifyUser sendPush to ["+userTo+"]: " + msg);
			} else {
				log.error(String.format("Erro receiveNotifyUser userFrom: %s, userTo: %s, conversa: %s",userFrom, userTo, msgConversa));
			}
		}
	 
	}

	private void toast(ActorRef actorTo, String msg) {
		JsonRawMessage rawErro = ChatUtils.getJsonToast(msg);
		
		AkkaHelper.tellTcpJson(getSelf(),actorTo, rawErro);
	}

	/**
	 * Forward individual
	 * 
	 * {"code":"forward","msgId":1,"from":2,"to":"1", identifier: 123}
	 * 
	 * Ou para grupo
	 * 
	 * {"code":"forward","msgId":1,"from":2,"gId":"1", identifier: 123}
	 * 
	 * Ou para conversa
	 * 
	 * {"code":"forward","msgId":1,"from":2,"cId":"1", identifier: 123}
	 * 
	 * @param raw
	 */
	private void forward(ChatVO c) {
		try {
			JSONArray msgs = c.msgs;
			if(msgs != null && msgs.length() > 0) {
				for (int i = 0; i < msgs.length(); i++) {
					JSONObject json = msgs.getJSONObject(i);
					long msgId = json.optLong("msgId");
					long identifier = json.optLong("identifier");
					if(msgId == 0 && identifier == 0) {
						throw new DomainException("Parametros invalidos");
					}
					c.identifier = identifier;
					c.msgId = msgId;
					
					// salva a mensagem que e para ser replicada
					MensagemVO msg = getLivecomInterface().forward(c);
					//1) avisa que o servidor recebeu a mensagem
					sendServerReceivedMessage(c, msg);
					//2) envia a mensagem para os usuarios da conversa
					JsonRawMessage raw = sendMessageToUsers(c, msg);
					
					//3) replica a mensagem para as outras conexoes
					ChatUtils.sendReplicateMessage(c, raw);
				}
			} else {
				// salva a mensagem que e para ser replicada
				MensagemVO msg = getLivecomInterface().forward(c);
				//1) avisa que o servidor recebeu a mensagem
				sendServerReceivedMessage(c, msg);
				//2) envia a mensagem para os usuarios da conversa
				JsonRawMessage raw = sendMessageToUsers(c, msg);
				
				//3) replica a mensagem para as outras conexoes
				ChatUtils.sendReplicateMessage(c, raw);
			}
		} catch (DomainException | JSONException e) {
			ChatUtils.logError("Erro ao salvar a mensagem " + e.getMessage(), e);
			exception(c, e);
		}
	}

	/**
	 * {"code":"ping","content":{"user":"rlecheta@livetouch.com.br"}}
	 * 
	 * @param raw
	 */
	private void ping(ChatVO c) {
		Long userId = c.from;
		if(userId != null) {

			if(AkkaHelper.isOnline(userId)) {
				JsonRawMessage jsonPingOk = ChatUtils.getJsonPingOK(userId);

				ActorSelection userActor = AkkaFactory.getUserActor(getContext(), userId.toString());
				AkkaHelper.tellWithFuture(getContext(), getSelf(), userActor, jsonPingOk);
			}
		}		
	}

	/**
	 * {"code":"typing","content":{"conversationID":1596,"fromUserID":2,"fromUserName":"Ricardo Lecheta"}}
	 * 
	 * @param raw
	 */
	private void typing(ChatVO c) {

		Long fromUserId = c.from;
		Long conversationID = c.conversaId;

		LivecomChatInterface livecomInterface = getLivecomInterface();
		List<UsuarioVO> usuarios = livecomInterface.findUsersFromConversation(conversationID, fromUserId, false);

		if(usuarios != null) {
			for (UsuarioVO u : usuarios) {
				Long userId = u.getId();
				if (userId.equals(fromUserId)) {
					// Proprio cara que esta digitando
					continue;
				}

				if(AkkaHelper.isOnline(userId)) {
					ActorSelection userActor = AkkaFactory.getUserActor(getContext(), userId.toString());
					AkkaHelper.tellWithFuture(getContext(), getSelf(), userActor, c.raw);
				}
			}
		}
	}
	
	private void msg2C_2A(final ChatVO c, boolean callback, final int status) throws DomainException {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					// status 1 - recebida, 2 lida
					updateMsgStatus(c, status);
				} catch (Exception e) {
					exception(c, e);
				}				
			}
		});
	}

	private void callback(final ChatVO c, boolean callback, final int status) throws DomainException {
		//final Long msgId = c.msgId;
		//final Long conversaId = c.conversaId;
		/**
		 * 1 - check1cinza, 2 check2cinza, 3 check2azul, 4 conversationWasRead
		 */
		// TODO remover callback se outra parada do sync der certo.
		//getLivecomInterface().updateStatusMensagemCallback(conversaId,msgId, status);
	}

	/**
	 * {"code":"conversationWasRead","content":{"conversationID":"2275","fromUserID":"2524","status":"OK"}}
	 * 
	 * @param raw
	 * @param status 1 - recebida, 2 lida
	 * @throws DomainException 
	 * @throws JSONException 
	 */
	private void c2A(ChatVO c, boolean callback) throws DomainException, JSONException {
		if(callback) {
			
			Long conversaId = c.conversaId;

			getLivecomInterface().updateStatusMensagemCallback(conversaId,-1, 3);

		} else {

			final Long conversationId = c.conversaId;
			
			final long grupoId = c.grupoId;
			
			if(grupoId > 0) {
				/**
				 * Permissao: accessDenied
				 */
				final ActorRef sender = c.raw.actor;
				boolean isDentroGrupo = getLivecomInterface().isUsuarioDentroDoGrupo(c.from, grupoId);
				if(!isDentroGrupo) {
					sendMessageDenied(c, conversationId, sender, grupoId);
					return;
				}
			}
			
			/**
			 * Neste caso o fromUserId é quem enviou a mensagem, e não quem está respondendo que leu).
			 */
			final Long fromUserId = c.from;
			
			boolean todasLidas = getLivecomInterface().markConversationAsRead(fromUserId, conversationId);

			log("LivecomRouter: SetConversationWasRead lida(" + conversationId + "), todasLidas: " + todasLidas);

			// Todos usuarios do grupo leram a msg.
			// Nao busca o user q clicou.
			List<UsuarioVO> usuarios = getLivecomInterface().findUsersFromConversation(conversationId, fromUserId, false);

			// Devolve json msg2A e msg2C
			JsonRawMessage raw = c.raw;
			
			if(grupoId > 0 && todasLidas) {
				raw = ChatUtils.putInt("g2A",1,raw);
			} else {
				/**
				 * Envia data do ultimo login
				 */
				if(usuarios != null && usuarios.size() > 0) {
					final ActorRef sender = c.raw.actor;
					for (UsuarioVO u : usuarios) {
						if(fromUserId != u.id) {
							String so = c.raw.userSo;
							JsonRawMessage rawStatusUser = ChatUtils.getUserStatusToMe(getLivecomInterface(), u.id, so);
							AkkaHelper.tellTcpJson(getSelf(), sender, rawStatusUser);
							break;
						}
					}
				}
			}
			
			// Envia JSON para todos do grupo
			// {"code":"c2A","cId":2391,"from":5251,"status":"OK"}
			// {"code":"c2A","cId":2493,"gId":556,"g2A":1,"from":2524,"status":"OK"}
			sendJsonToUsers(usuarios,raw, null, false, false);
			
			// Replicate
			ChatUtils.sendReplicateMessage(c, raw);
		}
	}

	private void msg1CC(ChatVO c) throws DomainException {
		final Long msgId = c.msgId;

		getLivecomInterface().updateStatusMensagemCallback(0,msgId, 1);
	}

	/**
	 * {"code":"msg3","content":{"conversationID":1673,"messageID":19173,"fromUserID":2,"toUserID":"1","status":"Ok"}}
	 * 
	 * @param raw
	 * @param status 1 - recebida, 2 lida
	 * @throws DomainException 
	 * @throws JSONException 
	 */
	private void updateMsgStatus(ChatVO c, final int status) throws DomainException, JSONException {
		JsonRawMessage raw = c.raw;

		/**
		 * Neste caso o fromUserId é quem enviou a mensagem, e não quem está respondendo que leu).
		 */
		final Long fromUserId = c.from;
		final Long toUserId = c.to;
		final Long msgId = c.msgId;

		log("LivecomRouter: UpdateMessage lida(" + msgId + ")");

		// select entregue,lida,msg from mensagem where id in (select
		// max(id) from mensagem );
		UpdateStatusMensagemResponseVO response = getLivecomInterface().updateStatusMensagem(toUserId, msgId, status);
		if(response != null) {

			/**
			 * Avisa chat que foi recebida/lida
			 */
			boolean online = AkkaHelper.isOnline(fromUserId);
			if(online) {

				/**
				 * Neste caso o fromUserId é quem enviou a mensagem, e não quem está respondendo que leu).
				 */
				ActorSelection userQueEnviouMsg = AkkaFactory.getUserActor(getContext(), fromUserId.toString());

				MensagemVO mensagem = response.mensagem;

				boolean isGrupo = mensagem != null && mensagem.isGroup();

				if(isGrupo) {
					boolean entregue = mensagem.isEntregue();
					boolean lida = mensagem.isLida();
					
					if(entregue) {
						raw = ChatUtils.putInt("g2C",1,raw);
					} 
					if(lida) {
						raw = ChatUtils.putInt("g2A",1,raw);
					}
				}

				// Avisa quem enviou a msg (que amigo recebeu ou leu.)
				// {"code":"msg2C","msgId":77631,"cId":2493,"from":2524,"to":"2523"}
				AkkaHelper.tellWithFuture(getContext(), getSelf(), userQueEnviouMsg, raw);
			}
		}
	}
	
	static interface InterceptorBeforeSendMsgToUser {
		void intercept(UsuarioVO u,final JsonRawMessage raw);
	}
	
	public void sendJsonToUsers(List<UsuarioVO> usuarios,final JsonRawMessage json,final MensagemVO mensagem,final boolean sendPush, boolean sendToBot) {
		sendJsonToUsers(usuarios, json, mensagem, sendPush, sendToBot, null);
	}

	/**
	 * Manda uma msg para os usuarios online do grupo
	 * @param c 
	 * @param fromUserId 
	 */
	public void sendJsonToUsers(List<UsuarioVO> usuarios,final JsonRawMessage raw,final MensagemVO mensagem,final boolean sendPush, boolean sendToBot, InterceptorBeforeSendMsgToUser interceptor) {

		// Usuários da conversa
		final LivecomChatInterface livecomInterface = getLivecomInterface();
		
		if(usuarios != null) {

			/**
			 * Lista users offline para receber push
			 */
			List<UsuarioVO> usersOffline = new ArrayList<UsuarioVO>();
			
			// Todos usuarios da mensagem
			for (final UsuarioVO u : usuarios) {
				Long userId = u.id;
				
				boolean isLogadoChat = Livecom.getInstance().isUsuarioLogadoChat(userId);

				if(isLogadoChat) {
					// encontra ator do usuario
					ActorSelection userActor = AkkaFactory.getUserActor(getContext(), userId);

					Runnable runnableError = new Runnable() {
						@Override
						public void run() {
							// Contigencia Offline, manda push.
							log("sendJsonToAllUsersFromGrupo error [" + u + "]: " + raw);
						}
					};
					
					if(interceptor != null) {
						interceptor.intercept(u, raw);
					}

					// manda msg pro ator (se online)
					AkkaHelper.tellWithFuture(getContext(), getSelf(), userActor, raw, runnableError);
				} else {
					if(StringUtils.isNotEmpty(u.getBotKey()) && sendToBot) {
					    if(mensagem != null) {
                            sendMessageToBot(mensagem, u);
                        }
						continue;
					}
					if(sendPush) {
						usersOffline.add(u);
					}
				}
			}

			/**
			 * Somente se for para enviar por Push.
			 */
			if(sendPush && !usersOffline.isEmpty()) {
				if(usersOffline.size() == 1) {
					log("Enviando push para ["+usersOffline.get(0)+"] ");
				} else {
					log("Enviando push para ["+usersOffline.size()+"] users. ");
				}
				if(mensagem != null) {
                    livecomInterface.sendPush(mensagem, usersOffline);
                }

			}
		}
	}

	private void sendMessageToBot(final MensagemVO mensagem, final UsuarioVO u) {
		ApiAiHelper apiAi = new ApiAiHelper(u.getBotKey());
		try {
			Long from = u.getId();
			
			// envia que o bot recebeu e leu a mensagem
			Long cId = mensagem.getConversaId();
			Long msgId = mensagem.getId();
			Long to = mensagem.getFromId();
			
			JsonRawMessage raw = ChatUtils.getJsonMsg2C(msgId, cId, to, from);
			getSelf().tell(raw, getSelf());
			String d = mensagem.getMsg();
			
			/**
			 * Google api.ai
			 */
			apiAi.sendText(d, from.toString());
			if (apiAi.responseOk()) {
				// Devolve pro usuario a resposta
				raw = ChatUtils.getJsonMsg2A(msgId, cId, to, from);
				getSelf().tell(raw, getSelf());
				AIResponse response = apiAi.getResponse();
				List<ResponseMessage> messages = response.getResult().getFulfillment().getMessages();
				List <JSONObject> cards = null;
				if(messages.size() > 0) {
					for (ResponseMessage responseMessage : messages) {
						if(responseMessage instanceof ResponseSpeech) {
							JSONObject json = new JSONObject();
							json.put("code", "msg");
							json.put("identifier", UUID.randomUUID().getLeastSignificantBits());
							json.put("cId", cId);
							json.put("from", from);
							
							ResponseSpeech r = (ResponseSpeech) responseMessage;
							List<String> msg = r.getSpeech();
							String s = msg.get(0);
							json.put("msg", s);
							
							getSelf().tell(new JsonRawMessage(json), getSelf());
						} else if(responseMessage instanceof ResponsePayload) {
							ResponsePayload r = (ResponsePayload) responseMessage;
							JSONObject payload = new JSONObject(r.getPayload().toString());
							String type = payload.optString("type");
							if(StringUtils.equals(type, "CARD")) {
								if(cards == null) cards = new ArrayList<JSONObject>();
								cards.add(payload);
							} else {
								JSONObject facebook = payload.optJSONObject("facebook");
								if(facebook == null) {
									continue;
								}
								JSONObject attachment = facebook.optJSONObject("attachment");
								if(attachment == null) {
									continue;
								}
								JSONObject p = attachment.optJSONObject("payload");
								if(p == null) {
									continue;
								}
								JSONArray buttons = p.optJSONArray("buttons");
								if(buttons == null) {
									continue;
								}
								JSONObject json = new JSONObject();
								json.put("code", "msg");
								json.put("identifier", DateUtils.getTimeMilisecond());
								json.put("cId", cId);
								json.put("from", from);
								json.put("buttons", buttons);
								getSelf().tell(new JsonRawMessage(json), getSelf());
							}
						}
					}
				}
				if(cards != null) {
					JSONObject json = new JSONObject();
					json.put("code", "msg");
					json.put("identifier", DateUtils.getTimeMilisecond());
					json.put("cId", cId);
					json.put("from", from);
					json.put("cards", cards);
					getSelf().tell(new JsonRawMessage(json), getSelf());
				}
			}
		} catch (Exception e) {
			log.error("Erro ao enviar mensagem para o API.AI" + e.getMessage(), e);
		}
	}

	/**
	 *  {"code":"login","content":{"user":"rlecheta@livetouch.com.br","pass":"ricardo","away":true}}
	 *  
	 * @param raw
	 * @throws JSONException 
	 */
	private void login(ChatVO c) throws JSONException {
		
		ActorRef tcpWebActor = c.raw.actor;
		
		String user = c.user;
		String pass = c.pass;
		String so = c.so;
		boolean away = c.away;
		
		log("Chat Login(" + user + "/"+ so + ")");
		
		checkLoginVersion(c);
		
		// Login
		UserInfoVO userInfo = getLivecomInterface().login(user,pass, so, tcpWebActor, away);

		if(userInfo != null) {
			long userId = userInfo.id;
			String userLogin = userInfo.login;
			
			String appVersion = c.version;
			Integer appVersionCode = c.versionCode;
			String userAgentSO = ChatUtils.getUserAgentSO(so);
			ControleVersaoVO controleVersao = getLivecomInterface().validateVersion(userInfo.getEmpresaId(), appVersion, appVersionCode, userAgentSO);			
			// Resposta do login
			// {"code":"loginResponse","content":{"login":"rlecheta@livetouch.com.br","status":"Ok"}}
			JsonRawMessage json = ChatUtils.getJsonLoginResponse(userLogin, "Ok", controleVersao);
			AkkaHelper.tellTcpJson(getSelf(), tcpWebActor, json);

			// Cria o User Actor
			ActorSelection mainActor = AkkaFactory.getMainActor(getContext());
			MainActor.CreateUserActor create = new MainActor.CreateUserActor(c.raw,so, String.valueOf(userId));
			mainActor.tell(create, getSelf());
		} else {
			log("LivecomRouter login ERROR para: " + user);
			
			// {"code":"loginResponse","content":{"login":"rlecheta@livetouch.com.br","status":"nOk"}}
			JsonRawMessage json = ChatUtils.getJsonLoginResponse(user, "nOk", null);

			AkkaHelper.tellTcpJson(getSelf(), tcpWebActor, json);
		}
	}

	private void checkLoginVersion(ChatVO c) {
		
	}

	private void log(String string) {
		int max = 250;
		if(string.length() > max) {
			string = string.substring(0,max);
		}
		log.debug(string);
	}
}
