package br.livetouch.livecom.domain.repository;

import java.util.Set;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.CategoriaIdioma;
import br.livetouch.livecom.domain.CategoriaPost;

@Repository
public interface CategoriaIdiomaRepository extends net.livetouch.tiger.ddd.repository.Repository<CategoriaIdioma> {

	Set<CategoriaIdioma> findByCategoria(CategoriaPost c);

}