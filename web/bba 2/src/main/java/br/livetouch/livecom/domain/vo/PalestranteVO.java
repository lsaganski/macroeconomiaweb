package br.livetouch.livecom.domain.vo;

import br.livetouch.livecom.domain.Palestrante;

public class PalestranteVO {
	
	public Long id;
	public String nome;
	public String email;
	public String curriculo;
	public String fotos3;

	
	public PalestranteVO (Palestrante palestrante){
		this.setId(palestrante.getId());
		this.setNome(palestrante.getNome());
		this.setEmail(palestrante.getEmail());
		this.setFotos3(palestrante.getFotoS3());
		this.setCurriculo(palestrante.getCurriculo());
		
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getCurriculo() {
		return curriculo;
	}


	public void setCurriculo(String curriculo) {
		this.curriculo = curriculo;
	}


	public String getFotos3() {
		return fotos3;
	}


	public void setFotos3(String fotos3) {
		this.fotos3 = fotos3;
	}
}