package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import br.livetouch.livecom.domain.LoginReport;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.Plataforma;
import br.livetouch.livecom.utils.DateUtils;

public class RelatorioLoginVO implements Serializable {

	private static final long serialVersionUID = 1524516347642576158L;
	
	private String data;
	private Long count;
	private Long countDistinct;
	private String login;
	private String nome;
	private String email;
	private Long id;
	private String plataforma;
	private String appVersion;
	private String appVersionCode;
	private Integer quantidade;
	
	public RelatorioLoginVO() {
		
	}
	
	public RelatorioLoginVO(LoginReport l) {
		this.data = DateUtils.toString(l.getData(), "dd/MM/yyyy HH:mm:ss");
		this.login = l.getLogin();
		Usuario usuario = l.getUsuario();
		if(usuario != null) {
			this.email = usuario.getEmail();
			this.nome = usuario.getNome();
			this.appVersion = usuario.getDeviceAppVersion();
			this.appVersionCode = usuario.getDeviceVersionCode();
		}
	}

	public RelatorioLoginVO(Usuario u,LoginReport l) {
		if(u != null) {
			this.id = u.getId();
			this.login = u.getLogin();
			this.email = u.getEmail();
			this.nome = u.getNome();
			this.appVersion = u.getDeviceAppVersion();
			this.appVersionCode = u.getDeviceVersionCode();
		}
		if(l != null) {
			this.plataforma = l.getPlataforma() != null ? l.getPlataforma().name() : "Outros";
		}
	}
	
	public RelatorioLoginVO(String data, Long countDistinct, Long count) {
		this.data = data;
		this.count = count;
		this.countDistinct = countDistinct;
	}

	public RelatorioLoginVO(String version, Long count) {
//		this.quantidade = count;
		this.count = count;
		this.appVersion = !StringUtils.isEmpty(version) ? version : "Nenhuma";
	}
	
	public RelatorioLoginVO(String data, String login, Long id) {
		this.data = data;
		this.login = login;
		this.id = id;
	}
	
	public RelatorioLoginVO(String data, String login, Long count, Long id) {
		this.data = data;
		this.login = login;
		this.count = count;
		this.id = id;
	}
	
	public RelatorioLoginVO(Date data, Plataforma plataforma, Long count) {
		this.data = DateUtils.toString(data, "dd/MM/yyyy");
		this.plataforma = plataforma.name();
		this.count = count;
	}
	
	public RelatorioLoginVO(Date data, String login, Long id, Plataforma plataforma) {
		this.data = DateUtils.toString(data, "dd/MM/yyyy HH:mm:ss");
		this.plataforma = plataforma.name();
		this.login = login;
		this.id = id;
	}
	
	public RelatorioLoginVO(Date data, String login, Long id, Plataforma plataforma, Long count) {
		this.data = DateUtils.toString(data, "dd/MM/yyyy");
		this.plataforma = plataforma.name();
		this.login = login;
		this.id = id;
		this.count = count;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Long getCountDistinct() {
		return countDistinct;
	}

	public void setCountDistinct(Long countDistinct) {
		this.countDistinct = countDistinct;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlataforma() {
		return plataforma;
	}

	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getAppVersionCode() {
		return appVersionCode;
	}

	public void setAppVersionCode(String appVersionCode) {
		this.appVersionCode = appVersionCode;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
