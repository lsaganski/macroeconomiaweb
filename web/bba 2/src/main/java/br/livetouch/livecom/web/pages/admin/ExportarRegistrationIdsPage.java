package br.livetouch.livecom.web.pages.admin;

import java.io.IOException;
import java.util.HashMap;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.vo.DeviceVO;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.livetouch.extras.util.ServletUtil;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

/**
 * 
 */
@Controller
@Scope("prototype")
public class ExportarRegistrationIdsPage extends LivecomAdminPage {
	public Form form = new Form();

	public HashMap<String, DeviceVO> map = new HashMap<String, DeviceVO>();
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissoes(true, ParamsPermissao.VIZUALIZAR_RELATORIOS, ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();

		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "botao info");
		form.add(tExportar);
		
	}

	public boolean exportar() throws DomainException {

		map = logService.getRegistrationIds(getEmpresa());

		String csv = "";
		for (DeviceVO vo : map.values()) {
			csv += vo.getLogin() + ";" + vo.getRegistrationId() + ";" + vo.getSo() + ";" + vo.getVersao() + System.getProperty("line.separator");
		}
		try {
			ServletUtil.exportBytesDownload(getContext().getResponse(), csv.getBytes(Utils.CHARSET), "devices.csv");
		} catch (IOException e) {
			logError(e.getMessage(), e);
		}

		setPath(null);

		return false;
	}
}
