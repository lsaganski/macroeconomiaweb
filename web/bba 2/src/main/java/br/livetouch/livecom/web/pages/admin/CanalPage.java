package br.livetouch.livecom.web.pages.admin;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Canal;
import net.livetouch.click.control.link.Link;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LinkDecorator;
import net.sf.click.extras.control.LongField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class CanalPage extends LivecomAdminPage {

	public PaginacaoTable table = new PaginacaoTable();
	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public Canal canal;

	public List<Canal> canals;
	
	@Override
	public void onInit() {
		super.onInit();

		form();
		
		table();
	}
	
	private void table() {
		Column c = new Column("id",getMessage("codigo.label"));
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("nome", getMessage("nome.label"));
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes",getMessage("coluna.detalhes.label"));
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	public void form(){

		TextField tNome = new TextField("nome", getMessage("nome.label"));
		tNome.setAttribute("autocomplete", "off");
		tNome.setAttribute("rel", "Adicionar canal");
		tNome.setAttribute("class", "mascara");
		form.add(tNome);
		LongField lId = new LongField("id");
		form.add(lId);
		
		if (id != null) {
			canal = canalService.get(id);
		} else {
			canal = new Canal();
		}

		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip botao export");
		form.add(tExportar);
		
		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);
		
		Submit cancelar = new Submit("novo", getMessage("novo.label"), this,"novo");
		cancelar.setAttribute("class", "botao btSalvar");
		form.add(cancelar);

		form.copyFrom(canal);
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				canal = id != null ? canalService.get(id) : new Canal();
				form.copyTo(canal);

				canalService.saveOrUpdate(canal);

				setMessageSesssion(getMessage("msg.salvar.canal.sucess", canal.getNome()),getMessage("canal.salvo.sucess"));
				
				setRedirect(CanalPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(CanalPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		canal = canalService.get(id);
		form.copyFrom(canal);
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Canal e = canalService.get(id);
			if(id.equals(1L) && "livecom".equalsIgnoreCase(e.getNome())) {
				throw new DomainException(getMessage("msg.remover.canal.error"));
			}
			canalService.delete(e);
			setRedirect(CanalPage.class);
			setMessageSesssion(getMessage("msg.remover.canal.sucess",e.getNome()),getMessage("msg.excluir.canal.sucess"));
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.excluir.canal.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		canals = canalService.findAll();

		// Count(*)
		int pageSize = 100;
		long count = canals.size();

		createPaginator(page, count, pageSize);

		table.setPageSize(pageSize);
		table.setRowList(canals);
	}
	public boolean exportar(){
		String csv = canalService.csvCanal();
		download(csv, "canais.csv");
		return true;
	}
}
