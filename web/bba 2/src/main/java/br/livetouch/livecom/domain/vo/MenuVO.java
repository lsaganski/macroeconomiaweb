package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Menu;

public class MenuVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String label;
	private String link;
	private String icone;
	private String descricao;
	private int ordem;
	private Boolean linkExterno;
	private Boolean divider;
	
	private MenuVO parent;
	private List<MenuVO> filhos;
	private EmpresaVO empresa;
	
	public MenuVO() {}

	public MenuVO(Menu m) {
		setId(m.getId());
		setLabel(m.getLabel());
		setLink(m.getLink());
		setIcone(m.getIcone());
		setOrdem(m.getOrdem());
		setLinkExterno(m.isLinkExterno());
		setDivider(m.isDivider());
		setDescricao(m.getDescricao());
	}
	
	public void setMenu(Menu m) {
		setId(m.getId());
		setLabel(m.getLabel());
		setLink(m.getLink());
		setIcone(m.getIcone());
		setOrdem(m.getOrdem());
		setLinkExterno(m.isLinkExterno());
		setDivider(m.isDivider());
		setDescricao(m.getDescricao());
		
		parent = m.getParent() != null ? new MenuVO(m.getParent()) : null;
		
	}
	
	public static List<MenuVO> fromList(List<Menu> menus) {
		List<MenuVO> vos = new ArrayList<>();
		if(menus == null) {
			return vos;
		}
		for (Menu m : menus) {
			MenuVO menuVO = new MenuVO();
			menuVO.setMenu(m);
			vos.add(menuVO);
		}
		return vos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getIcone() {
		return icone;
	}

	public void setIcone(String icone) {
		this.icone = icone;
	}

	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	public MenuVO getParent() {
		return parent;
	}

	public void setParent(MenuVO parent) {
		this.parent = parent;
	}

	public List<MenuVO> getFilhos() {
		return filhos;
	}

	public void setFilhos(List<MenuVO> filhos) {
		this.filhos = filhos;
	}
	
	public void addFilho(MenuVO filho) {
		if(this.filhos != null) {
			this.filhos.add(filho);
		} else {
			this.filhos = new ArrayList<MenuVO>();
			this.filhos.add(filho);
		}
	}

	public Boolean getLinkExterno() {
		return linkExterno;
	}

	public void setLinkExterno(Boolean linkExterno) {
		this.linkExterno = linkExterno;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public EmpresaVO getEmpresa() {
		return empresa;
	}

	public void setEmpresa(EmpresaVO empresa) {
		this.empresa = empresa;
	}

	public Boolean isDivider() {
		return divider;
	}

	public void setDivider(Boolean divider) {
		this.divider = divider;
	}

}
