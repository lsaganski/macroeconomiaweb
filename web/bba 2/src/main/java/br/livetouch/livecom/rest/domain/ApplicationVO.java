package br.livetouch.livecom.rest.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Application;

public class ApplicationVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public String nome;
	public String descricao;
	public String website;
	public String callbackUrl;
	public String consumerKey;
	public String consumerSecret;

	public ApplicationVO(Application app) {
		this.id = app.getId();
		this.nome = app.getNome();
		this.descricao = app.getDescricao();
		this.website = app.getWebsite();
		this.callbackUrl = app.getCallbackUrl();
		this.consumerKey = app.getConsumerKey();
		this.consumerSecret = app.getConsumerSecret();
	}

	public static List<ApplicationVO> createByList(List<Application> apps) {
		List<ApplicationVO> applications = new ArrayList<ApplicationVO>();
		for (Application app : apps) {
			applications.add(new ApplicationVO(app));
		}
		return applications;
	}
}
