package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.security.LivecomSecurity;
import br.livetouch.livecom.security.UserAgentUtil;
import net.sf.click.Page;

@Controller
@Scope("prototype")
public class IpPage extends Page {
	public String ip;
	public String host;
	public String userAgent;
	
	@Override
	public boolean onSecurityCheck() {
		return true;
	}

	@Override
	public void onGet() {
		super.onGet();

        ip = LivecomSecurity.getIp(getContext());
        host = LivecomSecurity.getHost(getContext());
        userAgent = UserAgentUtil.getUserAgent(getContext());
	}
}
