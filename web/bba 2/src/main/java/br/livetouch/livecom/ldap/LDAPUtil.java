package br.livetouch.livecom.ldap;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;

/**
 * 
 * 
 * @author rlech
 *
 */
public class LDAPUtil {
	protected static final Logger log = Log.getLogger(LDAPUtil.class);

	private DirContext ldapContext;
	private ParametrosMap params;

	public LDAPUtil(ParametrosMap params) {
		this.params = params;
	}

	public DirContext getContext(String login, String senha) throws NamingException {

		String urlProvider = getParam(Params.LDAP_PROVIDER_URL, "ldap://visanet.corp:389");
		String contextFactory = getParam(Params.LDAP_INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		String securityAuthentication = getParam(Params.LDAP_SECURITY_AUTHENTICATION, "simple");

		// String securityPrincipal =
		// getParam(Params.LDAP_SECURITY_PRINCIPAL,"CN=Jaime Peres Servidone
		// Nagase,OU=Users,OU=Alphaville,OU=Sites,DC=visanet,DC=corp");
		// String securityCredentials = getParam(Params.LDAP_SECURITY_PASSWD,
		// "livetouch2013");

		Hashtable<String, String> ldapEnv = new Hashtable<String, String>(11);
		ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
		ldapEnv.put(Context.PROVIDER_URL, urlProvider);
		ldapEnv.put(Context.SECURITY_AUTHENTICATION, securityAuthentication);
		ldapEnv.put(Context.SECURITY_PRINCIPAL, login);
		ldapEnv.put(Context.SECURITY_CREDENTIALS, senha);
		ldapEnv.put("com.sun.jndi.ldap.read.timeout", "15000");
		ldapEnv.put("com.sun.jndi.ldap.connect.timeout", "15000");

		log("LDAPUtil ldapEnv: " + ldapEnv);
		log("LDAPUtil params: " + params);

		InitialDirContext ctx = new InitialDirContext(ldapEnv);
		log("LDAPUtil ctx: " + ctx);
		return ctx;
	}

	public LDAPUser loginByDN(String userCn,String login, String pwd) {
		// Find login
		try {
			log(">> LDAPUtil login userCn: " + userCn);

			ldapContext = getContext(userCn, pwd);

			// Specify the Base for the search
			String searchBase = getSearchBase();

			String searchFilter = getParam(Params.LDAP_SEARCH_FILTER, "(&(objectClass=user)(sAMAccountName=" + login + "))");
			if (searchFilter.contains("%loginOrMail%")) {
				searchFilter = searchFilter.replaceAll("%loginOrMail%", login);
			}

			// Create the search controls
			SearchControls searchCtls = new SearchControls();

			// Specify the attributes to return
			String attrs = getParam(Params.LDAP_RETURN_ATTRS, "sn, givenName, samAccountName, userPrincipalName, MAIL");
			if (StringUtils.isNotEmpty(attrs) && StringUtils.contains(attrs, ",")) {
				searchCtls.setReturningAttributes(StringUtils.split(attrs, ","));
			}

			// Specify the search scope
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			LDAPUser user = find(ldapContext, searchBase, searchFilter, searchCtls);

			log("<< LDAPUtil login: " + user);

			return user;

		} catch (Exception e) {
			log.error("Erro de autenticação no LDAP:" + e.getMessage(), e);
			return null;
		} finally {
			if (ldapContext != null) {
				try {
					ldapContext.close();
				} catch (NamingException e) {
					log.error("Erro de NamingException no LDAP:" + e.getMessage(), e);
				}
			}
		}
	}

	public String getSearchBase() {
		return getParam(Params.LDAP_SEARCH_BASE, "dc=visanet,dc=corp");
	}

	public LDAPUser findUser(String login) {
		// Find login
		try {
			log(">> LDAPUtil findUser: " + login);

			String loginAdmin = getParam(Params.LDAP_SECURITY_PRINCIPAL, "CN=Jaime Peres Servidone Nagase,OU=Users,OU=Alphaville,OU=Sites,DC=visanet,DC=corp");
			String pwdAdmin = getParam(Params.LDAP_SECURITY_PASSWD, "livetouch2013");
			ldapContext = getContext(loginAdmin, pwdAdmin);

			// Create the search controls
			SearchControls searchCtls = new SearchControls();

			// Specify the attributes to return
			String attrs = getParam(Params.LDAP_RETURN_ATTRS, "sn, givenName, samAccountName, userPrincipalName, MAIL");
			if (StringUtils.isNotEmpty(attrs) && StringUtils.contains(attrs, ",")) {
				String[] split = StringUtils.split(attrs, ",");
				searchCtls.setReturningAttributes(split);
			}

			// Specify the search scope
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			// Specify the Base for the search
			String searchBase = getSearchBase();

			// Find by sAMAccountName
			String searchFilter = getParam(Params.LDAP_SEARCH_FILTER, "(&(objectClass=user)(sAMAccountName=" + login + "))");
			if (searchFilter.contains("%loginOrMail%")) {
				searchFilter = searchFilter.replaceAll("%loginOrMail%", login);
			}

			// if(searchFilter.contains("%passwd%")) {
			// searchFilter = searchFilter.replaceAll("%passwd%", pwd);
			// }

			LDAPUser user = find(ldapContext, searchBase, searchFilter, searchCtls);

			log("<< LDAPUtil login: " + user);

			return user;

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return null;
		} finally {
			if (ldapContext != null) {
				try {
					ldapContext.close();
				} catch (NamingException e) {
					log.error(e.getMessage(), e);
				}
			}
		}

	}

	private LDAPUser find(DirContext ctx, String searchBase, String searchFilter, SearchControls searchControls) throws NamingException {

		log(">> LDAPUtil find: searchBase [" + searchBase + "], searchFilter [" + searchFilter + "] ");

		NamingEnumeration<SearchResult> results = ctx.search(searchBase, searchFilter, searchControls);

		SearchResult searchResult = null;
		
		if (results.hasMoreElements()) {
			searchResult = (SearchResult) results.next();

			LDAPUser user = new LDAPUser(searchResult,searchBase);
			log("<< LDAPUtil find LDAPUser: " + user);
			
			return user;
		}

		return null;
	}

	private String getParam(String key, String defaultValue) {
		if (params == null) {
			throw new IllegalArgumentException("Params is null");
		}
		String value = params.get(key, defaultValue);
		log("getParam [" + key + "] > [" + value + "]");
		return value;
	}

	private void log(String string) {
		log.debug(string);
	}
}
