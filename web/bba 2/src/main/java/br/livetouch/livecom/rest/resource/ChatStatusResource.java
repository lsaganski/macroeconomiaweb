package br.livetouch.livecom.rest.resource;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.StatusChat;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.StatusChatService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.ChatStatusVO;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

/**
 * GET
 * http://localhost:8080/livecom/rest/v1/chatStatus/list
 * 
 * GET (lista status usuario)
 * http://localhost:8080/livecom/rest/v1/chatStatus/list/user/2
 * 
 * (Status Usuario)
 * http://localhost:8080/livecom/rest/v1/chatStatus/user/2524/status
 * http://livecom.livetouchdev.com.br/rest/v1/chatStatus/user/2524/status
 * 
 * POST = /user
 * DELETE = /user/{idStatus}
 * 
 * @author rlech
 *
 */
@Path("/v1/chatStatus")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class ChatStatusResource {

	protected static final Logger log = Log.getLogger(ChatStatusResource.class);
	
	@Autowired
	protected StatusChatService statusChatService;
	
	@Autowired
	protected UsuarioService usuarioService;
	
	@GET
	@Path("/list")
	public List<ChatStatusVO> getAll() {
		List<StatusChat> list = statusChatService.findAll();
		return ChatStatusVO.create(list);
	}
	
	@GET
	@Path("/list/user/{id}")
	public List<ChatStatusVO> get(@PathParam("id") long id) {
		Usuario user = usuarioService.get(id);
		List<StatusChat> list = statusChatService.findAll(user);
		return ChatStatusVO.create(list);
	}
	
	@GET
	@Path("/user/{id}/status")
	public Object getStatusUser(@PathParam("id") Long userId, @QueryParam("wsVersion") String wsVersion) {
		Usuario user = usuarioService.get(userId);
		if(user == null) {
			if(!StringUtils.isEmpty(wsVersion) && StringUtils.equals(wsVersion, "3")) {
				br.livetouch.livecom.web.pages.ws.Response r = br.livetouch.livecom.web.pages.ws.Response.error("ERROR");
				return r;
			}
			return Response.ok(MessageResult.error("Usuario não existe ou não informado")).build();
		}
		StatusChat statusChat = user.getStatusChat();
		ChatStatusVO vo = new ChatStatusVO(statusChat);

		UserInfoVO userInfo = Livecom.getInstance().getUserInfo(userId);
		if(userInfo != null) {
			vo.setTipoLogin(userInfo.getTipoLogin());
			vo.setDataLogin(userInfo.getDataLoginString());
			vo.setDataUltTransacao(userInfo.getDataUltTransacaoString());
			vo.setDataLoginChat(userInfo.getDataLoginChatString());
			vo.setDataUltChat(userInfo.getDataUltChatString());

			vo.setSessions(userInfo.getListSession());
		}

		boolean chatOn = Livecom.getInstance().isUsuarioLogadoChat(userId);
		vo.setChatOn(chatOn);
		
		boolean away = Livecom.getInstance().isUsuarioLogadoChatAway(userId);
		vo.setAway(away);
		
		if(!StringUtils.isEmpty(wsVersion) && StringUtils.equals(wsVersion, "3")) {
			br.livetouch.livecom.web.pages.ws.Response r = br.livetouch.livecom.web.pages.ws.Response.ok("OK");
			r.chatStatus = vo;
			return r;
		}
		
		return Response.status(Status.OK).entity(vo).build();
	}
	
	@POST
	@Path("/user/{id}/status/{idStatus}")
	public Response saveStatusUser(@PathParam("id") Long id, @PathParam("idStatus") Long idStatus) {
		Usuario user = usuarioService.get(id);
		if(user == null) {
			return Response.ok(MessageResult.error("Usuario não enconstrado.")).build();
		}
		
		StatusChat statusChat = statusChatService.get(idStatus);
		if(statusChat == null) {
			return Response.ok(MessageResult.error("Status chat não existe.")).build();
		}
		
		if(statusChat.getUsuario() != null) {
			// Valida se o status é teu.
			if(! statusChat.getUsuario().equals(user)) {
				return Response.ok(MessageResult.error("Usuario não corresponde ao chat.")).build();
			}
		}
		user.setStatusChat(statusChat);
		try {
			usuarioService.saveOrUpdate(null,user);
		} catch (DomainException e) {
			return Response.ok(MessageResult.error("Erro ao salvar usuário.")).build();
		}
		
		ChatStatusVO vo = new ChatStatusVO(statusChat);
		return Response.status(Status.OK).entity(vo).build();
	}
	
	/**
	 * http://192.168.43.159:8080/livecom/rest/v1/chatStatus/user/5
	 * 
	 * @param idStatus
	 * @return
	 */
	@DELETE
	@Path("/user/{id}/status/{idStatus}")
	public Response deleteStatusUser(@PathParam("id") Long id, @PathParam("idStatus") Long idStatus) {
		Usuario user = usuarioService.get(id);
		if(user == null) {
			return Response.ok(MessageResult.error("Usuario não encontrado")).build();
		}
		
		StatusChat statusChat = statusChatService.get(idStatus);
		if(statusChat == null) {
			return Response.ok(MessageResult.error("Status chat não encontrado")).build();
		}
		
		if(statusChat.getUsuario() != null) {
			// Valida se o status é teu.
			if(! statusChat.getUsuario().equals(user)) {
				return Response.ok(MessageResult.error("Usuario não corresponde ao Statuschat")).build();
			}
		}
		
		try {
			StatusChat statusChatAtual = user.getStatusChat();
			if(statusChatAtual != null) {
				if(statusChat.getId() == statusChatAtual.getId()) {
					user.setStatus(null);
					usuarioService.saveOrUpdate(null,user);
				}
			}
			statusChatService.delete(statusChat);
			return Response.status(Status.OK).build();
		} catch (DomainException e) {
			return Response.ok(MessageResult.error("Não foi possivel deletar o StatusChat")).build();
		}
	}
	
	/**
		{
		 id: 6,
		 nome: 'Atualizando um registro',
		 usuario: {
		   id: 102
		 }
		}
	 * @param statusChat
	 * @return
	 */
	@POST
	@Path("/user")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response post(StatusChat statusChat) {
		if(statusChat == null) {
			return Response.ok("Status inválido").build();
		}
		try {
			log.debug("Cadastrando o status " + statusChat.getNome());
			statusChatService.saveOrUpdate(statusChat);
			
			if(statusChat.getUsuario() != null) {
				Usuario usuario = usuarioService.get(statusChat.getId());
				if(usuario != null) {
					usuario.setStatusChat(statusChat);
					usuarioService.saveOrUpdate(null,usuario);
				}
			}
			log.debug("Cadastro realizado com sucesso " + statusChat.getId());
		} catch (DomainException e) {
			log.error("Não foi possivel realizar o cadastro do status " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel cadastrar o Statuschat")).build();
		}
		return Response.status(Status.OK).entity(statusChat).build();
	}
	
	@PUT
	@Path("/user")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response put(StatusChat statusChat) {
		try {
			if(statusChat.getId() == null) {
				log.error("Id não foi enviado");
				return Response.ok(MessageResult.error("Usuario inexistente")).build();
			}
			log.debug("Atualizando o status " + statusChat.getId());
			statusChatService.saveOrUpdate(statusChat);
			
			if(statusChat.getUsuario() != null) {
				Usuario usuario = usuarioService.get(statusChat.getId());
				if(usuario != null) {
					usuario.setStatusChat(statusChat);
					usuarioService.saveOrUpdate(null,usuario);
				}
			}
			
			log.debug("Status atualizado com sucesso " + statusChat.getId());
		} catch (DomainException e) {
			log.error("Não foi possivel realizar o atualizar o status " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar o Statuschat")).build();
		}
		return Response.status(Status.OK).entity(statusChat).build();
	}
}