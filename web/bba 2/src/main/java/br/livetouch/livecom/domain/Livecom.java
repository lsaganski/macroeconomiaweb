package br.livetouch.livecom.domain;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import akka.actor.ActorRef;
import br.infra.util.Log;
import br.livetouch.livecom.chatAkka.AkkaChatServer;
import br.livetouch.livecom.chatAkka.DateUtils;
import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.domain.vo.UserInfoSession;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.pushserver.lib.PushService;

public class Livecom {

	protected static final Logger log = Log.getLogger(Livecom.class);

	private static boolean DEV_MODE_ON = false;
	public static final Livecom instance = new Livecom();

	public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";

	public static final Long MIN_RANGE = 1000000000000000L;
	public static final Long MAX_RANGE = 9999999999999999L;

	private Map<Long, UserInfoVO> mapUserInfo = new HashMap<Long, UserInfoVO>();

	private boolean chatOn;

	private Livecom() {

	}

	public static Livecom getInstance() {
		return instance;
	}

	public void add(UserInfoVO u) {
		if (u != null && u.getId() != null) {
			mapUserInfo.put(u.getId(), u);
		}
	}

	public boolean isUsuarioLogadoChat(Long userId) {
		UserInfoVO userInfo = getUserInfo(userId);
		if (userInfo != null) {
			List<UserInfoSession> sessions = userInfo.getListSession();
			if(sessions != null) {
				for (UserInfoSession s : sessions) {
					if(s.isLogadoChat()) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public boolean isUsuarioLogadoChatAway(Long userId) {
		UserInfoVO userInfo = getUserInfo(userId);
		if (userInfo != null) {
			List<UserInfoSession> sessions = userInfo.getListSession();
			if(sessions != null) {
				for (UserInfoSession s : sessions) {
					if(s.isAway()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public UserInfoVO getUserInfoChat(Long idUsuario) {
		boolean logadoChat = isUsuarioLogadoChat(idUsuario);
		log.debug("getUserActorChat: " + idUsuario + " logado: " + logadoChat);
		if (logadoChat) {
			UserInfoVO userInfo = getUserInfo(idUsuario);
			log.debug("<< getUserActorChat: " + userInfo);
			return userInfo;
		}
		return null;
	}

	/**
	 * Busca este ActorRef nas sessions do UserInfo.
	 * 
	 * Limpa dados do Chat, deixa UserInfoVO vivo.
	 * 
	 * @param userId
	 * @param chatActor
	 */
	public void removeSessionChat(Long userId, ActorRef chatActor) {
		UserInfoVO userInfo = getUserInfo(userId);
		if (userInfo != null && chatActor != null) {

			Map<String, UserInfoSession> map = userInfo.getMapLogins();

			if(map != null) {
				Set<String> keys = map.keySet();
				for (String key : keys) {
					UserInfoSession session = map.get(key);
					// Procura o ActorRef correto
					if(session.isActorRef(chatActor)) {
						// Limpa dados do Chat, deixa UserInfoVO vivo
						session.setDataLoginChat(null);
						session.setActorRef(null);
						break;
					}
				}
			}
		}
	}

	public void removeUserInfo(Long userId) {
		UserInfoVO userInfo = getUserInfo(userId);
		removeUserInfo(userInfo);
	}

	public void removeUserInfo(UserInfoVO userInfo) {
		if (userInfo != null) {
			log.debug("remove: " + userInfo);

			stopChat(userInfo.getId());

			mapUserInfo.remove(userInfo.getId());
		}
	}

	public void removeUserInfo(UserInfoVO userInfo, String so) {
		if (userInfo != null) {
			log.debug("remove: " + userInfo);

			// Busca novamente para ter certeza de que está completo.
			Long userId = userInfo.getId();
			userInfo = getUserInfo(userId);

			if (userInfo != null) {
				
				userInfo.removeSession(so);
				
				List<UserInfoSession> sessions = userInfo.getListSession();

				if(sessions == null || sessions.isEmpty()) {

					mapUserInfo.remove(userId);

					stopChat(userInfo.getId());

				}
			}
		}
	}

	public void stopChat(Long userId) {
		if (AkkaChatServer.actorSystem != null) {
			log.debug("stopUserActor: " + userId);
			if (userId != null && userId > 0) {
				AkkaHelper.stopActor(userId);
			} 
		}
	}

	public List<UserInfoVO> getUsuarios() {
		return new ArrayList<UserInfoVO>(mapUserInfo.values());
	}

	public List<UserInfoVO> getUsuarios(Empresa empresa) {
		List<UserInfoVO> users = new ArrayList<UserInfoVO>();
		if(empresa == null) {
			return users;
		}

		if (empresa != null) {
			Set<Long> keys = mapUserInfo.keySet();
			for (Long id : keys) {
				UserInfoVO userInfo = mapUserInfo.get(id);
				if(userInfo != null) {
					Long empresaId = userInfo.getEmpresaId();
					if(empresaId != null) {
						if (empresaId.equals(empresa.getId())) {
							users.add(userInfo);
						}
					}
				}
			}
		}
		
		Collections.sort(users,new Comparator<UserInfoVO>() {
			@Override
			public int compare(UserInfoVO o1, UserInfoVO o2) {
				boolean b1 = o1 != null && o2 != null && o1.getDataUltTransacao() != null && o2.getDataUltTransacao() != null;
				boolean b2 = o1 != null && o2 != null && o1.getDataLogin() != null && o2.getDataLogin() != null;
				int compareTo = 0;
				if(b1) {
					if(DateUtils.isMaior(o2.getDataUltTransacao(), o1.getDataUltTransacao())) {
						compareTo = 1;
					}
				} else if(b2) {
					if(DateUtils.isMaior(o2.getDataLogin(), o1.getDataLogin())) {
						compareTo = 1;
					}
				}
				return compareTo;
			}
		});

		return users;
	}

	public UserInfoVO getUserInfo(String login) {
		for (UserInfoVO userInfo : mapUserInfo.values()) {
			if (userInfo.getLogin().equals(login)) {
				log.debug("<< getUserInfo by login (" + login + "): " + userInfo);
				return userInfo;
			}
		}
		return null;
	}

	public UserInfoVO getUserInfo(Long idUsuario) {
		UserInfoVO userInfo = idUsuario != null ? mapUserInfo.get(idUsuario) : null;
		return userInfo;
	}
	
	public UserInfoVO getUserInfoByWsToken(String wstoken, String so) {

		if (StringUtils.isNotEmpty(wstoken)) {
			List<UserInfoVO> usuarios = Livecom.getInstance().getUsuarios();
			for (UserInfoVO userInfo : usuarios) {

				if(so != null) {
					UserInfoSession session = userInfo.getUserSession(so);
					if (session != null && StringUtils.equals(session.getWstoken(), wstoken)) {
						return userInfo;
					}
				} else {
					List<UserInfoSession> sessions = userInfo.getListSession();
					if(sessions != null) {
						for (UserInfoSession s : sessions) {
							if(StringUtils.equals(wstoken, s.getWstoken())) {
								return userInfo;
							}
						}
					}
				}
			}
		}

		return null;
	}

	public boolean isDevMode() {
		if (!DEV_MODE_ON) {
			return false;
		}
		// else {
		// return true;
		// }
		return isLocalhost();
	}

	public boolean isLocalhost() {
		try {
			String hostName = Inet4Address.getLocalHost().getHostName();
			if ("RicardoLecheta".equals(hostName)) {
				return true;
			} else if ("Felipe".equals(hostName)) {
				return true;
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Somente UserInfo que possui ActorRef
	 * 
	 * @param empresa
	 * @return
	 */
	public List<UserInfoVO> getUsuariosChat(Empresa empresa) {
		List<UserInfoVO> list = new ArrayList<UserInfoVO>();
		List<UserInfoVO> users = getUsuarios(empresa);
		if (users != null) {
			for (UserInfoVO userInfo : users) {
				if (userInfo.isLogadoChat()) {
					list.add(userInfo);
				}
			}
		}

		return list;
	}
	
//	public Map<Long, ActorRef> getMapChat() {
//		return mapChatUserActor;
//	}

	public static Locale getLocale() {
		return new Locale("pt", "BR");
	}

	public static String getHostName() {
		try {
			return Inet4Address.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "";
		}
	}

	public static String getHostAddress() {
		try {
			return Inet4Address.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			return "";
		}
	}

	public static PushService getPushService() {
		ParametrosMap p = ParametrosMap.getInstance();
		return getPushService(p);
	}

	public static PushService getPushService(ParametrosMap p) {
		String server = p.get(Params.PUSH_SERVER_HOST, "push.livetouchdev.com.br");
		String path = p.get(Params.PUSH_SERVER_CONTEXTPATH, "");
		String protocol = p.get(Params.PUSH_SERVER_PROTOCOL, "https");
		PushService service = new PushService(server, path, protocol);
		return service;
	}

	public static String getUrlBuildAndroidBeta(Empresa e) {
		ParametrosMap param = ParametrosMap.getInstance(e);
		String url = param.get(Params.BUILD_BETA_ANDROID_URL);
		return url;
	}
	
	public static String getUrlBuildAdrnoidAPK(Empresa e) {
		ParametrosMap param = ParametrosMap.getInstance(e);
		String url = param.get(Params.BUILD_APP_ANDROID_URL);
		return url;
	}
	
	public static String getUrlBuildAndroid(Empresa e) {
		String url = getUrlBuildAndroidBeta(e);
		if(StringUtils.isEmpty(url)) {
			url = getUrlBuildAdrnoidAPK(e);
		}
		return url;
	}
	
	public static String getUrlBuilIOS(Empresa e) {
		ParametrosMap param = ParametrosMap.getInstance(e);
		String url = param.get(Params.BUILD_APP_IOS_URL);
		return url;
	}
	
	public void setChatOn(boolean chatOn) {
		this.chatOn = chatOn;
	}
	
	public boolean isChatOn() {
		return chatOn;
	}

	public Boolean isUsuarioAwayChat(Long userId) {
		UserInfoVO userInfo = getUserInfo(userId);
		if (userInfo != null) {
			List<UserInfoSession> sessions = userInfo.getListSession();
			if(sessions != null) {
				for (UserInfoSession s : sessions) {
					if(s.isAway()) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public boolean hasPermissao(String permissao, Usuario u) {
		UserInfoVO user = getUserInfo(u.getLogin());
		if(user != null) {
			return user.isPermissao(permissao);
		}
		
		if(u.getPermissao() == null) {
			return false;
		}
		
		Map<String, Boolean> permissoes = new HashMap<String, Boolean>();
		for (PerfilPermissao p : u.getPermissao().getPermissoes()) {
			if(p.getPermissao() != null) {
				permissoes.put(p.getPermissao().getCodigo(), p.isLigado());
			}
		}
		
		Boolean hasPermission = permissoes.get(permissao);
		return hasPermission != null ? hasPermission : false;
	}

}
