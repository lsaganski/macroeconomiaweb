package br.livetouch.livecom.web.pages.ws;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.EntityField;
import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.FileVO;
import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.domain.vo.UploadRequest;
import br.livetouch.livecom.domain.vo.UploadResponse;
import br.livetouch.livecom.utils.UploadHelper;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.FieldSet;
import net.sf.click.control.FileField;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class UploadFilePage extends WebServiceXmlJsonPage {

	public Form form = new Form();

	public List<Tag> tags;
	public String dir;
	public String descricao;
	public String fileUrl;
	public String fileBase64;
	public String fileName;
	private LongField tId;
	private TextField tThumbIds;
	private LongField tDestaqueAtual;
	private TextField tMode;
	private TextField tDestaque;
	private TextField tResize;

	//usado para fazer crop de image
	private TextField tEixoX;
	private TextField tEixoY;
	private TextField tWidth;
	private TextField tHeight;

	private TextField tAngle;

	private IntegerField tOrdem;

	private FileField fileField;

	private EntityField<Post> tPost;

	private EntityField<Comentario> tComentario;

	private EntityField<Mensagem> tMensagem;

	private TextField tTags;
	private TextField tDimensao;

	private UsuarioLoginField tUser;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		FieldSet set = new FieldSet("Dados do arquivo");
		form.add(set);
		set.add(tId = new LongField("id", "id = Opcional, informe o id para atualizar os dados do arquivo"));
		set.add(tThumbIds = new TextField("thumb_ids", "Thumb ids separados por virgula"));
		set.add(tDestaqueAtual = new LongField("id_destaque_atual", "id_destaque_atual = Opcional, informe o id para remover este arquivo como destaque do post"));
		set.add(tUser = new UsuarioLoginField("user_id", true, usuarioService, getEmpresa()));
		set.add(new TextField("descricao", "descrição"));
		set.add(new TextField("dir", "Diretório Amazon", true));
		set.add(tMode = new TextField("mode", "mode: xml, json"));

		set = new FieldSet("Arquivo","Upload por Multpart, URL ou Base64.");
		set.add(fileField = new FileField("fileField"));
		set.add(new TextField("fileUrl", "fileUrl = se informar a URL o sistema faz download do arquivo."));
		set.add(new TextField("fileBase64", "fileBase64 = arquivo em Base64."));
		set.add(new TextField("fileName", "fileName = nome do arquivo, obrigatório para Base64."));
		set.add(tDestaque = new TextField("destaque", "Arquivo de destaque do post"));
		set.add(tResize = new TextField("resize", "Redimensionamento"));
		form.add(set);

		set = new FieldSet("Associar arquivo com objetos (informe o id do arquivo)");
		form.add(set);
		set.add(tPost = new EntityField<Post>("post_id", false, postService));
		set.add(tComentario = new EntityField<Comentario>("comentario_id", false, comentarioService));
		set.add(tMensagem = new EntityField<Mensagem>("mensagem_id", false, mensagemService));
		set.add(tTags = new TextField("tags"));
		set.add(tDimensao = new TextField("dimensao"));
		set.add(tOrdem = new IntegerField("ordem"));
		tMode.setValue("json");


		set = new FieldSet("Informacoes de Crop para imagem");
		form.add(set);
		set.add(tEixoX = new TextField("eixoX"));
		set.add(tEixoY = new TextField("eixoY"));
		set.add(tWidth = new TextField("width"));
		set.add(tHeight = new TextField("height"));
		set.add(tAngle = new TextField("angle"));

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));


		form.add(new Submit("Upload File"));

		setFormTextWidth(form, 350);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {

			log("params: " + getRequestParams());

			Usuario user = tUser.getEntity();
			if (user == null) {
				return MensagemResult.erro("Usuário inválido.");
			}
			
			Post post = tPost.getEntity();
			Comentario comentario = tComentario.getEntity();
			Mensagem mensagem = tMensagem.getEntity();

			UploadRequest req = new UploadRequest();
			if(StringUtils.isNotEmpty(tDestaqueAtual.getValue())) {
				req.destaqueAtual = tDestaqueAtual.getLong();
			}

			req.id = tId.getLong();
			req.thumbIds = getListIds(tThumbIds.getName());
			req.usuario = user;
			req.fileItem = fileField.getFileItem();
			req.fileUrl = fileUrl;
			req.fileBase64 = fileBase64;
			req.fileName = fileName;
			req.post = post;
			req.comentario = comentario;
			req.mensagem = mensagem;
			req.tagsString = tTags.getValue();
			req.dimensao = tDimensao.getValue();
			req.empresa = getEmpresa();
			req.dir = dir;
			req.descricao = descricao;
			req.ordem = tOrdem.getInteger();

			if(StringUtils.isNumeric(tEixoX.getValue())) {
                req.eixoX = Integer.parseInt(tEixoX.getValue());
            }

            if(StringUtils.isNumeric(tEixoY.getValue())) {
                req.eixoY = Integer.parseInt(tEixoY.getValue());
            }

            if(StringUtils.isNumeric(tWidth.getValue())) {
                req.width = Integer.parseInt(tWidth.getValue());
            }

            if(StringUtils.isNumeric(tEixoY.getValue())) {
                req.height = Integer.parseInt(tHeight.getValue());
            }

            if(StringUtils.isNumeric(tAngle.getValue())) {
            	req.angle = Integer.parseInt(tAngle.getValue());
            }

			if(StringUtils.equalsIgnoreCase(tDestaque.getValue(), "true")) {
				req.destaque = true;
			}

			if(StringUtils.equalsIgnoreCase(tResize.getValue(), "true")) {
				req.resize = true;
			}

			try {
				UploadResponse response = uploadService.upload(req);
				
				if(response != null && response.arquivo != null) {
					
					Arquivo a = response.arquivo;
					
					FileVO f = new FileVO();
					if(a != null) {
						a.setDimensao(req.dimensao);
						arquivoService.saveOrUpdate(a);
						f.setArquivo(a, true);
						log("<< FileVO [" + f.getId() + "]");
					}
					return f;
				} else {
					return MensagemResult.erro("Erro ao fazer upload.");
				}
			} catch(DomainException e) {
				return MensagemResult.erro(e.getMessage());
			} catch(Exception e) {
				e.printStackTrace();
				return MensagemResult.erro(e.getMessage());
			}
		}

		return new MensagemResult("Error", "Invalid params: " + getFormError(form));
	}

	@Override
	protected void log(String string) {
		UploadHelper.log.debug(string);
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("file", FileVO.class);
		x.alias("thumb", ThumbVO.class);
		super.xstream(x);
	}

	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
