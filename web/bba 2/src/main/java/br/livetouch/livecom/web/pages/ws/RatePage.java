package br.livetouch.livecom.web.pages.ws;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.PostField;
import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Rate;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RateVO;
import net.sf.click.control.FieldSet;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LongField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class RatePage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	public Long id;
	private TextField tMode;
	private TextField tAction;
	private UsuarioLoginField tUser;
	private PostField postField;
	public List<RateVO> rates;
	public IntegerField tValor;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tUser = new UsuarioLoginField("user", true,usuarioService, getEmpresa()));
		form.add(tAction = new TextField("action","list , save , delete"));
		form.add(tValor = new IntegerField("value","1,2,3,4,5"));
		form.add(tMode = new TextField("mode"));
		
		FieldSet set = new FieldSet("Save / Delete");
		set.add(new LongField("id"));
		set.add(postField = new PostField(postService));
		form.add(set);
		
		tAction.setValue("list");

		tUser.setFocus(true);

		tMode.setValue("json");

		form.add(new Submit("consultar"));

		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario u = tUser.getEntity();
			if(u == null) {
				return new MensagemResult("NOK" ,"Usuario nao encontrado");
			}

			boolean actionList = "list".equalsIgnoreCase(tAction.getValue());
			boolean actionSave = "save".equalsIgnoreCase(tAction.getValue());
			boolean actionDelete = "delete".equalsIgnoreCase(tAction.getValue());
			
			if(actionList) {
				List<Rate> list = rateService.findAllByUser(u);
				rates = RateVO.toListVO(list);
				return rates;
			}  else if(actionSave || actionDelete) {

				Post p = postField.getPost();
				if(p == null) {
					return new MensagemResult("NOK" ,"Post nao encontrado.");
				}
				
				Rate v = rateService.findByUserPost(u,p);

				if(v == null) {
					v = new Rate();
					v.setPost(p);
					v.setUsuario(u);
				}

				if(actionSave) {
					v.setData(new Date());
					v.setValue(tValor.getInteger());

					p.incrementRate();

					postService.saveOrUpdate(getUsuario(),p);
					rateService.saveOrUpdate(v);
					
					RateVO vo = new RateVO();
					vo.rateMedia = rateService.findMedia(p);
					vo.rateCount = rateService.countByPost(p);;
					vo.setRate(v);
					
					return vo;
				} else {
					rateService.delete(v);
					
					return new MensagemResult("OK","Rate excluido com sucesso.");
				}
			}

			return new MensagemResult("NOK","Invalid action.");
		}

		return new MensagemResult("NOK","Invalid parameters.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("rate", RateVO.class);
		super.xstream(x);
	}
}