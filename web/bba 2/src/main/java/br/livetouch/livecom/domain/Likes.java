package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.utils.DateUtils;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Likes extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "LIKES_SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id", nullable = true)
	private Post post;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "comentario_id", nullable = true)
	private Comentario comentario;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario usuario;
	
	private boolean favorito;

	private Boolean lidaNotification;
	
	private Date data;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Boolean getLidaNotification() {
		if(lidaNotification == null) {
			return false;
		}
		return lidaNotification;
	}
	public void setLidaNotification(Boolean lidaNotification) {
		this.lidaNotification = lidaNotification;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Post getPost() {
		return post;
	}
	
	public void setPost(Post post) {
		this.post = post;
	}
	
	public void setFavorito(boolean favorito) {
		this.favorito = favorito;
	}
	
	public boolean isFavorito() {
		return favorito;
	}
	
	public Comentario getComentario() {
		return comentario;
	}
	
	public void setComentario(Comentario comentario) {
		this.comentario = comentario;
	}

	@Override
	public String toString() {
		return "Favorito [post=" + post + ", usuario=" + usuario + ", favorito=" + favorito + "]";
	}
	

	public void setData(Date data) {
		this.data = data;
	}
	
	public Date getData() {
		if(data == null) {
			data = new Date();
		}
		return data;
	}
	
	public String getDataString() {
		return net.livetouch.extras.util.DateUtils.toString(data, "dd/MM/yyyy HH:mm:ss");
	}
	
	public String getDataStringHojeOntem() {
		return DateUtils.toDateStringHojeOntem(getData());
	}
	
	public static List<Long> getIds(Collection<Likes> list) {
		List<Long> ids = new ArrayList<Long>();
		if(list == null || list.size() == 0) {
			return ids;
		}
		for (net.livetouch.tiger.ddd.Entity e : list) {
			ids.add(e.getId());
		}
		return ids;
	}
}
