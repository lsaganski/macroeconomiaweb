package br.livetouch.livecom.jobs.importacao;

import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.text.Normalizer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;

/**
 * @author rlech
 *
 */
public class JobImportacaoUtil {
	public static final Logger log = Log.getLogger(JobImportacaoUtil.class);

	static String getNomeFileSemEmpresa(File dir) {
		if(dir == null) {
			return ""; 
		}
		return dir.getName().replaceAll("[^a-zA-Z.]", "");
	}
	
	public static Long getIdEmpresa(File file) {
		String strEmpresa = file.getName().replaceAll("[^0-9]", "");
		if (StringUtils.isEmpty(strEmpresa) || StringUtils.isBlank(strEmpresa)) {
			throw new IllegalArgumentException("O nome do arquivo ["+file.getName()+"] não contem o id de uma empresa");
		}
		return Long.parseLong(strEmpresa);
	}
	
	public static File moveToOldDir(File dir, File f) throws IOException {
		// Mover para /livecom/out/old
		File dirOld = new File(dir, "old");
		if (!dirOld.exists()) {
			boolean ok = dirOld.mkdir();
			log("Dir ["+dirOld+"] criado [" + ok + "]: " + dirOld);
		}

		File toFile = new File(dirOld, f.getName());
		return move(f,toFile);
	}
	
	public static File move(File f, File toFile) throws IOException {
		log("Fazendo backup do arquivo [" + f.getName() + "] para [" + toFile + "]");
		FileUtils.copyFile(f, toFile);
		if (toFile.exists()) {
			log("Backup feito com sucesso");
		} else {
			log("Erro ao fazer o backup.");
		}

		log("Excluindo arquivo [" + f.getName() + "]");
		FileUtils.deleteQuietly(f);
		
		return toFile;
	}
	
	private static void log(String s) {
		log.debug(s);		
	}

	public static boolean isFileGrupo(File file) {
		String fileName = ParametrosMap.getInstance().get("importacao.arquivo.grupos", "grupos.csv");
		return getNomeFileSemEmpresa(file).equals(fileName);
	}
	
	public static boolean isFileDiretoria(File file) {
		String fileName = ParametrosMap.getInstance().get(Params.IMPORTACAO_ARQUIVO_DIRETORIAS, "diretorias.csv");
		return getNomeFileSemEmpresa(file).equals(fileName);
	}
	
	public static boolean isFileArea(File file) {
		String fileName = ParametrosMap.getInstance().get(Params.IMPORTACAO_ARQUIVO_AREAS, "areas.csv");
		return getNomeFileSemEmpresa(file).equals(fileName);
	}
	
	public static boolean isFileUsuario(File file) {
		String fileName = ParametrosMap.getInstance().get(Params.IMPORTACAO_ARQUIVO_USUARIOS, "usuarios.csv");
		return getNomeFileSemEmpresa(file).equals(fileName);
	}
	
	public static boolean isFileTransacao(File file) {
		String fileName = ParametrosMap.getInstance().get(Params.IMPORTACAO_ARQUIVO_TRANSACAO, "transacoes.csv");
		return getNomeFileSemEmpresa(file).equals(fileName);
	}
	
	public static boolean isFileCargo(File file) {
		String fileName = ParametrosMap.getInstance().get(Params.IMPORTACAO_ARQUIVO_CARGO, "cargos.csv");
		return getNomeFileSemEmpresa(file).equals(fileName);
	}

	public static boolean isFileConvite(File file) {
		String fileName = ParametrosMap.getInstance().get(Params.IMPORTACAO_ARQUIVO_CONVITE, "convite.csv");
		return getNomeFileSemEmpresa(file).equals(fileName);
	}
	
	public static String get(String[] split, int col) {
		if (col == 0) {
			throw new RuntimeException("A coluna do split começa em zero.");
		}
		if(col > split.length) {
			return null;
		}
		return StringUtils.trim(split[col - 1]);
	}

	public static String getInt(String[] split, int col) {
		return String.valueOf(NumberUtils.toInt(get(split, col), 0));
	}

	public static String getInt(String[] split, int col, int length) {
		String sInt = String.valueOf(NumberUtils.toInt(get(split, col), 0));
		return StringUtils.leftPad(sInt, length, "0");
	}

	public static float getFloat(String[] split, int col) {
		String replace = StringUtils.replace(get(split, col), ".", "");
		replace = StringUtils.replace(replace, ",", ".");
		float f = NumberUtils.toFloat(replace, 0);
		return f;
	};

	public static Boolean isImportFile(File f) {
		String nomeArquivo = f.getName();
		nomeArquivo = JobImportacaoUtil.getNomeFileSemEmpresa(f);
		ParametrosMap map = ParametrosMap.getInstance(JobImportacaoUtil.getIdEmpresa(f));
		return isImportFile(nomeArquivo, map);
	}
	
	public static Boolean isImportFile(String fileName, ParametrosMap map) {
 		String filesNames = getFilesNamesImport(map);
		if(!StringUtils.contains(fileName, ".csv")) {
			fileName += ".csv";
		}
		return StringUtils.contains(filesNames, fileName);
	}
	
	public static String getFilesNamesImport(ParametrosMap map) {
		String defaultFiles = "usuarios.csv, grupos.csv, diretorias.csv, areas.csv, transacoes.csv, cargos.csv, convite.csv,";
		String filesNames = defaultFiles + map.get(Params.ARQUIVOS_IMPORTACAO, "");
		return filesNames;
	}
	
	public static String getFloatString(String[] split, int i) {
		float f = getFloat(split, i);
		return String.valueOf(f);
	};
	
	/**
	 * Remove acentos e caracteres especiais do arquivo
	 * EX: usuários(1) -> usuario
	 * @param nomeArquivo
	 * @return
	 */
	public static String normalizeNomeArquivo(String nomeArquivo) {
		if(StringUtils.isEmpty(nomeArquivo)) {
			return "";
		}
		
		if(nomeArquivo.contains(".")) {
			String[] split = StringUtils.split(nomeArquivo, ".");
			if(split == null) {
				return "";
			}
			nomeArquivo = split[0];
		}
		
		nomeArquivo = Normalizer.normalize(nomeArquivo, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		nomeArquivo = StringUtils.lowerCase(nomeArquivo.replaceAll("[^a-zA-Z]", ""));
		return nomeArquivo;
	}

	public static Time getTime(String[] split, int i) {
		String string = get(split, i);
		if(StringUtils.isNotEmpty(string)) {
			String[] s = StringUtils.split(string, ":");
			if(s.length == 2) {
				string += ":00";
			}
			return Time.valueOf(string);
		}
		return null;
	}
	
}
