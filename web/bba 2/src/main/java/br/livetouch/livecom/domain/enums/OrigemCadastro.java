package br.livetouch.livecom.domain.enums;

/**
 * @author Ricardo Lecheta
 * @created 23/02/2013
 */
public enum OrigemCadastro {

	IMPORTADO("Importado","Importado via planilha pelo Livecom"),
	WEB("Cadastrado pela web","Cadastrado pelo Livecom");
	
	private final String s;
	private final String desc2;

	OrigemCadastro(String s,String desc2){
		this.s = s;
		this.desc2 = desc2;
	}

	@Override
	public String toString() {
		return s != null ? s : "?";
	}

	public String getDesc() {
		return s;
	}
	
	public String getDesc2() {
		return desc2;
	}
}
