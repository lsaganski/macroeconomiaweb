package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.ConversaNotification;
import br.livetouch.livecom.domain.repository.ConversaNotificationRepository;
import br.livetouch.livecom.domain.service.ConversaNotificationService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ConversaNotificationServiceImpl implements ConversaNotificationService {
	@Autowired
	private ConversaNotificationRepository rep;

	@Override
	public ConversaNotification get(Long id) {
		return rep.get(id);
	}

	@Override
	public List<ConversaNotification> findAll() {
		return rep.findAll();
	}

	@Override
	public void saveOrUpdate(ConversaNotification c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public void delete(ConversaNotification c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public ConversaNotification findByConversationUser(Long conversationId, Long userId) {
		return rep.findByConversationUser(conversationId, userId);
	}

}
