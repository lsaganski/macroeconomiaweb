package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Campo;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.vo.CamposMapCache;
import net.livetouch.tiger.ddd.DomainException;

public interface CampoService extends Service<Campo> {

	List<Campo> findAll(Empresa e);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Campo c, Empresa e) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Campo c, Empresa e) throws DomainException;

	CamposMapCache init(Empresa e);
	
	Campo findByNameValid(Campo campo, Empresa empresa);
}
