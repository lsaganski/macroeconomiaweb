package br.livetouch.livecom.web.pages.training.admin;


import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboCategoriaPostChildren;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.utils.FileExtensionUtils;
import br.livetouch.livecom.web.pages.training.TrainingUtil;
import net.livetouch.click.control.IdField;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class PostPage extends TrainingAdminPage {

	public Form form = new Form();
	public String msgErro;
	public Long id;

	public int page;

	public Post post;
	private TextField tTags;

	@Override
	public void onInit() {
		super.onInit();

		form();
		
		if (id != null) {
			post = postService.get(id);

			form.copyFrom(post);
			
			String s = post.getTags();
			tTags.setValue(s);
			
		} else {
			post = new Post();
		}
	}
	
	public void form(){
		form.add(new IdField());
		
		form.add(new ComboCategoriaPostChildren("categoria",categoriaPostService, getEmpresa()));

		TextField t = new TextField("titulo");
		t.setAttribute("autocomplete", "off");
		t.setFocus(true);
		form.add(t);
		
		t = new TextField("mensagem");
		t.setAttribute("autocomplete", "off");
		form.add(t);
		
		t = new TextField("urlVideo");
		t.setAttribute("autocomplete", "off");
		form.add(t);
		
		t = new TextField("videoPlayer");
		t.setAttribute("autocomplete", "off");
		form.add(t);

		t = new TextField("urlImagem");
		t.setAttribute("autocomplete", "off");
		form.add(t);

//		ComboCategoriaPost comboCateg = new ComboCategoriaPost(categoriaPostService);
//		comboCateg.getOptionList().add(0,new Option("","- Categoria -"));
//		form.add(comboCateg);
		
		tTags = new TextField("tags_id");
		tTags.setAttribute("autocomplete", "off");
		form.add(tTags);

		Submit tsalvar = new Submit("salvar", this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);
		
		if (id != null) {
			Submit tDeletar = new Submit("deletar", this, "deletar");
			tDeletar.setAttribute("class", "botao btSalvar");
			form.add(tDeletar);
		}
		Submit cancelar = new Submit("cancelar",this,"cancelar");
		cancelar.setAttribute("class", "botao");
		form.add(cancelar);

		setFormTextWidth(form);
		TrainingUtil.setTrainingStyle(form);
	}
	
	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				post = id != null ? postService.get(id) : new Post();
				form.copyTo(post);
				
				String tags = getParam("tags_id");
				if(StringUtils.isNotEmpty(tags)) {
					List<Tag> tagsList = tagService.getTagsAndSave(tags, getEmpresa());
					post.setTagsList(new HashSet<Tag>(tagsList));
				}

				if(id == null) {
					post.setData(new Date());
					post.setDataPublicacao(new Date());
				}
				post.setDataUpdated(new Date());
				
				if(StringUtils.contains(post.getUrlVideo(), "youtube.com")) {
					post.setVideoPlayer("youtube");
				} else if(StringUtils.contains(post.getUrlVideo(), "vimeo.com")) {
					post.setVideoPlayer("vimeo");
				} else if(StringUtils.isEmpty(post.getVideoPlayer())) {
					post.setVideoPlayer(FileExtensionUtils.getExtensao(post.getUrlVideo()));
				}
				
//				post.setUsuario(getUserInfo());
				postService.saveOrUpdate(getUsuario(),post);

				setFlashAttribute("msg",  "Post ["+post.getTitulo()+"] salvo com sucesso.");
				setRedirect(PostsPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean cancelar() {
		setRedirect(PostsPage.class);
		return true;
	}

	public boolean deletar() {
		try {
			try {
				post = id != null ? postService.get(id) : new Post();

				postService.delete(getUsuario(),post,false);

				setFlashAttribute("msg",  "Post ["+post.getTitulo()+"] deletado com sucesso.");
				setRedirect(PostsPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = "Entidade possui relacionamentos. Não foi possível excluir.";
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
