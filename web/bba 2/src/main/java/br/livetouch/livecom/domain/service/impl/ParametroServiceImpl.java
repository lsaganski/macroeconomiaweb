package br.livetouch.livecom.domain.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.enums.TipoParametro;
import br.livetouch.livecom.domain.repository.ParametroRepository;
import br.livetouch.livecom.domain.service.ParametroService;
import br.livetouch.livecom.domain.vo.ParametroVO;
import br.livetouch.livecom.email.EmailArgs;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class ParametroServiceImpl implements ParametroService {
	@Autowired
	private ParametroRepository rep;

	@Override
	public Parametro get(Long id) {
		return rep.get(id);
	}
	
	@Override
	public ParametrosMap init() {
		List<Parametro> list = findAll();
		ParametrosMap.clear();
		ParametrosMap params = ParametrosMap.setParametros(list);
		return params;
	}

	@Override
	public void saveOrUpdate(Parametro c) throws DomainException {
		rep.saveOrUpdate(c);
		init();
	}

	@Override
	public List<Parametro> findAll() {
		return rep.findAll(false);
	}

	@Override
	public void delete(Parametro c) throws DomainException {
		rep.delete(c);
		init();
	}

	@Override
	public Parametro findByNome(String nome, Empresa empresa) {
		return rep.findByNome(nome, empresa);
	}

	@Override
	public EmailArgs getEmailArgs(Empresa empresa) {
		EmailArgs e = new EmailArgs();
		e.setSmtpServer(getValue("smtp_server", empresa));
		e.setSmtpPort(getValue("smtp_port", empresa));
		e.setSmtpUser(getValue("smtp_user", empresa));
		e.setSmtpPwd(getValue("smtp_password", empresa));
		e.setSmtpEmailAdmin(getValue("smtp_email", empresa));
		
		if(e.getSmtpServer() == null && e.getSmtpPort() == null) {
			e.setSmtpServer("smtp.telecorp.com.br");
			e.setSmtpPort("25");
			e.setSmtpUser("wasys@telecorp.com.br");
			e.setSmtpPwd("passSMTP");
			e.setSmtpEmailAdmin("livecom@livetouch.com.br");	
		}
		
		return e;
	}

	private String getValue(String s, Empresa empresa) {
		Parametro p = findByNome(s, empresa);
		return p != null ? p.getValor() : null;
	}

	@Override
	public String get(String name, Empresa empresa) {
		return getValue(name, empresa);
	}

	@Override
	public Map<String, String> findAllParametrosKeyPair() {
		List<Parametro> list = findAll();
		Map<String, String> params = new HashMap<String, String>();
		for (Parametro p : list) {
			params.put(p.getNome(), p.getValor());
		}
		return params;
	}

	@Override
	public List<Parametro> findDefaultByNomeNotIn(List<String> nomes) {
		return rep.findDefaultByNomeNotIn(nomes);
	}

	@Override
	public List<Parametro> findAllByEmpresa(Long id) {
		return rep.findAllByEmpresa(id);
	}

	@Override
	public List<ParametroVO> parametrosAutocomplete(Long empresa, String nome) {
		return rep.parametrosAutocomplete(empresa, nome);
	}

	@Override
	public List<Parametro> findAllParametros(Empresa e, Integer page, Integer max, boolean count) throws DomainException {
		return rep.findAllParametros(e, page, max, count);
	}

	@Override
	public long getCount(Empresa e, int page, int max, boolean count) throws DomainException {
		return rep.getCount(e, page, max, count);
	}

	@Override
	public List<Parametro> findAllParametros(Empresa e) throws DomainException {
		return rep.findAllParametros(e);
	}

	@Override
	public long getCount() {
		return rep.getCount();
	}

	@Override
	public List<Parametro> findAllParametrosByTipo(TipoParametro tipo, Empresa empresa) {
		return rep.findAllParametrosByTipo(tipo, empresa);
	}

	@Override
	public List<Parametro> findAll(Long empresaId, TipoParametro tipoParametro) {
		return rep.findAll(empresaId, tipoParametro);
	}

	@Override
	public void saveOrUpdate(List<Parametro> parametros, Empresa empresa) {
		for (Parametro parametro : parametros) {
			Parametro newParam = rep.findByNome(parametro.getNome(), empresa);
			if(newParam == null) {
				newParam = new Parametro();
				newParam.setNome(parametro.getNome());
			}
			newParam.setTipoParametro(parametro.getTipoParametro());
			newParam.setValor(parametro.getValor());
			newParam.setEmpresa(empresa);
			rep.saveOrUpdate(newParam);
		}
		init();
	}

	@Override
	public List<Parametro> findParametrosMobile(Empresa empresa) {
		return rep.findParametrosMobile(empresa);
	}

    @Override
    public List<Parametro> findMobileDefault() {
        return rep.findMobileDefault();
    }

    @Override
    public void saveCloneDefaults(List<Parametro> parametrosEmpresa, Empresa empresa) {
        for (Parametro parametro : parametrosEmpresa) {
            Parametro newParam = rep.findByNome(parametro.getNome(), empresa);
            if(newParam == null) {
                newParam = new Parametro();
                newParam.setNome(parametro.getNome());
                newParam.setTipoParametro(parametro.getTipoParametro());
                newParam.setValor(parametro.getValor());
                newParam.setEmpresa(empresa);
                rep.saveOrUpdate(newParam);
            }
        }
        init();
    }
}
