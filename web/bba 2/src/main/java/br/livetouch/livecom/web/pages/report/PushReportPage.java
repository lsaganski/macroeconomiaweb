package br.livetouch.livecom.web.pages.report;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboTipoPush;
import br.livetouch.livecom.domain.vo.PushReportFiltro;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

@Controller
@Scope("prototype")
public class PushReportPage extends RelatorioPage {
	
	public PushReportFiltro filtro;
	public int page;
	public Select status = new Select();
	public Form form = new Form();
	public String dataInicio;
	public String dataFim;
	public ComboTipoPush comboTipo;
	
	
	@Override
	public void onInit() {
		super.onInit();

		if (clear) {
			getContext().removeSessionAttribute(PushReportFiltro.SESSION_FILTRO_KEY);
			filtro = new PushReportFiltro();
			filtro.setDataInicialString(DateUtils.toString(br.livetouch.livecom.utils.DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
			filtro.setDataFinalString(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		} else {
			filtro = (PushReportFiltro) getContext().getSessionAttribute(PushReportFiltro.SESSION_FILTRO_KEY);
		}

		form();
	}
	
	public void form() {
		

		form.add(comboTipo = new ComboTipoPush());
		
		status.add(new Option(true, "Enviado"));
		status.add(new Option(false, "Não Enviado"));
		form.add(status);

		
	}

	public boolean exportar() throws DomainException {
		filtro = (PushReportFiltro) getContext().getSessionAttribute(PushReportFiltro.SESSION_FILTRO_KEY);
		String csv = pushReportService.csvPushReport(filtro, getEmpresa());
		download(csv, "push-report.csv");
		return true;
	}
	
}
