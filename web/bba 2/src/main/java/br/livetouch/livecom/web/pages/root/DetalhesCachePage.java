package br.livetouch.livecom.web.pages.root;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.service.Service;
import br.livetouch.livecom.domain.vo.CacheVO;
import br.livetouch.livecom.web.pages.LivecomRootPage;
import net.livetouch.hibernate.HibernateUtil;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

@Controller
@Scope("prototype")
public class DetalhesCachePage extends LivecomRootPage {

	public String key;
	public Long id;
	public Cache cache;
	public String cacheName;
	public String className;
	public String cacheGuid;
	public String cacheSize;
	public String cacheStatus;
	public String cacheCount;
	public List<CacheVO> cacheSons = new ArrayList<CacheVO>();
	public CacheVO vo;
	public String descricao;

	@Override
	public void onGet() {
		cache = HibernateUtil.getCache(key);

		cacheName = cache.getName();
		className = StringUtils.substringAfterLast(cacheName, "domain.");
		cacheGuid = cache.getGuid();
		cacheSize = String.valueOf(cache.getSize());
		cacheStatus = String.valueOf(cache.getStatus());
		cacheCount = String.valueOf(cache.getKeys().size());

		for (Object o : cache.getKeys()) {
			if (o != null) {
				id = Long.valueOf(StringUtils.substringAfterLast(o.toString(), "#"));
				Element element = cache.get(o);
				if(element != null) {
					descricao = classeToString(className, id, element);
					vo = new CacheVO(element.getKey().toString(), String.valueOf(element.getCreationTime()), String.valueOf(element.getHitCount()), element.getValue().toString(), String.valueOf(element.getVersion()), id, descricao, String.valueOf(element.getSerializedSize()));
					cacheSons.add(vo);
				}
			}
		}
	}

	public String classeToString(String classe, Long id, Element e) {

		if (StringUtils.isEmpty(classe) || classe == null) {
			return "";
		}

		if (id == null) {
			return "";
		}

		if (e == null || e.getValue() == null) {
			return "";
		}

		String toString = "";

		switch (classe) {
		case "Arquivo":
			toString = arquivoService.get(id) != null ? arquivoService.get(id).toString() : "";
			break;
		case "Comentario.arquivos": {
			toString = comentarioService.get(id) != null ? comentarioService.get(id).toString() : "";
			if (subclasseToString(e, arquivoService) != null) {
				toString += subclasseToString(e, arquivoService);
			}
			break;
		}
		case "Mensagem.statusUsuarios": {
			toString = mensagemService.get(id) != null ? mensagemService.get(id).toString() : "";
			if (subclasseToString(e, statusMensagemUsuarioService) != null) {
				toString += subclasseToString(e, statusMensagemUsuarioService);
			}
			break;
		}
		case "Post.arquivos": {
			toString = postService.get(id) != null ? postService.get(id).toString() : "";
			if (subclasseToString(e, arquivoService) != null) {
				toString += subclasseToString(e, arquivoService);
			}
			break;
		}
		case "Post.favoritos": {
			toString = postService.get(id) != null ? postService.get(id).toString() : "";
			if (subclasseToString(e, favoritoService) != null) {
				toString += subclasseToString(e, favoritoService);
			}
			break;
		}
		case "Post.likes": {
			toString = postService.get(id) != null ? postService.get(id).toString() : "";
			if (subclasseToString(e, likeService) != null) {
				toString += subclasseToString(e, likeService);
			}
			break;
		}
		case "Post.comentarios": {
			toString = postService.get(id) != null ? postService.get(id).toString() : "";
			if (subclasseToString(e, comentarioService) != null) {
				toString += subclasseToString(e, comentarioService);
			}
			break;
		}
		case "Post.grupos": {
			toString = postService.get(id) != null ? postService.get(id).toString() : "";
			if (subclasseToString(e, grupoService) != null) {
				toString += subclasseToString(e, grupoService);
			}
			break;
		}
		case "Mensagem.arquivos": {
			toString = mensagemService.get(id) != null ? mensagemService.get(id).toString() : "";
			if (subclasseToString(e, arquivoService) != null) {
				toString += subclasseToString(e, arquivoService);
			}
			break;
		}
		case "Arquivo.tags": {
			toString = arquivoService.get(id) != null ? arquivoService.get(id).toString() : "";
			if (subclasseToString(e, tagService) != null) {
				toString += subclasseToString(e, tagService);
			}
			break;
		}
		case "statusUsuarios":
			toString = statusMensagemUsuarioService.get(id) != null ? statusMensagemUsuarioService.get(id).toString() : "";
			break;
		case "favoritos":
			toString = favoritoService.get(id) != null ? favoritoService.get(id).toString() : "";
			break;
		case "likes":
			toString = likeService.get(id) != null ? likeService.get(id).toString() : "";
			break;
		case "tags":
			toString = tagService.get(id) != null ? tagService.get(id).toString() : "";
			break;
		case "comentarios":
			toString = comentarioService.get(id) != null ? comentarioService.get(id).toString() : "";
			break;
		case "grupos":
			toString = grupoService.get(id) != null ? grupoService.get(id).toString() : "";
			break;
		case "Usuario":
			toString = usuarioService.get(id) != null ? usuarioService.get(id).toString() : "";
			break;
		case "Cidade":
			toString = cidadeService.get(id) != null ? cidadeService.get(id).toString() : "";
			break;
		case "Favorito":
			toString = favoritoService.get(id) != null ? favoritoService.get(id).toString() : "";
			break;
		case "CategoriaPost":
			toString = categoriaPostService.get(id) != null ? categoriaPostService.get(id).toString() : "";
			break;
		case "Historico":
			toString = historicoService.get(id) != null ? historicoService.get(id).toString() : "";
			break;
		case "Comentario":
			toString = comentarioService.get(id) != null ? comentarioService.get(id).toString() : "";
			break;
		case "Faq":
			toString = faqService.get(id) != null ? faqService.get(id).toString() : "";
			break;
		case "Likes":
			toString = likeService.get(id) != null ? likeService.get(id).toString() : "";
			break;
		case "Funcao":
			toString = funcaoService.get(id) != null ? funcaoService.get(id).toString() : "";
			break;
		case "Canal":
			toString = canalService.get(id) != null ? canalService.get(id).toString() : "";
			break;
		case "Chapeu":
			toString = chapeuService.get(id) != null ? chapeuService.get(id).toString() : "";
			break;
		case "Complemento":
			toString = complementoService.get(id) != null ? complementoService.get(id).toString() : "";
			break;
		case "Empresa":
			toString = empresaService.get(id) != null ? empresaService.get(id).toString() : "";
			break;
		case "Parametro":
			toString = parametroService.get(id) != null ? parametroService.get(id).toString() : "";
			break;
		case "Mensagem":
			toString = mensagemService.get(id) != null ? mensagemService.get(id).toString() : "";
			break;
		case "MensagemConversa":
			toString = mensagemService.get(id) != null ? mensagemService.get(id).toString() : "";
			break;
		case "Tag":
			toString = tagService.get(id) != null ? tagService.get(id).toString() : "";
			break;
		case "Perfil":
			toString = perfilService.get(id) != null ? perfilService.get(id).toString() : "";
			break;
		case "UsuarioAcessoMural":
			toString = usuarioService.get(id) != null ? usuarioService.get(id).toString() : "";
			break;
		case "ArquivoThumb":
			toString = arquivoService.get(id) != null ? arquivoService.get(id).toString() : "";
			break;
		case "Permissao":
			toString = perfilService.get(id) != null ? perfilService.get(id).toString() : "";
			break;
		case "Post":
			toString = postService.get(id) != null ? postService.get(id).toString() : "";
			break;
		case "Notification":
			toString = notificationService.get(id) != null ? notificationService.get(id).toString() : "";
			break;
		case "GrupoUsuarios":
			toString = grupoService.get(id) != null ? grupoService.get(id).toString() : "";
			break;
		case "Playlist":
			toString = playlistService.get(id) != null ? playlistService.get(id).toString() : "";
			break;
		case "Formacao":
			toString = formacaoService.get(id) != null ? formacaoService.get(id).toString() : "";
			break;
		case "PostDestaque":
//			toString = postService.get(id) != null ? postViewService.get(id).toString() : "";
			break;
		case "Grupo":
			toString = grupoService.get(id) != null ? grupoService.get(id).toString() : "";
			break;
		case "Campo":
			toString = campoService.get(id) != null ? campoService.get(id).toString() : "";
			break;

		default:
			break;
		}

		return toString;
	}

	public String subclasseToString(Element e, @SuppressWarnings("rawtypes") Service service) {

		String array[] = null;
		String value = "";

		if (e == null) {
			return null;
		}

		if (StringUtils.isEmpty(e.getValue().toString())) {
			return null;
		}

		String list = e.getValue().toString().replace("[", "");
		list = list.replace("]", "");

		if (StringUtils.isEmpty(list)) {
			return null;
		}

		array = list.split(",");

		if (array.length > 0) {
			for (String s : array) {
				id = Long.valueOf(s.trim());
				value += service.get(id) != null ? service.get(id).toString() : "";
			}
			return value;
		}

		return null;
	}
}
