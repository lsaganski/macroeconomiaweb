package br.livetouch.livecom.jobs.importacao;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.dialect.Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Service;

import br.infra.livetouch.dialect.OracleDialect;
import br.infra.util.Log;
import br.infra.util.SQLUtils;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ImportarArquivoResponse;
import br.livetouch.livecom.domain.LinhaImportacao;
import br.livetouch.livecom.domain.LogImportacao;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.service.AreaService;
import br.livetouch.livecom.domain.service.DiretoriaService;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.LinhaImportacaoService;
import br.livetouch.livecom.domain.service.LogImportacaoService;
import br.livetouch.livecom.jobs.importacao.ImportarArquivosJob.JobInfo;
import net.livetouch.hibernate.HibernateUtil;
import net.livetouch.tiger.ddd.DomainException;

/**
 * Importa arquivo padrão de grupos do livecom.
 * 
 * @author Usuário
 * 
 */
@Service
public class ImportarArquivoAreaWork extends ImportacaoArquivoWork {

	protected static final Logger log = Log.getLogger("importacao_area");
	protected static final Logger logError = Log.getLogger("importacao_area_error");
	private File file;
	private Scanner scanner;
	private ImportarArquivoResponse response;

	private int linhas;
	private double porcentagem;
	
	private Long idEmpresa;

	@Autowired
	protected LogImportacaoService logImportacaoService;
	@Autowired
	protected LinhaImportacaoService linhaImportacaoService;

	@Autowired
	protected AreaService areaService;
	@Autowired
	protected DiretoriaService diretoriaService;

	@Autowired
	protected EmpresaService empresaService;
	
	private HashMap<String, Long> mapAreas;
	private HashMap<String, Long> mapDiretorias;
	private Session session;
	private List<String> linhaErros = new ArrayList<String>();
	
	private Dialect dialect;
	private Boolean importacaoByNome;

	@Override
	public void init(Session session, File file, Dialect dialect) throws DomainException, IOException {
		this.session = session;
		this.file = file;
		this.dialect = dialect;

		idEmpresa = JobImportacaoUtil.getIdEmpresa(file);
		
		linhas = countLines(file.getAbsolutePath());
		JobInfo jobInfo = JobInfo.getInstance(idEmpresa);
		jobInfo.setSuccessful(true);
		jobInfo.setFim(linhas);
		jobInfo.setTemArquivo(true);
		jobInfo.setStatus("");
		
		
		Empresa empresa = empresaService.get(idEmpresa);
		importacaoByNome = "1".equals(ParametrosMap.getInstance(idEmpresa).get(Params.IMPORTACAO_ARQUIVO_BY_NOME, "0"));
		if (empresa == null) {
			throw new DomainException("Empresa não cadastrada no sistema");
		}
		jobInfo.setEmpresa(empresa);

		// Salva map com todas as areas
		log.debug("Carregando areas");
		List<Object[]> cods = importacaoByNome ? areaService.findIdNomeAreas(idEmpresa) : areaService.findIdCodAreas(idEmpresa);
		mapAreas = new HashMap<String, Long>();
		for (Object[] area : cods) {
			Long id = (Long) area[0];
			String cod = (String) area[1];
			if (StringUtils.isEmpty(cod)) {
				cod = id.toString();
			}
			mapAreas.put(cod, id);
		}
		log.debug("Existem [" + mapAreas.size() + "] areas na base.");

		// Salva map com todas as diretorias
		log.debug("Carregando diretorias");
		cods = importacaoByNome ? diretoriaService.findIdNomeDiretorias(idEmpresa) :  diretoriaService.findIdCodDiretorias(idEmpresa);
		mapDiretorias = new HashMap<String, Long>();
		for (Object[] dire : cods) {
			Long id = (Long) dire[0];
			String cod = (String) dire[1];
			if (StringUtils.isEmpty(cod)) {
				cod = id.toString();
			}
			mapDiretorias.put(cod, id);
		}
		log.debug("Existem [" + mapDiretorias.size() + "] diretorias na base.");
	}

	@Override
	public void execute(Connection conn) throws SQLException {
		if (file == null || !file.exists()) {
			throw new IllegalArgumentException("Arquivo inválido");
		}

		response = new ImportarArquivoResponse();
		int countOk = 0;
		int countError = 0;

		long timeA = System.currentTimeMillis();

		Date inicio = new Date();
		
		/**	 https://docs.oracle.com/javase/7/docs/api/java/nio/charset/Charset.html  **/
		String set = ParametrosMap.getInstance(idEmpresa).get(Params.IMPORTACAO_ARQUIVO_CHARSET, "UTF-8");
		final Charset ENCODING = Charset.isSupported(set) ? Charset.forName(set): Charset.forName("UTF-8");
		Path path = Paths.get(file.getAbsolutePath());

		PreparedStatement stmtInsert = createStatementInsert(dialect, conn);
		PreparedStatement stmtUpdate = conn.prepareStatement("update area set codigo=?,nome=?,diretoria_id=?, empresa_id=? where id=?");

		int row = 0;
		JobInfo jobInfo = JobInfo.getInstance(idEmpresa);
		try {
			log.debug("Iniciou ImportarArquivoWork com Batch ...");

			scanner = new Scanner(path, ENCODING.name());

			List<Long> idsAreas = new ArrayList<>();

			boolean insert = false;
			String linha = "";
			jobInfo.setStatus("Inserindo areas");
			while (scanner.hasNextLine()) {
				try {
					row++;

					boolean header = row == 1;

					linha = scanner.nextLine();
					String[] split = StringUtils.split(linha, ";");

					if (!header) {
						String cod = JobImportacaoUtil.get(split, 1);
						String nome = JobImportacaoUtil.get(split, 2);
						Long diretoriaId = null;
						if (split.length == 3) {
							diretoriaId = mapDiretorias.get(JobImportacaoUtil.get(split, 3));
						}

						insert = false;

						Long id = mapAreas.get(importacaoByNome ? nome : cod);
						if (id == null) {

							insert = true;

							// insert into grupo
							log("insert row[" + row + "] " + cod);
							stmtInsert.setString(1, cod);
							stmtInsert.setString(2, nome);
							if (diretoriaId == null) {
								stmtInsert.setNull(3, java.sql.Types.BIGINT);
							} else {
								stmtInsert.setLong(3, diretoriaId);
							}
							stmtInsert.setLong(4, idEmpresa);

							stmtInsert.execute();

							id = SQLUtils.getGeneratedId(stmtInsert);
							if (id == null) {
								linhaErros.add(linha);
								logError.error("Erro row [" + row + "] ao inserir area: " + cod);
								continue;
							}
							mapAreas.put(importacaoByNome ? nome : cod, id);
						} else {
							// update
							stmtUpdate.setString(1, cod);
							stmtUpdate.setString(2, nome);

							if (diretoriaId == null) {
								stmtUpdate.setNull(3, java.sql.Types.BIGINT);
							} else {
								stmtUpdate.setLong(3, diretoriaId);
							}
							stmtUpdate.setLong(4, idEmpresa);
							stmtUpdate.setLong(5, id);
							stmtUpdate.execute();
							log("update row[" + row + "] " + cod);
						}

						if (id != null) {
							idsAreas.add(id);

							// count
							if (insert) {
								insert = false;
								response.countInsert++;
							} else {
								response.countUpdate++;
							}
						}

						countOk++;

						porcentagem = ((double) (row - 1) * 100) / (double) linhas;
						jobInfo.setPorcentagem(porcentagem);
						jobInfo.setAtual(row);
					} else {
						jobInfo.setHeader(linha);
					}

				} catch (Exception e) {
					countError++;
					linhaErros.add(linha);
					log.error("Erro area linha [" + row + "]: " + e.getMessage(), e);
				}
			}

			log.debug("total areas insert/update: " + idsAreas.size());

			long timeB = System.currentTimeMillis();
			log("TimeB min: " + (timeB - timeA) / 1000 / 60);

		} catch (Exception e) {
			jobInfo.setSuccessful(false);
			String msgError = "Erro importar areas row[" + row + "] " + e.getMessage();
			logError.error(msgError, e);
			throw new SQLException(msgError, e);
		} finally {
			
			JdbcUtils.closeStatement(stmtInsert);
			JdbcUtils.closeStatement(stmtUpdate);

			HibernateUtil.clearCache(session);

			response.countOk = countOk;
			response.countError = countError;
			response.nomeArquivo = JobImportacaoUtil.getNomeFileSemEmpresa(file);
			jobInfo.setResponse(response);
			
			// Log Importacao
			finish(jobInfo);
			
			log.debug("Início: " + inicio);
			log.debug("Final: " + new Date());
			long timeC = System.currentTimeMillis();
			log("TimeC min: " + (timeC - timeA) / 1000 / 60);
		}
	}

	private void finish(JobInfo jobInfo) {
		LogImportacao logImportacao = new LogImportacao(jobInfo);
		try {
			logImportacaoService.saveOrUpdate(logImportacao);
			jobInfo.response.idArquivo = logImportacao.getId();
			for (String erros : linhaErros) {
				LinhaImportacao linhaImportacao = new LinhaImportacao();
				linhaImportacao.setLinha(erros);
				linhaImportacao.setLogImportacao(logImportacao);
				linhaImportacaoService.saveOrUpdate(linhaImportacao);
			}
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			jobInfo.setSuccessful(false);
		}

		jobInfo.setStatus("Fim da Importação");
		jobInfo.setTemArquivo(false);
		jobInfo.setFim(0);
		jobInfo.setAtual(0);
		jobInfo.setAtual(0);
	}

	private void log(String string) {
		JobInfo.getInstance(idEmpresa).lastMessageImportacao = string;
		log.debug(string);
	}

	public ImportarArquivoResponse getResponse() {
		return response;
	}
	
	private PreparedStatement createStatementInsert(Dialect dialect, Connection conn) throws SQLException {
		
		if(dialect instanceof OracleDialect) {
			return conn.prepareStatement("insert into area (id,codigo,nome,diretoria_id, empresa_id, padrao) VALUES(AREA_SEQ.nextval,?,?,?,?, 0)", new String [] {"id"});
		}

		return conn.prepareStatement("insert into area (codigo,nome,diretoria_id, empresa_id, padrao) VALUES(?,?,?,?, 0)", Statement.RETURN_GENERATED_KEYS);
		
	}
}
