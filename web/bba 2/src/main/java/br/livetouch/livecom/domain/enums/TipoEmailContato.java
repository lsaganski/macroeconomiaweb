package br.livetouch.livecom.domain.enums;

/**
 * @author Ricardo Lecheta
 * @created 14/03/2013
 */
public enum TipoEmailContato {

	DUVIDA,
	SUGESTAO,
	RECLAMACAO,
	SEMINARIO,
	OUTROS
}
