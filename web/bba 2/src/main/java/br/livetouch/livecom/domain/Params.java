package br.livetouch.livecom.domain;

/**
select nome,valor from parametro where empresa_id=1 and
(
	nome like "security.host%"
)
 */
public abstract class Params {

	/**
	 * Charset para importar arquivo csv.
	 * Default UTF-8 ou ISO-8859-1
	 */
	public static final String IMPORTACAO_ARQUIVO_CHARSET = "importacao.arquivo.charset";
	
	/**
	 * Charset para importar arquivo csv.
	 * Default UTF-8 ou ISO-8859-1
	 */
	public static final String EXPORTACAO_ARQUIVO_CHARSET = "exportacao.arquivo.charset";
	
	/**
	 * Pattern campo de data para importar arquivo csv.
	 * Default dd/MM/yyyy ou ddMMyyyy, yyyy-MM-dd...
	 */
	public static final String IMPORTACAO_ARQUIVO_DATE_PATTERN = "importacao.arquivo.date.pattern";
	/**
	 * Codigo da permissao padrao para importar arquivo csv usuarios.
	 * Default 3 (visualização) ou 1 (admin), 2 (usuario), 4 (master), 5 (usuario sem postar)
	 */
	public static final String IMPORTACAO_PERMISSAO = "importacao.permissao_id";
	
	/**
	 * Senha padrão após importar arquivo csv usuarios.
	 * Default senha123
	 */
	public static final String USER_SENHA_PADRAO = "user.cadastro.senha";
	public static final String USER_SENHA_RANDOM_SIZE = "user.cadastro.senha.random.length";
	
	public static final String AMAZON_CLOUDFRONT_ON = "amazon.cloudfront.on";
	public static final String AMAZON_CLOUDFRONT_ARQUIVOS = "amazon.cloudfront.arquivos";
	public static final String PASTAENTRADA = "pastaEntrada";
	/**
	 * Logs de importação aparecendo no console
	 * Default 0 ou 1 para ligado
	 */
	public static final String IMPORTACAO_LOG_ON = "importacao.logOn";
	/**
	 * Bucket amazon file manager
	 * Default "livecom"
	 */
	public static final String FILE_MANAGER_AMAZON_BUCKET = "file.manager.amazon.bucket";

	/**
	 * Bucket amazon file manager
	 * Default "livecom-chat"
	 */
	public static final String FILE_MANAGER_AMAZON_BUCKET_CHAT = "file.manager.amazon.bucket.chat";

	/**
	 * Bucket amazon file manager
	 * Default "livecom-chat-resize"
	 */
	public static final String FILE_MANAGER_AMAZON_BUCKET_RESIZE = "file.manager.amazon.bucket.resize";

	/**
	 * Bucket amazon file manager
	 * Default "livecom-chat-resize"
	 */
	public static final String FILE_MANAGER_AMAZON_BUCKET_CHAT_RESIZE = "file.manager.amazon.bucket.chat.resize";
	/**
	 * Server amazon file manager
	 * Default "https://s3-sa-east-1.amazonaws.com"
	 */
	public static final String FILE_MANAGER_AMAZON_SERVER = "file.manager.amazon.server";

	/**
	 * Server amazon file manager lambda (CA)
	 * Default "https://s3-us-west-1.amazonaws.com"
	 */
	public static final String FILE_MANAGER_AMAZON_SERVER_LAMBDA = "file.manager.amazon.server.lambda";
	public static final String AMAZON_S3_ARQUIVOS = "amazon.s3.arquivos";
	
	/**
	 * tentativas de verificar se a thumb foi criada
	 */
	public static final String FILE_AMAZON_TRIES_GET = "file.amazon.tries.get";
	/**
	 * Parametro de mendagem push
	 * Default "%s: %s" ou "[%s] postou uma nova mensagem [%s]"
	 */
	public static final String MSG_PUSH_MENSAGEM = "msg.push.mensagem";
	/**
	 * Parametro de mendagem de imagem push
	 * Default "%s: %s" ou "[%s] postou uma nova mensagem [%s]"
	 */
	public static final String MSG_PUSH_MENSAGEM_IMAGEM = "msg.push.mensagem.imagem";
	/**
	 * Restrição de horario por grupo
	 * Default 0 ou 1 para ligado
	 */
	public static final String GRUPO_RESTRICAO_HORARIO_ON = "grupo.restricao.horario.on";
	/**
	 * Restrição de horario por usuario
	 * Default 0 ou 1 para ligado
	 */
	public static final String USUARIO_RESTRICAO_HORARIO_ON = "usuario.restricao.horario.on";
	/**
	 * Restrição de horario default
	 * Default 0 ou 1 para ligado
	 */
	public static final String HORARIO_GRUPO_DEFAULT_ON = "horario.grupo.default.on";
	/**
	 * Restrição de horario default abertura
	 * Default "09:00"
	 */
	public static final String HORARIO_GRUPO_DEFAULT_ABERTURA = "horario.grupo.default.abertura";
	/**
	 * Restrição de horario default fechamento
	 * Default "14:00"
	 */
	public static final String HORARIO_GRUPO_DEFAULT_FECHAMENTO = "horario.grupo.default.fechamento";
	
	/**
	 * Envio de push
	 * Default 1 ou 0 para desligado
	 */
	public static final String PUSH_ON = "push.on";
	
	/**
	 * Push Silent por projeto
	 */
	public static final String PROJETO_PUSH_SILENT_ON = "projeto.push.silent.on";

	/**
	 * Host do push
	 * Default "push.livetouchdev.com.br"
	 */
	public static final String PUSH_SERVER_HOST = "push.server.host";
	
	/**
	 * Contextpath do push
	 * Default ""
	 */
	public static final String PUSH_SERVER_CONTEXTPATH = "push.server.contextpath";

	/**
	 * Protocolo do push
	 * Default "https"
	 */
	public static final String PUSH_SERVER_PROTOCOL = "push.server.protocol";
	
	/**
	 * Se vai mandar badge de novos posts
	 */
	public static final String PUSH_BADGES_POST_ON = "push.badges.post.on";
	
	/**
	 * Se vai mandar badge de novos comentarios
	 */
	public static final String PUSH_BADGES_COMENTARIO_ON = "push.badges.comentario.on";

	/**
	 * Se vai mandar badge de novos like
	 */
	public static final String PUSH_BADGES_LIKE_ON = "push.badges.like.on";
	
	/**
	 * Se vai mandar badge de novos fav
	 */
	public static final String PUSH_BADGES_FAVORITO_ON = "push.badges.favorito.on";
	
	/**
	 * Se vai mandar badge de novos chat
	 */
	public static final String PUSH_BADGES_CHAT_ON = "push.badges.chat.on";

	/**
	 * Se o autor do comentario recebe push
	 */
	public static final String PUSH_COMENTARIO_AUTOR_RECEBE_ON = "push.comentario.autor.recebe.on";
	
	/**
	 * Se o autor do comentario recebe push
	 */
	public static final String PUSH_POST_AUTOR_RECEBE_ON = "push.post.autor.recebe.on";

	/**
	 * Modo desenvolvimento
	 * Default 0 ou 1 para ligado
	 */
	public static final String DEVMODE_ON = "devmode.on";
	/**
	 * Costumer key
	 * Default MyApplication.CONSUMER_KEY
	 */
	public static final String LIVECOM_CONSUMER_KEY = "livecom.consumer.key";
	/**
	 * Costumer secret
	 * Default MyApplication.CONSUMER_SECRET
	 */
	public static final String LIVECOM_CONSUMER_SECRET = "livecom.consumer.secret";
	/**
	 * Costumer name
	 * Default MyApplication.NAME
	 */
	public static final String LIVECOM_CONSUMER_NAME = "livecom.consumer.name";
	/**
	 * Função padrão
	 * Default "Administrador"
	 */
	public static final String FUNCAO_PADRAO = "funcao.padrao";
	/**
	 * Versão do push 
	 * Default "1" 
	 */
	public static final String PUSH_VERSION = "push.version";
	public static final String BUILDTYPE = "buildType";
	/**
	 * Email do admin 
	 * Default "livecom@livetouch.com.br"
	 */
	public static final String EMAIL_EMAIL_ADMIN = "email.email_admin";
	public static final String EMAIL_CONTATO_LIVETOUCH = "email.contato.livetouch";
	public static final String EMAIL_BCC_TRIAL = "email.bcc.trial";

	public static final String FILE_MANAGER_AMAZON_CREDENTIALS = "file.manager.amazon.credentials";
	/**
	 * Server host 
	 * Default "livetouchdev.com.br"
	 */
	public static final String SERVER_HOST = "server.host";
	/**
	 * Server stmp para envio de email
	 * Default "email-smtp.us-east-1.amazonaws.com"
	 */
	public static final String EMAIL_SMTP_SERVER = "email.smtp_server";
	/**
	 * Porta stmp para envio de email
	 * Default 25
	 */
	public static final String EMAIL_SMTP_PORT = "email.smtp_port";
	/**
	 * User stmp para envio de email
	 * Default "AKIAJYT22PIFZ25K6R5Q"
	 */
	public static final String EMAIL_SMTP_USER = "email.smtp_user";
	/**
	 * Senha para envio de email
	 * Default "AieXbWqtVbQ8Et3xcyeIKkkYIrrDjon5+KZeacjuYfP2"
	 */
	public static final String EMAIL_SMTP_PASSWORD = "email.smtp_password";
	/**
	 * SSL stmp para envio de email
	 * Default "1"
	 */
	public static final String EMAIL_SMTP_SSL = "email.smtp_ssl";
	/**
	 * Ftp file manager
	 * Default "0" ou 1 para ligado
	 */
	public static final String FTP_ON = "ftp.on";
	/**
	 * Server ftp file manager
	 * Default "livecom.livetouchdev.com.br"
	 */
	public static final String FILE_MANAGER_FTP_SERVER = "file.manager.ftp.server";
	/**
	 * Login ftp file manager
	 * Default "livetouch"
	 */
	public static final String FILE_MANAGER_FTP_LOGIN = "file.manager.ftp.login";
	/**
	 * Senha ftp file manager
	 * Default "livetouch@2013"
	 */
	public static final String FILE_MANAGER_FTP_SENHA = "file.manager.ftp.senha";
	/**
	 * Porta ftp file manager
	 * Default 21
	 */
	public static final String FILE_MANAGER_FTP_PORT = "file.manager.ftp.port";
	/**
	 * Workplace ftp file manager
	 * Default "/static"
	 */
	public static final String FILE_MANAGER_FTP_WORKPLACE = "file.manager.ftp.workplace";
	/**
	 * Bucket ftp file manager
	 * Default "livecom"
	 */
	public static final String FILE_MANAGER_FTP_BUCKET = "file.manager.ftp.bucket";
	/**
	 * Protocolo ftp file manager
	 * {http; https; ftp}
	 */
	public static final String FILE_MANAGER_FTP_PROTOCOLO = "file.manager.ftp.protocolo";
	/**
	 * Modo de restrição de horario grupo/usuario
	 */
	public static final String RESTRICAO_HORARIO_MODO = "restricao.horario.modo";

	/**
	 * Push para todos os usuarios dentro do horario permitido pelo grupo padrao
	 * Default 0 ou 1 para ligado
	 */
	public static final String GRUPO_HORARIO_POST = "grupo.horario.post";
	/**
	 * Mensagem padrão para usuario fora do horario
	 */
	public static final String MSG_HORARIO_USUARIO = "msg.horario.usuario";
	/**
	 * Importação de usuários no itau
	 * Default 0 ou 1 para ligado
	 */
	public static final String IMPORTACAO_USUARIO_ITAU = "importacao.usuario.itau";
	/**
	 * Chat on
	 * "yes" ou "1" para ligado
	 */
	public static final String CHAT_ON = "chat_on";
	
	public static final String CHAT_KEEP_ALIVE_ON = "chat.keepAlive.on";
	public static final String CHAT_SEND_KEEP_ALIVE_ON = "chat.sendKeepAlive.on";
	public static final String CHAT_CHECK_KEEP_ALIVE_ON = "chat.checkKeepAlive.on";
	public static final String CHAT_SEND_KEEP_ALIVE_TIME_SECONDS = "chat.sendKeepAlive.timeSeconds";
	public static final String CHAT_CHECK_KEEP_ALIVE_TIME_SECONDS = "chat.checkKeepAlive.timeSeconds";
	
	/**
	 * Grupo id para importacao itauRevista
	 * Default "5"
	 */
	public static final String IMPORTACAO_GRUPO_ID = "importacao.grupo_id";
	/**
	 * Permissão id para importacao itauRevista
	 * Default "3"
	 */
	public static final String IMPORTACAO_PERMISSAO_ID = "importacao.permissao_id";
	/**
	 * Arquivos conhecidos pelo sistema para importação
	 * Default "usuarios.csv, grupos.csv, diretorias.csv, areas.csv, transacoes.csv, " 
	 * (obs.: o nome dos arquivos devem ser separados por virgula)
	 */
	public static final String ARQUIVOS_IMPORTACAO = "arquivos.importacao";
	/**
	 * -> SUBSTITUIDO POR Params.ARQUIVOS_IMPORTACAO <-
	 * Arquivo de diretoria conhecido pelo sistema para importação
	 * Default "diretorias.csv" 
	 */
	public static final String IMPORTACAO_ARQUIVO_DIRETORIAS = "importacao.arquivo.diretorias";
	/**
	 * SUBSTITUIDO POR Params.ARQUIVOS_IMPORTACAO
	 * Arquivo de area conhecido pelo sistema para importação
	 * Default "areas.csv" 
	 */
	public static final String IMPORTACAO_ARQUIVO_AREAS = "importacao.arquivo.areas";
	/**
	 * -> SUBSTITUIDO POR Params.ARQUIVOS_IMPORTACAO <-
	 * Arquivo de usuario conhecido pelo sistema para importação
	 * Default "usuarios.csv" 
	 */
	public static final String IMPORTACAO_ARQUIVO_USUARIOS = "importacao.arquivo.usuarios";
	/**
	 * -> SUBSTITUIDO POR Params.ARQUIVOS_IMPORTACAO <-
	 * Arquivo de transacao conhecido pelo sistema para importação
	 * Default "transacoes.csv" 
	 */
	public static final String IMPORTACAO_ARQUIVO_TRANSACAO = "importacao.arquivo.transacao";
	/**
	 * -> SUBSTITUIDO POR Params.ARQUIVOS_IMPORTACAO <-
	 * Arquivo de transacao conhecido pelo sistema para importação
	 * Default "cargos.csv" 
	 */
	public static final String IMPORTACAO_ARQUIVO_CONVITE = "importacao.arquivo.convite";
	/**
	 * -> SUBSTITUIDO POR Params.ARQUIVOS_IMPORTACAO <-
	 * Arquivo de transacao conhecido pelo sistema para importação
	 * Default "convite.csv" 
	 */
	public static final String IMPORTACAO_ARQUIVO_CARGO = "importacao.arquivo.cargo";
	/**
	 * Chave de acesso amazon file manager
	 * Default "AKIAIFLNTEN23RVG67OQ"
	 */
	public static final String FILE_MANAGER_AMAZON_ACCESSKEY = "file.manager.amazon.accessKey";
	/**
	 * Secret key amazon file manager
	 * Default "CmwxLwaUZyJT4pu9mAN6u2c6dwZmfwoBcKMKPh5v"
	 */
	public static final String FILE_MANAGER_AMAZON_SECRETKEY = "file.manager.amazon.secretKey";
	
	public static final String CONFIG_LOGIN_ON = "config.login.on";
	/**
	 * Configuração de login
	 * Quantidade de chances de login invalido
	 * Default 5
	 */
	public static final String CONFIG_LOGIN_QTDEMAXERROSLOGIN = "config.login.qtdeMaxErrosLogin";
	/**
	 * Configuração de login
	 * Tempo de bloqueio após quantidade CONFIG_LOGIN_QTDEMAXERROSLOGIN de erros de login
	 * Default 30 (para 30 minutos)
	 */
	public static final String CONFIG_LOGIN_TEMPOBLOQUEIOERROLOGIN = "config.login.tempoBloqueioErroLogin";
	
	
	/**
	 * Autocomplete input form login
	 */
	public static final String LOGIN_FORM_AUTOCOMPLETE_ON = "login.form.autocomplete.on";
	
	/**
	 * Captcha form login
	 */
	public static final String LOGIN_FORM_CAPTCHA_ON = "login.form.captcha.on";
	
	/**
	 * Quantidade de tentativas para ativar o captcha no login
	 */
	public static final String LOGIN_FORM_CAPTCHA_QTD = "login.form.captcha.qtd";
	
	/**
	 * Expirar senha inativa
	 */
	public static final String EXPIRAR_SENHA_INATIVIDADE_ON = "expirar.senha.inatividade.on";
	/**
	 * A senha expirará após X dias de inatividade
	 */
	public static final String EXPIRAR_SENHA_INATIVIDADE_QTDEDIAS = "expirar.senha.inatividade.qtdeDias";
	/**
	 * Expirar senha periodicamente
	 */
	public static final String EXPIRAR_SENHA_PERIODICIDADE_ON = "expirar.senha.periodicidade.on";
	/**
	 * A senha expirará periodicamente após X dias
	 */
	public static final String EXPIRAR_SENHA_PERIODICIDADE_QTDEDIAS = "expirar.senha.periodicidade.qtdeDias";
	/**
	 * Site wordpress
	 */
	public static final String WORDPRESS_SITE = "wordpress.site";
	/**
	 * Wordpress On
	 * Default 0 ou 1 para ligado
	 */
	public static final String WORDPRESS_ON = "wordpress.on";
	public static final String VERSION_CONTROL_ON = "version.control.on";
	
	/**
	 * habilita o redirecionamento do usuário para a página de download
	 */
	public static final String REDIRECT_USER_DOWNLOAD_APP_ON = "redirect.user.download.app.on";
	
	/**
	 * habilita o controle de versão feito por terceiros
	 */
	public static final String THIRD_PARTY_VERSION_CONTROL_ON = "third.party.version.control.on";
	
	/**
	 * url para direcionamento - no caso de controle feito por terceiros
	 */
	public static final String THIRD_PARTY_VERSION_CONTROL_URL = "third.party.version.control.url";
	
	// 0.0.1
	public static final String VERSION_CONTROL_ANDROID_NUMBER = "version.control.android.number";
	// 1
	public static final String VERSION_CONTROL_ANDROID_NUMBER_CODE = "version.control.android.numberCode";

	// 0.0.1
	public static final String VERSION_CONTROL_IOS_NUMBER = "version.control.ios.number";
	// 1
	public static final String VERSION_CONTROL_IOS_NUMBER_CODE = "version.control.ios.numberCode";
	/**
	 * Mensagem para controle de versão
	 * Default "Você está com a versão desatualizada do aplicativo"
	 */
	public static final String VERSION_CONTROL_ANDROID_MENSAGEM = "version.control.android.mensagem";
	public static final String VERSION_CONTROL_ANDROID_URLDOWNLOAD = "version.control.android.urlDownload";
	public static final String VERSION_CONTROL_ANDROID_MODE = "version.control.android.mode";					//Versão Informativa
	
	
	public static final String VERSION_CONTROL_IOS_MENSAGEM = "version.control.ios.mensagem";
	public static final String VERSION_CONTROL_IOS_URLDOWNLOAD = "version.control.ios.urlDownload";
	public static final String VERSION_CONTROL_IOS_MODE = "version.control.ios.mode";
			
			
	public static final String VERSION_CONTROL_WEB_TEXT = "version.control.web.text";
	public static final String VERSION_CONTROL_WEB_IMAGE = "version.control.web.image";
	
	public static final String URL_GOOGLE_PLAY_DOWNLOAD = "url.google.play.download";
	public static final String URL_APP_STORE_DOWNLOAD = "url.app.store.download";

	/**
	 * Senha para usuario admin ao criar empresa
	 * Default "admin"
	 */
	public static final String SENHA_PADRAO = "senha.padrao";
	/**
	 * Grupo padrão para admin ao criar empresa
	 * Default "Geral"
	 */
	public static final String GRUPO_PADRAO = "grupo.padrao";

	/**
	 * Grupo publico para admin ao criar empresa
	 * Default "Público"
	 */
	public static final String GRUPO_PUBLICO = "grupo.publico";
	/**
	 * Cor da busca ao criar empresa
	 * Default "#0e709c"
	 */
	public static final String COR_BUSCA = "cor.busca";
	/**
	 * Cor do menu ao criar empresa
	 * Default "#0096D0"
	 */
	public static final String COR_MENU = "cor.menu";
	/**
	 * Cor do span de badges ao criar empresa
	 * Default "#e44c34"
	 */
	public static final String COR_BADGE_SPAN = "cor.badge.span";
	/**
	 * Cor do texto de badges ao criar empresa
	 * Default "#fff"
	 */
	public static final String COR_BADGE_TEXT = "cor.badge.text";
	
	/**
	 * Cor da borda dos posts agendados
	 * Default "#00D04D"
	 */
	public static final String COR_POST_AGENDADO = "cor.post.agendado";
	
	/**
	 * Cor da borda dos posts destacados
	 * Default "#0096d0"
	 */
	public static final String COR_POST_DESTACADO = "cor.post.destacado";
	
	/**
	 * Cor da borda dos posts expirados
	 * Default "#F99512"
	 */
	public static final String COR_POST_EXPIRADO = "cor.post.expirado";
	
	/**
	 * Cor da borda dos posts expirados
	 * Default "#f97d12"
	 */
	public static final String COR_POST_PRIORITARIO = "cor.post.prioritario";
	
	/**
	 * Define o background da página de login do sistema
	 * Default "http://static.livetouchdev.com.br/cielo/16cb376d-a1a4-4cd0-ab94-5f620f49403b/bg/3e2cdab2-1eff-4904-86dc-f430b54b51f0_login.jpeg"
	 */
	public static final String LAYOUT_BACKGROUD = "layout.backgroud";
	/**
	 * Define a logo do sistema
	 */
	public static final String LAYOUT_LOGO = "layout.logo";
	/**
	 * Define o favicon do sistema
	 */
	public static final String LAYOUT_FAVICON = "layout.favicon";
	/**
	 * Define a capa do mural do sistema
	 */
	public static final String LAYOUT_CAPA_MURAL = "layout.capa_mural";
	/**
	 * Define a capa do mural do sistema
	 */
	public static final String LAYOUT_THUMB_GRUPO = "layout.thumb_grupo";
	/**
	 * Url do aplicativo android
	 */
	public static final String BUILD_APP_ANDROID_URL = "build_app_android_url";
	/**
	 * Url do aplicativo IOS
	 */
	public static final String BUILD_APP_IOS_TEST_FLIGHT = "build_app_ios_test_flight";
	public static final String BUILD_APP_IOS_URL = "build_app_ios_url";
	/**
	 * Título do web template
	 */
	public static final String WEB_TEMPLATE_TITLE = "web.template.title";
	/**
	 * Chave do Google Analytics
	 */
	public static final String WEB_TEMPLATE_ANALYTICS_KEY = "web_template_analytics_key";
	/**
	 * Exportar csv
	 */
	public static final String EXPORTAR_CSV = "exportar.csv.on";
	/**
	 * Copyright no border
	 */
	public static final String BORDER_COPYRIGHT_EMPRESA = "border.copyright.empresa";
	/**
	 * Default "$userPost postou um novo comunicado $titulo"
	 */
	public static final String MSG_PUSH_NEWPOST = "msg.push.newPost";
	public static final String PUSH_SERVER_PROJECT = "push.server.project";
	
	/**
	 * 1 para ligado, 0 para desligado
	 */
	public static final String IMPORTACAO_ARQUIVO_ON = "importacao.arquivo.on";
	/**
	 * Duração do cookie
	 * Default (24 * 60 * 60) (1dia)
	 */
	public static final String WEB_COOKIE_MAXAGE = "web.cookie.maxAge";
	/**
	 * Default "1"  (Livecom)
	 */
	public static final String EMPRESA_DEV_TESTE = "empresa.dev.teste";
	
	public static final String SECURITY_IP_ON = "security.ip.on";
	public static final String SECURITY_ALLOWED_ADMIN_HOST = "security.host.domains";
	public static final String SECURITY_ALLOWED_IP = "security.host.ip";
	public static final String SECURITY_HOST_ON = "security.host.on";
	public static final String SECURITY_USERAGENT_ON = "security.userAgent.on";
	
//	public static final String SECURITY_WS_GET_ON = "security.ws.get.on";
//	public static final String SECURITY_WS_TOKEN_ON = "security.ws.token.on";
	public static final String SECURITY_WS_CRYPT_ON = "security.ws.crypt.on";

	public static final String SECURITY_ATTACK_REPLAY_ON = "security.attack.replay.on";
	
	public static final String SECURITY_CORS_ON = "security.cors.on";
	public static final String SECURITY_CORS_ORIGIN = "security.cors.origin";
	public static final String SECURITY_XSS_ON = "security.xss.on";
	
	public static final String SECURITY_UPLOAD_MIN_SIZE = "security.upload.min.size";
	public static final String SECURITY_UPLOAD_MAX_SIZE = "security.upload.max.size";
	public static final String SECURITY_UPLOAD_ALLOWED_EXTENSIONS = "security.upload.allowed.extensions";
	/**
	 * Default "ws://localhost:8080/livecom/chat/"
	 */
	public static final String CHAT_SERVICE_LOCATION = "chat_service_location";
	public static final String CHAT_SERVICE_LOCATION_NOTIFICATION = "chat_service_location_notification";
	
	/**
	 * Podemos deixar vazio.
	 */
	public static final String SECURITY_USERAGENT_WEB_ALLOWED = "security.userAgent.web.allowed";
	
	/**
	 * revistaitau,revista-negocios-android,revista-negocios-ios,android,Dalvik,iPhone,iPad
	 */
	public static final String SECURITY_USERAGENT_MOBILE_ALLOWED = "security.userAgent.mobile.allowed";
	
	/**
	 * android,Dalvik, apple
	 */
	public static final String SECURITY_USERAGENT_WEB_BLOCKED = "security.userAgent.web.blocked";
	
	/**
	 * Firefox,Mozilla,Chrome,Chromium,Internet,Explorer,Windows,Macintosh,Mac,Ubuntu
	 */
	public static final String SECURITY_USERAGENT_MOBILE_BLOCKED = "security.userAgent.mobile.blocked";
	
	// Restringir acesso aos web services
	public static final String SECURITY_WS_PATH_ON = "security.wspath.on";
	
	/**
	 * /time.htm,/ws/like.htm,/ws/login.htm,/ws/categorias.htm,/ws/buscaPostsUsuarios.htm,/ws/chapeus.htm,/ws/post.htm
	 */
	public static final String SECURITY_WS_PATH_ALLOWED = "security.wspath.allowed";
	
	/**
	 * Default 1 ou 0 para desligado
	 */
	public static final String PUSH_ON_COMENTAR = "push.comentar.on";
	/**
	 * 1 - Somente para dono do grupo e quem comentou
	 * 2 - Para todos do grupo
	 */
	public static final String PUSH_COMENTARIO_MODE = "push.comentario.mode";
	
	/**
	 * Default "[%s] comentou o post [%s]"
	 */
	public static final String MSG_PUSH_COMENTARIO = "msg.push.comentario";

	/**
	 * Default "[%s] comentou o post [%s]"
	 */
	public static final String MSG_PUSH_UPDATE_COMENTARIO = "msg.push.update.comentario";
	/**
	 * Default "[%s] favoritou seu post [%s]"
	 */
	public static final String MSG_PUSH_FAVORITO_TITULO = "msg.push.favorito.titulo";
	/**
	 * Default "[%s] favoritou seu post"
	 */
	public static final String MSG_PUSH_FAVORITO = "msg.push.favorito";
	/**
	 * Default "[%s] favoritou seu arquivo [%s]"
	 */
	public static final String MSG_PUSH_FAVORITO_ARQUIVO_NOME = "msg.push.favorito.arquivo.nome";
	/**
	 * Default "[%s] favoritou seu arquivo"
	 */
	public static final String MSG_PUSH_FAVORITO_ARQUIVO = "msg.push.favorito.arquivo";
	public static final String GRUPO_PADRAO_POST = "grupo_padrao_post";
	/**
	 * Default "[%s] curtiu seu post [%s]"
	 */
	public static final String MSG_PUSH_LIKE_POST = "msg.push.like.post";
	/**
	 * Default "[%s] curtiu seu comentário [%s]"
	 */
	public static final String MSG_PUSH_LIKE_COMENTARIO = "msg.push.like.comentario";
	public static final String INFRA_SAVELOGTRANSACAO_LOGIN = "infra.saveLogTransacao.login";
	public static final String PUSH_CHECKBOXON = "push.checkBoxOn";
	public static final String PUSH_CHECKBOX_CHECKED = "push.checkBox.checked";
	/**
	 * Default "Não é possível utilizar o sistema fora da área de trabalho"
	 */
	public static final String HORARIO_GRUPO_MSG = "horario.grupo.msg";
	/**
	 * Default "Diretório de upload não configurado"
	 */
	public static final String FILE_UPLOAD_PHP_DIR = "file.upload.dir";
	/**
	 * Default 0 ou 1 para ligado
	 */
	public static final String INFRA_SAVE_LOG_TRANSACAO_WS = "logs.transacao.on";
	/**
	 * Default 0 ou 1 para ligado. Se user-agent for da web.
	 */
	public static final String INFRA_SAVE_LOG_TRANSACAO_WEB_WS = "logs.transacao.web.on";
	
	/**
	 * Importar utilizando o nome(atributo da classe), 1 para logado
	 * Deafault 0, importar utilizando o codigo.
	 */
	public static final String IMPORTACAO_ARQUIVO_BY_NOME = "importacao.arquivo.by.nome";
	
	/**
	 * Habilita a seguraça dos Webservices via token
	 * Deafault 0, deixa desabilitado a validação via token.
	 */
	public static final String WS_SECURITY_TOKEN_ON = "ws.security.token.on";

	/**
	 * Habilita a seguraça dos Webservices via OTP
	 * Deafault 0, deixa desabilitado a validação via OTP.
	 */
	public static final String WS_SECURITY_OTP_ON = "ws.security.otp.on";
	
	/**
	 * Habilita a seguraça dos Webservices para não deixar requisições via get passar
	 * Deafault 1, deixa desabilitado para deixar acesso via GET
	 */
	public static final String WS_SECURITY_GET_ALLOWED = "ws.security.get.allowed";
	public static final String BUILD_BETA_ANDROID_URL = "build_beta_android_url";

	/**
	 * Habilita a coluna codigo nos dataTable
	 */
	public static final String CADASTRO_DATATABLE_COLUMN_CODIGO = "cadastros.dataTable.codigo";
	
	/**
	 * Título das postagens obrigatório
	 */
	public static final String MURAL_POST_TITULO_OBRIGATORIO_ON = "mural.post.titulo.obrigatorio.on";
	
	/**
	 * Habilita visibilidade do post
	 */
	public static final String MURAL_VISIBILIDADE_POST_ON = "mural.visibilidade.post.on";

	/**
	 * Habilita campo tag
	 */
	public static final String MURAL_TAG_ON = "mural.tags.on";

	/**
	 * Habilita upload
	 */
	public static final String MURAL_ENVIAR_ARQUIVOS_ON = "mural.enviar.arquivos.on";
	
	/**
	 * Habilita post em destaque
	 */
	public static final String MURAL_POST_DESTAQUE_ON = "destacar.post.on";
	
	/**
	 * Habilita post com webview e editor de html inline
	 */
	public static final String MURAL_WEBVIEW_ON = "mural.webview.on";
	
	/**
	 * Habilita galeria de imagens no mural
	 */
	public static final String MURAL_GALERIA_ON = "mural.galeria.on";
	
	/**
	 * Habilita thumbs na galeria
	 */
	public static final String MURAL_GALERIA_THUMBS_ON = "mural.galeria.thumbs.on";
	
	/**
	 * Thumbs com tamanho fixo
	 */
	public static final String MURAL_GALERIA_THUMBS_FIXA_ON = "mural.galeria.thumbs.fixa.on";
	
	/**
	 * Tamanho fixo para edição de thumbs
	 */
	public static final String MURAL_GALERIA_THUMBS_TAMANHO_FIXO = "mural.galeria.thumbs.tamanho.fixo";

	/**
	 * Habilita edição das thumbs na galeria
	 */
	public static final String MURAL_GALERIA_THUMBS_EDIT_ON = "mural.galeria.thumbs.edit.on";
	
	/**
	 * Habilita crop de imagem na galeria
	 */
	public static final String MURAL_GALERIA_CROP_ON = "mural.galeria.crop.on";

	/**
	 * Habilita resize na galeria
	 */
	public static final String MURAL_GALERIA_RESIZE_ON = "mural.galeria.resize.on";
	
	/**
	 * Habilita rotate na galeria
	 */
	public static final String MURAL_GALERIA_ROTATE_ON = "mural.galeria.rotate.on";
	
	/**
	 * Habilita layout reduzido dos arquivos do post
	 */
	public static final String MURAL_LAYOUT_ANEXOS_REDUZIDO_ON = "mural.layout.anexos.reduzido.on";
	
	/**
	 * Habilida post prioritario
	 */
	public static final String MURAL_POST_PRIORITARIO_ON = "mural.post.prioritario.on";

	/**
	 * Habilita rascunho
	 */
	public static final String MURAL_RASCUNHO_ON = "mural.rascunho.on";

	/**
	 * Habilita tarefa
	 */
	public static final String MURAL_POST_TASK_ON = "mural.post.task.on";	
	
	/**
	 * Habilita sidebox
	 */
	public static final String MURAL_SIDEBOX_ON = "web.layout.sideBox";
	
	
	public static final String SIDEBOX_NOTIFICATION_ON = "sideBox.notification.on";

	public static final String SIDEBOX_NOTIFICATION_AGRUPAR_ON = "sideBox.notification.agrupar.on";

	/**
	 * Habilita campo chapeu
	 */
	public static final String MURAL_CHAPEU_ON = "mural.chapeu.on";

	/**
	 * Habilita campo resumo
	 */
	public static final String MURAL_RESUMO_ON = "mural.resumo.on";

	/**
	 * Habilita campo hora
	 */
	public static final String MURAL_HORA_ON = "mural.hora.on";
	
	/**
	 * Habilita Amizade (conexões)
	 */
	public static final String MURAL_AMIZADE_ON = "mural.amizade.on";
	
	/**
	 * Habilita Crop img
	 */
	public static final String MURAL_UPLOAD_CROP_ON = "mural.upload.crop.on";
	
	/**
	 * Habilita Crop img thumb
	 */
	public static final String MURAL_UPLOAD_CROP_THUMB_ON = "mural.upload.crop.thumb.on";

	/**
	 * Preview do post com layout app Revista - quando desabilitado, mostra preview com layout Livecom mobile
	 */
	public static final String MURAL_PREVIEW_POST_REVISTA_ON = "mural.preview.post.revista.on";
	
	/**
	 * Habilita data de agendamento do post
	 */
	public static final String MURAL_DATA_AGENDAMENTO_POST_ON = "mural.data.agendamento.post.on";
	
	/**
	 * Habilita data de expiração do post
	 */
	public static final String MURAL_DATA_EXPIRACAO_POST_ON = "mural.data.expiracao.post.on";
	
	/**
	 * Habilita reminder do post
	 */
	public static final String MURAL_DATA_REMINDER_POST_ON = "mural.data.reminder.post.on";

	/**
	 * Placeholder do título do post
	 */
	public static final String MURAL_PLACEHOLDER_TITULO_POST = "mural.placeholder.titulo.post";
	
	/**
	 * Habilita publicações de incidentes (cetip)
	 */
	public static final String MURAL_POSTAR_INCIDENTE_ON = "mural.postar.incidente.on";
	
	/**
	 * Habilita tutorial
	 */
	public static final String MURAL_TUTORIAL_ON = "mural.tutorial.on";
	
	/**
	 * Habilita conteúdo coluna cargo na lista de usuários
	 */
	public static final String USUARIOS_COLUNA_CARGO_ON = "usuarios.coluna.cargo.on";
	
	
	/**
	 * Habilita integração para mural
	 */
	public static final String INTEGRACAO_SISTEMA_ON = "integracao.sistema.on";
	
	/**
	 * URL integração mural - postar incidente
	 */
	public static final String INTEGRACAO_SISTEMA_URL_POSTAR_INCIDENTE = "integracao.sistema.url.postar.incidente";
	
	/**
	 * URL integração mural - deletar incidente
	 */
	public static final String INTEGRACAO_SISTEMA_URL_DELETAR_INCIDENTE = "integracao.sistema.url.deletar.incidente";
	
	/**
	 * Habilita Amizade
	 */
	public static final String MURAL_GRUPO_PARTICIPACAO_ON = "mural.grupo.participar.on";
	
	/**
	 *  Categorias
	 */
	public static final String CATEGORIA_PAI_ON = "categoria.pai.on";

	/**
	 *  Grupos
	 */
	public static final String CADASTRO_GRUPO_CATEGORIA_ON = "cadastro.grupo.categoria.on";
	public static final String CADASTRO_GRUPO_TIPO_ON = "cadastro.grupo.tipo.on";
	public static final String CADASTRO_GRUPO_VIRTUAL_ON = "cadastro.grupo.virtual.on";
	
	/**
	 * DEFAULT = 0
	 */
	public static final String USER_SENHA_PADRAO_ON = "user.senha.padrao.on";
	
	/**
	 * LDAP
	 */
	public static final String LDAP_PROVIDER_URL = "ldap.PROVIDER_URL";
	public static final String LDAP_SECURITY_AUTHENTICATION = "ldap.SECURITY_AUTHENTICATION";
	public static final String LDAP_SECURITY_PRINCIPAL = "ldap.SECURITY_PRINCIPAL";
	public static final String LDAP_SEARCH_FILTER = "ldap.SEARCH_FILTER";
	public static final String LDAP_SEARCH_BASE = "ldap.SEARCH_BASE";
	public static final String LDAP_RETURN_ATTRS = "ldap.RETURN_ATTRS";
	public static final String LDAP_SECURITY_PASSWD = "ldap.SECURITY_PASSWD";
	public static final String LOGIN_CONNECTOR_ON = "login.connector.on";
	public static final String LDAP_INITIAL_CONTEXT_FACTORY = "ldap.INITIAL_CONTEXT_FACTORY";
	public static final String MURAL_GRUPO_HEADER_ON = "mural.grupo.header.on";
	public static final String LOGIN_CONNECTOR_RECUPERAR_SENHA = "login.connector.recuperar.senha";
	
	
	
	/**
	 * EMAIL
	 */
	public static final String EMAIL_SUBJECT_CONVITE = "email.subject.convite";
	public static final String EMAIL_SUBJECT_RECUPERAR_SENHA = "email.subject.recuperar.senha";
	public static final String EMAIL_SUBJECT_ALTERAR_SENHA = "email.subject.alterar.senha";
	public static final String IMPORTAR_USUARIO_LDAP_GRUPO = "importar.usuario.ldap.grupo";
	
	
	/**
	 * CASCADE Entidades
	 */
	public static final String DELETAR_EMPRESA_CASCADE_ON = "deletar.empresa.cascade.on";
	public static final String DELETAR_POST_CASCADE_ON = "deletar.post.cascade.on";
	public static final String DELETAR_CATEGORIA_CASCADE_ON = "deletar.categoria.cascade.on";
	public static final String DELETAR_CATEGORIA_CASCADE_ON_TIPO = "deletar.categoria.cascade.tipo";
	
	public static final String DELETAR_TAG_C1_ON = "deletar.tag.c1.on";
	public static final String DELETAR_TAG_R1_ON = "deletar.tag.r1.on";
	public static final String DELETAR_TAG_R2_ON = "deletar.tag.r2.on";
	
	public static final String DELETAR_CATEGORIA_R2_ON = "deletar.categoria.r2.on";
	public static final String DELETAR_CATEGORIA_C1_ON = "deletar.categoria.c1.on";
	public static final String DELETAR_CATEGORIA_C2_ON = "deletar.categoria.c2.on";
	
	public static final String DELETAR_GRUPO_R2_ON = "deletar.grupo.r2.on";
	public static final String DELETAR_GRUPO_R3_ON = "deletar.grupo.r3.on";
	public static final String DELETAR_GRUPO_R4_ON = "deletar.grupo.r4.on";
	//Não permite excluir grupo se tiver notificações
	public static final String DELETAR_GRUPO_R5_ON = "deletar.grupo.r5.on";
	
	public static final String DELETAR_GRUPO_C1_ON = "deletar.grupo.c1.on";
	public static final String DELETAR_GRUPO_C2_ON = "deletar.grupo.c2.on";
	public static final String DELETAR_GRUPO_C3_ON = "deletar.grupo.c3.on";
	public static final String DELETAR_GRUPO_C4_ON = "deletar.grupo.c4.on";	
	//set null em grupo_id nad notificações
	public static final String DELETAR_GRUPO_C5_ON = "deletar.grupo.c5.on";	
	
	public static final String DELETAR_USUARIO_R2_ON = "deletar.usuario.r2.on";
	public static final String DELETAR_USUARIO_R3_ON = "deletar.usuario.r3.on";
	public static final String DELETAR_USUARIO_R4_ON = "deletar.usuario.r4.on";
	
	public static final String DELETAR_USUARIO_C1_ON = "deletar.usuario.c1.on";
	public static final String DELETAR_USUARIO_C2_ON = "deletar.usuario.c2.on";
	
	public static final String DELETAR_AREA_R1_ON = "deletar.area.r1.on";
	public static final String DELETAR_DIRETORIA_R1_ON = "deletar.diretoria.r1.on";
	public static final String DELETAR_DIRETORIA_R2_ON = "deletar.diretoria.r2.on";
	public static final String DELETAR_DIRETORIA_R3_ON = "deletar.diretoria.r3.on";
	
	/**
	 * NULL Entidades
	 */
	public static final String DELETAR_FUNCAO_USUARIO_NULL_ON = "deletar.funcao.usuario.null.on";
	public static final String PUSH_MSG_NEW_POST = "push.msg.new.post";
	public static final String PUSH_MSG_REMINDER = "push.msg.reminder";
	public static final String PUSH_MSG_UPDATE_POST = "push.msg.update.post";
	
	
	/**
	 * PUSH
	 */
	public static final String PUSH_COMENTARIO_TITULO_NOTIFICATION = "push.comentario.titulo.notification";
	public static final String PUSH_COMENTARIO_UPDATE_TITULO_NOTIFICATION = "push.comentario.update.titulo.notification";
	public static final String PUSH_COMENTARIO_SUB_TITULO_NOTIFICATION = "push.comentario.sub.titulo.notication";
	public static final String PUSH_COMENTARIO_SUB_UPDATE_TITULO_NOTIFICATION = "push.comentario.sub.update.titulo.notication";
	public static final String PUSH_FAVORITO_TITULO_NOTIFICATION = "push.favorito.titulo.notification";;
	public static final String PUSH_FAVORITO_SUB_TITULO_NOTIFICATION = "push.favorito.sub.titulo.notication";
	public static final String PUSH_COMUNICADO_NEW_TITULO_NOTIFICATION = "push.comunicado.new.titulo.notication";
	public static final String PUSH_REMINDER_NEW_TITULO_NOTIFICATION = "push.reminder.new.titulo.notication";
	public static final String PUSH_COMUNICADO_NEW_SUB_TITULO_NOTIFICATION = "push.comunicado.new.sub.titulo.notication";
	public static final String PUSH_REMINDER_NEW_SUB_TITULO_NOTIFICATION = "push.reminder.new.sub.titulo.notication";
	public static final String PUSH_COMUNICADO_UPDATE_TITULO_NOTIFICATION = "push.comunicado.update.titulo.notication";
	public static final String PUSH_COMUNICADO_UPDATE_SUB_TITULO_NOTIFICATION = "push.comunicado.update.sub.titulo.notication";
	public static final String PUSH_LIKE_POST_TITULO_NOTIFICATION = "push.like.post.titulo.notication";
	public static final String PUSH_LIKE_POST_SUB_TITULO_NOTIFICATION = "push.like.post.sub.titulo.notication";
	public static final String PUSH_LIKE_COMENTARIO_TITULO_NOTIFICATION = "push.like.comentario.titulo.notication";
	public static final String PUSH_LIKE_COMENTARIO_SUB_TITULO_NOTIFICATION = "push.like.comentario.sub.titulo.notication";
	
	/**
	 * Foto usuario default
	 * Thumb do usuario
	 */
	public static final String FOTO_USUARIO_DEFAULT = "usuario.foto.default";
	
	/**
	 * Enviar email de erro
	 */
	public static final String LOG_SEND_EMAIL_ON = "log.send.email.on";
	
	/**
	 * Enviar email de erro para
	 */
	public static final String LOG_SEND_EMAIL_TO = "log.send.email.to";
	/**
	 * Enviar email de erro assunto
	 */
	public static final String LOG_SEND_EMAIL_SUBJECT = "log.send.email.subject";
	
	/**
	 * Enviar email de erro
	 */
	public static final String HIBERNATE_CACHE_ON = "hibernate.query.cache.on";
	
	/**
	 * Censurar palavras post
	 */
	public static final String POST_CENSURA_ON = "post.censura.on";
	/**
	 * Validar email unico ao cadastrar usuario
	 */
	public static final String VALIDA_EMAIL_UNICO_ON = "validate.email.on";
	/**
	 * Validar login unico ao cadastrar usuario
	 */
	public static final String VALIDA_LOGIN_UNICO_ON = "validate.login.on";
	/**
	 * Força o envio de convite para o usuario, mesmo que ele ja tenha recebido
	 */
	public static final String CONVIDAR_USUARIO_FORCE_ON = "convidar.usuario.force.on";
	
	/**
	 * status_publicacao=1
	 */
	public static final String EMAIL_FACTORY = "email.factory";

	public static final String EMAIL_REPORT_ON = "email.report.on";

	public static final String MENU_DINAMICO_ON = "menu.dinamico.on";

	public static final String PERFIL_ADMIN_PADRAO = "perfil.admin.padrao";

	public static final String PERFIL_USER_PADRAO = "perfil.user.padrao";

	public static final String SESSION_TIME = "session.time";

	public static final String PERFIL_ADMIN_PADRAO_ID = "perfil.admin.padrao.id";
	public static final String PERFIL_USER_PADRAO_ID = "perfil.user.padrao.id";

	public static final String MANUTENCAO_ON = "manutencao.on";

	public static final String MANUTENCAO_MESSASGE = "manutencao.message";

	/**
	 * Fechar o app caso o usuario erre a senha
	 */
	public static final String RESTRICAO_FERIAS_ON = "restricao.ferias.on";
	
	
	/**
	 * habilita cadastro de codigo pra grupo
	 */
	public static final String CADASTRO_GRUPO_CODIGO = "cadastro.grupo.codigo";
	
	/**Boolean
	 * Ativa push via firebase
	 */
	public static final String PUSH_FIREBASE_ON = "push.firebase.on";

	/**String
	 * Parametro que sera enviado em CollapseKey
	 */
	public static final String PUSH_FIREBASE_COLLAPSEKEY = "push.firebase.collapseKey";

	/**Boolean
	 * Ativa parametro ContentAvaliable
	 */
	public static final String PUSH_FIREBASE_CONTENTAVALIABLE = "push.firebase.contentAvaliable";
	
	/**Boolean
	 * Ativa parametro DelayWhileIdle
	 */
	public static final String PUSH_FIREBASE_DELAYWHILEIDLE = "push.firebase.delayWhileIdle";

	/**String
	 * Parametro para definir Priority do push
	 */
	public static final String PUSH_FIREBASE_PRIORITY = "push.firebase.priority";
	
	/**Long
	 * Parametro para definir TimeToLive do push
	 */
	public static final String PUSH_FIREBASE_TIMETOLIVE = "push.firebase.timeToLive";

	//CHAT
	
	/**Boolean
	 * Ativa parametro ContentAvaliable
	 */
	public static final String PUSH_FIREBASE_CONTENTAVALIABLE_CHAT = "push.firebase.contentAvaliable.chat";
	
	/**Boolean
	 * Ativa parametro DelayWhileIdle
	 */
	public static final String PUSH_FIREBASE_DELAYWHILEIDLE_CHAT = "push.firebase.delayWhileIdle.chat";
	
	/**String
	 * Parametro para definir Priority do push
	 */
	public static final String PUSH_FIREBASE_PRIORITY_CHAT = "push.firebase.priority.chat";
	
	/**Long
	 * Parametro para definir TimeToLive do push
	 */
	public static final String PUSH_FIREBASE_TIMETOLIVE_CHAT = "push.firebase.timeToLive.chat";
	
	/**String
	 * Parametro que sera enviado em CollapseKey
	 */
	public static final String PUSH_FIREBASE_COLLAPSEKEY_CHAT = "push.firebase.collapseKey.chat";
	
	/**String
	 * Parametro para ativar hash md5 de login
	 */
	public static final String ITAU_LOGIN_HASHMD5 = "itau.login.hashmd5";

	public static final String CATEGORIA_DEFAULT_IMG = "categoria.default.img";

	public static final String PUSH_FAST_MODE = "push.fastMode";

	public static final String PUSH_BATCH_SIZE = "push.batch.size";

	public static final String CATEGORIA_ORDEM_ON = "categoria.ordem.on";

	public static final String PUSH_SEND_TOALL = "push.send.toAll";

	public static final String GRUPO_CHAT_BOT = "grupo.chat.bot";
	
	/**String
	 * Parametro que permite remover admin do grupo
	 */
	public static final String REMOVER_ADMIN_GRUPO_ON = "remover.admin.grupo.on";

	public static final String PUSH_POST_CREATE_NOTIFICATION_UPDATE = "push.post.create.notification.update";
	
	// Parametros do mobile
	public static final String APP_LOGIN_AUTOMATICO = "app.login.automatico";
	public static final String APP_PERMITE_LOGOUT = "app.permite.logout";
	public static final String APP_PODE_POSTAR = "app.pode.postar";
	public static final String APP_CHAT_ON = "app.chat.on";
	public static final String APP_SHOW_WHATS_NEWS = "app.show.whats.news";
	public static final String APP_SHOW_AUDIENCIA_POST = "app.show.audiencia.post";
	public static final String APP_OPTIONS_MENU = "app.options.menu";
	public static final String APP_MENU_CATEGORIA = "app.menu.categoria";
    public static final String APP_BY_PASS_LOGIN = "app.by.pass.login";
    public static final String APP_LOGIN_OFFLINE = "app.login.offline";


	public static final String PUSH_API_KEY = "push.api.key";
	
	public static final String AMAZON_LAMBDA_CHAT_RESIZE_ON = "amazon.lambda.chat.resize.on";
	public static final String AMAZON_LAMBDA_CHAT_RESIZE_GARANTIR_ON = "amazon.lambda.chat.resize.garantir.on";

	public static final String PUSH_NOTIFICATION_SOUND = "push.notification.sound";

	/**
	 * Ativa subcategorias
	 */
	public static final String SUBCATEGORIAS_ON = "subcategorias.on";

	public static final String RESUMIR_LOGIN_BBA = "resumir.login.bba";

	public static final String BBA_TERMOS_URL_PT = "bba.termos.url.pt";
	
	public static final String BBA_TERMOS_URL_EN = "bba.termos.url.en";

	public static final String BBA_SOBRE_URL_PT = "bba.sobre.url.pt";
	
	public static final String BBA_SOBRE_URL_EN = "bba.sobre.url.en";
	
}
