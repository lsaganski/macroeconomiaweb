package br.livetouch.livecom.files;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

import br.infra.util.Log;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;

public class SFTPFileManager implements  FileManager{

	protected static final Logger log = Log.getLogger(FileManager.class);

	private FTPClient ftp;
	private Session session;
	private ChannelSftp channelSftp;
	
	/** EX: livetouchdev.com.br **/
	private String server;
	/** EX: livetouch **/
	private String login;
	/** EX: livetouch@2013 **/
	private String senha;
	/** Default: 21 **/
	private Integer porta;

	/**
	 * Caminho para enviar os arquivos para o ftp <br>
	 * Defautl: /var/www/html
	 **/
	private String workplace;
	/**
	 * Default: livecom
	 * */
	private String bucket;

	public SFTPFileManager(ParametrosMap params) {
		
		server = params.get(Params.FILE_MANAGER_FTP_SERVER, "static.livetouchdev.com.br");
		if (StringUtils.isEmpty(server)) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.ftp.server]");
		}

		login = params.get(Params.FILE_MANAGER_FTP_LOGIN, "livetouch");
		if (StringUtils.isEmpty(login)) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.ftp.login]");
		}

		senha = params.get(Params.FILE_MANAGER_FTP_SENHA, "livetouch@2013");
		if (StringUtils.isEmpty(senha)) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.ftp.senha]");
		}

		porta = params.getInt(Params.FILE_MANAGER_FTP_PORT, 22);
		if (porta == null) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.ftp.port]");
		}

		workplace = params.get(Params.FILE_MANAGER_FTP_WORKPLACE, "/static");
		if (StringUtils.isEmpty(workplace)) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.ftp.workplace]");
		}

		bucket = params.get(Params.FILE_MANAGER_FTP_BUCKET, "livecom");
		if (StringUtils.isEmpty(bucket)) {
			throw new IllegalArgumentException("Configure o parâmetro [file.manager.ftp.bucket]");
		}
		
		JSch jSch = new JSch();
		try {
			session = jSch.getSession(login, server, porta);
			session.setPassword(senha);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(workplace);
			channelSftp.cd(bucket);
		} catch (JSchException | SftpException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LivecomFile> getFiles() {
		try {
			String dir = workplace + "/" + bucket;
			Vector<ChannelSftp.LsEntry> filelist = channelSftp.ls(dir);
            for(int i=0; i<filelist.size();i++){
            	ChannelSftp.LsEntry f = filelist.get(i);
            	String filename = f.getFilename();
            	SftpATTRS stat = channelSftp.stat(dir + "/" + filename);
            	if(stat.isDir()) {
            		Vector<ChannelSftp.LsEntry> subfiles = channelSftp.ls(dir);
            	}
            }
			return null;
		} catch (SftpException e) {
			return null;
		}
	}

	@Override
	public LivecomFile getFile(String name) {
		return null;
	}

	@Override
	public List<LivecomFile> getFilesInFolder(String folder) {
		return null;
	}

	@Override
	public List<String> getFoldersInFolder(String folder) {
		return null;
	}

	@Override
	public String putFile(String dir, String fileName, String contentType, byte[] bytes) throws IOException {
		try {
			InputStream fs = new ByteArrayInputStream(bytes);
			channelSftp.put(fs, dir);
			channelSftp.chmod(775, dir + fileName);
		} catch (SftpException e) {
			throw new IOException(e.getMessage());
		}
		return null;
	}

	@Override
	public String putFileLambda(String dir, String fileName, String contentType, byte[] bytes) throws IOException {
		return putFile(dir, fileName, contentType, bytes);
	}

	@Override
	public String putFileThumb(String dir, String fileName, String contentType, byte[] bytes) throws IOException {
		return putFile(dir, fileName, contentType, bytes);
	}

	@Override
	public void delete(String path) throws IOException {
		try {
			channelSftp.rmdir(path);
		} catch (SftpException e) {
			throw new IOException(e.getMessage());
		}
	}

	@Override
	public String getFileUrl(String dir, String file) {
		try {
			InputStream inputStream = channelSftp.get(dir + file);
			String url = null;
			if(inputStream != null) {
				if (StringUtils.isNotEmpty(dir)) {
					url = getServerUrlBucket() + "/" + dir + "/" + file;
				} else {
					url = getServerUrlBucket() + "/" + file;
				}
			}
			return url;
		} catch (SftpException e) {
			return null;
		}
	}
	
	@Override
	public String getFileLambdaUrl(String dir, String file) {
		return getFileUrl(dir, file);
	}

	@Override
	public String getThumbLambdaURL(String dir, String file) {
		return getFileUrl(dir, file);
	}

	@Override
	public void createFolder(String dir) throws IOException {
		try {
			channelSftp.mkdir(dir);
			channelSftp.chmod(775, dir);
		} catch (SftpException e) {
			throw new IOException(e.getMessage());
		}
	}

	@Override
	public void close() throws IOException {
		if(channelSftp != null) {
			channelSftp.exit();
		}
		
		if(session != null) {
			session.disconnect();
		}
	}
	
	private String getServerUrlBucket() {
		if (StringUtils.isNotEmpty(bucket)) {
			return server + "/" + bucket;
		}
		return server;
	}
	
	@Override
	public boolean somenteFotosProfile(String dir) {
		return false;
	}
}
