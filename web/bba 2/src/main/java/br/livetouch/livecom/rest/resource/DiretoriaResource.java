
package br.livetouch.livecom.rest.resource;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Diretoria;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.DiretoriaVO;
import br.livetouch.livecom.domain.vo.DiretoriasVO;
import br.livetouch.livecom.domain.vo.Filtro;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/diretoria")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class DiretoriaResource extends MainResource {
	protected static final Logger log = Log.getLogger(DiretoriaResource.class);
	
	@POST
	public Response create(Diretoria diretoria) {
		
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Diretoria.isFormValid(diretoria, false);
			Response  messageResult = existe(diretoria);
			if (messageResult != null){
				return messageResult;
			}
			
			Usuario userInfo = getUserInfo();
			
			diretoriaService.saveOrUpdate(userInfo, diretoria, getEmpresa());
			DiretoriaVO vo = new DiretoriaVO(diretoria);
			Diretoria diretoriaExecutiva = diretoria.getDiretoriaExecutiva();
			if(diretoriaExecutiva != null) {
				diretoriaExecutiva = diretoriaService.get(diretoriaExecutiva.getId());
				DiretoriaVO diretoriaExecVO = new DiretoriaVO(diretoriaExecutiva);
				vo.diretoriaExecutiva = diretoriaExecVO;
			}
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", vo)).build();
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel efetuar o cadastro da diretoria")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@PUT
	public Response update(Diretoria diretoria) {
		
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Diretoria.isFormValid(diretoria, true);
			
			Response  messageResult = existe(diretoria);
			if (messageResult != null){
				return messageResult;
			}
			
			Usuario userInfo = getUserInfo();
			diretoriaService.saveOrUpdate(userInfo, diretoria, getEmpresa());
			DiretoriaVO vo = new DiretoriaVO(diretoria);
			Diretoria diretoriaExecutiva = diretoria.getDiretoriaExecutiva();
			if(diretoriaExecutiva != null) {
				diretoriaExecutiva = diretoriaService.get(diretoriaExecutiva.getId());
				DiretoriaVO diretoriaExecVO = new DiretoriaVO(diretoriaExecutiva);
				vo.diretoriaExecutiva = diretoriaExecVO;
			}
			return Response.ok(MessageResult.ok("Diretoria atualizada com sucesso", vo)).build();
		} catch (DomainException e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel atualizar a diretoria")).build();
		} catch (Throwable e) {
			log.debug(e.getMessage(), e);
			return Response.ok(MessageResult.error("Sistema temporariamente indisponivel")).build();
		}
	}
	
	@GET
	public Response findAll() {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		List<Diretoria> diretorias = diretoriaService.findAll(getEmpresa());
		List<DiretoriaVO> vos = DiretoriaVO.fromList(diretorias);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/datatable")
	public Response findByNome(@QueryParam("buscaTotal") String buscaTotal, @QueryParam("q") String nome, @QueryParam("page") int page, @QueryParam("maxRows") int maxRows) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		List<Diretoria> diretorias = diretoriaService.findByNome(nome, page, maxRows, getEmpresa());
		
		DiretoriasVO vo = new DiretoriasVO();
		if(StringUtils.isNotEmpty(buscaTotal) || StringUtils.equals(buscaTotal, "1")) {
			Long count = diretoriaService.countByNome(nome, page, maxRows, getEmpresa());
			vo.total = count;
		}
		vo.diretorias = DiretoriaVO.setFromList(diretorias);
		
		return Response.ok().entity(vo).build();
		
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Diretoria diretoria = diretoriaService.get(id);
		if(diretoria == null) {
			return Response.ok(MessageResult.error("Diretoria não encontrada")).build();
		}
		DiretoriaVO vo = new DiretoriaVO(diretoria);
		if(diretoria.getDiretoriaExecutiva() != null) {
			DiretoriaVO exec = new DiretoriaVO(diretoria.getDiretoriaExecutiva());
			vo.diretoriaExecutiva = exec;
		}
		return Response.ok().entity(vo).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Diretoria diretoria = diretoriaService.get(id);
			if(diretoria == null) {
				return Response.ok(MessageResult.error("Diretoria não encontrada")).build();
			}
			Usuario userInfo = getUserInfo();
			diretoriaService.delete(userInfo, diretoria);
			return Response.ok().entity(MessageResult.ok("Diretoria deletada com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error(e.getMessage())).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir a Diretoria  " + id)).build();
		}
	}
	
	@GET
	@Path("/filtro")
	public Response findByName(@BeanParam Filtro filtroDiretoria) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		List<Diretoria> diretorias = diretoriaService.filterDiretoria(filtroDiretoria, getEmpresa());
		List<DiretoriaVO> list = DiretoriaVO.fromList(diretorias);
		return Response.ok().entity(list).build();
	}
	
	@GET
	@Path("/filtro/executiva")
	public Response findByNameExecutiva(@BeanParam Filtro filtroDiretoria) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		filtroDiretoria.executiva = true;
		List<Diretoria> diretorias = diretoriaService.filterDiretoria(filtroDiretoria, getEmpresa());
		List<DiretoriaVO> list = DiretoriaVO.fromList(diretorias);
		return Response.ok().entity(list).build();
	}
	
	@GET
	@Path("/filtro/diretoria")
	public Response findByNameDiretoria(@BeanParam Filtro filtroDiretoria) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		filtroDiretoria.diretoriaNaoExecutiva = true;
		List<Diretoria> diretorias = diretoriaService.filterDiretoria(filtroDiretoria, getEmpresa());
		List<DiretoriaVO> list = DiretoriaVO.fromList(diretorias);
		return Response.ok().entity(list).build();
	}
	
	@GET
	@Path("/exportar")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response exportar() throws IOException {
		
		if(!hasPermissoes(true, ParamsPermissao.ACESSO_CADASTROS, ParamsPermissao.VIZUALIZAR_RELATORIOS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Empresa empresa = getEmpresa();
		String csv = diretoriaService.csvDiretoria(empresa);
		
		String charset = ParametrosMap.getInstance(empresa).get(Params.EXPORTACAO_ARQUIVO_CHARSET, "ISO-8859-1");

		String attachament = "attachment; filename=" + URLEncoder.encode("diretorias.csv", "UTF-8");
		ResponseBuilder b = Response.status(Status.OK);
		b.encoding(charset);
		b.header("Content-Disposition", attachament);
		b.header("Content-Type", "application/octet-stream"+";charset="+charset);
		b.entity(csv);
		Response r = b.build();
		return r;
	}
	
	public Response existe(Diretoria diretoria) {
		
		if(diretoriaService.findByName(diretoria, getEmpresa()) != null) {
			log.debug("Ja existe uma diretoria com este nome");
			return Response.ok(MessageResult.error("Ja existe uma diretoria com este nome")).build();
		}

		if(diretoriaService.findByCodigo(diretoria, getEmpresa()) != null){
			log.debug("Ja existe uma diretoria com este codigo");
			return Response.ok(MessageResult.error("Ja existe uma diretoria com este codigo")).build();
		}

		return null;
	}
	
	
}
