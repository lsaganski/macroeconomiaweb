package br.livetouch.livecom.web.pages.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Evento;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.EventoVO;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;

/**
 * 
 */
@Controller
@Scope("prototype")
public class ConvidarUsuariosPage extends LivecomLogadoPage {
	
	public String id;
	public List<EventoVO> eventosVo;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.ENVIAR_EMAILS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		
		Usuario user = getUsuario();
		String param = this.getParam("id");
		
		if(user != null) {
			id = user.getId() != null ? String.valueOf(user.getId()) : null;
		} else if(StringUtils.isNotEmpty(param)) {
			id = param;
		}
		
		if(StringUtils.isEmpty(id) || id == null) {
			setMessage("NOK", getMessage("msg.id.invalido.error"));
			return;
		}
		
		Usuario u = usuarioService.get(Long.valueOf(id));
		
		if(u == null) {
			setMessage("NOK", getMessage("msg.usuario.nao.encontrado.error", id));
			return;
		}
		
		List<Grupo> grupos = new ArrayList<Grupo>();
		for (GrupoUsuarios gu : u.getGrupos()) {
			grupos.add(gu.getGrupo());
		}
		
		if(grupos.size() > 0){
			eventosVo = new ArrayList<EventoVO>();
			List<Evento> eventos = eventoService.findByAtivo(grupos);
			
			for (Evento evento : eventos) {
				eventosVo.add(new EventoVO(evento));
			}
		}
		
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onPost() {
		super.onPost();
	}
	
	
	@Override
	public void onRender() {
		super.onRender();

		/**
		 * try {
			// Count(*)
			int pageSize = 10;
			long count = grupoService.getCountByFilter(filtro);
			grupos = grupoService.findbyFilter(filtro, page, pageSize);

			createPaginator(page, count, pageSize);

			table.setPageSize(pageSize);
			table.setRowList(grupos);

		} catch (DomainException e) {
			setFlashAttribute("msg", "Erro: " + e.getMessage());
			return;
		}
		 */
	}
	
}