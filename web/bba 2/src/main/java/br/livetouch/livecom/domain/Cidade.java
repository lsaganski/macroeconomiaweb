package br.livetouch.livecom.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.domain.enums.Estado;
import net.livetouch.tiger.ddd.DomainException;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Cidade extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 3678924620135757820L;
	private Long id;
	private String nome;
	
	@Enumerated(EnumType.STRING)
	@Column(name="estado", nullable=false)
	private Estado estado;

	@Id
	@Column(name="id", unique=true, nullable=false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "CIDADE_SEQ")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="nome", nullable=false, length=100)
	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return this.estado;
	}
	
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	@Override
	public String toString() {
		return estado.getNome() + " - " + getNome();
	}
	
	public String nomeComEstado() {
		return estado.getNome() + " - " + getNome();
	}
	
	public static void isFormValid(Cidade cidade, boolean isUpdate) throws DomainException {
		if(cidade == null) {
			throw new DomainException("Parametros inválidos");
		}
		
		if(StringUtils.isEmpty(cidade.nome)) {
			throw new DomainException("Campo nome não pode ser vazio");
		}
		
		if(isUpdate && cidade.getId() == null) {
			throw new DomainException("Cidade não localizada");
		}
		
	}
}
