package br.livetouch.livecom.utils;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

public class ChartSetUtils {
	
	public static final boolean isUTF8(String s) {
		final byte[] pText = s.getBytes();

	    int expectedLength = 0;

	    for (int i = 0; i < pText.length; i++) {
	        if ((pText[i] & 0b10000000) == 0b00000000) {
	            expectedLength = 1;
	        } else if ((pText[i] & 0b11100000) == 0b11000000) {
	            expectedLength = 2;
	        } else if ((pText[i] & 0b11110000) == 0b11100000) {
	            expectedLength = 3;
	        } else if ((pText[i] & 0b11111000) == 0b11110000) {
	            expectedLength = 4;
	        } else if ((pText[i] & 0b11111100) == 0b11111000) {
	            expectedLength = 5;
	        } else if ((pText[i] & 0b11111110) == 0b11111100) {
	            expectedLength = 6;
	        } else {
	            return false;
	        }

	        while (--expectedLength > 0) {
	            if (++i >= pText.length) {
	                return false;
	            }
	            if ((pText[i] & 0b11000000) != 0b10000000) {
	                return false;
	            }
	        }
	    }

	    return true;
	}

	public static boolean isUTF8MisInterpreted(String input) {
		// convenience overload for the most common UTF-8 misinterpretation
		// which is also the case in your question
		return isUTF8MisInterpreted(input, "Windows-1252");
	}

	public static boolean isUTF8MisInterpreted(String input, String encoding) {

		CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
		CharsetEncoder encoder = Charset.forName(encoding).newEncoder();
		ByteBuffer tmp;
		try {
			tmp = encoder.encode(CharBuffer.wrap(input));
		}

		catch (CharacterCodingException e) {
			return false;
		}

		try {
			decoder.decode(tmp);
			return true;
		} catch (CharacterCodingException e) {
			return false;
		}
	}
}
