package br.livetouch.livecom.email;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EmailArgs implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -386785693432361135L;

	@Id
	private Long id;

	private String buildType;

	private String smtpServer;
	private String smtpPort;
	private String smtpUser;
	private String smtpPwd;
	private String smtpEmailAdmin;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBuildType() {
		return buildType;
	}

	public void setBuildType(String buildType) {
		this.buildType = buildType;
	}

	public String getSmtpServer() {
		return smtpServer;
	}

	public void setSmtpServer(String smtpServer) {
		this.smtpServer = smtpServer;
	}

	public String getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getSmtpUser() {
		return smtpUser;
	}

	public void setSmtpUser(String smtpUser) {
		this.smtpUser = smtpUser;
	}

	public String getSmtpPwd() {
		return smtpPwd;
	}

	public void setSmtpPwd(String smtpPwd) {
		this.smtpPwd = smtpPwd;
	}

	public String getSmtpEmailAdmin() {
		return smtpEmailAdmin;
	}

	public void setSmtpEmailAdmin(String smtpEmailAdmin) {
		this.smtpEmailAdmin = smtpEmailAdmin;
	}
}