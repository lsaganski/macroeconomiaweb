package br.livetouch.livecom.web.pages.cadastro;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.vo.CamposMap;


/**
 * 
 * @author gabriel
 *
 */
@Controller
@Scope("prototype")
public class StatusPostPage extends CadastrosPage {

	public CamposMap campos = getCampos();
	
	@Override
	public void onInit() {
		super.onInit();
		
	}
}
