package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.RandomUtils;

import akka.actor.ActorRef;
import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.utils.JSONUtils;
import net.livetouch.extras.util.DateUtils;

/**
 * Info de um login web/android/ios
 * 
 * UserInfo tem uma lista de SessionLogin
 *
 */
public class UserInfoSession implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8587503297770762351L;

	/**
	 * web, android, ios
	 */
	public String tipo;

	public String dataLogin;
	public String dataUltTransacao;
	public String dataLoginChat;
	
	public String dataUltChat;
	private long timestampUltChat;

	// ws
	public String pathUltimoWebService;
	public String pathUltimoWebServiceFull;

	// login
	public long timestampLogin;
	public String wstoken;
	public long timestampUltTransacao;

	private String actorRef;
	private boolean chatOn;
	private boolean away;

	/**
	 * Keep alive comeca ok.
	 * Fica nao ok quando server manda keepalive.
	 */
	private boolean keepAliveOk = true;
	
	public UserInfoSession() {
		
	}

	public boolean isTipoLoginMobile() {
		return "android".equalsIgnoreCase(tipo) || "ios".equalsIgnoreCase(tipo);
	}

	public boolean isTipoLoginWeb() {
		return "web".equalsIgnoreCase(tipo);
	}

	public void setDataLoginChat(Date date) {
		this.dataLoginChat = DateUtils.toString(date, Livecom.DATE_FORMAT);
	}

	public void setDataLogin(Date dataLogin) {
		this.timestampLogin = dataLogin.getTime();
		this.wstoken = String.valueOf(RandomUtils.nextLong(Livecom.MIN_RANGE, Livecom.MAX_RANGE));
		this.dataLogin = DateUtils.toString(dataLogin, Livecom.DATE_FORMAT);
	}

	public void setDataUltTransacao(Date dataUltTransacao) {
		this.timestampUltTransacao = dataUltTransacao.getTime();
		this.dataUltTransacao = DateUtils.toString(dataUltTransacao, Livecom.DATE_FORMAT);
	}

	public void setDataUltChat(Date dataLastChat) {
		this.timestampUltChat = dataLastChat.getTime();
		this.dataUltChat = DateUtils.toString(dataLastChat, Livecom.DATE_FORMAT);
	}

	public long getTempoLogadoMinutos() {
		if (timestampLogin <= 0) {
			return 0;
		}
		long now = System.currentTimeMillis();
		long time = timestampLogin;
		long minutos = (now - time) / 1000 / 60;
		return minutos;
	}
	
	public long getTempoInativoMinutos() {
		if (timestampLogin <= 0) {
			return 0;
		}
		long now = System.currentTimeMillis();
		long time = timestampUltTransacao;
		long minutos = (now - time) / 1000 / 60;
		return minutos;
	}

	public Date getDtLogin() {
		Date dt = timestampLogin < 0 ? new Date(timestampLogin) : null;
		return dt;
	}

	public Date getDtUltTransacao() {
		Date dt = timestampUltTransacao > 0 ? new Date(timestampUltTransacao) : null;
		return dt;
	}

	public String getWstoken() {
		return wstoken;
	}

	public void setWstoken(String wstoken) {
		this.wstoken = wstoken;
	}

	public void setPathUltimoWebService(String pathUltimoWebService) {
		this.pathUltimoWebService = pathUltimoWebService;
	}

	public void setPathUltimoWebServiceFull(String pathUltimoWebServiceFull) {
		this.pathUltimoWebServiceFull = pathUltimoWebServiceFull;
	}

	public String getDataLoginChat() {
		return dataLoginChat;
	}
	
	public Date getDataLoginChatDate() {
		Date dt = new Date();
		dt.setTime(timestampLogin);
		return dt;
	}

	public String getDataUltChat() {
		return dataUltChat;
	}
	
	public long getTimestampUltChat() {
		return timestampUltChat;
	}

	public String getTipo() {
		return tipo;
	}

	public String getPathUltimoWebService() {
		return pathUltimoWebService;
	}

	public String getPathUltimoWebServiceFull() {
		return pathUltimoWebServiceFull;
	}

	public long getTimestampLogin() {
		return timestampLogin;
	}

	public String getDataLogin() {
		return dataLogin;
	}

	public String getDataUltTransacao() {
		return dataUltTransacao;
	}

	public long getTimestampUltTransacao() {
		return timestampUltTransacao;
	}

	public void setActorRef(String actorRef) {
		this.actorRef = actorRef;
		this.chatOn = StringUtils.isNotEmpty(actorRef);
	}
	
	public boolean isChatOn() {
		return chatOn;
	}

	public String getActorRef() {
		return actorRef;
	}

	public boolean isLogadoChat() {
		return StringUtils.isNotEmpty(actorRef);
	}
	
	public boolean isAway() {
		return isLogadoChat() && away;
	}
	
	public boolean isOnline() {
		return isLogadoChat();
	}

	public boolean isTipoSo(String so) {
		return StringUtils.equals(this.tipo, so);
	}

	public boolean isActorRef(ActorRef tcpWebActor) {
		return tcpWebActor != null && tcpWebActor.toString().equals(actorRef);
	}
	
	public void setAway(boolean away) {
		this.away = away;
	}

	public void setKeepAliveOk(boolean b) {
		this.keepAliveOk = b;
	}
	
	public boolean isKeepAliveOk() {
		return keepAliveOk;
	}

	@Override
	public String toString() {
		return JSONUtils.toJSON(this);
	}
}