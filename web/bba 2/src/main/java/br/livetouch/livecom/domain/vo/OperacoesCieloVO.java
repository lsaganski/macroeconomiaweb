package br.livetouch.livecom.domain.vo;

import java.io.Serializable;

public class OperacoesCieloVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Double valor;
	public String data;
	
	public OperacoesCieloVO() {	}
	
	public OperacoesCieloVO(Double valor, String data) {
		super();
		this.valor = valor;
		this.data = data;
	}

}
