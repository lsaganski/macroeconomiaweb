package br.livetouch.livecom.domain.exception;

import br.infra.util.MessageUtil;
import net.livetouch.tiger.ddd.DomainException;

/**
 * Exception lançada se o nome do grupo já existe
 * 
 * @author ricardo
 *
 */
public class GrupoJaExisteException extends DomainException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5814101732860144916L;

	public GrupoJaExisteException() {
		super("");
	}

	@Override
	public String getMessage() {
		String s = MessageUtil.getString("exception.grupo.existente");
		return s;
	}
}
