package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Area;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.repository.AreaRepository;
import br.livetouch.livecom.domain.vo.FiltroArea;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class AreaRepositoryImpl extends StringHibernateRepository<Area> implements AreaRepository {

	public AreaRepositoryImpl() {
		super(Area.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findIdCodGrupos(Long idEmpresa) {
		Query query = createQuery("select id,codigo from Area where empresa.id = :idEmpresa");
		query.setLong("idEmpresa", idEmpresa);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Area> filterArea(FiltroArea filtro, Empresa empresa) {
		StringBuffer sb = new StringBuffer("FROM Area d where 1=1 AND d.empresa.id = :empresaId");
		if(filtro == null) {
			Query q = createQuery(sb.toString());
			q.setLong("empresaId", empresa.getId());
			return q.list();
		}
		
		if(filtro.nome != null && filtro.codigo != null) {
			sb.append(" AND (lower(d.nome) like :nome OR lower(d.codigo) like :codigo");
		} else if(filtro.nome != null) {
			sb.append(" AND lower(d.nome) like :nome");
		} else if(filtro.codigo != null) {
			sb.append(" AND lower(d.codigo) like :codigo");
		} 
		
		if(StringUtils.isNotEmpty(filtro.dirExecId) && StringUtils.isNotEmpty(filtro.diretoria)) {
			sb.append(" AND (d.diretoria.diretoriaExecutiva.id = :dirExec OR d.diretoria.id = :diretoria) ");
		} else if(StringUtils.isNotEmpty(filtro.dirExecId)) {
			sb.append(" AND d.diretoria.diretoriaExecutiva.id = :dirExec");
		} else if(StringUtils.isNotEmpty(filtro.diretoria)) {
			sb.append(" AND d.diretoria.id = :diretoria");
		}
		
		sb.append(" order by d.nome");
		
		Query q = createQuery(sb.toString());
		q.setLong("empresaId", empresa.getId());
		if(filtro.nome != null && filtro.codigo != null) {
			q.setParameter("nome", "%" + filtro.nome +"%");
			q.setParameter("codigo", "%" + filtro.codigo +"%");
		} else if(filtro.nome != null) {
			q.setParameter("nome", "%" + filtro.nome +"%");
		} else if(filtro.codigo != null) {
			q.setParameter("codigo", "%" + filtro.codigo +"%");
		}
		
		if(StringUtils.isNotEmpty(filtro.dirExecId)) {
			q.setParameter("dirExec",  Long.parseLong(filtro.dirExecId));
		}
		
		if(StringUtils.isNotEmpty(filtro.diretoria)) {
			q.setParameter("diretoria", Long.parseLong(filtro.diretoria));
		}
		
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Area> findAll(Empresa empresa) {
		StringBuffer sb = new StringBuffer("FROM Area a WHERE a.empresa.id = :empresaId");
		Query q = createQuery(sb.toString());
		q.setParameter("empresaId", empresa.getId());
		q.setCacheable(true);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Area findDefaultByEmpresa(Long empresaId) {
		Query q = createQuery("FROM Area a where a.empresa.id = :empresaId");
		q.setLong("empresaId", empresaId);
		List<Funcao> list = q.list();
		Area a = (Area) (list.size() > 0 ? q.list().get(0) : null);
		return a;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findIdNomeAreas(Long empresaId) {
		Query query = createQuery("select id,nome from Area where empresa.id = :empresaId");
		query.setLong("empresaId", empresaId);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Area findByName(Area area, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Area a where a.nome = :nome");
		sb.append(" and a.empresa = :empresa");
		
		if(area.getId() != null) {
			sb.append(" and a.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("empresa", empresa);
		q.setParameter("nome", area.getNome());
		
		if(area.getId() != null) {
			q.setParameter("id", area.getId());
		}
		
		List<Area> list = q.list();
		Area a = (Area) (list.size() > 0 ? q.list().get(0) : null);
		return a;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Area findByCodigoValid(Area area, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Area a where a.codigo = :codigo");
		sb.append(" and a.empresa = :empresa");
		
		if(area.getId() != null) {
			sb.append(" and a.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("codigo", area.getCodigo());
		q.setParameter("empresa", empresa);
		
		if(area.getId() != null) {
			q.setParameter("id", area.getId());
		}
		
		List<Area> list = q.list();
		Area d = (Area) (list.size() > 0 ? q.list().get(0) : null);
		return d;
	}
	
	
}