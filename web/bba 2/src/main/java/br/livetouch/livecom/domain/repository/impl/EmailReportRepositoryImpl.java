package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.infra.util.Utils;
import br.livetouch.livecom.domain.EmailReport;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoTemplate;
import br.livetouch.livecom.domain.repository.EmailReportRepository;
import br.livetouch.livecom.domain.vo.RelatorioEmailVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.spring.StringHibernateRepository;
import net.livetouch.tiger.ddd.DomainException;

@Repository
public class EmailReportRepositoryImpl extends StringHibernateRepository<EmailReport> implements EmailReportRepository {

	public EmailReportRepositoryImpl() {
		super(EmailReport.class);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<EmailReport> findAll(Empresa empresa) {
		StringBuffer sb = new StringBuffer("FROM EmailReport e WHERE e.empresa.id = :empresaId");
		Query q = createQuery(sb.toString());
		q.setParameter("empresaId", empresa.getId());
		q.setCacheable(true);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioEmailVO> reportEmail(RelatorioFiltro filtro) throws DomainException {
		Query query = queryReportEmail(filtro);

		int page = filtro.getPage();

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * filtro.getMax();
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(filtro.getMax());

		query.setCacheable(true);
		List<RelatorioEmailVO> emails = query.list();

		return emails;
	}
	
	public Query queryReportEmail(RelatorioFiltro filtro) throws DomainException {

		Date dataInicio = null;
		Date dataFim = null;
		
		TipoTemplate tipo = filtro.getTipoTemplateEnum();

		if (StringUtils.isNotEmpty(filtro.getDataIni())) {
			filtro.setDataInicial(DateUtils.toDate(filtro.getDataIni()));
		}
		if (StringUtils.isNotEmpty(filtro.getDataFim())) {
			filtro.setDataFinal(DateUtils.toDate(filtro.getDataFim()));
		}

		Long empresa = filtro.getEmpresaId();
		
		Usuario usuario = filtro.getUsuario();
		
		if (Utils.dataValida(filtro)) {
			if (filtro.getDataInicial() != null) {
				dataInicio = DateUtils.setTimeInicioDia(filtro.getDataInicial());
			}
			if (filtro.getDataFinal() != null) {
				dataFim = DateUtils.setTimeFimDia(filtro.getDataFinal());
			}
		}

		StringBuffer sb = new StringBuffer();
		sb.append("select new br.livetouch.livecom.domain.vo.RelatorioEmailVO(e) ");

		sb.append(" from EmailReport e ");
		
		sb.append(" where 1=1 ");
		
		if (dataInicio != null) {
			sb.append(" and e.data >= :dataIni");
		}

		if (dataFim != null) {
			sb.append(" and e.data <= :dataFim");
		}

		if (usuario != null) {
			sb.append(" and e.usuario = :usuario");
		}
		
		if (tipo != null) {
			sb.append(" and e.tipoTemplate = :tipo");
		}
		
		sb.append(" and e.status = :status");

		if (empresa != null) {
			sb.append(" and e.empresa.id = :empresa");
		}
		
		sb.append(" group by e.data, e.id");
		sb.append(" order by e.data desc");

		Query q = createQuery(sb.toString());

		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}

		if (usuario != null) {
			q.setParameter("usuario", usuario);
		}
		
		if (tipo != null) {
			q.setParameter("tipo", tipo);
		}
		
		q.setParameter("status", filtro.isEnviado());

		if (empresa != null) {
			q.setParameter("empresa", empresa);
		}
		
		q.setCacheable(false);

		return q;

	}
}