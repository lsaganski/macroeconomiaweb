package br.livetouch.livecom.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Permissao extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771785275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "PERMISSAO_SEQ")
	private Long id;

	private String nome;
	private String codigo;
	private String descricao;
	
	@OneToMany(mappedBy = "permissao", fetch = FetchType.LAZY)
	@XStreamOmitField
	private Set<PerfilPermissao> perfis;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "permissao_categoria_id", nullable = true)
	private PermissaoCategoria categoria;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String desc) {
		this.descricao = desc;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Set<PerfilPermissao> getPerfis() {
		return perfis;
	}

	public void setPerfis(Set<PerfilPermissao> perfis) {
		this.perfis = perfis;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public PermissaoCategoria getCategoria() {
		return categoria;
	}

	public void setCategoria(PermissaoCategoria categoria) {
		this.categoria = categoria;
	}
	
}
