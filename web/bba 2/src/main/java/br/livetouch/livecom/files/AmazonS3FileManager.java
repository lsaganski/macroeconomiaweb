package br.livetouch.livecom.files;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import br.infra.util.Log;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.utils.S3File;
import br.livetouch.livecom.utils.S3Helper;

public class AmazonS3FileManager implements FileManager {
	protected static final Logger log = Log.getLogger(FileManager.class);

	private AmazonS3 s3;
	private String bucket;
	private S3Helper s3Helper;
	private ParametrosMap params;
	
	public AmazonS3FileManager(ParametrosMap params) throws IOException {
		this.params = params;
		s3Helper = new S3Helper(params);
		s3 = s3Helper.connect();
		bucket = s3Helper.getBucket();
	}
	
	@Override
	public List<LivecomFile> getFiles() {
		List<S3ObjectSummary> all = s3Helper.listObjects(s3, bucket);
		List<LivecomFile> list = new ArrayList<LivecomFile>();
		for (S3ObjectSummary o : all) {
			S3File f = new S3File(o, this.params);
			list.add(f);
		}
		return list;
	}
	
	@Override
	public LivecomFile getFile(String name) {
		List<LivecomFile> files = getFiles();
		if(files != null) {
			for (LivecomFile f : files) {
				if(StringUtils.equals(name, f.getFileName())) {
					return f;
				}
			}
		}
		return null;
	}

	@Override
	public void delete(String path) throws IOException {
		log.debug("delete: " + path);
		int idx = path.indexOf("livecom");
		if(idx != -1) {
			log.debug("delete key: " + path);
			String key = path.substring(idx+8);
			s3.deleteObject(bucket,key);
		}
	
	}

	@Override
	public List<LivecomFile> getFilesInFolder(String folder) {
		List<LivecomFile> list = new ArrayList<LivecomFile>();
		ObjectListing objectListing = s3Helper.listObjectsInFolder(s3, bucket, folder+ "/");
		for (S3ObjectSummary o : objectListing.getObjectSummaries()) {
			S3File f = new S3File(o, this.params);
			list.add(f);
		}
		return list;
	}

	@Override
	public List<String> getFoldersInFolder(String folder) {
		List<String> listDir = new ArrayList<String>();
		ObjectListing objectListing = s3Helper.listObjectsInFolder(s3, bucket, folder + "/");
		List<String> listaString  = objectListing.getCommonPrefixes();
		listDir = new ArrayList<String>();
		for (String s : listaString) {
			String nome = s.replaceAll(folder, "");
			nome =  nome.replaceFirst("/", "");
			listDir.add(nome);
		}
		return listDir;
	}

	@Override
	public String putFile(String dir, String fileName, String contentType,byte[] bytes) throws IOException {
		return s3Helper.putFile(dir, fileName, contentType, bytes);
	}

	@Override
	public String putFileLambda(String dir, String fileName, String contentType,byte[] bytes) throws IOException {
		return s3Helper.putFileChat(dir, fileName, contentType, bytes);
	}

	@Override
	public String putFileThumb(String dir, String fileName, String contentType,byte[] bytes) throws IOException {
		return s3Helper.putFileThumb(dir, fileName, contentType, bytes);
	}

	@Override
	public String getFileUrl(String dir, String nome) {
		return s3Helper.getFileURL(dir, nome);
	}

	@Override
	public void createFolder(String dir) throws IOException {
		s3Helper.createFolder(dir);
	}
	
	@Override
	public boolean somenteFotosProfile(String dir) {
		ObjectListing objectListing = s3Helper.listFilesInFolder(s3, bucket, dir+ "/");
		dir = dir + "/fotoProfile";
		for (S3ObjectSummary o : objectListing.getObjectSummaries()) {
			S3File f = new S3File(o, this.params);
			//082b8e87-ee47-426e-b561-c5b754aa9502/fotoProfile
			
			String folder = s3Helper.getDirFromURL(f.getUrl());
			if (!folder.equals(dir)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void close() throws IOException {
		
	}

	@Override
	public String getFileLambdaUrl(String dir, String nomeS3) {
		return s3Helper.getFileLambdaURL(dir, nomeS3);
	}

	@Override
	public String getThumbLambdaURL(String dir, String nomeS3) {
		return s3Helper.getThumbLambdaURL(dir, nomeS3);
	}
}
