package br.livetouch.livecom.chatAkka.protocol;

import akka.actor.ActorRef;

public class Toast {

	private String msg;
	private ActorRef actor;

	public Toast(ActorRef actor,String msg) {
		super();
		this.actor = actor;
		this.msg = msg;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public ActorRef getActor() {
		return actor;
	}

	@Override
	public String toString() {
		return "Toast [msg=" + msg + "]";
	}
	
	
}
