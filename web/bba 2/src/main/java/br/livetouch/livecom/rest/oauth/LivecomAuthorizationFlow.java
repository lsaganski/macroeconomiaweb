package br.livetouch.livecom.rest.oauth;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.log4j.Logger;
import org.glassfish.jersey.client.oauth1.AccessToken;
import org.glassfish.jersey.client.oauth1.ConsumerCredentials;
import org.glassfish.jersey.client.oauth1.OAuth1AuthorizationFlow;
import org.glassfish.jersey.client.oauth1.OAuth1Builder.FlowBuilder;
import org.glassfish.jersey.client.oauth1.OAuth1ClientSupport;
import org.glassfish.jersey.server.oauth1.OAuth1Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.rest.MyApplication;
import br.livetouch.livecom.rest.domain.UserPrincipal;

@Component
public class LivecomAuthorizationFlow {
	
	@Context HttpServletRequest req; 
	
	private static Logger log = Logger.getLogger(LivecomOAuthProvider.class);
	private final String REQUEST_TOKEN = "/rest/requestToken";
	private final String ACCESS_TOKEN = "/rest/accessToken";
	private final String LOGON_PAGE = "/logon.htm";
	
	
	@Autowired
	protected LivecomOAuthProvider livecomOauthProvider;
	
	/**
	 * Metodo para criar os token e adicionar na sessão
	 * 
	 * @param usuario
	 * @param req
	 */
	public void accessToken(Usuario usuario,HttpServletRequest req) {
		LivecomToken rt = new LivecomToken();
		String token = LivecomToken.newUUIDString();
		String secret = LivecomToken.newUUIDString();
		
		String consumerKey = ParametrosMap.getInstance().get(Params.LIVECOM_CONSUMER_KEY, MyApplication.CONSUMER_KEY);
		rt.newAccessToken(token, secret, consumerKey, usuario);
		AccessTokenMap.getInstance().put(token, rt);
		req.getSession().setAttribute("token", rt.getToken());
		req.getSession().setAttribute("accessToken", rt.getSecret());
		req.getSession().setAttribute("consumerKey", rt.getConsumer().getKey());
		req.getSession().setAttribute("consumerSecret", rt.getConsumer().getSecret());
		
		String logString = "Access token gerado, token:  %s, Secret:  %s, "
				+ "ConsumerKey: %s, ConsumerSecret: %s, userPrincipal: %s, permisao: %s";
		UserPrincipal userPrincipal = (UserPrincipal) rt.getPrincipal();
		OAuth1Consumer consumer = rt.getConsumer();
		
		
		
		log.debug(String.format(
				logString, rt.getToken(), rt.getSecret(), 
				consumer.getKey(), consumer.getSecret(), userPrincipal.getName(), userPrincipal.getPermisao())
		);
		
	}
	
	/**
	 * Metodo para gerar os token de autenticação para o mobile
	 * 
	 * @param req
	 * @param resp
	 * @param usuario
	 * @param consumerKey
	 * @param consumerSecret
	 * @return
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public AccessToken accessTokenMobile(HttpServletRequest req, HttpServletResponse resp, Usuario usuario, String consumerKey, String consumerSecret) throws IOException, URISyntaxException {
		ConsumerCredentials consumerCredentials = new ConsumerCredentials(consumerKey, consumerSecret);
		FlowBuilder builder = OAuth1ClientSupport.builder(consumerCredentials).authorizationFlow( getURL() + REQUEST_TOKEN, getURL() + ACCESS_TOKEN, getURL() + LOGON_PAGE);
		
		OAuth1AuthorizationFlow authFlow = builder.build();
		
		String authorizationUri = authFlow.start();
		List<NameValuePair> params = URLEncodedUtils.parse(new URI(authorizationUri), "UTF-8");
		String token = params.get(0).getValue();
		
		final LivecomToken requestToken = (LivecomToken) livecomOauthProvider.getRequestToken(token);
		Set<String> roles = new HashSet<>();
		roles.add(usuario.getPermissao().getNome().toLowerCase());
		roles.add("logado");
		UserPrincipal userPrincipal = new UserPrincipal(usuario);
		String verifier = authorizeToken(requestToken, userPrincipal, roles);
		
		return authFlow.finish(verifier);
	}
	
	private String authorizeToken(LivecomToken token, Principal userPrincipal, Set<String> roles) {
		token.authorize(userPrincipal, roles);
		RequestTokenMap.getInstance().put(token.getToken(), token);
		String verifier = LivecomToken.newUUIDString();
		VerifierTokenMap.getInstance().put(token.getToken(), verifier);
		return verifier;
	}
	
	private String getURL() {
		StringBuffer url = req.getRequestURL();
		String uri = req.getRequestURI();
		String ctx = req.getContextPath();
		return url.substring(0, url.length() - uri.length() + ctx.length());
	}
	
}