package br.livetouch.livecom.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import net.livetouch.extras.util.DateUtils;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NotificationUsuario extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;
	public static final String KEY = "Notification";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "NOTIFICATION_USUARIO_SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario usuario;

	private Date dataLida;
	private boolean lida = false;
	private boolean visivel = false;
	
	private Date dataPush;
	

	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.REMOVE)
	@JoinColumn(name = "notification_id", nullable = true)
	private Notification notification;
	
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getDataLidaString() {
		return DateUtils.toString(dataLida, "dd/MM/yyyy HH:mm:ss");
	}
	
	public String getDataLidaStringHojeOntem() {
		return br.livetouch.livecom.utils.DateUtils.toDateStringHojeOntem(dataLida);
	}

	public Date getDataLida() {
		return dataLida;
	}
	
	public void setDataLida(Date dataLida) {
		this.dataLida = dataLida;
	}

	public boolean isLida() {
		return lida;
	}
	public void setLida(boolean lida) {
		this.lida = lida;
	}
	
	public void setDataPush(Date dataPush) {
		this.dataPush = dataPush;
	}
	
	public Date getDataPush() {
		return dataPush;
	}

	public Notification getNotification() {
		return notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isVisivel() {
		return visivel;
	}

	public void setVisivel(boolean visivel) {
		this.visivel = visivel;
	}
}
