package br.livetouch.livecom.domain.vo;

import java.util.List;

public class AudienciaQuantidadeVO {

	private AudienciaVO audiencia;
	private List<RelatorioAudienciaVO> relatorioAudiencia;
	
	public AudienciaQuantidadeVO(){
		
	}
	
	public AudienciaQuantidadeVO(AudienciaVO audienciaVoEntrada, List<RelatorioAudienciaVO> relatorioAudienciaVoEntrada ){
		this.audiencia = audienciaVoEntrada;
		this.relatorioAudiencia = relatorioAudienciaVoEntrada;
	}
	
	public AudienciaVO getAudiencia() {
		return audiencia;
	}

	public void setAudiencia(AudienciaVO audiencia) {
		this.audiencia = audiencia;
	}

	public List<RelatorioAudienciaVO> getRelatorioAudiencia() {
		return relatorioAudiencia;
	}

	public void setRelatorioAudiencia(List<RelatorioAudienciaVO> relatorioAudiencia) {
		this.relatorioAudiencia = relatorioAudiencia;
	}
	
}
