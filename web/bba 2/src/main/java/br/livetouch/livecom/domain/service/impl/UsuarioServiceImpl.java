package br.livetouch.livecom.domain.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.chat.webSocket.WebSocketChat;
import br.livetouch.livecom.connector.login.LoginConnectorService;
import br.livetouch.livecom.domain.Amizade;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.LogAuditoria;
import br.livetouch.livecom.domain.LogSistema;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.UsuarioAcessoMural;
import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import br.livetouch.livecom.domain.enums.GrupoNotification;
import br.livetouch.livecom.domain.enums.StatusAmizade;
import br.livetouch.livecom.domain.enums.StatusParticipacao;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.exception.SenhaExpiradaException;
import br.livetouch.livecom.domain.exception.SenhaInvalidaException;
import br.livetouch.livecom.domain.repository.UsuarioRepository;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.ComentarioService;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.FavoritoService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.LikeService;
import br.livetouch.livecom.domain.service.LogService;
import br.livetouch.livecom.domain.service.MensagemService;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.service.PerfilService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.StatusMensagemUsuarioService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.AlterarSenhaVO;
import br.livetouch.livecom.domain.vo.ControleVersaoVO;
import br.livetouch.livecom.domain.vo.ExportarUsuariosFiltro;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVO;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioFuncionaisVO;
import br.livetouch.livecom.domain.vo.StatusAtivacaoVO;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.domain.vo.UsuarioAutocompleteFiltro;
import br.livetouch.livecom.domain.vo.UsuarioStatusCountVO;
import br.livetouch.livecom.domain.vo.UsuarioVO;
import br.livetouch.livecom.jobs.ConvidarUsuariosEmailJob;
import br.livetouch.livecom.jobs.JobUtil;
import br.livetouch.livecom.push.PushNotificationVO;
import br.livetouch.livecom.security.LivecomSecurity;
import br.livetouch.livecom.sender.Sender;
import br.livetouch.livecom.sender.SenderFactory;
import br.livetouch.livecom.sender.SenderRecuperarSenha;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.livecom.utils.LivecomUtil;
import br.livetouch.pushserver.lib.PushNotification;
import br.livetouch.pushserver.lib.UsuarioToPush;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.extras.util.CripUtil;
import net.livetouch.extras.util.ExceptionUtil;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class UsuarioServiceImpl extends LivecomService<Usuario> implements UsuarioService {
	protected static final Logger log = Log.getLogger(UsuarioService.class);

	@Autowired
	private UsuarioRepository rep;

	@Autowired
	private LogService logService;

	@Autowired
	private MensagemService mensagemService;

	@Autowired
	private ArquivoService arquivoService;

	@Autowired
	private PostService postService;

	@Autowired
	protected FavoritoService favoritoService;

	@Autowired
	protected LikeService likeService;

	@Autowired
	protected EmpresaService empresaService;

	@Autowired
	protected ComentarioService comentarioService;

	@Autowired
	protected PerfilService permissaoService;

	@Autowired
	protected GrupoService grupoService;
	
	@Autowired
	protected LoginConnectorService loginConnector;

	@Autowired
	protected ApplicationContext applicationContext;

	@Autowired
	protected NotificationService notificationService;
	
	@Autowired
	protected StatusMensagemUsuarioService statusMensagemUsuarioService;

	@Override
	public Usuario get(Long id) {
		return rep.get(id);
	}

	@Override
	public Usuario load(Long id) {
		return rep.load(id);
	}
	
	@Override
	public void delete(Usuario userInfo, Usuario u) throws DomainException {
		delete(userInfo, u,false);
	}

	/**

* Regras:

(r1) Não permite excluir o Admin do sistema.
(r2) Não permite excluir usuários que tenham Posts.
(r3) Não permite excluir usuários que tenham Arquivos.
(r4) Não permite excluir usuários que tenham grupos criados.

* Cascades ao excluir:

(c1) Reassocia: Ao excluir o usuário transfere seus Posts, Grupos, Categorias e Tags criadas para o Admin.
(c2) Exclui Tudo: Ao excluir o usuário remove seus Posts, Arquivos, Grupos, Categorias e Tags.

	 */
	@Override
	public void delete(Usuario userInfo, Usuario u, boolean force) throws DomainException {
		if (u == null) {
			return;
		}
		
		if(!u.isCanDelete()) {
			throw new DomainException("Não é possível excluir esse usuário (flag).");
		}

		if (userInfo != null && userInfo.getId().equals(u.getId())) {
			throw new DomainException("Por acaso você está tentando excluir você mesmo?");
		}
		
		boolean r1 = true;
		Empresa empresa = u.getEmpresa();
		boolean r2 = ParametrosMap.getInstance(empresa).getBoolean(Params.DELETAR_USUARIO_R2_ON,true);
		boolean r3 = ParametrosMap.getInstance(empresa).getBoolean(Params.DELETAR_USUARIO_R3_ON,true);
		boolean r4 = ParametrosMap.getInstance(empresa).getBoolean(Params.DELETAR_USUARIO_R4_ON,true);

		boolean c1 = ParametrosMap.getInstance(empresa).getBoolean(Params.DELETAR_USUARIO_C1_ON,false);
		boolean c2 = ParametrosMap.getInstance(empresa).getBoolean(Params.DELETAR_USUARIO_C2_ON,false);

		if(!force) {

			if(r1) {
				if(u.isAdminEmpresa()) {
					throw new DomainException("Não é possível excluir o admin do sistema.");
				}
			}

			if(r2) {
				List<Long> postsIds = postService.findIdsCriadosByUser(u);
				if(postsIds != null && postsIds.size() > 0) {
					throw new DomainException("Este usuário postou comunicados e não pode ser excluído.");
				}
			}

			if(r3) {
				List<Long> arquivos = arquivoService.findIdsByUser(u);
				if(arquivos.size() > 1) {
					throw new DomainException("Este usuário possui arquivos e não pode ser excluído.");
				}
			}
			
			if(r4) {
				List<Grupo> grupos = grupoService.findAllByUser(u);
				if(grupos != null && grupos.size() > 0) {
					throw new DomainException("Este usuário possui grupos e não pode ser excluído.");
				}
			}
		} else {
			if(u.isAdminEmpresa()) {
				execute("update Empresa set admin=null where id=?",empresa.getId());
			}
		}

		log.debug("delete: " + u.getId() + ":" + u.getLogin());

		Usuario responsavel = u.getResponsavel() != null ? u.getResponsavel() : empresa.getAdmin();
//		if(admin != null) {
//			
//		}

		LogAuditoria auditoria = null;

		try {

			// Auditoria
			if(userInfo != null) {
				String msg = "Usuário " + u.getNome() + " deletado com sucesso";
				auditoria = LogAuditoria.log(userInfo, u, AuditoriaEntidade.USUARIO, AuditoriaAcao.DELETAR,msg,false);
			}
			
			// Comentarios deste user em outros posts
			List<Long> comentariosUser = comentarioService.findAllIdsByUser(u);
			comentarioService.delete(userInfo, comentariosUser, false );

			if(!force && c1) {
				/**
				 * Reassocia
				 */
				
				// Arquivos
				execute("update Arquivo set usuario.id = ? where usuario.id=?",responsavel.getId(),u.getId());

				// Tag
				execute("update Tag set userCreate.id = ? where userCreate.id=?",responsavel.getId(),u.getId());
				execute("update Tag set userUpdate.id = ? where userUpdate.id=?",responsavel.getId(),u.getId());

				// Categ
				execute("update CategoriaPost set userCreate.id = ? where userCreate.id=?",responsavel.getId(),u.getId());
				execute("update CategoriaPost set userUpdate.id = ? where userUpdate.id=?",responsavel.getId(),u.getId());

				// Grupo Posts
				execute("update Grupo set usuario.id = ? where usuario.id=?",responsavel.getId(),u.getId());
				execute("update Grupo set userCreate.id = ? where userCreate.id=?",responsavel.getId(),u.getId());
				execute("update Grupo set userUpdate.id = ? where userUpdate.id=?",responsavel.getId(),u.getId());
				
				// Posts
				execute("update Post set usuarioUpdate.id = ? where usuarioUpdate.id=?",responsavel.getId(),u.getId());
				execute("update Post set usuario.id = ? where usuario.id=?",responsavel.getId(),u.getId());
			}
			
			if(c2 || force) {
				/**
				 * Excluir
				 * (c2) Ao excluir o usuário remove seus Posts, Arquivos, Grupos, Categorias e Tags.
				 */

				// Arquivos
				execute("update Usuario u set u.capa = null where u.id = ?", u.getId());
				List<Long> ids = arquivoService.findIdsByUser(u);
				arquivoService.delete(userInfo,ids,false);

				// Posts
				execute("update Post set usuarioUpdate = null where usuarioUpdate.id=?",u.getId());
				ids = postService.findAllOwnerByUser(u);
				postService.delete(userInfo, ids, false,force);
				
				// Tag
				execute("update Tag set userCreate = null where userCreate.id=?",u.getId());
				execute("update Tag set userUpdate = null where userUpdate.id=?",u.getId());
				
				// Categ
				execute("update CategoriaPost set userCreate = null where userCreate.id=?",u.getId());
				execute("update CategoriaPost set userUpdate = null where userUpdate.id=?",u.getId());
				
				// Grupo Posts
				execute("update Grupo set usuario = null where usuario.id=?",u.getId());
				execute("update Grupo set userCreate = null where userCreate.id=?",u.getId());
				execute("update Grupo set userUpdate = null where userUpdate.id=?",u.getId());
			}
			
			// Foto
			execute("update Usuario set foto = null where id=?",u.getId());

			// Notifications
			notificationService.deleteBy(u);
			
			// Restante dos deletes	
			execute("delete from EmailReport a where a.usuario.id=?", u.getId());
			execute("delete from UsuarioMarcado where usuario.id=?", u.getId());
			execute("delete from PostView where usuario.id=?", u.getId());
			execute("delete from PostUsuarios where usuario.id=?", u.getId());
			execute("delete from ConversaNotification where usuario.id=?", u.getId());
			
			//Amizade
			execute("delete from Amizade where usuario.id=? or amigo.id=?", u.getId(), u.getId());

			// Acesso Mural
			execute("delete from UsuarioAcessoMural a where a.usuario.id=?", u.getId());
			execute("delete from LoginReport a where a.usuario.id=?", u.getId());
			
			// Atualiza user create, se é ele mesmo.
			execute("update Usuario set userCreate = null where id=?",u.getId());
			execute("update Usuario set userUpdate = null where id=?",u.getId());

			// Likes
			execute("delete from Likes where usuario.id=?",u.getId());

			// Remove ele dos grupos
			execute("delete from GrupoUsuarios where usuario.id=?",u.getId());
			
			//cards
			executeNative("delete from card_button where card_id in (select id from card where mensagem_id in (select id from mensagem where usuario_to_id = ? or usuario_from_id = ?))",u.getId(),u.getId());
			executeNative("delete from card where mensagem_id in (select id from mensagem where usuario_to_id = ? or usuario_from_id = ?)",u.getId(),u.getId());
			
			// Chat
			// Limpa a ultima mensagem 'last_msg_id' de alguma MensagemConversa
			execute("update MensagemConversa c set c.lastMensagem = null where c.lastMensagem.id in (select msg.id from Mensagem msg where msg.from.id=? or msg.to.id=?)",u.getId(), u.getId());
			execute("update Usuario u set statusChat = null where u.id=?",u.getId());
			execute("update Usuario u set statusChat = null where statusChat.id in (select id from StatusChat where usuario.id=?)",u.getId());
			execute("delete StatusChat where usuario.id=?",u.getId());
			execute("delete StatusMensagemUsuario where usuario.id=?",u.getId());

			// Mensagens
			List<Long> msgsIds = mensagemService.findAllMensagensIdsOwnerByUser(u);
			mensagemService.deleteMensagens(msgsIds);

			List<Long> conversasIds = mensagemService.findAllIdsConversasByUser(u);
			mensagemService.deleteConversas(conversasIds);
			
			// Grupo Mensagens
//			execute("update GrupoMensagem set userCreate = null where userCreate.id=?",u.getId());
//			execute("update GrupoMensagem set userUpdate = null where userUpdate.id=?",u.getId());
//			execute("delete from GrupoMensagemUsuarios where usuario.id=?",u.getId());

			// Deletes 
			execute("delete from LogSistema where usuario.id=?",u.getId());
			execute("delete from LogTransacao where usuario.id=?",u.getId());
			execute("delete from Likes where usuario.id=?",u.getId());
			execute("delete from Favorito where usuario.id=?",u.getId());
			
			// Auditoria antiga
			execute("delete from CategoriaPostAuditoria a where a.user.id=?", u.getId());
			execute("delete from GrupoAuditoria a where a.user.id=?", u.getId());
			execute("delete from PostAuditoria a where a.user.id=?", u.getId());
			execute("delete from UsuarioAuditoria a where a.user.id=?", u.getId());
			execute("delete from TagAuditoria a where a.user.id=?", u.getId());

			// Auditoria nova
			execute("delete from LogAuditoria a where a.usuario.id=? ", u.getId());
			execute("delete from LogSistema a where a.usuario.id=? ", u.getId());
			execute("delete from LogTransacao a where a.usuario.id=? ", u.getId());
			
			// perfil
			execute("update Usuario set permissao = null where id=?",u.getId());
			
			// Application
			execute("delete from Application a where a.usuario.id=? ", u.getId());

			// set Null função
			execute("update Usuario set funcao = null where id=?",u.getId());
			
			// Application
			execute("delete from Application a where a.usuario.id=? ", u.getId());

			// set Null função
			execute("update Usuario set funcao = null where id=?",u.getId());
			
			// delete
			rep.deleteById(u.getId());
			
			// OK
			if(auditoria != null) {
				auditoria.finish();
			}

		} catch (DataIntegrityViolationException e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if(root == null) {
				root = e;
			}
			log.error("Erro ao excluir o usuário [" + u + "]: " + root.getMessage(), root);
			
			LogSistema.logError(userInfo, e);
			
			throw e;
		} catch (Exception e) {
			Throwable root = ExceptionUtils.getRootCause(e);
			if(root == null) {
				root = e;
			}
			String msg = "Erro ao excluir o usuário [" + u + "]: " + root.getMessage();
			log.error(msg, root);
			LogSistema.logError(userInfo, e);
			
			throw e;
		} finally {
			
		}
	}

	@Override
	public List<Usuario> findAll(Empresa empresa) {
		return rep.findAll(empresa);
	}

	@Override
	public Usuario findByLogin(String login) {
		return rep.findByLogin(login, null);
	}

	@Override
	public Usuario findByLogin(String login, Empresa empresa) {
		return rep.findByLogin(login, empresa);
	}

	@Override
	public Usuario findByEmail(String email) {
		return rep.findByEmail(email);
	}
	
	@Override
	public boolean saveAudit(Usuario userInfo, Usuario u) throws DomainException {
		Long id = u.getId();
		boolean insert = id == null;
		AuditoriaAcao acao = insert ? AuditoriaAcao.INSERIR : AuditoriaAcao.EDITAR;
		String msg = null;
		if(insert) {
			msg = "Usuário " + u.getNome() + " criado com sucesso";
		} else {
			msg = "Usuário " + u.getNome() + " editado com sucesso";
		}
		try {
			saveOrUpdate(userInfo, u);
			LogAuditoria.log(userInfo, u, AuditoriaEntidade.USUARIO, acao, msg);
			UserInfoVO.update(u);
			return true;
		} catch(Exception e) {
			LogAuditoria.log(userInfo, u, AuditoriaEntidade.USUARIO, acao, "Não foi possível realizar o cadastro do [" + u.getNome() + "]", e);
			throw new DomainException(e);
		}
	}

	@Override
	public boolean saveOrUpdate(Usuario userInfo, Usuario u) throws DomainException {
		Long id = u.getId();
		boolean insert = id == null;
		
		if (u.getStatus() == null) {
			u.setStatus(StatusUsuario.NOVO);
		}
		
		if (StringUtils.isEmpty(u.getChave())) {
			this.gerarChave(u);
		}
		
		if(insert) {
			u.setDataCreated(new Date());
			u.setUserCreate(userInfo);
		}
		u.setDataUpdated(new Date());
		u.setUserUpdate(userInfo);
		
		try {
			rep.saveOrUpdate(u);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}
	

	@Override
	public List<Usuario> findByFilter(Usuario userInfo, Usuario filtro, int page, int pageSize) {
		return rep.findByFilter(userInfo, filtro, page, pageSize, userInfo.getEmpresa());
	}

	@Override
	public void alterarSenha(Usuario usuario, AlterarSenhaVO alterar) throws DomainException {
		if (!usuario.validaSenha(alterar.getSenhaAtual())) {
			throw new DomainException("msg.senha.atual.error");
		}

		if (!StringUtils.equals(alterar.getSenhaNova(), alterar.getConfirmaSenha())) {
			throw new DomainException("msg.senha.confirmacao.error");
		}

		if (StringUtils.isEmpty(alterar.getSenhaNova())) {
			throw new DomainException("msg.senha.nova.error");
		}

		String cript = CripUtil.criptToMD5(alterar.getSenhaNova());

		usuario.setSenha(cript);
		usuario.setDataUpdatedSenha(new Date());
		usuario.setDataLastLogin(new Date());

		rep.saveOrUpdate(usuario);
	}
	
	@Override
	public void alterarSenhaExpirada(Usuario usuario, AlterarSenhaVO alterar) throws DomainException {
		if (!StringUtils.equals(alterar.getSenhaNova(), alterar.getConfirmaSenha())) {
			throw new DomainException("msg.senha.confirmacao.error");
		}

		if (StringUtils.isEmpty(alterar.getSenhaNova())) {
			throw new DomainException("msg.senha.nova.error");
		}

		String cript = CripUtil.criptToMD5(alterar.getSenhaNova());

		usuario.setSenha(cript);
		usuario.setDataLastLogin(new Date());
		usuario.setDataUpdatedSenha(new Date());

		rep.saveOrUpdate(usuario);
	}

	@Override
	public void esqueciSenha(Usuario usuario, String novaSenha, String confirmaNovaSenha) throws DomainException {
		if (StringUtils.isEmpty(novaSenha)) {
			throw new DomainException("Informe a nova senha");
		}

		if (StringUtils.isEmpty(confirmaNovaSenha)) {
			throw new DomainException("Informe confirmação da senha");
		}

		if (!StringUtils.equals(novaSenha, confirmaNovaSenha)) {
			throw new DomainException("Senha de confirmação incorreta");
		}

		String cript = CripUtil.criptToMD5(novaSenha);
		usuario.setSenha(cript);
		rep.saveOrUpdate(usuario);
	}

	@Override
	public Usuario login(String login, String senha) throws DomainException {
		boolean isMobile = false;
		return login(login, senha, null, isMobile);
	}

	@Override
	public Usuario login(String login, String senha, Empresa empresa, boolean isMobile) throws DomainException {
		Usuario u = null;
		try {
			
			LivecomSecurity.checkBlockLogin(login, empresa.getId());
			/**
			 * O login precisa existir na base do livecom
			 */
			u = rep.findByLoginFetch(login, empresa, isMobile);
			if (u != null) {
				/**
				 * Mas a senha pode ficar fora.
				 */
				boolean ok = loginConnector.validate(u, senha);
				if (ok) {

					if (u.getDataFirstLogin() == null) {
						u.setDataFirstLogin(new Date());
					}
					if(!LivecomSecurity.isExpiredPasswordByInactivity(u)){
						u.setDataLastLogin(new Date());
					}
					if (u.getEmpresa() == null) {
						Empresa e = empresaService.get(9L);
						u.setEmpresa(e);
					}
					u.setReceivePush(true);
					saveOrUpdate(u, u);

					return u;
				} else {
					isLogoutApp("Senha incorreta.", u);
					throw new SenhaInvalidaException("Usuário não encontrado ou senha inválida");
				}
			}

			throw new SenhaInvalidaException("Usuário não encontrado ou senha inválida");
		} catch (DomainMessageException e) {
			isLogoutApp(e.getMessage(), u);
			throw e;
		} catch (SenhaInvalidaException e) {
			isLogoutApp(e.getMessage(), u);
			throw e;
		} catch (SenhaExpiradaException e) {
			isLogoutApp(e.getMessage(), u);
			throw e;
		} catch (Throwable e) {
			log.error(e.getMessage(), e);
			LogSistema log = LogSistema.logError(u, "Exception login: " + ExceptionUtil.getStackTrace(e));
			logService.saveOrUpdate(log, empresa);
			isLogoutApp("Usuário não encontrado ou senha inválida", u);
			throw new DomainException("Usuário não encontrado ou senha inválida");
		}
	}

	private void isLogoutApp(String msg, Usuario u) throws DomainException, SenhaInvalidaException {
		if(ParametrosMap.getInstance().getBoolean(Params.RESTRICAO_FERIAS_ON, false)) {
			if(u != null) {
				u.setReceivePush(false);
				saveOrUpdate(u, u);
			}
			throw new SenhaInvalidaException(msg, "logout");
		}
	}

	@Override
	public long count() {
		return rep.getCount();
	}

	@Override
	public long getCountByFilter(Usuario usuarioSistema, Usuario filtro) {
		return rep.getCountByFilter(usuarioSistema, filtro);
	}

	@Override
	public List<Usuario> findByFiltro(UsuarioAutocompleteFiltro filtro, Empresa empresa) {
		return rep.findByFiltro(filtro, empresa);
	}

	@Override
	public List<Usuario> findAllByIds(List<Long> usersIds) {
		if (usersIds == null || usersIds.isEmpty()) {
			return new ArrayList<Usuario>();
		}
		return rep.findAllByKeys(usersIds);
	}

	@Override
	public void gerarChave(Usuario u) throws DomainException {
		if (StringUtils.isEmpty(u.getChave())) {
			UUID uuid = UUID.randomUUID();
			u.setChave(uuid.toString());
			rep.saveOrUpdate(u);
		}
	}

	@Override
	public List<StatusAtivacaoVO> findStatusAtivacao(Empresa empresa) {
		return rep.findStatusAtivacao(empresa);
	}

	@Override
	public UsuarioAcessoMural findUsuarioAcessoMural(Usuario u) {
		UsuarioAcessoMural a = rep.findUsuarioAcessoMural(u, null);
		return a;
	}

	@Override
	public UsuarioAcessoMural saveUsuarioAcessoMural(Usuario u, Post post) {
		UsuarioAcessoMural a = rep.findUsuarioAcessoMural(u, null);
		if (a == null) {
			a = new UsuarioAcessoMural();
		}

		a.setUsuario(u);
		a.setDate(new Date());
		a.setPost(post);
		rep.saveOrUpdateUsuarioAcesso(a);

		return a;
	}

	@Override
	public void saveOrUpdate(GrupoUsuarios gu) {
		rep.saveOrUpdate(gu);
	}

	@Override
	public void saveOrUpdate(Amizade a) {
		rep.saveOrUpdate(a);
	}

	@Override
	public void insertGrupoUsuario(Long userId, Long grupoId) throws SQLException {
		rep.insertGrupoUsuario(userId, grupoId);
	}

	@Override
	public void saveGruposToUser(Usuario u, List<Grupo> grupos) {
		for (Grupo g : grupos) {
			GrupoUsuarios gu = new GrupoUsuarios();
			gu.setUsuario(u);
			gu.setGrupo(g);
			gu.setPush(true);
			gu.setMostraMural(true);
			gu.setPostar(true);
			rep.saveOrUpdate(gu);
		}
	}
	
	@Override
	public void saveGruposToUser(Usuario u, Set<GrupoUsuarios> grupos, List<Grupo> list) {
		for (GrupoUsuarios gu : grupos) {
			rep.saveOrUpdate(gu);
		}
	}

	@Override
	public GrupoUsuarios addGrupoToUser(Usuario u, Grupo g, boolean postar) {
		GrupoUsuarios gu = findCreateGrupoUsuario(u, g);
		gu.setPostar(postar);
		rep.saveOrUpdate(gu);
		return gu;
	}

	@Override
	public GrupoUsuarios addSubadmin(Usuario u, Grupo g, boolean admin) {
		GrupoUsuarios gu = findCreateGrupoUsuario(u, g);
		gu.setAdmin(admin);
		rep.saveOrUpdate(gu);
		return gu;
	}

	@Override
	public GrupoUsuarios addGrupoToUserSemSalvar(Usuario u, Grupo g, boolean postar) {
		GrupoUsuarios gu = findCreateGrupoUsuario(u, g);
		gu.setPostar(postar);
		return gu;
	}

	private GrupoUsuarios findCreateGrupoUsuario(Usuario u, Grupo g) {
		GrupoUsuarios gu = getGrupoUsuario(u, g);
		if(gu == null) {
			gu = new GrupoUsuarios();
			gu.setPush(true);
			gu.setMostraMural(true);
			gu.setPostar(true);
			gu.setAdmin(false);
		}
		gu.setUsuario(u);
		gu.setGrupo(g);
		gu.setStatusParticipacao(StatusParticipacao.APROVADA);
		return gu;
	}
	
	@Override
	public void saveUsersToGrupo(Usuario userInfo, Grupo g, List<Usuario> users) throws DomainException {
		if (users != null) {
			for (Usuario u : users) {
				GrupoUsuarios gu = new GrupoUsuarios();
				gu.setUsuario(u);
				gu.setGrupo(g);
				gu.setPostar(true);
				gu.setMostraMural(true);
				gu.setPush(true);
				gu.setStatusParticipacao(StatusParticipacao.APROVADA);
				rep.saveOrUpdate(gu);
				String msg = "Usuario ["+u.getNome()+"] adicionado ao grupo ["+g.getNome()+"]";
				LogAuditoria.log(userInfo, g, AuditoriaEntidade.GRUPO, AuditoriaAcao.INSERIR, msg);
				
				grupoService.criarNotificacao(gu, TipoNotificacao.GRUPO_USUARIO_ADICIONADO);
			}
		}
	}

	@Override
	public void deleteUserToGrupo(Grupo g) {
		rep.deleteGrupoUsuarios(g);
	}

	@Override
	public List<Grupo> getGrupos(Usuario u) {
		return rep.getGrupos(u);
	}

	@Override
	public List<Grupo> getGrupos(Usuario u, boolean postar) {
		return rep.getGrupos(u, postar);
	}

	@Override
	public void deleteGrupoUsuarios(Grupo g, Usuario u) {
		rep.deleteGrupoUsuarios(g, u);
	}
	
	@Override
	public void deleteGrupoUsuarios(GrupoUsuarios grupoUsuarios) {
		rep.deleteGrupoUsuarios(grupoUsuarios);
	}

	@Override
	public void deleteGrupoUsuarios(Usuario u, List<Grupo> grupos) {
		rep.deleteGrupoUsuarios(u, grupos);
	}
	
	@Override
	public void deleteAmizade(Usuario u, Usuario amigo) {
		rep.deleteAmizade(u, amigo);
	}

	@Override
	public GrupoUsuarios getGrupoUsuario(Usuario usuario, Grupo grupo) {
		return rep.getGrupoUsuario(usuario, grupo);
	}

	@Override
	public Amizade getAmizade(Usuario usuario, Usuario amigo) {
		return rep.getAmizade(usuario, amigo);
	}

	@Override
	public List<Usuario> findUsuariosByGrupo(Grupo grupo, int page, int maxSize) {
		return rep.findUsuariosByGrupo(grupo, page, maxSize);
	}

	@Override
	public List<Usuario> findAll(Usuario user, int page, int max) {
		return rep.findAll(user, page, max);
	}

	@Deprecated
	@Override
	public List<Usuario> findAllByStatusAtivacaoByGrupo(Grupo g, int page, Integer maxRows, String status) {
		return rep.findAllByStatusAtivacaoByGrupo(g, page, maxRows, status);
	}
	
	@Override
	public List<Usuario> findByStatus(Grupo g, StatusUsuario status, Empresa empresa, int page, Integer maxRows) {
		return rep.findByStatus(g, status, empresa, page, maxRows);
	}

	@Override
	public void deleteAllUsersByTipoImportacao() {
		rep.deleteAllUsersByTipoImportacao();
	}

	@Override
	public List<String> findLogins() {
		return rep.findLogins();
	}

	@Override
	public List<Object[]> findIdLoginsTipoImportacao() {
		return rep.findLoginsTipoImportacao();
	}

	@Override
	public boolean isUsuarioDentroDoGrupo(Usuario user, Grupo grupo) {
		return rep.isUsuarioDentroDoGrupo(user, grupo);
	}

	@Override
	public Long insertUsuarioImportacao(String login, String senha, String dataNasc, Long permissaoId, Long grupoId) throws SQLException {
		return rep.insertUsuarioImportacao(login, senha, dataNasc, permissaoId, grupoId);
	}

	@Override
	public void deleteById(Long id) {
		rep.deleteById(id);
	}

	@Override
	public List<RelatorioFuncionaisVO> getFuncionais(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException {
		return rep.getFuncionais(filtro, count, empresa);
	}

	@Override
	public long getCountByFilter(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException {
		return rep.getCountByFilter(filtro, count, empresa);
	}

	@Override
	public List<RelatorioAcessosVO> findbyFilter(RelatorioFiltro filtro, boolean count, Empresa empresa) throws DomainException {
		return rep.findbyFilter(filtro, count, empresa);
	}

	@Override
	public List<RelatorioAcessosVersaoVO> findAcessosByVersao(RelatorioFiltro filtro) throws DomainException {
		return rep.findAcessosByVersao(filtro);
	}

	@Override
	public long getCountVersaoByFilter(RelatorioFiltro filtro) throws DomainException {
		return rep.getCountVersaoByFilter(filtro);
	}

	@Override
	public List<Object[]> findIdLoginsUsuario() {
		return rep.findIdLoginsUsuario();
	}

	@Override
	public List<Usuario> findByNomeLikeLogin(UsuarioAutocompleteFiltro filtro, Empresa empresa) {
		return rep.findByNomeLikeLogin(filtro, empresa);
	}

	@Override
	public String csvUsuarios(Empresa empresa) {
		// LOGIN;NOME;EMAIL;FONE_FIXO;FONE_CEL;DATA_NASC;CARGO;AREA;DIRETORIA;DIRETORIA_EXECUTIVA;PERMISSAO;GRUPOS
		List<Usuario> findAll = findAll(empresa);
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		String type = ParametrosMap.getInstance(empresa.getId()).get(Params.BUILDTYPE);
		header.add("LOGIN").add("NOME").add("EMAIL").add("DDD_FIXO").add("FONE_FIXO").add("DDD_CEL").add("FONE_CEL").add("SEXO").add("DATA_NASC").add("CARGO").add("PERMISSAO").add("GRUPO_ORIGEM").add("GRUPOS").add("PERIODO_TRAB").add("HORARIO_TRAB_INI").add("HORARIO_TRAB_FIM").add("CONECTOR");
 		
		if (StringUtils.equals(type, "cielo")) {
			header.add("AREA").add("DIRETORIA");
			// .add("DIRETORIA_EXECUTIVA");
		}
		for (Usuario u : findAll) {
			Perfil permissao = u.getPermissao();
			if (permissao == null) {
				Long permissaoId = ParametrosMap.getInstance(empresa.getId()).getLong(Params.IMPORTACAO_PERMISSAO, 3);
				permissao = permissaoService.get(permissaoId);
			}
			List<Grupo> grupos = grupoService.getGrupos(u);
			String stringGrupos = "";
			for (Grupo grupo : grupos) {
				if (StringUtils.isEmpty(stringGrupos)) {
					stringGrupos += grupo.getCodigoDesc();
				} else {
					stringGrupos += "," + grupo.getCodigoDesc();
				}
			}

			body.add(u.getLogin()).add(u.getNome() != null ? u.getNome() : "").add(u.getEmail() != null ? u.getEmail() : "").add(u.getDddTelefone() != null ? u.getDddTelefone() : "").add(u.getTelefoneFixo() != null ? u.getTelefoneFixo() : "").add(u.getDddCelular() != null ? u.getDddCelular() : "").add(u.getTelefoneCelular() != null ? u.getTelefoneCelular() : "")
					.add(u.getGenero()).add(u.getDataNascString() != null ? u.getDataNascString() : "").add(u.getFuncao() != null ? u.getFuncao().getCodigoDesc() : "").add(permissao.getNome() != null ? permissao.getNome() : "")
					.add(stringGrupos).add(u.getExpediente() != null ? u.getExpediente().getCodigo() : "").add(DateUtils.toString(u.getHoraAbertura(), "HH:mm")).add(DateUtils.toString(u.getHoraFechamento(), "HH:mm")).add(u.getFlags() != null ? u.getFlags() : "");
		 	if (StringUtils.equals(type, "cielo")) {
				body.add(u.getArea() != null ? u.getArea().getCodigoDesc() : "").add(u.getDiretoria() != null ? u.getDiretoria().getCodigoDesc() : "");
				// .add("DIRETORIA_EXECUTIVA");
			}
			body.end();
		}
		return generator.toString();
	}

	@Override
	public String csvUsuariosByFilter(ExportarUsuariosFiltro filtro, Empresa empresa) {
		List<Usuario> findAll;
		if (StringUtils.equals(filtro.getRadio(), "todos")) {
			findAll = findAll(empresa);
		} else {
			findAll = findAllByFilter(filtro, empresa);
		}
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		if (filtro.isLogin())
			header.add("LOGIN");
		if (filtro.isNome())
			header.add("NOME");
		if (filtro.isEmail())
			header.add("EMAIL");
		if (filtro.isFoneFixo())
			header.add("FONE_FIXO");
		if (filtro.isFoneCel())
			header.add("FONE_CEL");
		if (filtro.isDataNasc())
			header.add("DATA_NASC");
		if (filtro.isCargo())
			header.add("CARGO");
		if (filtro.isPermissao())
			header.add("PERMISSAO");
		if (filtro.isGrupos())
			header.add("GRUPOS");
		if (filtro.isArea())
			header.add("AREA");
		if (filtro.isDiretoria())
			header.add("DIRETORIA");
		for (Usuario u : findAll) {
			if (filtro.isLogin())
				body.add(u.getLogin());
			if (filtro.isNome())
				body.add(u.getNome() != null ? u.getNome() : "");
			if (filtro.isEmail())
				body.add(u.getEmail() != null ? u.getEmail() : "");
			if (filtro.isFoneFixo())
				body.add(u.getTelefoneFixo() != null ? u.getTelefoneFixo() : "");
			if (filtro.isFoneCel())
				body.add(u.getTelefoneCelular() != null ? u.getTelefoneCelular() : "");
			if (filtro.isDataNasc())
				body.add(u.getDataNascString() != null ? u.getDataNascString() : "");
			if (filtro.isCargo())
				body.add(u.getFuncao() != null ? u.getFuncao().getCodigoDesc() : "");
			if (filtro.isPermissao()) {
				Perfil permissao = u.getPermissao();
				if (permissao == null) {
					Long permissaoId = ParametrosMap.getInstance(empresa.getId()).getLong(Params.IMPORTACAO_PERMISSAO, 3);
					permissao = permissaoService.get(permissaoId);
				}
				body.add(permissao.getNome() != null ? permissao.getNome() : "");
			}
			if (filtro.isGrupos()) {
				List<Grupo> grupos = grupoService.findAllByUser(u);
				String stringGrupos = "";
				for (Grupo grupo : grupos) {
					if (StringUtils.isEmpty(stringGrupos)) {
						stringGrupos += grupo.getCodigoDesc();
					} else {
						stringGrupos += "," + grupo.getCodigoDesc();
					}
				}
				body.add(stringGrupos);
			}
			if (filtro.isArea())
				body.add(u.getArea() != null ? u.getArea().getCodigoDesc() : "");
			if (filtro.isDiretoria())
				body.add(u.getDiretoria() != null ? u.getDiretoria().getCodigoDesc() : "");
			body.end();
		}
		return generator.toString();

	}

	@Override
	public List<Usuario> findAllByFilter(ExportarUsuariosFiltro filtro, Empresa empresa) {
		List<Usuario> list = new ArrayList<Usuario>();
		if (filtro.getUsuariosString() != null) {
			for (String idString : filtro.getUsuariosString()) {
				Usuario usuario = get(Long.parseLong(idString));
				list.add(usuario);
			}
		}
		return list;
	}

	@Override
	public List<String> findLoginsByIds(List<Long> ids) {
		return rep.findLoginsByIds(ids);
	}

	@Override
	public String createPassword(Usuario u) throws DomainException {
		Long empresaId = 1L;
		if(u.getEmpresa() != null) {
			empresaId = u.getEmpresa().getId();
		}
		String senha = LivecomUtil.gerarSenha(empresaId);
		u.setSenhaCript(senha);
		rep.saveOrUpdate(u);
		return senha;
	}

	@Override
	public Long count(Empresa empresa) {
		return rep.cont(empresa);
	}

	@Override
	public long getCountByNomeLike(UsuarioAutocompleteFiltro filtro, Empresa empresa) {
		return rep.getCountByNomeLike(filtro, empresa);
	}

	@Override
	public boolean isUsuarioDentroHorario(Usuario u) {
		return rep.isUsuarioDentroHorario(u);
	}

	@Override
	public void revemoUsersToGrupo(Usuario userInfo, Grupo grupo, List<Usuario> users) throws DomainException {
		for (Usuario u : users) {
			
			//statusMensagemUsuarioService.delete(u, grupo);
			
			GrupoUsuarios gu = new GrupoUsuarios();
			gu.setGrupo(grupo);
			gu.setUsuario(u);
			grupoService.criarNotificacao(gu, TipoNotificacao.GRUPO_USUARIO_REMOVIDO);
			grupoService.removeUsuario(userInfo, grupo, u);
			String msg = "Usuario ["+u.getNome()+"] removido do grupo ["+grupo.getNome()+"] com sucesso";
			LogAuditoria.log(userInfo, grupo, AuditoriaEntidade.GRUPO, AuditoriaAcao.DELETAR, msg);
		}
	}

	@Override
	public void recuperarSenha(Empresa empresa, Usuario userInfo, Usuario u) throws DomainException {
		String senha = LivecomUtil.gerarSenha(empresa.getId());
		u.setSenhaCript(senha);
		u.setDataUpdatedSenha(new Date());
		saveOrUpdate(userInfo,u);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("senha", senha);
		
		
		UsuarioToPush usuarioToPush = new UsuarioToPush();
		usuarioToPush.setEmail(u.getEmail());
		usuarioToPush.setCodigo(u.getLogin());
		usuarioToPush.setNome(u.getNome());
		usuarioToPush.setIdLivecom(u.getId());
		usuarioToPush.setParams(params);
		
		PushNotification not = new PushNotification();
		not.setIsEmail(true);
		not.setUsuario(usuarioToPush);
		
		Sender sender = SenderFactory.getSender(applicationContext, SenderRecuperarSenha.class);
		sender.send(empresa, not);
	}

	@Override
	public void alterarSenha(Empresa empresa, Usuario userInfo, Usuario u, String senha) throws DomainException {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("senha", senha);
		
		UsuarioToPush usuarioToPush = new UsuarioToPush();
		usuarioToPush.setEmail(u.getEmail());
		usuarioToPush.setCodigo(u.getLogin());
		usuarioToPush.setNome(u.getNome());
		usuarioToPush.setIdLivecom(u.getId());
		usuarioToPush.setParams(params);
		
		PushNotification not = new PushNotification();
		not.setIsEmail(true);
		not.setUsuario(usuarioToPush);
		
		Sender sender = SenderFactory.getSender(applicationContext, SenderRecuperarSenha.class);
		sender.send(empresa, not);
	}

	@Override
	public void setNullFuncaoByFuncoes(List<Funcao> funcoes) {
		rep.setNullFuncaoByCargos(funcoes);
	}

	@Override
	public List<Usuario> findByFuncoes(List<Funcao> funcoes) {
		return rep.findByCargos(funcoes);
	}

	@Override
	public List<Long> findAllUsuarioIdsDentroHorario(List<Long> idsToFilter) {
		String modoRestricao = ParametrosMap.getInstance().get(Params.RESTRICAO_HORARIO_MODO,"");
		
		if(modoRestricao.equals("grupo")) {
			//Restrição grupos
			return grupoService.gruposUsuariosDentroHorario(idsToFilter);
		} else {
			if("1".equals(ParametrosMap.getInstance().get(Params.USUARIO_RESTRICAO_HORARIO_ON,"0"))) {
				return rep.usuariosDentroHorarioExpediente(idsToFilter);
			} else {
				return idsToFilter;
			}
		}
	}

	@Override
	public boolean existsByLogin(Usuario u, Empresa empresa) {
		return rep.existsByLogin(u, empresa);
	}

	@Override
	public boolean existsByEmail(Usuario u, Empresa empresa) {
		return rep.existsByEmail(u, empresa);
	}

	@Override
	public List<Long> findAllIds(Empresa empresa) {
		return rep.findAllIds(empresa);
	}

	@Override
	public Long getCountFindByStatus(Grupo g, StatusUsuario statusAtivacao, Empresa empresa) {
		return rep.getCountFindByStatus(g, statusAtivacao, empresa);
	}

	@Override
	public void convidarUsuarios(Empresa empresa, Usuario userInfo, String contextPath, List<Long> usersId, TemplateEmail template, String assunto) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("template", template);
		params.put("empresa", empresa);
		params.put("usuarios_ids", usersId);
		params.put("contextPath", contextPath);
		params.put("userInfo", userInfo);
		params.put("assunto", assunto);
		JobUtil.executeJob(applicationContext, ConvidarUsuariosEmailJob.class, params);
	}

	@Override
	public UsuarioStatusCountVO getCountUsuariosStatus(Empresa empresa) {
		return rep.getCountUsuariosStatus(empresa);
	}

	@Override
	public List<Usuario> findNovos(Empresa empresa) {
		return rep.findNovos(empresa);
	}

	@Override
	public List<Usuario> findInativos(Empresa empresa) {
		return rep.findInativos(empresa);
	}

	@Override
	public List<Usuario> findAtivos(Empresa empresa) {
		return rep.findAtivos(empresa);
	}

	@Override
	public void addGrupo(Usuario userInfo, Usuario u,Grupo g) throws DomainException {
		Set<GrupoUsuarios> grupos = u.getGrupos();
		if(grupos == null) {
			grupos = new HashSet<GrupoUsuarios>();
			u.setGrupos(grupos);
		}

		// Adiciona usuario no grupo.
		GrupoUsuarios gsu = new GrupoUsuarios();
		gsu.setUsuario(u);
		gsu.setGrupo(g);
		grupos.add(gsu);

		// save
		saveOrUpdate(userInfo, u);
	}
	
	@Override
	public Amizade solicitar(Usuario usuario, Usuario amigo, boolean solicitacao) throws DomainException {

		try {
			Amizade amizade = getAmizade(usuario, amigo);
			if(amizade != null && !solicitacao) {
				return desfazerAmizade(usuario, amigo, amizade);
			}

			// participar
			if(solicitacao) {
				amizade = new Amizade();
				amizade.setAmigo(amigo);
				amizade.setUsuario(usuario);
				amizade.setStatusAmizade(StatusAmizade.SOLICITADA);
				
				criarNotificacao(amizade, TipoNotificacao.USUARIO_SOLICITAR_AMIZADE);
				
				saveOrUpdate(amizade);
			}
			
			return amizade;
		} catch (Exception e) {
			throw new DomainException("Erro ao solicitar/desfazer amizade: ", e);
		}
	}
	
	private Amizade desfazerAmizade(Usuario usuario, Usuario amigo, Amizade amizade) throws DomainException {
		notificationService.markSolicitacaoAmizadeAsRead(usuario, amigo);
		deleteAmizade(usuario, amigo);
		criarNotificacao(usuario, amigo, TipoNotificacao.USUARIO_DESFAZER_AMIZADE);
		
		return null;
	}
	
	@Override
	public void criarNotificacao(Usuario usuario, Usuario amigo, TipoNotificacao tipo) throws DomainException {
		Amizade amizade = new Amizade();
		amizade.setUsuario(usuario);
		amizade.setAmigo(amigo);
		criarNotificacao(amizade, tipo);;
	}
	
	@Override
	public void criarNotificacao(Amizade amizade, TipoNotificacao tipo) throws DomainException {
		Usuario amigo = amizade.getAmigo();
		Usuario user = amizade.getUsuario();
		Notification n = new Notification();
		n.setData(new Date());
		n.setSendPush(false);
		n.setPublicado(true);
		n.setVisivel(true);
		
		TipoNotificacao tipoNotificacao = null;
		String titulo = null;
		String texto = null;
		String textoNotificacao = null;
		Usuario to = null;
		Usuario from = null;
		
		switch (tipo) {
		case USUARIO_SOLICITAR_AMIZADE: {
			tipoNotificacao = TipoNotificacao.USUARIO_SOLICITAR_AMIZADE;
			titulo = "Solicitação de conexão";
			texto = user.getNome() + " quer se conectar com você.";
			textoNotificacao = user.getNome() + " quer se conectar com você.";
			from = user;
			to = amigo;
			break;
		}
		case USUARIO_DESFAZER_AMIZADE: {
			tipoNotificacao = TipoNotificacao.USUARIO_DESFAZER_AMIZADE;
			titulo = "Conexão desfeita";
			texto = user.getNome() + " não é mais uma conexão.";
			textoNotificacao = user.getNome() + " não é mais uma conexão.";
			from = user;
			to = amigo;
			break;
		}
		case USUARIO_RECUSAR_AMIZADE: {
			tipoNotificacao = TipoNotificacao.USUARIO_RECUSAR_AMIZADE;
			titulo = "Conexão recusada";
			texto = amigo.getNome() + " não aceitou ser sua conexão.";
			textoNotificacao = amigo.getNome() + " não aceitou ser sua conexão.";
			from = amigo;
			to = user;
			break;
		}
		case USUARIO_ACEITAR_AMIZADE: {
			tipoNotificacao = TipoNotificacao.USUARIO_ACEITAR_AMIZADE;
			titulo = "Conexão aceita";
			texto = amigo.getNome() + " agora é uma conexão.";
			textoNotificacao = amigo.getNome() + " agora é uma conexão.";
			from = amigo;
			to = user;
			break;
		}
		default:
			break;
		}
		n.setTipo(tipoNotificacao);
		n.setTitulo(titulo);
		n.setTexto(texto);
		n.setTextoNotificacao(textoNotificacao);
		n.setUsuario(from);
		n.setGrupoNotification(GrupoNotification.AMIZADE);
		notificationService.save(n);
		
		// Salva a notification. O Job vai enviar depois
		NotificationUsuario notUsuario = new NotificationUsuario();
		notUsuario.setNotification(n);
		notUsuario.setUsuario(to);
		notUsuario.setVisivel(true);
		notificationService.saveOrUpdate(notUsuario);
		
		PushNotificationVO not = PushNotificationVO.createNotificacaoAmizade(n, to);
		WebSocketChat.sendMsg(to.getId(), not);
	}
	
	@Override
	public List<UsuarioVO> findUsuariosByStatusAmizade(StatusAmizade status, Usuario usuario) {
		List<Amizade> amizades = rep.findUsuariosByStatusAmizade(status, usuario);
		
		List<UsuarioVO> users = new ArrayList<UsuarioVO>();
		for (Amizade a : amizades) {
			UsuarioVO vo = new UsuarioVO();
			vo.setUsuario(a.getAmigo());
			users.add(vo);
		}
		
		return users;
	}
	
	@Override
	public List<UsuarioVO> findByStatusAmizade(Usuario usuario) {
		List<Amizade> amizades = rep.findUsuariosByStatusAmizade(StatusAmizade.SOLICITADA, usuario);
		List<Amizade> list = rep.findUsuariosByStatusAmizade(StatusAmizade.APROVADA, usuario);
		amizades.addAll(list);
		
		List<UsuarioVO> users = new ArrayList<UsuarioVO>();
		
		for (Amizade a : amizades) {
			UsuarioVO vo = new UsuarioVO();
			String status = "";
			StatusAmizade statusAmizade = a.getStatusAmizade();
			
			if(StatusAmizade.APROVADA.equals(statusAmizade)) {
				status = "AMIGOS";
				vo.amigo = status;
				
				if(usuario.getId().equals(a.getAmigo().getId())){
					vo.setUsuario(a.getUsuario());
				} else{
					vo.setUsuario(a.getAmigo());
				}
				
				users.add(vo);
				continue;	
			}
			
			if(usuario.getId().equals(a.getAmigo().getId())){
				vo.setUsuario(a.getUsuario());
				status = "AGUARDANDO_APROVACAO";
			} else{
				status = "SOLICITADO";
				vo.setUsuario(a.getAmigo());
			}
			vo.amigo = status;
			users.add(vo);
		}
		
		return users;
	}
	
	@Override
	public List<Usuario> findAmigos(Usuario usuario) {
		List<Amizade> amizades = rep.findUsuariosByStatusAmizade(StatusAmizade.APROVADA, usuario);
		
		List<Usuario> users = new ArrayList<Usuario>();
		
		//Reconhece como amigo independente de quem fez a solicitação de amizade
		for (Amizade a : amizades) {
			if(usuario.getId().equals(a.getAmigo().getId())){
				users.add(a.getUsuario());
			} else{
				users.add(a.getAmigo());
			}
		}
		
		return users;
	}

	@Override
	public List<Long> findIdsAmigos(Usuario usuario) {
		List<Amizade> amizades = rep.findUsuariosByStatusAmizade(StatusAmizade.APROVADA, usuario);
		
		List<Long> users = new ArrayList<Long>();
		
		//Reconhece como amigo independente de quem fez a solicitação de amizade
		for (Amizade a : amizades) {
			if(usuario.getId().equals(a.getAmigo().getId())){
				users.add(a.getUsuario().getId());
			} else{
				users.add(a.getAmigo().getId());
			}
		}
		
		return users;
	}

	@Override
	public List<UsuarioVO> findByAmigos(Usuario usuario, String nome) {
		List<Amizade> amizades = rep.findUsuariosByStatusAmizade(StatusAmizade.APROVADA, usuario, nome);
		
		List<UsuarioVO> users = new ArrayList<UsuarioVO>();
		
		//Reconhece como amigo independente de quem fez a solicitação de amizade
		for (Amizade a : amizades) {
			UsuarioVO vo = new UsuarioVO();
			if(usuario.getId().equals(a.getAmigo().getId())){
				vo.setUsuario(a.getUsuario());
			} else{
				vo.setUsuario(a.getAmigo());
			}
			users.add(vo);
		}
		
		return users;
	}
	
	@Override
	public List<UsuarioVO> findByPendentes(Usuario usuario) {
		List<Amizade> amizades = rep.findUsuariosByStatusAmizade(StatusAmizade.SOLICITADA, usuario);
		
		List<UsuarioVO> users = new ArrayList<UsuarioVO>();
		
		//Set usuarios apenas que me pediram solicitação de amizade
		for (Amizade a : amizades) {
			UsuarioVO vo = new UsuarioVO();
			if(usuario.getId().equals(a.getAmigo().getId())){
				vo.setUsuario(a.getUsuario());
				users.add(vo);
			}
		}
		
		return users;
	}

	@Override
	public List<Long> findUsuariosToPush(Post p, List<Long> amigos, Idioma idioma) {
		return rep.findUsuariosToPush(p, amigos, idioma);
	}

	@Override
	public void saveOrUpdate(Usuario usuario) {
		rep.saveOrUpdate(usuario);
		rep.clear();
	}

	@Override
	public ControleVersaoVO validateVersion(Long empresaId, String appVersion, Integer appVersionCode, String userAgentSO) {
		log.debug("Validando Versao empresa: " + empresaId);
		log.debug("User-Agent: " + userAgentSO);

		ParametrosMap parametrosMap = ParametrosMap.getInstance(empresaId);
		
		String isAtivado = parametrosMap.get(Params.VERSION_CONTROL_ON, "0");
		if(!"1".equals(isAtivado)) {
			return null;
		}
		
		// Android
		if("android".equals(userAgentSO)) {
			String link = parametrosMap.get(Params.VERSION_CONTROL_ANDROID_URLDOWNLOAD, "https://play.google.com/apps/testing/br.livetouch.livecom.livetouch");
			String mensagem = parametrosMap.get(Params.VERSION_CONTROL_ANDROID_MENSAGEM, "Existe uma nova versão do aplicativo, baixar?");
			boolean isInformativo = "informativo".equals(parametrosMap.get(Params.VERSION_CONTROL_ANDROID_MODE));
			boolean isImpeditivo = "impeditivo".equals(parametrosMap.get(Params.VERSION_CONTROL_ANDROID_MODE));
			String paramsNumberVersion = parametrosMap.get(Params.VERSION_CONTROL_ANDROID_NUMBER, "");
			int paramsNumberCode =  NumberUtils.toInt(parametrosMap.get(Params.VERSION_CONTROL_ANDROID_NUMBER_CODE, "0"),0);

			log.debug(String.format("Validando Android: %s %s %s %s %s %s" ,isInformativo, isImpeditivo, paramsNumberVersion, paramsNumberCode , appVersion, appVersionCode));

			return validateVersion(appVersion, appVersionCode, link, mensagem, isInformativo, isImpeditivo, paramsNumberVersion, paramsNumberCode);
		} else if("ios".equals(userAgentSO)) {
			String link = parametrosMap.get(Params.VERSION_CONTROL_IOS_URLDOWNLOAD, "http://livecom.livetouchdev.com.br/download");
			String mensagem = parametrosMap.get(Params.VERSION_CONTROL_IOS_MENSAGEM, "Existe uma nova versão do aplicativo, baixar?");
			boolean isInformativo = "informativo".equals(parametrosMap.get(Params.VERSION_CONTROL_IOS_MODE));
			boolean isImpeditivo = "impeditivo".equals(parametrosMap.get(Params.VERSION_CONTROL_IOS_MODE));
			String paramsNumberVersion = parametrosMap.get(Params.VERSION_CONTROL_IOS_NUMBER, "");
			int paramsNumberCode =  NumberUtils.toInt(parametrosMap.get(Params.VERSION_CONTROL_IOS_NUMBER_CODE, "0"),0);;

			log.debug(String.format("Validando iOS: %s %s %s %s %s" ,isInformativo, isImpeditivo, paramsNumberVersion, paramsNumberCode , appVersion));

			return validateVersion(appVersion, appVersionCode, link, mensagem, isInformativo, isImpeditivo, paramsNumberVersion, paramsNumberCode);
			
		}
		
		return null;
	}
	
	private ControleVersaoVO validateVersion(String appVersion, Integer appVersionCode, String link, String mensagem, boolean isInformativo, boolean isImpeditivo, String paramsNumberVersion,
			int paramsNumberCode) {
		ControleVersaoVO controleVersao = null;
		boolean hasCorrectVersion = true;
		
		/**
		 * Valida pelo version name: ex: 0.0.1
		 */
		if (StringUtils.isNotEmpty(paramsNumberVersion)) {
			paramsNumberVersion = paramsNumberVersion.replaceAll("\\.", "");

			if (!StringUtils.isEmpty(appVersion)) {
				appVersion = appVersion.replaceAll("\\.", "");

				if (!appVersion.equals(paramsNumberVersion)) {
					log.debug("Versao incorreta: " + appVersion + " Desejada : " + paramsNumberVersion);
					hasCorrectVersion = false;
				}
			}
		}

		/**
		 * Valida pelo version Code, ex: 1
		 */
		if (paramsNumberCode > 0 && appVersionCode != null) {
			if (appVersionCode == 0 || appVersionCode != paramsNumberCode) {
				hasCorrectVersion = false;
				log.debug("Versao Code incorreta: " + appVersionCode + " Desejada : " + paramsNumberCode);
			}
		}
		
		if (!hasCorrectVersion) {

			if (isImpeditivo) {
				controleVersao = new ControleVersaoVO(mensagem, "impeditivo");
				controleVersao.link = link;
			} else if (isInformativo) {
				controleVersao = new ControleVersaoVO(mensagem, "informativo");
				controleVersao.link = link;
			}
		}
		
		return controleVersao;
	}

	@Override
	public Date updateDataLastChat(Long userId) {
		return rep.updateDataLastChat(userId);
	}

	@Override
	public void updateAllDataSenha(Long empresaId) {
		rep.updateAllDataSenha(empresaId);
	}

}