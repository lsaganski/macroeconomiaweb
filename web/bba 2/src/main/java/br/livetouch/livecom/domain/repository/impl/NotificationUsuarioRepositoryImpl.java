package br.livetouch.livecom.domain.repository.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.enums.StatusPublicacao;
import br.livetouch.livecom.domain.enums.TipoNotificacao;
import br.livetouch.livecom.domain.repository.NotificationUsuarioRepository;

@Repository
public class NotificationUsuarioRepositoryImpl extends LivecomRepository<NotificationUsuario> implements NotificationUsuarioRepository {

	public NotificationUsuarioRepositoryImpl() {
		super(NotificationUsuario.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Notification> findAllToSendPush() {
		StringBuffer sb = new StringBuffer("from Notification n");
		sb.append(" where n.sendPush=1 and ((n.tipo = :newPost and n.post.statusPublicacao = :publicado) or n.tipo in(:like, :favorito, :comentario, :reminder))");
		
		Query q = createQuery(sb.toString());
		q.setParameter("newPost", TipoNotificacao.NEW_POST);
		q.setParameter("like", TipoNotificacao.LIKE);
		q.setParameter("favorito", TipoNotificacao.FAVORITO);
		q.setParameter("comentario", TipoNotificacao.COMENTARIO);
		q.setParameter("reminder", TipoNotificacao.REMINDER);
		q.setParameter("publicado", StatusPublicacao.PUBLICADO);
		
		// cache para job de push
		q.setCacheable(true);
		List<Notification> list = q.list();
		return list;
	}

	@Override
	public int updateSendPushNotification(Long id, List<Long> userIds) {
		StringBuffer sb = new StringBuffer("UPDATE NotificationUsuario n set n.visivel = 1, n.dataPush = :now where n.notification.id = :notId and n.usuario.id in(:users)");
		Query q = createQuery(sb.toString());
		q.setParameter("now", new Date());
		q.setParameter("notId", id);
		q.setParameterList("users", userIds);
		return q.executeUpdate();
	}

	@Override
	public void markAllAsRead(Long id) {
		StringBuffer sb = new StringBuffer("UPDATE NotificationUsuario n set n.lida = 1, n.dataLida = :now where n.notification.id = :id");
		Query q = createQuery(sb.toString());
		q.setParameter("id", id);
		q.setParameter("now", new Date());
		q.executeUpdate();
	}
}