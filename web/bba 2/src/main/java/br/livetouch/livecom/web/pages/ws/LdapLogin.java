package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.ldap.LDAPUser;
import br.livetouch.livecom.ldap.LDAPUtil;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class LdapLogin extends WebServiceXmlJsonPage {
	public Form form = new Form();
	private TextField tLoginAdmin;
	private TextField tPasswdAdmin;
	private TextField tMode;

	@Override
	public void onInit() {
		super.onInit();
		form.setMethod("post");
		tLoginAdmin = new TextField("login", true);
		form.add(tLoginAdmin);
		
		tPasswdAdmin = new TextField("senha", true);
		form.add(tPasswdAdmin);
		
		tMode = new TextField("mode");
		tMode.setValue("json");
		form.add(tMode);
		form.add(new Submit("postar"));
	}

	@Override
	protected Object execute()  {
		if (form.isValid()) {
			
			String loginAdmin = tLoginAdmin.getValue();
			String passwdAdmin = tPasswdAdmin.getValue();
			try {
				ParametrosMap params = ParametrosMap.getInstance(getEmpresa());
				LDAPUtil ldap = new LDAPUtil(params);
				
				LDAPUser login = ldap.findUser(loginAdmin);
				String userDN = login.getUserDN();
				login = ldap.loginByDN(userDN,loginAdmin, passwdAdmin);
				
				if(login != null) {
					return new MensagemResult("OK", "Usuário encontrado na base de dados");
				}
				return new MensagemResult("NOK", "Não foi possivel connectar no LDAP");
			} catch (Exception e) {
				return new MensagemResult("NOK", e.getMessage());
			}
		}
		return new MensagemResult("NOK", "Formulario invalido");
	}

	@Override
	protected void xstream(XStream x) {
		super.xstream(x);
	}
}
