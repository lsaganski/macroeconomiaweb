package br.livetouch.livecom.web.pages.report;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PushReport;
import br.livetouch.livecom.domain.vo.PushReportFiltro;

@Controller
@Scope("prototype")
public class DetalhesPushReportPage extends RelatorioPage {
	
	public PushReportFiltro filtro;
	public int page;
	public Long id;
	
	public Post post;
	
	@Override
	public void onInit() {
		super.onInit();

		if(clear) {
			getContext().removeSessionAttribute(PushReportFiltro.SESSION_FILTRO_KEY);
		}
		
		if (id != null) {
			PushReport push = pushReportService.get(id);
			if(push.getPost() != null) {
				post = push.getPost();
			}
		}

	}
	

	@Override
	public void onGet() {
		super.onGet();
	}
	
	@Override
	public String getContentType() {
		return super.getContentType();
	}
	
	@Override
	public String getTemplate() {
		return super.getTemplate();
	}
	
}
