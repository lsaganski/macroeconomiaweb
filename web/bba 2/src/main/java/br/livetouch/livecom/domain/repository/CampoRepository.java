package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Campo;
import br.livetouch.livecom.domain.Empresa;

@Repository
public interface CampoRepository extends net.livetouch.tiger.ddd.repository.Repository<Campo> {

	Campo findByNameValid(Campo campo, Empresa empresa);

	List<Campo> findAll(Empresa empresa);

	
}