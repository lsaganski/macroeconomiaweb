package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.UsuarioVO;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class GetUserPage extends WebServiceFormPage {
	
	private UsuarioLoginField tUser;
	public Long grupo_id;

	@Override
	protected void form() {
		form.add(tUser = new UsuarioLoginField("user", true,usuarioService, getEmpresa()));
	}

	@Override
	protected Object go() throws Exception {
		if (form.isValid()) {
			Usuario user = tUser.getEntity();

			if(user == null) {
				return new MensagemResult("NOK" ,"User not found");
			}
			
			UsuarioVO vo = new UsuarioVO();
			vo.setUsuario(user);
			
			return vo;
		}
		
		return new MensagemResult("NOK" ,"User not found");
	}
	
	@Override
	protected void xstream(XStream x) {
		x.alias("usuarios", UsuarioVO.class);
		super.xstream(x);
	}
}
