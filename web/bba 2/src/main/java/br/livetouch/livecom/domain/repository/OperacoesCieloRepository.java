package br.livetouch.livecom.domain.repository;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.OperacoesCielo;
import br.livetouch.livecom.domain.vo.OperacoesCieloVO;

@Repository
public interface OperacoesCieloRepository extends net.livetouch.tiger.ddd.repository.Repository<OperacoesCielo> {

	List<Object[]> findIdDataTransacao(Long idEmpresa);
	
	List<OperacoesCielo> transacoesByEmpresa(Empresa empresa, int page, Integer max, boolean count);
	
	long getCountTransacoesByEmpresa(Empresa empresa, int page, Integer max, boolean count);
	
	List<OperacoesCielo> findAll(Empresa empresa);

	List<OperacoesCieloVO> graficoPicoTps();

	List<OperacoesCieloVO> graficoMomento();

	List<OperacoesCieloVO> graficoTransacao();

	OperacoesCielo getByData(Date data);

	void deleteAll(Long empresaId);

}