package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class UserToPushVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Boolean push;
	private Boolean notification;
	
	private Long id;
	private String login;
	private String email;
	
	public UserToPushVO(Long id,String login,String email, Boolean push, Boolean notification) {
		super();
		this.id = id;
		this.login = login;
		this.email = email;
		this.setPush(push != null ? push : true);
		this.setNotification(notification != null ? notification : true);
	}
	
	public UserToPushVO() {	}
	
	public static List<Long> getListIds(List<UserToPushVO> users) {
		List<Long> list = new ArrayList<Long>();
		for (UserToPushVO u : users) {
			list.add(u.getId());
		}
		return list;
	}

	public static List<UserToPushVO> toListUsers(List<Long> ids) {
		List<UserToPushVO> list = new ArrayList<UserToPushVO>();
		for (Long id : ids) {
			UserToPushVO vo = new UserToPushVO();
			vo.setId(id);
			vo.setPush(true);
			vo.setNotification(true);
			list.add(vo);
		}
		return list;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getPush() {
		return push;
	}

	public void setPush(Boolean push) {
		this.push = push;
	}

	public Boolean getNotification() {
		return notification;
	}

	public void setNotification(Boolean notification) {
		this.notification = notification;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String toString() {
		return "UserToPushVO [id=" + id + ", push=" + push + ", usuario=" + login
				+ "]";
	}

	/**
	 * Converte SQL retornado pelo hibernate no formato Object[]
	 * 
	 * [0] - user id
	 * [1] - user login
	 * [2] - user email
	 * [3] - boolean send push
	 * [4] - boolean create notification
	 * 
	 * @param array
	 * @return
	 */
	public static List<UserToPushVO> hibernate(List<Object[]> array) {

		List<UserToPushVO> list = new ArrayList<UserToPushVO>();
		
		for (Object[] item : array) {
			UserToPushVO u = new UserToPushVO();
			if(item[0] instanceof java.math.BigInteger) {
				java.math.BigInteger bigInteger = (BigInteger) item[0];
				u.setId(bigInteger.longValue());
			} else {
				u.setId((Long) item[0]);
			}
			u.setLogin((String) item[1]);
			u.setEmail((String) item[2]);
			
			if(item[3] instanceof java.math.BigDecimal) {
				java.math.BigDecimal sendPush = (BigDecimal) item[3];
				int intValue = sendPush.intValue();
				if(sendPush != null && intValue == 1) {
					u.setPush(true);
				} else {
					u.setPush(false);
				}
			} else {
				u.setPush((Boolean) (item[3] != null ? item[3] : true));
			}
			
			if(item[4] instanceof java.math.BigDecimal) {
				java.math.BigDecimal sendNotification = (BigDecimal) item[4];
				int intValue = sendNotification.intValue();
				if(sendNotification != null && intValue == 1) {
					u.setNotification(true);
				} else {
					u.setNotification(false);
				}
			} else {
				u.setNotification((Boolean) (item[4] != null ? item[4] : true));
			}
			
			list.add(u);
		}
		return list;
	}
	
	

}