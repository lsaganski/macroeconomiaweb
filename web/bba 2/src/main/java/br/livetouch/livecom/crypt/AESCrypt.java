package br.livetouch.livecom.crypt;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/**
 * Aes encryption
 * 
 * https://stackoverflow.com/questions/15554296/simple-java-aes-encrypt-decrypt-example
 */
public class AESCrypt {


	/**
	 * @param key 128 bit key
	 * @param value
	 * @return
	 */
	public static String encrypt(String key, String value) {
        try {
        	// 16 bytes IV
        	String initVector = key;

            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());

            String base64 = DatatypeConverter.printBase64Binary(encrypted);
			return base64;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static String decrypt(String key, String encryptedBase64) {
        try {
        	String initVector = key;

            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] orignal = DatatypeConverter.parseBase64Binary(encryptedBase64);
			byte[] decrypted = cipher.doFinal(orignal);

            return new String(decrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static void main(String[] args) {
    	//CBC
        String key = "1234567890123456"; // 128 bit key
        
        String cript = encrypt(key, "Ricardo Lecheta");
        System.out.println(cript);
		String decript = decrypt(key,
                cript);
		System.out.println(decript);
    }


}