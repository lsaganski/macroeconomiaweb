package br.livetouch.livecom.web.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.CategTree;
import br.infra.web.click.ComboCategoriaPostChildren;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;

@Controller
@Scope("prototype")
public class TestePage extends LivecomLogadoPage {

	public Form form = new Form();
	public String msg;

	public CategoriaPost categ;
	public Long categId;
	private ComboCategoriaPostChildren tComboCateg;
	public CategTree tree;

	@Override
	public String getTemplate() {
		return getPath();
	}
	
	@Override
	public boolean onSecurityCheck() {
		return super.onSecurityCheck();
	}

	@Override
	public void onInit() {
		super.onInit();

		form();
		
		tree = new CategTree("tree", categoriaPostService, getEmpresa());
		tree.build(getContext());
	}
	
	public void form(){
		form.add(tComboCateg = new ComboCategoriaPostChildren("categ",categoriaPostService, getEmpresa()));

		Submit tDeletar = new Submit("go", this, "go");
		tDeletar.setAttribute("class", "botao btSalvar");
		form.add(tDeletar);

		setFormTextWidth(form);
	}
	
	@Override
	public void onGet() {
		super.onGet();
		
		if(categId != null) {
			categ = categoriaPostService.get(categId);
		}
	}

	public boolean go() {
		if (form.isValid()) {
			try {
				categ = tComboCateg.getCategoriaPost();

				return false;

			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();
		
		
	}
}
