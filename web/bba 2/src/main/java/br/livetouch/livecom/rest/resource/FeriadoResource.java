package br.livetouch.livecom.rest.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Feriado;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.vo.FeriadoVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/v1/feriado")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class FeriadoResource extends MainResource {
	protected static final Logger log = Log.getLogger(FeriadoResource.class);
	
	@POST
	public Response create(Feriado feriado) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Feriado.isFormValid(feriado, false);
		} catch (DomainException e) {
			return Response.ok(MessageResult.error(e.getMessage())).build();
		}
		
		try {
			feriadoService.saveOrUpdate(feriado, getEmpresa());
			return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", FeriadoVO.from(feriado))).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel realizar o cadastro do feriado " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel cadastrar o feriado")).build();
		}
	}
	
	@PUT
	public Response update(Feriado feriado) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			Feriado.isFormValid(feriado, true);
		} catch (DomainException e) {
			return Response.ok(MessageResult.error(e.getMessage())).build();
		}
		
		try {
			feriadoService.saveOrUpdate(feriado, getEmpresa());
			return Response.ok(MessageResult.ok("Feriado atualizo com sucesso com sucesso", FeriadoVO.from(feriado))).build();
		} catch (DomainException e) {
			log.debug("Não foi possivel atualizar o cadastro da feriado " + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possivel cadastrar o feriado")).build();
		}
	}
	
	@GET
	public Response findAll() {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		List<Feriado> feriados = feriadoService.findAll(getEmpresa());
		List<FeriadoVO> vos = FeriadoVO.fromList(feriados);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Feriado feriado = feriadoService.get(id);
		if(feriado == null) {
			return Response.ok(MessageResult.error("Feriado não encontrado")).build();
		}
		FeriadoVO vo = new FeriadoVO(feriado);
		return Response.ok().entity(vo).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		Feriado feriado = feriadoService.get(id);
		if(feriado == null) {
			return Response.ok(MessageResult.error("Feriado não encontrado")).build();
		}
		try {
			feriadoService.delete(feriado);
			return Response.ok().entity(MessageResult.ok("Feriado deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error("Não foi possível excluir o feriado id: " + id + " ERRO:" + e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Feriado  " + id)).build();
		}
	}
	
//	@GET
//	@Path("/filtro")
//	public Response findByName(@BeanParam FiltroFeriado filtro) {
//		List<Feriado> feriados = feriadoService.filterFeriado(filtro, getEmpresa());
//		List<FeriadoVO> vo = FeriadoVO.fromList(feriados);
//		return Response.status(Status.OK).entity(vo).build();
//	}
}
