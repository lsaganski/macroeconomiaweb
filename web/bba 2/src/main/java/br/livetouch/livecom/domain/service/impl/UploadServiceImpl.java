package br.livetouch.livecom.domain.service.impl;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.imgscalr.Scalr.Rotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.TagService;
import br.livetouch.livecom.domain.service.UploadService;
import br.livetouch.livecom.domain.vo.UploadRequest;
import br.livetouch.livecom.domain.vo.UploadResponse;
import br.livetouch.livecom.utils.FileExtensionUtils;
import br.livetouch.livecom.utils.UploadHelper;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class UploadServiceImpl implements UploadService {
	@Autowired
	private ArquivoService arquivoService;

	@Autowired
	private TagService tagService;
	
	@Override
	public UploadResponse upload(UploadRequest req) throws DomainException {
		try {
			Long id = req.id;
			List<Long> thumbIds = req.thumbIds;
			Arquivo a = null;
			UploadResponse info = null;
			FileItem item = req.fileItem;
			Usuario usuario = req.usuario;
			String dir = req.dir;
			boolean isBase64 = StringUtils.isNotEmpty(req.fileBase64);
			boolean isFileUrl = StringUtils.isNotEmpty(req.fileUrl);

			Empresa empresa = req.empresa;
			if (id != null) {
				// update
				a = arquivoService.get(new Long(id));
				info = new UploadResponse();
				info.arquivo = a;
			} else {
				// new file

				if (StringUtils.isEmpty(dir)) {
					throw new DomainMessageException("Informe o diretório remoto para upload.");
				}

                boolean isLambda = ParametrosMap.getInstance(empresa).getBoolean(Params.AMAZON_LAMBDA_CHAT_RESIZE_ON, false);
                long timestamp = new Date().getTime();

                if(isLambda) {
                    dir = empresa.getCodigo();
                }


				if(req.width != null && req.height != null && req.eixoX != null && req.eixoY != null) {
				    byte[] bytes = null;
				    String nome = null;
                    if(isBase64) {
                        nome = req.fileName;
                        bytes = Base64.decodeBase64(req.fileBase64);
                    } else if(isFileUrl) {
                        File f = UploadHelper.downloadFileToTempDir(req.fileUrl);
                        if(f == null || !f.exists()) {
                            throw new DomainMessageException("Erro ao salvar a URL em arquivo.");
                        }
                        bytes = IOUtils.toByteArray(new FileInputStream(f));
                        nome = f.getName();
                    } else if(item != null) {
                        bytes = item.get();
                        nome = item.getName();
                    }

                    if(bytes != null) {
                    	
                    	File file = null;
                    	int x = req.eixoX, y = req.eixoY, w = req.width, h = req.height;
                    	
                    	if(req.resize) {
                    		file = resize(bytes, nome, w, h);
                    		bytes = IOUtils.toByteArray(new FileInputStream(file));
                    	} else {
                    		file = cropImage(nome, bytes, w, h, x, y);
                    		bytes = IOUtils.toByteArray(new FileInputStream(file));
                    	}
                    	
                        long fileSize = file.length();
                        
                        if(thumbIds != null && !thumbIds.isEmpty()) {
                        	
                        	for (Long thumbId : thumbIds) {
                        		ArquivoThumb thumb = arquivoService.find(thumbId);
                        		
                        		String name = FilenameUtils.getName(thumb.getUrl());
                        		File thumbFile = UploadHelper.resizeThumbDown(file, thumb, name);
                        		bytes = IOUtils.toByteArray(new FileInputStream(thumbFile));
                                fileSize = thumbFile.length();
                                
                        		info = UploadHelper.uploadFileThumb(usuario, thumb, name, bytes, fileSize, dir, isLambda, timestamp);
                        		
                        		BufferedImage bimg = ImageIO.read(thumbFile);
                        		int width = bimg.getWidth();
                        		int height = bimg.getHeight();
                        		
                        		ArquivoThumb t = info.thumb;
                        		t.setHeight(height);                        		
                        		t.setWidth(width);                        		
								
                        		arquivoService.saveOrUpdate(t);
							}
                        	
                        } else {
                        	info = UploadHelper.uploadFile(usuario, nome, bytes, fileSize, dir, isLambda, timestamp);
                        }
                    }
                } else {
                    if (isBase64) {
                        String url = req.fileUrl;
                        log("Upload URL: ["+url +"] ");

                        // URL
                        info = UploadHelper.uploadBase64(usuario, req.fileName,req.fileBase64, dir, isLambda, timestamp);

                    } else if (isFileUrl) {
                        String url = req.fileUrl;
                        log("Upload URL: ["+url +"] ");

                        //URL
                        if(isRotate(req.angle)) {
                        	File f = UploadHelper.downloadFileToTempDir(req.fileUrl);
                            if(f == null || !f.exists()) {
                                throw new DomainMessageException("Erro ao salvar a URL em arquivo.");
                            }
                            
                            byte[] bytes = IOUtils.toByteArray(new FileInputStream(f));
                            String nome = f.getName();
                            long fileSize = f.length();

                            File file = rotate(bytes, nome, req.angle);
                            bytes  = IOUtils.toByteArray(new FileInputStream(file));
                            fileSize = file.length();
                            
                            info = UploadHelper.uploadFile(usuario, nome, bytes, fileSize, dir, isLambda, timestamp);
                        } else {
                        	info = UploadHelper.uploadURL(usuario, url, dir, isLambda, timestamp);
                        }
                        
                    } else if (item != null) {
                        byte[] bytes = item.get();
                        if (bytes == null || bytes.length == 0) {
                            return null;
                        }

                        String nome = item.getName();
                        long fileSize = item != null ? item.getSize() : bytes.length;

                        log("Upload file: ["+nome+"] ");

                        info = UploadHelper.uploadFile(usuario, nome, bytes, fileSize, dir, isLambda, timestamp);
                    }
                }
				
				if (info != null) {
					log("Upload OK : " + info.arquivo);
					log("Upload OK : " + info.file);
					
					a = info.arquivo;
					
					arquivoService.saveOrUpdate(a);

					if(thumbIds == null || thumbIds.isEmpty()) {
						createThumbs(req, info, timestamp);
					}
				}
			}

			if (a != null) {
				
				
				Post post = req.post;
				if(req.destaqueAtual != null && req.destaque) {
					Arquivo arquivo = arquivoService.get(req.destaqueAtual);
					if(arquivo != null) {
						arquivo.setDestaque(false);
						arquivoService.saveOrUpdate(arquivo);
					}
				}
				
				// Post/Mensagem
				a.setPost(post);
				a.setComentario(req.comentario);
				a.setMensagem(req.mensagem);
				a.setDestaque(req.destaque);
				a.setDescricao(req.descricao);
				Integer ordem = req.ordem != null ? req.ordem : 0;
				a.setOrdem(ordem);

				// Tags
				List<Tag> tags = tagService.getTagsAndSave(req.tagsString, empresa);
				a.setTags(new HashSet<Tag>(tags));

				log("Salvando arquivo: " + a.getNome());
				a.setDataUpdate(new Date());
				arquivoService.saveOrUpdate(a);

				log("Arquivo [" + a.getId() + "] salvo com sucesso: " + a.getNome());

				return info;
			}
		} catch (DomainMessageException e) {
			throw e;
		} catch (Throwable e) {
			UploadHelper.log.error(e.getMessage(), e);
			throw new DomainException("Ocorreu um erro ao salvar o arquivo.");
		}
		return null;		
	}

	private boolean isRotate(Integer angle) {
		if(angle != null && angle > 0) {
			return true;
		}
		return false;
	}

	private File resize(byte[] bytes, String nome, int w, int h) throws IOException, FileNotFoundException {
		String tempDir = System.getProperty("java.io.tmpdir");
		File tempFile = new File(tempDir, nome);
		IOUtils.write(bytes, new FileOutputStream(tempFile));

		File file = UploadHelper.resize(tempFile, w, h, nome);
		return file;
	}

	private File rotate(byte[] bytes, String nome, int angle) throws IOException, FileNotFoundException {
		String tempDir = System.getProperty("java.io.tmpdir");
		File tempFile = new File(tempDir, nome);
		IOUtils.write(bytes, new FileOutputStream(tempFile));
		
		Rotation rotation = null;
		if (angle == 90) {
			rotation = Rotation.CW_90;
        } else if (angle == 180) {
        	rotation = Rotation.CW_180;
        } else if (angle == 270) {
        	rotation = Rotation.CW_270;
        }
		
		File file = UploadHelper.rotate(tempFile, rotation, nome);
		return file;
	}

	private void log(String string) {
		UploadHelper.log.debug(string);		
	}

    public File cropImage(String fileName, byte[] bytes, int w, int h, int x, int y) throws IOException {
        String tempDir = System.getProperty("java.io.tmpdir");
        File tempFile = new File(tempDir, fileName);
        IOUtils.write(bytes, new FileOutputStream(tempFile));

        if(!FileExtensionUtils.isImage(fileName)) {
            return tempFile;
        }

        Image src = ImageIO.read(tempFile);

        BufferedImage dst = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        dst.getGraphics().drawImage(src, 0, 0, w, h, x, y, x + w, y + h, null);
        tempFile = new File(tempDir, fileName);
        ImageIO.write(dst, "png", tempFile);

        return tempFile;

    }

	public void createThumbs(UploadRequest req, UploadResponse uploadInfo, long timestamp) throws DomainException {
		Arquivo arquivoUpload = uploadInfo.arquivo;
		if(arquivoUpload == null) {
			return;
		}
		
		ParametrosMap params = ParametrosMap.getInstance(req.empresa);

		String dir = req.dir;
		
		UploadHelper.log.debug("Gerando thumbs para arquivo: " + arquivoUpload.getId());
		
		// Ao gerar thumbs o arquivo fica atualizado.
		try {
			
			Arquivo a = null;
			Empresa empresa = req.empresa;
			
			boolean isLambdaAtivo = ParametrosMap.getInstance(empresa).getBoolean(Params.AMAZON_LAMBDA_CHAT_RESIZE_ON, true);
			
			if(isLambdaAtivo) {
				String dirUpload = empresa.getCodigo();
				a = UploadHelper.createThumbsLambda(params, arquivoUpload, uploadInfo, dirUpload, req.usuario, timestamp);
			} else {
				a = UploadHelper.createThumbs(params, arquivoUpload, uploadInfo, dir, timestamp);
			}
			
			Set<ArquivoThumb> thumbs = a.getThumbs();
			if (thumbs != null) {
				UploadHelper.log.debug(thumbs.size() + " thumbs criadas para arquivo: " + a.getId());
				for (ArquivoThumb t : thumbs) {
					t.setArquivo(a);
					arquivoService.saveOrUpdate(t);
				}
			}
			a.setThumbs(thumbs);
			arquivoService.saveOrUpdate(a);

			UploadHelper.log.debug("Arquivo [" + a.getId() + "] atualizado com thumbs: " + thumbs.size());
		} catch (IOException e) {
			UploadHelper.log.error(e.getMessage(), e);
			throw new DomainException("Erro ao criar as thumbs");
		}
		
	}	
	

}
