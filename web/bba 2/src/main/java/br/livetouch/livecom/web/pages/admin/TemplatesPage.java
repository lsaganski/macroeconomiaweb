package br.livetouch.livecom.web.pages.admin;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;

/**
 * 
 */
@Controller
@Scope("prototype")
public class TemplatesPage extends LivecomAdminPage {

	public ActionLink editLink = new ActionLink("editar", this, "editar");
	public Link deleteLink = new Link("deletar", this, "deletar");
	public Link downloadLink = new Link("download", this, "download");
	public String msgErro;
	public Long id;
	public String html;

	public int page;

	public TemplateEmail email;

	public List<TemplateEmail> templatesEmail;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.ENVIAR_EMAILS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onInit() {
		super.onInit();

		html = null;

		if (id != null) {
			email = templateEmailService.get(id);
		} else {
			email = new TemplateEmail();
		}
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean novo() {
		setRedirect(TemplatesPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		email = templateEmailService.get(id);
		setRedirect(TemplatePage.class, "id", String.valueOf(id));

		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			TemplateEmail e = templateEmailService.get(id);
			templateEmailService.delete(e);
			setRedirect(getClass());
			setFlashAttribute("msg", "Template deletado com sucesso.");
		} catch (DomainException e) {
			logError(e.getMessage(), e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(), e);
			this.msgErro = "Esse template possui relacionamentos. Não foi possível excluir.";
		}
		return true;
	}

	public boolean download() {
		Long id = downloadLink.getValueLong();
		email = templateEmailService.get(id);
		byte[] bytes = email.getHtml().getBytes();
		String fileName = email.getNomeHtml() != null ? email.getNomeHtml() : email.getNome();
		fileName = fileName.contains(".html") ? fileName : fileName + ".html";
		downloadFile(bytes, fileName);
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
