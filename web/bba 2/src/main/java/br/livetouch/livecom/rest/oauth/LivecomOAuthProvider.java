package br.livetouch.livecom.rest.oauth;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.glassfish.jersey.server.oauth1.OAuth1Consumer;
import org.glassfish.jersey.server.oauth1.OAuth1Provider;
import org.glassfish.jersey.server.oauth1.OAuth1Token;
import org.springframework.stereotype.Component;

@Component
public class LivecomOAuthProvider implements OAuth1Provider {
	private static Logger log = Logger.getLogger(LivecomOAuthProvider.class);
	
	@Override
	public OAuth1Consumer getConsumer(String consumerKey) { 
		LivecomConsumer livecomConsumer = ConsumerKeyMap.getInstance().get(consumerKey);
		if(livecomConsumer == null) {
			log.debug("Não foi localizada a consumer com a key " + consumerKey);
		}
		return livecomConsumer;
	}


	@Override
	public LivecomToken getRequestToken(String token) {
		return RequestTokenMap.getInstance().get(token);
	}

	@Override
	public OAuth1Token newRequestToken(String consumerKey, String callbackUrl, Map<String, List<String>> attributes) {
		return RequestTokenMap.getInstance().newRequestToken(consumerKey, callbackUrl, attributes);
	}

	@Override
	public OAuth1Token newAccessToken(OAuth1Token requestToken, String verifier) {
		OAuth1Token newAccessToken = AccessTokenMap.getInstance().newAccessToken(requestToken, verifier);
		return newAccessToken;
	}

	@Override
	public OAuth1Token getAccessToken(String token) {
		LivecomToken livecomToken = AccessTokenMap.getInstance().get(token);
		if(livecomToken == null) {
			log.debug("Não foi localizado o AccessToken " + token);
		}
		return livecomToken;
	}
}