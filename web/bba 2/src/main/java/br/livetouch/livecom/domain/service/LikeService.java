package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Likes;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.LikeVO;
import br.livetouch.livecom.domain.vo.PostCountVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioLikesVO;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import net.livetouch.tiger.ddd.DomainException;

public interface LikeService extends Service<Likes> {

	@Transactional(rollbackFor=Exception.class)
	Likes like(Usuario u, Post post, Comentario comentario, boolean liked) throws DomainException;
	
	List<Likes> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Likes c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Likes c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(List<Long> ids) throws DomainException;

	Likes findByUserAndPost(Usuario u, Post post);
	Likes findByUserAndComentario(Usuario u, Comentario comentario);

	List<Likes> findAllLikesOfComentarioByPost(Post post);
	
	List<Likes> findAllLikesOfPostByUser(Usuario u);
	List<Likes> findAllLikesByUser(Usuario u);
	
	List<Likes> findAllLikesByPost(Post p);
	
	long getCountLikesByPost(Post p);
	
	List<Likes> findAllLikesByComentario(Comentario c);
	
	List<Likes> findAllLikesNotificationByUser(Usuario user);

	@Transactional(rollbackFor=Exception.class)
	void clearNotification(Long id) throws DomainException;

	long getCountByFilter(RelatorioFiltro filtro, boolean count, boolean geral) throws DomainException;
	
	List<RelatorioLikesVO> findbyFilter(RelatorioFiltro filtro, boolean count) throws DomainException;

	List<LikeVO> findDetalhesByDate(RelatorioFiltro filtro);

	long getCountDetalhesByDate(RelatorioFiltro filtro, boolean b);

	String csvDetalhesLikes(RelatorioFiltro filtro);

	List<RelatorioLikesVO> findbyFilterConsolidado(RelatorioFiltro filtro) throws DomainException;

	String csvLikes(RelatorioFiltro filtro) throws DomainException;

	List<Likes> findAllByPosts(Usuario user, List<Long> ids);

	List<PostCountVO> findCountByPosts(List<Long> ids);

	Long countByPost(Post p);

	List<RelatorioVisualizacaoVO> reportLikes(RelatorioFiltro filtro) throws DomainException;
}
