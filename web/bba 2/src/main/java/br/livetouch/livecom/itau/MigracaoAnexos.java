package br.livetouch.livecom.itau;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.jdbc.support.JdbcUtils;

public class MigracaoAnexos {

	public static void main(String args[]) throws Exception {
		Connection connection = null;
		Connection connectionPostgres = null;
		
		ResultSet rs = null;
		Statement stmt = null;
		
		PreparedStatement stmtInsert = null;
		int BATCH_SIZE = 4000;

		try {
			
			//DRIVER MYSQL
			System.out.println("Testando conexao... Mysql");
			String driver = "com.mysql.jdbc.Driver";
			Class.forName(driver);
			String url = "jdbc:mysql://localhost/macroeconomia";
			System.out.println(url);
			connection = DriverManager.getConnection(url, "root", "root");
			System.out.println("Conn Mysql OK: " + connection);
			
			if (connection == null) {
				throw new SQLException("Sem conexao!");
			}
			
			//DRIVER POSTGRES
			System.out.println("Testando conexao... Postgres");
			String driverPostgres = "org.postgresql.Driver";
			Class.forName(driverPostgres);
			String urlPostgres = "jdbc:postgresql://localhost:5432/macroeconomia";
			System.out.println(urlPostgres);
			connectionPostgres = DriverManager.getConnection(urlPostgres, "postgres", "root");
			System.out.println("Conn Postgres OK: " + connectionPostgres);
			
			if (connectionPostgres == null) {
				throw new SQLException("Postgres sem conexao!");
			}

			//POSTS BBA
			String sql = "SELECT * FROM attachments";
			stmt = connectionPostgres.createStatement();
			rs = stmt.executeQuery(sql);

			if (rs == null) {
				throw new SQLException("Sem dados!");
			}
			
			//ZERA TABELA TEMPORARIA MYSQL
			connection.createStatement().execute("truncate table arquivos_temp");

			//INSERT TABELA TEMPORARIA MYSQL
			int countTemp = 0;
			String uri = "https://itau-macroeconomia.s3.amazonaws.com/files/";
			stmtInsert = connection.prepareStatement("INSERT INTO arquivos_temp (date_create, date_update, nome, descricao, url, post_id) values (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			while (rs.next()) {
				int i = 1;
				try {
					countTemp++;
					stmtInsert.setDate(i++, rs.getDate("created_at"));
					stmtInsert.setDate(i++, rs.getDate("updated_at"));
					stmtInsert.setString(i++, rs.getString("file"));
					stmtInsert.setString(i++, rs.getString("caption"));
					stmtInsert.setString(i++, uri + rs.getLong("id") + "/" + rs.getString("file"));
					stmtInsert.setLong(i++, rs.getLong("report_id"));
					stmtInsert.addBatch();
					if (countTemp > BATCH_SIZE) {
						stmtInsert.executeBatch();
						stmtInsert.clearBatch();
						countTemp = 0;
					}
				} catch(Exception e) {
					System.out.println("Erro: " + e.getMessage());
				}
			}
			
		} catch (Exception e) {
			System.out.println("Erro: " + e.getMessage());
			e.printStackTrace();
		} finally {
			
			stmtInsert.executeBatch();
			stmtInsert.clearBatch();
			
			JdbcUtils.closeResultSet(rs);
			JdbcUtils.closeStatement(stmt);
			JdbcUtils.closeStatement(stmtInsert);
			JdbcUtils.closeConnection(connection);
			
			JdbcUtils.closeConnection(connectionPostgres);
		}
	}
}