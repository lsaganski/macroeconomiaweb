package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.livetouch.livecom.domain.Parametro;

public class ParametroVO implements Serializable {

	private static final long serialVersionUID = 1524516347642576158L;
	public Long id;
	public String nome;
	public String valor;
	
	public ParametroVO() {}
	
	public ParametroVO(String nome, String valor) {
		this.nome = nome;
		this.valor = valor;
	}
	
	public ParametroVO(Long id, String nome, String valor) {
		this.id = id;
		this.nome = nome;
		this.valor = valor;
	}
	
	public static List<ParametroVO> toList(List<Parametro> list) {
		List<ParametroVO> params = new ArrayList<ParametroVO>();
		if (list != null) {
			for (Parametro p : list) {
				ParametroVO vo = new ParametroVO(p.getId(), p.getNome(), p.getValor());
				params.add(vo);
			}
		}
		return params;
	}
	
	public static Map<String, String> toMap(List<Parametro> list) {
		Map<String, String> params = new HashMap<String, String>();
		if (list != null) {
			for (Parametro p : list) {
				params.put(p.getNome(), p.getValor());
			}
		}
		return params;
	}
	
}
