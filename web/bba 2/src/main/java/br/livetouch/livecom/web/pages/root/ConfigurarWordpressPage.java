package br.livetouch.livecom.web.pages.root;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.web.pages.LivecomRootPage;
//import br.infra.web.click.ComboVersao;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;


@Controller
@Scope("prototype")
public class ConfigurarWordpressPage extends LivecomRootPage {	

	public Form form = new Form();
	private TextField tUrl;
	private Checkbox tAtivar;

	@Override
	public void onInit() {
		super.onInit();

		form();
	}
	
	public void form() {
		
		tUrl = new TextField("url");
		tUrl.setAttribute("class", "input inputClean");
		tUrl.setId("url");
		form.add(tUrl);
		
		tAtivar = new Checkbox("ativar");
		tAtivar.setAttribute("class", "inputClean");
		tAtivar.setChecked(false);
		form.add(tAtivar);
		
		Submit tSalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tSalvar.setAttribute("class", "botao salvar");
		form.add(tSalvar);
		
	}

	@Override
	public void onGet() {
		super.onGet();
		
		String url = ParametrosMap.getInstance(getEmpresaId()).get(Params.WORDPRESS_SITE, "");
		int ativar = Integer.valueOf(ParametrosMap.getInstance(getEmpresaId()).get(Params.WORDPRESS_ON, "0"));
		
		if(ativar == 0) {
			tAtivar.setChecked(false);
		} else {
			tAtivar.setChecked(true);
		}
		
		tUrl.setValue(url);
		
		
	}
	
	public boolean salvar() throws DomainException {		
		
		Parametro param = parametroService.findByNome(Params.WORDPRESS_SITE, getEmpresa()) != null ? parametroService.findByNome(Params.WORDPRESS_SITE, getEmpresa()) : new Parametro();
		param.setNome(Params.WORDPRESS_SITE);
		param.setValor(tUrl.getValue());
		param.setEmpresa(getEmpresa());
		parametroService.saveOrUpdate(param);
		
		String ativar = tAtivar.isChecked() ? "1" : "0";
		
		param = parametroService.findByNome(Params.WORDPRESS_ON, getEmpresa()) != null ? parametroService.findByNome(Params.WORDPRESS_ON, getEmpresa()) : new Parametro();
		param.setNome(Params.WORDPRESS_ON);
		param.setValor(ativar);
		param.setEmpresa(getEmpresa());
		parametroService.saveOrUpdate(param);
		
		return true;
	}
	
	@Override
	public String getContentType() {
		return super.getContentType();
	}
	
	@Override
	public String getTemplate() {
		return super.getTemplate();
	}

}
