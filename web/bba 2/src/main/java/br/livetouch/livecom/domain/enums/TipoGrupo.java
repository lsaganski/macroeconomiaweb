package br.livetouch.livecom.domain.enums;

/**
 * @author Ricardo Lecheta
 * @created 23/02/2013
 */
public enum TipoGrupo {

	PRIVADO("PRIVADO","Grupo privado aonde o usuário não pode visualizar ou solicitar para participar do grupo"),
	PRIVADO_OPCIONAL("PRIVADO_OPCIONAL","Grupo privado aonde usuário pode sair a qualquer momento"),
	ABERTO("ABERTO","Grupo aberto para qualquer usuário visualizar ou participar"),
	ABERTO_FECHADO("ABERTO_FECHADO","Grupo aberto para visualizar mas precisa solicitar permissão para participar do Grupo");

	private final String s;
	private final String desc2;

	TipoGrupo(String s,String desc2){
		this.s = s;
		this.desc2 = desc2;
	}

	@Override
	public String toString() {
		return s != null ? s : "?";
	}

	public String getDesc() {
		return s;
	}
	
	public String getDesc2() {
		return desc2;
	}
}
