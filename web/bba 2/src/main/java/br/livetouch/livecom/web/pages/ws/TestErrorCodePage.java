package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class TestErrorCodePage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;

	public String status;
	public String mensagem;
	public String errorCode;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");

		TextField tStatus = null;
		form.add(tStatus=new TextField("status","status"));
		form.add(new TextField("mensagem","mensagem"));
		form.add(new TextField("errorCode","errorCode"));
		
		form.add(new TextField("wsVersion"));
		form.add(tMode = new TextField("mode","mode"));
		
		tMode.setValue("json");

		form.add(new Submit("Test"));
		
		tStatus.setFocus(true);
		tStatus.setValue("OK");

		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Response r = new Response();
			
			r.status = status;
			r.errorCode = errorCode;
			r.mensagem = mensagem;
			
			return r;
		}
		
		return new MensagemResult("NOK","Erro ao buscar notificações.");
	}
	
	@Override
	protected boolean isResponseGSON() {
		return true;
	}
}
