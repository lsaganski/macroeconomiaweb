package br.livetouch.livecom.web.pages.admin;


import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Palavra;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.Table;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LinkDecorator;

/**
 * 
 */
@Controller
@Scope("prototype")
public class PalavraPage extends LivecomAdminPage {

	public Table table = new Table();
	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public int page;

	public Palavra palavra;

	public List<Palavra> palavras;
	
	@Override
	public void onInit() {
		super.onInit();

		form();
		
		table();

		if (id != null) {
			palavra = palavraService.get(id);
		} else {
			palavra = new Palavra();
		}
		form.copyFrom(palavra);
	}
	
	public void form(){
		form.add(new IdField());

		TextField t = new TextField("palavra", getMessage("palavra.label"));
		t.setAttribute("autocomplete", "off");
		t.setAttribute("class", "input");
		t.setFocus(true);
		form.add(t);
		
		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);
		
		Submit cancelar = new Submit("cancelar", getMessage("cancelar.label"), this,"novo");
		cancelar.setAttribute("class", "botao");
		form.add(cancelar);

		setFormTextWidth(form);
	}
	
	private void table() {
		Column c = new Column("id");
		c.setWidth("50px");
		c.setTextAlign("center");
		table.addColumn(c);

		c = new Column("palavra", getMessage("palavra.label"));
		c.setHeaderStyle("text-align", "center");
		c.setTextAlign("center");
		table.addColumn(c);
		
		ActionLink[] links = new ActionLink[] { editLink, deleteLink };
		c = new Column("detalhes", getMessage("detalhes.label"));
		c.setTextAlign("center");
		c.setWidth("150px");
		c.setDecorator(new LinkDecorator(table, links, "id"));
		table.addColumn(c);

		deleteLink.setConfirmMessage(getMessage("deseja.excluir.label"));
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				palavra = id != null ? palavraService.get(id) : new Palavra();
				form.copyTo(palavra);

				palavraService.saveOrUpdate(palavra);

				setFlashAttribute("msg",  getMessage("msg.palavra.salvar.sucess", palavra.getPalavra()));
				setRedirect(getClass());

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(getClass());
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		palavra = palavraService.get(id);
		form.copyFrom(palavra);
		
		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Palavra e = palavraService.get(id);
			palavraService.delete(e);
			setRedirect(getClass());
			setFlashAttribute("msg", getMessage("msg.palavra.excluir.sucess"));
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.palavra.excluir.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		palavras = palavraService.findAll();

		// Count(*)
		int pageSize = 1000;

		table.setPageSize(pageSize);
		table.setRowList(palavras);
	}
}
