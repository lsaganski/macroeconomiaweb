package br.livetouch.livecom.chatAkka.protocol;


import org.json.JSONObject;

import akka.actor.ActorRef;
import akka.util.ByteString;
import akka.util.ByteStringBuilder;

public class JsonRawMessage {

	/**
	 * Ator que enviou a msg
	 */
	public ActorRef actor;

	/**
	 * Mensagem JSON
	 */
	public RawMessage raw;

	/**
	 * Código da mensagem
	 */
	public String code;

	/**
	 * web, android, ios
	 */
	public String userSo;

	public Long userId;

	public JsonRawMessage(JSONObject json) {
		if(json != null) {
			raw = RawMessage.apply(json.toString());
		}
	}
	
	public JsonRawMessage(String json) {
		apply(json);
	}

	public void apply(String json) {
		raw = RawMessage.apply(json);
	}

	public JsonRawMessage(ActorRef actor, RawMessage raw, String code, String userSo, Long userId) {
		this.actor = actor;
		this.raw = raw;
		this.code = code;
		this.userSo = userSo;
		this.userId = userId;
	}

	@Override
	public String toString() {
		return getJson();
	}

	public String getJson() {
		return raw.getText();
	}
	
	public byte[] getBytes() {
		return raw.getBytes();
	}

	public ByteString getByteString() {
		byte[] bytes = raw.getBytes();
		
		ByteStringBuilder bb = new ByteStringBuilder();
		bb.putBytes(bytes);
		
		/**
		 * Avisa pro user que enviou a msg, que servidor recebeu
		 * "serverReceivedMessage"
		 */
		ByteString byteString = bb.result();

		return byteString;
	}

	public boolean isUserActorOk() {
		return raw != null && actor != null && userId != null && userSo != null;
	}
}
