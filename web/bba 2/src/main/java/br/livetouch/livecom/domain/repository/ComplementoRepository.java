package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Complemento;
import br.livetouch.livecom.domain.Empresa;

@Repository
public interface ComplementoRepository extends net.livetouch.tiger.ddd.repository.Repository<Complemento> {

	List<Complemento> findAll(Empresa empresa);
	
}