package br.livetouch.livecom.jobs;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.infra.util.Log;

public class HelloJob implements Job {

	protected static final Logger log = Log.getLogger(HelloJob.class);


	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("*** HelloJob ["+Thread.currentThread().getName()+"]: " + new Date());
		
	}
}
	