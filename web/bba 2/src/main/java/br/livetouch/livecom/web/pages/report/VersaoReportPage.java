package br.livetouch.livecom.web.pages.report;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboTipoNotificacao;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoFiltro;
import net.livetouch.extras.util.DateUtils;
import net.sf.click.control.Form;

@Controller
@Scope("prototype")
public class VersaoReportPage extends RelatorioPage {

	public Form form = new Form();
	
	public RelatorioFiltro filtro;
	public int page;
	public String action;
	public ComboTipoNotificacao comboTipo;
	
	@Override
	public void onInit() {
		super.onInit();

		if (clear) {
			getContext().removeSessionAttribute(RelatorioVisualizacaoFiltro.SESSION_FILTRO_KEY);
			filtro = new RelatorioFiltro();
			filtro.setDataIni(DateUtils.toString(br.livetouch.livecom.utils.DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
			filtro.setDataFim(DateUtils.toString(new Date(), "dd/MM/yyyy"));
		} else {
			filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
			if (filtro != null) {
				if (filtro.getUsuarioId() != null) {
					Usuario usuario = usuarioService.get(filtro.getUsuarioId());
					if (usuario != null) {
						filtro.setUsuario(usuario);
					}
				}
				if (filtro.getPostId() != null) {
					Post post = postService.get(filtro.getPostId());
					if (post != null) {
						filtro.setPost(post);
					}
				}
				if (filtro.getCategoriaId() != null) {
					CategoriaPost categoria = categoriaPostService.get(filtro.getCategoriaId());
					if (categoria != null) {
						filtro.setCategoria(categoria);
					}
				}
			}
		}
		
		form();
	}
	
	public void form() {
		comboTipo = new ComboTipoNotificacao();
		comboTipo.setAttribute("class", "input margin");
		form.add(comboTipo);
	}

//	@Override
//	public void onPost() {
//		super.onPost();
//		if ("1".equals(action)) {
//			String csv = reportService.csvVisualizacao(filtro);
//			download(csv, "relatorio-visualizacao.csv");
//		}
//	}
}
