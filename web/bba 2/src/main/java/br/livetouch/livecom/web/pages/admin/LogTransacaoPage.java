package br.livetouch.livecom.web.pages.admin;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboStatusTransacao;
import br.livetouch.livecom.domain.LogTransacao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.LogTransacaoFiltro;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.DateField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LinkDecorator;

/**
 * Logs das transações de consulta celular.
 * 
 * Admin pode ver todos os logs Usuario celular somente verifica os seus logs.
 * 
 */
@Controller
@Scope("prototype")
public class LogTransacaoPage extends RelatorioPage {

	public PaginacaoTable table = new PaginacaoTable();
	public ActionLink detalhes = new ActionLink("detalhes", getMessage("detalhes.label"), this, "detalhes");
	public Form form = new Form();
	public LogTransacaoFiltro filtro;
	public int page;
	public boolean exportar;
	private IntegerField tMax;
	public ComboStatusTransacao comboStatus;

	@Override
	public void onInit() {
		super.onInit();
		
		bootstrap_on = true;
		escondeChat = true;

		if (clear) {
			getContext().removeSessionAttribute(LogTransacaoFiltro.SESSION_FILTRO_KEY);
		}

		filtro = (LogTransacaoFiltro) getContext().getSessionAttribute(LogTransacaoFiltro.SESSION_FILTRO_KEY);
		if (filtro != null) {
			form.copyFrom(filtro);
		} else {
			filtro = new LogTransacaoFiltro();
			form.copyTo(filtro);
		}

		table();

		form();
	}

	private void table() {
		Column c = new Column("id", getMessage("coluna.id.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("login", getMessage("coluna.login.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("deviceSo", "SO");
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("requestPath", getMessage("coluna.log.requestPath.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("dataInicioString", getMessage("dataInicioString.headerTitle"));
		c.setTextAlign("center");
		c.setAttribute("align", "center");
		table.addColumn(c);

		c = new Column("status", getMessage("coluna.status.label"));
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("detalhes", getMessage("detalhes.label"));
		c.setTextAlign("center");
		c.setAttribute("align", "center");
		c.setDecorator(new LinkDecorator(table, detalhes, "id"));
		table.addColumn(c);
		
	}

	public void form() {
		DateField tDataInicio = new DateField("dataInicial", getMessage("dataInicio.label"));
		DateField tDataFim = new DateField("dataFinal", getMessage("dataFim.label"));
		tDataInicio.setAttribute("class", "input data datepicker");
		tDataFim.setAttribute("class", "input data datepicker");
		tDataInicio.setFormatPattern("dd/MM/yyyy");
		tDataFim.setFormatPattern("dd/MM/yyyy");

		tDataInicio.setValue(DateUtils.toString(DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
		tDataFim.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));

		tDataInicio.setMaxLength(10);
		tDataFim.setMaxLength(10);

		IntegerField tId = new IntegerField("id");
		tId.setAttribute("class", "input");
		tId.setFocus(true);

		form.setColumns(1);

		form.add(tId);

		TextField t = new TextField("requestPath", getMessage("coluna.log.requestPath.label"));
		t.setAttribute("class", "input");
		form.add(t);

		form.add(tDataInicio);
		form.add(tDataFim);

		TextField tLogin = new TextField("usuarioId", getMessage("coluna.login.label"));
		tLogin.setAttribute("class", "input");
		tLogin.setId("usuario");
		form.add(tLogin);

		form.add(comboStatus = new ComboStatusTransacao());
		comboStatus.setLabel(getMessage("coluna.log.status.label"));

		tMax = new IntegerField("max");
		tMax.setAttribute("class", "input");
		form.add(tMax);

		// form.add(tMax = new IntegerField("max"));

		Submit tfiltrar = new Submit("filtrar", getMessage("filtrar.label"), this, "filtrar");
		tfiltrar.setAttribute("class", "btn btn-primary min-width");
		form.add(tfiltrar);

		Submit texportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		texportar.setAttribute("class", "btn btn-outline blue min-width");
		form.add(texportar);

		Submit tExportarCsv = new Submit("exportarCsv", getMessage("exportar.csv.label"), this, "exportarCsv");
		tExportarCsv.setAttribute("class", "btn btn-outline blue min-width");
		form.add(tExportarCsv);
	}

	@Override
	public void onGet() {
		super.onGet();

		filtro = (LogTransacaoFiltro) getContext().getSessionAttribute(LogTransacaoFiltro.SESSION_FILTRO_KEY);
		if (filtro != null) {
			form.copyFrom(filtro);
			if (filtro.getUsuarioId() != null) {
				Usuario u = usuarioService.get(filtro.getUsuarioId());
				filtro.setUsuario(u);
			} else {
				filtro.setUsuario(null);
			}
		}
	}

	public boolean filtrar() {
		filtro = (LogTransacaoFiltro) getContext().getSessionAttribute(LogTransacaoFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new LogTransacaoFiltro();
			filtro.setEmpresa(getEmpresa());
			getContext().setSessionAttribute(LogTransacaoFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);
		if (filtro.getUsuarioId() != null) {
			Usuario u = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(u);
		} else {
			filtro.setUsuario(null);
		}
		return true;
	}

	public boolean exportar() {
		filtrar();
		exportar = true;
		return true;
	}

	public boolean exportarCsv() throws DomainException {
		filtro = (LogTransacaoFiltro) getContext().getSessionAttribute(LogTransacaoFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new LogTransacaoFiltro();
			filtro.setEmpresa(getEmpresa());
			getContext().setSessionAttribute(LogTransacaoFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);
		if (filtro.getUsuarioId() != null) {
			Usuario u = usuarioService.get(filtro.getUsuarioId());
			filtro.setUsuario(u);
		} else {
			filtro.setUsuario(null);
		}

		String csv = logService.csvTransacao(filtro, getUsuario(), -1, -1, getEmpresa());
		download(csv, "relatorio-transacao.csv");

		return true;
	}

	public boolean detalhes() {
		DetalhesLogTransacaoPage page = (DetalhesLogTransacaoPage) getContext().createPage(DetalhesLogTransacaoPage.class);
		page.id = detalhes.getValueLong();
		setForward(page);
		return false;
	}

	@Override
	public String getContentType() {
		// if(exportar) {
		// return "application/vnd.ms-excel";
		// }
		return super.getContentType();
	}

	@Override
	public String getTemplate() {
		if (exportar) {
			return getPath();
		}
		return super.getTemplate();
	}

	@Override
	public void onRender() {
		super.onRender();

		if (filtro == null) {
			filtro = new LogTransacaoFiltro();
			form.copyTo(filtro);
		}
		Integer max = tMax.getInteger();
		if (max == null) {
			max = 20;
		}

		List<LogTransacao> logs;
		try {
			if (exportar) {
				max = -1;
			}

			logs = logService.findbyFilter(filtro, getUsuario(), page, max, getEmpresa());
		} catch (DomainException e) {
			form.setError(e.getMessage());
			return;
		}

		// Count(*)
		try {
			if (!exportar) {
				table.setPageSize(max);
				long logsCount = logService.getCountByFilter(filtro, getUsuario(), getEmpresa());
				table.setCount(logsCount);
			}
		} catch (DomainException e) {
			logError(e.getMessage(), e);
		}
		table.setRowList(logs);
	}
}
