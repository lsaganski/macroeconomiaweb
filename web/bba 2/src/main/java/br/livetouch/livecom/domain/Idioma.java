package br.livetouch.livecom.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.gson.Gson;

import br.livetouch.livecom.domain.vo.IdiomaVO;
import net.livetouch.tiger.ddd.DomainException;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Idioma extends JsonEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1424214675602321861L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "IDIOMA_SEQ")
	private Long id;

	private String nome;
	private String codigo;
	private String icone;
	
	@OneToMany(mappedBy = "idioma", fetch = FetchType.LAZY)
	private Set<CategoriaIdioma> categorias;

	@OneToMany(mappedBy = "idioma", fetch = FetchType.LAZY)
	private Set<PostIdioma> posts;
	
	public Idioma() {
		super();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	@Override
	public String toJson() {
		IdiomaVO vo = new IdiomaVO(this);
		return new Gson().toJson(vo);
	}
	
	public static void isFormValid(Idioma idioma, boolean isUpdate) throws DomainException {
		if(idioma == null) {
			throw new DomainException("Parametros inválidos");
		}
		
		if(StringUtils.isEmpty(idioma.codigo)) {
			throw new DomainException("Campo codigo não pode ser vazio");
		}
		
		if(isUpdate && idioma.getId() == null) {
			throw new DomainException("Idioma não localizado");
		}
		
	}

	public Set<CategoriaIdioma> getCategorias() {
		return categorias;
	}

	public void setCategorias(Set<CategoriaIdioma> categorias) {
		this.categorias = categorias;
	}

	public Set<PostIdioma> getPosts() {
		return posts;
	}

	public void setPosts(Set<PostIdioma> posts) {
		this.posts = posts;
	}

	public String getIcone() {
		return icone;
	}

	public void setIcone(String icone) {
		this.icone = icone;
	}

}
