package br.livetouch.livecom.email;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.service.TemplateEmailService;

public class EmailUtils {
	/**
	 * email.log
	 */
	protected static Logger log = Log.getLogger(EmailUtils.class);
	
	@Autowired
	protected TemplateEmailService templateEmailService;

	public static boolean enviar(String from, String to, String subject, String msg, boolean html) throws IOException, EmailException {

		log.debug("Enviando email para: " + to + ", [" + subject + "]: " + msg);

		EmailManager emailManager = EmailFactory.getEmailManager();
		boolean b = emailManager.sendEmail(from, to, subject, msg, html);

		log.debug("Email enviado: " + to + ", [" + subject + "]: " + b);

		return b;
	}

	public static boolean isEmailValido(String email) {
		Pattern p = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");
		Matcher m = p.matcher(email);
		return m.find() ? true : false;
	}
}
