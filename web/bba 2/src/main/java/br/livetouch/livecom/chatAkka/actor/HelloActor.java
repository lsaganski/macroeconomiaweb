package br.livetouch.livecom.chatAkka.actor;

import org.apache.log4j.Logger;

import akka.actor.UntypedActor;
import br.infra.util.Log;

/**
ActorRef helloActor = AkkaFactory.createActor(getContext(),HelloActor.class);
helloActor.tell(new HelloActor.HelloMessage("Olá"), getSelf());
 * 
 * @author rlech
 *
 */
public class HelloActor extends UntypedActor {
	private static Logger log = Log.getLogger(HelloActor.class);
	public static class HelloMessage {

		private String string;

		public HelloMessage(String string) {
			this.string = string;
		}

		@Override
		public String toString() {
			return "HelloMessage [string=" + string + "]";
		}

	}

	@Override
	public void onReceive(Object obj) throws Exception {
		log.debug("HelloActor.onReceive: " + obj);
	}

}
