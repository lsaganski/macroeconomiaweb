package br.livetouch.livecom.domain.service;

import java.util.List;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PushReport;
import br.livetouch.livecom.domain.vo.PushReportFiltro;
import br.livetouch.livecom.domain.vo.PushReportVO;

public interface PushReportService extends Service<PushReport> {

	public void saveOrUpdate(PushReport p);
	public void delete(PushReport p);
	public List<PushReport> findAll();
	public List<PushReportVO> findReportByFilterVO(PushReportFiltro filtro, Empresa empresa);
	public List<PushReport> findReportByFilter(PushReportFiltro filtro, Empresa empresa);
	public String csvPushReport(PushReportFiltro filtro, Empresa empresa);
	public List<PushReport> findByPost(List<Post> posts);
	public void delete(List<PushReport> pushReports);
}
