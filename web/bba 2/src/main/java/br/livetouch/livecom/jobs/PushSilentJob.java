package br.livetouch.livecom.jobs;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.MensagemService;
import br.livetouch.livecom.push.PushLivecomService;
import br.livetouch.livecom.push.PushNotificationVO;

/**
 * V2: Varre a tabela de Notification para enviar Pushs.
 * 
 * @author rlech
 *
 */
@Service
public class PushSilentJob extends SpringJob {
	protected static final Logger log = Log.getLogger(PushSilentJob.class);

	@Autowired
	private PushLivecomService pushLivecomService;

	@Autowired
	protected MensagemService mensagemService;

	@Autowired
	protected EmpresaService empresaService;

	@Autowired
	protected SessionFactory sessionFactory;
	
	private static boolean running = false;

	@Override
	protected void execute(Map<String, Object> params) throws Exception {
		if (running) {
			log("PushJob already running.");
			return;
		}
		
		running = true;
		
		try {
			if (mensagemService != null) {

				Long count = mensagemService.countMensagensNaoLidas();
				
				if(count == null || count <= 0L) {
					log("Nenhuma msg não lida para enviar silent.");
					running = false;
					return;
				}
				
				List<Empresa> empresas = empresaService.findAll();
				for (Empresa empresa : empresas) {
					
					boolean silentOn = ParametrosMap.getInstance(empresa).isSilentOn();
					if(!silentOn) {
						log("Silent desligado para empresa: " + empresa);
						continue;
					}
					
					List<Long> list = mensagemService.usersToSilent(empresa);
					
					if(list != null && list.size() > 0) {
						log("SilentJob.execute(), list: " + list.size());
						
						try {
							PushNotificationVO n = new PushNotificationVO();
							n.setEmpresaId(empresa.getId());
						
							pushLivecomService.silent(list, n);
						}catch (Exception ex) {
							ex.printStackTrace();
							log("<< push silent[" + ex.getMessage() + "]: ERROR");
						} finally {
							log("<< push silent OK");
						}
					}
					
				}

			}
		}finally {
			running = false;
		}
	}

	private void log(String string) {
		log.debug(string);
	}
}
