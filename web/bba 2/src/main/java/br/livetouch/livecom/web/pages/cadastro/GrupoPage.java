package br.livetouch.livecom.web.pages.cadastro;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.EntityField;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoGrupo;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.vo.CamposMap;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Form;
import net.sf.click.control.Radio;
import net.sf.click.control.RadioGroup;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;


@Controller
@Scope("prototype")
public class GrupoPage extends CadastrosPage {

	public ActionLink editarLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public ActionLink detalhesLink = new ActionLink("detalhes", getMessage("detalhes.label"), this, "detalhes");

	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public Form form = new Form();
	public int page;
	public String msgErro;
	public Long id;
	public String tab;
	public int salvoComSucesso;
	public Grupo grupo;
	public List<Usuario> usuarios;
	public TextField tHoraAbertura;
	public TextField tHoraFechamento;
	public Checkbox tPadrao;
	public Checkbox tVirtual;
	public Checkbox tTodosPostam;
	public Usuario adminGrupo;
	public RadioGroup tTipoGrupo;
	public EntityField<CategoriaPost> tCategoria;
	
	public GrupoUsuarios gu;
	
	
	public Long arquivoFotoId;

	public boolean tipoGrupo = false;
	public boolean favorito = false;
	public boolean somenteAdmin = false;
	public boolean isSubadmin = false;

	public CamposMap campos;
	
//	@Override
//	public boolean onSecurityCheck() {
//		super.onSecurityCheck();
//		Aguardar tela de permissões
//		return hasPermissao("cadastrar_grupo");
//	}

	@Override
	public String getTemplate() {
		return super.getTemplate();//"/border_modal.htm";
	}
	
	@Override
	public void onInit() {
		super.onInit();
		
		escondeChat = true;
		bootstrap_on = true;
		
		campos = getCampos();

		if(id != null) {
			grupo = grupoService.get(id);
		}
		
		tipoGrupo = getParametrosMap().getBoolean(Params.CADASTRO_GRUPO_TIPO_ON, false);
		
		form();
		
		if (grupo != null) {
			StringBuilder sb = new StringBuilder("");
			
			boolean isAdmin = grupoService.isAdminOrSubadminGrupo(getUsuario(), grupo);
			isSubadmin = isAdmin;
			validateAcessoGrupo(isAdmin, tipoGrupo, true, grupo);
			markNotificationAsRead(isAdmin, grupo);
			
			gu = usuarioService.getGrupoUsuario(getUsuario(), grupo);
			if(gu != null) {
				favorito = gu.isFavorito();
			}
			
			List<Usuario> usuarios = grupoService.findUsuarios(grupo);
			for (Usuario u : usuarios) {
				sb.append(u.getId()).append(", ");
			}
			form.copyFrom(grupo);
			tPadrao.setChecked(grupo.isPadrao());
			adminGrupo = grupo.getUserCreate();
			somenteAdmin = somenteAdminNoGrupo(grupo);
		}
		
	}
	
	private void markNotificationAsRead(boolean isAdmin, Grupo grupo) {
		if(isAdmin) {
			notificationService.markGrupoAsRead(grupo, getUsuario());
		}
	}

	public void form() {
		form.setMethod("post");
		
		form.add(new IdField());

		tCategoria = new EntityField<>("categoria_grupo", categoriaPostService);
		form.add(tCategoria);
		
		TextField tNome = new TextField("nome", getMessage("nome.label"), true);
		tNome.setFocus(false);
		tNome.setAttribute("class", "form-control input-sm");
		tNome.setAttribute("placeholder", getMessage("grupo.vai.ser.chamado.label"));
		tNome.setAttribute("autocomplete", "off");
		//if(id != null){
		//	tNome.setReadonly(true);
		//}
		form.add(tNome);
		
		TextField tCod = new TextField("codigo", getMessage("codigo.label"), false);
		tCod.setFocus(false);
		tCod.setAttribute("placeholder", "Código do grupo");
		tCod.setAttribute("class", "form-control input-sm");
		tCod.setAttribute("autocomplete", "off");
		//if(id != null){
		//	tNome.setReadonly(true);
		//}
		form.add(tCod);
		
		tHoraAbertura = new TextField("horaAbertura");
		form.add(tHoraAbertura);
		
		tHoraFechamento = new TextField("horaFechamento");
		form.add(tHoraFechamento);
		
		tPadrao = new Checkbox("padrao", getMessage("padrao.label"));
		tPadrao.setAttribute("class", "bootstrap");
		tPadrao.setAttribute("data-off-color", "danger");
		tPadrao.setAttribute("data-off-text", "Não");
		tPadrao.setAttribute("data-on-text", "Sim");
		form.add(tPadrao);

		tVirtual = new Checkbox("grupoVirtual", "Grupo Virtual");
		tVirtual.setAttribute("class", "bootstrap");
		tVirtual.setAttribute("data-off-color", "danger");
		tVirtual.setAttribute("data-off-text", "Não");
		tVirtual.setAttribute("data-on-text", "Sim");
		form.add(tVirtual);

		tTodosPostam = new Checkbox("todosPostam", "Todos Podem Postar");
//		tVirtual.setAttribute("class", "bootstrap");
//		tVirtual.setAttribute("data-off-color", "danger");
//		tVirtual.setAttribute("data-off-text", "Não");
//		tVirtual.setAttribute("data-on-text", "Sim");
		tTodosPostam.setChecked(true);
		form.add(tTodosPostam);

		tTipoGrupo = new RadioGroup("radioTipoGrupo");
		Radio radioPrivado = new Radio("PRIVADO", "Privado");
		radioPrivado.setChecked(true);
		tTipoGrupo.add(radioPrivado);
		Radio radioPrivadoOpcional = new Radio("PRIVADO_OPCIONAL", "Privado Opcional");
		tTipoGrupo.add(radioPrivadoOpcional);
		Radio radioAberto = new Radio("ABERTO", "Aberto");
		tTipoGrupo.add(radioAberto);
		Radio radioAbertoFechado = new Radio("ABERTO_FECHADO", "Aberto com aprovação");
		tTipoGrupo.add(radioAbertoFechado);
		
		if (!isCampoVisivel(campos, "tipoGrupo")) {
			tipoGrupo = false;
		}
		
		form.add(tTipoGrupo);
	
	
		if(grupo != null) {
			if(grupo.getTipoGrupo() == TipoGrupo.PRIVADO) {
				radioPrivado.setChecked(true);
			} else if(grupo.getTipoGrupo() == TipoGrupo.PRIVADO_OPCIONAL) {
				radioPrivadoOpcional.setChecked(true);
			} else if(grupo.getTipoGrupo() == TipoGrupo.ABERTO) {
				radioAberto.setChecked(true);
			} else if(grupo.getTipoGrupo() == TipoGrupo.ABERTO_FECHADO) {
				radioAbertoFechado.setChecked(true);
			} else {
				radioPrivado.setChecked(true);
			}
		}
		
		
		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"),  this, "salvar");
		tsalvar.setAttribute("class", "btn btn-primary min-width hidden");
		form.add(tsalvar);
		
		form.add(new Submit("novo", getMessage("novo.label"), this, "novo"));
		form.setLabelAlign("left");
	}

	@Override
	public void onGet() {
		super.onGet();
	}
	
	public boolean editar() {
		Long id = editarLink.getValueLong();
		Grupo grupo = grupoService.get(id);
		form.copyFrom(grupo);
		return true;
	
	}
	
	public boolean salvar() {
		if(form.isValid()) {
			boolean insert = id == null;
			Grupo g =  !insert ? grupoService.get(id) : new Grupo();
			
			if(!insert) {
				boolean paramVirtual = Boolean.valueOf(tVirtual.getValue());
				if(!g.isGrupoVirtual().equals(paramVirtual)) {
					
					if(g.isGrupoVirtual() && !g.getSubgrupos().isEmpty()) {
						return responseVirtual(paramVirtual);
					} else if(!g.isGrupoVirtual() && !g.getUsuarios().isEmpty()) {
						if(!somenteAdminNoGrupo(g)) {
							return responseVirtual(paramVirtual);
						}
					}
					
				}
			}
			
			form.copyTo(g);
			
			String tipoGrupoStr = tTipoGrupo.getValue();
			if(StringUtils.isNotEmpty(tipoGrupoStr)) {
				TipoGrupo tipoGrupo = TipoGrupo.valueOf(tipoGrupoStr);
				g.setTipoGrupo(tipoGrupo);
			}
			
			boolean existe = grupoService.existe(g, getEmpresa(), getUsuario());
			if(existe) {
				String grupoId = id != null ? String.valueOf(id) : "";
				setFlashAttribute("msg", "Já existe um grupo com este nome/codigo");
				setRedirect(GrupoPage.class,"id",grupoId);
				return false;
			}
			
			//verifica se os campos de horario foram preenchidos
			if((StringUtils.isEmpty(tHoraAbertura.getValue()) && StringUtils.isNotEmpty(tHoraFechamento.getValue()))
					|| (StringUtils.isNotEmpty(tHoraAbertura.getValue()) && StringUtils.isEmpty(tHoraFechamento.getValue()))) {
				setFlashAttribute("msg", "Um dos campos de horario não foi preenchido, por favor verifique!");
				setRedirect(GrupoPage.class,"id",String.valueOf(g.getId()));
				return false;
			}
			
			CategoriaPost categoriaPost = tCategoria.getEntity();
			g.setCategoria(categoriaPost);
			
			
			Date horaAbertura = null;
			if(tHoraAbertura != null) {
				horaAbertura = DateUtils.newDateByHoraMinutoSeg(tHoraAbertura.getValue());
			}
						
			Date horaFechamento = null; 
			if(tHoraFechamento != null) {
				horaFechamento = DateUtils.newDateByHoraMinutoSeg(tHoraFechamento.getValue());
			}
	
			g.setHoraAbertura(horaAbertura);
			g.setHoraFechamento(horaFechamento);
		    
			try {
//				boolean novo = g.getId() == null;
				
				boolean padrao = false;
				boolean virtual = false;
				
				padrao = g.isPadrao();
				g.setPadrao(padrao);

				virtual = g.isGrupoVirtual();
				g.setGrupoVirtual(virtual);
				
				if(virtual) {
					g.setTipoGrupo(null);
				} else {
					if(g.getTipoGrupo() == null && !tipoGrupo) {
						g.setTipoGrupo(TipoGrupo.PRIVADO);
					}
				}

				grupoService.saveOrUpdate(getUsuario(),g);
				usuarioService.addGrupoToUser(getUsuario(), g, true);

				if(id == null){
					setFlashAttribute("msg", getMessage("msg.grupo.salvar.sucess", g.getNome()));
				}else{
					setFlashAttribute("msg", getMessage("msg.grupo.atualizar.sucess", g.getNome()));
				}

				Map<String, String>  params = new HashMap<String, String>();
				params.put("id", String.valueOf(g.getId()));
				if(insert) {
					tab = "addUser";
					params.put("tab", tab);
				}
				setRedirect(GrupoPage.class, params);
				salvoComSucesso = 1;

			} catch (DomainMessageException e) {
				logError(e.getMessage(), e);
				setMessageError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(), e);
				setMessageError(getMessage("msg.grupo.salvar.error", e.getMessage()));
			}
		}
		return false;
	}

	private boolean responseVirtual(boolean paramVirtual) {
		String grupoId = id != null ? String.valueOf(id) : "";
		String tipo = paramVirtual ? "Grupo virtual. Remova seus usuarios antes!" : "Grupo comum. Remova seus subgrupos antes!";
		setFlashAttribute("msg", "Não é possivel converter o grupo para " + tipo);
		setRedirect(GrupoPage.class,"id",grupoId);
		return false;
	}

	public boolean novo() {
		
		setRedirect(GrupoPage.class);
		return false;
	}	

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Grupo grupo = grupoService.get(id);
			grupoService.delete(getUsuario(),grupo, false);
			setRedirect(getClass());
			setFlashAttribute("msg", getMessage("msg.grupo.excluir.sucess"));
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.grupo.excluir.error");
		} catch (DomainException e) {
			this.msgErro = e.getMessage();
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		if (grupo != null) {
			int maxSize = 24;

			usuarios = usuarioService.findUsuariosByGrupo(grupo, page, maxSize);
		}
	}
	
	public boolean somenteAdminNoGrupo(Grupo g) {
		if(!g.getUsuarios().isEmpty()) {
			if(g.getUsuarios().size() == 1) {
				GrupoUsuarios grupoUser = g.getUsuariosList().get(0);
				if(g.isAdmin(grupoUser.getUsuario())) {
					return true;
				} 
			}
			return false;
		}
		return true;
	}
}
