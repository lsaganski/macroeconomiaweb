package br.livetouch.livecom.domain.repository.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.ArquivoRepository;
import br.livetouch.livecom.domain.vo.ArquivoFiltro;
import net.livetouch.extras.util.DateUtils;

@Repository
public class ArquivoRepositoryImpl extends LivecomRepository<Arquivo> implements ArquivoRepository {

	public ArquivoRepositoryImpl() {
		super(Arquivo.class);
	}

	@Override
	public void deleteFrom(Post p) {
		Query q = getSession().createQuery("delete from Arquivo where post.id=?").setLong(0, p.getId());
		int i = q.executeUpdate();
		log.debug("Delete ["+i+"] arquivos from Post: " + p.getId());
	}

	@Override
	public void deleteFrom(Mensagem c) {
		Query q = getSession().createQuery("delete from Arquivo where mensagem.id=?").setLong(0, c.getId());
		int i = q.executeUpdate();
		log.debug("Delete ["+i+"] arquivos from Mensagem: " + c.getId());
	}

	@Override
	public void deleteFrom(Comentario c) {
		Query q = getSession().createQuery("delete from Arquivo where comentario.id=?").setLong(0, c.getId());
		int i = q.executeUpdate();
		log.debug("Delete ["+i+"] arquivos from Comentario: " + c.getId());
	}
	
	@Override
	public List<Long> findIdsByUser(Usuario u) {
		String hql = "select id from Arquivo where usuario.id = ? ";
		List<Long> list = queryIds(hql, true, u.getId());
		return list;
	}
	
	@Override
	public List<Long> findIdsArquivoRefs(Arquivo a) {
		String hql = "select id from Arquivo where arquivoRef.id = ? ";
		List<Long> list = queryIds(hql, false, a.getId());
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Arquivo> findAllByUser(Usuario usuario, String nome,List<Tag> tags, TipoBuscaArquivo tipoBusca, int page, int pageSize) {
		// Todos que o usuário possui acesso (grupos)
		StringBuffer sql = new StringBuffer("select distinct a from Arquivo a inner join a.usuario u ");

		boolean buscaTodos = tipoBusca.equals(TipoBuscaArquivo.TODOS_ARQUIVOS_QUE_USUARIO_POSSUI_ACESSO);
		if(buscaTodos) {
			// join com grupos de accesso
//			sql.append(" inner join a.post post inner join post.grupos g inner join g.usuarios gu ");
			sql.append(" inner join a.post post inner join post.grupos g ");
		}

		boolean searchTags = tags != null && tags.size() > 0;
		if(searchTags) {
			sql.append(" inner join fetch a.tags t ");
		}

		sql.append(" where 1=1 ");
		
		//StringBuffer sql = new StringBuffer("from Arquivo a where ");
		//a inner join a.tags tag where upper(a.nome) like :nome and tag.id like :tagsIds0_, :tagsIds1_]
		if(nome != null) {
			sql.append(" and upper(a.nome) like :nome");
		}
		
		if(searchTags) {
			sql.append(" or t.id in (:tagsIds)");
		}

		if(buscaTodos) {
			sql.append(" and g.id in (select gu.grupo.id from GrupoUsuarios gu where gu.usuario.id =:idUser)");
		} else {
			sql.append(" and u.id = :idUser");
		}
		sql.append(" AND a.arquivoRef is null");
		sql.append(" order by a.id desc");
		
		Query q = getSession().createQuery(sql.toString());
		q.setParameter("idUser", usuario.getId());
		
		if(nome != null) {
			q.setParameter("nome", "%" + nome.toUpperCase() + "%");
		}
		if(searchTags) {
			List<Long> tagsIds = Tag.getIds(tags);
			q.setParameterList("tagsIds", tagsIds);
		}
		if( pageSize > 0) {
			int firstResult = 0;
			if (page != 0) {
				firstResult = page * pageSize;
			}
			q.setFirstResult(firstResult);
			q.setMaxResults(pageSize);
		}
		
		q.setCacheable(true);
		
		
		List<Arquivo> list = q.list();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Arquivo> findAllByTipo(Usuario usuario, String[] tipoArquivo, TipoBuscaArquivo tipoBusca, int page, int pageSize, String nome) {
		// Todos que o usuário possui acesso (grupos)
		StringBuffer sql = new StringBuffer("select distinct a from Arquivo a");

		boolean buscaTodos = tipoBusca.equals(TipoBuscaArquivo.TODOS_ARQUIVOS_QUE_USUARIO_POSSUI_ACESSO);
		
		// join com grupos de accesso
		sql.append(" left join a.post post left join post.grupos g");
		
		sql.append(" left join a.mensagem m");

		sql.append(" where 1=1 ");
		
		if(tipoArquivo != null) {
			sql.append(" and upper(a.mimeType) in :tipo");
		}
		
		if(buscaTodos) {
			sql.append(" AND ((m.from.id = :idUser OR m.to.id = :idUser OR a.usuario.id = :idUser) OR g.id in (select gu.grupo.id from GrupoUsuarios gu where gu.usuario.id =:idUser)) ");
		} else {
			sql.append(" AND a.usuario.id = :idUser ");
		}
		
		if(nome != null) {
			sql.append(" and upper(a.nome) like :nome");
		}
		
		sql.append(" order by a.id desc");
		
		Query q = getSession().createQuery(sql.toString());
		q.setParameter("idUser", usuario.getId());
		
		if(tipoArquivo != null) {
			q.setParameterList("tipo", tipoArquivo);
		}
		if(nome != null) {
			q.setParameter("nome", "%" + nome.toUpperCase() + "%");
		}
		

		if( pageSize > 0) {
			int firstResult = 0;
			if (page != 0) {
				firstResult = page * pageSize;
			}
			q.setFirstResult(firstResult);
			q.setMaxResults(pageSize);
		}
		
		q.setCacheable(true);
		
		
		List<Arquivo> list = q.list();
		
		return list;
	}
	
	public enum TipoBuscaArquivo {
		POSTADOS_PELO_USUARIO,
		TODOS_ARQUIVOS_QUE_USUARIO_POSSUI_ACESSO,
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Arquivo> findAllByKeys(List<? extends Serializable> ids) {
		return createCriteria().add(Restrictions.in("id", ids)).setCacheable(true).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Arquivo> findAllByUser(ArquivoFiltro arquivoFiltro) {
		Usuario usuario = arquivoFiltro.usuario;
		Boolean buscaTodos = arquivoFiltro.todos;
		List<Tag> tags = arquivoFiltro.tags;
		boolean searchTags = tags != null &&  tags.size() > 0;
		
		String ordem = arquivoFiltro.ordem;
		
		Date dataInicio = null;
		Date dataFim = null;

		if (StringUtils.isNotEmpty(arquivoFiltro.start)) {
			arquivoFiltro.setDataInicial(DateUtils.toDate(arquivoFiltro.start));
		}
		if (StringUtils.isNotEmpty(arquivoFiltro.end)) {
			arquivoFiltro.setDataFinal(DateUtils.toDate(arquivoFiltro.end));
		}
		
		if (arquivoFiltro.getDataInicial() != null) {
			dataInicio = DateUtils.setTimeInicioDia(arquivoFiltro.getDataInicial());
		}
		if (arquivoFiltro.getDataFinal() != null) {
			dataFim = DateUtils.setTimeFimDia(arquivoFiltro.getDataFinal());
		}
		
		StringBuffer sql = new StringBuffer("select distinct a from Arquivo a ");
		sql.append(" left join a.post post ");
		if(buscaTodos || arquivoFiltro.grupos != null) {
			sql.append(" left join post.grupos g ");
		}
		
		sql.append(" left join a.mensagem m");
		

		if(searchTags) {
			sql.append(" left join fetch a.tags t ");
		}

		sql.append(" where 1=1 ");
		
		if(StringUtils.isNotEmpty(arquivoFiltro.texto)) {
			sql.append(" and ( upper(a.nome) like :nome or a.usuario.nome like :nome OR post.titulo like :nome)");
		}
		
		if(searchTags) {
			sql.append(" AND t in (:tags)");
		}

		if(buscaTodos) {
			sql.append(" AND ((m.from.id = :idUser OR m.to.id = :idUser OR a.usuario.id = :idUser) OR g.id in (select gu.grupo.id from GrupoUsuarios gu where gu.usuario.id =:idUser)) ");
		} else {
			sql.append(" AND a.usuario.id = :idUser ");
		}
		
		if(arquivoFiltro.grupos != null) {
			sql.append(" AND g in (:grupos)");
		}
		
		if(StringUtils.isNotEmpty(arquivoFiltro.extensao)) {
			sql.append(" AND a.nome like :extensao");
		}
		
		sql.append(" AND a.arquivoRef is null");
		
		if (dataInicio != null) {
			sql.append(" and (a.dataCreate >= :dataIni or a.dataUpdate >= :dataIni)");
		}

		if (dataFim != null) {
			sql.append(" and (a.dataCreate <= :dataFim or a.dataUpdate >= :dataIni)");
		}
		
		if(StringUtils.isEmpty(ordem)) {
			sql.append(" order by a.id desc");
		} else {
			sql.append(" order by a." + ordem + " asc");
		}
		
		Query q = getSession().createQuery(sql.toString());
		q.setParameter("idUser", usuario.getId());
		
		if (dataInicio != null) {
			q.setParameter("dataIni", dataInicio);
		}

		if (dataFim != null) {
			q.setParameter("dataFim", dataFim);
		}
		
		if(arquivoFiltro.grupos != null) {
			q.setParameterList("grupos", arquivoFiltro.grupos);
		}
		
		if(StringUtils.isNotEmpty(arquivoFiltro.texto)) {
			q.setParameter("nome", "%" + arquivoFiltro.texto.toUpperCase() + "%");
		}
		if(searchTags) {
			q.setParameterList("tags", tags);
		}
		
		if(StringUtils.isNotEmpty(arquivoFiltro.extensao)) {
			q.setParameter("extensao", "%." + arquivoFiltro.extensao);
		}
		
		Integer pageSize = arquivoFiltro.pageSize;
		Integer page = arquivoFiltro.page;
		if(page != null) {
			if( pageSize != null && pageSize > 0) {
				int firstResult = 0;
				if (page != 0) {
					firstResult = page * pageSize;
				}
				q.setFirstResult(firstResult);
				q.setMaxResults(pageSize);
			}
		}
		
		q.setCacheable(true);
		List<Arquivo> list = q.list();
		return list;
	}

	@Override
	public List<Arquivo> findAll(Empresa e) {
		if(e != null) {
			return query("from Arquivo where empresa.id=?",false, e.getId());
		} else {
			return query("from Arquivo",false);
		}
	}

	@Override
	public List<Arquivo> findAllByTag(Tag tag) {
		String hql = "select a from Arquivo a inner join a.tags tag where tag.id=?";
		List<Arquivo> list = query(hql,false, tag.getId());
		return list;
	}

	@Override
	public List<Long> findIdsByComentarios(Comentario c) {
		Long id = c.getId();
		String hql = "select id from Arquivo where comentario.id=?";
		List<Long> list = queryIds(hql,false,id);
		return list;
	}

	@Override
	public ArquivoThumb find(Long id) {
		String hql = "from ArquivoThumb where id=:id";
		Query q = createQuery(hql);
		q.setParameter("id", id);
		return (ArquivoThumb) q.uniqueResult();
	}

	@Override
	public void updateFileDestaquePost(Post post) {
		Query q = createQuery("update Arquivo a a.destaque = 0 where a.post.id = :id");
		q.setParameter("id", post.getId());
		q.executeUpdate();
	}

	@Override
	public void deleteNotInCategorias(List<Long> arquivosIds, CategoriaPost categoria) {
		executeIn("delete from ArquivoThumb a where a.arquivo.id in (select id from Arquivo where categoria.id = "+ categoria.getId() +" and id not in(:ids))", "ids",arquivosIds);
		executeIn("delete from Arquivo a where a.categoria.id = "+ categoria.getId() +" and a.id not in(:ids)", "ids",arquivosIds);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getIdsArquivosNotIn(Post p, List<Long> arquivoIds) {
		
		if(p != null && 0 >= p.getId()) {
			return null;
		}
		
		StringBuffer sb = new StringBuffer("SELECT id FROM Arquivo a where ");
		sb.append(" a.post.id = :postId ");
		if(arquivoIds != null && arquivoIds.size() > 0) {
			sb.append(" AND a.id not in(:ids) ");			
		}
		
		Query q = createQuery(sb.toString());
		q.setParameter("postId", p.getId());
		if(arquivoIds != null && arquivoIds.size() > 0) {
			q.setParameterList("ids", arquivoIds);
		}
		return q.list();
	}
}