package br.livetouch.livecom.web.pages.configuracoes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.ArquivoThumb;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.StatusUsuario;
import br.livetouch.livecom.domain.exception.DomainMessageException;
import br.livetouch.livecom.domain.vo.CamposMap;
import br.livetouch.livecom.domain.vo.ThumbVO;
import br.livetouch.livecom.domain.vo.UploadResponse;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import br.livetouch.livecom.utils.UploadHelper;
import br.livetouch.livecom.web.pages.LivecomRootEmpresaPage;
import br.livetouch.livecom.web.pages.pages.MuralPage;
import br.livetouch.livecom.web.pages.root.EmpresasPage;
import br.livetouch.pushserver.lib.PushNotification;
import br.livetouch.pushserver.lib.UsuarioToPush;
import net.livetouch.click.control.IdField;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Checkbox;
import net.sf.click.control.Field;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

@Controller
@Scope("prototype")
public class IndexPage extends LivecomRootEmpresaPage{

	public Empresa empresa;
	
	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", this, "editar");
	public Link deleteLink = new Link("deletar", this, "deletar");
	public String msgErro;
	public String id;
	
	/**
	 * Usada para trazer as tab selecionadas
	 */
	public String tab1;
	public String tab2;
	
	/**
	 * Se o web service enviar este id, atualiza a foto com o arquivo.
	 */
	public Long arquivoFotoId;
	public Long arquivoLogoId;
	public Long arquivoBgId;
	public Long arquivoFavId;
	public Long arquivoMuralCapaId;
	public Long arquivoCapaGrupoId;

	//Dados da Empresa
	public TextField tNome;
	public Usuario usuario;
	
	//Dados do Admin
	public TextField tNomeUsuario;
	public TextField tEmail;
	
	public Map<String, Boolean> paramDisplayTable = new HashMap<>();

	public int page;
	
	public CamposMap campos;
	
	public List<Parametro> params;

	
	@Override
	public void onInit() {
		super.onInit();
		UserInfoVO userInfoVO = getUserInfoVO();
		if(!userInfoVO.isRootEmpresa()) {
			setRedirect(MuralPage.class);
		}
		
		empresa = empresaService.get(userInfoVO.getEmpresaId());
		if(empresa == null) {
			setRedirect(MuralPage.class);
		}
		id = empresa.getId().toString();
		
		
		form();
		campos = getCampos();
		setParamDisplayTable();
		
		tab1 = getParam("tab1");
		tab2 = getParam("tab2");
		
		Long id = null;
		if(StringUtils.isNotEmpty(this.id) && StringUtils.isNumeric(this.id)) {
			id = Long.parseLong(this.id);
		}
		
		if (id != null) {
			empresa = empresaService.get(id);

			if (empresa != null && empresa.getAdmin() != null) {
				usuario = empresa.getAdmin();
				tNome.setValue(usuario.getNome());
				form.copyFrom(usuario);
				
			}
			
			params = parametroService.findAllByEmpresa(empresa.getId());
			/**Layout
			 */
			setText(Params.LAYOUT_BACKGROUD);
			setText(Params.LAYOUT_LOGO);
			setText(Params.LAYOUT_FAVICON);
			setText(Params.LAYOUT_CAPA_MURAL);
			setText(Params.FOTO_USUARIO_DEFAULT);
			setText(Params.LAYOUT_THUMB_GRUPO);
			setText(Params.COR_MENU);
			setText(Params.COR_BUSCA);
			setText(Params.COR_BADGE_SPAN);
			setText(Params.COR_BADGE_TEXT);
			setText(Params.WEB_TEMPLATE_TITLE);
			setText(Params.BORDER_COPYRIGHT_EMPRESA);
			
			/** PARAMETROS
			 * 
			 * Chat
			 */
			setChecked(Params.CHAT_ON);
			
			/**
			 * Controle Versao
			 */
			setText(Params.VERSION_CONTROL_ANDROID_NUMBER);
			setText(Params.VERSION_CONTROL_ANDROID_MENSAGEM);
			setText(Params.VERSION_CONTROL_ANDROID_URLDOWNLOAD);
			setText(Params.VERSION_CONTROL_ANDROID_MODE);
			
			/**
			* Download de Aplicativos
			*/
			setText(Params.BUILD_BETA_ANDROID_URL);
			setText(Params.BUILD_APP_ANDROID_URL);
			setText(Params.BUILD_APP_IOS_URL);
			
			/**
			* Email
			*/
			setText(Params.EMAIL_EMAIL_ADMIN);
			setText(Params.SERVER_HOST);
			setText(Params.EMAIL_SMTP_PORT);
			setText(Params.EMAIL_SMTP_USER);
			setText(Params.EMAIL_SMTP_PASSWORD);
			setChecked(Params.EMAIL_SMTP_SSL);
			
			/**
			* Excluir Regras
			*/
			setChecked(Params.DELETAR_TAG_R1_ON);
			setChecked(Params.DELETAR_TAG_R2_ON);
			setChecked(Params.DELETAR_TAG_C1_ON);
			setChecked(Params.DELETAR_CATEGORIA_R2_ON);
			setChecked(Params.DELETAR_CATEGORIA_C1_ON);
			setChecked(Params.DELETAR_CATEGORIA_C2_ON);
			setChecked(Params.DELETAR_GRUPO_R2_ON);
			setChecked(Params.DELETAR_GRUPO_R3_ON);
			setChecked(Params.DELETAR_GRUPO_R4_ON);
			setChecked(Params.DELETAR_GRUPO_R5_ON);
			setChecked(Params.DELETAR_GRUPO_C1_ON);
			setChecked(Params.DELETAR_GRUPO_C2_ON);
			setChecked(Params.DELETAR_GRUPO_C3_ON);
			setChecked(Params.DELETAR_GRUPO_C4_ON);
			setChecked(Params.DELETAR_GRUPO_C5_ON);
			setChecked(Params.DELETAR_USUARIO_R2_ON);
			setChecked(Params.DELETAR_USUARIO_R3_ON);
			setChecked(Params.DELETAR_USUARIO_R4_ON);
			setChecked(Params.DELETAR_USUARIO_C1_ON);
			setChecked(Params.DELETAR_USUARIO_C2_ON);
			setChecked(Params.DELETAR_AREA_R1_ON);
			setChecked(Params.DELETAR_DIRETORIA_R1_ON);
			setChecked(Params.DELETAR_DIRETORIA_R2_ON);
			setChecked(Params.DELETAR_DIRETORIA_R3_ON);
			
			/**
			* LDAP
			*/
			setText(Params.LDAP_PROVIDER_URL);
			setText(Params.LDAP_SEARCH_BASE);
			setText(Params.LDAP_SECURITY_PRINCIPAL);
			setText(Params.LDAP_SECURITY_PASSWD);
			setText(Params.LDAP_SEARCH_FILTER);
			
			/**
			* Loguin
			*/
			setChecked(Params.LOGIN_CONNECTOR_ON);
			setText(Params.LOGIN_CONNECTOR_RECUPERAR_SENHA);
			
			/**
			* Mural
			*/
			setChecked(Params.MURAL_SIDEBOX_ON);
			setChecked(Params.MURAL_POST_TITULO_OBRIGATORIO_ON);
			setChecked(Params.MURAL_TAG_ON);
			setChecked(Params.MURAL_CHAPEU_ON);
			setChecked(Params.MURAL_RESUMO_ON);
			setChecked(Params.MURAL_HORA_ON);
			setChecked(Params.MURAL_RASCUNHO_ON);
			setChecked(Params.MURAL_ENVIAR_ARQUIVOS_ON);
			setChecked(Params.MURAL_POST_DESTAQUE_ON);
			setChecked(Params.MURAL_GRUPO_HEADER_ON);
			
			/**
			* Push
			*/
			setChecked(Params.PUSH_ON);
			setChecked(Params.PUSH_CHECKBOXON);
			setChecked(Params.PUSH_CHECKBOX_CHECKED);
			setChecked(Params.PUSH_POST_AUTOR_RECEBE_ON);
			setChecked(Params.PUSH_COMENTARIO_AUTOR_RECEBE_ON);
			setText(Params.MSG_PUSH_NEWPOST);
			setText(Params.PUSH_SERVER_PROJECT);
			setText(Params.PUSH_SERVER_HOST);
			
			/**
			* Push Menssseges
			*/
			setText(Params.PUSH_COMENTARIO_TITULO_NOTIFICATION);
			setText(Params.PUSH_COMENTARIO_SUB_TITULO_NOTIFICATION);
			setText(Params.PUSH_FAVORITO_TITULO_NOTIFICATION);
			setText(Params.PUSH_FAVORITO_SUB_TITULO_NOTIFICATION);
			setText(Params.PUSH_COMUNICADO_NEW_TITULO_NOTIFICATION);
			setText(Params.PUSH_COMUNICADO_NEW_SUB_TITULO_NOTIFICATION);
			setText(Params.PUSH_COMUNICADO_UPDATE_TITULO_NOTIFICATION);
			setText(Params.PUSH_COMUNICADO_UPDATE_SUB_TITULO_NOTIFICATION);
			setText(Params.PUSH_LIKE_POST_TITULO_NOTIFICATION);
			setText(Params.PUSH_LIKE_POST_SUB_TITULO_NOTIFICATION);
			setText(Params.PUSH_LIKE_COMENTARIO_TITULO_NOTIFICATION);
			setText(Params.PUSH_LIKE_COMENTARIO_SUB_TITULO_NOTIFICATION);
			
			
			/**
			* Badges
			*/
			setChecked(Params.PUSH_BADGES_POST_ON);
			setChecked(Params.PUSH_BADGES_FAVORITO_ON);
			setChecked(Params.PUSH_BADGES_LIKE_ON);
			setChecked(Params.PUSH_BADGES_COMENTARIO_ON);
			setChecked(Params.PUSH_BADGES_CHAT_ON);
			
			/**
			* Restrições de Horario
			*/
			setCheckdGruposUsuarios(Params.RESTRICAO_HORARIO_MODO);
			setChecked(Params.USUARIO_RESTRICAO_HORARIO_ON);
			setChecked(Params.GRUPO_RESTRICAO_HORARIO_ON);
			
			/**
			* Senha
			*/
			setChecked(Params.USER_SENHA_PADRAO_ON);
			setText(Params.USER_SENHA_PADRAO);
			
			/**
			* Web
			*/
			setText(Params.WEB_TEMPLATE_TITLE);
			setText(Params.BORDER_COPYRIGHT_EMPRESA);
			//setText(Params.FOTO_USUARIO_DEFAULT);
			setChecked(Params.POST_CENSURA_ON);
			setChecked(Params.VALIDA_EMAIL_UNICO_ON);
			setChecked(Params.VALIDA_LOGIN_UNICO_ON);
			
			/**
			* Relatorios
			*/
			setChecked(Params.EXPORTAR_CSV);
			setText(Params.IMPORTACAO_ARQUIVO_CHARSET);
			setText(Params.EXPORTACAO_ARQUIVO_CHARSET);
			
			/**
			* Menu
			*/
			setChecked(Params.MENU_DINAMICO_ON);
			
			form.copyFrom(empresa);
		}
	}

	
	private void setCheckdGruposUsuarios(String name){
		
		ParametrosMap param = ParametrosMap.getInstance(empresa.getId());
		String value = param.get(name);
		Checkbox check = getCheckSimNao(name);
		check.setChecked("1".equals(value));
		check.setChecked("grupo".equals(param.get(Params.RESTRICAO_HORARIO_MODO)));
		
	}
	
	private void setChecked(String name) {
		ParametrosMap param = ParametrosMap.getInstance(empresa.getId());
		String value = param.get(name);
		Checkbox check = getCheckSimNao(name);
		check.setChecked("1".equals(value));
	}
	
	private void setText(String name) {
		ParametrosMap param = ParametrosMap.getInstance(empresa.getId());
		String value = param.get(name);
		TextField t = getText(name);
		t.setValue(value);
	}
	
	public void form() {
		form.add(new IdField(null));
		
		/**
		 * Form Dados Empresa
		 */
		form.add(getText("nome"));
		form.add(getText("codigo"));
		form.add(getText("dominio"));

		Submit tsalvar = new Submit("salvar", this, "salvar");
		tsalvar.setAttribute("class", "btn btn-primary min-width");
		form.add(tsalvar);

		if (id != null) {
			Submit tDeletar = new Submit("deletar", this, "deletar");
			tDeletar.setAttribute("class", "btn btn-outline red-thunderbird min-width");
			form.add(tDeletar);
		}
		Submit cancelar = new Submit("cancelar", this, "cancelar");
		cancelar.setAttribute("class", "btn btn-default min-width");
		form.add(cancelar);
/*		
		
		/**
		 * Dados do admin
		 */
		tNome = getText("nomeUsuario");
		form.add(tNome);
		tEmail = getText("email");
		tEmail.setDisabled(true);
		form.add(tEmail);

		tNome.setFocus(true);
		
		//ADICIONANDO NO FORM
		/**Layout
		 *
		 */
		form.add(getText(Params.LAYOUT_BACKGROUD));
		form.add(getText(Params.LAYOUT_LOGO));
		form.add(getText(Params.LAYOUT_FAVICON));
		form.add(getText(Params.LAYOUT_CAPA_MURAL));
		form.add(getText(Params.FOTO_USUARIO_DEFAULT));
		form.add(getText(Params.LAYOUT_THUMB_GRUPO));
		form.add(getTextCores(Params.COR_MENU));
		form.add(getTextCores(Params.COR_BUSCA));
		form.add(getTextCores(Params.COR_BADGE_SPAN));
		form.add(getTextCores(Params.COR_BADGE_TEXT));
		form.add(getText(Params.WEB_TEMPLATE_TITLE));
		form.add(getText(Params.BORDER_COPYRIGHT_EMPRESA));
		
		/**	PARAMETROS
		 * 
		 * Chat
		 */
		form.add(getCheckSimNao(Params.CHAT_ON));
		
		
		/**
		* Download de Aplicativos
		*/
		form.add(getText(Params.BUILD_BETA_ANDROID_URL));
		form.add(getText(Params.BUILD_APP_ANDROID_URL));
		form.add(getText(Params.BUILD_APP_IOS_URL));
		
		/**
		* Email
		*/
		form.add(getText(Params.EMAIL_EMAIL_ADMIN));
		form.add(getText(Params.SERVER_HOST));
		form.add(getText(Params.EMAIL_SMTP_PORT));
		form.add(getText(Params.EMAIL_SMTP_USER));
		form.add(getText(Params.EMAIL_SMTP_PASSWORD));
		form.add(getCheckSimNao(Params.EMAIL_SMTP_SSL));
		
		/**
		* Excluir Regras
		*/
		form.add(getCheckSimNao(Params.DELETAR_TAG_R1_ON));
		form.add(getCheckSimNao(Params.DELETAR_TAG_R2_ON));
		form.add(getCheckSimNao(Params.DELETAR_TAG_C1_ON));
		form.add(getCheckSimNao(Params.DELETAR_CATEGORIA_R2_ON));
		form.add(getCheckSimNao(Params.DELETAR_CATEGORIA_C1_ON));
		form.add(getCheckSimNao(Params.DELETAR_CATEGORIA_C2_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_R2_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_R3_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_R4_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_R5_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_C1_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_C2_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_C3_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_C4_ON));
		form.add(getCheckSimNao(Params.DELETAR_GRUPO_C5_ON));
		form.add(getCheckSimNao(Params.DELETAR_USUARIO_R2_ON));
		form.add(getCheckSimNao(Params.DELETAR_USUARIO_R3_ON));
		form.add(getCheckSimNao(Params.DELETAR_USUARIO_R4_ON));
		form.add(getCheckSimNao(Params.DELETAR_USUARIO_C1_ON));
		form.add(getCheckSimNao(Params.DELETAR_USUARIO_C2_ON));
		form.add(getCheckSimNao(Params.DELETAR_AREA_R1_ON));
		form.add(getCheckSimNao(Params.DELETAR_DIRETORIA_R1_ON));
		form.add(getCheckSimNao(Params.DELETAR_DIRETORIA_R2_ON));
		form.add(getCheckSimNao(Params.DELETAR_DIRETORIA_R3_ON));
		
		/**
		* LDAP
		*/
		form.add(getText(Params.LDAP_PROVIDER_URL));
		form.add(getText(Params.LDAP_SEARCH_BASE));
		form.add(getText(Params.LDAP_SECURITY_PRINCIPAL));
		form.add(getText(Params.LDAP_SECURITY_PASSWD));
		form.add(getText(Params.LDAP_SEARCH_FILTER));
		
		/**
		* Loguin
		*/
		form.add(getCheckSimNao(Params.LOGIN_CONNECTOR_ON));
		form.add(getText(Params.LOGIN_CONNECTOR_RECUPERAR_SENHA));
		
		/**
		* Mural
		*/
		form.add(getCheckSimNao(Params.MURAL_SIDEBOX_ON));
		form.add(getCheckSimNao(Params.MURAL_POST_TITULO_OBRIGATORIO_ON));
		form.add(getCheckSimNao(Params.MURAL_TAG_ON));
		form.add(getCheckSimNao(Params.MURAL_CHAPEU_ON));
		form.add(getCheckSimNao(Params.MURAL_RESUMO_ON));
		form.add(getCheckSimNao(Params.MURAL_HORA_ON));
		form.add(getCheckSimNao(Params.MURAL_RASCUNHO_ON));
		form.add(getCheckSimNao(Params.MURAL_ENVIAR_ARQUIVOS_ON));
		form.add(getCheckSimNao(Params.MURAL_POST_DESTAQUE_ON));
		form.add(getCheckSimNao(Params.MURAL_GRUPO_HEADER_ON));
		
		/**
		* Push
		*/
		form.add(getCheckSimNao(Params.PUSH_ON));
		form.add(getCheckSimNao(Params.PUSH_CHECKBOXON));
		form.add(getCheckSimNao(Params.PUSH_CHECKBOX_CHECKED));
		form.add(getCheckSimNao(Params.PUSH_POST_AUTOR_RECEBE_ON));
		form.add(getCheckSimNao(Params.PUSH_COMENTARIO_AUTOR_RECEBE_ON));
		form.add(getText(Params.MSG_PUSH_NEWPOST));
		form.add(getText(Params.PUSH_SERVER_PROJECT));
		form.add(getText(Params.PUSH_SERVER_HOST));
		
		
		
		
		/**
		* Senha
		*/
		form.add(getCheckSimNao(Params.USER_SENHA_PADRAO_ON, "Padrão", "Randomica"));
		form.add(getText(Params.USER_SENHA_PADRAO));
		
	}

	private Field getCheckSimNao(String name, String labelSim, String labelNao) {
		name = StringUtils.replace(name, ".", "_");
		Checkbox check = (Checkbox) form.getField(name);
		if(check != null) {
			return check;
		}
		check = new Checkbox(name);
		check.setAttribute("class", "bootstrap");
		check.setAttribute("data-off-color", "danger");
		check.setAttribute("data-off-text", labelNao);
		check.setAttribute("data-on-text", labelSim);
		return check;
	}

	// Set atributos Check padrão
	private Checkbox getCheckSimNao(String name) {
		name = StringUtils.replace(name, ".", "_");
		Checkbox check = (Checkbox) form.getField(name);
		if(check != null) {
			return check;
		}
		check = new Checkbox(name);
		check.setAttribute("class", "bootstrap");
		check.setAttribute("data-off-color", "danger");
		check.setAttribute("data-off-text", "Não");
		check.setAttribute("data-on-text", "Sim");
		return check;
	}
	
	// Set atributos do TextField para seleção de cores
	private TextField getTextCores(String name){
		
		Integer maxLength = 255;

		name = StringUtils.replace(name, ".", "_");
		TextField t = (TextField) form.getField(name);
		if(t != null) {
			return t;
		}
		t = new TextField(name);
		t.setAttribute("class", "form-control minicolors-input");
		t.setAttribute("data-control", "hue");
		t.setAttribute("size", "7");
		t.setMaxLength(maxLength);
		return t;
	}
	
	
	// Set atributos TextField Padrão
	private TextField getText(String name) {
		String classInput = "form-control input-sm";
		String classInputIgnore = classInput + " ignore";
		Integer maxLength = 255;

		name = StringUtils.replace(name, ".", "_");
		TextField t = (TextField) form.getField(name);
		if(t != null) {
			return t;
		}
		t = new TextField(name);
		t.setAttribute("class", classInputIgnore);
		t.setMaxLength(maxLength);
		
		if(StringUtils.equalsIgnoreCase(name, "nome")) {
			t.setFocus(true);
		}
		return t;
	}

	@Override
	public void onGet() {
		super.onGet();
		
	}

	public boolean salvar() throws DomainException, IOException, EmailException {
		if (form.isValid()) {
			boolean salvo = execSalvar();

			if (salvo && usuario != null && usuario.getStatus().equals(StatusUsuario.NOVO)) {
				String senha = usuarioService.createPassword(usuario);
			
				Map<String, Object> map = new HashMap<>();
				map.put("senha", senha);
				if(empresa !=  null) {
					map.put("dominio", empresa.getDominio());
				}
				
				UsuarioToPush usuarioToPush = new UsuarioToPush();
				usuarioToPush.setCodigo(usuario.getLogin());
				usuarioToPush.setNome(usuario.getNome());
				usuarioToPush.setEmail(usuario.getEmail());
				usuarioToPush.setIdLivecom(usuario.getId());
				usuarioToPush.setParams(map);
				
				PushNotification not = new PushNotification();
				not.setUsuario(usuarioToPush);
				not.setIsEmail(true);
			}

			return salvo;
		} else {
			msgErro = error(form);
		}

		return true;
	}

	public boolean execSalvar() {
		if (form.isValid()) {
			try {
				Long id = null;
				if(StringUtils.isNotEmpty(this.id) && StringUtils.isNumeric(this.id)) {
					id = Long.parseLong(this.id);
				}
				empresa = id != null ? empresaService.get(id) : new Empresa();
				form.copyTo(empresa);

				String nome = tNome.getValue();
				List<Parametro> parametros  = new ArrayList<Parametro>();
				
				
				//ADICIONANDO PARAMETRO
				/**Layout
				 *
				 */
				addTextParam(parametros, Params.LAYOUT_BACKGROUD);
				addTextParam(parametros, Params.LAYOUT_LOGO);
				addTextParam(parametros, Params.LAYOUT_FAVICON);
				addTextParam(parametros, Params.LAYOUT_CAPA_MURAL);
				addTextParam(parametros, Params.FOTO_USUARIO_DEFAULT);
				addTextParam(parametros, Params.LAYOUT_THUMB_GRUPO);
				addTextParam(parametros, Params.COR_MENU);
				addTextParam(parametros, Params.COR_BUSCA);
				addTextParam(parametros, Params.COR_BADGE_SPAN);
				addTextParam(parametros, Params.COR_BADGE_TEXT);
				addTextParam(parametros, Params.WEB_TEMPLATE_TITLE);
				addTextParam(parametros, Params.BORDER_COPYRIGHT_EMPRESA);
				
				/** PARAMETROS
				 * 
				 * CHAT
				 */
				addCheckParam(parametros,Params.CHAT_ON);
				
				
				/**
				* Email
				*/
				addTextParam(parametros, Params.EMAIL_EMAIL_ADMIN);
				addTextParam(parametros, Params.SERVER_HOST);
				addTextParam(parametros, Params.EMAIL_SMTP_PORT);
				addTextParam(parametros, Params.EMAIL_SMTP_USER);
				addTextParam(parametros, Params.EMAIL_SMTP_PASSWORD);
				addCheckParam(parametros, Params.EMAIL_SMTP_SSL);
				
				
				
				/**
				* Mural
				*/
				addCheckParam(parametros, Params.MURAL_SIDEBOX_ON);
				addCheckParam(parametros, Params.MURAL_POST_TITULO_OBRIGATORIO_ON);
				addCheckParam(parametros, Params.MURAL_TAG_ON);
				addCheckParam(parametros, Params.MURAL_CHAPEU_ON);
				addCheckParam(parametros, Params.MURAL_RESUMO_ON);
				addCheckParam(parametros, Params.MURAL_HORA_ON);
				addCheckParam(parametros, Params.MURAL_RASCUNHO_ON);
				addCheckParam(parametros, Params.MURAL_ENVIAR_ARQUIVOS_ON);
				addCheckParam(parametros, Params.MURAL_POST_DESTAQUE_ON);
				addCheckParam(parametros, Params.MURAL_GRUPO_HEADER_ON);
				
				/**
				* Push
				*/
				addCheckParam(parametros, Params.PUSH_ON);
				addCheckParam(parametros, Params.PUSH_CHECKBOXON);
				addCheckParam(parametros, Params.PUSH_CHECKBOX_CHECKED);
				addCheckParam(parametros, Params.PUSH_POST_AUTOR_RECEBE_ON);
				addCheckParam(parametros, Params.PUSH_COMENTARIO_AUTOR_RECEBE_ON);
				addTextParam(parametros, Params.MSG_PUSH_NEWPOST);
				addTextParam(parametros, Params.PUSH_SERVER_PROJECT);
				addTextParam(parametros, Params.PUSH_SERVER_HOST);
				
				
				
				
				/**
				* Senha
				*/
				addCheckParam(parametros, Params.USER_SENHA_PADRAO_ON);
				addTextParam(parametros, Params.USER_SENHA_PADRAO);
				
								
				usuario.setNome(nome);

				setLogoEmpresa();
				setBgEmpresa();
				setFavicon();

				empresaService.saveOrUpdate(empresa, usuario, getUsuario(), parametros);

				setFlashAttribute("msg", "Dados atualizados com sucesso.");
				if(StringUtils.isEmpty(tab1) && StringUtils.isEmpty(tab2)) {
					setRedirect(IndexPage.class);
				} else {
					Map<String, Object> params = new HashMap<>();
					params.put("tab1", tab1);
					if(StringUtils.isNotEmpty(tab2)) {
						params.put("tab2", tab2);
					}
					setRedirect(IndexPage.class, params);
				}
				

				return true;

			} catch (Exception e) {
				logError(e.getMessage(), e);
				form.setError(e.getMessage());
			}
		}
		return false;
	}

	private void addTextParam(List<Parametro> parametros, String name) {
		TextField t = getText(name);
		
		Parametro p = new Parametro();
		p.setNome(name);
		p.setValor(t.getValue());
		parametros.add(p);
	}

	private void addCheckParam(List<Parametro> parametros,String name) {
		Checkbox check = getCheckSimNao(name);
		Parametro p = new Parametro();
		p.setNome(name);
		p.setValor(check.isChecked() ? "1" : "0");
		parametros.add(p);
		
	}
	
	

	private void setFavicon() throws FileNotFoundException, DomainMessageException, IOException {
		if(arquivoFavId != null) {
			Arquivo a = arquivoService.get(arquivoFavId);

			if (a != null) {
				UploadResponse info = UploadHelper.uploadFile(usuario, a, "fav", new Date().getTime());
				String url = info.arquivo.getUrl();
				empresa.setUrlFav(url);
			}
		}
	}
	
	private void setLogoEmpresa() throws IOException {
		if(arquivoLogoId != null) {
			Arquivo a = arquivoService.get(arquivoLogoId);
			if (a != null) {
				ArquivoThumb t = createThumbsArquivoLogo(a);
				empresa.setUrlLogo(t.getUrl());
			} 
		}
	}
	
	private void setBgEmpresa() throws DomainMessageException, FileNotFoundException, IOException {
		if(arquivoBgId != null) {
			Arquivo a = arquivoService.get(arquivoBgId);

			if (a != null) {
				UploadResponse info = UploadHelper.uploadFile(usuario, a, "bg", new Date().getTime());
				empresa.setUrlBg(info.arquivo.getUrl());
			}
		}
	}
	
	public boolean cancelar() {
		setRedirect(EmpresasPage.class);
		return true;
	}

	protected Arquivo createThumbsArquivoFoto(Arquivo a) throws IOException {

		UploadHelper.log.debug("Arquivo: " + a.getId());

		UploadHelper.log.debug("Gerando thumbs arquivo: " + a.getNome());
		ThumbVO voThumb = UploadHelper.createThumbFotoUsuario(getParametrosMap(), usuario.getChave() + "/fotoProfile", a.getUrl(),
				a.getNome());
		ArquivoThumb t = new ArquivoThumb();
		t.setThumbVo(voThumb);
		t.setArquivo(a);
		arquivoService.saveOrUpdate(t);

		return a;
	}

	protected ArquivoThumb createThumbsArquivoLogo(Arquivo a) throws IOException {
		
		UploadHelper.log.debug("Arquivo: " + a.getId());
		
		UploadHelper.log.debug("Gerando thumbs arquivo: " + a.getNome());
		ThumbVO voThumb = UploadHelper.createThumbCustom(getParametrosMap(), a, 150, 37);
		ArquivoThumb t = new ArquivoThumb();
		t.setThumbVo(voThumb);
		t.setArquivo(a);
		arquivoService.saveOrUpdate(t);
		
		return t;
	}

	private void setParamDisplayTable() {
		paramDisplayTable.put(Params.BUILD_APP_ANDROID_URL, false);
		paramDisplayTable.put(Params.BUILD_APP_IOS_URL, false);
		paramDisplayTable.put(Params.WEB_TEMPLATE_TITLE, false);
		paramDisplayTable.put(Params.BORDER_COPYRIGHT_EMPRESA, false);
		paramDisplayTable.put(Params.MSG_PUSH_NEWPOST, false);
		paramDisplayTable.put(Params.PUSH_SERVER_PROJECT, false);
		paramDisplayTable.put(Params.PUSH_ON, false);
		paramDisplayTable.put(Params.CHAT_ON, false);
		paramDisplayTable.put(Params.COR_MENU, false);
		paramDisplayTable.put(Params.COR_BUSCA, false);
		paramDisplayTable.put(Params.COR_BADGE_SPAN, false);
		paramDisplayTable.put(Params.COR_BADGE_TEXT, false);
	}
	
	@Override
	public void onRender() {
		super.onRender();
		
		
	}
	
}
