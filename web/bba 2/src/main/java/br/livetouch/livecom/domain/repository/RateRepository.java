package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Rate;
import br.livetouch.livecom.domain.Usuario;

@Repository
public interface RateRepository extends net.livetouch.tiger.ddd.repository.Repository<Rate> {

	List<Rate> findAllByUser(Usuario u);

	Rate findByUserPost(Usuario u, Post p);

	int findMedia(Post p);

	Long countByPost(Post p);
	
}