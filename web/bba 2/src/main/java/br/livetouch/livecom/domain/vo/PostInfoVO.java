package br.livetouch.livecom.domain.vo;

import java.util.List;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostDestaque;

/**
 * Classe utilitária para salvar posts.
 *
 */
public class PostInfoVO {
	public List<Long> arquivoIds;
	public PostDestaque destaque;
	
	public String dataPublicacao;
	public String horaPublicacao;

	public String dataExpiracao;
	public String horaExpiracao;
	
	public String dataReminder;
	public String horaReminder;
	
	public List<Long> tagsIds;
	public String tags;

	public boolean push;
	public boolean prioritario;
	public List<Long> grupoIds;
	
	public List<String> grupos;
	public Long statusPost;
	public boolean webview;
	public boolean html;

	public Long arquivoDestaqueId;

	//request rest
	public Post post;
	public String codigoCategoria;
	public String destaqueDescricaoUrl;
	public String destaqueTituloUrl;
	public int destaqueTipo;
}
