package br.livetouch.livecom.domain.service;


import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.CategoriaIdioma;
import br.livetouch.livecom.domain.CategoriaPost;
import net.livetouch.tiger.ddd.DomainException;

public interface CategoriaIdiomaService extends br.livetouch.livecom.domain.service.Service<CategoriaIdioma> {

	List<CategoriaIdioma> findAll();
	
	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(CategoriaIdioma ci) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(CategoriaIdioma ci) throws DomainException;

	Set<CategoriaIdioma> findByCategoria(CategoriaPost c);
	
}