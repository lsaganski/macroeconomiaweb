package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.domain.vo.MensagemVO;
import br.livetouch.livecom.domain.vo.UserInfoVO;
import net.livetouch.extras.util.DateUtils;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MensagemConversa extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "MENSAGEMCONVERSA_SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_from_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Usuario from;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_to_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Usuario to;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "grupo_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Grupo grupo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Post post;

	private Date dataCreated;
	private Date dataUpdated;
	
	@OneToMany(mappedBy = "conversa", fetch = FetchType.LAZY)
	@OrderBy("id asc")
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Mensagem> mensagens;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "last_msg_id", nullable = true)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Mensagem lastMensagem;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getFrom() {
		return from;
	}

	public void setFrom(Usuario from) {
		this.from = from;
	}

	public Usuario getTo() {
		return to;
	}

	public void setTo(Usuario to) {
		this.to = to;
	}

	public Date getDataCreated() {
		return dataCreated;
	}

	public void setDataCreated(Date dataCreated) {
		this.dataCreated = dataCreated;
	}

	public Date getDataUpdated() {
		return dataUpdated;
	}

	public void setDataUpdated(Date dataUpdated) {
		this.dataUpdated = dataUpdated;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public Set<Mensagem> getMensagens() {
		return mensagens;
	}

	public void setMensagens(Set<Mensagem> mensagens) {
		this.mensagens = mensagens;
	}
	

	public String getDataCreatedString() {
		return DateUtils.toString(dataCreated, "dd/MM/yyyy HH:mm:ss");
	}
	
	public String getDataCreatedStringChat() {
		return br.livetouch.livecom.utils.DateUtils.toDateStringChat(dataCreated);
	}
	
	public String getDataUpdatedString() {
		return br.livetouch.livecom.utils.DateUtils.toDateChat(dataUpdated);
	}
	
	public String getDataUpdatedStringChat() {
		return br.livetouch.livecom.utils.DateUtils.toDateStringChat(dataUpdated);
	}
	
	public Usuario getOutroUser(UserInfoVO user) {
		if(user == null) {
			return getTo();
		}
		boolean equals = user.getId().equals(getTo().getId());
		return equals ? getFrom() : getTo();
	}

	public static void sortByDatUpdated(List<MensagemConversa> list) {
		Collections.sort(list, new Comparator<MensagemConversa>() {
			@Override
			public int compare(MensagemConversa m1, MensagemConversa m2) {
				return m1.getDataUpdated().compareTo(m2.getDataUpdated());
			}
		});
	}
	
	public static void sortByDatUpdatedLastFirst(List<MensagemConversa> list) {
		sortByDatUpdated(list);
		Collections.reverse(list);
	}

	/**
	 * se tem acesso
	 * 
	 * @param userInfo
	 * @return
	 */
	public boolean isFromTo(Usuario userInfo) {
		return getTo().equals(userInfo) || getFrom().equals(userInfo);
	}

	public MensagemVO getPrimeiraMensagensVO(Usuario user,boolean sortAsc ) {
		// arquivos
		List<Mensagem> list = new ArrayList<Mensagem>(getMensagens());
		// todo mensagens
		if(sortAsc) {
			Mensagem.sortByIdLastFirst(list);
		}

		if (list != null && list.size() > 0) {
			for (Mensagem m : list) {

				boolean excluida = m.isExcluidaFrom(user);
				//boolean excluida  = false;
				if ( !excluida) {
					MensagemVO f = new MensagemVO();
					f.setMensagem(m, user);
					return f;
				}
			}
		}
		return null;
	}

	public static List<Long> getIds(Collection<? extends net.livetouch.tiger.ddd.Entity> list) {
		List<Long> ids = new ArrayList<Long>();
		if(list == null || list.size() == 0) {
			return ids;
		}
		for (net.livetouch.tiger.ddd.Entity e : list) {
			ids.add(e.getId());
		}
		return ids;
	}
	
	public Mensagem getLastMensagem() {
		return lastMensagem;
	}
	
	public void setLastMensagem(Mensagem lastMensagem) {
		this.lastMensagem = lastMensagem;
	}

	public Mensagem getFirstMensagemNaoExcluida(Usuario userInfo) {
		for (Mensagem m : this.mensagens) {
			boolean excluida = m.isExcluidaFrom(userInfo);
			if (!excluida) {
				return m;
			}
		}
		return null;		
	}

	public static List<Mensagem> getLastMensagens(List<MensagemConversa> conversas, Usuario userInfo) {
		List<Mensagem> mensagens = new ArrayList<Mensagem>();

		if(conversas != null) {
			for (MensagemConversa c : conversas) {
				Mensagem m = c.getLastMensagem();
				mensagens.add(m);
			}
		}

		return mensagens;
	}
	
	public Grupo getGrupo() {
		return grupo;
	}
	
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public boolean isGrupo() {
		return grupo != null;
	}

	public boolean isAdminGrupo(Usuario u) {
		if(grupo == null || u == null) {
			return false;
		}
		return grupo.isAdmin(u);
	}
	
	@Override
	public String toString() {
		return "MensagemConversa [id=" + id + ", grupo=" + grupo + ", from=" + from + ", to=" + to + ", post=" + post + ", dataCreated=" + dataCreated + ", dataUpdated=" + dataUpdated + ", mensagens=" + mensagens + "]";
	}

	public String toStringDesc() {
		return toString();
	}

	public String getDataString() {
		return br.livetouch.livecom.utils.DateUtils.toDateChat(dataCreated);
	}
}
