package br.livetouch.livecom.spring;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import br.infra.util.Log;

/**
 * Facilita pegar a Session do Hibernate pelo Spring
 * 
 * Bom para TestCases...
 * 
 * Na web pega a session por IoC controlado pelo Spring normal
 * 
 * @author ricardo
 *
 */
public class SpringHelper {
	protected static final Logger log = Log.getLogger(SpringHelper.class);
	private HibernateTransactionManager txManager;
	private Session session;

	/**
	 * Deixa a Session viva nesta Thread. Mesma coisa que uma Thread de uma
	 * requisição web utilizando o filtro "OpenSessionInViewFilter"
	 */
	public void openSession(ApplicationContext ctx) {

		if(ctx != null) {
			txManager = (HibernateTransactionManager) ctx.getBean("transactionManager");

			SessionFactory sessionFactory = txManager.getSessionFactory();
			boolean bound = TransactionSynchronizationManager.hasResource(sessionFactory);
			
			System.out.println("Thread ["+Thread.currentThread().getName()+"] bound: " + bound);
			if(!bound) {
				session = sessionFactory.openSession();
				SessionHolder sessionHolder = new SessionHolder(session);
				TransactionSynchronizationManager.bindResource(sessionFactory, sessionHolder);
			}
		}
	}

	/**
	 * Remove a Session da Thread
	 */
	public void closeSession(ApplicationContext ctx) {
		if(ctx != null && txManager != null && session != null) {
			SessionFactory sessionFactory = txManager.getSessionFactory();

			TransactionSynchronizationManager.unbindResource(sessionFactory);

			SessionFactoryUtils.closeSession(session);
			session = null;
		}
	}

	public Session getSession() {
		return session;
	}

	@SuppressWarnings("rawtypes")
	public Object getBean(ApplicationContext ctx,Class c) {
		if(ctx == null) {
			return null;
		}
		String[] beanNamesForType = ctx.getBeanNamesForType(c);
		if(beanNamesForType == null || beanNamesForType.length == 0) {
			return null;
		}
		String name = beanNamesForType[0];
		Object bean = ctx.getBean(name);
		return bean;
	}
}
