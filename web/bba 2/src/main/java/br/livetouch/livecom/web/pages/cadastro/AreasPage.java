package br.livetouch.livecom.web.pages.cadastro;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.click.control.Form;
import net.sf.click.control.Submit;


@Controller
@Scope("prototype")
public class AreasPage extends CadastrosPage {

	public Long id;
	public boolean escondeChat = true;
	public Form form = new Form();
	
	@Override
	public void onInit() {
		super.onInit();

		form();
	}

	public void form(){
		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip botao export");
		form.add(tExportar);
	}

	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();
	}
	public boolean exportar(){
		String csv = areaService.csvArea(getEmpresa());
		download(csv, "areas.csv");
		return true;
	}
}
