package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.domain.Favorito;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.FavoritoVO;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;
import net.sf.click.extras.control.NumberField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class FavoritoPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private LongField tUserId;
	private LongField tPostId;
	private LongField tFileId;
	private NumberField tFavorito;
	private TextField tMode;

	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tUserId = new LongField("user_id", true));
		form.add(tPostId = new LongField("post_id", false));
		form.add(tFileId = new LongField("file_id", false));
		form.add(tFavorito = new NumberField("favorito", "favorito 0 ou 1"));

		form.add(new TextField("wsVersion"));
		form.add(new TextField("wstoken"));

		form.add(tMode = new TextField("mode"));

		tMode.setValue("json");

		tFavorito.setValue("1");

		tUserId.setFocus(true);

		form.add(new Submit("enviar"));

		// setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {

			long userId = tUserId.getLong();
			long postId;
			boolean isFavorito = "1".equals(tFavorito.getValue());

			boolean favPost = true;
			boolean favFile = true;

			Usuario u = usuarioService.get(userId);
			if (u == null) {
				return new MensagemResult("ERROR", "Usuário inválido.");
			}

			if (tPostId.getLong() == null) {
				favPost = false;
			}
			if (tFileId.getLong() == null) {
				favFile = false;

			}
			if (!favPost && !favFile) {
				return new MensagemResult("ERROR", "Arquivo ou post devem ser válido");
			}

			if (favPost) {
				postId = tPostId.getLong();
				Post post = postService.get(postId);
				if (post == null) {
					return new MensagemResult("ERROR", "Post inválido.");
				}

				Favorito f = favoritoService.favoritar(u, post,isFavorito);

				// JSON
				FavoritoVO fav = new FavoritoVO();
				fav.setFavorito(f);
				
				if(isFavorito) {
					notificationService.markPostAsRead(post, u);
				}
				
				return fav;
			}
		}

		return new MensagemResult("NOK", "Erro ao favoritar post.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("favorito", FavoritoVO.class);
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
