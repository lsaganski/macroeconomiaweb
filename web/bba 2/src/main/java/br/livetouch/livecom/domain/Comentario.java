package br.livetouch.livecom.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.google.gson.Gson;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.livetouch.livecom.domain.vo.ComentarioVO;
import br.livetouch.livecom.utils.DateUtils;


@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Comentario extends JsonEntity{
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "COMENTARIO_SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id", nullable = true)
	private Post post;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario usuario;
	
	@Column(length=2000)
	private String comentario;
	
	private Date data;
	
	private int ordem;
	
	private String urlArquivo;
	
	@OneToMany(mappedBy = "comentario", fetch = FetchType.LAZY)
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Arquivo> arquivos;
	
	@OneToMany(mappedBy = "comentario", fetch = FetchType.LAZY)
	@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
	@XStreamOmitField
	private List<UsuarioMarcado> usuariosMarcado;

	private Long likeCount;
	
	private Boolean lidaNotification;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Post getPost() {
		return post;
	}
	
	public void setPost(Post post) {
		this.post = post;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public int getOrdem() {
		return ordem;
	}
	
	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}
	
	public Date getData() {
		return data;
	}
	
	public long getDataTime() {
		return data != null ? data.getTime() : 0;
	}
	
	public String getDataStringHojeOntem() {
		String dataStr = "Hoje";
		Date dt = getData();
		if(dt != null) {
			long timestamp = dt.getTime();
			
			if(DateUtils.isToday(timestamp)) {
				dataStr = "Hoje";
			} else if(DateUtils.isYesterday(timestamp)) {
				dataStr = "Ontem";
			} else {
				dataStr = net.livetouch.extras.util.DateUtils.toString(dt,"dd/MMM");
			}
		}
		return dataStr;
	}
	
	public String getDataString() {
		return net.livetouch.extras.util.DateUtils.toString(data, "dd/MM/yyyy HH:mm:ss");
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public String getUrlArquivo() {
		return urlArquivo;
	}
	
	public void setUrlArquivo(String urlArquivo) {
		this.urlArquivo = urlArquivo;
	}
	
	public Set<Arquivo> getArquivos() {
		return arquivos;
	}
	
	public void setArquivos(Set<Arquivo> arquivos) {
		this.arquivos = arquivos;
	}
	
	public Boolean getLidaNotification() {
		if(lidaNotification == null) {
			return false;
		}
		return lidaNotification;
	}
	public void setLidaNotification(Boolean lidaNotification) {
		this.lidaNotification = lidaNotification;
	}

	public Long getLikeCount() {
		if (likeCount == null) {
			return 0L;
		}
		return likeCount;
	}

	public void setLikeCount(Long likeCount) {
		this.likeCount = likeCount;
	}

	public void like(boolean favorito) {
		long count = getLikeCount();
		if (favorito) {
			count++;
		} else {
			count--;
		}
		if (count < 0) {
			count = 0;
		}
		setLikeCount(count);
	}
	
	public List<UsuarioMarcado> getUsuariosMarcado() {
		return usuariosMarcado;
	}
	
	public void setUsuariosMarcado(List<UsuarioMarcado> usuariosMarcado) {
		this.usuariosMarcado = usuariosMarcado;
	}

	@Override
	public String toJson() {
		ComentarioVO vo = new ComentarioVO();
		vo.setComentario(this);
		return new Gson().toJson(vo); 
	}

	public String toStringDesc() {
		return toString();
	}

	@Override
	public String toString() {
		return "id=" + id + ", comentario=" + comentario;
	}

	public static List<Long> getIds(Collection<Comentario> list) {
		List<Long> ids = new ArrayList<Long>();
		if(list == null || list.size() == 0) {
			return ids;
		}
		for (net.livetouch.tiger.ddd.Entity e : list) {
			ids.add(e.getId());
		}
		return ids;
	}
}
