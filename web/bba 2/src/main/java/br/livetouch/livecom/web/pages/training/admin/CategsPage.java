package br.livetouch.livecom.web.pages.training.admin;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.CategTree;
import br.livetouch.livecom.domain.CategoriaPost;
import net.sf.click.extras.tree.TreeNode;

/**
 * 
 */
@Controller
@Scope("prototype")
public class CategsPage extends TrainingAdminPage {

	public CategTree tree;
	public TreeNode treeNode;
	public CategoriaPost categ;
	public Long categId;

	@Override
	public String getTemplate() {
		return super.getTemplate();
	}

	// Event Handlers ---------------------------------------------------------

	/**
	 * @see org.apache.click.Page#onInit()
	 */
	@Override
	public void onInit() {
		super.onInit();

		buildTree();
	}

	public void buildTree() {
		// Create the tree and tree model and add it to the page
		tree = new CategTree("tree", categoriaPostService, getEmpresa());
		tree.build(getContext());
		tree.setJavascriptEnabled(true);

		if (categId != null) {
			categ = categoriaPostService.get(categId);
			if (categ != null) {
				setRedirect(CategPage.class, "id", categ.getId().toString());
			}
		}
	}
}
