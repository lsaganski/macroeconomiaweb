package br.livetouch.livecom.rest.resource;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.enums.TipoParametro;
import br.livetouch.livecom.domain.vo.UploadResponse;
import br.livetouch.livecom.rest.domain.MessageResult;
import br.livetouch.livecom.utils.UploadHelper;

@Path("/v1/upload")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class UploadArquivoResource extends MainResource{

	protected static final Logger log = Log.getLogger(UploadArquivoResource.class);
	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response postFoto(final FormDataMultiPart multiPart) {
		if(multiPart != null && multiPart.getFields() != null) {
			Set<String> keys = multiPart.getFields().keySet();
			for (String key : keys) {
				// Obtem a InputStream para ler o arquivo
				FormDataBodyPart field = multiPart.getField(key);
				InputStream in = field.getValueAs(InputStream.class);
				try {
					// Salva o arquivo
					String fileName = field.getFormDataContentDisposition().getFileName();
					File file = new File(System.getProperty("java.io.tmpdir"), fileName);
					FileOutputStream out = new FileOutputStream(file);
					IOUtils.copy(in, out);
					IOUtils.closeQuietly(out);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Path("/bba/webview")
	public Response webviewTermosBBA(final FormDataMultiPart multiPart) {
		
		String url = "";
		try {
			UploadResponse uploadInfo = UploadHelper.uploadMultipart(getUserInfo(), multiPart, "bba");
			if(uploadInfo != null) {
				url = uploadInfo.arquivo.getUrl();
				
				Parametro param = parametroService.findByNome(uploadInfo.parametro, getEmpresa());
				if(param != null) {
					param.setValor(url);
				} else {
					param = new Parametro();
					param.setEmpresa(getEmpresa());
					param.setNome(uploadInfo.parametro);
					param.setTipoParametro(TipoParametro.SERVER);
					param.setValor(url);
				}
				parametroService.saveOrUpdate(param);
				
				return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", url)).build();
			}
			
			return Response.ok(MessageResult.error("Erro ao efetuar upload")).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			return Response.ok(MessageResult.error(e.getMessage())).build();
		}
	}
	
}
	
