package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Chapeu;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.vo.ChapeuFilter;
import net.livetouch.tiger.ddd.DomainException;

public interface ChapeuService extends Service<Chapeu> {

	List<Chapeu> findAll();

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Chapeu f) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Chapeu f) throws DomainException;

	List<Chapeu> findAllByFilter(ChapeuFilter filter);
	
	String csvChapeu();

	Chapeu findByName(Chapeu chapeu, Empresa empresa);

}
