package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class UpdateOrdemCategoriaPage extends WebServiceFormPage {

	private TextField tIdsCategoria;

	@Override
	protected void form() {
		form.add(tIdsCategoria = new TextField("ids", true));
	}

	@Override
	protected Object go() throws Exception {
		if (form.isValid()) {
			String ids = tIdsCategoria.getValue();
			categoriaPostService.updateOrdemByIds(getUsuario(), ids);
			return new MensagemResult("OK", "Ordens alteradas com sucesso para os ids [" + ids + "]");
		}
		return new MensagemResult("ERROR", "Formulário inválido");
	}

}
