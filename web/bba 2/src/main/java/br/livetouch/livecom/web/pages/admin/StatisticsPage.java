package br.livetouch.livecom.web.pages.admin;

import java.util.Arrays;

import org.hibernate.SessionFactory;
import org.hibernate.stat.EntityStatistics;
import org.hibernate.stat.QueryStatistics;
import org.hibernate.stat.Statistics;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.web.pages.pages.MuralPage;

@Controller
@Scope("prototype")
public class StatisticsPage extends LivecomAdminPage {
	
	public String entity;
	public String query;
	
	@Override
	public boolean onSecurityCheck() {
		super.onSecurityCheck();
			
		if(hasPermissao(ParamsPermissao.CONFIGURACOES_AVANCADAS)) {
			return true;
		}
			
		setRedirect(MuralPage.class);
		return onSecurityCheckNotOk();
	}

	@Override
	public void onGet() {
		super.onGet();
		
		SessionFactory sf = (SessionFactory) applicationContext.getBean("sessionFactory");
		
		Statistics s = sf.getStatistics();
		
		addModel("statistics", s.toString());
		
		if (entity != null) {
			EntityStatistics s2 = s.getEntityStatistics(entity);
			if (s2 != null) {
				addModel("statisticsEntity", s2.toString());
			}
		}
		
		addModel("queries", Arrays.asList(s.getQueries()));
		
		if(query != null) {
			QueryStatistics qs = s.getQueryStatistics(query);
			if (qs != null) {
				addModel("queryStatistics", qs.toString());
			}
		
		}
	}
}
