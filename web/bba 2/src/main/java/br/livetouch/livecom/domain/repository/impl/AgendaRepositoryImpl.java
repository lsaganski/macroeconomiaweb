package br.livetouch.livecom.domain.repository.impl;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Agenda;
import br.livetouch.livecom.domain.repository.AgendaRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class AgendaRepositoryImpl extends StringHibernateRepository<Agenda> implements AgendaRepository {

	public AgendaRepositoryImpl() {
		super(Agenda.class);
	}

}