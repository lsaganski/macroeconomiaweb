package br.livetouch.livecom.rest.resource;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import br.infra.util.Log;
import br.livetouch.livecom.domain.NotificationUsuario;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.NotificationSearch;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.PermissaoService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.BuscaPost;
import br.livetouch.livecom.domain.vo.NotificationResponseVO;
import br.livetouch.livecom.domain.vo.NotificationVO;
import br.livetouch.livecom.domain.vo.PostVO;
import br.livetouch.livecom.rest.domain.UserPrincipal;

@Path("/mural")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class MuralResource extends MainResource{

	protected static final Logger log = Log.getLogger(MuralResource.class);
	
	@Context
	private SecurityContext securityContext;
	
	@Autowired
	protected UsuarioService usuarioService;
	
	@Autowired
	PermissaoService permissaoService;
	
	@Autowired
	protected GrupoService grupoService;
	
	@GET
	@RolesAllowed({"admin", "user"})
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public List<PostVO> mural() {
		Usuario usuario = usuarioService.get(getUser().getId());
		BuscaPost b = new BuscaPost();
		b.user = usuario;
		b.empresa = usuario.getEmpresa();
		b.comAnexo = false;
		List<Post> posts = postService.findAllPostsByUserAndTitle(b, 0, 50);
		List<PostVO> postsVO = postService.toListVo(usuario, posts);
		return postsVO;
	}
	
	@GET
	@RolesAllowed({"admin", "user"})
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Path("/notifications")
	public NotificationResponseVO notifications() {
		Usuario usuario = usuarioService.get(getUser().getId());
		NotificationSearch search = new NotificationSearch();
		search.page = 0;
		search.maxRows = 10;
		search.user = usuario;
		List<NotificationUsuario> notifications = notificationService.findAll(search);

		NotificationResponseVO r = new NotificationResponseVO();
		r.list = new ArrayList<NotificationVO>();
		if(notifications != null) {
			for (NotificationUsuario n : notifications) {
				NotificationVO vo = new NotificationVO();
				vo.copyFrom(n);
				r.list.add(vo);
			}
		}
		return r;
	}
	
	protected Usuario getUser() {
		UserPrincipal userPrincipal = (UserPrincipal) securityContext.getUserPrincipal();
		return userPrincipal.getUsuario();
	}
}
	
