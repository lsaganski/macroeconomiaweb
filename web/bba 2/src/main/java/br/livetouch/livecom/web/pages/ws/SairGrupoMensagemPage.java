package br.livetouch.livecom.web.pages.ws;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.chatAkka.router.AkkaHelper;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.GrupoUsuarios;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 * Deixa o usuario sair do Grupo.
 * 
 * Regra: se for admin não pode sair.
 * 
 */
@Controller
@Scope("prototype")
public class SairGrupoMensagemPage extends WebServiceFormPage {

	public Long user_id;
	public Long grupo_id;

	@Override
	protected void form() {
		form.add(new TextField("user_id", true));
		form.add(new TextField("grupo_id", true));
	}

	@Override
	protected Object go() throws Exception {
		if (form.isValid()) {

			Grupo g = null;
			if (grupo_id != null && grupo_id > 0) {
				g = grupoService.get(grupo_id);
				if (g == null) {
					if(isWsVersion3()) {
						Response r = Response.error("Grupo não encontrado com o id [" + grupo_id + "]");
						return r;
					}
					return new MensagemResult("NOK", "Grupo não encontrado com o id [" + grupo_id + "]");
				}
			}

			Usuario u = null;
			if (user_id != null && user_id > 0) {
				u = usuarioService.get(user_id);
				if (u == null) {
					if(isWsVersion3()) {
						Response r = Response.error("Usuario não encontrado com o id [" + user_id + "]");
						return r;
					}
					return new MensagemResult("NOK", "Usuario não encontrado com o id [" + user_id + "]");
				}
			}

			// Valida se user esta no grupo
			boolean pertenceAoGrupo = grupoService.isUsuarioDentroDoGrupo(g, u);
			if (!pertenceAoGrupo) {
				if(isWsVersion3()) {
					Response r = Response.error("Usuario [" + u + "] não esta no grupo [" + g + "]");
					return r;
				}
				return new MensagemResult("NOK", "Usuario [" + u + "] não esta no grupo [" + g + "]");
			}
			
//			// Remove o user do grupo
			GrupoUsuarios gu = grupoService.findByGrupoEUsuario(g, u);
			usuarioService.deleteGrupoUsuarios(gu);

			// Envia msg Akka dizendo que saiu
			Usuario admin = g.getAdmin();
			String message = u.getNome() + " saiu";
			MensagemConversa conversa = mensagemService.findByGrupo(g);
			
			// Enviar msg do AKKA aqui no final.
			livecomChatInterface.saveMessageAdminAndSendAkka(admin, g, conversa,message);
			
			AkkaHelper.sendMessageDenied(u.getId(), conversa.getId(), g.getId());
			
			if(isWsVersion3()) {
				Response r = Response.ok("Usuario [" + u + "] saiu da conversa");
				return r;
			}
			return new MensagemResult("OK", "Usuario [" + u + "] saiu da conversa");
			
		}

		if(isWsVersion3()) {
			Response r = Response.error("Preencha o formulario");
			return r;
		}
		return new MensagemResult("NOK", "Preencha o formulario");

	}

	@Override
	protected void xstream(XStream x) {
		super.xstream(x);
	}
	
	@Override
	public boolean isSaveLogTransacao() {
		return true;
	}
}
