package br.livetouch.livecom.web.pages.ws;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.infra.web.click.PostField;
import br.infra.web.click.UsuarioLoginField;
import br.livetouch.livecom.domain.Historico;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.HistoricoVO;
import net.sf.click.control.FieldSet;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class HistoricoPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	private TextField tMode;
	private TextField tAction;
	private UsuarioLoginField tUser;
	private PostField postField;
	public List<HistoricoVO> historicos;
	
	@Override
	public void onInit() {
		super.onInit();

		form.setMethod("post");
		form.add(tUser = new UsuarioLoginField("user", true,usuarioService, getEmpresa()));
		form.add(tAction = new TextField("action","list , save , delete"));
		form.add(tMode = new TextField("mode"));
		
		FieldSet set = new FieldSet("Save / Delete");
		set.add(postField = new PostField(postService));
		form.add(set);
		
		tAction.setValue("list");

		tUser.setFocus(true);

		tMode.setValue("json");

		form.add(new Submit("consultar"));

		setFormTextWidth(form);
	}

	@Override
	protected Object execute() throws Exception {
		if (form.isValid()) {
			Usuario u = tUser.getEntity();
			if(u == null) {
				return new MensagemResult("NOK" ,"Usuario nao encontrado");
			}

			boolean actionList = "list".equalsIgnoreCase(tAction.getValue());
			boolean actionSave = "save".equalsIgnoreCase(tAction.getValue());
			boolean actionDelete = "delete".equalsIgnoreCase(tAction.getValue());
			
			if(actionList) {
				List<Historico> list = historicoService.findAllByUser(u);
				historicos = HistoricoVO.toListVO(list);
				return historicos;
			} else if(actionSave || actionDelete) {

				Post p = postField.getPost();
				if(p == null) {
					return new MensagemResult("NOK" ,"Post nao encontrado.");
				}
				
				Historico h = historicoService.findByUserPost(u,p);

				if(h == null) {
					h = new Historico();
					h.setPost(p);
					h.setUsuario(u);
				}
				
				if(actionSave) {
					h.setData(new Date());

					historicoService.saveOrUpdate(h);
					
					HistoricoVO vo = new HistoricoVO();
					vo.setHistorico(h);
					
					return vo;
				} else {
					historicoService.delete(h);
					
					return new MensagemResult("OK","Historico excluido com sucesso.");
				}
			}

			return new MensagemResult("NOK","Invalid action.");
		}

		return new MensagemResult("NOK","Invalid parameters.");
	}

	@Override
	protected void xstream(XStream x) {
		x.alias("historico", HistoricoVO.class);
		super.xstream(x);
	}
}
