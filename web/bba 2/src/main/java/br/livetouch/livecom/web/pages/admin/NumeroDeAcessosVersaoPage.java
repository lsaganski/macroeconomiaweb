package br.livetouch.livecom.web.pages.admin;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.infra.web.click.ComboVersao;
//import br.infra.web.click.ComboVersao;
import br.livetouch.livecom.domain.vo.RelatorioAcessosVersaoVO;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.web.pages.report.RelatorioPage;
import net.livetouch.click.table.PaginacaoTable;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.extras.control.DateField;
import net.sf.click.extras.control.IntegerField;
import net.sf.click.extras.control.LinkDecorator;

/**
 * Logs das auditorias.
 * 
 */
@Controller
@Scope("prototype")
public class NumeroDeAcessosVersaoPage extends RelatorioPage {

	private RelatorioFiltro filtro;
	public ActionLink detalhes = new ActionLink("detalhes", getMessage("detalhes.label"), this, "detalhes");
	public PaginacaoTable table = new PaginacaoTable();
	public int page;
	private IntegerField tMax;
	public Form form = new Form();
	public DateField tDataInicio = new DateField("dataInicial", getMessage("dataInicio.label"));
	public DateField tDataFim = new DateField("dataFinal", getMessage("dataFim.label"));
	public String dataInicio;
	public String dataFim;
	public ComboVersao comboVersao;

	@Override
	public void onInit() {
		super.onInit();
		
		bootstrap_on = true;
		escondeChat = true;

		if (clear) {
			getContext().removeSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		}

		table();

		form();
	}

	public void form() {

		tDataInicio.setAttribute("class", "input data datepicker");
		tDataFim.setAttribute("class", "input data datepicker");
		tDataInicio.setFormatPattern("dd/MM/yyyy");
		tDataFim.setFormatPattern("dd/MM/yyyy");

		tDataInicio.setValue(DateUtils.toString(DateUtils.addDia(new Date(), -7), "dd/MM/yyyy"));
		tDataFim.setValue(DateUtils.toString(new Date(), "dd/MM/yyyy"));

		tDataInicio.setMaxLength(10);
		tDataFim.setMaxLength(10);

		form.add(tDataInicio);
		form.add(tDataFim);

		form.add(comboVersao = new ComboVersao(logService, getEmpresa()));
		comboVersao.setLabel(getMessage("coluna.versao.label"));

		Submit tfiltrar = new Submit("filtrar", getMessage("filtrar.label"), this, "filtrar");
		tfiltrar.setAttribute("class", "btn btn-primary min-width");
		form.add(tfiltrar);

		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "btn btn-outline blue min-width");
		form.add(tExportar);

		tMax = new IntegerField("max");
		tMax.setAttribute("class", "input");
		form.add(tMax);

	}

	private void table() {
		Column c = new Column("deviceSo", "Plataforma");
		c.setAttribute("align", "left");
		c.setWidth("40%");
		table.addColumn(c);

		c = new Column("versao", getMessage("coluna.versao.label"));
		c.setAttribute("align", "left");
		c.setWidth("40%");
		table.addColumn(c);

		c = new Column("logados", getMessage("usuarios.versao.label"));
		c.setAttribute("align", "left");
		c.setWidth("40%");
		table.addColumn(c);

		c = new Column("detalhes", getMessage("detalhes.label"));
		c.setTextAlign("center");
		c.setAttribute("align", "center");
		c.setDecorator(new LinkDecorator(table, detalhes, "versao"));
		table.addColumn(c);

	}

	public boolean detalhes() {
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		setRedirect(DetalhesAcessoVersaoPage.class, "versao", detalhes.getValue());
		return false;
	}

	@Override
	public void onGet() {
		super.onGet();

		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro != null) {
			form.copyFrom(filtro);
			filtro.setPage(page);
			filtro.setEmpresaId(getEmpresa().getId());
		}
	}

	public boolean filtrar() {
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new RelatorioFiltro();
			getContext().setSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);
		filtro.setEmpresaId(getEmpresa().getId());

		return true;
	}

	public boolean exportar() throws DomainException {
		filtro = (RelatorioFiltro) getContext().getSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY);
		if (filtro == null) {
			filtro = new RelatorioFiltro();
			getContext().setSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		}
		form.copyTo(filtro);

		String csv = logService.csvAcessosVersao(filtro, getEmpresa());
		download(csv, "relatorio-acessos-versao.csv");

		return true;
	}

	@Override
	public String getContentType() {
		return super.getContentType();
	}

	@Override
	public String getTemplate() {
		return super.getTemplate();
	}

	@Override
	public void onRender() {
		super.onRender();

		if (filtro == null) {
			filtro = new RelatorioFiltro();
			form.copyTo(filtro);
			filtro.setEmpresaId(getEmpresa().getId());
			getContext().setSessionAttribute(RelatorioFiltro.SESSION_FILTRO_KEY, filtro);
		}

		Integer max = tMax.getInteger();
		if (max == null) {
			max = 20;
			filtro.setMax(max);
		}

		long acessosCount = 0;
		List<RelatorioAcessosVersaoVO> acessos = null;
		try {
			acessos = usuarioService.findAcessosByVersao(filtro);
			acessosCount = usuarioService.getCountVersaoByFilter(filtro);
		} catch (DomainException e) {
			form.setError(e.toString());
		}
		table.setCount(acessosCount);
		table.setPageSize(max);

		table.setRowList(acessos);

	}
}
