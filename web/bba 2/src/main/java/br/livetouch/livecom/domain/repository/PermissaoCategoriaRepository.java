package br.livetouch.livecom.domain.repository;

import java.util.List;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.PermissaoCategoria;
import net.livetouch.tiger.ddd.repository.Repository;

public interface PermissaoCategoriaRepository extends Repository<PermissaoCategoria> {

	List<PermissaoCategoria> findAll(Empresa e);

	PermissaoCategoria findByNomeValid(PermissaoCategoria p, Empresa empresa);

	PermissaoCategoria findByCodigoValid(PermissaoCategoria p, Empresa empresa);

}