
package br.livetouch.livecom.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.infra.util.Utils;
import br.infra.web.click.HTMLEncode;
import br.livetouch.livecom.domain.enums.StatusTransacao;
import net.livetouch.extras.util.DateUtils;
import net.livetouch.extras.util.ExceptionUtil;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LogTransacao extends net.livetouch.tiger.ddd.Entity {
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario usuario;

	static final long serialVersionUID = 1L;

	public static final String SESSION_FILTRO_KEY = "LogTransacao";

	// Usado como valor padrao caso os atributos do celular estejam vazios
	private static final String SERVIDOR = "Servidor";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "LOGTRANSACAO_SEQ")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date dataInicio;

	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date dataFim;

	@Column(length = 200)
	private String comando;

	@Column(length = 100)
	private String login;

	@Enumerated(EnumType.STRING)
	private StatusTransacao status;

	@Column(length = 2000)
	private String msgErro;

	@Column(length = 2000, nullable = true)
	private String msgErroStack;

	private long tempo;

	private String versaoAplicativo;

	// @ManyToOne
	// @JoinColumn(name = "cliente_id", nullable = false)
	// private Cliente cliente;

	@Transient
	private long timeA;

	// Dados do celular
	private String deviceWidth;
	private String deviceHeight;
	// Modelo do celular
	private String deviceModel;
	private String deviceType;
	// J2ME ou ANDROID
	private String deviceSo;
	// Version SO
	private String deviceSoVersion;
	
	private String deviceRegistrationId;
	
	private String deviceImei;
	
	private String userAgent;

	private String requestPath;
	
	@Column(columnDefinition = "clob")
	private String requestParams;

	@Column(length = 2000, nullable = true)
	private String xml;

	@Transient
	private Throwable exception;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;
	
	public LogTransacao() {
		timeA = System.currentTimeMillis();
	}

	/** -------------------------------- **/

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		if (login != null) {
			return login;
		}
		Usuario u = getUsuario();
		return u != null ? u.getLogin() : null;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getVersaoAplicativo() {
		return versaoAplicativo;
	}

	public void setVersaoAplicativo(String versaoAplicativo) {
		this.versaoAplicativo = versaoAplicativo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceSo() {
		return deviceSo;
	}

	public void setDeviceSo(String deviceSo) {
		this.deviceSo = deviceSo;
	}

	public String getDeviceSoVersion() {
		return deviceSoVersion;
	}

	public void setDeviceSoVersion(String deviceSoVersion) {
		this.deviceSoVersion = deviceSoVersion;
	}

	public void setDeviceHeight(String deviceHeight) {
		this.deviceHeight = deviceHeight;
	}

	public void setDeviceWidth(String deviceWidth) {
		this.deviceWidth = deviceWidth;
	}

	public String getDeviceHeight() {
		return deviceHeight;
	}

	public String getDeviceWidth() {
		return deviceWidth;
	}

	public java.util.Date getDataInicio() {
		return dataInicio;
	}

	public String getDataInicioString() {
		return DateUtils.toString(dataInicio, "dd/MM/yyyy HH:mm:ss");
	}

	public String getDataInicioStringDMY() {
		return DateUtils.toString(dataInicio, "dd/MM/yyyy");
	}

	public String getDataFimStringDMY() {
		return DateUtils.toString(dataFim, "dd/MM/yyyy");
	}

	public void setDataInicio(java.util.Date dataChegada) {
		this.dataInicio = dataChegada;
	}

	public String getDataFimString() {
		return DateUtils.toString(dataFim, "dd/MM/yyyy HH:mm:ss");
	}

	public java.util.Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(java.util.Date dataEntregue) {
		this.dataFim = dataEntregue;
	}

	public StatusTransacao getStatus() {
		return status;
	}

	public void setStatus(StatusTransacao status) {
		this.status = status;
	}

	public long getTempo() {
		return tempo;
	}

	public void setTempo(long tempo) {
		this.tempo = tempo;
	}

	public String getMsgErro() {
		return msgErro;
	}

	public String getMsgErroStack() {
		return msgErroStack;
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = Utils.truncate(msgErro, 2000);
	}

	public void setMsgErroStack(String msgErroStack) {
		this.msgErroStack = Utils.truncate(msgErroStack, 2000);
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public void finalizar() {
		long timeB = System.currentTimeMillis();
		this.tempo = timeB - timeA;
		this.dataFim = new Date();
		if (StatusTransacao.ERRO != status) {
			this.status = StatusTransacao.OK;
		}

		// Caso os parametros exclusivos do celular estejam vazios, e porque a
		// requisição foi feita pelo
		// aplicativo de Administracao, salvar o valor default nesses casos
		// (Servidor)
		if (StringUtils.isEmpty(deviceSoVersion)) {
			this.deviceSoVersion = SERVIDOR;
		}
		if (StringUtils.isEmpty(deviceModel)) {
			this.deviceModel = SERVIDOR;
		}
		if (StringUtils.isEmpty(deviceSo)) {
			this.deviceSo = SERVIDOR;
		}
		if (StringUtils.isEmpty(deviceWidth)) {
			this.deviceWidth = SERVIDOR;
		}
		if (StringUtils.isEmpty(deviceHeight)) {
			this.deviceHeight = SERVIDOR;
		}
		if (StringUtils.isEmpty(versaoAplicativo) || versaoAplicativo == null) {
			this.versaoAplicativo = "web";
		}
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(getId()).toHashCode();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof LogTransacao))
			return false;
		LogTransacao castOther = (LogTransacao) other;
		return new EqualsBuilder().append(this.getId(), castOther.getId()).isEquals();
	}

	public boolean isStatusOk() {
		boolean ok = StringUtils.isEmpty(msgErro) && StringUtils.isEmpty(msgErroStack);
		return ok;
	}

	// Como na tela de DetalhesLogTransacao, podemos ter 3 layouts diferentes
	// para o campo RESULTADO,
	// usamos os metodos abaixo para saber o que mostrar

	/**
	 * Metodo que verifica se a transacao foi originada com ERRO
	 * 
	 * @return
	 */
	public boolean isErro() {
		if (StringUtils.isNotEmpty(this.msgErro) || (StringUtils.isNotEmpty(this.msgErroStack))) {
			return true;
		} else {
			return false;
		}
	}

	public String getRequestPath() {
		return requestPath;
	}

	public void setRequestPath(String requestPath) {
		this.requestPath = requestPath;
	}

	public String getRequestParams() {
		return requestParams;
	}

	public void setRequestParams(String requestParams) {
		this.requestParams = Utils.truncate(requestParams, 1900);
	}

	public String getXml() {
		if (xml != null) {
			return HTMLEncode.encode(xml);
		}
		return xml;
	}

	public void setXml(String xml) {
		this.xml = Utils.truncate(xml, 2000);
	}

	public void setTransacaoRequest(HttpServletRequest request,Usuario usuario, String path, String requestParams) {
		deviceWidth = request.getParameter("device.width");
		deviceHeight = request.getParameter("device.height");
		deviceSo = request.getParameter("device.so");
		
		deviceSoVersion = request.getParameter("device.so.version");
		if(StringUtils.isEmpty(deviceSoVersion)) {
			deviceSoVersion = request.getParameter("device.so_version");
		}
		
		deviceModel = request.getParameter("device.model");
		if(StringUtils.isEmpty(deviceModel)) {
			deviceModel = request.getParameter("device.name");
		}
		
		deviceType = request.getParameter("device.type");
		
		versaoAplicativo = request.getParameter("app.version");
		if(StringUtils.isEmpty(versaoAplicativo)) {
			versaoAplicativo = request.getParameter("versao.app");
		}

		deviceRegistrationId = request.getParameter("device.registration_id");
		if(StringUtils.isEmpty(deviceRegistrationId)) {
			deviceRegistrationId = request.getParameter("registration_id");
		}
		
		deviceImei = request.getParameter("device.imei");
		
		userAgent = request.getHeader("user-agent");

		if(usuario != null) {
			this.login = usuario.getLogin();
			this.usuario = usuario;
		}

		this.timeA = System.currentTimeMillis();
		this.dataInicio = new Date();

		this.requestPath = path;
		
		setRequestParams(requestParams);
	}

	public void setException(Throwable exception) {
		if(exception  != null) {
			this.status = StatusTransacao.ERRO;
			setMsgErro("Erro: " + exception.getMessage());
			setMsgErroStack(ExceptionUtil.getStackTrace(exception));
		}
		this.exception = exception;
	}

	public Throwable getException() {
		return exception;
	}

	public void setComando(String comando) {
		this.comando = comando;
	}

	public String getComando() {
		return comando;
	}

	@Override
	public String toString() {
		return "LogTransacao [usuario=" + usuario + ", id=" + id + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim + ", login=" + login + ", status=" + status + ", msgErro=" + msgErro + ", msgErroStack=" + msgErroStack + ", tempo=" + tempo + ", versaoAplicativo=" + versaoAplicativo
				+ ", timeA=" + timeA + ", deviceWidth=" + deviceWidth + ", deviceHeight=" + deviceHeight + ", deviceModel=" + deviceModel + ", deviceType=" + deviceType + ", deviceSo=" + deviceSo + ", deviceSoVersion=" + deviceSoVersion + ", requestPath=" + requestPath + ", requestParams="
				+ requestParams + ", xml=" + xml + ", exception=" + exception + "]";
	}

	public String getDeviceRegistrationId() {
		return deviceRegistrationId;
	}

	public void setDeviceRegistrationId(String deviceRegistrationId) {
		this.deviceRegistrationId = deviceRegistrationId;
	}

	public String getDeviceImei() {
		return deviceImei;
	}

	public void setDeviceImei(String deviceImei) {
		this.deviceImei = deviceImei;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
}
