package br.livetouch.livecom.web.pages.training.admin;


import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Faq;
import br.livetouch.livecom.web.pages.training.TrainingUtil;
import net.livetouch.click.control.IdField;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class FaqPage extends TrainingAdminPage {

	public Form form = new Form();
	public String msgErro;
	public Long id;

	public int page;

	public Faq faq;

	@Override
	public void onInit() {
		super.onInit();

		form();
		
		if (id != null) {
			faq = faqService.get(id);
		} else {
			faq = new Faq();
		}
		form.copyFrom(faq);
	}
	
	public void form(){
		form.add(new IdField());

		TextField t = new TextField("enunciado","titulo");
		t.setAttribute("autocomplete", "off");
		t.setFocus(true);
		form.add(t);
		
		t = new TextField("resposta");
		t.setAttribute("autocomplete", "off");
		t.setFocus(true);
		form.add(t);
		
		t = new TextField("ordem");
		t.setAttribute("autocomplete", "off");
		t.setFocus(true);
		form.add(t);
		
		Submit tsalvar = new Submit("salvar", this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);
		
		if (id != null) {
			Submit tDeletar = new Submit("deletar", this, "deletar");
			tDeletar.setAttribute("class", "botao btSalvar");
			form.add(tDeletar);
		}
		Submit cancelar = new Submit("cancelar",this,"cancelar");
		cancelar.setAttribute("class", "botao");
		form.add(cancelar);

		setFormTextWidth(form);
		TrainingUtil.setTrainingStyle(form);
	}
	
	@Override
	public void onGet() {
		super.onGet();
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				faq = id != null ? faqService.get(id) : new Faq();
				form.copyTo(faq);

				faqService.saveOrUpdate(faq);

				setFlashAttribute("msg",  "Faq ["+faq.getEnunciado()+"] salvo com sucesso.");
				setRedirect(FaqsPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean cancelar() {
		setRedirect(FaqsPage.class);
		return true;
	}

	public boolean deletar() {
		try {
			try {
				faq = id != null ? faqService.get(id) : new Faq();

				faqService.delete(faq);

				setFlashAttribute("msg",  "Faq ["+faq.getEnunciado()+"] deletado com sucesso.");
				setRedirect(FaqsPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = "Entidade possui relacionamentos. Não foi possível excluir.";
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
