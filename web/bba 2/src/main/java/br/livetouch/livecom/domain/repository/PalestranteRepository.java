package br.livetouch.livecom.domain.repository;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Palestrante;

@Repository
public interface PalestranteRepository extends net.livetouch.tiger.ddd.repository.Repository<Palestrante> {
	
}