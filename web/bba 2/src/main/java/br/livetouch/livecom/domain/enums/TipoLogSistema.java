/**
 * 
 */
package br.livetouch.livecom.domain.enums;

public enum TipoLogSistema {
	ERRO("Erro"), 
	INFORMACAO("Informação"),
	Convite("Convite");

	private final String s;
	TipoLogSistema(String s){
		this.s = s;
	}

	@Override
	public String toString() {
		return s != null ? s : "?";
	}
}