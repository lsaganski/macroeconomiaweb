package br.livetouch.livecom.web.pages.cadastro;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Usuario;
import net.sf.click.control.Form;



/**
 * Alterar senha do usuario
 * 
 * @author augusto
 *
 */
@Controller
@Scope("prototype")
public class AlterarSenhaPage extends CadastrosPage {

	public Form form = new Form();
	
	public Long id;
	
	public Usuario usuario;

	@Override
	public void onInit() {
		super.onInit();
		
		bootstrap_on = true;
		
		initGetUsuario();
	}
	
	protected void initGetUsuario() {
		if (id != null) {
			usuario = usuarioService.get(id);
		}
	}

}
