package br.livetouch.livecom.web.pages.admin;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Formacao;
import net.livetouch.click.control.link.Link;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.LongField;

/**
 * 
 */
@Controller
@Scope("prototype")
public class FormacaoPage extends LivecomAdminPage {

	public Form form = new Form();
	public ActionLink editLink = new ActionLink("editar", getMessage("editar.label"), this,"editar");
	public Link deleteLink = new Link("deletar", getMessage("deletar.label"), this, "deletar");
	public String msgErro;
	public Long id;

	public int page;
//	public int lastPage;

	public List<Formacao> formacoes;

	public Formacao formacao;


	@Override
	public void onInit() {
		super.onInit();
		
		form();
	}

	public void form(){
//		form.add(new IdField());
		LongField lId = new LongField("id");
		form.add(lId);

		TextField tNome = new TextField("nome", getMessage("nome.label"));
		tNome.setAttribute("autocomplete", "off");
		tNome.setAttribute("rel", "Adicionar formação");
		tNome.setAttribute("class", "mascara");
		form.add(tNome);

		Submit tExportar = new Submit("exportar", getMessage("exportar.label"), this, "exportar");
		tExportar.setAttribute("class", "tooltip botao export");
		form.add(tExportar);
		
		Submit tsalvar = new Submit("salvar", getMessage("salvar.label"), this, "salvar");
		tsalvar.setAttribute("class", "botao btSalvar");
		form.add(tsalvar);
		
		form.add(new Submit("novo", getMessage("novo.label"), this,"novo"));

	}

	@Override
	public void onGet() {
		super.onGet();
		
		if (id != null) {
			formacao = domainService.getFormacao(id);
		} else {
			formacao = new Formacao();
		}
		
		if(formacao != null) {
			form.copyFrom(formacao);
		}
	}

	public boolean salvar() {
		if (form.isValid()) {
			try {
				formacao = id != null ? domainService.getFormacao(id) : new Formacao();
				form.copyTo(formacao);

				domainService.saveOrUpdate(formacao,getUsuario());

				setMessageSesssion(getMessage("msg.formacao.salvar.sucess", formacao.getNome()), getMessage("msg.formacao.salvar.label.sucess"));
				
				setRedirect(FormacaoPage.class);

				return false;

			} catch (DomainException e) {
				form.setError(e.getMessage());
			} catch (Exception e) {
				logError(e.getMessage(),e);
				form.setError(e.getMessage());
			}
		}
		return true;
	}

	public boolean novo() {
		setRedirect(FormacaoPage.class);
		return true;
	}

	public boolean editar() {
		Long id = editLink.getValueLong();
		formacao = domainService.getFormacao(id);
		form.copyFrom(formacao);

		return true;
	}

	public boolean deletar() {
		try {
			Long id = deleteLink.getValueLong();
			Formacao formacao = domainService.getFormacao(id);
			domainService.delete(formacao);
			setRedirect(FormacaoPage.class);
			setMessageSesssion(getMessage("msg.formacao.excluir.sucess"), getMessage("msg.formacao.excluir.label.sucess"));
		} catch (DomainException e) {
			logError(e.getMessage(),e);
			this.msgErro = e.getMessage();
		} catch (ConstraintViolationException e) {
			logError(e.getMessage(),e);
			this.msgErro = getMessage("msg.formacao.excluir.error");
		}
		return true;
	}

	@Override
	public void onRender() {
		super.onRender();

		formacoes = domainService.findAllFormacao(getUsuario());

		// Count(*)
		int pageSize = 10;
		long count = formacoes.size();

		createPaginator(page, count, pageSize);
	}
	public boolean exportar(){
		String csv = formacaoService.csvFormacao(getEmpresa());
		download(csv, "formacoes.csv");
		return true;
	}
}
