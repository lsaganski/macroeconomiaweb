package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Chapeu;

public class ChapeuVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String nome;
	private String cor;
	private int ordem;
	
	private Long idCategoria;
	private String nomeCategoria;

	public ChapeuVO(Chapeu chapeu) {
		this.id = chapeu.getId();
		this.nome = chapeu.getNome();
		this.ordem = chapeu.getOrdem();
		CategoriaPost categoria = chapeu.getCategoria();
		if (categoria != null) {
			this.cor = categoria.getCor();
			this.nomeCategoria = categoria.getNome();
			this.idCategoria = categoria.getId();
		}
	}

	public static List<ChapeuVO> create(List<Chapeu> list) {
		List<ChapeuVO> retorno = new ArrayList<ChapeuVO>();
		for (Chapeu c : list) {
			retorno.add(new ChapeuVO(c));
		}
		return retorno;
	}


	public ChapeuVO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}
	public Long getIdCategoria() {
		return idCategoria;
	}
	public String getNomeCategoria() {
		return nomeCategoria;
	}

	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}
}
