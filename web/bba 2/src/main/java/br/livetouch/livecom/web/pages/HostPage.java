package br.livetouch.livecom.web.pages;



import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.thoughtworks.xstream.XStream;

import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.livecom.web.pages.ws.WebServiceXmlJsonPage;
import net.sf.click.control.Form;

/**
 * @author Ricardo Lecheta
 * 
 */
@Controller
@Scope("prototype")
public class HostPage extends WebServiceXmlJsonPage {

	public Form form = new Form();
	public String data;
	public Long timestamp;
	public String requestUri;
	
	@Override
	public void onInit() {
		super.onInit();
		data = DateUtils.getDate("dd/MM/yyyy H:mm:ss");
		timestamp = DateUtils.toDate(data).getTime();
		HttpServletRequest req = getContext().getRequest();
		StringBuffer url = req.getRequestURL();
		String uri = req.getRequestURI();
		String ctx = req.getContextPath();
		requestUri = url.substring(0, url.length() - uri.length() + ctx.length()) + "/";
	}

	@Override
	protected void xstream(XStream x) {
		super.xstream(x);
	}

	@Override
	protected Object execute() throws Exception {
		return null;
	}
}
