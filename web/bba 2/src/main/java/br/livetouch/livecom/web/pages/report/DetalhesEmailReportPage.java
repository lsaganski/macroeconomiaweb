package br.livetouch.livecom.web.pages.report;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.EmailReport;
import br.livetouch.livecom.domain.vo.PushReportFiltro;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;

@Controller
@Scope("prototype")
public class DetalhesEmailReportPage extends RelatorioPage {
	
	public RelatorioFiltro filtro;
	public int page;
	public Long id;
	
	public String html;
	public EmailReport email;
	
	@Override
	public void onInit() {
		super.onInit();

		if(clear) {
			getContext().removeSessionAttribute(PushReportFiltro.SESSION_FILTRO_KEY);
		}
		
		if (id != null) {
			email = emailReportService.get(id);
			if (email != null) {
				html = StringUtils.isNotEmpty(email.getMsg()) ? email.getMsg() : "Email sem conteudo!";
				html = html.replace("\n", "").replace("\r", "").replace(System.lineSeparator(), "");
			}
		}

	}
	

	@Override
	public void onGet() {
		super.onGet();
	}
	
	@Override
	public String getContentType() {
		return super.getContentType();
	}
	
	@Override
	public String getTemplate() {
		return super.getTemplate();
	}
	
}
