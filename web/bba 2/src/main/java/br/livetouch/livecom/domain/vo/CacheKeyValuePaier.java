package br.livetouch.livecom.domain.vo;

import java.io.Serializable;


public interface CacheKeyValuePaier extends Serializable {
	long serialVersionUID = 1L;

	String getKey();
	String getValue();
}