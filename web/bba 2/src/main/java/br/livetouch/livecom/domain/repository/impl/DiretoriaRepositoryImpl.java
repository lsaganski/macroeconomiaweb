
package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Diretoria;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.repository.DiretoriaRepository;
import br.livetouch.livecom.domain.vo.Filtro;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class DiretoriaRepositoryImpl extends StringHibernateRepository<Diretoria> implements DiretoriaRepository {

	public DiretoriaRepositoryImpl() {
		super(Diretoria.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Diretoria> filterDiretoria(Filtro filtroDiretoria, Empresa empresa) {
		StringBuffer sb = new StringBuffer("FROM Diretoria d where 1=1 ");
		sb.append(" AND d.empresa.id = :empresaId");
		if(filtroDiretoria == null) {
			Query q = createQuery(sb.toString());
			q.setParameter("empresaId", empresa.getId());
			return q.list();
		}
		
		if(filtroDiretoria.nome != null && filtroDiretoria.codigo != null) {
			sb.append(" AND (lower(d.nome) like :nome OR lower(d.codigo) like :codigo");
		} else if(filtroDiretoria.nome != null) {
			sb.append(" AND lower(d.nome) like :nome");
		} else if(filtroDiretoria.codigo != null) {
			sb.append(" AND lower(d.codigo) like :codigo");
		}
		
		if(filtroDiretoria.diretoriaNaoExecutiva) {
			sb.append(" AND d.diretoriaExecutiva is not null");
		} else if(filtroDiretoria.executiva) {
			sb.append(" AND d.diretoriaExecutiva is null");
		}
		
		if(StringUtils.isNotEmpty(filtroDiretoria.dirExecId)) {
			sb.append(" AND d.diretoriaExecutiva.id = :dirExcId");
		}
		
		sb.append(" order by d.nome ");
		
		Query q = createQuery(sb.toString());
		q.setLong("empresaId", empresa.getId());
		
		if(filtroDiretoria.nome != null && filtroDiretoria.codigo != null) {
			q.setParameter("nome", "%" + filtroDiretoria.nome +"%");
			q.setParameter("codigo", "%" + filtroDiretoria.codigo +"%");
		} else if(filtroDiretoria.nome != null) {
			q.setParameter("nome", "%" + filtroDiretoria.nome +"%");
		} else if(filtroDiretoria.codigo != null) {
			q.setParameter("codigo", "%" + filtroDiretoria.codigo +"%");
		}
		
		if(StringUtils.isNotEmpty(filtroDiretoria.dirExecId)) {
			q.setParameter("dirExcId", Long.parseLong(filtroDiretoria.dirExecId));
		}
		
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findIdCodDiretorias(Long idEmpresa) {
		Query query = createQuery("select id,codigo from Diretoria where empresa.id = :idEmpresa");
		query.setLong("idEmpresa", idEmpresa);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Diretoria> findAll(Empresa empresa) {
		StringBuffer sb = new StringBuffer("FROM Diretoria d WHERE d.empresa.id = :empresaId");
		Query q = createQuery(sb.toString());
		q.setLong("empresaId", empresa.getId());
		q.setCacheable(true);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Diretoria findDiretoriaDefaultByEmpresa(Long empresaId) {
		Query q = createQuery("FROM Diretoria a where diretoriaExecutiva is not null and a.empresa.id = :empresaId");
		q.setLong("empresaId", empresaId);
		List<Funcao> list = q.list();
		Diretoria a = (Diretoria) (list.size() > 0 ? q.list().get(0) : null);
		return a;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Diretoria findDiretoriaExecutivaDefaultByEmpresa(Long empresaId) {
		Query q = createQuery("FROM Diretoria a where diretoriaExecutiva is null and a.empresa.id = :empresaId");
		q.setLong("empresaId", empresaId);
		List<Funcao> list = q.list();
		Diretoria a = (Diretoria) (list.size() > 0 ? q.list().get(0) : null);
		return a;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findIdNomeDiretorias(Long empresaId) {
		Query query = createQuery("select id,nome from Diretoria where empresa.id = :empresaId");
		query.setLong("empresaId", empresaId);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Diretoria> findByNome(String nome, int page, int maxRows, Empresa empresa) {
		Query query = getQueryFindByNome(nome, false, empresa);

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * maxRows;
			
		}
		query.setFirstResult(firstResult);
		
		if(maxRows != 0) {
			query.setMaxResults(maxRows);
		}

		query.setCacheable(true);
		List<Diretoria> list = query.list();

		return list;
	}
	
	private Query getQueryFindByNome(String nome, boolean count, Empresa empresa) {
		StringBuffer sb = new StringBuffer();

		if (count) {
			sb.append("select count(distinct d.id) ");
		} else {
			sb.append("select distinct d ");
		}

		sb.append(" from Diretoria d ");

		sb.append(" where 1=1 ");

		sb.append(" AND d.empresa = :empresa");
		
		if (StringUtils.isNotBlank(nome)) {
			sb.append(" and (upper(d.nome) like :nome)");
		}

		if (!count) {
			sb.append(" order by d.nome ");
		}

		Query query = createQuery(sb.toString());

		if (StringUtils.isNotBlank(nome)) {
			query.setParameter("nome", "%" + nome.toUpperCase() + "%");
		}

		query.setParameter("empresa", empresa);

		query.setCacheable(true);

		return query;
	}

	@Override
	public Long countByNome(String nome, int page, int maxRows, Empresa empresa) {
		Query query = getQueryFindByNome(nome, true, empresa);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Diretoria findByName(Diretoria diretoria, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Diretoria d where d.nome = :nome");
		sb.append(" and d.empresa = :empresa");
		
		if(diretoria.getId() != null) {
			sb.append(" and d.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("empresa", empresa);
		q.setParameter("nome", diretoria.getNome());
		
		if(diretoria.getId() != null) {
			q.setParameter("id", diretoria.getId());
		}
		
		List<Diretoria> list = q.list();
		Diretoria d = (Diretoria) (list.size() > 0 ? q.list().get(0) : null);
		return d;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Diretoria findByCodigo(Diretoria diretoria, Empresa empresa){
		
		StringBuffer sb = new StringBuffer("from Diretoria d where d.codigo = :codigo");
		sb.append(" and d.empresa = :empresa");
		
		if(diretoria.getId() != null) {
			sb.append(" and d.id != :id");
		}

		Query q = createQuery(sb.toString());
		
		q.setParameter("codigo", diretoria.getCodigo());
		q.setParameter("empresa", empresa);
		
		if(diretoria.getId() != null) {
			q.setParameter("id", diretoria.getId());
		}
			
		List<Diretoria> list = q.list();
		Diretoria d = (Diretoria) (list.size() > 0 ? q.list().get(0) : null);
		return d;
	}
	

	

}