package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostView;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.PostViewRepository;
import br.livetouch.livecom.domain.service.PostViewService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class PostViewServiceImpl  extends LivecomService<PostView> implements PostViewService {
	@Autowired
	private PostViewRepository rep;

	@Override
	public PostView get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(PostView c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<PostView> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void delete(PostView c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public List<PostView> findAllByUser(Usuario u) {
		return rep.findAllByUser(u);
	}

	@Override
	public long getCount() {
		return rep.getCount();
	}

	@Override
	public PostView findByUserPost(Usuario u, Post p) {
		return rep.findByUserPost(u, p);
	}

	@Override
	public List<PostView> findAllByPost(Post post) {
		return rep.findAllByPost(post);
	}

	@Override
	public void visualizarPosts(List<Post> posts, Usuario user) {
		rep.insertPostView(posts, user);	
		rep.visualizarPosts(posts, user);	
	}

}