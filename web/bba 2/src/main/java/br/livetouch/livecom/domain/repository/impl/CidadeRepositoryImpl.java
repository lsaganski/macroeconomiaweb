package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Cidade;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.enums.Estado;
import br.livetouch.livecom.domain.repository.CidadeRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class CidadeRepositoryImpl extends StringHibernateRepository<Cidade> implements CidadeRepository {

	public CidadeRepositoryImpl() {
		super(Cidade.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cidade> findAllByEstado(Estado estado) {
		StringBuilder sql = new StringBuilder("from Cidade c where 1=1");
		sql.append(" and estado=?");
		sql.append(" order by c.nome ");
		
		Query q = createQuery(sql.toString());
		
		q.setInteger(0, estado.ordinal());
		
		List<Cidade> list = q.list();
		return list;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Cidade> findAllOrderByEstado() {
		StringBuilder sql = new StringBuilder("from Cidade c ");
		sql.append(" order by c.estado ");
		
		Query q = createQuery(sql.toString());
		
		List<Cidade> list = q.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cidade> findByNome(String nome, Estado estado, int page, int maxRows) {
	
		Query query = getQueryFindByNome(nome, estado, false);

		int firstResult = 0;
		if (page != 0) {
			firstResult = page * maxRows;
			
		}
		query.setFirstResult(firstResult);
		
		if(maxRows != 0) {
			query.setMaxResults(maxRows);
		}

		query.setCacheable(true);
		List<Cidade> list = query.list();

		return list;
	}
	
	private Query getQueryFindByNome(String nome, Estado estado, boolean count) {
		StringBuffer sb = new StringBuffer();

		if (count) {
			sb.append("select count(distinct f.id) ");
		} else {
			sb.append("select distinct f ");
		}

		sb.append(" from Cidade f ");

		sb.append(" where 1=1 ");

		if (StringUtils.isNotBlank(nome)) {
			sb.append(" and (upper(f.nome) like :nome)");
		}

		if (estado != null) {
			sb.append(" and f.estado = :estado");
		}

		if (!count) {
			sb.append(" order by f.nome ");
		}

		Query query = createQuery(sb.toString());

		if (StringUtils.isNotBlank(nome)) {
			query.setParameter("nome", "%" + nome.toUpperCase() + "%");
		}
		
		if (estado != null) {
			query.setParameter("estado", estado);
		}

		return query;
	}

	@Override
	public Long countByNome(String nome, Estado estado, int page, int maxRows) {
		Query query = getQueryFindByNome(nome, estado, true);

		Object obj = query.uniqueResult();

		return new Long(obj.toString());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Cidade findByName(Cidade cidade, Empresa empresa) {
		StringBuffer sb = new StringBuffer("from Cidade c where c.nome = :nome");
//		sb.append(" and c.empresa = :empresa");
		sb.append(" and c.estado = :estado");
		
		if(cidade.getId() != null) {
			sb.append(" and c.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
//		q.setParameter("empresa", empresa);
		q.setParameter("nome", cidade.getNome());
		q.setParameter("estado", cidade.getEstado());
		
		if(cidade.getId() != null) {
			q.setParameter("id", cidade.getId());
		}
		
		List<Cidade> list = q.list();
		Cidade c = (Cidade) (list.size() > 0 ? q.list().get(0) : null);
		return c;
	}

}