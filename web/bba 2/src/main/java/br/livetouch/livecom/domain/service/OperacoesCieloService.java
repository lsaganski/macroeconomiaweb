package br.livetouch.livecom.domain.service;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.OperacoesCielo;
import br.livetouch.livecom.domain.vo.OperacoesCieloVO;
import net.livetouch.tiger.ddd.DomainException;

public interface OperacoesCieloService extends Service<OperacoesCielo> {

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(OperacoesCielo transacao, Empresa empresa) throws DomainException;

	@Transactional(rollbackFor=Exception.class)
	void delete(OperacoesCielo transacao) throws DomainException;

	List<Object[]> findIdDataTransacao(Long idEmpresa);
	
	List<OperacoesCielo> transacoesByEmpresa(Empresa empresa, int page, Integer max);

	long getCountTransacoesByEmpresa(Empresa empresa, int page, Integer max);
	
	String csvTransacao(Empresa empresa);
	
	List<OperacoesCielo> findAll(Empresa empresa);
	
	List<OperacoesCieloVO> graficoPicoTps();

	List<OperacoesCieloVO> graficoMomento();
	
	List<OperacoesCieloVO> graficoTransacao();
	
	OperacoesCielo getByData(Date data);

	void deleteAll(Long empresaId);

}
