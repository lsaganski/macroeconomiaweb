package br.livetouch.livecom.web.pages.pages;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.web.pages.admin.LivecomLogadoPage;

@Controller
@Scope("prototype")
public class SobreEsteAplicativoPage extends LivecomLogadoPage {
}
