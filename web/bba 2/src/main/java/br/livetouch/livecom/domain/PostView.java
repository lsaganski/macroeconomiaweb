package br.livetouch.livecom.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.utils.DateUtils;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PostView extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "POSTVIEW_SEQ")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id", nullable = true)
	private Post post;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", nullable = true)
	private Usuario usuario;
	
	// 0,1,2,3,4,5
	private int contador;
	private int contadorLido;

	private Boolean visualizado;
	private Boolean lido;
	
	private Date data;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Post getPost() {
		return post;
	}
	
	public void setPost(Post post) {
		this.post = post;
	}
	
	@Override
	public String toString() {
		return "Favorito [post=" + post + ", usuario=" + usuario + "]";
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	public Date getData() {
		if(data == null) {
			data = new Date();
		}
		return data;
	}
	
	public int getContador() {
		return contador;
	}
	
	public void setContador(int contador) {
		this.contador = contador;
	}

	public String getDataString() {
		return net.livetouch.extras.util.DateUtils.toString(data, "dd/MM/yyyy HH:mm:ss");
	}
	
	public String getDataStringHojeOntem() {
		return DateUtils.toDateStringHojeOntem(getData());
	}

	public void incrementContador() {
		contador++;
	}

	public void incrementContadorLido() {
		contadorLido++;
	}

	public boolean isVisualizado() {
		if(visualizado == null) {
			return false;
		}
		return visualizado;
	}

	public void setVisualizado(boolean visualizado) {
		this.visualizado = visualizado;
	}

	public Boolean isLido() {
		if(lido == null) {
			return false;
		}
		return lido;
	}

	public void setLido(Boolean acessado) {
		this.lido = acessado;
	}

	public int getContadorLido() {
		return contadorLido;
	}

	public void setContadorLido(int contadorLido) {
		this.contadorLido = contadorLido;
	}

}