package br.livetouch.livecom.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.jdbc.Work;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Service;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Notification;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.service.NotificationService;
import br.livetouch.livecom.domain.vo.UserToPushVO;
import br.livetouch.livecom.utils.DateUtils;
import br.livetouch.livecom.utils.JDBCUtils;
import br.livetouch.livecom.utils.ListUtils;

@Service
public class InsertNotificationWork implements Work {

	protected static final Logger log = Log.getLogger(InsertNotificationWork.class);

	private NotificationService notificationService;
	private ApplicationContext applicationContext;

	private Notification notification;

	private List<UserToPushVO> users;

	private Post post;

	private boolean insert;

	public void init(ApplicationContext applicationContext, Notification n, List<UserToPushVO> listUsers, Post p, boolean insert) {
		this.applicationContext = applicationContext;
		this.notificationService = applicationContext.getBean(NotificationService.class);
		this.notification = n;
		this.users = listUsers;
		this.post = p;
		this.insert = insert;
		
	}
	
	@Override
	public void execute(Connection connection) throws SQLException {
		log("\n---------------------");
		log("Notification " + notification.toString());
		
		JDBCUtils.printConnectionInfo(connection);
		Date start = new Date();
		try {
			
			if (this.notification == null) {
				throw new IllegalArgumentException("notification inválido");
			}
			
			Boolean newNotification = this.insert;

			connection.setAutoCommit(false);
			int totalRaws = 0;
			
			if (ListUtils.isNotEmpty(users)) {
				totalRaws = insertOrUpdate(connection, start, notification, post, users, newNotification);
			}
			
			log(">> Salvando notification pai: " + totalRaws);

			notificationService.save(notification);
			
			log("<< Notification pai criada: " + notification.getId());

		} catch (Exception e) {
			throw new SQLException(e.getMessage(), e);
		} finally {
			Date end = new Date();

			log("Início: " + DateUtils.toString(start, DateUtils.DATE_TIME_AM_PM));
			log("Final: " + DateUtils.toString(end, DateUtils.DATE_TIME_AM_PM));

			log("Tempo segundos: " + DateUtils.getDiffInSeconds(start, end) + " segundos.");
			log("Tempo minutos: " + DateUtils.getDiffInMinutes(start, end));

			boolean fastMode = ParametrosMap.getInstance().getBoolean(Params.PUSH_FAST_MODE, false);
			if (fastMode) {
				log("Start Job ForcePushJob!");
				JobUtil.executeJob(applicationContext, ForcePushJob.class, null);
			}
		}
	}
	
	/**
	 * Faz com Statement
	 */
	private int insertOrUpdate(Connection connection, Date now, Notification notification, Post post, List<UserToPushVO> users, Boolean newNotification) throws SQLException {
		Dialect dialect = getDialect();
		String sqlInsert = getStmtInsert(dialect, connection);
		String sqlUpdate = getStmtUpdate(dialect, connection);

		PreparedStatement stmtInsert = connection.prepareStatement(sqlInsert, Statement.RETURN_GENERATED_KEYS);

		int BATCH_SIZE = ParametrosMap.getInstance().getInt("notifications.work.batchSize", 10000);
		
		log(">> Start batch (qtde="+BATCH_SIZE+") para inserir/atualizar " + users.size() + " notifications");

		int row = 0;
		
		try {
			Timestamp sqlTimestampNow = DateUtils.toSqlTimestamp(now);

			int total = users.size();
			int countInsertUpdates = 0;
			int countTemp = 0;

			StringBuilder usersUpdate = new StringBuilder();
			for (UserToPushVO userPush : users) {
				Boolean insert = newNotification;
				row++;

				int i = 1;

				boolean sendPush = notification.isSendPush() && userPush.getPush();
				if (insert) {
					countTemp++;
					countInsertUpdates++;
					stmtInsert.setLong(i++, userPush.getId());
					stmtInsert.setLong(i++, notification.getId());
					stmtInsert.setBoolean(i++, false);
					stmtInsert.setBoolean(i++, userPush.getNotification());
					stmtInsert.addBatch();
					if (countTemp > BATCH_SIZE) {
						log("executeBatch() Insert " + countInsertUpdates + " de " + total);
						stmtInsert.executeBatch();
						stmtInsert.clearBatch();
						countTemp = 0;
					}
				} else {
					countTemp++;
					countInsertUpdates++;
					usersUpdate.append(userPush.getId());

					if (countTemp == BATCH_SIZE || row == total ) {
						String sql = String.format(sqlUpdate, usersUpdate.toString());
						PreparedStatement stmtUpdate = null;
						try {
							stmtUpdate = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
							if (sendPush) {
								stmtUpdate.setNull(i++, java.sql.Types.DATE);
							} else {
								stmtUpdate.setTimestamp(i++, sqlTimestampNow);
							}
							stmtUpdate.setBoolean(i++, userPush.getNotification());
							stmtUpdate.setLong(i++, notification.getId());
							log("execute() Update: " + countInsertUpdates + " de " + total);
							stmtUpdate.execute();
							countTemp = 0;
							usersUpdate = new  StringBuilder();
						} finally {
							JdbcUtils.closeStatement(stmtUpdate);
						}
					} else {
						usersUpdate.append(",");
					}
				}
			}

			log("executeBatch(): Final " + countInsertUpdates + " de " + total);
			stmtInsert.executeBatch();
			stmtInsert.clearBatch();

			log("<< End batch, total rows: " + countInsertUpdates);

			return countInsertUpdates;
		} catch (Exception e) {
			// FIXME salvar a notifici como nao enviada... talvez estudar se precisa usar REQUIRES_NEW. Ver com Lecheta.
			// Se simular exception, a notific pai 'e criada e filhas nao.. Precisa ter contingencia.
			
			log.error("Erro row[" + row + "] " + e.getMessage(), e);
			throw e;
		} finally {
			JdbcUtils.closeStatement(stmtInsert);
		}
	}

	private String getStmtUpdate(Dialect dialect, Connection conn) throws SQLException {
		return "UPDATE notification_usuario SET data_push = ?, visivel=? WHERE notification_id = ? AND usuario_id in(%s)";
	}

	private void log(String s) {
		log.debug(s);
	}

	private String getStmtInsert(Dialect dialect, Connection conn) throws SQLException {
		return "INSERT INTO notification_usuario (usuario_id, notification_id, lida, visivel) values (?, ?, ?, ?)";
	}
	
	private Dialect getDialect() {
		SessionFactory sessionFactory = applicationContext.getBean(SessionFactory.class);
		return ((SessionFactoryImplementor) sessionFactory).getDialect();
	}
}
