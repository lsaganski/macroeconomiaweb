package br.livetouch.livecom.web.pages.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.rest.oauth.ConsumerKeyMap;
import br.livetouch.livecom.rest.oauth.LivecomConsumer;
import net.livetouch.click.table.PaginacaoTable;
import net.sf.click.control.Column;

/**
 * Relatório de acessos
 * 
 */
@Controller
@Scope("prototype")
public class MapsPage extends LivecomAdminPage {

	public PaginacaoTable table = new PaginacaoTable();

	@Override
	public void onInit() {
		super.onInit();

		table();
	}

	private void table() {

		Column c = new Column("key");
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("secret");
		c.setAttribute("align", "left");
		table.addColumn(c);

		c = new Column("owner");
		c.setAttribute("align", "left");
		table.addColumn(c);

	}

	@Override
	public void onRender() {
		super.onRender();

		ConsumerKeyMap map = ConsumerKeyMap.getInstance();
		ConcurrentHashMap<String, LivecomConsumer> values = map.getAll();
		List<LivecomConsumer> list = new ArrayList<LivecomConsumer>();
		
		for (LivecomConsumer lv : values.values()) {
		    list.add(lv);
		}

		table.setRowList(list);
	}
}
