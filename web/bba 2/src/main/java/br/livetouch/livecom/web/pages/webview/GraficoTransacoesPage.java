package br.livetouch.livecom.web.pages.webview;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Livecom;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.web.pages.admin.LivecomPage;

/**
 * Livecom:
 * http://livecom.livetouchdev.com.br/webview/graficoTransacoes.htm?userId=2524&grafico=1
 * 
 * Cielo Prod:
 * https://livecom.cielo.com.br/webview/graficoTransacoes.htm?userId=760&grafico=1
 *  
 */
@Controller
@Scope("prototype")
public class GraficoTransacoesPage extends LivecomPage {

	public String msgErro;
	public int page;
	public Boolean isPermissao = false;
	public String grafico;
	public String debug;

	@Override
	public void onInit() {
		super.onInit();
		grafico = getParam("grafico");
		debug = getParam("debug");
		if(StringUtils.isEmpty(grafico)) {
			grafico = "1";
		}
		String userId = getParam("userId");
		if(StringUtils.isNotEmpty(userId) && StringUtils.isNumeric(userId)) {
			Usuario usuario = usuarioService.get(Long.parseLong(userId.trim()));
			if(usuario != null) {
				isPermissao = Livecom.getInstance().hasPermissao(ParamsPermissao.VIZUALIZAR_RELATORIOS, usuario);
			}
		}
	}
	

	@Override
	public String getTemplate() {
		return "/webview/webviews.htm";
	}
	
	@Override
	public void onGet() {
		super.onGet();
	}

	@Override
	public void onRender() {
		super.onRender();
	}
}
