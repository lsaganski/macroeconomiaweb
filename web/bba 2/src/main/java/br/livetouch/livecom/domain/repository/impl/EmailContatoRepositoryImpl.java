package br.livetouch.livecom.domain.repository.impl;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.EmailContato;
import br.livetouch.livecom.domain.repository.EmailContatoRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class EmailContatoRepositoryImpl extends StringHibernateRepository<EmailContato> implements EmailContatoRepository {

	public EmailContatoRepositoryImpl() {
		super(EmailContato.class);
	}

}