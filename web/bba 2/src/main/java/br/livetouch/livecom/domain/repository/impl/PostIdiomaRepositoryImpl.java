package br.livetouch.livecom.domain.repository.impl;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.PostIdioma;
import br.livetouch.livecom.domain.repository.PostIdiomaRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class PostIdiomaRepositoryImpl extends StringHibernateRepository<PostIdioma> implements PostIdiomaRepository {

	public PostIdiomaRepositoryImpl() {
		super(PostIdioma.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<PostIdioma> findByPost(Post p) {
		StringBuffer sb = new StringBuffer("select pi from PostIdioma pi inner join pi.post p WHERE 1=1 ");
		
		if(p != null) {
			sb.append(" AND p = :post ");
		}
		
		sb.append(" order by pi.idioma.nome ");
		
		Query q = createQuery(sb.toString());
		
		if(p != null) {
			q.setParameter("post", p);
		}
		
		Set<PostIdioma> list = new HashSet<PostIdioma>(q.list());
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<PostIdioma> findByIdioma(Idioma i) {
		StringBuffer sb = new StringBuffer("select pi from PostIdioma pi inner join pi.idioma i WHERE 1=1 ");
		
		if(i != null) {
			sb.append(" AND i = :idioma ");
		}
		
		sb.append(" order by i.nome ");
		
		Query q = createQuery(sb.toString());
		
		if(i != null) {
			q.setParameter("idioma", i);
		}
		
		Set<PostIdioma> list = new HashSet<PostIdioma>(q.list());
		return list;
	}
	
}