package br.livetouch.livecom.wordpress;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.livetouch.infra.util.HttpHelper;
import br.livetouch.livecom.domain.Comentario;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.PostRepository;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.vo.PostVO;

/**
 * API para fazer a integração via json com o wordpress:
 * 
 * https://wordpress.org/plugins/json-api/other_notes/
 * 
 * @author juillianlee
 *
 */
@Service
public class WordPressImpl implements WordPress {

	@Autowired
	private PostRepository rep;

	@Autowired
	private PostService postService;

	public static void main(String[] args) {
		WordPressImpl wordPressImpl = new WordPressImpl();
		wordPressImpl.postComentario(10L, "juillian lee", "juillianlee@hotmail.com", "asdadsa1231");
	}

	@Override
	public String getUrl() {
		return ParametrosMap.getInstance().get(Params.WORDPRESS_SITE, "http://localhost/wordpress");
	}

	@Override
	public List<PostVO> getPosts(Usuario user, Integer maxRows, Integer page) {
		try {
			String response = HttpHelper.doGet(getUrl(), String.format("json=%s&page=%s", maxRows, page));
			JSONObject json = new JSONObject(response);
			if (json.has("posts")) {
				Type listType = new TypeToken<List<Post>>() {
				}.getType();
				List<Post> posts = new Gson().fromJson(json.optJSONArray("posts").toString(), listType);

				List<br.livetouch.livecom.domain.Post> postsLivecom = savePosts(posts);
				return postService.toListVo(user, postsLivecom);
			}
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Metodo para salvar os posts na base de dados do livecom,
	 * Salva apenas o id do wordpress até o momento
	 * @param posts
	 * @return
	 */
	private List<br.livetouch.livecom.domain.Post> savePosts(List<Post> posts) {
		List<br.livetouch.livecom.domain.Post> postsLivecom = new ArrayList<>();
		for (Post post : posts) {
			br.livetouch.livecom.domain.Post p = rep.findByIdWordpress(post.getId());
			if (p == null) {
				p = new br.livetouch.livecom.domain.Post();
				p.setIdWordpress(post.getId());
			}
			rep.saveOrUpdate(p);
			br.livetouch.livecom.domain.Post copyToPostLivecom = Post.copyToPostLivecom(post);
			copyToPostLivecom.setId(p.getId());
			
			if(post.getComments() != null) {
				List<Comments> comments = post.getComments();
				Set<Comentario> comentarios = new HashSet<>();
				for (Comments c : comments) {
					Comentario comentario = new Comentario();
					comentario.setId(c.getId());
					comentario.setComentario(c.getContent());
					Usuario usuario = new Usuario();
					usuario.setNome(c.getName());
					comentario.setUsuario(usuario);
					comentarios.add(comentario);
				}
				copyToPostLivecom.setComentarios(comentarios);
			}
			postsLivecom.add(copyToPostLivecom);
		}
		return postsLivecom;
	}

	@Override
	public List<PostVO> getPostByTitle(String title) {
		try {
			String response = HttpHelper.doGet(getUrl(), "json=get_post&slug=" + title);
			JSONObject json = new JSONObject(response);
			if (json.has("post")) {
				Post post = new Gson().fromJson(json.optJSONObject("post").toString(), Post.class);
				List<PostVO> listVO = new ArrayList<>();
				// listVO.add(Post.copyToPost(post));
				return listVO;
			}
		} catch (IOException | JSONException e) {
		}
		return null;
	}

	@Override
	public String getPostById(String id) {
		return null;
	}

	@Override
	public String postComentario(Long postId, String nameUser, String emailUser, String msg) {
		String mensagem = null;
		try {
			String params = String.format(
					"json=submit_comment&post_id=%s&name=%s&email=%s&content=%s"
					, postId
					, URLEncoder.encode(nameUser, "UTF-8")
					, URLEncoder.encode(emailUser, "UTF-8")
					, URLEncoder.encode(msg, "UTF-8"));
			String response = HttpHelper.doGet(getUrl(),  params);
			JSONObject json = new JSONObject(response);
			String status = json.optString("status");
			switch (status) {
				case "pending":
					mensagem = "Comentário enviado com sucesso, está aguardando aprovação da moderação.";
				break;
				default:
					mensagem = "Comentário enviado com sucesso.";
				break;
			}
		} catch (IOException | JSONException e) {
			String message = e.getMessage();
			if(message.contains("Server returned HTTP response code: 409 for")) {
				mensagem = "Detectado comentário repetido; parece que você já disse isso!";
			} else if(message.contains("Server returned HTTP response code: 429 for URL")) {
				mensagem = "Você está enviando comentários rápido demais. Calma aí.";
			} else {
				e.printStackTrace();
			}
		}
		return mensagem;
	}

	@Override
	public Boolean isWordpressOn() {
		String wordpressOn = ParametrosMap.getInstance().get(Params.WORDPRESS_ON, "0");
		return "1".equals(wordpressOn);
	}
}
