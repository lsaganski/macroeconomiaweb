package br.livetouch.livecom.domain.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Favorito;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.RelatorioFiltro;
import br.livetouch.livecom.domain.vo.RelatorioVisualizacaoVO;
import br.livetouch.spring.SearchInfo;
import net.livetouch.tiger.ddd.DomainException;

public interface FavoritoService extends Service<Favorito> {
	@Transactional(rollbackFor=Exception.class)
	Favorito favoritar(Usuario u, Post post, boolean isFavorito) throws DomainException;
	
	List<Favorito> findAll();
	
	List<Favorito> findAllFavoritosNotificationByUser(Usuario u);

	@Transactional(rollbackFor=Exception.class)
	void saveOrUpdate(Favorito c) throws DomainException;
	
	@Transactional(rollbackFor=Exception.class)
	void delete(Favorito c) throws DomainException;

	Favorito findByUserAndPost(Usuario u, Post post);
	
	Favorito findByUserAndFile(Usuario u, Arquivo file);

	List<Favorito> findAllByUser(Usuario userInfo);

	@Transactional(rollbackFor=Exception.class)
	void clearNotification(Long id) throws DomainException;

	List<Post> findAllPostsByUser(Usuario userInfo, SearchInfo search);

	List<Favorito> findAllByPosts(Usuario user, List<Long> ids);

	List<RelatorioVisualizacaoVO> reportFavoritos(RelatorioFiltro filtro) throws DomainException;

	
}
