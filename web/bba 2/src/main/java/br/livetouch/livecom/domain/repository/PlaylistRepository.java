package br.livetouch.livecom.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Playlist;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.Usuario;

@Repository
public interface PlaylistRepository extends net.livetouch.tiger.ddd.repository.Repository<Playlist> {

	List<Playlist> findAllByUser(Usuario u);

	List<Post> findAllPostsByUser(Usuario u, int page, int max);

	Playlist findByUserPost(Usuario u, Post p);
	
}