package br.livetouch.livecom.email;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;

public class SmtpEmailManager implements EmailManager {
	protected static Logger log = Log.getLogger(EmailUtils.class);
	private String smtp;
	private int smtpPort;
	private String smtpUser;
	private String smtpPwd;
	private boolean smtpSSL;

	public SmtpEmailManager() {
		
		ParametrosMap params = ParametrosMap.getInstance();

//		this.smtp = params.get("email.smtp_server","smtp.telecorp.com.br");
//		this.smtpPort = NumberUtils.toInt(params.get("email.smtp_port"),25);
//		this.smtpUser = params.get("email.smtp_server","wasys@telecorp.com.br");
//		this.smtpPwd = params.get("email.smtp_password","passSMTP");

		// https://console.aws.amazon.com/iam (ses-livecom)
		this.smtp = params.get(Params.EMAIL_SMTP_SERVER,"email-smtp.us-east-1.amazonaws.com");
		this.smtpPort = NumberUtils.toInt(params.get(Params.EMAIL_SMTP_PORT),25);
		this.smtpUser = params.get(Params.EMAIL_SMTP_USER,"AKIAJYT22PIFZ25K6R5Q");
		this.smtpPwd = params.get(Params.EMAIL_SMTP_PASSWORD,"AieXbWqtVbQ8Et3xcyeIKkkYIrrDjon5+KZeacjuYfP2");
		this.smtpSSL = "1".equals(params.get(Params.EMAIL_SMTP_SSL,"1"));
	}

	@Override
	public boolean sendEmail(String to, String subject, String body, boolean html) throws IOException, EmailException {
		ParametrosMap params = ParametrosMap.getInstance();
		String from = params.get(Params.EMAIL_EMAIL_ADMIN,"livecom@livetouchdev.com.br");
		
		return sendEmail(from,to, subject, body, html);
	}
	
	@Override
	public boolean sendEmail(String from, String to, String subject, String msg, boolean html) throws IOException, EmailException {
		String tos[] = new String[]{to};
		return sendEmail(from, tos, subject, msg, html, new String[]{});
	}

	@Override
	public boolean sendEmail(String from, String tos[], String subject, String msg, boolean html, String[] bcc) throws IOException, EmailException {
		if(tos == null || tos.length == 0) {
			return false;
		}
		log.debug(String.format("sendEmail smtp[%s:%d]: from [%s] to [%s] subject[%s] msg[%s] html [%s]", smtp,smtpPort,from,tos,subject,msg,html));
		try {
			Email email = html ? new HtmlEmail() : new SimpleEmail();
			if(smtpSSL) {
				log.debug("sendEmail SSL on.");
				email.setSSLOnConnect(smtpSSL);
			}
			email.setHostName(smtp);
			email.setSmtpPort(smtpPort);
			if(StringUtils.isNotEmpty(smtpUser) && StringUtils.isNotEmpty(smtpPwd)) {
				log.debug(String.format("SMTM user [%s] pwd [%s]", smtpUser, smtpPwd));
				email.setAuthenticator(new DefaultAuthenticator(smtpUser, smtpPwd));
			}
			email.setFrom(from);
			email.setSubject(subject);
			
			if (html) {
				StringBuffer sb = new StringBuffer();
				sb.append("<html><body>");
				sb.append(msg);
				sb.append("</body></html>");
				msg = sb.toString();
			}
			if(html) {
				HtmlEmail htmlEmail = (HtmlEmail) email;
				htmlEmail.setHtmlMsg(msg);
			} else {
				email.setMsg(msg);
			}
			
			for (String to : tos) {
				email.addTo(to);
			}

			for (String bc : bcc) {
				email.addBcc(bc);
			}

			email.send();

			logReport(">> Email enviado com sucesso para ["+tos+"].",null);
			
			return true;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			logReport("Erro ao enviar email to ["+tos+"]: " + e.getMessage(), e);
			throw e;
		}
	}

	private void logReport(String msg, Exception e) {
		if(e == null) {
			log.debug(msg);
		} else {
			log.error(msg, e);
		}
	}

}
