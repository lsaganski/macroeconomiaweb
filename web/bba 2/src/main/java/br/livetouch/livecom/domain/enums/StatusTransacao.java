/**
 * 
 */
package br.livetouch.livecom.domain.enums;

public enum StatusTransacao {
	ERRO("ERRO"), 
	OK("OK");

	private final String label;
	StatusTransacao(String label){
		this.label = label;
	}

	@Override
	public String toString() {
		return label != null ? label : "?";
	}
}