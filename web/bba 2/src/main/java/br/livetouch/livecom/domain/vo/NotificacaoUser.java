package br.livetouch.livecom.domain.vo;

import java.util.List;

import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;

@Deprecated
public class NotificacaoUser {

	public Usuario user;
	
	public List<MensagemConversa> conversasNaoLidas;

	public List<NotificacaoVO> notificacoes;

}
