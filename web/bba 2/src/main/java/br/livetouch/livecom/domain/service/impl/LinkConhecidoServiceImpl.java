package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LinkConhecido;
import br.livetouch.livecom.domain.repository.LinkConhecidoRepository;
import br.livetouch.livecom.domain.service.LinkConhecidoService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class LinkConhecidoServiceImpl implements LinkConhecidoService {

	@Autowired
	private LinkConhecidoRepository rep;

	@Override
	public LinkConhecido get(Long id) {
		return rep.get(id);
	}
	
	public List<LinkConhecido> findAll(){
		return rep.findAll();
	}
	
	public void saveOrUpdate(LinkConhecido link, Empresa empresa){
		link.setEmpresa(empresa);
		rep.saveOrUpdate(link);
	}
	
	public void save(LinkConhecido link, Empresa empresa){
		rep.save(link, empresa);
	}
	
	
	public void delete(LinkConhecido link){
		rep.delete(link);
	}
	
	public Response valid(LinkConhecido link) throws DomainException{
	
		/*if(link == null){
			throw new DomainException("Objeto link null");
			log.debug("Ja existe uma diretoria com este codigo");
			return Response.ok(MessageResult.error("Ja existe uma diretoria com este codigo")).build();
		}*/
		return null;		
		
	}

	@Override
	public LinkConhecido findByName(LinkConhecido link) {
		return rep.findByName(link);
	}

	@Override
	public LinkConhecido findByDominio(LinkConhecido link) {
		return rep.findByDominio(link);
	}
	
	
}
