package br.livetouch.livecom.web.pages.training.admin;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.web.pages.training.TrainingUtil;
import net.livetouch.tiger.ddd.DomainException;
import net.sf.click.control.ActionLink;
import net.sf.click.control.Column;
import net.sf.click.control.FieldSet;
import net.sf.click.control.Form;
import net.sf.click.control.Submit;
import net.sf.click.control.Table;
import net.sf.click.control.TextField;
import net.sf.click.extras.control.FieldColumn;
import net.sf.click.extras.control.FormTable;
import net.sf.click.extras.control.LinkDecorator;

@Controller
@Scope("prototype")
public class ParametrosPage extends TrainingAdminPage {

	public Form formNovo = new Form();
	public Long id = new Long(1);
	private List<Parametro> parametros;
	public String nome;
	public String valor;
	
	public FormTable table;
	
	public ActionLink deleteCustomer = new ActionLink("delete", "Excluir", this, "onDeleteClick");
	
	public Form form = new Form("form") {

	        private static final long serialVersionUID = 1L;
	        @Override
	        public boolean onProcess() {
	            if (form.isFormSubmission()) {
	                return super.onProcess();
	            } else {
	                deleteCustomer.onProcess();
	                return table.onProcess();
	            }
	        }
	    };
	    
	@Override
	public void onInit() {
		formNovo.add(new TextField("nome", true));
		formNovo.add(new TextField("valor", true));
		formNovo.add(new Submit("Salvar Novo",this,"salvar"));
		
		table = new FormTable("table", form);
		
		table.setClass(Table.CLASS_SIMPLE);
        table.setWidth("700px");
        table.setPageSize(50);
        table.setShowBanner(true);
        
        table.addColumn(new Column("id"));
        
        FieldColumn column = new FieldColumn("nome", new TextField());
        column.getField().setRequired(true);
        column.setVerticalAlign("baseline");
        table.addColumn(column);
        
        column = new FieldColumn("valor", new TextField());
        column.getField().setRequired(true);
        table.addColumn(column);
        
        Column actionColumn = new Column("Action");
        actionColumn.setSortable(false);
        ActionLink[] links = new ActionLink[]{deleteCustomer};
        actionColumn.setDecorator(new LinkDecorator(table, links, "id"));
        table.addColumn(actionColumn);
        
        deleteCustomer.setAttribute("onclick", "return window.confirm('Deseja deletar?');");
       
        table.getForm().add(new Submit("update", "Salvar", this, "salvarTodos"));
        table.getForm().add(new Submit("cancel","cancelar", this, "cancelar"));
  
        table.setSortable(true);
        
        FieldSet fieldSet = new FieldSet("parametros");
        
        form.add(fieldSet);
        
        fieldSet.add(table);
        
        addControl(formNovo);
        addControl(form);
        
        refreshTable();

        setFormTextWidth(formNovo);
        setFormTextWidth(form);
        setFormTextWidth(table.getForm());
        TrainingUtil.setTrainingStyle(formNovo);
        TrainingUtil.setTrainingStyle(form);
	}
	
	private void refreshTable() {
		parametros = parametroService.findAll();
		table.setRowList(parametros);
	}

	public boolean cancelar() {
		refresh();
		refreshTable();
		return false;
	}

	public boolean salvar() {
		if(form.isValid()) {
			salvar(nome, valor);
		}
		return false;
	}

	private Parametro salvar(String nome, String valor) {
		Parametro p = parametroService.findByNome(nome, getEmpresa());
		if(p == null) {
			p = new Parametro();
			p.setNome(nome);
		}
		p.setValor(valor);
		try {
			parametroService.saveOrUpdate(p);
		} catch (DomainException e) {
			formNovo.setError(e.getMessage());
		}
		return p;
	}


	public boolean salvarTodos() {
		
		for (Parametro c : parametros) {
			try {
				parametroService.saveOrUpdate(c);
			} catch (DomainException e) {
				e.printStackTrace();
			}
		}

		refresh();

		return false;
	}
	
	public boolean onDeleteClick() throws DomainException {
		 long id = deleteCustomer.getValueLong();
	     Parametro parametro =  parametroService.get(id);
	     if (parametro != null) {
	    	 parametroService.delete(parametro); 
	     }
	     refreshTable();
	     return true;
	}

	
	@Override
	public void onRender() {
		super.onRender();
	}
}