package br.livetouch.livecom.domain.service;



/**
 * @author ricardo
 *
 * @param <T>
 */
public interface Service<T> {

	public abstract T get(Long id);
}