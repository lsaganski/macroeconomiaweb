package br.livetouch.livecom.domain.repository.impl;


import br.livetouch.livecom.domain.Card;
import br.livetouch.livecom.domain.repository.CardRepository;

@org.springframework.stereotype.Repository
public class CardRepositoryImpl extends LivecomRepository<Card> implements CardRepository{
	public CardRepositoryImpl() {
		super(Card.class);
	}
}
