package br.livetouch.livecom.utils;

import java.util.Date;

public class LivecomDateUtils {

	public static String getDateString(Date dt) {
		if(dt == null) {
			return "";
		}
		return DateUtils.toString(dt, "dd/MM/yyyy HH:mm:ss");
	
	}
	
	public static String getDataStringHojeOntem(Date dt) {
		if(dt == null) {
			return "";
		}
		return DateUtils.toDateStringHojeOntem(dt);
	
	}
}
