package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Canal;
import br.livetouch.livecom.domain.repository.CanalRepository;
import br.livetouch.livecom.domain.service.CanalService;
import net.livetouch.extras.util.CSVBody;
import net.livetouch.extras.util.CSVGenerator;
import net.livetouch.extras.util.CSVHeader;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class CanalServiceImpl implements CanalService {
	@Autowired
	private CanalRepository rep;

	@Override
	public Canal get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Canal c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Canal> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void delete(Canal c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public String csvCanal() {
		List<Canal> findAll = findAll();
		CSVGenerator generator = new CSVGenerator();
		CSVHeader header = generator.getHeader();
		CSVBody body = generator.getBody();
		header.add("CODIGO").add("NOME");
		for (Canal c : findAll) {
			body.add(c.getCodigoDesc()).add(c.getNome());
			body.end();
		}
		return generator.toString();
	}
}
