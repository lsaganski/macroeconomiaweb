package br.livetouch.livecom.wordpress;

import java.util.List;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.PostVO;

public interface WordPress {
	public Boolean isWordpressOn();
	public String getUrl();
	public List<PostVO> getPostByTitle(String id);
	public String getPostById(String id);
	List<PostVO> getPosts(Usuario user, Integer maxRows, Integer page);
	public String postComentario(Long postId, String nameUser, String emailUser, String msg);
	
}
