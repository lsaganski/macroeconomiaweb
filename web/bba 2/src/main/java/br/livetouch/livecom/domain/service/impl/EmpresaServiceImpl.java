package br.livetouch.livecom.domain.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.internal.util.SerializationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Parametro;
import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.TemplateEmail;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.enums.TipoGrupo;
import br.livetouch.livecom.domain.enums.TipoParametro;
import br.livetouch.livecom.domain.enums.TipoTemplate;
import br.livetouch.livecom.domain.repository.EmpresaRepository;
import br.livetouch.livecom.domain.service.ArquivoService;
import br.livetouch.livecom.domain.service.CategoriaPostService;
import br.livetouch.livecom.domain.service.EmpresaService;
import br.livetouch.livecom.domain.service.FuncaoService;
import br.livetouch.livecom.domain.service.GrupoService;
import br.livetouch.livecom.domain.service.MenuService;
import br.livetouch.livecom.domain.service.ParametroService;
import br.livetouch.livecom.domain.service.PerfilService;
import br.livetouch.livecom.domain.service.PostService;
import br.livetouch.livecom.domain.service.TemplateEmailService;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.livecom.domain.vo.Filtro;
import br.livetouch.livecom.utils.HostUtil;
import br.livetouch.livecom.utils.S3Helper;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class EmpresaServiceImpl extends LivecomService<Empresa> implements EmpresaService {
	@Autowired
	private EmpresaRepository rep;

	@Autowired
	private GrupoService grupoService;

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ArquivoService arquivoService;
	
	@Autowired
	private FuncaoService funcaoService;
	
	@Autowired
	private PerfilService perfilService;
	
	@Autowired
	private ParametroService parametroService;

	@Autowired
	private CategoriaPostService categoriaService;
	
	@Autowired
	private TemplateEmailService templateEmailService;

	@Autowired
	MenuService menuService;
	
	@Autowired
	private PostService postService;

	@Override
	public Empresa get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(Empresa c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<Empresa> findAll() {
		return rep.findAll(true);
	}

	@Override
	public void saveOrUpdate(Empresa empresa, Usuario usuario, Usuario userInfo, List<Parametro> parametros) throws DomainException {
		boolean insert = true;
		if(empresa.getId() != null) {
			insert = false;
		}
		
		this.saveOrUpdate(empresa);
		usuario.setEmpresa(empresa);
		
		String perfilAdmin = ParametrosMap.getInstance(1L).get(Params.PERFIL_ADMIN_PADRAO, "Admin");
		
		if(insert) {
			menuService.generateMenus(empresa);
//			menuService.cloneDefaultMenu(empresa);
			perfilService.generatePerfis(empresa);
		}
		
		Perfil admin = perfilService.findByNome(perfilAdmin, empresa);
		
		Funcao funcao = usuario.getFuncao();
		if(funcao == null) {
			String funcaoPadrao = ParametrosMap.getInstance(1L).get(Params.FUNCAO_PADRAO,"Administrador");
			funcao = funcaoService.findByNome(funcaoPadrao, empresa.getId());
			if(funcao == null) {
				funcao = new Funcao();
				funcao.setEmpresa(empresa);
				funcao.setNome(funcaoPadrao);
				funcaoService.saveOrUpdate(funcao, userInfo);
			}
		}
		
		if (usuario.getPermissao() == null) {
			usuario.setPermissao(admin);
		}

		usuario.setFuncao(funcao);
		usuario.setEmpresa(empresa);

		Usuario db = usuarioService.findByLogin(usuario.getLogin());
		if (db != null && !db.getId().equals(usuario.getId())) {
			throw new DomainException("Este login já está em uso.");
		}

		db = usuarioService.findByEmail(usuario.getEmail());
		if (db != null && !db.getId().equals(usuario.getId())) {
			throw new DomainException("Este e-mail já está em uso.");
		}
		
		usuarioService.saveOrUpdate(userInfo,usuario);
		empresa.setAdmin(usuario);
		
		CategoriaPost categoria = categoriaService.findByCodigo(empresa, "comunicados");
		if (categoria == null) {
			categoria = new CategoriaPost();
			categoria.setEmpresa(empresa);
			categoria.setCodigo("comunicados");
			categoria.setNome("Comunicados");
			categoria.setPadrao(true);
		}
		categoriaService.saveOrUpdate(usuario, categoria);
		
		if(parametros != null && parametros.size() > 0) {
			for (Parametro parametro : parametros) {
				Parametro p = parametroService.findByNome(parametro.getNome(), empresa);
				if(p == null) {
					p = parametro;
				}
				p.setValor(parametro.getValor());
				p.setTipoParametro(TipoParametro.SERVER);
				p.setEmpresa(empresa);
				parametroService.saveOrUpdate(p);
			}
		}
		
		if(insert) {

			menuService.cloneDefaultMenu(empresa);

			String grupo = ParametrosMap.getInstance(1L).get(Params.GRUPO_PUBLICO,"Público");
			// GRUPO PUBLICO
			Grupo g = grupoService.findByNomeAndUser(grupo, usuario);
			if (g == null) {
				g = new Grupo();
				g.setNome(grupo);
				g.setEmpresa(empresa);
				g.setPadrao(true);
				g.setTipoGrupo(TipoGrupo.ABERTO);
				grupoService.saveOrUpdate(usuario, g);
				usuarioService.addGrupoToUser(usuario, g, true);
			}
			
			String bemVindoTitulo = ParametrosMap.getInstance(1L).get("mural.bemvindo.titulo", "Bem vindo ao Livecom");
			String bemVindoMensagem = ParametrosMap.getInstance(1L).get("mural.bemvindo.mensagem", "Olá, aqui você pode gerenciar e postar conteúdos para sua equipe.");
			
			Post post = new Post();
			post.setTitulo(bemVindoTitulo);
			post.setMensagem(bemVindoMensagem);
			post.setData(new Date());
			post.setDataPublicacao(new Date());
			post.setUsuario(usuario);
			Set<Grupo> grupos = new HashSet<>();
			grupos.add(g);
			post.setGrupos(grupos);
			post.setCategoria(categoria);
			postService.saveOrUpdate(usuario, post);
			
			saveTemplateEmailByEmpresa(empresa);
			
		}
		
		this.saveOrUpdate(empresa);
	}

	private void saveTemplateEmailByEmpresa(Empresa empresa) throws DomainException {
		try {
			ParametrosMap param = ParametrosMap.getInstance(1L);
			S3Helper s3Helper = new S3Helper(param);
			String url = null;
			TemplateEmail t = new TemplateEmail();
			if(StringUtils.isNotEmpty(empresa.getTrialCode())) {
				url = s3Helper.getFileURL("templates", "email_convidar_trial.htm");
				t.setTipoTemplate(TipoTemplate.conviteTrial);
			} else {
				url = s3Helper.getFileURL("templates", "email_convidar_externo.htm");
				t.setTipoTemplate(TipoTemplate.convite);
			}
			
			if(StringUtils.isNotEmpty(url)) {
				InputStream in = new URL( url ).openStream();
				String string = IOUtils.toString(new InputStreamReader(in, "utf-8"));
				t.setDateCreated(new Date());
				t.setEmpresa(empresa);
				t.setHtml(string);
				t.setNome("Convite");
				t.setNomeHtml("Convite");
				templateEmailService.saveOrUpdate(t, empresa);
			}
		} catch (IOException e) {
			log.debug("Erro ao cadastrar o template de e-mail para a empresa  " + empresa.getNome() + " erro " + e.getMessage(), e);
		}
	}

	@Override
	public List<Empresa> filterEmpresa(Filtro filtroEmpresa) {
		return rep.filterEmpresa(filtroEmpresa);
	}

	@Override
	public Empresa findDominio(String dominio) {
		return rep.findDominio(dominio);
	}
	
	@Override
	public Empresa findTrial(String codigo) {
		return rep.findTrial(codigo);
	}

	@Override
	public Empresa findByName(Empresa empresa) {
		return rep.findByName(empresa);
	}

	@Override
	public List<Parametro> parametrosTrial(String nome, Empresa empresa) {
		List<Parametro> parametros  = new ArrayList<Parametro>();
		
		Parametro p = new Parametro();
		p.setNome(Params.BUILD_APP_ANDROID_URL);
		p.setValor("https://play.google.com/apps/testing/br.livetouch.livecom.beta.trial");
		parametros.add(p);
		
		p = new Parametro();
		p.setNome(Params.BUILD_APP_IOS_URL);
		p.setValor("itms-services://?action=download-manifest&url=https://s3-sa-east-1.amazonaws.com/livetouch-download/trial.plist");
		parametros.add(p);
		
		p = new Parametro();
		p.setNome(Params.WEB_TEMPLATE_TITLE);
		p.setValor(nome);
		parametros.add(p);
		
		p = new Parametro();
		p.setNome(Params.BORDER_COPYRIGHT_EMPRESA);
		p.setValor(nome);
		parametros.add(p);
		
		p = new Parametro();
		p.setNome(Params.MSG_PUSH_NEWPOST);
		p.setValor("$userPost postou um novo comunicado $titulo");
		parametros.add(p);
		
		p = new Parametro();
		p.setNome(Params.PUSH_SERVER_PROJECT);
		p.setValor("trial");
		parametros.add(p);

		p = new Parametro();
		p.setNome(Params.PUSH_SERVER_HOST);
		p.setValor("push.livetouchdev.com.br");
		parametros.add(p);

		p = new Parametro();
		p.setNome(Params.MURAL_SIDEBOX_ON);
		p.setValor("1");
		parametros.add(p);

		p = new Parametro();
		p.setNome(Params.MURAL_TAG_ON);
		p.setValor("1");
		parametros.add(p);

		p = new Parametro();
		p.setNome(Params.PUSH_ON);
		p.setValor("1");
		parametros.add(p);

		p = new Parametro();
		p.setNome(Params.PUSH_CHECKBOXON);
		p.setValor("1");
		parametros.add(p);
		
		p = new Parametro();
		p.setNome(Params.PUSH_CHECKBOX_CHECKED);
		p.setValor("1");
		parametros.add(p);
		
		p = new Parametro();
		p.setNome(Params.USER_SENHA_PADRAO_ON);
		p.setValor("1");
		parametros.add(p);
		
		p = new Parametro();
		p.setNome(Params.USER_SENHA_PADRAO);
		p.setValor("abc123");
		parametros.add(p);

		p = new Parametro();
		p.setNome(Params.SECURITY_ALLOWED_ADMIN_HOST);
		p.setValor(HostUtil.getHostFromUrl(empresa.getDominio()));
		parametros.add(p);
		
		return parametros;
	}

	@Override
	public Empresa findByAdmin(Usuario u) {
		return rep.findByAdmin(u);
	}

    @Override
    public void setParametrosDefaultEmpresa(Empresa empresa) {
        List<Parametro> mobileDefault = parametroService.findMobileDefault();
        List<Parametro> parametrosEmpresa = new ArrayList<>();
        if(mobileDefault != null && mobileDefault.size() > 0) {
            for (Parametro p : mobileDefault) {
                Parametro clone = (Parametro) SerializationHelper.clone(p);
                clone.setId(null);
                parametrosEmpresa.add(clone);
            }
            parametroService.saveCloneDefaults(parametrosEmpresa, empresa);
        }
    }

    @Override
	public void delete(Usuario userInfo,Empresa e) throws DomainException {
		if (e != null) {
			long count = rep.getCount();
			
			Long id = e.getId();
			if (id.equals(1L) && "livecom".equalsIgnoreCase(e.getNome())) {
				throw new DomainException("Não é possível excluir a empresa Livecom");
			}
			
			// zera o admin
			execute("update Empresa set admin=null where id=?", e.getId());
			
			// deleta usuarios
			List<Usuario> usuarios = usuarioService.findAll(e);
			for (Usuario u : usuarios) {
				System.out.println(u);
				usuarioService.delete(null, u, true);
			}

//			List<CategoriaPost> categs = categoriaService.findAll(e);
//			
//			for (CategoriaPost c : categs) {
//				categoriaService.delete(null, c, true);
//			}
			
			execute("update CategoriaPost set categoriaParent = null where empresa.id=?", e.getId());
			execute("update Grupo set categoria = null where categoria.id in (select id from CategoriaPost where empresa.id=?)", e.getId());
			execute("update Chapeu set categoria = null where categoria.id in (select id from CategoriaPost where empresa.id=?)", e.getId());
			execute("update Post set categoria = null where categoria.id in (select id from CategoriaPost where empresa.id=?)", e.getId());
			execute("delete from CategoriaPost where empresa.id=?", e.getId());
			
			List<Grupo> grupos = grupoService.findAll(e);
			for (Grupo g : grupos) {
				grupoService.delete(null, g, true);
			}

			// arquivos
			List<Arquivo> arquivos = arquivoService.findAll(e);
			for (Arquivo a : arquivos) {
				arquivoService.delete(userInfo,a,true);
			}

			execute("delete from Area where empresa.id=?", e.getId());
			execute("delete from Campo where empresa.id=?", e.getId());

			boolean isUltimaEmpresa = count == 1;
			if(isUltimaEmpresa) {
				// Ultima
				executeNative("delete from cidade where empresa_id=?", e.getId());
				
				// Limpa residuos se for ultima empresa (id=1)
				execute("delete from PostDestaque");
				execute("delete from Post");
				execute("delete from LogSistema");
				execute("delete from LogTransacao");
				execute("delete from EmailReport");
				execute("delete from LoginReport");
				execute("delete from Canal");
				execute("delete from Notification");
//				execute("delete from StatusChat");
			}
			
			// categ cascade
			execute("delete from CategoriaPostAuditoria where empresa.id=?", e.getId());
			
			execute("delete from Complemento where empresa.id=?", e.getId());
			
			// Diretoria cascade
			executeCascadeUpdate(e.getId(),"Diretoria","diretoriaExecutiva");
			
			execute("delete from Feriado where empresa.id=?", e.getId());
			execute("delete from Formacao where empresa.id=?", e.getId());
			execute("update Usuario set funcao = null where empresa.id=?", e.getId());
			execute("update Usuario set funcao = null where funcao.id in (select id from Funcao where empresa.id=?)", e.getId());
			execute("delete from Funcao where empresa.id=?", e.getId());
			
//			execute("delete from Grupo where empresa.id=?", e.getId());
			execute("delete from GrupoAuditoria where empresa.id=?", e.getId());
			
			execute("delete from LogAuditoria where empresa.id=?", e.getId());
			execute("delete from LogSistema where empresa.id=?", e.getId());
			execute("delete from LogTransacao where empresa.id=?", e.getId());
			execute("delete from LoginReport where empresa.id=?", e.getId());
			
			// cascade LogImportacao
			executeCascadeDelete(e.getId(), "LogImportacao", "LinhaImportacao");
			
			executeCascadeUpdate(e.getId(),"LogImportacao","diretoriaExecutiva");
			execute("delete from LogImportacao where empresa.id=?", e.getId());

			execute("delete from OperacoesCielo where empresa.id=?", e.getId());
			
			execute("delete from PerfilPermissao pp where pp.perfil.id in (select id from Perfil where empresa.id=?)", e.getId());
			
			execute("delete from Permissao where empresa.id=?", e.getId());
			execute("delete from Parametro where empresa.id=?", e.getId());
			execute("delete from PostAuditoria where empresa.id=?", e.getId());
			execute("delete from PushReport where empresa.id=?", e.getId());
			
			execute("delete from Tag where empresa.id=?", e.getId());
			execute("delete from TagAuditoria where empresa.id=?", e.getId());
			execute("delete from TemplateEmail where empresa.id=?", e.getId());
			
			execute("delete from Usuario where empresa.id=?", e.getId());
			execute("delete from UsuarioAuditoria where empresa.id=?", e.getId());
			
			//cascade perfil
			execute("delete from MenuPerfil mp where mp.perfil.id in (select id from Perfil where empresa.id=?)", e.getId());
			execute("delete from Perfil where empresa.id=?", e.getId());
			
			//menu
			execute("delete from Menu where parent is not null and empresa.id=?", e.getId());
			execute("delete from Menu where empresa.id=?", e.getId());
			
			// empresa
			rep.deleteById(e.getId());
		}
	}
}
