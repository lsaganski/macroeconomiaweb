package br.livetouch.livecom.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

/**
 * @author Ricardo Lecheta
 * @created 13/03/2013
 */
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.livetouch.livecom.utils.DummyUtils;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Faq extends net.livetouch.tiger.ddd.Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -758348226134417865L;

	@Id
	@Column(name="id", unique=true, nullable=false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "FAQ_SEQ")
	private Long id;
	
	@Column(name="ENUNCIADO", nullable=false, length=500)
	private String enunciado;
	
	@Column(name="RESPOSTA", nullable=false, length=500)
	private String resposta;
	
	@Column(name="ordem", nullable=false)
	private Integer ordem;

	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public String getEnunciado() {
		return this.enunciado;
	}

	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}

	
	public String getResposta() {
		return this.resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	
	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	@Transient
	public String getRespostaHtml() {
		return DummyUtils.getHtmlFromString(getResposta());
	}
}
