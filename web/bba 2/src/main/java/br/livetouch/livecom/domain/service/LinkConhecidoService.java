package br.livetouch.livecom.domain.service;

import java.util.List;

import javax.ws.rs.core.Response;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.LinkConhecido;
import net.livetouch.tiger.ddd.DomainException;

public interface LinkConhecidoService extends Service<LinkConhecido>{

	@Override
	LinkConhecido get(Long id); 
	
	List<LinkConhecido> findAll();
	
	void saveOrUpdate( LinkConhecido link, Empresa empresa);
	
	void delete(LinkConhecido link);
	
	Response valid(LinkConhecido link) throws DomainException;

	LinkConhecido findByName(LinkConhecido link);
	
	LinkConhecido findByDominio(LinkConhecido link);
	
}
