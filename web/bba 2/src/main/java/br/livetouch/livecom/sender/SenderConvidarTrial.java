package br.livetouch.livecom.sender;

import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.enums.TipoTemplate;

@Service
public class SenderConvidarTrial extends Sender {

	@Override
	public String getSubject() {
		String subject = "Versão Trial Livecom";
		return subject;
	}

	@Override
	protected TipoTemplate getTipoTemplate() {
		TipoTemplate tipo = TipoTemplate.conviteTrial;
		return tipo;
	}

	@Override
	protected String getNameFileTemplate() {
		String file = "email_convidar_trial.htm";
		return file;
	}
}
