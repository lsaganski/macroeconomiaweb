package br.livetouch.livecom.sender;

import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.enums.TipoTemplate;

@Service
public class SenderConvidar extends Sender {

	@Override
	protected TipoTemplate getTipoTemplate() {
		return null;
	}

	@Override
	protected String getDefaultTemplate() {
		return null;
	}

	@Override
	protected String getNameFileTemplate() {
		return null;
	}

}
