package br.livetouch.livecom.rest.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.ParamsPermissao;
import br.livetouch.livecom.domain.StatusPost;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.vo.StatusPostVO;
import br.livetouch.livecom.rest.domain.MessageResult;
import net.livetouch.tiger.ddd.DomainException;

@Path("/status")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class StatusPostResource extends MainResource {
	protected static final Logger log = Log.getLogger(StatusPostResource.class);
	
	@POST
	public Response create(StatusPost status) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}

		StatusPost.isFormValid(status, false);
		
		//Validar codigo
		Response messageResult = existe(status);
		if(messageResult != null){
			return messageResult;
		}
		
		Usuario userInfo = getUserInfo();
		statusPostService.saveOrUpdate(status, userInfo.getEmpresa());
		return Response.ok(MessageResult.ok("Cadastro realizado com sucesso", new StatusPostVO(status))).build();
	}
	
	@PUT
	public Response update(StatusPost status) throws DomainException {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		StatusPost.isFormValid(status, true);
		
		//Validar codigo
		Response messageResult = existe(status);
		if(messageResult != null){
			return messageResult;
		}
		
		Usuario userInfo = getUserInfo();
		statusPostService.saveOrUpdate(status, userInfo.getEmpresa());
		
		return Response.ok(MessageResult.ok("StatusPost atualizado com sucesso", new StatusPostVO(status))).build();
	}
	
	@GET
	public Response findAll() {
		
		List<StatusPost> status = statusPostService.findAll(getEmpresa());
		List<StatusPostVO> vos = StatusPostVO.fromList(status);
		
		br.livetouch.livecom.web.pages.ws.Response r = br.livetouch.livecom.web.pages.ws.Response.ok("OK");
		r.statusPost = vos;
		
		return Response.ok().entity(r).build();
	}

	@GET
	@Path("/user/{id}")
	public Response findByUser(@PathParam("id") Long userId) {
		
		if(userId == null) {
			return Response.ok(MessageResult.error("Status não encontrado")).build();
		}
		
		Usuario usuario = usuarioService.get(userId);
		
		if(usuario == null) {
			return Response.ok(MessageResult.error("Status não encontrado")).build();
		}
		
		List<StatusPost> status = statusPostService.findAll(usuario.getEmpresa());
		List<StatusPostVO> vos = StatusPostVO.fromList(status);
		return Response.ok().entity(vos).build();
	}
	
	@GET
	@Path("/{id}")
	public Response find(@PathParam("id") Long id) {
		
		if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
			return Response.ok(MessageResult.error("Ação não autorizada")).build();
		}
		
		StatusPost status = statusPostService.get(id);
		if(status == null) {
			return Response.ok(MessageResult.error("Status não encontrado")).build();
		}
		StatusPostVO vo = new StatusPostVO(status);
		return Response.ok().entity(vo).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		try {
			
			if(!hasPermissao(ParamsPermissao.ACESSO_CADASTROS)) {
				return Response.ok(MessageResult.error("Ação não autorizada")).build();
			}
			
			StatusPost status = statusPostService.get(id);
			if(status == null) {
				return Response.ok(MessageResult.error("Status não encontrado")).build();
			}
			statusPostService.delete(status);
			return Response.ok().entity(MessageResult.ok("Status deletado com sucesso")).build();
		} catch (DomainException e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error(e.getMessage())).build();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Response.ok(MessageResult.error("Não foi possível excluir o Status  " + id)).build();
		}
	}
	
	public Response existe(StatusPost status) {
		
		if(statusPostService.findByCodigo(status, getEmpresa()) != null) {
			log.debug("Ja existe um status com este codigo");
			return Response.ok(MessageResult.error("Ja existe um status com este codigo")).build();
		}

		return null;
	}
	
}
