package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.CategoriaIdioma;
import br.livetouch.livecom.domain.CategoriaPost;
import br.livetouch.livecom.domain.Idioma;
import br.livetouch.livecom.domain.Usuario;

public class CategoriaVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public String nome;
	private String tree;
	private String cor;
	private Boolean padrao;
	private Boolean menu;
	private CategoriaVO categoriaPai;
	private List<CategoriaVO> subcategorias;
	private List<CategoriaIdiomaVO> idiomas;
	private List<FileVO> arquivos;
	private String codigo;
	private String icone;
	private String userCreate;
	private Long userCreateId;
	private Boolean likeable;
	private Boolean hideData;
	private int ordem;
	
	public CategoriaVO() {
	}
	
	
	public CategoriaVO(CategoriaPost categ) {
		if(categ != null){
			this.id = categ.getId();
			this.nome = categ.getNome();
			this.cor = categ.getCor();
			this.padrao = categ.isPadrao();
			this.menu = categ.isMenu();
			this.likeable = categ.isLikeable();
			this.hideData = categ.isHideData();
			this.icone = StringUtils.isNotEmpty(categ.getUrlIcone()) ? categ.getUrlIcone() : "";
			this.setCodigo(categ.getCodigo());
			
			setArquivosVO(categ);
		}
	}

	public CategoriaVO(CategoriaPost categ, Idioma idioma) {
		if(categ != null){
			this.id = categ.getId();
			this.nome = getNomeByIdioma(categ, idioma);
			this.cor = categ.getCor();
			this.padrao = categ.isPadrao();
			this.menu = categ.isMenu();
			this.likeable = categ.isLikeable();
			this.hideData = categ.isHideData();
			this.icone = StringUtils.isNotEmpty(categ.getUrlIcone()) ? categ.getUrlIcone() : "";
			this.codigo = categ.getCodigo();
			
			Set<CategoriaIdioma> idiomas = categ.getIdiomas();
			if(idiomas != null) {
				this.idiomas = CategoriaIdiomaVO.fromList(idiomas);
			}
			
			setArquivosVO(categ);
		}
	}
	
	public CategoriaVO getCategoriaVO(CategoriaPost c) {
		return new CategoriaVO(c);
	}

	public void setCategoria(CategoriaPost c) {
		setId(c.getId());
		setNome(c.getNome());
		setCor(c.getCor());
		setMenu(c.isMenu());
		setLikeable(c.isLikeable());
		setHideData(c.isHideData());
		Usuario user = c.getUserCreate();
		if(user != null) {
			setUserCreate(user.getNome());
			setUserCreateId(user.getId());
		}
		
		setArquivosVO(c);
	}


	private void setArquivosVO(CategoriaPost c) {
		Set<Arquivo> arquivos = c.getArquivos();
		if(arquivos != null) {
			setArquivos(new ArrayList<FileVO>());
			for (Arquivo arquivo : arquivos) {
				FileVO fileVO = new FileVO();
				fileVO.setArquivo(arquivo, false);
				getArquivos().add(fileVO);
			}
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}
	
	public Boolean getPadrao() {
		return padrao;
	}

	public void setPadrao(Boolean padrao) {
		this.padrao = padrao;
	}

	public static List<CategoriaVO> create(List<CategoriaPost> categorias) {
		return create(categorias, null);
	}

	public static List<CategoriaVO> create(List<CategoriaPost> categorias, Idioma idioma) {
		List<CategoriaVO> retorno = new ArrayList<CategoriaVO>();
		for (CategoriaPost c : categorias) {
			CategoriaVO categoriaVO = new CategoriaVO(c, idioma);
			CategoriaPost categoriaParent = c.getCategoriaParent();
			
			if(categoriaParent != null) {
				categoriaVO.setCategoriaPai(new CategoriaVO(categoriaParent, idioma));
				if(categoriaParent.getCategoriaParent() != null) {
					categoriaVO.getCategoriaPai().setCategoriaPai(new CategoriaVO(categoriaParent.getCategoriaParent(), idioma));
				}
			}


			if(categoriaVO.nome != null) {
				retorno.add(categoriaVO);
			}
			
		}
		return retorno;
	}

	public static List<CategoriaVO> createTree(List<CategoriaPost> categorias, Idioma idioma) {
		List<CategoriaVO> retorno = new ArrayList<CategoriaVO>();
		for (CategoriaPost c : categorias) {
			CategoriaVO categoriaVO = new CategoriaVO(c, idioma);
			categoriaVO.setTree(categoriaVO.nome);
			String tree = CategoriaVO.setParents(c, categoriaVO, idioma, categoriaVO.nome);
			categoriaVO.setTree(tree);
			retorno.add(categoriaVO);
		}
		return retorno;
	}
	
	public CategoriaVO getCategoriaPai() {
		return categoriaPai;
	}


	public void setCategoriaPai(CategoriaVO categoriaPai) {
		this.categoriaPai = categoriaPai;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public Boolean isMenu() {
		return menu;
	}


	public void setMenu(Boolean menu) {
		this.menu = menu;
	}


	public String getIcone() {
		return icone;
	}


	public void setIcone(String icone) {
		this.icone = icone;
	}


	public String getUserCreate() {
		return userCreate;
	}


	public void setUserCreate(String userCreate) {
		this.userCreate = userCreate;
	}


	public Long getUserCreateId() {
		return userCreateId;
	}


	public void setUserCreateId(Long userCreateId) {
		this.userCreateId = userCreateId;
	}


	public Boolean isLikeable() {
		return likeable;
	}


	public void setLikeable(Boolean likeable) {
		this.likeable = likeable;
	}


	public int getOrdem() {
		return ordem;
	}


	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}


	public Boolean getHideData() {
		return hideData;
	}


	public void setHideData(Boolean hideData) {
		this.hideData = hideData;
	}


	public List<FileVO> getArquivos() {
		return arquivos;
	}


	public void setArquivos(List<FileVO> arquivos) {
		this.arquivos = arquivos;
	}
	
	public String getNomeByIdioma(CategoriaPost categoria, Idioma idioma) {
		
		if(idioma == null || idioma.getId() == null) {
			if(StringUtils.isNotEmpty(categoria.getNome())) {
				return categoria.getNome();
			}
		}
		
		Set<CategoriaIdioma> idiomas = categoria.getIdiomas();
		
		if(idiomas == null) {
			return categoria.getNome();
		}
		
		if(idioma == null) {
			return idiomas.iterator().next().getNome();
		}
		
		for (CategoriaIdioma categoriaIdioma : idiomas) {
			
			if(categoriaIdioma.getIdioma().getId().equals(idioma.getId())) {
				return categoriaIdioma.getNome();
			}
			
		}
		
		return null;
	}


	public List<CategoriaVO> getSubcategorias() {
		return subcategorias;
	}


	public void setSubcategorias(List<CategoriaVO> subcategorias) {
		this.subcategorias = subcategorias;
	}


	public List<CategoriaIdiomaVO> getIdiomas() {
		return idiomas;
	}


	public void setIdiomas(List<CategoriaIdiomaVO> idiomas) {
		this.idiomas = idiomas;
	}

	public static String setParents(CategoriaPost c, CategoriaVO vo, Idioma i, String tree) {
		if (c != null) {
			CategoriaPost parent = c.getCategoriaParent();
			if (parent != null) {
				CategoriaVO p = new CategoriaVO(parent, i);
				vo.setCategoriaPai(p);
				tree = p.nome + " > " + tree;
				return setParents(parent, p, i, tree);
			}
		}
		
		return tree;
	}


	public String getTree() {
		return tree;
	}


	public void setTree(String tree) {
		this.tree = tree;
	}
	
}
