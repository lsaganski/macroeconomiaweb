package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.LoginReport;
import br.livetouch.livecom.domain.repository.LoginReportRepository;
import br.livetouch.livecom.domain.service.LoginReportService;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class LoginReportServiceImpl implements LoginReportService {

	@Autowired
	private LoginReportRepository rep;

	@Override
	public LoginReport get(Long id) {
		return rep.get(id);
	}

	@Override
	public List<LoginReport> findAll() {
		return rep.findAll();
	}

	@Override
	public void saveOrUpdate(LoginReport f) throws DomainException {
		rep.saveOrUpdate(f);
	}

	@Override
	public void delete(LoginReport f) throws DomainException {
		rep.delete(f);
	}

}
