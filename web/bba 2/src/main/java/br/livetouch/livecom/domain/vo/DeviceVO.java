package br.livetouch.livecom.domain.vo;


public class DeviceVO {

	private String login;
	private String so;
	private String registrationId;
	private String versao;
	
	public DeviceVO (String login, String so, String registrationId, String versao) {
		this.login = login;
		this.so = so;
		this.registrationId = registrationId;
		this.versao = versao;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getSo() {
		return so;
	}
	
	public void setSo(String so) {
		this.so = so;
	}
	
	public String getRegistrationId() {
		return registrationId;
	}
	
	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getVersao() {
		return versao;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}
	
}