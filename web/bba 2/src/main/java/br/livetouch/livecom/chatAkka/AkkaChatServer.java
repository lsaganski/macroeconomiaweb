package br.livetouch.livecom.chatAkka;

import org.springframework.context.ApplicationContext;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.DeadLetter;
import akka.actor.Props;
import br.livetouch.livecom.chatAkka.actor.AkkaFactory;
import br.livetouch.livecom.chatAkka.actor.DeadLetterActor;
import br.livetouch.livecom.chatAkka.actor.MainActor;

/**
 * @author rlecheta
 *
 */
public class AkkaChatServer {
	public static ApplicationContext appContext;
	public static ActorSystem actorSystem;

	public static void start(ApplicationContext appContext) {
		AkkaChatServer.appContext = appContext;

		int port = getLivecomInterface().chatPort();

		actorSystem = ActorSystem.create("ChatLivecom");

		// Main Actor
		ActorRef mainActor = actorSystem.actorOf(Props.create(MainActor.class), "mainActor");
		mainActor.tell(new MainActor.Start(port), ActorRef.noSender());
	
		// Dead Letter
		ActorRef deadLetterActor = actorSystem.actorOf(Props.create(DeadLetterActor.class), "DeadLetterActor");
		actorSystem.eventStream().subscribe(deadLetterActor, DeadLetter.class);
	}
	
	public static LivecomChatInterface getLivecomInterface() {
		String beanName = appContext.getBeanNamesForType(LivecomChatInterfaceImpl.class)[0];
		LivecomChatInterface bean = (LivecomChatInterface) appContext.getBean(beanName);
		return bean;
	}

	public static void stop() {
		if (actorSystem != null) {
			actorSystem.shutdown();
		}
	}

	public static void sendMsg(Long userIdTo, Object sendObj) {
		if (actorSystem != null) {
			ActorSelection userActor = AkkaFactory.getUserActor(actorSystem,userIdTo.toString());
			if (userActor != null) {
				userActor.tell(sendObj, ActorRef.noSender());
			}
		}
	}
	
	public static ActorSystem getActorSystem() {
		return actorSystem;
	}
}
