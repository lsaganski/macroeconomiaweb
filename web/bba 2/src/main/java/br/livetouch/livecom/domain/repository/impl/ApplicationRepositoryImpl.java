package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Application;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.repository.ApplicationRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class ApplicationRepositoryImpl extends
		StringHibernateRepository<Application> implements ApplicationRepository {

	public ApplicationRepositoryImpl() {
		super(Application.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Application> findByUsuario(Usuario usuario) {
		Query q = createQuery("FROM Application app WHERE app.usuario = :usuario");
		q.setParameter("usuario", usuario);
		return q.list();
	}

	@Override
	public Boolean exists(Application app) {
		StringBuffer sb = new StringBuffer("SELECT id FROM Application app WHERE app.nome = :nome");
		if(app.getId() != null) {
			sb.append(" AND app.id != :id");
		}

		Query q = createQuery(sb.toString());
		q.setParameter("nome", app.getNome());
		if(app.getId() != null) {
			q.setParameter("id", app.getId());
		}
		
		return q.list().size() > 0 ? true : false;
	}

	@Override
	public Application get(Long id, Usuario u) {
		
		StringBuffer sb = new StringBuffer("from Application app WHERE app.usuario = :usuario and app.id = :id");

		Query q = createQuery(sb.toString());
		
		if(id != null) {
			q.setParameter("id", id);
		}
		
		if(u != null) {
			q.setParameter("usuario", u);
		}
		
		return (Application) q.uniqueResult();
	}

	@Override
	public Application find(String consumerKey, String consumerSecret) {
		Query q = createQuery("FROM Application app where app.consumerKey = :consumerKey AND app.consumerSecret = :consumerSecret");
		q.setParameter("consumerKey", consumerKey);
		q.setParameter("consumerSecret", consumerSecret);
		return (Application) q.uniqueResult();
	}
	
}