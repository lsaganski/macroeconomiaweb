package br.livetouch.livecom.domain.repository.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.PermissaoCategoria;
import br.livetouch.livecom.domain.repository.PermissaoCategoriaRepository;
import br.livetouch.spring.StringHibernateRepository;

@Repository
public class PermissaoCategoriaRepositoryImpl extends StringHibernateRepository<PermissaoCategoria> implements PermissaoCategoriaRepository {

	public PermissaoCategoriaRepositoryImpl() {
		super(PermissaoCategoria.class);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PermissaoCategoria> findAll(Empresa e) {
		StringBuffer sb = new StringBuffer("from PermissaoCategoria p where 1=1 ");
		sb.append(" and (p.empresa = :empresa or p.empresa is null)");
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("empresa", e);
		
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public PermissaoCategoria findByNomeValid(PermissaoCategoria p, Empresa empresa) {
		
		StringBuffer sb = new StringBuffer("from PermissaoCategoria p where p.nome = :nome");
		sb.append(" and (p.empresa = :empresa or p.empresa is null)");
		
		if(p.getId() != null) {
			sb.append(" and p.id != :id");
		}

		Query q = createQuery(sb.toString());
		
		q.setParameter("nome", p.getNome());
		q.setParameter("empresa", empresa);
		
		if(p.getId() != null) {
			q.setParameter("id", p.getId());
		}
			
		List<PermissaoCategoria> list = q.list();
		PermissaoCategoria d = (PermissaoCategoria) (list.size() > 0 ? q.list().get(0) : null);
		return d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PermissaoCategoria findByCodigoValid(PermissaoCategoria p, Empresa empresa) {
		
		StringBuffer sb = new StringBuffer("from PermissaoCategoria p where p.codigo = :codigo");
		sb.append(" and (p.empresa = :empresa or p.empresa is null)");
		
		if(p.getId() != null) {
			sb.append(" and p.id != :id");
		}
		
		Query q = createQuery(sb.toString());
		
		q.setParameter("codigo", p.getCodigo());
		q.setParameter("empresa", empresa);
		
		if(p.getId() != null) {
			q.setParameter("id", p.getId());
		}
		
		List<PermissaoCategoria> list = q.list();
		PermissaoCategoria d = (PermissaoCategoria) (list.size() > 0 ? q.list().get(0) : null);
		return d;
	}

}
