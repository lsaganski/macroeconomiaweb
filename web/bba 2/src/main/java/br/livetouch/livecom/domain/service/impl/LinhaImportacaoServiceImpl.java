package br.livetouch.livecom.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.livetouch.livecom.domain.LinhaImportacao;
import br.livetouch.livecom.domain.LogImportacao;
import br.livetouch.livecom.domain.repository.LinhaImportacaoRepository;
import br.livetouch.livecom.domain.repository.LogImportacaoRepository;
import br.livetouch.livecom.domain.service.LinhaImportacaoService;
import br.livetouch.livecom.domain.vo.LinhaImportacaoVO;
import net.livetouch.tiger.ddd.DomainException;

@Service
public class LinhaImportacaoServiceImpl implements LinhaImportacaoService {
	@Autowired
	private LinhaImportacaoRepository rep;

	@Autowired
	private LogImportacaoRepository repImportacao;

	@Override
	public LinhaImportacao get(Long id) {
		return rep.get(id);
	}

	@Override
	public void saveOrUpdate(LinhaImportacao c) throws DomainException {
		rep.saveOrUpdate(c);
	}

	@Override
	public List<LinhaImportacao> findAll() {
		return rep.findAll();
	}

	@Override
	public void delete(LinhaImportacao c) throws DomainException {
		rep.delete(c);
	}

	@Override
	public LinhaImportacaoVO findAllVOByLogId(Long id) {
		List<LinhaImportacao> linhas = findAllByLogId(id);
		LogImportacao log = repImportacao.get(id);
		String header = log.getHeader();
		LinhaImportacaoVO vo = new LinhaImportacaoVO(header, linhas);
		return vo;
	}

	@Override
	public List<LinhaImportacao> findAllByLogId(Long id) {
		return rep.findAllByLogId(id);
	}

	@Override
	public List<LinhaImportacao> findAllByIdLog(Long id) {
		return rep.findAllByIdLog(id);
	}

}
