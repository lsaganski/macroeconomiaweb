package br.livetouch.livecom.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Campo extends net.livetouch.tiger.ddd.Entity {
	private static final long serialVersionUID = 8771734275567884350L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "generator")
	@SequenceGenerator(name = "generator", sequenceName = "CAMPO_SEQ")
	private Long id;

	private String nome;
	
	private int visivel;
	
	/**
	 * Público ou privado (somente o admin e o próprio usuário enxergam campos privados)
	 */
	private int publico;
	
	/**
	 * - Comum ou específico (específico significa que as opções que este vampo vai exibir dependem de algum outro campo selecionado pelo usuário, 
	 * como a diferença entre funcionário interno e externo, por exemplo)
	 */
	private String dependeCampo;
	private String dependeCampoValor;
	
	/**
	 * - Editável pelo próprio usuário ou não
	 */
	private int editavel;
	
	/**
	 * - Obrigatório ou não de ser preenchido pelo Admin no momento do cadastro 
	 */
	private int obrigatorioCadastroAdmin;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id", nullable = true)
	private Empresa empresa;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getPublico() {
		return publico;
	}

	public void setPublico(int publico) {
		this.publico = publico;
	}

	public String getDependeCampo() {
		return dependeCampo;
	}

	public void setDependeCampo(String dependeCampo) {
		this.dependeCampo = dependeCampo;
	}

	public String getDependeCampoValor() {
		return dependeCampoValor;
	}

	public void setDependeCampoValor(String dependeCampoValor) {
		this.dependeCampoValor = dependeCampoValor;
	}

	public int getEditavel() {
		return editavel;
	}

	public void setEditavel(int editavel) {
		this.editavel = editavel;
	}

	public int getObrigatorioCadastroAdmin() {
		return obrigatorioCadastroAdmin;
	}

	public void setObrigatorioCadastroAdmin(int obrigatorioCadastroAdmin) {
		this.obrigatorioCadastroAdmin = obrigatorioCadastroAdmin;
	}
	
	public int getVisivel() {
		return visivel;
	}
	
	public void setVisivel(int visivel) {
		this.visivel = visivel;
	}

	@Override
	public String toString() {
		return "Campo [nome=" + nome + ", visivel=" + visivel + ", publico=" + publico + ", dependeCampo=" + dependeCampo + ", dependeCampoValor=" + dependeCampoValor + ", editavel=" + editavel + ", obrigatorioCadastroAdmin=" + obrigatorioCadastroAdmin + "]";
	}

	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
}
