package br.livetouch.livecom.domain.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.livetouch.livecom.domain.Tag;
import br.livetouch.livecom.domain.Usuario;

public class TagVO implements Serializable {
	private static final long serialVersionUID = 1L;
	public Long id;
	public String nome;
	private String tagParent;
	private String userCreate;
	private Long userCreateId;

	public void setTag(Tag c) {
		setId(c.getId());
		setNome(c.getNome());
		if(c.getTagParent() != null) {
			setTagParent(c.getTagParent().getNome());
		} else {
			setTagParent("--");
		}
		Usuario user = c.getUserCreate();
		if(user != null) {
			setUserCreate(user.getNome());
			setUserCreateId(user.getId());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public static List<TagVO> createByList(List<Tag> tags) {
		ArrayList<TagVO> list = new ArrayList<>();
		if(tags == null) {
			return list;
		}
		for (Tag tag : tags) {
			TagVO tagVO = new TagVO();
			tagVO.setTag(tag);
			list.add(tagVO);
		}
		return list;
	}

	public String getTagParent() {
		return tagParent;
	}

	public void setTagParent(String tagParent) {
		this.tagParent = tagParent;
	}

	public String getUserCreate() {
		return userCreate;
	}

	public void setUserCreate(String userCreate) {
		this.userCreate = userCreate;
	}

	public Long getUserCreateId() {
		return userCreateId;
	}

	public void setUserCreateId(Long userCreateId) {
		this.userCreateId = userCreateId;
	}

}
