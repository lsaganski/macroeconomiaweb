package br.livetouch.token;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

/**
 * Classe que gera a semente utilizando um "HASH SHA1" e retorna um Hexadecimal
 * 
 * @author ricardo
 * 
 */
public class TokenSecret {

	public static String create(String s) {
		try {

			byte[] input = s.getBytes();
			MessageDigest md = MessageDigest.getInstance("SHA1");

			md.reset();
			byte[] h1 = md.digest(input);
			md.reset();
			byte[] h2 = md.digest(mergeByteArray(input, h1));

			String seed = toHexa(h2);
			return seed;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("erro: " + e.getMessage());
		}
	}

	protected static String toHexa(byte[] bytes) {

		StringBuffer buffer = new StringBuffer();

		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(0xff & bytes[i]);

			if (hex.length() == 1)
				buffer.append("0");

			buffer.append(hex);

		}

		return buffer.toString();
	}

	protected static String toBase64(byte[] bytes) {
		return DatatypeConverter.printBase64Binary(bytes);
	}

	private static byte[] mergeByteArray(byte[] b1, byte[] b2) {

		byte[] result = new byte[b1.length + b2.length];

		int i = 0;

		for (byte b : b1) {
			result[i] = b;
			i++;
		}

		for (byte b : b2) {
			result[i] = b;
			i++;
		}

		return result;
	}
}
