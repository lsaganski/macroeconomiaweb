package br.livetouch.token;

import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * TOTP Token (rfc 6238)
 * 
 * Generates an OTP based on the time
 * 
 * https://tools.ietf.org/html/rfc6238
 * http://tools.ietf.org/html/draft-mraihi-totp-timebased-00
 * 
 * This an example implementation of the OATH TOTP algorithm. Visit
 * www.openauthentication.org for more information.
 *
 * @author Johan Rydell, PortWise
 */
public class TotpToken {

	/**
	 * This method uses the JCE to provide the crypto algorithm. HMAC computes a
	 * Hashed Message Authentication Code with the crypto hash algorithm as a
	 * parameter.
	 *
	 * @param crypto
	 *            the crypto algorithm (HmacSHA1, HmacSHA256, HmacSHA512)
	 * @param keyBytes
	 *            the bytes to use for the HMAC key
	 * @param text
	 *            the message or text to be authenticated.
	 */
	public static byte[] hmac_sha1(String crypto, byte[] keyBytes, byte[] text) {
		try {
			Mac hmac;

			hmac = Mac.getInstance(crypto);
			SecretKeySpec macKey = new SecretKeySpec(keyBytes, crypto);
			hmac.init(macKey);
			return hmac.doFinal(text);
		} catch (GeneralSecurityException gse) {
			throw new UndeclaredThrowableException(gse);
		}
	}

	private static final int[] DIGITS_POWER
	// 0 1 2 3 4 5 6 7 8
			= { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000 };

	/**
	 * This method generates an TOTP value for the given set of parameters.
	 *
	 * @param key
	 *            the shared secret, HEX encoded
	 * @param time
	 *            a value that reflects a time
	 * @param returnDigits
	 *            number of return digits
	 *
	 * @return A numeric String in base 10 that includes
	 *         {@link truncationDigits} digits
	 */
	static public String generateOtp(String key, String time, int returnDigits) {
		String crypto = "HmacSHA1";
		String result = null;
		byte[] bArray;

		byte[] msg = new byte[8];
		// Put the bytes of "time" to the message
		// Input is the HEX value of "time"
		if (time.length() > 0) {
			bArray = new BigInteger(time, 16).toByteArray();
			if (bArray.length == 9) {
				// First byte is the "sign" byte
				for (int i = 0; i < 8 && i < bArray.length; i++) {
					msg[i + 8 - bArray.length] = bArray[i + 1];
				}
			} else {
				for (int i = 0; i < 8 && i < bArray.length; i++) {

					msg[i + 8 - bArray.length] = bArray[i];
				}
			}
		}

		byte[] hash;
		bArray = new BigInteger(key, 16).toByteArray();
		if (bArray[0] == 0) {
			byte[] b = new byte[bArray.length - 1];
			for (int i = 0; i < b.length; i++)
				b[i] = bArray[i + 1];
			hash = hmac_sha1(crypto, b, msg);
		} else {
			// compute hmac hash
			hash = hmac_sha1(crypto, bArray, msg);
		}

		// put selected bytes into result int
		int offset = hash[hash.length - 1] & 0xf;

		int binary = ((hash[offset] & 0x7f) << 24) | ((hash[offset + 1] & 0xff) << 16)
				| ((hash[offset + 2] & 0xff) << 8) | (hash[offset + 3] & 0xff);

		int otp = binary % DIGITS_POWER[returnDigits];

		result = Integer.toString(otp);
		while (result.length() < returnDigits) {
			result = "0" + result;
		}
		return result;
	}

	public static String getTimeOfT(long expires) {
		long time = System.currentTimeMillis();
		return getTimeOfT(time, expires);
	}

	public static String getTimeOfT(long time, long expires) {

		BigInteger b = new BigInteger("0" + time);
		long expiresMs = expires * 1000;
		b = b.divide(new BigInteger(String.valueOf(expiresMs)));
		
		String timeT = b.toString(16).toUpperCase();
		while (timeT.length() < 16) {
			timeT = "0" + timeT;
		}

		return timeT;
	}
	
	public static void main(String[] args) {
		String secret = "3132333435363738393031323334353637383930";

		String otp = TotpToken.generateOtp(secret, "0000000001FCA055", 6);
		System.out.println("OTP: " + otp);

		otp = TotpToken.generateOtp(secret, "00000000023523ED", 6);
		System.out.println(otp);

		otp = TotpToken.generateOtp(secret, "000000000273EF07", 6);
		System.out.println(otp);

		otp = TotpToken.generateOtp(secret, "0000000003F940AA", 6);
		System.out.println(otp);
	}
}
