package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Formacao;
import br.livetouch.livecom.domain.service.FormacaoService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboFormacao extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final FormacaoService service;
	
	/**
	 * 
	 */
	public ComboFormacao(FormacaoService service) {
		super("formacao");
		this.service = service;
		
//		add(new Option("","-- Selecione a formação ---"));

		List<Formacao> values = service.findAll();
		
		for (Formacao p : values) {
			if(p.getId() != null && p.getNome() != null) {
				Option option = new Option(p.getId(), p.getNome());
				add(option);
			}
		}

	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Formacao) {
			Formacao obj = (Formacao) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Formacao q = getFormacao();
			return q;
		}
		return super.getValueObject();
	}

	public Formacao getFormacao() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			Formacao q = service.get(id);
			return q;
		}
		return null;
	}
}
