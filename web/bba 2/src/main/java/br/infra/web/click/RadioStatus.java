package br.infra.web.click;

import net.sf.click.control.Radio;
import net.sf.click.control.RadioGroup;

public class RadioStatus extends RadioGroup {
	private static final long serialVersionUID = 2266546939424630508L;

	public RadioStatus() {
		super("ativo");
		Radio sim = new Radio("1", "Sim");
		sim.setChecked(true);
		add(sim);
		add(new Radio("0", "Não"));
	}
}