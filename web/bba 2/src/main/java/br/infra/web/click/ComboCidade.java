package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Cidade;
import br.livetouch.livecom.domain.service.CidadeService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboCidade extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final CidadeService service;
	
	/**
	 * 
	 */
	public ComboCidade(CidadeService service) {
		super("cidade");
		this.service = service;
		
		List<Cidade> values = service.findAll();
		
		add(new Option("","- Selecione -"));
		setValue("0");
		
		for (Cidade p : values) {
			Option option = new Option(p.getId(), p.getNome());
			add(option);
		}

	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Cidade) {
			Cidade obj = (Cidade) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Cidade q = getCidade();
			return q;
		}
		return super.getValueObject();
	}

	public Cidade getCidade() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			Cidade q = service.get(id);
			return q;
		}
		return null;
	}
}
