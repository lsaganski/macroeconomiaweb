package br.infra.web.click;

import org.apache.commons.lang.StringUtils;

import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboRegiao extends Select {
	private static final long serialVersionUID = -5349652621703786323L;

	/**
	 * 
	 */
	public ComboRegiao() {
		super("regiao");

		Option option = new Option("S", "S");
		add(option);
		option = new Option("SE", "SE");
		add(option);
		option = new Option("CO", "CO");
		add(option);
		option = new Option("N", "N");
		add(option);
		option = new Option("NE", "NE");
		add(option);

	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof String) {
			super.setValueObject(value);
		}
	}

	public String getRegiao() {
		String value = super.getValue();
		if (StringUtils.isNotEmpty(value)) {
			return value;
		}
		return "";
	}
}
