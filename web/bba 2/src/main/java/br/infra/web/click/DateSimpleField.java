package br.infra.web.click;



import java.util.Date;

import br.livetouch.livecom.utils.DateUtils;
import net.sf.click.control.TextField;

/**
 * Combo para exibir os estados do Brasil
 * 
 * @author Walter Scharf
 */
public class DateSimpleField extends TextField {
	String FORMAT = "dd/MM/yyyy";
	/**
	 * 
	 */
	private static final long serialVersionUID = 8823790209481196235L;

	public DateSimpleField(String name) {
		super(name);
	}

	@Override
	public Object getValueObject() {
		return getDate();
	}

	/**
     * Return the field Date value, or null if value was empty or a parsing
     * error occured.
     *
     * @return the field Date value
     */
    public Date getDate() {
        if (value != null && value.length() > 0) {
        	 Date date = DateUtils.toDate(value, FORMAT, null);

             return new Date(date.getTime());

        } else {
            return null;
        }
    }

	
	public String getDateString() {
		return DateUtils.toString(getDate(),FORMAT);
	}

	@Override
	public void setValueObject(Object object) {
        if (object != null) {
            if (Date.class.isAssignableFrom(object.getClass())) {
                setDate((Date) object);

            } else {
                String msg =
                    "Invalid object class: " + object.getClass().getName();
                throw new IllegalArgumentException(msg);
            }
        }
	}
	
	/**
     * Set the field Date value.
     *
     * @param date the Date value to set
     */
    public void setDate(Date date) {
        if (date != null) {
            value = DateUtils.toString(date, FORMAT);
        }
    }

	@Override
	public String getValue() {
		return super.getValue();
	}
	
	@Override
	public void setValue(String value) {
		super.setValue(value);
	}
}
