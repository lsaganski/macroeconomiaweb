package br.infra.web.click;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.enums.TipoLogSistema;
import net.sf.click.control.Option;
import net.sf.click.control.Select;



public class ComboTipoLogSistema extends Select{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2526837590528672970L;

	public ComboTipoLogSistema() {
		super("tipo");		

		TipoLogSistema[] values = TipoLogSistema.values();

		add(new Option("-1", "--- Todos--- "));

		for (TipoLogSistema tipo : values) {			
			int value = tipo.ordinal();
			//String label = getMessage(situacao.toString().toUpperCase());
			String label = tipo.toString();

			//add(new Radio(String.valueOf(value),label));

			add(new Option(String.valueOf(value), label));
		}
	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof TipoLogSistema) {
			TipoLogSistema tipo = (TipoLogSistema) value;
			String ordinal = String.valueOf(tipo.ordinal());
			super.setValueObject(ordinal);
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Integer ordinal = Integer.parseInt(value.toString());
			if(ordinal == -1) {
				return null;
			}
			TipoLogSistema tipo = TipoLogSistema.values()[ordinal];
			return tipo;
		}
		return super.getValueObject();
	}

	public TipoLogSistema getTipoLog() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Integer ordinal = Integer.parseInt(value.toString());
			TipoLogSistema tipo = TipoLogSistema.values()[ordinal];
			return tipo;
		}
		return null;
	}
}


