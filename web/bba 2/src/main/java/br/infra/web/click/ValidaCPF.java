package br.infra.web.click;

public class ValidaCPF
{
   private static int count;
   private static int Soma;
private static int x;
private static int y;
private static int CPF[] = new int[11];
   
   // Construtor de classe
   public ValidaCPF()
   {
       Soma = 0;
       for(count = 0; count < 11; count++)
           CPF[count] = 0;
   }
   
   // Método responsável pela validação do CPF
   // formato do CPF: abc.def.ghi-xy
   public static boolean verificaCPF(String cpf)
   {
		if (cpf != null) {
			if (cpf.length() != 11) {
				return false;
			}
		}else{
			return false;
		}
       char vetorChar[] = new char[11];
       String temp, CPFvalido = "";
       
       for(count = 0; count < 11; count++)
       {
           // Transformar String em vetor de caracteres
           vetorChar = cpf.toCharArray();
           // Transformar cada caractere em String
           temp = String.valueOf(vetorChar[count]);
           // Transformar String em inteiro e jogar no vetor
           CPF[count] = Integer.parseInt(temp);
       }    
       // Método da árvore para obter o x
       for(count = 0; count < 9; count++)
       {
           // Pegar soma da permutação dos dígitos do CPF
           Soma += CPF[count] * (10 - count);
       }    
       
       // se o resto da divisão der 0 ou 1, x terá dois dígitos
       if(Soma % 11 < 2)
           x = 0;  // x não pode ter dois dígitos
       else
           x = 11 - (Soma % 11); // obtendo o penúltimo dígito do CPF
       
       CPF[9] = x;
       
       // Método da árvore para obter o y
       Soma = 0;
       for(count = 0; count < 10; count++)
       {
           // Pegar soma da permutação dos dígitos do CPF
           Soma += CPF[count] * (11 - count);
       }
       
       // se o resto da divisão der 0 ou 1, y terá dois dígitos
       if(Soma % 11 < 2)
           y = 0;  // y não pode ter dois dígitos
       else
           y = 11 - (Soma % 11); // obtendo o último dígito do CPF

       CPF[10] = y;
       Soma = 0;

       // Verificando se o cpf informado é válido para retornar resultado ao programa
       for(count = 0; count < 11; count++)
       {
           CPFvalido += String.valueOf(CPF[count]);
       }    
       if(cpf.compareTo(CPFvalido) == 0)
           return true;
       else
           return false;
   }
   
   public String gerarCPF()
   {
       int aleatorio, ValoresAleatorios[] = new int[9];
       String cpf = "";
       
       // Obtendo os 9 primeiros números do CPF
       for(count = 0; count < 9; count++)
       {
           aleatorio = (int) (Math.random() * 9);

           ValoresAleatorios[count] = aleatorio;
           cpf += String.valueOf(ValoresAleatorios[count]);
       }
       
       verificaCPF(cpf.concat("00")); // Enviando os 9 dígitos sorteados junto com os números 00
       cpf += String.valueOf(x); // Obtendo o penúltimo dígito do CPF criado por esta função
       cpf += String.valueOf(y); // Obtendo o último dígito do CPF criado por esta função

       return cpf;
   }
}
