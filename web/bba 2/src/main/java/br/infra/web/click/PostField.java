package br.infra.web.click;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Post;
import br.livetouch.livecom.domain.service.PostService;
import net.sf.click.control.TextField;

/**
 * @author ricardo
 *
 */
public class PostField extends TextField {
	private static final long serialVersionUID = 7324379192779327235L;

	private Post entity;

	/** <? super T> para o caso de a Entity em questão usar o service da classe pai */
	private final PostService service;

	public PostField(PostService service) {
		super("post_id");
		this.service = service;
	}
	
	public PostField(boolean required,PostService service) {
		super("post_id", required);
		this.service = service;
	}
	/**
	 * Retorna a entidade que entao sendo editada ou inserida
	 * 
	 * @return
	 */
	public Post getPost() {

		if(service == null){
			throw new IllegalStateException("O service não foi definido");
		}
		
		String id = getValue();

		if(NumberUtils.isNumber(id)) {
			entity = service.get(new Long(id));
		}

		return entity;
	}
}
