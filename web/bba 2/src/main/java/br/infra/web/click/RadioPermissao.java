package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.service.PerfilService;
import net.sf.click.control.Radio;
import net.sf.click.control.RadioGroup;

public class RadioPermissao extends RadioGroup{
	private static final long serialVersionUID = -5349652621703786323L;
	private final PerfilService service;
	
	/**
	 * 
	 */
	public RadioPermissao(PerfilService service) {
		super("permissao");
		this.service = service;
		
		List<Perfil> values = service.findAll();
		
		for (Perfil p : values) {
			if(p.getId() != null && p.getNomeDescricao() != null) {
				Radio option = new Radio(p.getId().toString(), p.getNomeDescricao());
				add(option);
				
				if(p.isPadrao()) {
					option.setChecked(true);
				}
			}
		}
	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Perfil) {
			Perfil obj = (Perfil) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Perfil q = getPermissao();
			return q;
		}
		return super.getValueObject();
	}

	public Perfil getPermissao() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			Perfil q = service.get(id);
			return q;
		}
		return null;
	}
}
