package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Perfil;
import br.livetouch.livecom.domain.service.PerfilService;
import net.sf.click.control.Radio;
import net.sf.click.control.RadioGroup;

public class RadioPerfil extends RadioGroup{
	private static final long serialVersionUID = -5349652621703786323L;
	private final PerfilService service;
	
	/**
	 * 
	 */
	public RadioPerfil(PerfilService service, Empresa e) {
		super("perfil");
		this.service = service;
		
		List<Perfil> values = service.findAll(e);
		
		for (Perfil p : values) {
			Radio option = new Radio(p.getId().toString(), p.getNome());
			add(option);
		}

	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Perfil) {
			Perfil obj = (Perfil) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Perfil q = getPerfil();
			return q;
		}
		return super.getValueObject();
	}

	public Perfil getPerfil() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			Perfil q = service.get(id);
			return q;
		}
		return null;
	}
}
