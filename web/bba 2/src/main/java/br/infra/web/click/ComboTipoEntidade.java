package br.infra.web.click;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.enums.AuditoriaEntidade;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboTipoEntidade extends Select{

       /**
        * 
        */
       private static final long serialVersionUID = -5349652621703786323L;

       public ComboTipoEntidade() {
               super("tipoEntidade");

               AuditoriaEntidade[] values = AuditoriaEntidade.values();
               add(new Option("-1", "Todos"));

               for (AuditoriaEntidade tipo : values) {
                       int value = tipo.ordinal();
                       String label = tipo.toString();
                       add(new Option(String.valueOf(value), label));
               }
       }

       /**
        * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
        */
       @Override
       public void setValueObject(Object value) {
               if (value != null && value instanceof AuditoriaEntidade) {
            	   AuditoriaEntidade tipo = (AuditoriaEntidade) value;
                       String ordinal = String.valueOf(tipo.ordinal());
                       super.setValueObject(ordinal);
               }
       }

       @Override
       public Object getValueObject() {
               String value = super.getValue();
               if (value != null && NumberUtils.isNumber(value.toString())) {
                       Integer ordinal = Integer.parseInt(value.toString());
                       if(ordinal == -1) {
                               return null;
                       }
                       AuditoriaEntidade tipo = AuditoriaEntidade.values()[ordinal];
                       return tipo;
               }
               return super.getValueObject();
       }

       public AuditoriaEntidade getTipoEntidade() {
               String value = super.getValue();
               if (value != null && NumberUtils.isNumber(value.toString())) {
                       Integer ordinal = Integer.parseInt(value.toString());
                       AuditoriaEntidade tipo = AuditoriaEntidade.values()[ordinal];
                       return tipo;
               }
               return null;
       }
}

