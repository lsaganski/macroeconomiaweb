package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Funcao;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.FuncaoService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboFuncao extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final FuncaoService service;
	
	/**
	 * 
	 */
	public ComboFuncao(FuncaoService service,Usuario userInfo) {
		super("funcao");
		this.service = service;
		
		List<Funcao> values = service.findAll(userInfo);
		
		for (Funcao p : values) {
			if(p.getId() != null && p.getNome() != null) {
				Option option = new Option(p.getId(), p.getNome());
				add(option);
			}
		}

	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Funcao) {
			Funcao obj = (Funcao) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Funcao q = getFuncao();
			return q;
		}
		return super.getValueObject();
	}

	public Funcao getFuncao() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			Funcao q = service.get(id);
			return q;
		}
		return null;
	}
}
