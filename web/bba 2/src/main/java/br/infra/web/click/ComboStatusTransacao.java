package br.infra.web.click;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.enums.StatusTransacao;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

/**
 * @author Ricardo Lecheta
 *
 */
public class ComboStatusTransacao extends Select{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5349652621703786323L;

	public ComboStatusTransacao() {
		super("status");

		StatusTransacao[] values = StatusTransacao.values();
		add(new Option("-1", "--- Todos ---"));

		for (StatusTransacao status : values) {
			int value = status.ordinal();
			//String label = getMessage(situacao.toString().toUpperCase());
			String label = status.toString();

			//add(new Radio(String.valueOf(value),label));
			add(new Option(String.valueOf(value), label));
		}
	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof StatusTransacao) {
			StatusTransacao tipo = (StatusTransacao) value;
			String ordinal = String.valueOf(tipo.ordinal());
			super.setValueObject(ordinal);
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Integer ordinal = Integer.parseInt(value.toString());
			if(ordinal == -1) {
				return null;
			}
			StatusTransacao tipo = StatusTransacao.values()[ordinal];
			return tipo;
		}
		return super.getValueObject();
	}

	public StatusTransacao getStatusTransacao() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Integer ordinal = Integer.parseInt(value.toString());
			StatusTransacao tipo = StatusTransacao.values()[ordinal];
			return tipo;
		}
		return null;
	}
}


