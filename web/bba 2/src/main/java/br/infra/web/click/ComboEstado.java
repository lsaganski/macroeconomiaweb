package br.infra.web.click;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import br.livetouch.livecom.domain.enums.Estado;
import net.sf.click.control.Option;
import net.sf.click.control.Select;
import net.sf.click.util.HtmlStringBuffer;

/**
 * Combo para exibir os estados do Brasil
 * 
 * @author Walter Scharf
 */
@SuppressWarnings({"rawtypes"})
public class ComboEstado extends Select {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8646508771616572945L;
	private final String EMPTY = "";
	private boolean showPropertyLabel = false;

	public ComboEstado(){
		this(null);
	}

	public ComboEstado(String nome){
		this(nome,null);
	}

	public ComboEstado(String name, String header){
		this(name, header, null);
	}

	public ComboEstado(String name, String header, String label){
		super(name, label);
		if(header != null){
			setRequired(true);
		}

		setValue(EMPTY);
	}

	@Override
	public void bindRequestValue() {
		refreshOptions();
		super.bindRequestValue();
	}

	/**
	 * atualiza a lista de opcoes
	 */
	public void refreshOptions() {

		setOptionList(new ArrayList());

//		add(new Option("","- Selecione -"));
//		setValue("0");

		setUpOptionList();
	}

	@Override
	public void render(HtmlStringBuffer buffer) {
		refreshOptions();
		super.render(buffer);
	}

	@Override
	public String toString() {
		refreshOptions();
		return super.toString();
	}

	/**
	 * Seta o combo estados com os valores determinados
	 */
	private void setUpOptionList(){
		for (Estado estado : Estado.values()) {
			add(new Option(estado, showPropertyLabel ? getMessage(estado.name() + ".label") : estado.name()));
		}
	}

	/**
	 * sobreescreve para que nao sejah mais necessario ter registros la lista para efetuar a validacao
	 * e para que o primeiro da lista nao seja sempre considerado como cabecalho, porque o cabecalho nao eh um registro valido para selecionar
	 * se este for required
	 * 
	 * agora a entrada sera invalida quando nao houverem opcoes no combo ou quando a opcao selecionada for menor que 0 ou vazia
	 * 
	 * @see net.sf.click.control.Select#validate()
	 */
	@Override
	public void validate() {
		setError(null);

		if (isRequired()) {
			if (isMultiple()) {
				if (getSelectedValues().isEmpty()) {
					setErrorMessage("select-error");
				}

			} else {
				if (getValue().length() == 0) {
					setErrorMessage("select-error");

				} else {
					if (getOptionList().isEmpty()) {
						setErrorMessage("select-error");
					}

					if (getValue() == null || getValue().isEmpty()) {
						setErrorMessage("select-error");
					}
				}
			}
		}
	}
	
	@Override
	public void setValueObject(Object object) {
		super.setValueObject(object);
	}

	@Override
	public Object getValueObject() {
		Estado estado = getEstado();
		if(estado != null) {
			return estado.toString();
		}
		return "";
//		return Estado.SP.toString();
	}
	
	@Override
	public String getValue() {
		return super.getValue();
	}

	public Estado getEstado() {
		String value = getValue();
		if(StringUtils.equals(value, "0")) {
			return null;
		}
		Estado estado = StringUtils.isNotBlank(value) ? Estado.valueOf(value) : null;
		return estado;
	}

	/**
	 * Caso seja configurado para "true", o sistema irá buscar o nome do estado no properties (ex.: MT.label=Mato Grosso)
	 * 
	 * @param showPropertyLabel the showPropertyLabel to set
	 */
	public void setShowPropertyLabel(boolean showPropertyLabel) {
		this.showPropertyLabel = showPropertyLabel;
	}
}
