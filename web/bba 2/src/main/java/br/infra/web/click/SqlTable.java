package br.infra.web.click;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.NestableRuntimeException;

import net.livetouch.extras.db.util.JdbcParamUtils;
import net.sf.click.Context;
import net.sf.click.control.Column;
import net.sf.click.control.Table;
import net.sf.click.util.HtmlStringBuffer;

/**
 * Utilitary table component for newbies play with SQL.
 * 
 * That's not recommended.
 * 
 * @author rlecheta
 * 
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class SqlTable extends Table {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3164535638430079534L;
	private final DataSource dataSource;

	public SqlTable(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
		setClass(Table.CLASS_SIMPLE);
	}

	public SqlTable(String name, DataSource dataSource) {
		super(name);
		this.dataSource = dataSource;

		setClass(Table.CLASS_SIMPLE);
	}

	public void executeQuery(String sql) throws SQLException {
		executeQuery(sql, null);
	}

	public void executeQuery(String sql,  List parameters) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			ps = connection.prepareStatement(sql);

			if (parameters != null) {
				JdbcParamUtils.addParameters(parameters, ps);
			}

			rs = ps.executeQuery();

			setResultSet(rs);
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			throw new NestableRuntimeException(e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				throw e;
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				throw e;
			}
		}
	}

	public int execute(String sql) throws SQLException {

		java.sql.Statement ps = null;
		Connection connection = null;
		try {
			connection  = dataSource.getConnection();

			ps = connection.createStatement();

			int results = ps.executeUpdate(sql);

			if(!connection.getAutoCommit()) {
				connection.commit();
			}

			return results;
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			throw new NestableRuntimeException(e);
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				throw e;
			}
		}
	}

	/**
	 * Create the rowList using the ResultSet
	 * 
	 * @param rs
	 * @throws SQLException 
	 */
	
	private void setResultSet(ResultSet rs) throws SQLException {

		try {
			ResultSetMetaData meta = rs.getMetaData();
			List columnNames = new ArrayList();
			int columnsSize = meta.getColumnCount();
			for (int i = 0; i < columnsSize; i++) {
				String columnName = meta.getColumnName(i + 1);
				columnName = StringUtils.capitalize(columnName.toLowerCase());
				columnNames.add(columnName);

				MapColumn columnResultado = new MapColumn(columnName);
				columnResultado.setAttribute("nowrap", "true");
				addColumn(columnResultado);
			}

			List rowList = new ArrayList();

			while (rs.next()) {
				Map row = new HashMap();
				for (int i = 0; i < columnsSize; i++) {
					row.put(columnNames.get(i), rs.getString(i + 1));
				}
				rowList.add(row);
			}

			setRowList(rowList);
		} catch (SQLException e) {
			throw e;
		} catch (Exception e) {
			throw new NestableRuntimeException(e);
		} 
	}

	/**
	 * @see net.sf.click.control.Table#renderBodyRows(net.sf.click.util.HtmlStringBuffer)
	 */
	protected void renderBodyRows(HtmlStringBuffer buffer) {
		buffer.append("<tbody>\n");

		// Range sanity check
		int pageNumber = Math.min(getPageNumber(), getRowList().size() - 1);
		pageNumber = Math.max(pageNumber, 0);
		setPageNumber(pageNumber);

		int firstRow = getFirstRow();
		int lastRow = getLastRow();

		if (lastRow == 0) {
			renderBodyNoRows(buffer);
		} else {
			final List tableRows = getRowList();

			for (int i = firstRow; i < lastRow; i++) {
				boolean even = (i + 1) % 2 == 0;
				if (even) {
					buffer.append("<tr class=\"even\"");
				} else {
					buffer.append("<tr class=\"odd\"");
				}

				if (getHoverRows()) {
					buffer.append(" onmouseover=\"this.className='hover';\"");
					buffer.append(" onmouseout=\"this.className='");
					if (even) {
						buffer.append("even");
					} else {
						buffer.append("odd");
					}
					buffer.append("';\"");
				}

				buffer.append(">\n");

				renderBodyRowColumns(buffer, i);

				buffer.append("</tr>");
				if (i < tableRows.size() - 1) {
					buffer.append("\n");
				}
			}
		}

		buffer.append("</tbody>");
	}

	private final class MapColumn extends Column {
		/**
		 * 
		 */
		private static final long serialVersionUID = -3170840828905779686L;

		private MapColumn(String name) {
			super(name);
		}

		protected void renderTableDataContent(Object row, HtmlStringBuffer buffer, Context context, int rowIndex) {

			if (getDecorator() != null) {
				Object value = getDecorator().render(row, context);
				if (value != null) {
					buffer.append(value);
				}

			} else {
				Object columnValue = ((Map) row).get(getName());
				if (columnValue != null) {
					if (getAutolink() && renderLink(columnValue, buffer)) {
						// Has been rendered

					} else if (getMessageFormat() != null) {
						Object[] args = new Object[] { columnValue };
						buffer.append(getMessageFormat().format(args));

					} else {
						buffer.append(columnValue);
					}
				}
			}

		}
	}
}