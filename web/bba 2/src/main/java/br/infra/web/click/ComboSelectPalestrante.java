package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Palestrante;
import br.livetouch.livecom.domain.service.PalestranteService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboSelectPalestrante extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final PalestranteService service;
	
	public ComboSelectPalestrante(PalestranteService service) {
		super("palestrante");
		this.service = service;
		
		List<Palestrante> values = service.findAll();
		
		add(new Option("","Selecione"));
		setValue("0");
		
		for (Palestrante e : values) {
			Option option = new Option(e.getId(), e.getNome());
			add(option);
		}

	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Palestrante) {
			Palestrante obj = (Palestrante) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Palestrante q = getPalestrante();
			return q;
		}
		return super.getValueObject();
	}

	public Palestrante getPalestrante() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			Palestrante e = service.get(id);
			return e;
		}
		return null;
	}
}
