package br.infra.web.click;

import org.apache.commons.lang.StringUtils;

import br.infra.util.Utils;
import net.sf.click.control.TextField;


/**
 * Converte o celular para o formato (99)9999-9999
 * 
 * @author ricardo
 *
 */
public class CelularField extends TextField {
	private static final long serialVersionUID = -4436025808116929207L;

	public CelularField() {
		super("celular");
	}

	@Override
	public String getValue() {
		String s = super.getValue();
		if(StringUtils.isNotEmpty(s)) {
			s = Utils.getCelularFormatado(s);
		}
		return s;
	}

	@Override
	public void setValue(String value) {
		if(value != null) {
			value = Utils.getCelularDesformatado(value);
		}
		super.setValue(value);
	}

	@Override
	public void validate() {
		super.validate();

		String celular = this.value;

		if(celular != null) {
			boolean ok = Utils.validaCelular(celular);
			if(!ok) {
				setError("Celular Inválido.");
			}
		}

	}

	@Override
	public Object getValueObject() {
		return super.getValueObject();
	}

	@Override
	public void setValueObject(Object object) {
		super.setValueObject(object);
	}
}
