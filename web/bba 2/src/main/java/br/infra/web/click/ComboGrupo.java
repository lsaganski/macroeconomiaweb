package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Grupo;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.GrupoService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboGrupo extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final GrupoService service;
	
	/**
	 * 
	 */
	public ComboGrupo(GrupoService service,Usuario u) {
		super("grupo");
		this.service = service;
		
		add(new Option("", "Todos"));
		
		List<Grupo> values = service.getGrupos(u);
		
		for (Grupo p : values) {
			if(p.getId() != null && p.getNome() != null) {
				Option option = new Option(p.getId(), p.getNome());
				add(option);
			}
		}

	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Grupo) {
			Grupo obj = (Grupo) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Grupo q = getGrupo();
			return q;
		}
		return super.getValueObject();
	}

	public Grupo getGrupo() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			Grupo q = service.get(id);
			return q;
		}
		return null;
	}
}
