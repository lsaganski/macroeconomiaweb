package br.infra.web.click;

import org.apache.commons.lang.StringUtils;

import br.livetouch.livecom.domain.service.Service;
import net.livetouch.tiger.ddd.Entity;
import net.sf.click.control.Select;



/**
 * Select que permite copiar os valores diretamente do Form para o Domain Model
 * 
 * @author ricardo
 *
 */
public class EntitySelect<T extends Entity> extends Select {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8632687403337154327L;

	private T entity;

	/** <? super T> para o caso de a Entity em questão usar o service da classe pai */
	private Service<? super T> service;

	public EntitySelect(String name, boolean required,Service<T> service) {
		super(name, required);
		this.service = service;
	}

	public EntitySelect(String name, String label, boolean required,Service<T> service) {
		super(name, label, required);
		this.service = service;
	}

	public EntitySelect(String name, String label,Service<T> service) {
		super(name, label);
		this.service = service;
	}

	public EntitySelect(String name,Service<T> service) {
		super(name);
		this.service = service;
	}

	public void setService(Service<? super T> service){
		this.service = service;
	}

	/**
	 * se o objeto que estiver sendo controlado já estiver persistido, retorna true
	 * 
	 * @return
	 */
	public boolean isUpdate(){
		return  getEntity() != null && getEntity().getId() != null;
	}

	/**
	 * Retorna a entidade que entao sendo editada ou inserida
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T getEntity() {

		Object valueObject = super.getValueObject();
		if(valueObject instanceof Entity){
			T entity = (T) valueObject;
			return entity;
		}

		if(service == null){
			throw new IllegalStateException("O service  foi definido");
		}

		Long id = getEntityId();

		// Pode ser que mudou o id, então tem que checar...
		// Isto porque ao criar o combo, pode setar o valor do primeiro item sempre
		if(entity == null || entity.getId() != id) {

			if(id != null) {
				//se tem o id, pega a entidade persistida
				entity = (T) service.get(id);
			}
		}

		return entity;
	}

	private Long getEntityId() {

		String value = super.getValue();
		
		if (StringUtils.isEmpty(value)) {
			bindRequestValue();
			value = super.getValue();
		}

		return StringUtils.isEmpty(value) ? null : new Long(value);
	}

	@Override
	public String getValue() {

		//pega o valor do HiddenField, se no render acaba saindo o toString da 
		//Entity, que é recuperada pelo getValueObject() que foi sobrescrito aqui (this)
		Object value = super.getValueObject();

		return value != null ? value.toString() : "";
	}

	@Override
	public T getValueObject() {
		return getEntity();
	}

	@SuppressWarnings("unchecked")
	public void setValue(String value) {
		super.setValue(value);
		
		Long id = StringUtils.isNotEmpty(value) ? new Long(value) : null;

		if(id != null) {
			//se tem o id, pega a entidade persistida
			entity = (T) service.get(id);
			setValueObject(entity);
		}
	}

	@SuppressWarnings("unchecked")
	public void setValueObject(Object object) {
		super.setValueObject(object);
		
		if(object instanceof Entity){
			this.entity = (T) object;

			Long id = entity != null ? entity.getId() : null;

			super.setValueObject(id);
		}
	}

	public void setValueObject(T object) {

		this.entity = object;

		Long id = entity != null ? entity.getId() : null;

		super.setValueObject(id);
	}
}
