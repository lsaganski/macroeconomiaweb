package br.infra.web.click;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.Expediente;
import br.livetouch.livecom.domain.service.ExpedienteService;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboExpediente extends Select{
	private static final long serialVersionUID = -5349652621703786323L;
	private final ExpedienteService service;
	
	/**
	 * 
	 */
	public ComboExpediente(ExpedienteService service) {
		super("expediente");
		this.service = service;
		
		List<Expediente> values = service.findAll();
		
		for (Expediente e : values) {
			if(e.getId() != null && e.getCodigo() != null) {
				Option option = new Option(e.getId(), e.getCodigo());
				add(option);
			}
		}

	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Expediente) {
			Expediente obj = (Expediente) value;
			super.setValueObject(obj.getId());
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Expediente e = getExpediente();
			return e;
		}
		return super.getValueObject();
	}

	public Expediente getExpediente() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Long id = Long.parseLong(value.toString());
			Expediente e = service.get(id);
			return e;
		}
		return null;
	}
}
