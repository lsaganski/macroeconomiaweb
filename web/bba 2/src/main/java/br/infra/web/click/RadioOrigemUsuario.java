package br.infra.web.click;

import net.sf.click.control.Radio;
import net.sf.click.control.RadioGroup;

public class RadioOrigemUsuario extends RadioGroup {
	private static final long serialVersionUID = 2266546939424630508L;

	public RadioOrigemUsuario() {
		super("flags");
		
		add(new Radio("ldap", "Interno (LDAP)"));
		add(new Radio("", "Externo"));
	}
}


