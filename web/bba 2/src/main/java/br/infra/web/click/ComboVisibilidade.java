package br.infra.web.click;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.enums.Visibilidade;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboVisibilidade extends Select{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5349652631703786323L;

	public ComboVisibilidade() {
		super("visibilidade");

		Visibilidade[] values = Visibilidade.values();

		for (Visibilidade visibilidade : values) {
			int value = visibilidade.ordinal();
			String label = visibilidade.getDesc2();
			add(new Option(String.valueOf(value), label));
		}
	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof Visibilidade) {
			Visibilidade visibilidade = (Visibilidade) value;
			String ordinal = String.valueOf(visibilidade.ordinal());
			super.setValueObject(ordinal);
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Integer ordinal = Integer.parseInt(value.toString());
			if(ordinal == -1) {
				return null;
			}
			Visibilidade tipo = Visibilidade.values()[ordinal];
			return tipo;
		}
		return super.getValueObject();
	}

	public Visibilidade getVisibilidade() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Integer ordinal = Integer.parseInt(value.toString());
			Visibilidade visibilidade = Visibilidade.values()[ordinal];
			return visibilidade;
		}
		return null;
	}
}


