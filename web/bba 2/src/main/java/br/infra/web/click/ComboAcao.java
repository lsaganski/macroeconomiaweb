package br.infra.web.click;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.enums.AuditoriaAcao;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboAcao extends Select{

       /**
        * 
        */
       private static final long serialVersionUID = -5349652621703786323L;

       public ComboAcao() {
               super("acao");

               AuditoriaAcao[] values = AuditoriaAcao.values();
               add(new Option("-1", "Todos"));

               for (AuditoriaAcao acao : values) {
                       int value = acao.ordinal();
                       String label = acao.toString();
                       add(new Option(String.valueOf(value), label));
               }
       }

       /**
        * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
        */
       @Override
       public void setValueObject(Object value) {
               if (value != null && value instanceof AuditoriaAcao) {
            	   AuditoriaAcao tipo = (AuditoriaAcao) value;
                       String ordinal = String.valueOf(tipo.ordinal());
                       super.setValueObject(ordinal);
               }
       }

       @Override
       public Object getValueObject() {
               String value = super.getValue();
               if (value != null && NumberUtils.isNumber(value.toString())) {
                       Integer ordinal = Integer.parseInt(value.toString());
                       if(ordinal == -1) {
                               return null;
                       }
                       AuditoriaAcao tipo = AuditoriaAcao.values()[ordinal];
                       return tipo;
               }
               return super.getValueObject();
       }

       public AuditoriaAcao getStatusMensagem() {
               String value = super.getValue();
               if (value != null && NumberUtils.isNumber(value.toString())) {
                       Integer ordinal = Integer.parseInt(value.toString());
                       AuditoriaAcao tipo = AuditoriaAcao.values()[ordinal];
                       return tipo;
               }
               return null;
       }
}

