package br.infra.web.click;

import net.sf.click.control.Radio;
import net.sf.click.control.RadioGroup;

public class RadioGenero extends RadioGroup {
	private static final long serialVersionUID = 2266546939424630508L;

	public RadioGenero() {
		super("genero");
		
		add(new Radio("M", "Masculino"));
		add(new Radio("F", "Feminino"));
	}
}


