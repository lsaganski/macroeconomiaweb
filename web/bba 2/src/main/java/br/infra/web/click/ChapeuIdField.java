package br.infra.web.click;

import br.livetouch.livecom.domain.Chapeu;
import br.livetouch.livecom.domain.service.Service;

/**
 * Select que permite copiar os valores diretamente do Form para o Domain Model
 * 
 * @author ricardo
 *
 */
public class ChapeuIdField extends EntityField<Chapeu> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1338131318964372751L;

	public ChapeuIdField(String name, boolean required, Service<Chapeu> service) {
		super(name, required, service);
	}

	public Chapeu getChapeu() {
		return getEntity();
	}
}
