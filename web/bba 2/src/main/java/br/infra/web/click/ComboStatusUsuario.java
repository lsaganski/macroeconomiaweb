package br.infra.web.click;

import org.apache.commons.lang.math.NumberUtils;

import br.livetouch.livecom.domain.enums.StatusUsuario;
import net.sf.click.control.Option;
import net.sf.click.control.Select;

public class ComboStatusUsuario extends Select{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5349652621703786323L;

	public ComboStatusUsuario() {
		super("status");

		StatusUsuario[] values = StatusUsuario.values();
		add(new Option("-1", "--- Status ---"));

		for (StatusUsuario status : values) {
			int value = status.ordinal();
			//String label = getMessage(situacao.toString().toUpperCase());
			String label = status.toString();

			//add(new Radio(String.valueOf(value),label));
			add(new Option(String.valueOf(value), label));
		}
	}

	/**
	 * @see net.sf.click.control.Field#setValueObject(java.lang.Object)
	 */
	@Override
	public void setValueObject(Object value) {
		if (value != null && value instanceof StatusUsuario) {
			StatusUsuario tipo = (StatusUsuario) value;
			String ordinal = String.valueOf(tipo.ordinal());
			super.setValueObject(ordinal);
		}
	}

	@Override
	public Object getValueObject() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Integer ordinal = Integer.parseInt(value.toString());
			if(ordinal == -1) {
				return null;
			}
			StatusUsuario tipo = StatusUsuario.values()[ordinal];
			return tipo;
		}
		return super.getValueObject();
	}

	public StatusUsuario getStatusUsuario() {
		String value = super.getValue();
		if (value != null && NumberUtils.isNumber(value.toString())) {
			Integer ordinal = Integer.parseInt(value.toString());
			StatusUsuario tipo = StatusUsuario.values()[ordinal];
			return tipo;
		}
		return null;
	}
}


