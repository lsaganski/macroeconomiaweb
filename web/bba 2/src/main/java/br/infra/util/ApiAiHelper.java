package br.infra.util;

import java.io.UnsupportedEncodingException;

import ai.api.AIConfiguration;
import ai.api.AIDataService;
import ai.api.AIServiceContext;
import ai.api.AIServiceException;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;

public class ApiAiHelper {
	private AIDataService service;
	private Boolean isResponseOk;
	private AIResponse response;


	//"97a8d20a71694be58042a1fc91e8ff18"
	public ApiAiHelper(String token) {
		AIConfiguration aiConfig = new AIConfiguration(token, AIConfiguration.SupportedLanguages.PortugueseBrazil);
		service = new AIDataService(aiConfig);
	}
	
	
	public AIResponse sendText(String text, String sessionId) throws AIServiceException, UnsupportedEncodingException {
		AIServiceContextImpl ai = new AIServiceContextImpl();
		ai.setSessionId(sessionId);
		AIRequest request = new AIRequest(text);
		response = service.request(request, ai);
		this.isResponseOk = getResponse().getStatus().getCode() == 200;
		return response;
	}
	
	public boolean responseOk() {
		return this.isResponseOk != null ? isResponseOk : false;
	}


	public AIResponse getResponse() {
		return response;
	}
	
	
	
	class AIServiceContextImpl implements AIServiceContext {

		private String sessionId;

		@Override
		public String getSessionId() {
			return this.sessionId;
		}
		
		public String setSessionId(String sessionId) {
			return this.sessionId = sessionId;
		}
		
	}
}
