package br.infra.util;

import java.io.StringWriter;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import br.livetouch.livecom.email.EmailUtils;

public class VelocityUtil {
	
	protected static Logger log = Log.getLogger(EmailUtils.class);

	public static String fromFile(String file, Map<String, Object> params) {
		
		try {
			VelocityEngine ve = new VelocityEngine();
			ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
			ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
			ve.init();
			VelocityContext context = new VelocityContext();
			Template t = ve.getTemplate( "templates/" + file, "UTF-8" );
			if(params != null && params.size() > 0) {
				for(Map.Entry<String, Object> entry : params.entrySet()) {
					context.put(entry.getKey(), entry.getValue());
				}
			}
			StringWriter writer = new StringWriter();
			t.merge(context, writer);
			return writer.toString();
			
		} catch (Exception e) {
			return null;
		}
	}
	
	public static String fromString(String data,  Map<String, Object> params) {
		try {
			log.debug("Construindo o template de e-mail com os params= " + params);
			Velocity.init();
			VelocityContext context = new VelocityContext();
	        for(Map.Entry<String, Object> entry : params.entrySet()) {
				context.put(entry.getKey(), entry.getValue());
			}
	        StringWriter writer = new StringWriter();
	        Velocity.evaluate(context, writer, "", data);
	        return writer.toString();
		} catch (Exception e) {
			log.debug("Erro ao gerar o template de e-mail " + e.getMessage(), e);
			for(Map.Entry<String, Object> entry : params.entrySet()) {
				data = StringUtils.replace(data, "${" + entry.getKey() + "}", (String) entry.getValue());
				data = StringUtils.replace(data, "$!{" + entry.getKey() + "}", (String) entry.getValue());
			}
			return data;
		}
	}
}