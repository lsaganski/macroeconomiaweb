package br.infra.util;
import java.text.MessageFormat;
import java.util.ResourceBundle;


public class MessageUtil {

	private static final ResourceBundle b = ResourceBundle.getBundle("messages");

	public static String getString(String key) {
		try {
			String s = b.getString(key);
			return s;
		} catch (Exception e) {
			return key;
		}
	}

	public static String getString(String key, Object[] args) {
		try {
			String s = b.getString(key);
			s =  format(s, args);
			return s;
		} catch (Exception e) {
			return key;
		}
	}

	private static String format(String s, Object[] args) {
		return MessageFormat.format(s, args);
	}

	private static String format(String s, Object arg) {
		return MessageFormat.format(s, arg);
	}

	public static String getString(String key, Object arg) {
		try {
			String s = b.getString(key);
			s =  format(s, arg);
			return s;
		} catch (Exception e) {
			return key;
		}
	}
}
