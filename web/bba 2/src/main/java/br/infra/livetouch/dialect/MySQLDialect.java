package br.infra.livetouch.dialect;

import java.sql.Types;

import org.hibernate.dialect.MySQL5Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StandardBasicTypes;

public class MySQLDialect extends MySQL5Dialect{
	
	public MySQLDialect() {
		super();
		registerColumnType(Types.CLOB, "longvarchar");
		registerFunction("DATE_FORMAT", new SQLFunctionTemplate(StandardBasicTypes.STRING, "DATE_FORMAT(?1, ?2)") );
		registerFunction("STR_TO_DATE", new DateFormatSqlFunction(StandardBasicTypes.DATE, "STR_TO_DATE(?1, ?2)"));
		registerFunction("CONVERT_DATE_TO_TIME", new ConvertSqlFunction(StandardBasicTypes.TIME, "?1"));
	}
	
}
