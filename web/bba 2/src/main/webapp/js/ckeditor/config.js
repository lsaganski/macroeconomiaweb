/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
	var stringConfig = 'bidi,pastefromword,dialogadvtab,div,flash,format,forms,horizontalrule,iframe,justify,liststyle,pagebreak,showborders,stylescombo,table,tabletools,templates,links';

	CKEDITOR.editorConfig = function( config) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//'pastetext,colorbutton',
	config.contentsLanguage = 'pt-br';
	config.autoGrow_minHeight = 500;
	config.filebrowserUploadUrl = urlContext+'/ws/uploadFile.htm';
	//config.fillEmptyBlocks = false;
	//config.basicEntities = false;
	config.disableObjectResizing = true;
	config.forcePasteAsPlainText = true;
	config.clipboard_defaultContentType = 'text';
	config.toolbarLocation = 'bottom';
	config.extraPlugins = 'bbcode,pastetext,colorbutton,filebrowser'; // -> remover da lista abaixo
	config.removePlugins = stringConfig;
	config.resize_enabled = false;
	config.tabIndex = 0;
	config.fontSize_sizes = '30/30%;50/50%;100/100%;120/120%;150/150%;200/200%;300/300%';
	config.toolbar = [
	   ['Bold', 'Italic', 'Underline'],['Subscript','Superscript'],['TextColor'],['RemoveFormat'],['Undo', 'Redo'], ['Maximize']
	];
	
	smiley_images = [
		'regular_smile.png', 'sad_smile.png', 'wink_smile.png', 'teeth_smile.png', 'tongue_smile.png',
		'embarrassed_smile.png', 'omg_smile.png', 'whatchutalkingabout_smile.png', 'angel_smile.png',
		'shades_smile.png', 'cry_smile.png', 'kiss.png'
	];
	smiley_descriptions = [
		'smiley', 'sad', 'wink', 'laugh', 'cheeky', 'blush', 'surprise',
		'indecision', 'angel', 'cool', 'crying', 'kiss'
		];
};