var sideBoxLinks = (function() {
	
	var ajaxTime;
	
	return function(obj) {
		jsHttp.get(urlContext + "/rest/v1/links", {
			beforeSend: function() {
				if(obj.isScrollSidebox){
					ajaxTime = new Date().getTime()
				}
				$(".listaLinks").empty();
				$(".scroll-list .spinner").show();
			},
			data: obj.data,
			success: function(data) {
				$(".scroll-list .spinner").hide();
				if(data.status == "ERROR") {
					return;
				}
				var conteudo = "";
				if(data) {
					for(var i in data) {
						var l = data[i];
						conteudo += '<li class="" data-postId="'+l.postId+'">'
						
							conteudo += '<div class="clearfix">'
								conteudo += '<div class="fotoUsuario media bordered no-radius">'
									conteudo += '<div class="centraliza">'
										conteudo += '<div href="" rel="galeriaArquivos" >'
											if(l.url){
												conteudo += '<img src="'+l.url+'" />'
											}else{
												conteudo += '<img src="'+urlContext+'/img/link.png" />'
											}
										conteudo += '</div>'
									conteudo += '</div>'
								conteudo += '</div>'
									
								conteudo += '<div class="text-link-holder">';
									conteudo += '<p class="first font-black">'+l.titulo+'</p>';
									conteudo += '<a href="'+l.link+'" target="_blank" class="font-blue link-trigger">'+l.link+'</a>';
									conteudo += '<p class="second">enviado por: '+l.usuario+'</p>';
								conteudo += '</div>';
								
							conteudo += '</div>'
								
							conteudo += '<div class="clearfix acoes-user">';
								conteudo += '<a class="sbold font-blue" href="'+urlContext + "/pages/post.htm?id=" + l.postId+'">ver publicação</a>';
								conteudo += '<a class="font-blue-sharp sbold open-link" href="'+l.link+'" target="_blank">abrir</a>';
								conteudo += '<a class="font-green-sharp sbold copy-to-clipboard" data-copy="'+l.link+'">copiar</a>';
							conteudo += '</div>';
							
						conteudo += '</li>'
					}
				}
				$(".listaLinks").append(conteudo);
				if($(".listaLinks li").length == 0) {
					var conteudo = '<li class="sbold text-center">Nenhum resultado encontrado';
						if(obj.data && obj.data.texto && obj.data.site) {
							conteudo += " para [" + obj.data.texto + " Site: "+obj.data.site+"]";
						} else if(obj.data && obj.data.site) {
							conteudo += " para o Site [" + obj.data.site +"]";
						} else if(obj.data && obj.data.texto) {
							conteudo += " para [" + obj.data.texto + "]";
						}
					conteudo += '</li>';
					$(".listaLinks").append(conteudo);
				}
				highlightLink();
				if(obj.isScrollSidebox){
					var totalTime = new Date().getTime() - ajaxTime;
					timerScrollPosition(totalTime);
				}
			}
		});
	}
}());