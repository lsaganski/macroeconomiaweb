var ajaxSideBox = null;
var ajaxTime;
var arquivos = (function() {
	var sideBox = function(obj) {
		if(!obj) {
			obj = {};
		}
		
		if(!obj.files) {
			obj.files = {};
		}
		if(!obj.files.ext) {
			obj.files.ext = {};
		}
		
		if(!obj.files.ext.show) {
			if(typeof sideBox_arquivo_ext_show !== 'undefined') {
				var split = sideBox_arquivo_ext_show.split(",");
				obj.files.ext.show = {};
				for(var i in split) {
					obj.files.ext.show[split[i].trim()] = true;
				}
			} else {
				obj.files.ext.show = { 
						png : true, jpg: true, gif: true, jpeg: true, bmp: true,
						docx: true, doc: true, xls: true, xlsx: true, xlt: true,
						csv: true, ppt: true, pptx: true, pdf: true, webp: true,
						mp4: true, mov: true, zip: true
				}
			}
		}
		
		if(typeof sideBox_arquivo_ext_not_show !== 'undefined') {
			var split = sideBox_arquivo_ext_not_show.split(",");
			for(var i in split) {
				obj.files.ext.show[split[i].trim()] = false;
			}
			
		}
		
		if(!obj.files.ext.openGoogleDocs) {
			obj.files.ext.openGoogleDocs = {
					docx: true, doc: true, xls: true, xlsx: true, xlt: true, 
					csv: true, ppt: true, pptx: true, zip: true
					
			}
		}
		
		if(!obj.files.ext.openFancybox) {
			obj.files.ext.openFancybox = {
				jpg: true, jpeg: true, gif: true, png: true, bmp: true, ico: true, webp: true, svg: true
			}
		}
		
		if(!obj.files.ext.openJWPlayer) {
			obj.files.ext.openJWPlayer = {
				mp4: true, mpeg: true, mov: true, avi: true, wmv: true
			}
		}
		
		var showAllArquivos = function(data) {
			var $otherTabsActive = $("#tab_cadastros, #sideBox-notificacoes, #tab_cadastros, #tab_links");
			if($otherTabsActive.hasClass("active")) {
				$otherTabsActive.removeClass("active");
			}
			var $tabArquivos = $("#tab_arquivos");
			$tabArquivos.addClass("active");
			$tabArquivos.addClass("in");
			
			var conteudo = createLiArquivos(data);
			if (obj.isClear || typeof obj.isScrollSidebox === "undefined") {
				$("ul.lista-arquivos").empty();
			}
			$(".lista-arquivos").append(conteudo);
			highlightArquivo();
			$("#buscarArquivo").removeClass("ac_loading");
		}
		var createLiArquivos = function(arquivos) {
			var defaultExtensions = {	"psd":"psd", "jpg":"img", "jpeg":"img", "png":"img",
										"gif":"img", "doc":"doc", "pdf":"pdf", "zip":"zip",
										"rar":"zip", "docx":"doc", "xls":"xls", "odt":"doc",
										"xlsx":"xls", "tar":"zip", "gz":"zip", "txt":"txt",
										"rtf":"rtf", "csv":"csv", "ico":"img", "ppt":"ppt",
										"pptx":"ppt", "ppsx":"ppt", "mp4":"mp4", "mp3":"mp3", "mov":"mp4",};
			
			var imgext;
			if(!arquivos || arquivos.length == 0) {
				return '<li class="sbold text-center">Nenhum arquivo encontrado</li>';
			}
			var conteudo = "";
			for(var i in arquivos) {
				var arquivo = arquivos[i];
				var ext = arquivo.extensao.toLowerCase();
				var name = arquivo.file;
				var url = arquivo.url;
				var classe = arquivo.file.length > 30 ? "overflow" : "";

				if(!obj.files.ext.show[ext]) {
					continue;
				}
				conteudo += '<li data-id="'+arquivo.id+'" class="arquivo">';
				
					conteudo += '<div class="clearfix">'
						
						conteudo += '<div class="fotoUsuario media bordered no-radius">'
							conteudo += '<div class="centraliza">'
								if(obj.files.ext.openFancybox[ext]) {
									conteudo += '<a href="'+ url + '" class="fancybox" rel="galeriaArquivos"><img src="' + arquivo.urlThumb + '"/></a>';
								} else {
									var link = "";
									if(obj.files.ext.openGoogleDocs[ext]) {
										link = 'https://docs.google.com/gview?url='+url+'&embedded=true';
									} else {
										link = arquivo.url;
									}
									conteudo += '<span data-link="'+link+'" class="previewFile">';
										if(!(imgext = defaultExtensions[ext])){
											imgext = "custom_file";
										}
										conteudo += '<span class="extensaoArquivo">' + ext + '</span>';
										conteudo += '<img class="file-extensions-icons-medium '+ext+'" alt="'+ext+'" />';
									conteudo += '</span>';
								}
							conteudo += '</div>';
						conteudo += '</div>'
						
						conteudo += '<div class="text-link-holder">';
							conteudo += '<p class="first font-black">'+name+'</p>';
							conteudo += '<p class="second">enviado por: '+arquivo.usuario.nome+'</p>';
							conteudo += '<p class="second">'+arquivo.data+'</a>';
						conteudo += '</div>';
						
					conteudo += '</div>';
						
					conteudo += '<div class="clearfix acoes-user">';
						conteudo += '<a class="font-blue sbold file-open-post" href="'+urlContext + '/pages/mural.htm?arquivo='+ arquivo.id+'">ver publicação</a>';
						conteudo += '<a class="font-blue-sharp sbold download-file" data-file-name="'+name+'" data-file-url="'+url+'">baixar</a>';
						if(obj.files.ext.openFancybox[ext]) {
							conteudo += '<a href="'+url+'" class="fancybox font-green-sharp sbold" rel="galeriaArquivos">visualizar</a>';
						} else if(obj.files.ext.openJWPlayer[ext]) {
							conteudo += '<a class="font-green-sharp sbold previewFile fancyJwplayer" data-ext="'+ext+'" data-link="'+url+'" data-url="'+url+'" data-name="'+name+'">visualizar</a>';
						} else {
							conteudo += '<a class="font-green-sharp sbold previewFile" data-ext="'+ext+'" data-link="'+url+'" data-url="'+url+'" data-name="'+name+'" data-toggle="modal" data-target="#modalPreviewFile">visualizar</a>';
						}
					conteudo += '</div>';
					
				conteudo += '</li>';
			}
			return conteudo;
		}

		service({
			data: obj.data,
			beforeSend: function() {
				if(obj.isScrollSidebox){
					ajaxTime = new Date().getTime();
				}
				$(".scroll-list .spinner").show();
			},
			success: function(data) {
				ajaxSideBox = null;
				$(".scroll-list .spinner").hide();
				showAllArquivos(data);
				if(obj.isScrollSidebox){
					var totalTime = new Date().getTime() - ajaxTime;
					timerScrollPosition(totalTime);
				}
			}
		});
		if(obj.isScroll) {
			var pageScroll = 0;
			$("#sideBox-arquivos .scroll-list").unbind("scroll");
			$("#sideBox-arquivos .scroll-list").on('scroll', function(){
				if(ajaxSideBox != null) {
					return;
				}
				
				var x = $(".lista-arquivos").height() - $(this).height() - 2;
				var isExecute = $(this).scrollTop() >= x;
				if(isExecute) {
					pageScroll += 1;
					obj.isClear = false;
					obj.data.page = pageScroll;
					service({
						data: obj.data,
						beforeSend: function() {
							$(".scroll-list .spinner").show();
						},
						success: function(data) {
							ajaxSideBox = null;
							$(".scroll-list .spinner").hide();
							if(data && data.length > 0) {
								showAllArquivos(data);
							} else {
								$("#tab_arquivos .scroll-list").unbind("scroll");
							}
						}
					});
				}
			});
		}
	}
	
	function service(ajax) {
		if(!ajax.beforeSend) {
			ajax.beforeSend = function(){};
		}
		if(ajaxSideBox != null) {
			ajaxSideBox.abort();
		}
		
		ajaxSideBox = jsHttp.get(urlContext + "/rest/v1/arquivo", {
			dataType: "json",
			data: ajax.data,
			beforeSend: ajax.beforeSend,
			success: ajax.success
		});
	}
	
	return {
		sideBox: sideBox,
		service: service,
	}
	
}());

function loadBibliotecaArquivos(page){
	var maximoPagina = 40;

	jQuery.ajax({
		type: "POST",
		url: urlContext+"/ws/arquivos.htm?mode=json&form_name=form",
	 	dataType: "json",
	 	data: {
	 		user_id: userId,
	 		modoBusca: "meusArquivos",
            maxRows: 40,
	 		page: page,
	 		wsVersion: wsVersion,
	 		wstoken:SESSION.USER_INFO.USER_WSTOKEN
        },
	 	success: function( data ) {
	 		if(data.list && data.list.length > 0){
	 			$("#modalInsertMediaMural .seletorArquivos .paginaAtual").show();
		 		var conteudo = "";
		 		for (var i=0;i<data.list.length;i++){
		 			var arquivo = data.list[i],
		 				descricao = "",
		 				extensao = arquivo.extensao.toLowerCase(),
		 				isImage = FILE_EXTENSIONS.image[extensao] ? true : false,
						isDoc = FILE_EXTENSIONS.docs[extensao] || FILE_EXTENSIONS.programs[arquivo.extensao] ? true : false;

		 			if(arquivo.descricao){
		 				descricao = arquivo.descricao;
		 			}

		 			var thumbs = '';
		 			if(arquivo.thumbs){
			 			for(t in arquivo.thumbs){
			 				thumbs += 'data-thumb-id' + t + '="'+arquivo.thumbs[t].id+'"';
			 				thumbs += 'data-thumb' + t + '="'+arquivo.thumbs[t].url+'" ';
			 				thumbs += 'data-thumb-width' + t + '="'+arquivo.thumbs[t].width+'" ';
			 				thumbs += 'data-thumb-height' + t + '="'+arquivo.thumbs[t].height+'" ';
			 			}
		 			}else{
		 				thumbs += 'data-thumb=""';
		 			}

		 			conteudo += '<li class="clearfix" data-arquivo-id="'+arquivo.id+'" data-descricao="'+descricao+'" data-extensao="'+extensao+'" data-dimensao="'+arquivo.dimensao+'"';
		 			conteudo += ' data-height="'+arquivo.height+'" data-width="'+arquivo.width+'" data-length="'+arquivo.length+'" data-human="'+arquivo.lengthHumanReadable.toLowerCase()+'" data-nome="'+arquivo.file+'"';
		 			conteudo += ' data-url="'+arquivo.url+'" data-is-image="'+isImage+'" data-is-doc="'+isDoc+'" data-ordem="'+arquivo.ordem+'" '+thumbs+'>';
		 				conteudo += '<div class="icon-destaque" data-arquivo-id="'+arquivo.id+'"><i class="fa fa-star fa-2x" aria-hidden="true" title="Tornar destaque"></i></div>';
		 				conteudo += '<div class="icon-wrapper"><i class="galeria-add-file fa fa-check-circle" title="Adicionar"></i></div>';

		 				conteudo += '<div data-arquivo-id="'+arquivo.id+'" class="arquivo mt-overlay-1 custom-overlay">';
		 					conteudo += '<div class="miniaturaArquivo">';


				 					if(FILE_EXTENSIONS.image[extensao]){conteudo += '<a>';
		 								if(FILE_EXTENSIONS.gif[extensao]){
		 									conteudo += '<img class="img-responsive" src="'+arquivo.url+'" alt="'+arquivo.file+'" />';
		 								}else{
		 									conteudo += '<img class="img-responsive" src="'+arquivo.urlThumb+'" alt="'+arquivo.file+'" />';
		 								}
	 								conteudo += '</a>';
								}else{
									conteudo += '<i class="file-extensions-icons-big '+extensao+'"></i>';
									conteudo += '<span class="extensaoArquivo">' + extensao + '</span>';
								}
		 					conteudo += '</div>';
		 					conteudo += '<div class="clearfix title-wrapper">';
								conteudo += '<h4 class="custom-title">'+arquivo.file+'</h4>';
							conteudo += '</div>';
		 				conteudo += '</div>';
		 			conteudo += '</li>';
		 		}
		 	}else if($(".listaHorizontal li").length <= 0){
	 			$("#insertMedia-biblioteca .muralVazio").html("Nenhum resultado encontrado.").show();
	 			$("#insertMedia-biblioteca .paginaAtual").hide();
	 		}else{
	 			$("#insertMedia-biblioteca .paginaAtual").hide();
	 		}
	 		if(data.list && data.list.length < maximoPagina){
	 			$("#insertMedia-biblioteca .paginaAtual").hide();
	 		}
	 		$("#insertMedia-biblioteca .paginaAtual").attr("data-page", parseInt($(".seletorArquivos .paginaAtual").attr("data-page"))+1);

	 		if(page === 0){
	 			$("#insertMedia-biblioteca .galeria-files > ul").empty();
	 		}

	 		$("#insertMedia-biblioteca .galeria-files > ul").append(conteudo);
	 		$("#modalInsertMediaMural .paginaAtual").removeClass("loading");

	 		if(data.mensagem){
	 			$("#insertMedia-biblioteca .paginaAtual").hide();
	 			jAlert(data.mensagem.mensagem, data.mensagem.status, null);
	 		}
		}
	});
}

var ajaxInfoArquivoGaleria = null, ajaxArquivoDescricao = null;

function updateInfoArquivoGaleria(arquivo){
	if(arquivo.descricao > PARAMS.QTD_DESCRICAO_ARQUIVO){
		$('.description-msg-text').addClass('font-yellow-gold').html('A descrição é maior que o permitido! <i class="fa fa-exclamation-triangle"></i>').fadeIn();
	}else{
		if(ajaxInfoArquivoGaleria != null){
			ajaxInfoArquivoGaleria.abort();
		}

		var fixNome = '';
		if(arquivo.nome.substring(arquivo.nome.lastIndexOf('.') + 1, arquivo.nome.length) == arquivo.ext){
			fixNome = arquivo.nome
		}else{
			fixNome = arquivo.nome + '.' + arquivo.ext;
		}

		ajaxInfoArquivoGaleria = jQuery.ajax({
			type: "POST",
			url: urlContext+"/ws/updateFile.htm?mode=json&form_name=form",
		 	dataType: "json",
		 	data:{
		 		id: arquivo.id,
		 		user_id: userId,
		 		descricao: arquivo.descricao,
		 		nome: fixNome,
		 		wsVersion: wsVersion,
		 		wstoken:SESSION.USER_INFO.USER_WSTOKEN
		 	},
		 	beforeSend: function(){
		 		$('.arquivo-descricao[data-arquivo-id="'+arquivo.id+'"]').addClass('ui-autocomplete-loading');
		 	},
		 	success: function(resp){
		 		$('.arquivo-descricao[data-arquivo-id="'+arquivo.id+'"]').removeClass('ui-autocomplete-loading');
		 		ajaxInfoArquivoGaleria = null;
		 		if(resp.mensagem && resp.mensagem.status == "OK"){
		 			$('.description-msg-text').addClass('font-green-sharp').html(resp.mensagem.mensagem + ' <i class="fa fa-check"></i>').fadeIn();
		 			setTimeout(function(){
		 				$('.description-msg-text').fadeOut(function(){
		 					$(this).removeClass('font-green-sharp').html('');
		 				});
		 			}, 2000);

		 			$('.ajax-file-upload-statusbar[data-id="'+arquivo.id+'"], .galeria-files ul > li[data-arquivo-id="'+arquivo.id+'"], .recent-files ul > li[data-arquivo-id="'+arquivo.id+'"]')
		 				.data('descricao', arquivo.descricao).attr('data-descricao', arquivo.descricao)
		 				.data('nome', fixNome).attr('data-nome', fixNome);

		 			var tamanho = $('.anexos > li[data-id="'+arquivo.id+'"] .nomeAnexo a').not('.adicionarDescricao').find('span').html();

		 			$('.arquivo[data-arquivo-id="'+arquivo.id+'"] .custom-title, #output .alterarNome[data-arquivo-id="'+arquivo.id+'"] .ajax-file-upload-filename > a').html(fixNome);
		 			$('.uploadPost .ajax-file-upload-statusbar.alterarNome[data-id="'+arquivo.id+'"] .ajax-file-upload-filename > a').html(fixNome);
		 			$('.anexos > li[data-id="'+arquivo.id+'"] .nomeAnexo a').not('.adicionarDescricao').html(fixNome + '<span> '+ tamanho+'</span>');
		 			
		 			if($('.ajax-file-upload-statusbar[data-id="'+arquivo.id+'"] .ajax-file-upload-filename .descricaoUpload').length > 0){
		 				$('.ajax-file-upload-statusbar[data-id="'+arquivo.id+'"] .ajax-file-upload-filename .descricaoUpload').html(arquivo.descricao);
		 			}else{
		 				$('.ajax-file-upload-statusbar[data-id="'+arquivo.id+'"] .ajax-file-upload-filename .open-gallery').after('<span class="descricaoUpload">'+arquivo.descricao+'</span>');
		 			}

		 			if($('.anexos > li[data-id="'+arquivo.id+'"] .nomeAnexo .descricaoUpload').length > 0){
		 				$('.anexos > li[data-id="'+arquivo.id+'"] .nomeAnexo .descricaoUpload').html(arquivo.descricao);
		 			}else{
		 				$('.anexos > li[data-id="'+arquivo.id+'"] .nomeAnexo .open-gallery').after('<span class="descricaoUpload">'+arquivo.descricao+'</span>');
		 			}

		 		}else{
		 			$('.description-msg-text').addClass('font-red-mint').html(resp.mensagem.mensagem + ' <i class="fa fa-exclamation-triangle"></i>').fadeIn();
		 			setTimeout(function(){
		 				$('.description-msg-text').fadeOut(function(){
		 					$(this).removeClass('font-red-mint').html('');
		 				});
		 			}, 2000);
		 		}
		 	}
		});
	}
}

function editThumbs(){
	$('.desabilitar-rotate').click();
	closeResize();
	closeEdit($('#habilitar-resize'));
	restaurarImg();
	setTimeout(function(){if(jcrop_api != null){
		jcrop_api.destroy();
	}

	var width = parseInt($('.insert-arquivo-thumbs .thumbnails-wrapper .img-wrapper:first-child').data('thumb-width')),
		height = parseInt($('.insert-arquivo-thumbs .thumbnails-wrapper .img-wrapper:first-child').data('thumb-height')),
		aspectRatio = PARAMS.GALERIA_EDITAR_THUMBS_FIXA_ON == '1' ? 1 : null;
		/*maxThumb = PARAMS.GALERIA_EDITAR_THUMBS_TAMANHO_FIXO != '' ? parseInt(PARAMS.GALERIA_EDITAR_THUMBS_TAMANHO_FIXO) : 0;*/
		
	$('#insertMedia-editFile .insert-arquivo-preview-images .html-crop > img.img-responsive').Jcrop({
		onChange: showPreviewThumb,
		onSelect: showPreviewThumb,
		bgColor: 'black',
		bgOpacity: .6,
		aspectRatio: aspectRatio,
		allowSelect: false,
		allowResize: true,
		minSize: [ 80, 80 ]
		/*maxSize: [ maxThumb, maxThumb ]*/
	}, function(){
		jcrop_api = this;
		openEdit($('#habilitar-thumb'));

		var previewThumbs = '', src = $('.original-crop-image').attr('src');

		previewThumbs += '<div class="crop-thumbs-thumbnail-container clearfix">';
			previewThumbs += '<div class="img-wrapper">';
				previewThumbs += '<img alt="Preview da thumbnail" src="'+src+'" class="crop-thumb-thumbnail" />';
			previewThumbs += '</div>';
			previewThumbs += '<div class="img-wrapper">';
				previewThumbs += '<img alt="Preview da thumbnail" src="'+src+'" class="crop-thumb-thumbnail" />';
			previewThumbs += '</div>';
		previewThumbs += '</div>';

		$('.insert-arquivo-thumbs .thumbnails-wrapper').addClass('invisible');
		$('.insert-arquivo-thumbs').find('.crop-thumbs-thumbnail-container').remove();
		$('.insert-arquivo-thumbs').append(previewThumbs);
	});

	setTimeout(function(){
		jcrop_api.animateTo([
			($('.insert-arquivo-thumbs .thumbnails-wrapper .img-wrapper:first-child').data('thumb-width') / 2) - 70,
        	($('.insert-arquivo-thumbs .thumbnails-wrapper .img-wrapper:first-child').data('thumb-height') / 2) - 70,
       		($('.insert-arquivo-thumbs .thumbnails-wrapper .img-wrapper:first-child').data('thumb-width') / 2) + 70,
			($('.insert-arquivo-thumbs .thumbnails-wrapper .img-wrapper:first-child').data('thumb-height') / 2) + 70
		]);
	}, 120);
	}, 99);
}

function showPreview(coords){
	var rx = 300 / coords.w;
	var ry = 300 / coords.h;

	$('.crop-thumbnail').css({
		width: Math.round(rx * $('#insertMedia-editFile .insert-arquivo-preview-images .html-crop > img.img-responsive').width()) + 'px',
		height: Math.round(ry * $('#insertMedia-editFile .insert-arquivo-preview-images .html-crop > img.img-responsive').height()) + 'px',
		marginLeft: '-' + Math.round(rx * coords.x) + 'px',
		marginTop: '-' + Math.round(ry * coords.y) + 'px'
	});
}

function showPreviewThumb(coords){
	var cropImg = $('.insert-arquivo-preview .insert-arquivo-preview-images .crop-image'),
		thumbA = $('.insert-arquivo-thumbs .crop-thumbs-thumbnail-container .img-wrapper:first-child'),
		thumbB = $('.insert-arquivo-thumbs .crop-thumbs-thumbnail-container .img-wrapper:last-child'),
		previewWidthA = Math.abs(cropImg.width() * thumbA.width() / coords.w),
		previewWidthB = Math.abs(cropImg.width() * thumbB.width() / coords.w);
	
	var h = parseInt($('.insert-arquivo-thumbs .thumbnails-wrapper .img-wrapper:first-child').data('thumb-height')),
		w = parseInt($('.insert-arquivo-thumbs .thumbnails-wrapper .img-wrapper:first-child').data('thumb-width')),
		cHeight = $('.jcrop-tracker').height(),
		cWidth = $('.jcrop-tracker').width(),
		razaoCropHeight = Math.abs(cHeight/cWidth),
		razaoCropWidth = Math.abs(cWidth/cHeight);
		
		if(cHeight == cWidth){
			thumbA.height(thumbA.width());
			thumbB.height(thumbB.width());
		}else if(cWidth < cHeight){
			thumbA.height('160');
			thumbB.height('80');
			thumbA.width(Math.floor(thumbA.height()*razaoCropWidth));
			thumbB.width(Math.floor(thumbB.height()*razaoCropWidth));
		}else if(cWidth > cHeight){
			thumbA.height(Math.floor(thumbA.width()*razaoCropHeight));
			thumbB.height(Math.floor(thumbB.width()*razaoCropHeight));
		}

	thumbA.find('img').css({
		width: previewWidthA,
		height: 'auto',
		marginLeft: '-' + Math.round(previewWidthA * coords.x / cropImg.width()) + 'px',
		marginTop: '-' + Math.round(previewWidthA * coords.y / cropImg.width()) + 'px'
	});
	thumbB.find('img').css({
		width: previewWidthB,
		height: 'auto',
		marginLeft: '-' + Math.round(previewWidthB * coords.x / cropImg.width()) + 'px',
		marginTop: '-' + Math.round(previewWidthB * coords.y / cropImg.width()) + 'px'
	});
}

function closeCrop() {
	if (jcrop_api != null) {
		jcrop_api.animateTo([300, 200, 300, 200], function() {
			jcrop_api.release();
		});				
		setTimeout(function(){
			jcrop_api.destroy();
			$('#insertMedia-editFile .insert-arquivo-preview-images').find('.crop-thumbnail-container').remove();
			$('.crop-image.resize-image').removeAttr('style');			
			$('#insertMedia-editFile .insert-arquivo-preview-images .html-crop > img.img-responsive').css({
				'opacity': '1',
				'visibility': 'visible',
			});
		}, 300);
	}
	$('#insertMedia-editFile .preview-crop-wrapper').removeClass('active').find('.clearfix').empty();
	$('#insertMedia-editFile .crop-thumbs-thumbnail-container').remove();
	$('#insertMedia-editFile .thumbnails-wrapper').removeClass('invisible');
	_modificouArquivos = false;
}

function updateCroppedImage(coords, arquivo, isThumb, refazerThumbs){
	var formData = new FormData(),
		_originalImage = $('.original-crop-image'),
		_cropImage = $('.insert-arquivo-preview .insert-arquivo-preview-images .crop-image'),
		razaoCrop = Math.abs( _originalImage.width() / _cropImage.width() );
	formData.append('fileUrl', arquivo.url);
	formData.append('dir', 'arquivos');
	formData.append('tipo', '1');
	formData.append('user_id', userId);
	formData.append('mode', 'json');
	formData.append('form_name', 'form');
	formData.append('dimensao', 'original');
	formData.append('nonce_token', SESSION.USER_INFO.USER_ID+Date.now());
	formData.append('wstoken', SESSION.USER_INFO.USER_WSTOKEN);

	if(isThumb){
		if(refazerThumbs){
			formData.append('width', Math.ceil(_originalImage.width()));
			formData.append('height', Math.ceil(_originalImage.height()));
			formData.append('eixoX', 0);
			formData.append('eixoY', 0);
		}else{
			formData.append('width',  Math.ceil(coords.w * razaoCrop));
			formData.append('height',  Math.ceil(coords.h * razaoCrop));
			formData.append('eixoX', Math.ceil(coords.x * razaoCrop));
			formData.append('eixoY', Math.ceil(coords.y * razaoCrop));
		}
		formData.append('thumb_ids', arquivo.thumb_ids);
	}else{
		formData.append('descricao', arquivo.descricao);
		formData.append('width', Math.ceil(coords.w * razaoCrop));
		formData.append('height', Math.ceil(coords.h * razaoCrop));
		formData.append('eixoX', Math.ceil(coords.x * razaoCrop));
		formData.append('eixoY', Math.ceil(coords.y * razaoCrop));
		formData.append('ordem', arquivo.ordem);
	}

	if(coords && coords != ''){
		jQuery.ajax({
			type: 'POST',
			url: urlContext + '/ws/uploadFile.htm',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function(){
				if(refazerThumbs){
					$('.refazer-thumbs').html('refazer thumbs <i class="fa fa-spinner fa-fw fa-spin"></i>').addClass('disabled');
				}else{
					if(isThumb){
						$('.salvar-thumb').addClass('disabled').html('<i class="fa fa-spinner fa-spin fa-fw"></i> Salvando...')
					}else{
						$('.insert-arquivo-preview-images .salvar-crop').addClass('disabled').html('<i class="fa fa-spinner fa-spin fa-fw"></i> Salvando...')
					}
				}					
				$('#modalInsertMediaMural .modal-footer .btn-primary').addClass('disabled');
			},
			success: function(resp){
				var _arquivoAntigo = arquivo;

				if(resp.file){
					if(!isThumb){
						var anexos = $('#anexosId').val();
						anexos = anexos.replace(_arquivoAntigo.id+',', resp.file.id+',');
						$('#anexosId').val(anexos);
						
						var galeria = createLiGaleria(resp.file);
						$('#insertMedia-biblioteca .galeria-files .arquivos').prepend(galeria);
						$('#insertMedia-biblioteca .galeria-files .arquivos li[data-arquivo-id="'+_arquivoAntigo.id+'"]').removeClass('active');
						$('#insertMedia-biblioteca .galeria-files .arquivos li[data-arquivo-id="'+resp.file.id+'"]').addClass('active');

						$('.anexos li[data-id="'+_arquivoAntigo.id+'"] .thumbnail .fotoUsuario .open-gallery img').attr('src', resp.file.url);
						$('.anexos li[data-id="'+_arquivoAntigo.id+'"] .open-gallery').data('arquivo-id', resp.file.id).attr('data-arquivo-id', resp.file.id);
						$('.anexos li[data-id="'+_arquivoAntigo.id+'"] .nomeAnexo.alterarNome > .open-gallery > span').html('('+resp.file.lengthHumanReadable.toLowerCase()+')');
						$('.anexos li[data-id="'+_arquivoAntigo.id+'"] .nomeAnexo.alterarNome').data('arquivo-id', resp.file.id).attr('data-arquivo-id', resp.file.id);
						$('.anexos li[data-id="'+_arquivoAntigo.id+'"] a.ico-remover').attr('href', urlContext + '/ws/deletar.htm?mode=json&form_name=form&id='+resp.file.id+'&tipo=arquivo');
						$('.anexos li[data-id="'+_arquivoAntigo.id+'"]').data('id', resp.file.id).attr('data-id', resp.file.id);

						$('.ajax-file-upload-statusbar.alterarNome[data-id="'+_arquivoAntigo.id+'"] .thumbnail .fotoUsuario .open-gallery img').attr('src', resp.file.url);
						$('.ajax-file-upload-statusbar.alterarNome[data-id="'+_arquivoAntigo.id+'"] .open-gallery').data('arquivo-id', resp.file.id).attr('data-arquivo-id', resp.file.id);
						$('.ajax-file-upload-statusbar.alterarNome[data-id="'+_arquivoAntigo.id+'"] .removeArquivoAnexo').data('id', resp.file.id).attr('data-id', resp.file.id);
						$('.ajax-file-upload-statusbar.alterarNome[data-id="'+_arquivoAntigo.id+'"]').data('id', resp.file.id).attr('data-id', resp.file.id);

						$('#insertMedia-arquivoNome-upload, #insertMedia-arquivoDescricao-upload, .recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo.id+'"] .arquivo').data('arquivo-id', resp.file.id);
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo.id+'"]')
							.data('id', resp.file.id)
							.data('url', resp.file.url)
							.data('arquivo-id', resp.file.id)
							.data('human', resp.file.lengthHumanReadable)
							.data('length', resp.file.length)
							.data('width', resp.file.width)
							.data('height', resp.file.height);

						$('body').find('.original-crop-image').attr('src', resp.file.url);
					}

					$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo.id+'"]')
						.data('thumb-id0', resp.file.thumbs[0].id)
						.data('thumb0', resp.file.thumbs[0].url)
						.data('thumb-width0', resp.file.thumbs[0].width)
						.data('thumb-height0', resp.file.thumbs[0].height);

					if(resp.file.thumbs[1]){
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo.id+'"]')
							.data('thumb-id1', resp.file.thumbs[1].id)
							.data('thumb1', resp.file.thumbs[1].url)
							.data('thumb-width1', resp.file.thumbs[1].width)
							.data('thumb-height1', resp.file.thumbs[1].height);
					}else{
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo.id+'"]')
							.data('thumb-id1', '')
							.data('thumb1', '')
							.data('thumb-width1', '')
							.data('thumb-height1', '');
					}
					if(resp.file.thumbs[2]){
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo.id+'"]')
							.data('thumb-id2', resp.file.thumbs[2].id)
							.data('thumb2', resp.file.thumbs[2].url)
							.data('thumb-width2', resp.file.thumbs[2].width)
							.data('thumb-height2', resp.file.thumbs[2].height);
					}else{
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo.id+'"]')
							.data('thumb-id2', '')
							.data('thumb2', '')
							.data('thumb-width2', '')
							.data('thumb-height2', '');
					}
					$('.insert-arquivo-preview .insert-arquivo-preview-images .html-crop > img.img-responsive').addClass('cropped').removeAttr('style');
				}else{
					$('.insert-arquivo-preview-images .form-group').append('<p class="font-red-mint pull-right"><i class="fa fa-exclamation-triangle"></i> Algo de errado aconteceu.</p>');
				}

				if(refazerThumbs){
					$('.refazer-thumbs').html('refazer thumbs').removeClass('disabled');
				}else{
					$('.insert-arquivo-preview-images .salvar-crop').removeClass('disabled').html('<i class="fa fa-check"></i> Salvar recorte');
				}

				$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo.id+'"] .miniaturaArquivo a > img').attr('src', resp.file.url);
				$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo.id+'"], .recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo.id+'"] .arquivo').attr('data-arquivo-id', resp.file.id);
				$('#modalInsertMediaMural .modal-footer .btn-primary').removeClass('disabled');
				$('.recent-files .arquivos > li[data-arquivo-id="'+resp.file.id+'"]').click();

				setTimeout(function(){
					if(isThumb){
						$('.insert-arquivo-preview-images .form-group').append('<p class="font-green-sharp pull-right"><i class="fa fa-check"></i> Thumbnail salvo com sucesso.</p>');
					}else{
						$('.insert-arquivo-preview-images .form-group').append('<p class="font-green-sharp pull-right"><i class="fa fa-check"></i> Recorte salvo com sucesso.</p>');
					}
				}, 100);

				setTimeout(function(){
					$('.insert-arquivo-preview-images .form-group').find('p').fadeOut(function(){
						$(this).remove();
					});
				}, 2000);
			}
		});
	}
}

function createLiAnexoPost(anexo){
	var htmlAnexos = '', extensao = anexo.extensao.toLowerCase(), descricao = anexo.descricao ? anexo.descricao : '';
	
	htmlAnexos += '<div class="ajax-file-upload-statusbar alterarNome" data-id="'+anexo.id+'">';
		htmlAnexos += '<img class="ajax-file-upload-preview" style="width: 100%; height: auto; display: none;">';
		htmlAnexos += '<div class="ajax-file-upload-filename">';
			if(FILE_EXTENSIONS.image[extensao]){
				htmlAnexos += '<div class="thumbnail preview">';
					htmlAnexos += '<div class="fotoUsuario media no-radius">';
						if(PARAMS.MURAL_GALERIA_ON == '1'){
							htmlAnexos += '<a class="open-gallery" data-arquivo-id="'+anexo.id+'">';
						}else{
							htmlAnexos += '<a href="'+anexo.url+'" class="fancybox centraliza" target="_blank">';
						}
							htmlAnexos += '<img alt="'+anexo.nome+'" src="'+anexo.url+'" />';
						htmlAnexos += '</a>';
					htmlAnexos += '</div>';
				htmlAnexos += '</div>';

				if(PARAMS.MURAL_GALERIA_ON == '1'){
					htmlAnexos += '<a class="open-gallery" data-arquivo-id="'+anexo.id+'">';
				}else{
					htmlAnexos += '<a href="'+anexo.url+'" class="fancybox" target="_blank">'
				}

					htmlAnexos += anexo.nome+'</a>';
			}else{
				htmlAnexos += '<div class="thumbnail">';
					htmlAnexos += '<i class="file-extensions-icons-medium '+extensao+'"></i>';
					htmlAnexos += '<span class="extension">'+extensao+'</span>';
				htmlAnexos += '</div>';

				htmlAnexos += '<a class="previewFile " data-url="'+anexo.url+'" data-link="'+anexo.url+'" data-ext="'+extensao+'" data-name="'+anexo.nome+'">'+anexo.nome+'</a>';
			}

			htmlAnexos += '<p class="descricaoUpload">'+descricao+'</p>';

			htmlAnexos += '<div class="ajax-file-upload-red removeArquivoAnexo" data-id="'+anexo.id+'"></div>';

		htmlAnexos += '</div>';
	htmlAnexos += '</div>';

	return htmlAnexos;
}

function createLiGaleria(arquivo){
	var html = '',
		nomestr = arquivo.file.substring(0, arquivo.file.lastIndexOf('.')),
		isImage = FILE_EXTENSIONS.image[arquivo.extensao] ? true : false,
		isDoc = FILE_EXTENSIONS.docs[arquivo.extensao.toLowerCase()] || FILE_EXTENSIONS.programs[arquivo.extensao.toLowerCase()] ? true : false,
		thumbs = '';

	if(arquivo.thumbs){
		for(t in arquivo.thumbs){
			thumbs +='data-thumb-id' + t + '="'+arquivo.thumbs[t].id+'"';
			thumbs += 'data-thumb' + t + '="'+arquivo.thumbs[t].url+'" ';
			thumbs += 'data-thumb-width' + t + '="'+arquivo.thumbs[t].width+'"';
			thumbs += 'data-thumb-height' + t + '="'+arquivo.thumbs[t].height+'" ';
		}
	}else{
		thumbs += 'data-thumb=""';
	}
	
	html += '<li class="clearfix recently-uploaded" data-arquivo-id="'+arquivo.id+'" data-descricao="" data-extensao="'+arquivo.extensao.toLowerCase()+'" data-dimensao="'+arquivo.dimensao+'" ';
		html += 'data-height="'+arquivo.height+'" data-width="'+arquivo.width+'" data-length="'+arquivo.length+'" data-human="'+arquivo.lengthHumanReadable.toLowerCase()+'" data-nome="'+arquivo.file+'" data-nome-str="'+nomestr+'" data-url="'+arquivo.url+'" ';
		html += 'data-is-image="'+isImage+'" data-is-doc="'+isDoc+'" data-ordem="'+arquivo.ordem+'" '+thumbs+'>';
	
		html += '<div class="icon-destaque" data-arquivo-id="'+arquivo.id+'">';
			html += '<i class="fa fa-star fa-2x" aria-hidden="true"></i>';
		html += '</div>';
		html += '<div class="icon-wrapper">';
			html += '<i class="galeria-add-file fa fa-check-circle"></i>';
			html += '<span class="fa-stack remove-from-recent fa-lg"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-times fa-stack-1x fa-inverse"></i></span>';
		html += '</div>';
		
		html += '<div class="arquivo mt-overlay-1 custom-overlay" data-arquivo-id="'+arquivo.id+'">';
			html += '<div class="miniaturaArquivo">';
				if(FILE_EXTENSIONS.image[arquivo.extensao.toLowerCase()]){
					html +='<a >';
						if(FILE_EXTENSIONS.gif[arquivo.extensao.toLowerCase()]){
							html +='<img src="'+arquivo.url+'" alt="'+arquivo.file+'" />'
						}else{
							html += '<img src="'+arquivo.urlThumb+'"alt="'+arquivo.file+'" />'
						}
					html += '</a>';
				}else{
					html += '<i class="file-extensions-icons-big '+arquivo.extensao.toLowerCase()+'"></i>';
					html += '<span class="extension">'+arquivo.extensao.toLowerCase()+'</span>';
				}
		
			html += '</div>';
		
			html += '<div class="clearfix title-wrapper">';
				html += '<h4 class="custom-title">'+arquivo.file+'</h4>';
			html += '</div>';
		
		html += '</div>';
	html += '</li>';
	
	return html;
}

function showAnexoInfo(obj){
	$(obj.wrapper).removeClass('in');
	setTimeout(function(){
		$(obj.wrapper).find('#insertMedia-arquivoNome-biblioteca').val(obj.nome).data('arquivo-id', obj.id);
		$(obj.wrapper).find('.info-arquivo-tamanho').empty().html(obj.tamanho);

		if(parseInt(obj.largura) > 0 && parseInt(obj.altura) > 0){
			$(obj.wrapper).find('.info-arquivo-dimensoes').empty().html(obj.largura + " x " + obj.altura + ' px');
			$(obj.wrapper).find('.dimensoes').addClass('active');
		}else{
			$(obj.wrapper).find('.info-arquivo-dimensoes').empty();
			$(obj.wrapper).find('.dimensoes').removeClass('active');
		}

		$(obj.wrapper).find('#insertMedia-arquivoDescricao-biblioteca').val(obj.descricao).data('arquivo-id', obj.id);

		$('.description-msg-text').fadeOut(function(){
			$(this).html('').removeClass('font-green-sharp font-yellow-gold');
		});

		$(obj.wrapper).find('.open-gallery').remove();
		if((PARAMS.GALERIA_RESIZE_ON == '1' || PARAMS.GALERIA_CROP_ON == '1' || PARAMS.GALERIA_EDITAR_THUMBS_ON == '1') && FILE_EXTENSIONS.image[obj.extensao.toLowerCase()]){
			$(obj.wrapper).find('.form-group.descricao').after('<a class="sbold font-blue open-gallery" data-arquivo-id="'+obj.id+'">editar imagem</a>');
		}

		$(obj.wrapper).find('.miniatura-wrapper').empty().append($(obj.miniatura));
		$(obj.wrapper).addClass('in');
	}, 300);
}

var resizeImg = function(image_target) {
	var $container, // variáveis e configurações
		orig_src = new Image(),
		image_target = $(image_target).get(0),
		event_state = {},
		constrain = false,
		min_width = 5, // altera quando obrigatório
		min_height = 5,
		max_width =  $('.original-crop-image').width(), // altera quando obrigatório
		max_height = $('.original-crop-image').height(),
		_semaforo = 1;
		resize_canvas = document.createElement('canvas');
		orig_src.crossOrigin = "Anonymous";
	init = function(){
		orig_src.src=image_target.src; // enquanto redimensiona, usaremos a cópia da img original
		$(image_target).wrap('<div class="resize-container"></div>') // envolve img com container e adiciona manipulação de redimensionamento
			.before('<span class="resize-handle resize-handle-nw"></span>')
		    .before('<span class="resize-handle resize-handle-ne"></span>')
		    .after('<span class="resize-handle resize-handle-se"></span>')
		    .after('<span class="resize-handle resize-handle-sw"></span>');
		$container =  $(image_target).parent('.resize-container'); // atribui container a variável
		$container.on('mousedown touchstart', '.resize-handle', startResize); // adiciona eventos
	}
	startResize = function(e){
	    e.preventDefault();
	    e.stopPropagation();
	    saveEventState(e);
	    $(document).on('mousemove touchmove', resizing);
	    $(document).on('mouseup touchend', endResize);
	    _semaforo++;
	}
	endResize = function(e){
	    e.preventDefault();
	    $(document).off('mouseup touchend', endResize);
	    $(document).off('mousemove touchmove', resizing);
	}
	saveEventState = function(e){ // salva detalhes do evento e estado do container
		if(_semaforo === 1){
			event_state.container_width = $container.width();
		}
	    event_state.container_height = $container.height();
	    event_state.container_left = $container.offset().left;
	    event_state.container_top = $container.offset().top;
	    event_state.mouse_x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft();
	    event_state.mouse_y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();
	    if(typeof e.originalEvent.touches !== 'undefined'){ // fix mobile safari
	    	event_state.touches = [];
	    	$.each(e.originalEvent.touches, function(i, ob){
	    		event_state.touches[i] = {};
	    		event_state.touches[i].clientX = 0+ob.clientX;
	    		event_state.touches[i].clientY = 0+ob.clientY;
	    	});
	    }
	    event_state.evnt = e;
	}
	resizing = function(e){
		var mouse={},side,axis,width,height,left,top,offset=$container.offset();
		mouse.x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft();
		mouse.y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();
		// posicionamento conforme redimensiona
		if( $(event_state.evnt.target).hasClass('resize-handle-se') ){
			width = mouse.x - event_state.container_left;
			height = mouse.y - event_state.container_top;
			left = event_state.container_left;
			top = event_state.container_top;
			axis = 'resize-handle-se';
			side = '.'+axis;
		} else if($(event_state.evnt.target).hasClass('resize-handle-sw') ){
			width = event_state.container_width - (mouse.x - event_state.container_left);
			height = mouse.y - event_state.container_top;
			left = mouse.x;
			top = event_state.container_top;
			axis = 'resize-handle-sw';
			side = '.'+axis;
		} else if($(event_state.evnt.target).hasClass('resize-handle-nw') ){
			width = event_state.container_width - (mouse.x - event_state.container_left);
			height = event_state.container_height - (mouse.y - event_state.container_top);
			left = mouse.x;
			top = mouse.y;
			top = mouse.y - ((width / orig_src.width * orig_src.height) - height);
			axis = 'resize-handle-nw';
			side = '.'+axis;
		} else if($(event_state.evnt.target).hasClass('resize-handle-ne') ){
			width = mouse.x - event_state.container_left;
			height = event_state.container_height - (mouse.y - event_state.container_top);
			left = event_state.container_left;
			top = mouse.y;
			top = mouse.y - ((width / orig_src.width * orig_src.height) - height);
			axis = 'resize-handle-ne';
			side = '.'+axis;
		}

		height = width / orig_src.width * orig_src.height; // mantem proporcao
		if(width > min_width && height > min_height && width < max_width && height < max_height){
			resizeImage(width, height, side, axis);
			$container.offset({'left': left, 'top': top});
		}
	}
	resizeImage = function(width, height, side, axis){
		var diff = orig_src.width/event_state.container_width,
			title = Math.floor(width*diff) + ' x ' + Math.floor(height*diff);
		if($('.resize-container .resize-tooltip').length == 0){
			$(side).after('<span class="resize-tooltip tooltip-'+axis+'" data-width="'+Math.floor(width*diff)+'" data-height="'+Math.floor(height*diff)+'">'+title+'</span>');
		}else{
			$('.resize-container .resize-tooltip')
				.removeClass('tooltip-resize-handle-se')
				.removeClass('tooltip-resize-handle-sw')
				.removeClass('tooltip-resize-handle-nw')
				.removeClass('tooltip-resize-handle-ne')
				.addClass('tooltip-'+axis)
				.data('width', Math.floor(width*diff))
				.data('height', Math.floor(height*diff))
				.html(title);
			$('.info-arquivo-dimensoes').empty().html(title+' px');
		}
	    resize_canvas.width =  width;
	    resize_canvas.height =  height;
	    resize_canvas.getContext('2d').drawImage(orig_src, 0, 0, width, height);
	    $(image_target).attr('src', resize_canvas.toDataURL("image/png"));
	}
	init();
}

function closeResize() {
	if($('.html-crop > .resize-image').length == 0){
		var clone = $('.resize-image').clone();
		clone.addClass('hidden').attr('src', $('.recent.files .arquivos > li.active').data('url'));
		$('.html-crop').append(clone);
	}
	$('.resize-container').remove();
	$('.html-crop').find('img.hidden').removeClass('hidden');
}

function marginAuto() {
	var imgWidth = $('#insertMedia-editFile .insert-arquivo-preview-images .html-crop > img.img-responsive').width();
	$('.html-crop > .form-group').css('min-width',imgWidth+'px');
}
function restaurarImg() {
	var originalImg = $('img.original-crop-image'),
		imgElm = $('#insertMedia-editFile .insert-arquivo-preview-images .html-crop > img.img-responsive');
	imgElm.attr('src',originalImg.attr('src'));
	$('.info-arquivo-dimensoes').empty().html(originalImg.width()+' x '+originalImg.height()+' px');
}

function saveResize(arquivo) {
	var formData = new FormData();

	formData.append('fileUrl', arquivo.url);
	formData.append('dir', 'arquivos');
	formData.append('tipo', '1');
	formData.append('user_id', userId);
	formData.append('mode', 'json');
	formData.append('form_name', 'form');
	formData.append('width', arquivo.width);
	formData.append('height', arquivo.height);
	formData.append('eixoX', 0);
	formData.append('eixoY', 0);
	formData.append('resize', true);
	formData.append('dimensao', 'original');
	formData.append('nonce_token', SESSION.USER_INFO.USER_ID+Date.now());
	formData.append('wstoken', SESSION.USER_INFO.USER_WSTOKEN);

	if(arquivo.url != '') {
		jQuery.ajax({
			type: 'POST',
			url: urlContext + '/ws/uploadFile.htm',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function(){
				$('.insert-arquivo-preview-images .salvar-resize').addClass('disabled').html('<i class="fa fa-spinner fa-spin fa-fw"></i> Salvando...');
				$('#modalInsertMediaMural .modal-footer .btn-primary').addClass('disabled');
			},
			success: function(resp){
				if(resp.file){
					var arquivoResize = resp.file,
						_arquivoAntigo = arquivo.id;
					
					var galeria = createLiGaleria(resp.file);
					$('#insertMedia-biblioteca .galeria-files .arquivos').prepend(galeria);
					$('#insertMedia-biblioteca .galeria-files .arquivos li[data-arquivo-id="'+_arquivoAntigo+'"]').removeClass('active');
					$('#insertMedia-biblioteca .galeria-files .arquivos li[data-arquivo-id="'+arquivoResize.id+'"]').addClass('active');

					var anexos = $('#anexosId').val();
					anexos = anexos.replace(_arquivoAntigo+',', arquivoResize.id+',');
					$('#anexosId').val(anexos);

					$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"]')
						.data('arquivo-id', arquivoResize.id)
						.data('url', arquivoResize.url)
						.data('length', arquivoResize.length)
						.data('human', arquivoResize.lengthHumanReadable)
						.data('width', arquivoResize.width)
						.data('height', arquivoResize.height)
						.data('thumb-id0', arquivoResize.thumbs[0].id)
						.data('thumb0', arquivoResize.thumbs[0].url)
						.data('thumb-width0', arquivoResize.thumbs[0].width)
						.data('thumb-height0', arquivoResize.thumbs[0].height);
					
					if(arquivoResize.thumbs[1]){
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"]')
							.data('thumb-id1', arquivoResize.thumbs[1].id)
							.data('thumb1', arquivoResize.thumbs[1].url)
							.data('thumb-width1', arquivoResize.thumbs[1].width)
							.data('thumb-height1', arquivoResize.thumbs[1].height);
					}else{
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"]')
							.data('thumb-id1', '')
							.data('thumb1', '')
							.data('thumb-width1', '')
							.data('thumb-height1', '');
					}

					if(resp.file.thumbs[2]){
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"]')
							.data('thumb-id2', arquivoResize.thumbs[2].id)
							.data('thumb2', arquivoResize.thumbs[2].url)
							.data('thumb-width2', arquivoResize.thumbs[2].width)
							.data('thumb-height2', arquivoResize.thumbs[2].height);
					}else{
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"]')
							.data('thumb-id2', '')
							.data('thumb2', '')
							.data('thumb-width2', '')
							.data('thumb-height2', '');
					}

					$('.anexos li[data-id="'+_arquivoAntigo+'"] .thumbnail .fotoUsuario .open-gallery img').attr('src', arquivoResize.url);
					$('.anexos li[data-id="'+_arquivoAntigo+'"] .open-gallery').data('arquivo-id', arquivoResize.id).attr('data-arquivo-id', arquivoResize.id);
					$('.anexos li[data-id="'+_arquivoAntigo+'"] .nomeAnexo.alterarNome > .open-gallery > span').html('('+arquivoResize.lengthHumanReadable.toLowerCase()+')');
					$('.anexos li[data-id="'+_arquivoAntigo+'"] .nomeAnexo.alterarNome').data('arquivo-id', arquivoResize.id).attr('data-arquivo-id', arquivoResize.id);
					$('.anexos li[data-id="'+_arquivoAntigo+'"] a.ico-remover').attr('href', urlContext + '/ws/deletar.htm?mode=json&form_name=form&id='+arquivoResize.id+'&tipo=arquivo');
					$('.anexos li[data-id="'+_arquivoAntigo+'"]').data('id', arquivoResize.id).attr('data-id', arquivoResize.id);

					$('.ajax-file-upload-statusbar.alterarNome[data-id="'+_arquivoAntigo+'"] .thumbnail .fotoUsuario .open-gallery img').attr('src', arquivoResize.url);
					$('.ajax-file-upload-statusbar.alterarNome[data-id="'+_arquivoAntigo+'"] .open-gallery').data('arquivo-id', arquivoResize.id).attr('data-arquivo-id', arquivoResize.id);
					$('.ajax-file-upload-statusbar.alterarNome[data-id="'+_arquivoAntigo+'"] .removeArquivoAnexo').data('id', arquivoResize.id).attr('data-id', arquivoResize.id);
					$('.ajax-file-upload-statusbar.alterarNome[data-id="'+_arquivoAntigo+'"]').data('id', arquivoResize.id).attr('data-id', arquivoResize.id);

					$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"], .recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"] .arquivo').attr('data-arquivo-id', resp.file.id);
					$('.recent-files .arquivos > li[data-arquivo-id="'+arquivoResize.id+'"] .miniaturaArquivo a > img').attr('src', resp.file.url);
				}else{
					$('.insert-arquivo-preview-images .form-group').append('<p class="font-red-mint pull-right"><i class="fa fa-exclamation-triangle"></i> Algo de errado aconteceu.</p>');
				}

				$('.insert-arquivo-preview-images .salvar-resize').removeClass('disabled').html('<i class="fa fa-check"></i> Salvar resize');
				$('#modalInsertMediaMural .modal-footer .btn-primary').removeClass('disabled');
				$('.recent-files .arquivos > li[data-arquivo-id="'+resp.file.id+'"]').click();

				setTimeout(function(){
					$('.insert-arquivo-preview-images .form-group').append('<p class="font-green-sharp pull-right"><i class="fa fa-check"></i> Tamanho atualizado com sucesso.</p>');
				}, 100);

				setTimeout(function(){
					$('.insert-arquivo-preview-images .form-group').find('p').fadeOut(function(){
						$(this).remove();
					});
				}, 2000);
			}
		});
	}
}

function rotateImg(elm) {
	var angle = (elm.data('angle') + 90) || 90;
	if(angle>359) {angle=0}
	elm.css({'transform': 'rotate(' + angle + 'deg)', 'width': '40%', 'margin-top': '80px'});
	elm.data('angle', angle);
}

function saveRotate(arquivo) {
	var angle = $('#insertMedia-editFile .insert-arquivo-preview-images .html-crop > img.img-responsive')[0].style.transform,
		formData = new FormData();
	
	angle = angle.split('(')[1];
	angle = parseInt(angle);

	formData.append('angle', angle);
	formData.append('fileUrl', arquivo.url);
	formData.append('dir', 'arquivos');
	formData.append('tipo', '1');
	formData.append('user_id', userId);
	formData.append('mode', 'json');
	formData.append('form_name', 'form');
	formData.append('dimensao', 'original');
	formData.append('nonce_token', SESSION.USER_INFO.USER_ID+Date.now());
	formData.append('wstoken', SESSION.USER_INFO.USER_WSTOKEN);
	
	if(arquivo.url != '') {
		jQuery.ajax({
			type: 'POST',
			url: urlContext + '/ws/uploadFile.htm',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function(){
				$('.insert-arquivo-preview-images .salvar-rotate').addClass('disabled').html('<i class="fa fa-spinner fa-spin fa-fw"></i> Salvando...');
				$('#modalInsertMediaMural .modal-footer .btn-primary').addClass('disabled');
			},
			success: function(resp){
				if(resp.file){
					var arquivoRotate = resp.file,
						_arquivoAntigo = arquivo.id;

					var galeria = createLiGaleria(resp.file);
					$('#insertMedia-biblioteca .galeria-files .arquivos').prepend(galeria);
					$('#insertMedia-biblioteca .galeria-files .arquivos li[data-arquivo-id="'+_arquivoAntigo+'"]').removeClass('active');
					$('#insertMedia-biblioteca .galeria-files .arquivos li[data-arquivo-id="'+resp.file.id+'"]').addClass('active');

					var anexos = $('#anexosId').val();
					anexos = anexos.replace(_arquivoAntigo+',', arquivoRotate.id+',');
					$('#anexosId').val(anexos);

					$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"]')
						.data('arquivo-id', arquivoRotate.id)
						.data('url', arquivoRotate.url)
						.data('length', arquivoRotate.length)
						.data('human', arquivoRotate.lengthHumanReadable)
						.data('width', arquivoRotate.width)
						.data('height', arquivoRotate.height)
						.data('thumb-id0', arquivoRotate.thumbs[0].id)
						.data('thumb0', arquivoRotate.thumbs[0].url)
						.data('thumb-width0', arquivoRotate.thumbs[0].width)
						.data('thumb-height0', arquivoRotate.thumbs[0].height);
					
					if(arquivoRotate.thumbs[1]){
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"]')
							.data('thumb-id1', arquivoRotate.thumbs[1].id)
							.data('thumb1', arquivoRotate.thumbs[1].url)
							.data('thumb-width1', arquivoRotate.thumbs[1].width)
							.data('thumb-height1', arquivoRotate.thumbs[1].height);
					}else{
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"]')
							.data('thumb-id1', '')
							.data('thumb1', '')
							.data('thumb-width1', '')
							.data('thumb-height1', '');
					}

					if(resp.file.thumbs[2]){
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"]')
							.data('thumb-id2', arquivoRotate.thumbs[2].id)
							.data('thumb2', arquivoRotate.thumbs[2].url)
							.data('thumb-width2', arquivoRotate.thumbs[2].width)
							.data('thumb-height2', arquivoRotate.thumbs[2].height);
					}else{
						$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"]')
							.data('thumb-id2', '')
							.data('thumb2', '')
							.data('thumb-width2', '')
							.data('thumb-height2', '');
					}

					$('.anexos li[data-id="'+_arquivoAntigo+'"] .thumbnail .fotoUsuario .open-gallery img').attr('src', arquivoRotate.url);
					$('.anexos li[data-id="'+_arquivoAntigo+'"] .open-gallery').data('arquivo-id', arquivoRotate.id).attr('data-arquivo-id', arquivoRotate.id);
					$('.anexos li[data-id="'+_arquivoAntigo+'"] .nomeAnexo.alterarNome > .open-gallery > span').html('('+arquivoRotate.lengthHumanReadable.toLowerCase()+')');
					$('.anexos li[data-id="'+_arquivoAntigo+'"] .nomeAnexo.alterarNome').data('arquivo-id', arquivoRotate.id).attr('data-arquivo-id', arquivoRotate.id);
					$('.anexos li[data-id="'+_arquivoAntigo+'"] a.ico-remover').attr('href', urlContext + '/ws/deletar.htm?mode=json&form_name=form&id='+arquivoRotate.id+'&tipo=arquivo');
					$('.anexos li[data-id="'+_arquivoAntigo+'"]').data('id', arquivoRotate.id).attr('data-id', arquivoRotate.id);

					$('.ajax-file-upload-statusbar.alterarNome[data-id="'+_arquivoAntigo+'"] .thumbnail .fotoUsuario .open-gallery img').attr('src', arquivoRotate.url);
					$('.ajax-file-upload-statusbar.alterarNome[data-id="'+_arquivoAntigo+'"] .open-gallery').data('arquivo-id', arquivoRotate.id).attr('data-arquivo-id', arquivoRotate.id);
					$('.ajax-file-upload-statusbar.alterarNome[data-id="'+_arquivoAntigo+'"] .removeArquivoAnexo').data('id', arquivoRotate.id).attr('data-id', arquivoRotate.id);
					$('.ajax-file-upload-statusbar.alterarNome[data-id="'+_arquivoAntigo+'"]').data('id', arquivoRotate.id).attr('data-id', arquivoRotate.id);

					$('.recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"], .recent-files .arquivos > li[data-arquivo-id="'+_arquivoAntigo+'"] .arquivo').attr('data-arquivo-id', resp.file.id);
					$('.recent-files .arquivos > li[data-arquivo-id="'+arquivoRotate.id+'"] .miniaturaArquivo a > img').attr('src', resp.file.url);
				}else{
					$('.insert-arquivo-preview-images .form-group').append('<p class="font-red-mint pull-right"><i class="fa fa-exclamation-triangle"></i> Algo de errado aconteceu.</p>');
				}
				
				$('.insert-arquivo-preview-images .salvar-rotate').removeClass('disabled').html('<i class="fa fa-check"></i> Salvar imagem');
				$('#modalInsertMediaMural .modal-footer .btn-primary').removeClass('disabled');
				$('.recent-files .arquivos > li[data-arquivo-id="'+resp.file.id+'"]').click();

				setTimeout(function(){
					$('.insert-arquivo-preview-images .form-group').append('<p class="font-green-sharp pull-right"><i class="fa fa-check"></i> Imagem atualizada com sucesso.</p>');
				}, 100);

				setTimeout(function(){
					$('.insert-arquivo-preview-images .form-group').find('p').fadeOut(function(){
						$(this).remove();
					});
				}, 2000);
			}
		});
	}
}

function openEdit(elm) {
	$(elm).css('visibility','visible').animate({
		 opacity: 1,
		 width: '220px'
	}, 50).addClass('open');
}

function closeEdit(elm) {
	$(elm).css('visibility','hidden').animate({
		 opacity: 0,
		 width: '0'
	}, 50).removeClass('open');
}

$(document).ready(function(){

	$('body').on('click', '.previewFile, .nomeAnexo a:not(.fancybox, .open-gallery, .no-preview, .adicionarDescricao)', function(){
		var a = $(this), conteudo = "";

		var link = a.data("link"),
			name = a.data("name"),
			url = a.data("url"),
			ext = a.data("ext").toLowerCase();

		var vh = $(window).height(), ih = vh - 210;

		if(FILE_EXTENSIONS.image[ext]){
			conteudo += '<img src="'+url+'" alt="'+name+'" class="img-responsive" />';
		}else if(FILE_EXTENSIONS.docs[ext] || FILE_EXTENSIONS.programs[ext]){
			if(!a.hasClass("fancyJwplayer")){
				if(FILE_EXTENSIONS.pdf[ext]){
					conteudo += '<iframe src="'+link+'" class="iframe medium bordered radius spinner-blue" style="height: '+ih+'px;"></iframe>';
				}else{
					conteudo += '<iframe src="https://docs.google.com/gview?url='+link+'&embedded=true" class="iframe medium bordered radius spinner-blue menor" style="height: '+ih+'px;"></iframe>';
				}
			}
		}else if(FILE_EXTENSIONS.video[ext]){
			if(!a.hasClass("fancyJwplayer")){
				conteudo += '<div class="clearfix text-center">';
					conteudo += '<video width="400" controls><source src="'+link+'" type="video/$!arquivo.extensao"></video>';
				conteudo += '</div>';
			}
		}else if(FILE_EXTENSIONS.audio[ext]){
			if(!a.hasClass("fancyJwplayer")){
				conteudo += '<div class="clearfix text-center">';
					conteudo += '<audio controls><source src="'+link+'" type="audio/'+ext+'"></audio>';
				conteudo += '</div>';
			}
		}else if(FILE_EXTENSIONS.compressed[ext]){
			conteudo += '<div class="clearfix modal-padding text-center">';
				conteudo += '<div class="uppercase profile-stat-title icon"><i class="fa fa-file-archive-o fa-3x" aria-hidden="true"></i><span class="file-extension-banner bg-blue-chambray">'+ext+'</span></div>';
				conteudo += '<h3>Este é um arquivo compactado.</h3><p class="file-info">Para visualizar faça o download.</p>';
			conteudo += '</div>';
		}else if(FILE_EXTENSIONS.executable[ext]){
			conteudo += '<div class="clearfix modal-padding text-center">';
				if(ext == 'apk'){
					conteudo += '<div class="uppercase profile-stat-title icon"><i class="fa fa-android font-green-android fa-3x" aria-hidden="true"></i></div>';
				}else{
					conteudo += '<div class="uppercase profile-stat-title icon"><i class="fa fa-cogs font-blue fa-3x" aria-hidden="true"></i></div>';
				}
				conteudo += '<h3>Não é possível fazer o preview deste arquivo, faça o download</p>';
			conteudo += '</div>';
		}

		if(!a.hasClass("fancyJwplayer")){
			$("#modalPreviewFile .modal-body").append(conteudo);
			$("#modalPreviewFile .download-file").attr('data-file-name', name);
			$("#modalPreviewFile .download-file").attr('data-file-url', url);
			if(FILE_EXTENSIONS.pdf[ext]){
				$("#modalPreviewFile .see-in-browser").attr("href", link);
			}else{
				$("#modalPreviewFile .see-in-browser").attr("href", 'https://docs.google.com/viewerng/viewer?url='+link);
			}
			$("#modalPreviewFile .modal-title .header-file-name").html("Preview Arquivo - "+name);
			$("#modalPreviewFile").modal("show");
		}
	});

})