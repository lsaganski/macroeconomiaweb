/*
 * jQuery LiveUrl
 *
 * MIT License - You are free to use this commercial projects as long as the copyright header is left intact.
 * @author        Stephan Fischer
 * @copyright     (c) 2012 Stephan Fischer (www.ainetworks.de)
 * @version 1.2.2
 * 
 * UriParser is a function from my addon "Superswitch" for Mozilla FireFox.
 */

(function( $ )
{
    $.fn.extend(
    { 
        liveUrl : function( options) 
        {
            var defaults = 
            {
                meta: [
                    ['description','name', 'description'],
                    ['description','property', 'og:description'],
                    ['description','property', 'pinterestapp:about'],
                    ['image','property', 'og:image'],
                    ['image','itemprop', 'image'],
                    ['title','property', 'og:title'],
                    ['video','property', 'og:video'],
                    ['video_type','property', 'og:video:type'],
                    ['video_width','property', 'og:video:width'],
                    ['video_height','property', 'og:video:height']
               ],
               findLogo         : false,
               findDescription  : true,
               matchNoData      : false,
               multipleImages   : true,
               defaultProtocol  : 'https://',
               minWidth         : 30,
               minHeight        : 30,
               logoWord         : 'logo',
               success          : function() {},
               loadStart        : function() {},
               loadEnd          : function() {},
               imgLoadStart     : function() {},
               imgLoadEnd       : function() {},
               addImage         : function() {}
            }

            var options =  $.extend(defaults, options);
            
            this.each(function() 
            {
                
                var o       = options,
                    core    = {already : []},
                    url     = {},
                    preview = {};
                
                core.init = function () 
                {
                    core.preview = false;
                    core.already = [];
                    preview      = {
                        url : '',
                        images: [],
                        image : '',
                        title: '' ,
                        description: ''
                    };  
                };
                
                core.textUpdate = function(self) 
                {                
                    // read all links   
                    var links = $.urlHelper.getLinks($(self).val());
                    core.cleanDuplicates(links);
                    
                    if (links != null) {
                        if (!core.preview) {
                            core.current = $(self);
                            core.process(links);
                        }
                    }
                    
                    
                };
                
                core.process = function(urlArray) 
                {
        
                    for (var index in urlArray) {
        
                        var strUrl      = urlArray[index] ;
                        strUrl = strUrl.match(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig);
                        strUrl = strUrl[0];
                        if (typeof strUrl == 'string') {
                            var pLink       = new $.urlHelper.UriParser(strUrl);

                            if (pLink.subdomain && pLink.subdomain.length > 0 || 
                                pLink.protocol  && pLink.protocol.length  > 0 ) {
                
                                if  (pLink.protocol.length == 0) {
                                    strUrl = o.defaultProtocol + strUrl;
                                }
                
                                if (!core.isDuplicate(strUrl, core.already)) {
                                   
                                   if ($.urlHelper.isImage(strUrl)) {
                                       preview.image = strUrl;
                                       core.getPreview({}, strUrl);
                                       
                                   } else {
                                       core.getData(strUrl);  
                                   }
                                   
                                   return true;
                                }
                            }
                        }
                    } 
                };
                
                core.cleanDuplicates = function(urlArray) 
                {
                    var links = core.createLinks(urlArray);
                    
                    for(var index in core.already) {
                        var strUrl  = core.already[index];
                          
                        if (!core.isDuplicate(strUrl, links)){
                            var index = $.inArray(strUrl, core.already);
                            core.already.splice(index, 1);
                        }
                    }
                };
                
                core.createLinks = function(urlArray) 
                {
                    var links = [];
                    
                    for(var index in urlArray) {
                        var strUrl  = urlArray[index];
                        if (typeof strUrl == 'string') {
                            var pLink   = new $.urlHelper.UriParser(strUrl);
                           
                            if (pLink.subdomain && pLink.subdomain.length > 0 || 
                                pLink.protocol  && pLink.protocol.length  > 0 ) {
                                    
                                if  (pLink.protocol.length == 0) {
                                    strUrl = o.defaultProtocol + strUrl;
                                }
                                
                                links.push(strUrl);
                            }
                        }
                    }
                    
                    return links;
                }
                
                core.isDuplicate = function(url, array) 
                {
                    var duplicate = false;
                    $.each(array, function(key, val)
                    {
                        if (val == url) {
                            duplicate = true;
                        } 
                    }); 
                    
                    return duplicate;
                };
                
                core.getData = function (url)
                {
                    var xpath  =  '//head|//body';
                	
                	if(url && url.indexOf('youtu.be') > -1) {
                    	var idYoutube = url.split('youtu.be/')[1];
                    	url = "https://www.youtube.com/watch?v=" + idYoutube;
                    }
                    
                    var url2 = url, url3 = "";
                	url3 = url2.substr(url2.lastIndexOf('.') + 1);
                	if(!url3 || url3 == ""){
                		url = url2.substr(0, url.lastIndexOf('.')).replace(' ', '');
                	}
                	
                	var query = 'select * from htmlstring where url="' + url + '" AND xpath="'+xpath+'"';

                    core.addLoader();
                    
                    $.yql(query, function(data)
                        {
                    		//error
                    		if(data.status != 200){
	                            var data = {
	                                query : {results: null}
	                            }
                    		}
                            
                            core.ajaxSuccess(data, url);
                            core.removeLoader();
                            return false;  
                        },
                        function(data) 
                        {
                        	//success
                            core.ajaxSuccess(data, url)
                        }
                    )
                }; //getData
                
                core.ajaxSuccess = function(data, url)
                {
                    // URL already loaded, or preview is already shown.
                    if (core.isDuplicate(url, core.already) || core.preview) {
                        core.removeLoader();
                        return false;
                    }
                    
                    if ($(data).find("results").text().length == 0) {
                        if (o.matchNoData) {
                        	core.already.push(url);
                            core.removeLoader();
                        }  else {
                        	if(data.query.results != null && data.query.results.result != null){
                        		core.getPreview(data.query.results.result, url);
                        	}else{
                        		core.getPreview(data, url);
                        	}
                        }
                        
                    } else {
                        core.getPreview(data, url);
                    }
                }
                
                
                core.hasValue     = function(section){
                    return (preview[section].length == 0) ? false : true;  
                };
                
                core.getPreview = function(data, uri)
                {
                    core.preview = true; 
                    core.already.push(uri);
                    var parser = new DOMParser(), xpto = parser.parseFromString(data, 'text/html'), title  = "" ;
                    
                    if(uri.match(__docRegexNoHTML)){
                		var doc = uri.match(__docRegexNoHTML), doc = doc[0], title = doc.substring(doc.lastIndexOf('/')+1, doc.length), ext = title.substring(title.lastIndexOf('.')+1, title.length), image = 'https://livecom.livetouchdev.com.br/img/file_extensions/avulso/';
                		var description = 'Documento '+ext;
                		
                		if(PARAMS.MURAL_WEBVIEW_ON == '1'){
                			if(title.lastIndexOf('.css')){
                				return false;
                			}
                		}
                		
                    	if(FILE_EXTENSIONS.pdf[ext]){
                    		image += 'pdf.png';
                    	}else if(FILE_EXTENSIONS.docs[ext]){
                    		if(ext == 'csv' || ext == 'xls' || ext == 'xlsx'){
                    			image += 'excel.png';
                    		}else{
                    			image += 'text.png';
                    		}
                    	}else if(FILE_EXTENSIONS.programs[ext]){
                    		image += 'code.png';
                    	}else if(FILE_EXTENSIONS.compressed[ext]){
                    		image += 'compressed.png';
                    	}else if(ext == 'apk'){
                    		image += 'apk.png';
                    	}else if(ext == 'ai' || ext == 'cdr' || ext == 'indd' || ext == 'eps'){
                    		image += 'vector.png';
                    	}else if(ext == 'psd'){
                    		image += 'paint.png';
                    	}else{
                    		image += 'default.png';
                    	}
                    	
                    	preview.description = description;
                    	preview.images.push(image);
                    }else{
                    	if($(xpto, uri).find('title').length > 0){
	                    	$(xpto, uri).find('title').each(function() {
	                            title = $(this).text();
	                        });
                    	}else{
                    		if(typeof data == 'string' && !data.query && data.lastIndexOf('<title>') > -1){
                    			title = data.substring(data.lastIndexOf('<title>')+7, data.lastIndexOf('</title>'));
                    		}else{
                    			title = uri;
                    		}
                    	}
                    	
                    	$(xpto, uri).find('meta').each(function() 
                        {
                            core.setMetaData($(this));
                             
                        });
                        
                        if(o.findDescription && !core.hasValue('description')) {
                            
                            $(xpto, uri).find('p').each(function() 
                            {
                                var text = $.trim($(this).text());
                                if(text.length > 3) {
                                    preview.description = text;
                                    return false;
                                }
                                 
                            });
                        }
                        
                        if (!core.hasValue('image')) {
                        // meta tag has no images:
                        
                            var images = $(xpto, uri).find('link[rel^="apple-touch-icon"]');
                            if(images.length == 0){
                            	images = $(xpto, uri).find('link[rel~="icon"]');
                            }
                            
                            if (o.findLogo ) {
                               images.each(function() 
                                {
                                    var self = $(this);
                                    
                                    if (self.attr('src') && self.attr('src').search(o.logoWord, 'i')  != -1 || 
                                        self.attr('id' ) && self.attr('id' ).search(o.logoWord, 'i')  != -1 ||
                                        this.className   &&   this.className.search(o.logoWord, 'i')  != -1  
                                                                    ) {
                                        preview.image = $(this).attr('src');
                                        return false;
                                    }

                                }); 
                            }

                            
                            if (!core.hasValue('image') && images.length > 0 ) {
                                images.each(function(){
                            		preview.images.push($(this).attr('href'));
                                });
                            } 
                        }
                    }
                    
                    preview.title       = ( title || uri);
                    preview.url         = uri;
                    
                    core.removeLoader();
                    
                    // prepare output
                    var not   = 'undefined';
                    var data  = {
                        title       : preview.title,
                        description : preview.description,
                        url         : preview.url,
                        video       : (typeof preview.video != not && preview.video.length > 0) ? {} : null
                    };
                    
                    if (data.video != null) {
                        data.video = {
                            file  :   preview.video,
                            type  : (typeof preview.video_type   != not) ? preview.video_type  : '',
                            width : (typeof preview.video_width  != not) ? preview.video_width : '',
                            height: (typeof preview.video_height != not) ? preview.video_height :''
                        }
                    }
                    
                    o.success(data);
                    
                    if (core.hasValue('image')){
                        preview.images.push(preview.image); 
                        preview.image = '';
                    }
                    
                    
                    core.addImages();
                    core.current.one('clear', function() 
                    {
                       core.init();
                    });     
                };
                
                core.addLoader = function()
                {  
                    o.loadStart();
                };
                
                core.removeLoader = function() 
                {
                    o.loadEnd();
                };
                
                core.setMetaData = function(val) 
                {
                    for (index in o.meta) {
                        var meta = o.meta[index];
                        preview[meta[0]] = (core.getValue(val,meta[1],meta[2])|| preview[meta[0]] );
                    }
                };
                
                core.getValue = function (val,key, tag) {
                    if (val.attr(key)) {
                        if (val.attr(key).toLowerCase() ==  tag.toLowerCase()) {
                            if (val.attr('content') && val.attr('content').length > 0) {
                                return val.attr('content');
                            }
                        } 
                        
                    }
                };
                
                core.addImages = function() 
                {
                    var images = [];
                    
                    for (var index in preview.images) {
                        var image = preview.images[index];
                        
                        if (!$.urlHelper.isAbsolute(image)) {
                            var pLink    = new $.urlHelper.UriParser(preview.url);
                            var host     = pLink.url + pLink.subdomain + pLink.domain;

                            if ($.urlHelper.isPathAbsolute(image)) 
                                 image = host + image; 
                            else image = host + $.urlHelper.stripFile(pLink.path) + '/' + image;
                        }
                        
                        core.getImage(image, function(img) 
                        {
                            if (img.width  >= o.minWidth  && 
                                img.height >= o.minHeight && core.preview) {

                                o.addImage(img);
                                
                                if(!o.multipleImages) {
                                    return;
                                }
                                
                            }
                        });
                    }

                };
                
                core.getImage = function(src, callback)
                {
                    var concat  =  $.urlHelper.hasParam(src) ? "&" : "?";
                    src        +=  concat + 'random=' + (new Date()).getTime();
                    
                    $('<img />').attr({'src': src}).load(function() 
                    {
                        var img = this;
                        var tmrLoaded = window.setInterval(function()
                        {   
                            if (img.width) {
                                window.clearInterval(tmrLoaded);  
                                callback(img);
                            }
                        }, 100);
                    });
                };

                core.init();
                var that  = this;
                var self  = $(this);
                
                self.on('keyup', function(e) 
                {
                    var links = $.urlHelper.getLinks($(self).val());
                    core.cleanDuplicates(links);
                    
                    window.clearInterval(core.textTimer); 
                    
                    var code = (e.keyCode ? e.keyCode : e.which);
                    
                    if(code == 13 || code == 32) { //Enter keycode
                        core.textUpdate(that);
                    } else {
                        core.textTimer = window.setInterval(function()
                        {   
                            core.textUpdate(that);
                            window.clearInterval(core.textTimer);  
                        }, 1000);
                    }
                }).on('paste', function() {core.textUpdate(that)});
                
                $("button[data-searchliveurl]").click(function(e) {
                	e.preventDefault();
                	e.stopPropagation();
                	core.init();
                	core.textUpdate(that);
                });
                
            });
        }
    });

    jQuery.yql = function yql(query, error, success)
    {
        var isIE = /msie/.test(navigator.userAgent.toLowerCase());
        
        if (isIE && window.XDomainRequest) {
            var xdr = new XDomainRequest();
            xdr.open("get", "https://query.yahooapis.com/v1/public/yql?q=" + encodeURIComponent(query) + "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
            xdr.onload = function() 
            {
                success(xdr.responseText);
            };
            
            xdr.send();
         
        } else {
            $.ajax({
                crossDomain : true,
                cache    : false,
                type     : 'GET',
                url      : "https://query.yahooapis.com/v1/public/yql?q=" + encodeURIComponent(query) + "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys", 
                dataType : 'json',
                error    : error,
                timeout: 10000,
                success  : success
            });   
        }
    };
    
    jQuery.urlHelper =
    {
        UriParser :  function (uri)
        { 
            this._regExp      = /^((\w+):\/\/\/?)?((\w+):?(\w+)?@)?([^\/\?:]+)?(:\d+)?(.*)?/;
            this._regExpHost  = /^(.+\.)?(.+\..+)$/;
   
            this._getVal = function(r, i) 
            {
                if(!r) return null;
                return (typeof(r[i]) == 'undefined' ? "" : r[i]);
            };
          
            this.parse = function(uri) 
            {
                var r          = this._regExp.exec(uri);
                this.results   = r;
                this.url       = this._getVal(r,1);
                this.protocol  = this._getVal(r,2);
                this.username  = this._getVal(r,4);
                this.password  = this._getVal(r,5);
                this.domain    = this._getVal(r,6);
                this.port      = this._getVal(r,7);
                this.path      = this._getVal(r,8);
                
                var rH         = this._regExpHost.exec( this.domain );
                this.subdomain = this._getVal(rH,1);
                this.domain    = this._getVal(rH,2); 
                return r;
            }
              
            if(uri) this.parse(uri);
        },
        getLinks : function(text) 
        {
            var expression = /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s\[]{2,}|www\.[^\s]+\.[^\s\[]{2,})/g;
            return (text.match(expression));
        },
        isImage : function(img, allowed)
        {
            //Match jpg, gif, png, wbep or ico image  
            if (allowed == null)  allowed = 'jpg|gif|png|jpeg|webp|ico';
            
            var expression = /([^\s]+(?=\.(jpg|gif|png|jpeg|webp|ico))\.\2)/gm; 
            return (img.match(expression));
        },
        isAbsolute : function(path)
        {
            var expression = /^(https?:)?\/\//i;
            var value =  (path.match(expression) != null) ? true: false;
            
                            
            return value;
        },
        isPathAbsolute : function(path)
        {
            if (path.substr(0,1) == '/') return true;
        },
        hasParam : function(path)
        {
             return (path.lastIndexOf('?') == -1 ) ? false : true;
        },
        stripFile : function(path) {
            return path.substr(0, path.lastIndexOf('/') + 1);
        } 
    }
})( jQuery );