var dTableModal = null;
var status = "";
var rows_selected = [];

function clearForm(assuntoOriginal){
	$("#form_assunto").val(assuntoOriginal);
}

var templatesHTML = [];
function consultaTemplate(){
	jQuery.ajax({
		type: "GET",
		url: urlContext+"/rest/v1/template/tipo?tipo=convite",
		beforeSend: function(data){
			
		},
		success: function (data){
			if(data && data.length > 0){
				var templates = "";
				var templateConvite = [];
				var combo = "";
				var qtdTemp = ""
				for (var i in data) {
					templates = data[i];
					qtdTemp++;
					templateConvite.push({
						id: templates.id,
						nome: templates.nome,
						html: templates.html
					});
					templatesHTML[templates.id] = templates.html;
				}
				
				combo += "<select id='formTemplate'>";
					for(var j = 0; j < qtdTemp; j++){
						combo += "<option value='"+templateConvite[j].id+"'>"+templateConvite[j].nome+"</option>";
					}
				combo += "</select>";
				$(".comboTemplate").removeClass("hidden").append(combo);
				$('select:not(.form-control)').customSelect();
 			}else{
 				$("#modalError").modal("show");
 			}
		}
	});
}

function convidaUsers(usuarios, tipo){
	jsHttp.post(urlContext+"/ws/convidarUsuarios.htm", {
		contentType: jsHttp.CONTENT_TYPE_FORM,
		data: {
			user_id: userId,
			usuarios: usuarios,
			assunto: $("#form_assunto").val(),
			template: $("#formTemplate option:selected").val(),
			tipo: tipo
		},
		beforeSend: function(data){
			$(".list-users-content, .send-mail-content, .error-content").hide();
			$(".cancelar").addClass("hidden");
			$(".spinner-content").fadeIn();
			$(".mt-step-col.second").removeClass("active");
			$(".mt-step-col.second").addClass("done");
			$(".mt-step-col.last").addClass("active");
			$(".invite-users").addClass("disabled");
		},
		success: function(data) {
			if(data.mensagem.status != "NOK"){
			    setTimeout(function(){
			    	$(".spinner-content, .error-content").hide();
			    	$(".mt-step-col.last").removeClass("active");
					$(".mt-step-col.last").addClass("done");
			    	$(".success-content").fadeIn(function(){
			    		$(".closeModal").removeClass("hidden");
			    		$(".invite-users").addClass("hidden");
			    		$(".invite-users").removeClass("disabled");
			    	});
			    	
			    	var $table = $('.table-email');
					if ( $.fn.DataTable.isDataTable( '.table-email' ) ) {
						$table.find('tbody').empty();
						$table.DataTable().clear();
					}
					
					$('.has-success').removeClass('has-success');
			    	
			    }, 3000);
			}else{
				setTimeout(function(){
			    	$(".spinner-content, .success-content").hide();
			    	$(".mt-step-col.last").removeClass("active");
					$(".mt-step-col.last").addClass("done");
					$(".error-content h4").html(data.mensagem.mensagem);
			    	$(".error-content").fadeIn(function(){
			    		$(".closeModal").removeClass("hidden");
			    		$(".invite-users").addClass("hidden");
			    		$(".invite-users").removeClass("disabled");
			    	});
			    }, 3000);
			}
		},
		error: function(data){
			setTimeout(function(){
		    	$(".spinner-content, .success-content").hide();
		    	$(".mt-step-col.last").removeClass("active");
				$(".mt-step-col.last").addClass("done");
				$(".error-content h5").html(data.mensagem);
		    	$(".error-content").fadeIn(function(){
		    		$(".closeModal").removeClass("hidden");
		    		$(".invite-users").addClass("hidden");
		    		$(".invite-users").removeClass("disabled");
		    	});
		    }, 3000);
		}
	});
}

$(document).ready(function(){
	
	var assuntoOriginal = $("#form_assunto").val();
	
	if(rows_selected.length == 0){
		$(".convidar").addClass("disabled");
	}
	
	$('#modalEnviar').on('hidden.bs.modal', function () {
		$(".tabelaModal tbody").html("");
		$(".invite-users, .closeModal").addClass("hidden");
		$(".proximo, .cancelar").removeClass("hidden");
		$(".list-users-content").show();
		$(".send-mail-content, .success-content, .spinner-content, .error-content").hide();
		clearForm(assuntoOriginal);
		$("body").find("input[type='checkbox']").iCheck('uncheck');
		$(".mt-step-col.first, .mt-step-col.second, .mt-step-col.last").removeClass("done");
		$(".mt-step-col.second, .mt-step-col.last").removeClass("active");
		$(".mt-step-col.fist").addClass("active");
	});
	
	$('body').on('click', '#inviteConvidarEmailLote', function(e) {
		e.preventDefault();
		if(rows_selected && rows_selected.length > 0) {
			convidaUsers(rows_selected.join(), 1);
		}
	});
	
	$(".proximo").click(function(){
		$(this).addClass("hidden");
		$(".invite-users").removeClass("hidden");
		$(".list-users-content").hide();
		$(".send-mail-content").fadeIn();
		$("#formTemplate").change();
		$(".mt-step-col.first").addClass("done");
		$(".mt-step-col.second").addClass("active");
	});
	
	$("body").on("change", "#formTemplate", function(){
		var value = $(this).children("option:selected").val();
		var html = templatesHTML[value];
		$('#tempFrame').contents().find('html').html(html);
	});
	
	$('#convidarLote').click(function (e) {
		e.preventDefault();
		$("#modalEnviar").modal('show');
		
		$(".invite-users").attr('id', 'inviteConvidarEmailLote');
		
		var $table = $('.tabelaModal');
		if ( $.fn.DataTable.isDataTable( '.tabelaModal' ) ) {
			$table.DataTable().clear();
			$table.DataTable().destroy();
		}
		
		$table.empty();
		var table = '';
		table += '<thead>';
			table += '<tr>';
				table += '<th>Nome</th>';
				table += '<th>Login</th>';
				table += '<th>E-mail</th>';
				table += '<th>Cargo</th>';
				table += '<th>Grupos</th>';
				table += '<th>Status</th>';
				table += '<th>Origem</th>';
			table += '</tr>';
		table += '</thead>';
		$('.tabelaModal').append(table);
		dTableModal = dtTableUtils('.tabelaModal');
		
		$("#removeModal").hide();
		$(".invite-users").show();
		if(rows_selected && rows_selected.length == 0) {
			jAlert("Favor selecione os usuários para os quais deseja enviar o convite.", "Atenção", null);
			return;
		}
		
		$(".tabela-modal-email").hide();
		var jsonDatatable = {
			columns: ['nome', 'login', 'email', 'funcao', 'grupo', 'status', 'origemCadastro'],
			columnDefs: [],
			ajax : {
				type : 'POST',
				url : urlContext+"/ws/usuariosAutoComplete.htm?mode=json&form_name=form",
			},
			element : ".tabelaModal",
			fnRowCallback: function(row, data, dataIndex) {
	        	createTooltip($(row).find('a.user-name.tooltipster'));
			},
			filter : [{
				name : 'wsVersion',
				value : wsVersion
			}, {
				name : 'wstoken',
				value : SESSION.USER_INFO.USER_WSTOKEN
			}, {
				name : 'usuarios',
				value : rows_selected.join()
			},{
				name : 'buscaTotal',
				value : 1
			}],
			filterResponse : function(res) {
				var json = {};
				if (res && res.list) {
					var list = res.list.usuarios;
					var data = [];
					for ( var i in list) {
						var u = list[i];
						
						var funcao = u.funcao ? u.funcao : '--';
						
						var colunaNome = '<a href="'+urlContext/+'/usuario.htm?id='+u.id+'" class="user-name tooltipster" data-name="'+u.nome+'" data-img="'+u.urlFotoUsuarioThumb+'" data-since="'+u.dataCreated+'" data-nasc="'+u.dataNasc+'" data-cargo="'+funcao+'">'+u.nome+'</a>';
						
						var stringGrupos = '<span class="trunca sm">'+u.gruposStr+'</span>';
						
						var js = {
							nome: colunaNome,
							login: u.login,
							email: u.email,
							funcao: funcao,
							grupo: stringGrupos,
							status: u.status,
							origemCadastro: u.origem,
						};
						data.push(js);
					}
					json.data = data;
					json.recordsTotal = res.list.total;
					json.recordsFiltered = res.list.total;
					return json;
				}
				return null;
			}
		}
		
		if(dTableModal != null) {
			$(".tabelaModal > tbody, .tabela-modal-email > tbody").empty();
			dTableModal.clear();
			dTableModal.destroy();
		}
		
		dTableModal = dtTableUtils(jsonDatatable);
		
	});
	
	/************************************************************
	 * 
	 * Convidar usuários que ainda não possuem cadastro no Livecom
	 * 
	 ************************************************************/
	$('#convidarUsuariosEmail').click(function(e) {
		e.preventDefault();
		var $table = $('.tabelaModal');
		if ( $.fn.DataTable.isDataTable( '.tabelaModal' ) ) {
			$table.DataTable().clear();
			$table.DataTable().destroy();
		}
		
		var data = $('.portlet-body .table').DataTable().data();
		$("#modalEnviar").modal('show');
		$(".invite-users").attr('id', 'inviteConvidarEmail');
		
		var table = '';
		table += '<thead>';
			table += '<tr>';
				table += '<th>Nome</th>';
				table += '<th>Login</th>';
				table += '<th>Grupos</th>';
			table += '</tr>';
		table += '</thead>';
		table += '<tbody>';
			data.each(function(user) {
				table += '<tr>';
					table += '<td>'+user[0]+'</td>';
					table += '<td>'+user[1]+'</td>';
					table += '<td>'+user[2]+'</td>';
				table += '</tr>';
			});
		table += '</tbody>';

		$table.empty();
		$table.append(table);
		var tableModal = dtTableUtils('.tabelaModal');
		$('.tabelaModal').attr('style', 'width:100%');
	});
	
	/**
	 * Envia os convite para os usuários da lista
	 */
	$('body').on('click', '#inviteConvidarEmail', function(e) {
		e.preventDefault();
		var $table = $('.tabelaModal');
		var data = $table.DataTable().data();
		var users = "";
		data.each(function(user) {
			var input = user[2];
			var grupos = $($.parseHTML(input)[1]).val().trim();
			users = user[0] + ":" + user[1] + ":" + grupos + ";";
		});
		convidaUsers(users, 2);
	});
});