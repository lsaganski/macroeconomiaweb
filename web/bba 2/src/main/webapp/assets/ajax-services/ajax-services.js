var AjaxServices = (function() {
	var usuariosAutoComplete = function(data, beforeSend){
		return jQuery.ajax({
			url: urlContext + "/ws/usuariosAutoComplete.htm?mode=json&form_name=form",
			type: 'POST',
			data: data,
			beforeSend: beforeSend
		});
	};
	
	var gruposAutoComplete = function(data){
		return jQuery.ajax({
			url: urlContext + "/ws/gruposAutoComplete.htm?mode=json&form_name=form",
			type: 'POST',
			data: data
		});
	}
	
	return {
		usuariosAutoComplete: usuariosAutoComplete,
		gruposAutoComplete: gruposAutoComplete,
	}
}());

var service = (function() {
	var tag = {};
	tag.salvar = function(params) {
		params.id = parseInt(params.id);
		if(params.id) {
			return jsHttp.put(urlContext + "/rest/v1/tag", {
				data: params
			});
		} else {
			return jsHttp.post(urlContext + "/rest/v1/tag", {
				data: params
			});
		}
	}
	
	var categoria = {};
	categoria.salvar = function(params) {
		params.id = parseInt(params.id);
		if(params.id) {
			return jsHttp.put(urlContext + "/rest/v1/categoria", {
				data: params
			});
		} else {
			return jsHttp.post(urlContext + "/rest/v1/categoria",  {
				data: params
			});
		}
		
	}

	return {
		tag: tag,
		categoria: categoria
	}
} ());

function usuarioTokenInput(objUsuarioTokenInput){
	if(!objUsuarioTokenInput.url){
		objUsuarioTokenInput.url = urlContext+"/ws/usuariosAutoComplete.htm?mode=json&form_name=form";
	}
	
	$(objUsuarioTokenInput.element).tokenInput(objUsuarioTokenInput.url, {
		method: "POST",
		jsonContainer : objUsuarioTokenInput.jsonContainer ? objUsuarioTokenInput.jsonContainer : "list",
		queryParam: objUsuarioTokenInput.queryParam ? objUsuarioTokenInput.queryParam : "q",
		hintText: objUsuarioTokenInput.hintText ? objUsuarioTokenInput.hintText : "$messages.get('msg.digitar')",
		noResultsText: objUsuarioTokenInput.noResultsText ? objUsuarioTokenInput.noResultsText : "$messages.get('msg.item.notFound.error')",
		searchingText: objUsuarioTokenInput.searchingText ? objUsuarioTokenInput.searchingText : "$messages.get('carregando.label')",
		preventDuplicates: true,
		prePopulate: objUsuarioTokenInput.prePopulate ? objUsuarioTokenInput.prePopulate : null,
		placeholder: objUsuarioTokenInput.placeholder ? objUsuarioTokenInput.placeholder : "Digite os usuários...",
		minChars: 0,
		propertyToSearch: objUsuarioTokenInput.propertyToSearch ? objUsuarioTokenInput.propertyToSearch : "nome",
		onSend: objUsuarioTokenInput.onSend ? objUsuarioTokenInput.onSend : null,
		onAdd: objUsuarioTokenInput.onAdd ? objUsuarioTokenInput.onAdd : function (item) {
			var ids = $("#idsUsers").val();
			$("#idsUsers").val(ids+" "+item.id+",");
			
			var names = $("#nameUsers").val();
			$("#nameUsers").val(names+" "+item.nome+",");
		},
		onDelete: objUsuarioTokenInput.onDelete ? objUsuarioTokenInput.onDelete : function (item){
			var idRemove = " "+item.id+",";
			var ids = $("#idsUsers").val();
			if (typeof ids != "undefined"){
				ids = ids.replace(idRemove,"");
				$("#idsUsers").val(ids);				
			}
	      
			var nameRemove = " "+item.nome+",";
			var names = $("#nameUsers").val();
			if (typeof names != "undefined"){
				names = names.replace(nameRemove,"");
				$("#nameUsers").val(names);				
			}
		},
		resultsFormatter: objUsuarioTokenInput.resultsFormatter ? objUsuarioTokenInput.resultsFormatter : function(item){
			if(item.urlFotoUsuarioThumb){
				return "<li>" + "<div class='fotoUsuario media'><div class='centraliza'><img src='" + item.urlFotoUsuarioThumb + "' alt='Foto do grupo" + item.nome + "' /></div></div>" + "<div class='center-block'><p class='user-name'>" + item.nome + "</p><p class='cargoUsuarioLista'>Cargo: " + item.funcao + "</p></div></li>"	
			}else{
				return "<li>" + "<div class='fotoUsuario media'><div class='centraliza'><img src='"+urlContext+"/imagens/user.png' alt='Foto do grupo" + item.nome + "' /></div></div>" + "<div class='center-block'><p class='user-name'>" + item.nome + "</p><p class='cargoUsuarioLista'>Cargo: " + item.funcao + "</p></div></li>"
			}	
     	}
	});
}

function usuarioRestTokenInput(objUsuarioRestTokenInput){
	if(!objUsuarioRestTokenInput.url){
		objUsuarioRestTokenInput.url = urlContext+"/ws/usuariosAutoComplete.htm?mode=json&form_name=form";
	}
	
	$(objUsuarioRestTokenInput.element).tokenInput(objUsuarioRestTokenInput.url, {
		method: "GET",
		jsonContainer : objUsuarioRestTokenInput.jsonContainer ? objUsuarioRestTokenInput.jsonContainer : "list",
		queryParam: objUsuarioRestTokenInput.queryParam ? objUsuarioRestTokenInput.queryParam : "q",
		hintText: objUsuarioRestTokenInput.hintText ? objUsuarioRestTokenInput.hintText : "$messages.get('msg.digitar')",
		noResultsText: objUsuarioRestTokenInput.noResultsText ? objUsuarioRestTokenInput.noResultsText : "$messages.get('msg.item.notFound.error')",
		searchingText: objUsuarioRestTokenInput.searchingText ? objUsuarioRestTokenInput.searchingText : "$messages.get('carregando.label')",
		preventDuplicates: true,
		prePopulate: objUsuarioRestTokenInput.prePopulate ? objUsuarioRestTokenInput.prePopulate : null,
		placeholder: objUsuarioRestTokenInput.placeholder ? objUsuarioRestTokenInput.placeholder : "Digite os usuários...",
		minChars: 0,
		propertyToSearch: objUsuarioRestTokenInput.propertyToSearch ? objUsuarioRestTokenInput.propertyToSearch : "nome",
		onAdd: objUsuarioRestTokenInput.onAdd ? objUsuarioRestTokenInput.onAdd : function (item) {
			var ids = $("#idsUsers").val();
			$("#idsUsers").val(ids+" "+item.id+",");
			
			var names = $("#nameUsers").val();
			$("#nameUsers").val(names+" "+item.nome+",");
		},
		onDelete: objUsuarioRestTokenInput.onDelete ? objUsuarioRestTokenInput.onDelete : function (item){
			var idRemove = " "+item.id+",";
			var ids = $("#idsUsers").val();
			ids = ids.replace(idRemove,"");
			$("#idsUsers").val(ids);
	      
			var nameRemove = " "+item.nome+",";
			var names = $("#nameUsers").val();
			names = names.replace(nameRemove,"");
			$("#nameUsers").val(names);
		},
		resultsFormatter: objUsuarioRestTokenInput.resultsFormatter ? objUsuarioRestTokenInput.resultsFormatter : function(item){
			if(item.urlFotoUsuarioThumb){
				return "<li>" + "<div class='fotoUsuario media'><div class='centraliza'><img src='" + item.urlFotoUsuarioThumb + "' alt='Foto do grupo" + item.nome + "' /></div></div>" + "<div class='center-block'><p class='user-name'>" + item.nome + "</p><p class='cargoUsuarioLista'>Cargo: " + item.funcao + "</p></div></li>"	
			}else{
				return "<li>" + "<div class='fotoUsuario media'><div class='centraliza'><img src='"+urlContext+"/imagens/user.png' alt='Foto do grupo" + item.nome + "' /></div></div>" + "<div class='center-block'><p class='user-name'>" + item.nome + "</p><p class='cargoUsuarioLista'>Cargo: " + item.funcao + "</p></div></li>"
			}	
     	}
	});
}

function gruposTokenInput(objGruposTokenInput){
	if(!objGruposTokenInput.url){
		objGruposTokenInput.url = urlContext+"/ws/gruposAutoComplete.htm?mode=jsonform_name=form";
	}
	
	$(objGruposTokenInput.element).tokenInput(objGruposTokenInput.url, {
		method: "POST",
	  	jsonContainer : objGruposTokenInput.jsonContainer ? objGruposTokenInput.jsonContainer : 'list',
	  	queryParam: objGruposTokenInput.queryParam ? objGruposTokenInput.queryParam : "q",
	  	hintText: objGruposTokenInput.hintText ? objGruposTokenInput.hintText : "Comece a digitar para buscar.",
		noResultsText: objGruposTokenInput.noResultsText ? objGruposTokenInput.noResultsText : "Nenhum item encontrado",
		searchingText: objGruposTokenInput.searchingText ? objGruposTokenInput.searchingText : "Carregando...",
	   	preventDuplicates: true,
	   	prePopulate: objGruposTokenInput.prePopulate ? objGruposTokenInput.prePopulate : null,
	   	propertyToSearch: objGruposTokenInput.propertyToSearch ? objGruposTokenInput.propertyToSearch : "nome",
	   	placeholder: objGruposTokenInput.placeholder ? objGruposTokenInput.placeholder : "Digite os grupos...",
		onSend: objGruposTokenInput.onSend ? objGruposTokenInput.onSend : null,
	   	onAdd: objGruposTokenInput.onAdd ? objGruposTokenInput.onAdd : function (item) {
	   		var ids = $("#idsGrupos").val();
			$("#idsGrupos").val(ids+" "+item.id+",");
			
			var names = $("#nameGrupos").val();
			$("#nameGrupos").val(names+" "+item.nome+",");
		},
		onDelete: objGruposTokenInput.onDelete ? objGruposTokenInput.onDelete : function (item) {
			var idRemove = " "+item.id+",";
			var ids = $("#idsGrupos").val();
			ids = ids.replace(idRemove,"");
			$("#idsGrupos").val(ids);
	      
			var nameRemove = " "+item.nome+",";
			var names = $("#nameGrupos").val();
			names = names.replace(nameRemove,"");
			$("#nameGrupos").val(names);
		},
		resultsFormatter: objGruposTokenInput.resultsFormatter ? objGruposTokenInput.resultsFormatter : function(item){
			if(item.virtual && item.countSubgrupos){
				var gu = item.countSubgrupos;
			}else{
				var gu = item.countUsers;
			}
			
			if(item.urlFotoThumb){
				var grupoImg = item.urlFotoThumb;
				if(grupoImg){
					if(grupoImg.substr(0,4).indexOf('img/') > -1) {
						grupoImg = urlContext + '/' + grupoImg;
					}
				}else{
					grupoImg = urlContext + "/imagens/bg_grupo_thumb.jpg";
				}
				return "<li>" + "<div class='fotoUsuario media'><div class='centraliza'><img src='" + grupoImg + "' alt='Foto do grupo" + item.nome + "' /></div></div>" + "<div class='center-block'><p class='user-name'>" + item.nome + "</p><p class='cargoUsuarioLista'>Integrantes: " + gu + "</p></div></li>"
			}else{
				return "<li>" + "<div class='fotoUsuario media'><div class='centraliza'><img src='$context/imagens/grupo.png' alt='Foto do grupo" + item.nome + "' /></div></div>" + "<div class='center-block'><p class='user-name'>" + item.nome + "</p><p class='cargoUsuarioLista'>Integrantes: " + gu + "</p></div></li>"
			}
		}
	});
}

function cargoTokenInput(objCargoTokenInput){
	if(!objCargoTokenInput.url){
		objCargoTokenInput.url = urlContext+"/rest/v1/funcao?buscaTotal=0&page=0&maxRows=10";
	}
	
	$(objCargoTokenInput.element).tokenInput(objCargoTokenInput.url, {
		method: "GET",
	  	jsonContainer : objCargoTokenInput.jsonContainer ? objCargoTokenInput.jsonContainer : "funcoes",
	  	queryParam: objCargoTokenInput.queryParam ? objCargoTokenInput.queryParam : "q",
	  	hintText: objCargoTokenInput.hintText ? objCargoTokenInput.hintText : "Comece a digitar para buscar.",
		noResultsText: objCargoTokenInput.noResultsText ? objCargoTokenInput.noResultsText : "Nenhum item encontrado",
		searchingText: objCargoTokenInput.searchingText ? objCargoTokenInput.searchingText : "Carregando...",
		preventDuplicates: true,
		prePopulate: objCargoTokenInput.prePopulate ? objCargoTokenInput.prePopulate : null,
		propertyToSearch: objCargoTokenInput.propertyToSearch ? objCargoTokenInput.propertyToSearch : "nome",
		tokenLimit: objCargoTokenInput.tokenLimit ? objCargoTokenInput.tokenLimit : 1,
		placeholder: objCargoTokenInput.placeholder ? objCargoTokenInput.placeholder : "Digite o cargo...",
		onAdd: objCargoTokenInput.onAdd ? objCargoTokenInput.onAdd : function (item) {
			$("#idsCargos").val(item.id);
		},
		onDelete: objCargoTokenInput.onDelete ? objCargoTokenInput.onDelete : function (item) {
	    	$("#idsCargos").val('');
		}
	});
}

function cidadesTokenInput(objCidadesTokenInput){
	if(!objCidadesTokenInput.url){
		objCidadesTokenInput.url = urlContext+ "/ws/cidades.htm?mode=json&form_name=form";
	}
	
	$(objCidadesTokenInput.element).tokenInput(objCidadesTokenInput.url, {
		method: "POST",
	  	jsonContainer : objCidadesTokenInput.jsonContainer ? objCidadesTokenInput.jsonContainer : "list",
	  	queryParam: objCidadesTokenInput.queryParam ? objCidadesTokenInput.queryParam : "cidade",
	  	hintText: objCidadesTokenInput.hintText ? objCidadesTokenInput.hintText : "Comece a digitar para buscar.",
		noResultsText: objCidadesTokenInput.noResultsText ? objCidadesTokenInput.noResultsText : "Nenhum item encontrado",
		searchingText: objCidadesTokenInput.searchingText ? objCidadesTokenInput.searchingText : "Carregando...",
		preventDuplicates: true,
		prePopulate: objCidadesTokenInput.prePopulate ? objCidadesTokenInput.prePopulate : null,
		propertyToSearch: objCidadesTokenInput.propertyToSearch ? objCidadesTokenInput.propertyToSearch : "nome",
		tokenLimit: objCidadesTokenInput.tokenLimit ? objCidadesTokenInput.tokenLimit : 1,
		placeholder: objCidadesTokenInput.placeholder ? objCidadesTokenInput.placeholder : "Digite a cidade...",
		onAdd: objCidadesTokenInput.onAdd ? objCidadesTokenInput.onAdd : function (item) {
			$("#idsCidades").val(item.id);
		},
		onDelete: objCidadesTokenInput.onDelete ? objCidadesTokenInput.onDelete : function (item) {
	    	$("#idsCidades").val('');
		}
	});
}

function areasTokenInput(objAreasTokenInput){
	if(!objAreasTokenInput.url){
		objAreasTokenInput.url = urlContext + "/rest/v1/area/";
	}
	
	$(objAreasTokenInput.element).tokenInput(objAreasTokenInput.url, {
		method: "GET",
	  	jsonContainer : objAreasTokenInput.jsonContainer ? objAreasTokenInput.jsonContainer : null,
	  	queryParam: objAreasTokenInput.queryParam ? objAreasTokenInput.queryParam : "nome",
	  	hintText: objAreasTokenInput.hintText ? objAreasTokenInput.hintText : "Comece a digitar para buscar.",
		noResultsText: objAreasTokenInput.noResultsText ? objAreasTokenInput.noResultsText : "Nenhum item encontrado",
		searchingText: objAreasTokenInput.searchingText ? objAreasTokenInput.searchingText : "Carregando...",
		preventDuplicates: true,
		prePopulate: objAreasTokenInput.prePopulate ? objAreasTokenInput.prePopulate : null,
		propertyToSearch: objAreasTokenInput.propertyToSearch ? objAreasTokenInput.propertyToSearch : "nome",
		tokenLimit: objAreasTokenInput.tokenLimit ? objAreasTokenInput.tokenLimit : 1,
		placeholder: objAreasTokenInput.placeholder ? objAreasTokenInput.placeholder : "Digite a área...",
		onAdd: objAreasTokenInput.onAdd ? objAreasTokenInput.onAdd : function (item) {
			$("#form_area").val(item.id);
		},
		onDelete: objAreasTokenInput.onDelete ? objAreasTokenInput.onDelete : function (item) {
			$("#form_area").val('');
		}
	});
}

function diretoriasTokenInput(objDiretoriasTokenInput){
	if(!objDiretoriasTokenInput.url){
		objDiretoriasTokenInput.url = urlContext + "/rest/v1/diretoria/filtro/";
	}
	
	$(objDiretoriasTokenInput.element).tokenInput(objDiretoriasTokenInput.url, {
		method: "GET",
	  	jsonContainer : objDiretoriasTokenInput.jsonContainer ? objDiretoriasTokenInput.jsonContainer : null,
	  	queryParam: objDiretoriasTokenInput.queryParam ? objDiretoriasTokenInput.queryParam : "nome",
	  	hintText: objDiretoriasTokenInput.hintText ? objDiretoriasTokenInput.hintText : "Comece a digitar para buscar.",
		noResultsText: objDiretoriasTokenInput.noResultsText ? objDiretoriasTokenInput.noResultsText : "Nenhum item encontrado",
		searchingText: objDiretoriasTokenInput.searchingText ? objDiretoriasTokenInput.searchingText : "Carregando...",
		preventDuplicates: true,
		prePopulate: objDiretoriasTokenInput.prePopulate ? objDiretoriasTokenInput.prePopulate : null,
		propertyToSearch: objDiretoriasTokenInput.propertyToSearch ? objDiretoriasTokenInput.propertyToSearch : "nome",
		tokenLimit: objDiretoriasTokenInput.tokenLimit ? objDiretoriasTokenInput.tokenLimit : 1,
		placeholder: objDiretoriasTokenInput.placeholder ? objDiretoriasTokenInput.placeholder : "Digite a diretoria...",
		onAdd: objDiretoriasTokenInput.onAdd ? objDiretoriasTokenInput.onAdd : function (item) {
			$("#form_diretoria").val(item.id);
		},
		onDelete: objDiretoriasTokenInput.onDelete ? objDiretoriasTokenInput.onDelete : function (item) {
			$("#form_diretoria").val('');
		}
	});
}

function chapeusTokenInput(objChapeusTokenInput){
	if(!objChapeusTokenInput.url){
		objChapeusTokenInput.url = urlContext+"/ws/chapeus.htm?mode=json&form_name=form";
	}
	
	$(objChapeusTokenInput.element).tokenInput(objChapeusTokenInput.url, {
		method: "POST",
	  	jsonContainer : objChapeusTokenInput.jsonContainer ? objChapeusTokenInput.jsonContainer : null,
	  	queryParam: objChapeusTokenInput.queryParam ? objChapeusTokenInput.queryParam : "nome",
	  	hintText: objChapeusTokenInput.hintText ? objChapeusTokenInput.hintText : "Comece a digitar para buscar.",
		noResultsText: objChapeusTokenInput.noResultsText ? objChapeusTokenInput.noResultsText : "Nenhum item encontrado",
		searchingText: objChapeusTokenInput.searchingText ? objChapeusTokenInput.searchingText : "Carregando...",
		preventDuplicates: true,
		prePopulate: objChapeusTokenInput.prePopulate ? objChapeusTokenInput.prePopulate : null,
		propertyToSearch: objChapeusTokenInput.propertyToSearch ? objChapeusTokenInput.propertyToSearch : "nome",
		tokenLimit: objChapeusTokenInput.tokenLimit ? objChapeusTokenInput.tokenLimit : 1,
		placeholder: objChapeusTokenInput.placeholder ? objChapeusTokenInput.placeholder : "Digite o chapéu...",
		onAdd: objChapeusTokenInput.onAdd ? objChapeusTokenInput.onAdd : function (item) {
			$("#chapeu").val(item.id);
		},
		onDelete: objChapeusTokenInput.onDelete ? objChapeusTokenInput.onDelete : function (item) {
			$("#chapeu").val('');
		}
	});
}

function tagsTokenInput(objTagsTokenInput){
	if(!objTagsTokenInput.url){
		objTagsTokenInput.url = urlContext+"/ws/chapeus.htm?mode=json&form_name=form";
	}
	
	$(objTagsTokenInput.element).tokenInput(objTagsTokenInput.url, {
		method: "POST",
	  	jsonContainer : objTagsTokenInput.jsonContainer ? objTagsTokenInput.jsonContainer : null,
	  	queryParam: objTagsTokenInput.queryParam ? objTagsTokenInput.queryParam : "nome",
	  	hintText: objTagsTokenInput.hintText ? objTagsTokenInput.hintText : "Comece a digitar para buscar.",
		noResultsText: objTagsTokenInput.noResultsText ? objTagsTokenInput.noResultsText : "Nenhum item encontrado",
		searchingText: objTagsTokenInput.searchingText ? objTagsTokenInput.searchingText : "Carregando...",
		preventDuplicates: true,
		prePopulate: objTagsTokenInput.prePopulate ? objTagsTokenInput.prePopulate : null,
		propertyToSearch: objTagsTokenInput.propertyToSearch ? objTagsTokenInput.propertyToSearch : "nome",
		placeholder: objTagsTokenInput.placeholder ? objTagsTokenInput.placeholder : "Digite a tag...",
		onAdd: objTagsTokenInput.onAdd ? objTagsTokenInput.onAdd : function (item) {
			$("#tags").val(item.id);
		},
		onDelete: objTagsTokenInput.onDelete ? objTagsTokenInput.onDelete : function (item) {
			$("#tags").val('');
		}
	});
}

function postsRestTokenInput(objPostsRestTokenInput){
	if(!objPostsRestTokenInput.url){
		objPostsRestTokenInput.url = urlContext+"/rest/post/titulo?max=15";
	}
	
	$(objPostsRestTokenInput.element).tokenInput(objPostsRestTokenInput.url, {
		method: "GET",
	  	jsonContainer : objPostsRestTokenInput.jsonContainer ? objPostsRestTokenInput.jsonContainer : null,
	  	queryParam: objPostsRestTokenInput.queryParam ? objPostsRestTokenInput.queryParam : "titulo",
	  	hintText: objPostsRestTokenInput.hintText ? objPostsRestTokenInput.hintText : "Comece a digitar para buscar.",
		noResultsText: objPostsRestTokenInput.noResultsText ? objPostsRestTokenInput.noResultsText : "Nenhum item encontrado",
		searchingText: objPostsRestTokenInput.searchingText ? objPostsRestTokenInput.searchingText : "Carregando...",
		preventDuplicates: true,
		prePopulate: objPostsRestTokenInput.prePopulate ? objPostsRestTokenInput.prePopulate : null,
		propertyToSearch: objPostsRestTokenInput.propertyToSearch ? objPostsRestTokenInput.propertyToSearch : "titulo",
		placeholder: objPostsRestTokenInput.placeholder ? objPostsRestTokenInput.placeholder : "Digite o título do post...",
		onAdd: objPostsRestTokenInput.onAdd ? objPostsRestTokenInput.onAdd : function (item) {
			$("#post").val(item.id);
		},
		onDelete: objPostsRestTokenInput.onDelete ? objPostsRestTokenInput.onDelete : function (item) {
			$("#post").val('');
		}
	});
}