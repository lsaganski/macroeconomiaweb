function buscarSolicitacoes(texto){
	jQuery.ajax({
		type: "post",
		url: urlContext+"/ws/notificacoes.htm?mode=json&form_name=form",
		contentType: jsHttp.CONTENT_TYPE_FORM,
		data: {
			user_id: userId,
			page: 0,
			maxRows: 50,
			texto: texto,
			solicitacoes: 1
		},
		success: function(data){
			$(".listaSolicitacoes").empty();
			if(!data.solicitacoes || data.solicitacoes.length == 0) {
				$(".listaSolicitacoes").append('<li class="sbold text-center">Nenhuma solicitação "'+texto+'" foi encontrada.</li>');
			}else{
				$(".listaSolicitacoes").append(createListSolicitacoes(data.solicitacoes));
			}
			$("#buscaSolicitacoes").removeClass("ac_loading");
		}
	});	
}

function getAllSolicitacoes(){
	jQuery.ajax({
		type: "post",
		url: urlContext+"/ws/notifications.htm?mode=json&form_name=form",
		contentType: jsHttp.CONTENT_TYPE_FORM,
		data: {
			user_id: userId,
			page: 0,
			maxRows: 50,
			solicitacoes: 1
		},
		success: function(data){
			if(!data.solicitacoes || data.solicitacoes.length == 0) {
				$(".listaSolicitacoes").append('<li class="sbold text-center noNotify"><img src="'+urlContext+'/imagens/notifyBell.png" class="img-responsive" alt="Você não têm nenhuma solicitação pendente." /><p>Você não têm nenhuma solicitação pendente.</p></li>');
			}else{
				$(".listaSolicitacoes").empty().append(createListSolicitacoes(data.solicitacoes));
			}
		}
	});
}

function getSolicitacoes(idGrupo, callback){
	jQuery.ajax({
		type: "get",
		url: urlContext+"/rest/grupo/aprovar/usuarios/" + idGrupo,
	 	contentType: jsHttp.CONTENT_TYPE_FORM,
	 	success: callback
	 });
}

function putSolicitacoes(userId, grupoId, aprovar, callback){
	jQuery.ajax({
		type: "PUT",
		url: urlContext+"/rest/grupo/participar/aprovar",
		data: {
			usuarioId: userId,
			grupoId: grupoId,
			aprovar: aprovar
		},
	 	contentType: jsHttp.CONTENT_TYPE_FORM,
	 	success: callback
	});
}

/* Solicitacoes */
function createListSolicitacoes(lista){
	var conteudo = "";
	for(var i in lista){
		var l = lista[i];
		conteudo +=	'<li data-tipo="'+l.tipo+'" class="notification" data-id="'+l.id+'">';
		
			conteudo += '<div class="clearfix">'
				
				conteudo += '<div class="grupo-pic-holder">';
					
					conteudo +=	'<div class="fotoUsuario media">';
						conteudo +=	'<div class="centraliza">';
							conteudo +=	'<img src="'+ l.urlThumbUsuario +'" alt="Foto do '+l.nomeUsuario+'">';
						conteudo +=	'</div>';
					conteudo +=	'</div>';
				
					conteudo +=	'<span class="interaction-badge">';
						if(l.tipo == 'GRUPO_SOLICITAR_PARTICIPAR'){
							conteudo +=	'<img src="'+urlContext+'/imagens/notifications/notification_grupo.png" class="not-read" alt="'+l.texto+'" />';
						}else{
							conteudo +=	'<img src="'+urlContext+'/imagens/notifications/notification_user_accept.png" class="not-read" alt="'+l.texto+'" />';
						}
					conteudo += '</span>';
					
				conteudo += '</div>';
					
				conteudo += '<div class="text-link-holder">';
					conteudo +=	'<p class="notifyText">'+l.texto+'</p>';
					conteudo += '<span class="dataPost" title="'+ dateConverter(l.timestamp) +'" rel="'+ l.timestamp +'">'+l.data+'</span>';
				conteudo += '</div>';
				
			conteudo += '</div>';
			
			conteudo += '<div class="clearfix acoes-user">';
				if(l.tipo == 'GRUPO_SOLICITAR_PARTICIPAR'){
					conteudo +=	'<span class="clearfix text-right notificacoes-participar">';
						conteudo += '<a class="sbold font-blue" href="'+urlContext+'/usuario.htm?id='+l.idUsuario+'">ver perfil</a>';
						conteudo += '<a class="sbold font-green-sharp" data-idGrupo="'+ l.grupoId +'" data-idUser="'+ l.idUsuario +'" data-resposta="true">aceitar</a>';
						conteudo += '<a class="sbold font-red-mint" data-idGrupo="'+ l.grupoId +'" data-idUser="'+ l.idUsuario +'" data-resposta="false">recusar</a>';
					conteudo +=	'</span>';
				}else{
					conteudo +=	'<span class="clearfix text-right notificacoes-aceitar-amizade">';
						conteudo += '<a class="sbold font-blue" href="'+urlContext+'/usuario.htm?id='+l.idUsuario+'">ver perfil</a>';
						conteudo += '<a class="sbold font-green-sharp aceitar-amizade" data-id="'+ l.idUsuario +'" data-resposta="true">aceitar</a>';
						conteudo += '<a class="sbold font-red-mint reprovar-amizade" data-id="'+ l.idUsuario +'" data-resposta="false">recusar</a>';
					conteudo +=	'</span>';
				}
			conteudo += '</div>';
			
		conteudo +=	'</li>';
	}
	return conteudo;
}

// Mostrar a sideBox
function showSolicitacoes(obj){	
	getAllSolicitacoes();
	
	$("#sideBox-cads, #sideBox-user_grupo, #sideBox-links, #sideBox-arquivos, #sideBox-notificacoes").removeClass("in active");
	$("#sideBox-notify_solicita, #sideBox-solicitacoes").addClass("in active");
	$(".icones-header li").removeClass("active");
	$(".icones-header .li-notificacoes, #sideBox-link-solicitacoes").addClass("active");
}

$(document).ready(function(){
	
	$('.menuNotificacao, .listaNotify').on('click', '[data-tipo="USUARIO_SOLICITAR_AMIZADE"], [data-tipo="GRUPO_SOLICITAR_PARTICIPAR"]', function(e){
		$('#sideBox-link-notificacoes').removeClass('active');
		$('#sideBox-link-solicitacoes').addClass('active');
		if( $(".header-links.notificacoes").hasClass("mural-located") && ($(this).attr("data-lida") == "false") ){
			e.preventDefault();
			e.stopPropagation();
			var tipo = $(this).attr('data-tipo');
			var obj = {
				tipo: tipo
			};
			
			showSolicitacoes(obj);
			$(".menuNotificacao").fadeOut();
		} else {
			if($(this).is('li')){
				$(this).children('a').attr("href", urlContext+"/pages/mural.htm?tab=tabSolicitacoes");
			}else{
				$(this).attr("href", urlContext+"/pages/mural.htm?tab=tabSolicitacoes");
			}
		}
	});
	
	$(".listaSolicitacoes").on("click", ".notificacoes-participar a", function(e){
		e.preventDefault();
		e.stopPropagation();
		var grupoId = $(this).attr("data-idGrupo");
		var userId = $(this).attr("data-idUser");
		var resposta = $(this).attr("data-resposta");
		if(typeof $(this).attr('href') == 'undefined'){
			if(resposta == "false"){
				jConfirm("Deseja mesmo recusar a solicitação?", "Atenção.", "deletar", function(r){
					if(r){
						putSolicitacoes(userId, grupoId, resposta, function (data){
							if(data.status != "ERROR"){
								toastr.success(data.message);
								var paramsNotifications = {
			 						data: { 
			 							user_id: SESSION.USER_INFO.USER_ID
			 						}, 
			    					isBadges: true, 
			    					isClear: false, 
			    					isScroll: false,
			    					isShowTabNotification: false,
			    					isFlipAnimation: false,
			    					isSpinner: false
			    				};
			    				notifications.getNotifications(paramsNotifications);
								$(e.target).closest("li").slideUp("normal", function() {
									$(this).remove();
									var qtdLI = $(".listaSolicitacoes li").length;
									if(qtdLI == 0){
										$(".listaSolicitacoes").append('<li class="sbold text-center noNotify"><img src="'+urlContext+'/imagens/notifyBell.png" class="img-responsive" alt="Você não têm nenhuma solicitação pendente." /><p>Você não têm nenhuma solicitação pendente.</p></li>');
									}
								});
							} else {
								toastr.error(data.message);
							}
						});	
					}else{
						return false;
					}
				});
			} else {
				putSolicitacoes(userId, grupoId, resposta, function (data){
					if(data.status != "ERROR"){
						toastr.success(data.message);
						var paramsNotifications = {
	 						data: { 
	 							user_id: SESSION.USER_INFO.USER_ID
	 						}, 
	    					isBadges: true, 
	    					isClear: false, 
	    					isScroll: false,
	    					isShowTabNotification: false,
	    					isFlipAnimation: false,
	    					isSpinner: false
	    				};
	    				notifications.getNotifications(paramsNotifications);
						$(e.target).closest("li").slideUp("normal", function() {
							$(this).remove();
							var qtdLI = $(".listaSolicitacoes li").length;
							if(qtdLI == 0){
								$(".listaSolicitacoes").append('<li class="sbold text-center noNotify"><img src="'+urlContext+'/imagens/notifyBell.png" class="img-responsive" alt="Você não têm nenhuma solicitação pendente." /><p>Você não têm nenhuma solicitação pendente.</p></li>');
							}
						});
					} else {
						toastr.error(data.message);
					}
				});
			}
		}else{
			location.href = $(this).attr('href');
		}
	});
	
	$("body").on("click", ".notificacoes-aceitar-amizade a", function(e){
		var a = $(this);
		var id = $(this).attr('data-id');
		var resposta = $(this).attr('data-resposta'); 
		var obj = {
			amigoId: id,
			aprovar: resposta,
		};
		responderAmizade(obj, function(data){
			if(data.status == "OK"){
				toastr.success(data.message);
			}else{
				toastr.error(data.message);
			}
			
			var paramsNotifications = {
				data: { 
					user_id: SESSION.USER_INFO.USER_ID
				}, 
				isBadges: true, 
				isClear: false, 
				isScroll: false,
				isShowTabNotification: false,
				isFlipAnimation: false,
				isSpinner: false
			};
			notifications.getNotifications(paramsNotifications);
			
			if(!a.parent('.notificacoes-aceitar-amizade').hasClass('not-list')){
				$(e.target).closest("li").slideUp("normal", function() {
					$(this).remove();
					var qtdLI = $(".listaSolicitacoes li").length;
					if(qtdLI == 0){
						$(".listaSolicitacoes").append('<li class="sbold text-center noNotify"><img src="'+urlContext+'/imagens/notifyBell.png" class="img-responsive" alt="Você não têm nenhuma solicitação pendente." /><p>Você não têm nenhuma solicitação pendente.</p></li>');
					}
				});
			}else{
				setTimeout(function(){ location.reload(); }, 500);
			}
		});
	});
	
	
	var interval = null;
	$("#buscaSolicitacoes").keyup(function(event) {
		event.preventDefault();
		if(event.which == 32 || event.which == 18 || event.which == 9 || event.which == 13
				|| event.which == 37 || event.which == 39 || event.which == 38 
				|| event.which == 40 || event.which == 16 || event.which == 39) {
			return;
		}
		$(this).addClass("ac_loading");
		
		var texto = $(this).val();
		
		if(interval != null) {
			clearInterval(interval);
		}
		interval = setInterval(function() {
			buscarSolicitacoes(texto);
			clearInterval(interval);
		},500);
	});
});