import React, {Component} from 'react';
import ReactDOM from 'react-dom';


export default class MessageVideo extends Component {
    componentDidMount() {
        $('.video-link').fancybox();
    }
    onClickVideo(){
        let videoURL = this.props.video;
        let videoType = this.props.type;

        $('.video-link').fancybox({
            maxWidth	: 800,
			maxHeight	: 600,
			fitToView	: false,
			width		: '70%',
			height		: '70%',
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
		    content: '<div id="video_jwplayer"><div id="video_content">Carregando vídeo</div></div>', // create temp content
		    scrolling: 'no', // don't show scrolling bars in fancybox
		    helpers     : {
	            overlay : {
	                closeClick: false,
	                locked: false
	            },
	            title:  null
            },
	        afterShow: () => {
	            jwplayer("video_content").setup({ 
	                file: videoURL,
				    image: "../../js/player.swf",
				    width: '100%',
				    heigth: '100%',
				    fallback: 'false',
				    autostart: 'true',
				    controls: 'true',
				    skin: 'stormtrooper'
	            });
	        }
		});
    }
    render() { 
        let videoURL = this.props.video;
        let videoType = this.props.type;
        let isLoading = this.props.status == 'image-sending';
        const classVideoThumb = isLoading ? 'image-thumb' : 'video-thumb';
        return(
            <div className={`${classVideoThumb} ${this.props.status}`} style={{width: '100%', height: '186px'}}>
                {
                    isLoading
                    ?
                        <i className="image-sending-icon" />
                    :
                        null
                }
                <span className="video-thumb-play" />
                <a href="#" className="video-link" onClick={this.onClickVideo.bind(this)}>
                    <video width="330"><source src={videoURL} type={videoType} /></video>
                </a>
                <div className="shade" />
            </div>	
		);
    }
}