import React from 'react';
import Dialog from 'material-ui/Dialog';

import {observer, inject} from 'mobx-react';

const DrawerSectionBody = ({label}) => {
    return(
        <div className="drawer-section-body">
			<span className="textfield-static selectable-text" dir="auto">
				{label}
			</span>
        </div>
    )
}

const DrawerSectionTitle = ({label, icone}) => {
    return (
        <div className="drawer-section-title  title-underlined">
            <div className="col-main">
                {label}
                <i className={`icon-session ${icone}`}></i>
            </div>
        </div>
    )
}

@inject('infoContatoStore', 'chatStore')
@observer export default class InfoAcesso extends React.Component {
    onCloseModal = () => {
        this.props.infoContatoStore.closeChatStatusUser();
    };

    render() {
        const infoChatStatusUser = this.props.infoContatoStore.infoChatStatusUser;
        let nodeSessions = null;
        if(infoChatStatusUser != null && infoChatStatusUser.get('sessions') && infoChatStatusUser.get('sessions').size > 0) {
            nodeSessions = infoChatStatusUser.get('sessions').map((session) => {
                let actorRef = session.get('actorRef');
                if(actorRef && actorRef != null && actorRef != ''){
                    let tipoAcesso = session.get('tipo')[0].toUpperCase() + session.get('tipo').slice(1);
                    let icon = session.get('away') ? 'away' : 'online';
                    icon = infoChatStatusUser.get('chatOn') ? icon : 'offline';
                    let dataLogin = session.get('dataLogin') ? session.get('dataLogin') : "--";
                    let dataUltTransacao = session.get('dataUltTransacao') ? session.get('dataUltTransacao') : "--";
                    let dataLoginChat = session.get('dataLoginChat') ? session.get('dataLoginChat') : "--";
                    let dataUltChat = session.get('dataUltChat') ? session.get('dataUltChat') : "--";
                    return(
                        <div className="animate-enter2 drawer-section well" key={`${Math.random()}_${session.get('tipo')}`}>
                            <DrawerSectionTitle label={tipoAcesso} icone={icon} />
                            <DrawerSectionBody label={`Data Login: ${dataLogin}`} />
                            <DrawerSectionBody label={`Data Últ. Acesso: ${dataUltTransacao}`} />
                            <DrawerSectionBody label={`Data Login Chat: ${dataLoginChat}`} />
                            <DrawerSectionBody label={`Data Últ. Chat: ${dataUltChat}`} />
                        </div>
                    )
                }

                return false;
            });
        }

        return (
            <Dialog
                title="Informações de Acesso"
                modal={false}
                open={true}
                onRequestClose={this.onCloseModal}
            >
                {
                    infoChatStatusUser != null && infoChatStatusUser.get('chatOn') != false
                        ?
                        <div className="drawer-body drawer-editable">
                            {nodeSessions}
                        </div>
                        :
                        <div className="drawer-body drawer-editable">
                            <div className="animate-enter2 drawer-section well">
                                <DrawerSectionTitle label="Dados de Acesso" icone="offline" />
                                <DrawerSectionBody label={`Status: Offline`} />
                            </div>
                        </div>
                }
            </Dialog>
        );
    }
}