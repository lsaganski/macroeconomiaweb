import React, {Component} from 'react';
import {observer, inject} from 'mobx-react';
import TextInput from '../../components/TextInput';
import BtnSend from '../../components/BtnSend';
import BtnGravarAudio from '../../components/BtnGravarAudio';
import RecordAudio from '../../components/RecordAudio';
import LiveUrl from '../../components/Liveurl';
import * as StringUtils from '../../utils/StringUtils';


@inject('chatStore')
@observer export default class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {
        	isRecording: false,
			message: null,
        }
    }
	componentWillReact() {
		if(this.props.chatStore.clearInputMessage) {
			this.setState({message: null, isRecording: false});
		}
	}
	onClickAnexar() {
		$(this.refs.fileUpload).click();
	}
	onFilePreview() {
		this.props.actions.showImagePreview($(this.refs.fileUpload)[0].files[0]);
		$(this.refs.fileUpload).val("");
	}
	onChangeMessage(message) {
		this.setState({message: message});
		this.props.chatStore.getLiveUrl(message);
	}
	recordAudio(){
		if(navigator.getUserMedia || navigator.webkitGetUserMedia)
		{
			let {isRecording} = this.state;
			this.setState({isRecording: !isRecording});
		} else {
			swal(
				"Microfone Não Encontrado",
				"Você não pode gravar uma Mensagem de voz pois parece que o seu computador não tem um microfone. Tente conectar um ou, caso você tenha um conectado, tente reiniciar seu navegador.",
				"info"
			);
		}
	}
	sendMessage() {
		let message = this.state.message;
		if(message && message != "") {
			message = message.replace(/(<([^>]+)>)/ig,"");
			$(this.refs.fileUpload).val("");
			let msg = jQuery.trim(message);
			if(msg != ''){
				this.setState({message: ""});
				this.props.chatStore.sendMessage(this.props.chatStore.conversa, msg);
			}
		}
		this.clearTimeoutTyping();
	}
    clearTimeoutTyping() {
       clearTimeout(this.timeoutTyping);
       delete this.timeoutTyping; 
    }
	sendTypingStatus() {
		if(!this.timeoutTyping) {
			this.timeoutTyping = setTimeout(() => {
				this.props.chatStore.sendTypingStatus(this.props.chatStore.conversa);
				this.clearTimeoutTyping();
			}, 5000);
		}
	}
    onComplete(blobAudio) {
        this.setState({isRecording: false});
        let nameFile = `${new Date().getTime()}_audio.wav`;
        let audio = new File([blobAudio], nameFile);
        const {chatStore} = this.props;
        const conversa = chatStore.conversa;
        audio.preview = URL.createObjectURL(blobAudio);
        chatStore.sendFile(conversa, audio);
    }
    onError(message) {
        console.log(message);
    }
    onCancel() {
        this.setState({isRecording: false});
    }
    onClickCloseLiveUrl() {
    	this.props.chatStore.clearLiveUrl();
    }
    render() {
    	let inputContainerClass = this.state.isRecording ? 'disabled' : '';
    	let {show, chatStore} = this.props;
    	let isLiveUrl = chatStore.liveUrl != null;
        return(
        	<footer className="pane-footer pane-chat-footer">
        		{
					isLiveUrl
					?
						<LiveUrl
							onClose={this.onClickCloseLiveUrl.bind(this)}
							title={this.props.chatStore.liveUrl.title}
							img={this.props.chatStore.liveUrl.img}
							description={this.props.chatStore.liveUrl.description}
						/>
					:
						null
				}
				<div className="block-compose">
					<div className={`input-container ${inputContainerClass}`}>
						<TextInput ref="textInput"
							onChange={this.onChangeMessage.bind(this)}
							sendTypingStatus={this.sendTypingStatus.bind(this)}
							sendMessage={this.sendMessage.bind(this)}
							message={this.props.chatStore.message}
							focus={this.props.chatStore.focusInputMessage}
							clear={this.props.chatStore.clearInputMessage}
						/>
					</div>
					{
						this.state.isRecording
						?
							<RecordAudio
								onComplete={this.onComplete.bind(this)}
								onError={this.onError.bind(this)}
								onCancel={this.onCancel.bind(this)}
							/>
						: null
					}
					<BtnSend onClick={this.sendMessage.bind(this)} />
					{
						/**
						this.state.message != null && this.state.message != ""
						? <BtnSend onClick={this.sendMessage.bind(this)} />
						: null
						(this.state.message == null || this.state.message == "") && !this.state.isRecording
						? <BtnGravarAudio onClick={this.recordAudio.bind(this)} />
						: null
						*/
					}
				</div>
			</footer>
        )
    }
}