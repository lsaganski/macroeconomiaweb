import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {observer, inject} from 'mobx-react';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

import * as FileUtils from '../../utils/FileUtils';
import * as StringUtils from '../../utils/StringUtils';
import {MessageStatus} from '../../stores/chatStore';
import {ImageMessageStatus} from '../../stores/chatStore';
import MessageImage from '../../components/messages/MessageImage';
import MessageVideo from '../../components/messages/MessageVideo';
import MessageAudio from '../../components/messages/MessageAudio';
import MessageFile from '../../components/messages/MessageFile';
import MessageText from '../../components/messages/MessageText';
import LocationMessage from '../../components/messages/MessageLocation';
import MessageCards from '../../components/messages/MessageCards';
import MessageButtons from '../../components/messages/MessageButtons';
import CopyToClipBoard from '../../components/menu-item/CopyToClipBoard';


const ContextMenu = ({show, open, anchorEl, onOpen, onClose, className, children}) => {
    if(show) {
        return(
            <div className={className} style={{opacity: '1'}}>
                <div className="js-context-icon context-icon" style={{transform: 'translateX(0px)'}} onClick={onOpen}>
                    <Popover
                        open={open}
                        anchorEl={anchorEl}
                        anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                        targetOrigin={{horizontal: 'left', vertical: 'top'}}
                        onRequestClose={onClose}
                    >
                        {children}
                    </Popover>
                </div>
            </div>
        )
    }
    return <span></span>

}

const isMessageMedia = (message) => {
    let location = message.get('location');
    const arquivos = message.get('arquivos');
    if(arquivos) {
        let arquivo = message.get('arquivos').get(0);
        let ext = arquivo.get('extensao').toLowerCase();
        if(FileUtils.isAudio(ext)) {
            return false;
        }
        return true;
    } else if(location && location.get('lat') != 0 && location.get('lng') != 0) {
        return true;
    }
    return false;
}

const getNodeMessage = (message, chatStore) => {
    let node = null;
    let location = message.get('location');
    let cards = message.get('cards');
    let buttons = message.get('buttons');
    if(message.get('arquivos') && message.get('arquivos').size > 0) {
        let arquivo = message.get('arquivos').get(0);
        let ext = arquivo.get('extensao').toLowerCase();
        let status, thumb;
        let texto = message.get('msg') ? message.get('msg') : '';
        if (chatStore.isLida(message) || chatStore.isEntregue(message) || chatStore.isEnviada(message)) {
            status = ImageMessageStatus['SENT'];
        } else {
            status = ImageMessageStatus['SENDING'];
        }
        if (FileUtils.isImage(ext)) {
            if(FileUtils.isGif(ext)){
                thumb = arquivo.get('url');
            }else{
                if(arquivo.get('urlThumb')){
                    thumb = arquivo.get('urlThumb');
                }else if(arquivo.get('thumbs') && arquivo.get('thumbs').get(0) && arquivo.get('thumbs').get(0).get('url')){
                    thumb = arquivo.get('thumbs').get(0).get('url');
                }else{
                    thumb = arquivo.get('url');
                }
            }
            node = <MessageImage conversaId={chatStore.conversa.get('id')} image={arquivo.get('url')} thumb={thumb} status={status} text={texto} />
        } else if (FileUtils.isVideo(ext)) {
            node = <MessageVideo video={arquivo.get('url')} type={`video/${ext}`} status={status}/>
        } else if (FileUtils.isAudio(ext)) {
            node = <MessageAudio messageId={message.get('id')} audio={arquivo.get('url')} type={`audio/${ext}`} status={status}
                                 from={message.get('fromUrlFotoThumb')}/>
        } else if (FileUtils.isDoc(ext) || FileUtils.isProgram(ext) || FileUtils.isExecutable(ext) || FileUtils.isCompressed(ext)) {
            node =
                <MessageFile file={arquivo.get('url')} ext={ext} name={arquivo.get('file')} msgId={message.get('id')} status={status} />
        }
    }else if(location && location.get('lat') != 0 && location.get('lng') != 0) {
        node = <LocationMessage lat={location.get('lat')} lng={location.get("lng")} chave={chatStore.chaveGoogleMap} />
    }else if(cards && cards.size > 0){
        node = <MessageCards cards={cards} message={message} />
    }else if(buttons && buttons.size > 0){
        node = <MessageButtons buttons={buttons} message={message} />
    }else{
        node = <MessageText text={StringUtils.parseURL(message.get('msg'))} />
    }
    return node;
}

const getDataMessage = (message) => {
    return message.get('dataUpdatedString') ? message.get('dataUpdatedString') : message.get('dataWeb');
}


const getBubbleClass = (message, hasAuthor) => {
    let location = message.get('location');
    let bubbleClass = hasAuthor ? 'bubble has-author bubble-image' : 'bubble bubble-image';
    if(message.get('arquivos') && message.get('arquivos').size > 0) {
        let arquivo = message.get('arquivos').get(0);
        let ext = arquivo.get('extensao').toLowerCase();
        let texto = message.get('msg') ? message.get('msg') : '';

        if (FileUtils.isVideo(ext)) {
            bubbleClass += ' bubble-attach bubble-video';
        } else if (FileUtils.isAudio(ext)) {
            bubbleClass += ' bubble-attach bubble-audio';
        } else if(FileUtils.isImage(ext) && texto != ''){
            bubbleClass += ' bubble-has-text';
        }
    } else if(location && location.get('lat') != 0 && location.get('lng') != 0) {
        return bubbleClass;
    }else{
        bubbleClass = hasAuthor ? 'bubble bubble-text has-author' : 'bubble bubble-text';
    }
    return bubbleClass;
}


@inject('chatStore', 'userInfoStore', 'infoMessageStore')
@observer export default class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openPopoverUser: false,
            contextMenu: false
        };
    }
    shouldComponentUpdate(nextProps, nextStates) {
        let isUpdateComponent = nextProps.message !== this.props.message || nextProps.continuation !== this.props.continuation;
        isUpdateComponent = isUpdateComponent || nextStates.openPopoverUser != this.state.openPopoverUser || this.state.contextMenu != nextStates.contextMenu;
        return isUpdateComponent;
    }
    handleTouchTap(event) {
       event.preventDefault();
       this.setState({
         openPopoverUser: true,
         anchorEl: event.currentTarget,
       });
    }
    handleRequestClose() {
        this.setState({
         openPopoverUser: false,
         contextMenu:false,
       });
    }
    showContextMenu() {
        this.setState({
             contextMenu: true
         });
    }
    hideContextMenu() {
        if(this.state.openPopoverUser == false) {
              this.setState({
                  contextMenu: false
              });
          }
    }
    onClickInfoMessage() {
        this.setState({
            openPopoverUser: false,
            contextMenu:false,
        });
        this.props.infoMessageStore.onOpenDrawerInfoMessage(this.props.chatStore.conversa, this.props.message);
    }
    onClickForward() {
        this.setState({
            openPopoverUser: false,
            contextMenu:false,
        });
        this.props.chatStore.messageForward = this.props.message;
    }
    render() {
        const chatStore = this.props.chatStore;
        const message = this.props.message;
        const isMyMessage  = chatStore.isMyMessage(message);
        const isGroup = message.get('isGroup');
        const classMedia = isMessageMedia(message) ? 'context-in context context-media' : 'context-in context';
        const hasAuthor = !isMyMessage && isGroup;
        const bubbleClass = getBubbleClass(message, hasAuthor);
        const nodeMessage = getNodeMessage(message, chatStore);
        const classMsg = this.props.continuation ? 'msg msg-continuation' : 'msg';
        let classMessage = 'message message-chat tail message-chat';
        classMessage += isMyMessage ? ' message-out ' : ' message-in ';
        classMessage += hasAuthor ? ' has-author' : '';

        let classStatusMessage = '';
        if(isMyMessage) {
            classStatusMessage = MessageStatus['SENDING'];
            if(chatStore.isLida(message)) {
                classStatusMessage = MessageStatus['READ'];
            } else if(chatStore.isEntregue(message)) {
                classStatusMessage = MessageStatus['DELIVERED'];
            } else if(chatStore.isEnviada(message)) {
                classStatusMessage = MessageStatus['SENT'];
            }
        }

        let nodeAuthor = null;
        if(hasAuthor) {
            nodeAuthor = <h3 className={`message-author ${chatStore.getColorUserName(message)} title-number`}>
                    <span className="hidden-token">
                        <span className="number text-clickable">{message.get('fromNome')}</span>
                    </span>
                </h3>
        }

        return(
            <div className={classMsg}
                onMouseOver={this.showContextMenu.bind(this)}
                onMouseLeave={this.hideContextMenu.bind(this)}>
				<div className={classMessage}>
					<div className={bubbleClass}>
                        {nodeAuthor}
						{nodeMessage}
						<div className="message-meta text-clickable">
							<span className="hidden-token">
								<span className="message-datetime">{chatStore.formatDateMsg(message.get('data'))}</span>
							</span>
							<span className={classStatusMessage}></span>
						</div>
					</div>
                    {this.props.contextMenu !== false
                        ?
                            <ContextMenu
                                onOpen={this.handleTouchTap.bind(this)}
                                onClose={this.handleRequestClose.bind(this)}
                                show={this.state.contextMenu}
                                anchorEl={this.state.anchorEl}
                                open={this.state.openPopoverUser}
                                className={classMedia}
                            >
                                <Menu>
                                    <MenuItem onTouchTap={this.onClickInfoMessage.bind(this)}>
                                        Dados da Mensagem
                                    </MenuItem>
                                    <MenuItem onTouchTap={this.onClickForward.bind(this)}>
                                        Encaminhar mensagem
                                    </MenuItem>
                                    {message.get('arquivos') && message.get('arquivos').size > 0 && message.get('arquivos').get(0).get('url')
                                        ?
                                            <CopyToClipBoard
                                                valueToCopy={message.get('arquivos').get(0).get('url')}
                                                label="Copiar link do arquivo"
                                                onCopy={() => {
                                                    chatStore.onShowSnackBar(`Link ${message.get('arquivos').get(0).get('url')} copiado`);
                                                }}
                                            />
                                        :
                                            null
                                    }
                                </Menu>
                            </ContextMenu>
                        : null
                    }
                </div>
			</div>
        );
    }
}