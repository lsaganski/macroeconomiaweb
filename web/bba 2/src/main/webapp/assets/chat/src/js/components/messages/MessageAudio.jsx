import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import * as moment from 'moment';
import * as StringUtils from '../../utils/StringUtils';
import Avatar from '../../components/Avatar';
import Sound from '../Sound';

export default class MessageAudio extends Component {
    constructor(props) {
        super(props);
        this.state = {
            playSound: false,
            duration: null,
            audioTrack: 0
        };
    }
    componentDidMount(){
        let audioNode = ReactDOM.findDOMNode(this.refs.sound.refs.playAudio);
        audioNode.addEventListener("loadedmetadata", () => {
            this.setState({duration: audioNode.duration});
        });

        audioNode.addEventListener("ended", () => {
            setTimeout(() => {
                let progressNode = ReactDOM.findDOMNode(this.refs.progress);
                progressNode.style.width = `${0}%`;
                this.setState({playSound: false, audioTrack: 0});
            }, 0);
        })
    }
    onTimeUpdate(e){
        let currentTime = e.target.currentTime;
        let audioTrack = (currentTime / this.state.duration) * 100;
        if(audioTrack <= 100 && !isNaN(audioTrack)) {
            this.setState({audioTrack: audioTrack});
            let progressNode = ReactDOM.findDOMNode(this.refs.progress);
            progressNode.style.width = `${audioTrack}%`;
        }
    }
    onClickPlay(e){
        this.setState({playSound: !this.state.playSound});
    }
    getDuration() {
        if(this.state.duration != null && !this.state.playSound) {
            let duration = moment.duration(this.state.duration, "seconds");
            let hours = duration.hours();
            let time = '';
            if (hours > 0) { time = hours + ":" ; }
            time = time + duration.minutes() + ":" + StringUtils.pad(duration.seconds());
            return time;
        }
        return "";
    }
    onChangeTranck(e) {
        let value = e.target.value;
        this.setState({audioTrack: value});
        let progressNode = ReactDOM.findDOMNode(this.refs.progress);
        let audio = ReactDOM.findDOMNode(this.refs.sound.refs.playAudio);
        progressNode.style.width = `${value}%`;
        let duration = Math.floor(audio.duration);
        audio.currentTime = (value * duration) / 100;
    }
    render() {
        let classBtnPlay = this.state.playSound ? 'icon-audio-pause' : 'icon-audio-play';
        let isLoading = this.props.status == 'image-sending';
        return(
            <div className="wrapper">
                <div className="preview">
                    <Avatar small={true} src={this.props.from} />
                    <span className="meta meta-audio">
                        <span  className="hidden-token">{this.getDuration()}</span>
                    </span>
                </div>
                <div className="contents">
                    <div className="text no-author">
                        <div className="audio">
                            <div className="audio-controls">
                                <div className={`image-thumb ${this.props.status}`} style={{backgroundColor: 'transparent'}}>
                                    {
                                        isLoading
                                        ?
                                            <i className="image-sending-icon" style={{backgroundPosition: '-2845px -2900px', height: '20px', width: '20px', marginTop: 'calc(43% - 15px)'}} />
                                        :
                                            null
                                    }
                                    <a href={this.props.image} className="audio-link">
                                        <button onClick={this.onClickPlay.bind(this)} className={`btn-audio icon ${classBtnPlay}`} />
                                    </a>
                                </div>
                            </div>
                            <div className="audio-body">
                                <div className="audio-track-container">
                                    <span ref="progress" className="audio-progress" />
                                    <input
                                        value={this.state.audioTrack}
                                        ref="audioTrack"
                                        type="range" min="0"
                                        max="100"
                                        className="audio-track"
                                        onChange={this.onChangeTranck.bind(this)}
                                    />
                                </div>
                                <Sound
                                    play={this.state.playSound}
                                    onTimeUpdate={this.onTimeUpdate.bind(this)}
                                    src={this.props.audio}
                                    type={this.props.type}
                                    ref="sound"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		);
    }
}