import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import {observer, inject} from 'mobx-react';
import MediaImagem from '../../components/media/Imagem';
import MediaAudio from '../../components/media/Audio';
import MediaVideo from '../../components/media/Video';
import MediaDocument from '../../components/media/Document';
import MediaCard from '../../components/media/Card';
import MediaLocation from '../../components/media/Location';
import MediaButtons from '../../components/media/Buttons';
import * as FileUtils from '../../utils/FileUtils';

import {MessageStatus} from '../../stores/chatStore';

@inject('chatStore')
@observer export default class LastMessage extends Component {
    render() {
        const chatStore = this.props.chatStore;
        const conversa = this.props.conversa;
        const hasFile = chatStore.conversaHasFile(conversa);
        const hasCard = chatStore.conversaHasCard(conversa);
        const hasLocation = chatStore.conversaHasLocation(conversa);
        const hasButtons = chatStore.conversaHasButtons(conversa);

        let iconFile = null;
        let message = null;

        if(hasFile) {
            const arquivos = conversa.get('arquivos');
            const ext = arquivos.get(0).get('extensao').toLowerCase();
            if(FileUtils.isImage(ext)) {
                iconFile = <MediaImagem />
                message = 'Foto'
            } else if(FileUtils.isVideo(ext)) {
                iconFile = <MediaVideo />
                message = 'Vídeo'
            } else if(FileUtils.isAudio(ext)) {
                iconFile = <MediaAudio />
                message = 'Áudio'
            } else if(FileUtils.isDoc(ext) || FileUtils.isProgram(ext) || FileUtils.isExecutable(ext) || FileUtils.isCompressed(ext)) {
                iconFile = <MediaDocument />
                message = 'Arquivo'
            }
        } else if(hasCard) {
            iconFile = <MediaCard />
            message = 'Card'
        } else if(hasLocation){
            iconFile = <MediaLocation />
            message = 'Localização'
        } else if(hasButtons){
            iconFile = <MediaButtons />
            message = 'Escolha uma ação'
        } else {
            message = conversa.get('msg');
        }
        
        let fromNome = null;
        if(conversa.get('isGroup')) {
            const from = chatStore.getFromUserConversa(conversa);
            if(from != null) {
                fromNome = `${from.get('nome')}:&nbsp;`;
            }
        }

        let classStatusMessage = '';
        if(chatStore.isMyMessage(conversa)) {
            classStatusMessage = MessageStatus['SENDING'];
            if(chatStore.isLida(conversa)) {
                classStatusMessage = MessageStatus['READ'];
            } else if(chatStore.isEntregue(conversa)) {
                classStatusMessage = MessageStatus['DELIVERED'];
            } else if(chatStore.isEnviada(conversa)) {
                classStatusMessage = MessageStatus['SENT'];
            }
        }
        if(message || iconFile) {
            return(
                <div className="chat-status ellipsify">
                    {message || iconFile ? <span className={classStatusMessage}></span> : null}
                    <span dangerouslySetInnerHTML={{__html: fromNome}}></span>{iconFile}
                    <span dangerouslySetInnerHTML={{__html: message}}></span>
                </div>
            )
        }
        return(<div className="chat-status ellipsify"></div>)
    }
}