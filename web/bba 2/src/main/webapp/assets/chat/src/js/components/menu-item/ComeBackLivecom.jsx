import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import IconButton from 'material-ui/IconButton/IconButton';
import HomeIcon from 'material-ui/svg-icons/action/home';
import {white} from 'material-ui/styles/colors';

export default class ComeBackLivecom extends Component {
    render() {
        return (
            <div className="menu-item">
                <IconButton 
                    onTouchTap={this.props.onClick} 
                    tooltip="Voltar ao Livecom">
                    <HomeIcon color={white} />
                </IconButton>
            </div>
        )
    }
}