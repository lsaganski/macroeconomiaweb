import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {observer, inject} from 'mobx-react';
import DadosUsuario from './DadosUsuario';
import DadosGrupo from './DadosGrupo';

@inject('infoContatoStore') 
@observer export default class DrawerInfoContato extends Component {
	render() {
		const isGroup = this.props.infoContatoStore.isGroup;
		return isGroup ? <DadosGrupo /> : <DadosUsuario />
	}
}