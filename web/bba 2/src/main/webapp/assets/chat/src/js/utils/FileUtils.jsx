export const FILE_EXTENSIONS = {
    image: {
        jpg: true,
        jpeg: true,
        gif: true,
        png: true,
        bmp: true,
        ico: true,
        webp: true,
        svg: true
    },
    gif: { gif: true },
    png: { png: true },
    pdf: { pdf: true },
    video: {
        mp4: true,
        mpeg: true,
        mov: true,
        avi: true,
        wmv: true
    },
    audio: {
        '3gp': true,
        ogg: true,
        mp3: true,
        midi: true,
        wav: true,
        m4a: true
    },
    docs: {
        txt: true,
        rtf: true,
        doc: true,
        docx: true,
        log: true,
        odt: true,
        mpp: true,
        
        csv: true,
        xls: true,
        xlsx: true,
        
        ppt: true,
        pptx: true,
        pps: true,
        ppsx: true,
        
        pdf: true
    },
    compressed: {
        rar: true,
        zip: true,
        tar: true,
        gz: true,
        '7zip': true
    },
    programs: {
        jmx: true,
        json: true,
        
        sql: true,
        
        conf: true,
        
        html: true,
        htm: true,
        xml: true,
        
        css: true,
        scss: true,
        sass: true,
        styl: true,
        
        js: true,
        jsx: true,
        ejs: true,
        
        php: true,
        asp: true,
        aspx: true,
        
        cpp: true,
        
        rake: true,
        rb: true,
        
        java: true,
        jsp: true,
        jspa: true,
        
        kt: true,
        gantter: true,
        
        swift: true,
        xib: true,
        plist: true
    },
    executable: {
        apk: true,
        cdr: true,
        ipa: true,
        indd: true,
        ai: true,
        eps: true,
        psd: true,
        raw: true,
        cer: true,
        p12: true,
        postman_collection: true
    }
}

export function getExtension(filename) {
    const re = /(?:\.([^.]+))?$/;
    return re.exec(filename.toLowerCase())[1];
}

export function isImage(filename) {
    let ext = getExtension(filename);
    ext = ext ? ext : filename;
    return FILE_EXTENSIONS.image[ext] ? true : false;
}

export function isGif(filename) {
    let ext = getExtension(filename);
    ext = ext ? ext : filename;
    return FILE_EXTENSIONS.gif[ext] ? true : false;
}

export function isVideo(filename) {
    let ext = getExtension(filename);
    ext = ext ? ext : filename;
    return FILE_EXTENSIONS.video[ext] ? true : false;
}

export function isAudio(filename) {
    let ext = getExtension(filename);
    ext = ext ? ext : filename;
    return FILE_EXTENSIONS.audio[ext] ? true : false;
}

export function isExecutable(filename) {
    let ext = getExtension(filename);
    ext = ext ? ext : filename;
    return FILE_EXTENSIONS.executable[ext] ? true : false;
}

export function isCompressed(filename) {
    let ext = getExtension(filename);
    ext = ext ? ext : filename;
    return FILE_EXTENSIONS.compressed[ext] ? true : false;
}

export function isDoc(filename) {
    let ext = getExtension(filename);
    ext = ext ? ext : filename;
    return FILE_EXTENSIONS.docs[ext] ? true : false;
}

export function isProgram(filename) {
    let ext = getExtension(filename);
    ext = ext ? ext : filename;
    return FILE_EXTENSIONS.programs[ext] ? true : false;
}

export function toBinaryString(file) {
    let reader = new FileReader();
    let deferred = $.Deferred();

    reader.onload = function(event) {
        deferred.resolve(event.target.result);
    };

    reader.onerror = function() {
        deferred.reject(this);
    };

    reader.readAsBinaryString(file);

    return deferred.promise(); 
}


export function toBase64(file, callback) {
    let reader = new FileReader();
    reader.onload = function (e) {
        callback(e.target.result);
    }
    reader.readAsDataURL(file);
}

export function encode(string) {
    let encode = encodeURI(btoa(string));
    return encode;
}

export function SaveToDisk(fileURL, fileName, callback) {
    if (!window.ActiveXObject) {
        // not IE
        var save = document.createElement('a');
        save.href = fileURL;
        save.target = '_blank';
        save.download = fileName || 'unknown';

        var evt = new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': false
        });
        save.dispatchEvent(evt);

        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    } else if ( !! window.ActiveXObject && document.execCommand)     {
        // IE < 11
        var _window = window.open(fileURL, '_blank');
        _window.document.close();
        _window.document.execCommand('SaveAs', true, fileName || fileURL)
        _window.close();
    }
    
    if(callback){
        callback();
    }
}