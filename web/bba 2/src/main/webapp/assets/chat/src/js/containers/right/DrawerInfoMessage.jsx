import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {observer, inject} from 'mobx-react';
import Message from '../center/Message';
import InfoMessageUsuario from '../modal/InfoMessageUsuario';
import InfoMessageGrupo from '../modal/InfoMessageGrupo';

@inject('infoMessageStore', 'userInfoStore')
@observer export default class DrawrInfoMessage extends Component {
	onClickClose() {
		this.props.infoMessageStore.onCloseDrawerInfoMessage();
	}
	render() {
		const isGroup = this.props.infoMessageStore.isGroup;
		return (
			<div className="drawer drawer-info">
				<header className="pane-header">
					<div className="header-close">
						<button onClick={this.onClickClose.bind(this)}>
							<span className="icon icon-x"></span>
						</button>
					</div>
					<div className="header-body">
						<div className="header-main">
							<div className="header-title">Dados da mensagem</div>
						</div>
					</div>
				</header>
				<div className="pane-preview-wrapper pane-preview-wrapper-bg">
					<div className="pane-preview">
						<div className="pane-chat-tile"></div>
						<Message contextMenu={false} message={this.props.infoMessageStore.message} />
					</div>
				</div>
				{isGroup 
					? <InfoMessageGrupo statusUsersMessage={this.props.infoMessageStore.statusUsersMessage} message={this.props.infoMessageStore.message} /> 
					: <InfoMessageUsuario message={this.props.infoMessageStore.message} />}
			</div>
		);
	}
}