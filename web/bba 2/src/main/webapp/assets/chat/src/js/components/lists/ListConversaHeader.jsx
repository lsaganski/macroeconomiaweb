import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItemMaterial from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconButton from 'material-ui/IconButton/IconButton';
import SpeakerNotes from 'material-ui/svg-icons/action/speaker-notes';
import HomeIcon from 'material-ui/svg-icons/action/home';
import AssignmentIcon from 'material-ui/svg-icons/action/assignment';
import AssessmentIcon from 'material-ui/svg-icons/action/assessment';
import {grey700, blue500} from 'material-ui/styles/colors';

import Avatar from '../Avatar';
import ListConversaControl from './ListConversaControl';
import ComeBackLivecom from '../menu-item/ComeBackLivecom';
import NovaConversa from '../menu-item/NovaConversa';


export default class ListConversaHeader extends Component {
    render() {
        return(
            <header className="pane-header pane-list-header">
                {this.props.children}
            </header>
        );
    }
}