import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import RefreshIndicator from 'material-ui/RefreshIndicator';

const style = {
  refresh: {
    display: 'inline-block',
    position: 'relative',
    boxShadow: 'none'
  },
};


export default class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: ''
        }
    }
	componentWillReceiveProps(props) {
		if(props.clearSearch && this.state.message != '') {
			this.clearSearch();
		}

		if(props.focus){
			this.setFocus();
		}
	}
    componentDidMount() {
		let input = ReactDOM.findDOMNode(this.refs.searchInput);
		input.addEventListener('focusout', this.onFocusout.bind(this));
		
		if(this.props.focus){
			this.setFocus();
		}
	}
	setFocus(){
		let input = ReactDOM.findDOMNode(this.refs.searchInput);
		setTimeout(() => {
			$(input).click();
			$(input).focus();
		}, 30);
	}
    onFocusout(event) {
		if(this.state.message == "") {
            let input = ReactDOM.findDOMNode(this.refs.searchInput);
			$(input).closest('.subheader-search').removeClass('active');
		}
	}
	onClick(event) {
		let input = ReactDOM.findDOMNode(this.refs.searchInput);
		$(input).closest('.subheader-search').addClass('active');
	}
	onKeyUp(event) {
		let input = ReactDOM.findDOMNode(this.refs.searchInput);
		if(this.state.message != "") {
			$(input).closest('.subheader-search').addClass('active');
		}
		this.clearTimeout(this.timeout);
		this.timeout = setTimeout(() => {
			this.props.onSearch(this.state.message);
		}, 200);
	}
	clearTimeout() {
		if(this.timeout) {
			clearTimeout(this.timeout);
			delete this.timeout;
		}
	}
    clearSearch() {
	    this.setState({message: ''});
        let input = ReactDOM.findDOMNode(this.refs.searchInput);
        this.props.onSearch("");
        $(input).closest('.subheader-search').removeClass('active');
    }
    onChangeValue(e) {
        this.setState({message: e.target.value});
    }
    render() {
        return(
            <div className="search-container" ref="search">
				<div className="subheader-search">
					<button className="icon icon-search-morph">
						<div className="icon icon-back-blue"></div>
						<div className="icon icon-search" onClick={this.clearSearch.bind(this)}></div>
					</button>
					<div className="input-placeholder">{this.props.placeholder}</div>
					<label htmlFor="input-chatlist-search" className="cont-input-search">
						<input ref="searchInput" 
                            onClick={this.onClick.bind(this)} 
                            onKeyUp={this.onKeyUp.bind(this)}
							type="text"
                               value={this.state.message}
                               onChange={this.onChangeValue.bind(this)}
							className="input input-search"
							data-tab="2"
							dir="auto"
							title={this.props.placeholder} />
							{this.props.spinner ?
								<RefreshIndicator
			                      size={30}
			                      left={50}
			                      top={0}
			                      status="loading"
			                      style={style.refresh}
			                    />
							: <span></span>}
					</label>
				</div>
			</div>
        )
    }
}