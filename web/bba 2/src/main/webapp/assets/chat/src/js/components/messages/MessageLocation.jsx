import React, {Component} from 'react';



export default class MessageLocation extends Component {
    onClickLocation(e){
        e.preventDefault();
        let target = $(e.target).closest('a');
        let link = $(target).attr('href');
        window.open(link, 'cht-loc');
    }
    render() {
        const {lng, lat, chave} = this.props;
        let latLng = `${lat},${lng}`;
        let link = `https://maps.google.com/maps?q=${lat}%2C${lng}&z=17&hl=pt-BR`;
        let src = `https://maps.googleapis.com/maps/api/staticmap?center=${latLng}&zoom=13&&zoom=14&size=270x200&scale=1&language=pt-BR&markers=color%3Ared%%7C${latLng}&key=${chave}`;
        return(
            <div className={`image-thumb ${this.props.status}`} style={{width: '100%', height: '186px'}}>
                <a href={link} className="location-link" onClick={this.onClickLocation.bind(this)}>
                    <img src={src} className="image-thumb-body" style={{width:'330px'}} />
                </a>
                <div className="shade"></div>
            </div>
        );
    }
}