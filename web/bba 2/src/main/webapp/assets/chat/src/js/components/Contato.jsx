import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Avatar from './Avatar';

export default class Contato extends Component {
	onClick(e) {
		if(this.props.onClick) {
			this.props.onClick(this.props.contato, this.props.isGroup);
		}
	}
	render() {
		return (
			<div className="chat-drag-cover" onClick={this.onClick.bind(this)}>
				<div className="chat">
					<Avatar src={this.props.avatar} />
					<div className="chat-body">
						<div className="chat-main">
							<div className="chat-title">
								<span className="text-title">{this.props.contato.get('nome')}</span>
							</div>
							{this.props.contato.get("adminChat") ? <div className="chat-meta"><div className="chat-marker chat-marker-admin">Admin do Grupo</div></div> : null}
						</div>
						<div className="chat-secondary">
							<div className="chat-status">
								<span className="emojitext ellipsify" dir="ltr"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}