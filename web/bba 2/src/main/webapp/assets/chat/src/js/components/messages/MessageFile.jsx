import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import FlatButton from 'material-ui/FlatButton';

import * as FileUtils from '../../utils/FileUtils';



const getContent = (vh, fileUrl, msgId) => {
    return (
        `<div class="fancybox-iframe">
            <iframe 
                class="mfp-iframe medium loading" 
                height="${vh}" src="${fileUrl}">Carregando...
            </iframe>
            <div class="fancybox-buttons">
                <button class="btn green-sharp ${msgId}-download-file">Download</button>
            </div>
        </div>`
    )
}

export default class MessageFile extends Component {
    componentDidMount() {
        const {msgId} = this.props;
        let {file, name, ext} = this.props;

        let vh = $(window).height() - 150;
        let fileUrl;

        if(FileUtils.isDoc(ext) || FileUtils.isProgram(ext)){
            fileUrl = "https://docs.google.com/gview?url="+file+"&embedded=true"
        }else{
            fileUrl = file;
        }
        
        if(FileUtils.isDoc(ext) || FileUtils.isProgram(ext)){
            $(this.refs.fileLink).fancybox({
                fitToView	: true,
                wrapCSS: 'fancybox-file',
                openEffect	: 'elastic',
                closeEffect	: 'elastic',
                content: getContent(vh, fileUrl, msgId),
                scrolling: 'no', // don't show scrolling bars in fancybox
                helpers     : {
                    overlay : {
                        closeClick: false,
                        locked: false
                    },
                    title:  null
                }
            });
        }else{
            $(this.refs.fileLink).click(() => {
                jQuery.fancybox.close();
                window.open(fileUrl, 'lvcm-file');
            });
        }

        jQuery('body').on('click', `.${msgId}-download-file`, (event) => {
            let callback = () => {
                jQuery.fancybox.close();
            }

            FileUtils.SaveToDisk(file, name, callback);
        });
    }
    render() {
        let {file, name, ext} = this.props;
        let isLoading = this.props.status == 'image-sending';

        return(
            <div className={`image-thumb ${this.props.status}`} style={{width: '100%', height: '186px'}}>
                {
                    isLoading
                        ?
                            <i className="image-sending-icon" />
                        :
                            null
                }
                <a href={this.props.image} className="file-link" ref="fileLink" style={{width: '330px'}}>
                	<i className={`image-thumb-body file-extensions ${ext}`} />
                    <span className="file-name">{name}</span>
                    <span className="file-ext">{ext}</span>
                </a>
                <div className="shade" />
            </div>
        );
    }
}