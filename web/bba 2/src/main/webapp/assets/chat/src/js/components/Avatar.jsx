import React, {Component} from 'react';
import ReactDOM from 'react-dom';


export default class Avatar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            src: this.props.src,
            avatarImgClass: "avatar-image spinner",
            avatarClass: '',
        };
    }
    componentWillReceiveProps (nextProps) {
        this.state = {
            src: nextProps.src,
            avatarImgClass: "avatar-image is-loaded",
            avatarClass: ''
        };
    }
    onError() {
        this.setState({
            src: null,
            avatarImgClass: "avatar-image is-error",
            avatarClass: ''
        });
    }
    onLoad() {
        this.setState({
            avatarImgClass: "avatar-image is-loaded",
            avatarClass: 'is-loaded'
        });
    }
    render() {
        const {away, online} = this.props;
        let classImg = 'medium';
        let statusChat = online && !away ? "statusChat online" : "";
        statusChat = away ? "statusChat away" : statusChat;
        classImg = this.props.small ? 'small' : classImg;
        classImg = this.props.large ? 'large' : classImg;
        return(
            <div className="chat-avatar">
                <div className={`avatar icon-user-default ${statusChat} ${classImg} ${this.state.avatarClass}`}>
                    <img 
                        onLoad={this.onLoad.bind(this)} 
                        onError={this.onError.bind(this)}
                        className={this.state.avatarImgClass} src={this.state.src}
                    />
                </div>
            </div>
        );
    }
}