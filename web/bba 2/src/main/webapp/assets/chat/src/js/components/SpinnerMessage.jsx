import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import RefreshIndicator from 'material-ui/RefreshIndicator';

const style = {
  refresh: {
    display: 'inline-block',
    position: 'relative',
    boxShadow: 'none',
    left: '-10050px'
  },
};

export default class SpinnerMessage extends Component {
    render() {
        if(this.props.show) {
            return (
                <div className="more">
                    <div className="btn-more" title={this.props.title}>
                        <RefreshIndicator
                                    size={30}
                                    left={50}
                                    top={0}
                                    status="loading"
                                    style={style.refresh}
                                    />
                    </div>
                </div>
            )
        } else {
            return <div className="more"></div>
        }
    }
}