import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {ListItem} from 'material-ui/List';


const listStyle = {
    height: '72px',
}

class InfinitList extends Component {
	render() {	
		return(
			<ListItem
                onMouseOver={this.props.onMouseOver}
                onClick={this.props.onClick}
                onMouseLeave={this.props.onMouseLeave}
                style={listStyle}
                innerDivStyle={{padding: '0px'}}
                className="first infinite-list-item infinite-list-item-transition">
				{this.props.children}
			</ListItem>
		);
	}
}

export default InfinitList;