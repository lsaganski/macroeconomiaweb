import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class MessageSystem extends Component {
    constructor(props) {
        super(props);
    }
    render() {
		return(
			<div className="msg">
				<div className="message message-system">
					<div className="message-system-body">
						<div className="hidden-token">
							<div className="emojitext" dir="auto">{this.props.text}</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}