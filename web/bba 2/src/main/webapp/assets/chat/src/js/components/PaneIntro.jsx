import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class PaneIntro extends Component {
	render() {
		return(
			<div className="pane pane-intro pane-two">
				<div className="intro-body">
					<div className="intro-image" style={{opacity: '1', transform: 'scale(1)'}}></div>
					<div className="intro-text-container" style={{opacity: 1, transform: 'translateY(0px)'}}>
						<h1>Bem vindo ao Chat do Livecom</h1>
						<div className="intro-text">
							O Chat utiliza os contatos do Livecom, assim você pode entrar em contato com sua equipe rapidamente, a qualquer momento.
						</div>
					</div>
				</div>
				<div className="intro-footer"></div>
			</div>
		);
	}
}
