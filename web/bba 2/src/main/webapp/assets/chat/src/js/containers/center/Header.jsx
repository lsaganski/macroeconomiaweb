import React, {Component} from 'react';
import {observer, inject} from 'mobx-react';
import Anexar from '../../components/menu-item/Anexar';
import Notificacao from '../../components/menu-item/Notificacao';
import ListConversaControl from '../../components/lists/ListConversaControl';
import Typing from '../../components/Typing';
import Avatar from '../../components/Avatar';
import MenuHeader from '../../components/MenuHeader'
import InfoAcesso from '../../containers/modal/ModalInfoAcesso';

import MenuItem from 'material-ui/MenuItem';
import AssessmentIcon from 'material-ui/svg-icons/action/assessment';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import PerfilIcon from 'material-ui/svg-icons/social/person';
import {black, redA200} from 'material-ui/styles/colors';

@inject('chatStore', 'infoContatoStore')
@observer export default class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            anchorEl: null,
            openModal: false
        }
    }

	onClickHeader(e) {
		const isTargetInputOrButtom = !$(e.target).closest('button').length && !$(e.target).closest('input').length;
		if(isTargetInputOrButtom){
			const conversa = this.props.chatStore.conversa;
	        const user = this.props.chatStore.getUser(conversa);
			this.props.infoContatoStore.openInfoUsuario(conversa, user);
		}
	}
	onClickAnexar(e) {
		this.props.chatStore.onOpenAnexarArquivos();
	}

    onClickOpenDadosUsuarioLivecom() {
        this.setState({open: false});
        const chatStore =  this.props.chatStore;
        const contato = chatStore.getUser(chatStore.conversa);
        if(contato.get('isGroup')) {
            window.open(`${chatStore.contextUrl}/grupo.htm?id=${contato.get('id')}`, 'lvcm-h');
        } else {
            window.open(`${chatStore.contextUrl}/usuario.htm?id=${contato.get('id')}`, 'lvcm-h');
        }
    }

    onClickInformacoesAcesso() {
        this.setState({open: false});
        const {infoContatoStore, chatStore} = this.props;
        const user = chatStore.getUser(chatStore.conversa);
        infoContatoStore.openChatStatusUser(user);
    }

    onClickDeleteConversa() {
        this.setState({open: false});
        const conversa = this.props.chatStore.conversa;
        this.props.chatStore.onDeleteConversa(conversa, conversa.get('id'));
    }

	onOpenHeaderMenu = (e) => {
        this.setState({open: true, anchorEl: e.currentTarget});
    }

    onCloseHeaderMenu = () => {
        this.setState({open: false, anchorEl: null});
    }

    onClickOpenModal = () => {
        this.props.chatStore.setModalNotification(true);
    };

    render() {
        const conversa = this.props.chatStore.conversa;
        const user = this.props.chatStore.getUser(conversa);
		let statusHeader = conversa.get('isGroup') ? `Grupo ${conversa.get('grupoQtdeUsers')} participantes` : user.get('status');

        return(
        	<header className="pane-header pane-chat-header" ref="infoUser">
				<Avatar small={true} src={user.get('urlFoto')} />
				<div className="chat-body">
					<div className="chat-main">
						<h2 className="chat-title" dir="auto">
							<span className="text-title">{user.get('nome')}</span>
						</h2>
					</div>
					{ conversa.get('isTyping') ? <Typing label={this.props.chatStore.getLabelTyping(conversa)} /> : statusHeader }
				</div>
				<ListConversaControl>
					<Anexar onClick={this.onClickAnexar.bind(this)} />
                    <Notificacao label="Modal Dialog" onClick={this.onClickOpenModal} tooltip="Notificação" />
                    <MenuHeader
                        open={this.state.open}
                        onClose={this.onCloseHeaderMenu}
                        onOpen={this.onOpenHeaderMenu}
                        anchorEl={this.state.anchorEl}
                    >
                        <MenuItem
                            primaryText="Ver Perfil"
                            leftIcon={<PerfilIcon color={black} />}
                            onClick={this.onClickOpenDadosUsuarioLivecom.bind(this)}
                        />
                        {
                                user.get('isGroup')
                                ? null
                                :
                                <MenuItem
                                    primaryText="Informações de Acesso"
                                    leftIcon={<AssessmentIcon color={black} />}
                                    onClick={this.onClickInformacoesAcesso.bind(this)}
                                />
                        }
                        {
                            user.get('isGroup')
                                ? null
                                :
                                <MenuItem
                                    primaryText="Apagar Conversa"
                                    leftIcon={<DeleteIcon color={redA200} />}
                                    onClick={this.onClickDeleteConversa.bind(this)}
                                />
                        }
                    </MenuHeader>
                </ListConversaControl>
			</header>
        )
    }
}