import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class IncomingMsgs extends Component {
    render() {
        return(
            <div className="incoming-msgs" style={{transform: 'scaleX(1) scaleY(1)', opacity: 1}} onClick={this.props.onClick}>
                {
                    this.props.badge > 0 
                    ?
                        <span>
                            <span className="icon-meta unread-count" style={{transform: 'scaleX(1) scaleY(1)', opacity: 1}}>{this.props.badge}</span>
                        </span>
                    :
                        null
                }
                <span className="icon icon-down"></span>
            </div>
        )
    }
}