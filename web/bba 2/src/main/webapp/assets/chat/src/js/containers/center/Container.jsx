import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Header from './Header';
import Messages from './Messages';
import Footer from './Footer';

import * as StringUtils from '../../utils/StringUtils';


export default class Container extends Component {
    render() {
        return(
            <div id="main" className="pane pane-chat pane-two">
                { SHOW_HEADER_CHAT ? <Header /> : null }
                <Messages />
                { SHOW_FORM_TEXT_CHAT ? <Footer /> : null }
            </div>
        )
    }
}