import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

let zIndex = {
    popover: 9999,
    layer: 9999
};

const muiTheme = getMuiTheme({
    zIndex: zIndex,
    list: {
        paddingTop: 0,
        paddingBottom: 0
    }
});

class App extends Component {
    render() {
        return(
            <MuiThemeProvider muiTheme={muiTheme}>
              {this.props.children}
            </MuiThemeProvider>
        )
    }
}

export default App;