import {observable, action, inject, computed, autorun} from 'mobx';
import request from 'superagent';
import immutable, {List, Map} from 'immutable';
import userInfoStore from './userInfoStore';
import infoContatoStore from './infoContatoStore';
import chatStore from './chatStore';

let prefix = require('superagent-prefix')(SERVER_URL);

export const DRAWER_INFO_MESSAGE = "DRAWER_INFO_MESSAGE";

class InfoMessageStore {
	@observable message = new Map();
  @observable statusUsersMessage = new List();
	@observable isGroup = false;
	@observable drawerActive = false;

	@action fetchInfoMessage(conversa, messageId) {
		const userInfo = userInfoStore.userInfo;
		this.isGroup = conversa.get('isGroup');
		this.message = new Map();
        request
          .post("/ws/mensagemInfo.htm")
          .use(prefix)
          .type('form')
          .set('nonce_token', userInfo.get('id') + Date.now())
          .send({
            user_id: userInfo.get('login'),
            form_name: 'form',
            mode: 'json',
            id: messageId,
            voMessage: 1,
            wstoken: userInfo.get('token')
          }).end((err, data) => {
            if(err) throw err;
            this.message = immutable.fromJS(data.body.response.msg);
          });


        if(conversa.get('isGroup')) {
            request
              .post("/ws/statusMensagemUsuario.htm")
              .use(prefix)
              .type('form')
              .set('nonce_token', userInfo.get('id') + Date.now())
              .send({
                user_id: userInfo.get('login'),
                form_name: 'form',
                mode: 'json',
                id: messageId,
                wstoken: userInfo.get('token')
              }).end((err, data) => {
                if(err) throw err;
                this.statusUsersMessage = immutable.fromJS(data.body.list);
              });
        }
	}

	@action onOpenDrawerInfoMessage(conversa, message) {
		this.drawerActive = true;
        chatStore.addDrawerOpened(DRAWER_INFO_MESSAGE);
        infoContatoStore.closeInfoUsuario();
		this.fetchInfoMessage(conversa, message.get('id'));
	}

	@action onCloseDrawerInfoMessage() {
        chatStore.removeDrawerOpened(DRAWER_INFO_MESSAGE);
		this.drawerActive = false;
	}

	@computed get drawerInfoMessageActive() {
		return this.drawerActive;
	}
}


const infoMessageStore = new InfoMessageStore();
export default infoMessageStore;
export {InfoMessageStore};