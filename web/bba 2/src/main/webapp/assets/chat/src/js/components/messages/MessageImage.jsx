import React, {Component} from 'react';
import ReactDOM from 'react-dom';


export default class MessageImage extends Component {
    componentDidMount() {
        $('.image-link').fancybox({
        	type: 'image',
			openEffect: 'elastic',
			closeEffect: 'elastic',
			maxHeight: '90%',
			loop: false,
			helpers: {
	            overlay: {
	                closeClick: true,
	                locked: false
	            }
	        }
		});
    }
    render() { 
		let isLoading = this.props.status == 'image-sending';
        return(
        	<div>
	            <div className={`image-thumb ${this.props.status}`} style={{width: '100%', height: '186px'}}>
	                <a href={this.props.image} className="image-link" rel={`imgs-${this.props.conversaId}`}>
						{
							isLoading
							?
								<i className="image-sending-icon" />
							:
								null
						}
						<img src={this.props.thumb} className="image-thumb-body" style={{width:'330px'}} />
					</a>
	                <div className="shade" />
	            </div>	
	            {
	            	this.props.text && this.props.text != ''
	            	?
	            		<div className="message-text">
			                <span className="emojitext selectable-text" dir="ltr" dangerouslySetInnerHTML={{__html: this.props.text}} />
			            </div>
	            	:
	            		null
	            }
			</div>
		);
    }
}