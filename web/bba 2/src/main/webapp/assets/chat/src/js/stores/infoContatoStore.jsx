import {observable, action, inject, computed, autorun} from 'mobx';
import request from 'superagent';
import immutable, {List, Map} from 'immutable';
import userInfoStore from './userInfoStore';
import infoMessageStore from './infoMessageStore';
import chatStore from './chatStore';

let prefix = require('superagent-prefix')(SERVER_URL);

export const PUSH_NOTIFICATION = 'push';
export const SOUND_NOTIFICATION = 'sound';
export const BROWSER_NOTIFICATION = 'notification';
export const DRAWER_INFO_CONTATO = "DRAWER_INFO_CONTATO";
export const DRAWER_CONTATO_STATUS = "DRAWER_CONTATO_STATUS";

class InfoContatoStore {
	@observable contato = new Map();
	@observable usersGrupo = new List();
	@observable isGroup = false;
	@observable openDrawerInfoContato = false;
	@observable chatStatusUser = false;
	@observable infoChatStatusUser = null;

	@action fetchInfoContato(conversa, user) {
		const userInfo = userInfoStore.userInfo;
		this.isGroup = conversa.get('isGroup');
		this.contato = new Map();
		this.usersGrupo = new List();
		if(conversa.get('isGroup')) {
			this.requestGetInfoUser = request
            .post("/ws/getUsersGrupoMensagem.htm")
            .use(prefix)
            .type('form')
            .set('nonce_token', userInfo.get('id') + Date.now())
           	.send({
           		form_name: 'form',
				mode: 'json',
				user:userInfo.get('login'),
	            grupo_id: conversa.get('grupoId'),
                wstoken: userInfo.get('token')
           	}).end((err, data) => {
           		let {list} = data.body;
           		if(list && list.length > 0) {
           			this.usersGrupo = immutable.fromJS(list);
           		}
           	});
		} else {
			this.requestGetInfoUser = request
            .post("/ws/usuariosAutoComplete.htm")
            .use(prefix)
            .type('form')
            .set('nonce_token', userInfo.get('id') + Date.now())
           	.send({
           		form_name: 'form',
           		mode: 'json',
           		user: userInfo.get('login'),
           		user_id: userInfo.get('id'),
           		conversa: userInfo.get('id'),
           		usuarios: user.get('id'),
                wstoken: userInfo.get('token')
           	}).end((err, data) => {
           		let {list} = data.body;
           		if(list && list.length > 0) {
           			this.contato = immutable.fromJS(list[0]);
           		}
           	});
		}
	}

	@action changeStatusPushNotification(status, conversa) {
		const userInfo = userInfoStore.userInfo;
		request
            .post("/rest/chat/notifications")
            .use(prefix)
            .type('form')
            .set('nonce_token', userInfo.get('id') + Date.now())
            .send({
            	conversationId: conversa.get('id'),
	            tipo: PUSH_NOTIFICATION,
	            status: status,
	            userId: userInfo.get('id'),
                wstoken: userInfo.get('token'),
            }).end((err, data) => {
            	if(chatStore.conversa && chatStore.conversa.get('id') == conversa.get('id')) {
            		let notifications = chatStore.conversa.get('notifications');
            		notifications = notifications.set('sendPush', status);
      				chatStore.conversa = chatStore.conversa.set('notifications', notifications);
            	}

                let conversas = chatStore.conversas;
                for(let i = 0; conversas.size > i; i++) {
                    let c = conversas.get(i);
                    if(conversa.get('id') == c.get('conversaId')) {
                        let notifications = c.get('notifications');
                        notifications = notifications.set('sendPush', status);
                        c = c.set('notifications', notifications);
                        chatStore.conversas = conversas.set(i, c);
                    }
                }
            });
	}

	@action changeStatusSoundNotification(status, conversa) {
		const userInfo = userInfoStore.userInfo;
		request
            .post("/rest/chat/notifications")
            .use(prefix)
            .type('form')
            .set('nonce_token', userInfo.get('id') + Date.now())
            .send({
            	conversationId: conversa.get('id'),
	            tipo: SOUND_NOTIFICATION,
	            status: status,
	            userId: userInfo.get('id'),
                wstoken: userInfo.get('token')
            }).end((err, data) => {
            	if(chatStore.conversa && chatStore.conversa.get('id') == conversa.get('id')) {
            		let notifications = chatStore.conversa.get('notifications');
            		notifications = notifications.set('sound', status);
      				chatStore.conversa = chatStore.conversa.set('notifications', notifications);
            	}

                let conversas = chatStore.conversas;
                for(let i = 0; conversas.size > i; i++) {
                    let c = conversas.get(i);
                    if(conversa.get('id') == c.get('conversaId')) {
                        let notifications = c.get('notifications');
                        notifications = notifications.set('sound', status);
                        c = c.set('notifications', notifications);
                        chatStore.conversas = conversas.set(i, c);
                    }
                }
            });
	}

	@action changeStatusBrowerNotification(status, conversa) {
		const userInfo = userInfoStore.userInfo;
		request
            .post("/rest/chat/notifications")
            .use(prefix)
            .type('form')
            .set('nonce_token', userInfo.get('id') + Date.now())
            .send({
            	conversationId: conversa.get('id'),
	            tipo: BROWSER_NOTIFICATION,
	            status: status,
	            userId: userInfo.get('id'),
                wstoken: userInfo.get('token')
            }).end((err, data) => {
            	if(chatStore.conversa && chatStore.conversa.get('id') == conversa.get('id')) {
            		let notifications = chatStore.conversa.get('notifications');
            		notifications = notifications.set('notification', status);
      				chatStore.conversa = chatStore.conversa.set('notifications', notifications);
            	}

                let conversas = chatStore.conversas;
                for(let i = 0; conversas.size > i; i++) {
                    let c = conversas.get(i);
                    if(conversa.get('id') == c.get('conversaId')) {
                        let notifications = c.get('notifications');
                        notifications = notifications.set('notification', status);
                        c = c.set('notifications', notifications);
                        chatStore.conversas = conversas.set(i, c);
                    }
                }
            });
	 }

	fetchInfoStatusUser(user) {
		this.infoChatStatusUser = null;
		request
			.get(`/rest/v1/chatStatus/user/${user.get('id')}/status`)
			.use(prefix)
			.end((err, data) => {
				if(err) throw  err;
				if(data.body) {
					this.infoChatStatusUser = immutable.fromJS(data.body);
				} else {
					this.infoChatStatusUser = null;
				}
			});
	}

	@action openChatStatusUser(user) {
		this.chatStatusUser = true;
		this.fetchInfoStatusUser(user);
		chatStore.addDrawerOpened(DRAWER_CONTATO_STATUS);
	}

	@action closeChatStatusUser() {
		this.chatStatusUser = false;
		chatStore.removeDrawerOpened(DRAWER_CONTATO_STATUS);
	}

	@action openInfoUsuario(conversa, user) {
		this.openDrawerInfoContato = true;
		this.chatStatusUser = false;
		chatStore.addDrawerOpened(DRAWER_INFO_CONTATO);
        infoMessageStore.onCloseDrawerInfoMessage();
		this.fetchInfoContato(conversa, user);
	}

	@action closeInfoUsuario() {
		this.openDrawerInfoContato = false;
		this.chatStatusUser = false;
		chatStore.removeDrawerOpened(DRAWER_INFO_CONTATO);
		if(this.requestGetInfoUser) {
			this.requestGetInfoUser.abort();
		}
	}

	@computed get drawerInfoActive() {
		return this.openDrawerInfoContato;
	}
}


const infoContatoStore = new InfoContatoStore();
export default infoContatoStore;
export {InfoContatoStore};