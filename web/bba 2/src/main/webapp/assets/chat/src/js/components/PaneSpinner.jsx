import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import RefreshIndicator from 'material-ui/RefreshIndicator';

const style = {
  refresh: {
    display: 'inline-block',
    position: 'relative',
    boxShadow: 'none'
  },
};

export default class PaneSpinner extends Component {
	render() {
		return(
			<div className="pane pane-intro pane-two">
				<div className="intro-body">
					<RefreshIndicator
                      size={30}
                      left={50}
                      top={0}
                      status="loading"
                      style={style.refresh}
                    />
				</div>
			</div>
		);
	}
}
