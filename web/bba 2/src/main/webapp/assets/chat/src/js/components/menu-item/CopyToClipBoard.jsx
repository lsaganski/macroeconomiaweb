import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import ReactCopyToClipBoard from 'react-copy-to-clipboard';
import MenuItem from 'material-ui/MenuItem';

export default class CopyToClipBoard extends Component {
    render() {
        return(
            <MenuItem className="fix-padding-btn">
                <ReactCopyToClipBoard text={this.props.valueToCopy} onCopy={this.props.onCopy}>
                    <span className="fix-padding-span">{this.props.label}</span>
                </ReactCopyToClipBoard>
            </MenuItem>
        )
    }
}