import React, {Component} from 'react';
import ReactDOM from 'react-dom';


export default class Butterbar extends Component {
	render() {
		return(
			<div className="butterbar-wrapper" style={{display: 'block'}}>
          		<div className="butterbar butterbar-livecom">
            		<div className="butterbar-icon">
              			<span className="icon icon-alert icon-alert-computer"></span>
            		</div>
            		<div className="butterbar-body">
              			<div className="butterbar-title">Você está desconectado!</div>
            			<div className="butterbar-text">
              				<span>Parece que você está desconectado.&nbsp;</span>
            				<span><span className="action" onClick={this.props.onClick}><br />Clique aqui</span> para reconectar</span>
          				</div>
          			</div>
          		</div>
          </div>
		);
	}
}