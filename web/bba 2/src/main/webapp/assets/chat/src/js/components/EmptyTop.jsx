import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class EmptyTop extends Component {
	render() {
		return(
			<div className="empty empty-top">
				<div className="empty-text">
					{this.props.children}
				</div>
			</div>
		);
	}
}
