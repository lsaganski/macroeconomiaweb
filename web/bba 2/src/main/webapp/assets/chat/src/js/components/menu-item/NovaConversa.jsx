import IconButton from 'material-ui/IconButton/IconButton';
import PersonAdd from 'material-ui/svg-icons/social/person-add';
import {white} from 'material-ui/styles/colors';

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class NovaConversa extends Component {
    render() {
        return (
            <div className="menu-item">
                <IconButton
                    onTouchTap={this.props.onClick}
                    tooltip="Nova Conversa">
                    <PersonAdd color={white } />
                </IconButton>
            </div>
        )
    }
}