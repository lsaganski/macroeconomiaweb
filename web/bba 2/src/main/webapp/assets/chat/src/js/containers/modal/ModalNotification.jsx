import React, {Component} from 'react';
import Dialog from 'material-ui/Dialog';
import Checkbox from 'material-ui/Checkbox';
import FlatButton from 'material-ui/FlatButton';
import {observer, inject} from 'mobx-react';

const styles = {
    checkbox: {
        marginBottom: 16,
    },
};

const DrawerSectionTitle = ({label}) => {
    return (
        <div className="drawer-section-title  title-underlined">
            <div className="col-main">{label}</div>
        </div>
    )
}


const DrawerSectionBodyCheck = ({checked, onCheck, label}) => {
    return(
        <div className="drawer-section-body">
			<span className="textfield-static selectable-text" dir="auto">
				<Checkbox
                    onCheck={onCheck}
                    checked={checked}
                    label={label}
                    labelPosition="left"
                    style={styles.checkbox}
                />
			</span>
        </div>
    )
}

const FecharModal = ({onClick}) => {
    return (
        <FlatButton
            label="Fechar"
            primary={true}
            onClick={onClick}
        />
    )
}

@inject('chatStore', 'infoContatoStore')
@observer export default class ModalNotification extends Component {
    constructor(props) {
        super(props);
        const conversa = this.props.conversa;
        const notifications = conversa.get('notifications');
        this.state = {
            notifications: notifications,
        }
    }
    componentWillReceiveProps(props) {
        const conversa = props.conversa;
        const notifications = conversa.get('notifications');
        this.setState({notifications});
    }
    onClickCloseModal = () => {
        this.props.chatStore.setModalNotification(false);
    }
    onChangeSoundNotification = (e) => {
        let notifications = this.state.notifications;
        const status = !notifications.get('sound');
        notifications = notifications.set('sound', status);
        this.setState({notifications: notifications});
        const conversa = this.props.chatStore.conversa;
        this.props.infoContatoStore.changeStatusSoundNotification(status, conversa);
    }
    onChangeNotification = (e) => {
        let notifications = this.state.notifications;
        const status = !notifications.get('notification');
        notifications = notifications.set('notification', status);
        this.setState({notifications: notifications});
        const conversa = this.props.chatStore.conversa;
        this.props.infoContatoStore.changeStatusBrowerNotification(status, conversa);
    }
    render() {
        const {openModalNotification} = this.props.chatStore;
        return(
            <Dialog
                title="Notificação"
                actions={[
                    <FecharModal onClick={this.onClickCloseModal} />
                ]}
                open={openModalNotification}
            >
                <div className="animate-enter2 drawer-section well">
                    <DrawerSectionBodyCheck
                        label="Som de notificação"
                        checked={this.state.notifications.get('sound')}
                        onCheck={this.onChangeSoundNotification}
                    />
                    <DrawerSectionBodyCheck
                        label="Notificação"
                        checked={this.state.notifications.get('notification')}
                        onCheck={this.onChangeNotification}
                    />
                </div>
            </Dialog>
        )
    }
}