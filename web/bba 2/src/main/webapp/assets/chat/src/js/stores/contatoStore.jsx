import {observable, action, computed, inject} from 'mobx';
import request from 'superagent';
import immutable, {List} from 'immutable';
import userInfoStore from './userInfoStore';
import chatStore from './chatStore';

let prefix = require('superagent-prefix')(SERVER_URL);

export const DRAWER_NOVA_CONVERSA = "DRAWER_NOVA_CONVERSA";

class ContatoStore {
    @observable openDrawer = false;
    @observable usuarios = new List();
    @observable grupos = new List();
    @observable isFetchContatos = false;
    @observable isSpinnerSearch = false;
    @observable clearSearch = false;
    @observable searchInputFocus = false;

    @action openDrawerNovaConversa() {
        this.openDrawer = true;
        this.searchInputFocus = true;
        chatStore.addDrawerOpened(DRAWER_NOVA_CONVERSA);
        this.fetchContatos();
    }

    @action closeDrawerNovaConversa() {
        this.usuarios = new List();
        this.grupos = new List();
        this.isFetchContatos = false;
        this.isSpinnerSearch = false;
        this.clearSearch = true;
        this.openDrawer = false;
        this.searchInputFocus = false;
        chatStore.removeDrawerOpened(DRAWER_NOVA_CONVERSA);
    }
    
    @computed get drawerNovaConversaAtivo() {
        return this.openDrawer;
    }

    @action fetchContatos(search) {
        this.clearSearch = false;
        const userInfo = userInfoStore.userInfo;
        this.isFetchContatos = true;
        search = search ? search : "";
        if(search) this.isSpinnerSearch = true;


        if(this.requestContatos) {
            this.requestContatos.abort();
            delete this.requestContatos;
        }
        this.usuarios = new List();
        this.requestContatos = request
            .post("/ws/gruposUsuariosAutoComplete.htm")
            .use(prefix)
            .type('form')
            .set('nonce_token', userInfo.get('id') + Date.now())
            .send({
                q: search,
                maxRows: 100,
                form_name: "form",
                mode: "json",
                user_id: userInfo.get('id'),
                user: userInfo.get('login'),
                not_grupo_ids: '-1',
                participo: 1,
                wstoken: userInfo.get('token')
            }).end((err, data) => {
                delete this.requestContatos;
                this.isFetchContatos = false;
                this.isSpinnerSearch = false;
                this.searchInputFocus = false;
                
                if(err) {
                    throw new Error('Falha ao buscar os contatos');
                }
                if(data.body && data.body.list) {
                    const {groups, users} = data.body.list;
                    this.grupos = groups ? immutable.fromJS(groups) : new List();
                    this.usuarios = users ? immutable.fromJS(users) : new List();
                }
             });
    }

    /**
     * Busca a conversa do contato
     */
    @action novaConversa(contatoId, isGroup) {
        this.clearSearch = true;
        chatStore.novaConversa(contatoId, isGroup).then((conversa) => {
            chatStore.fetchConversa(conversa);
            this.openDrawer = false;
            this.clearSearch = false;
        });
    }
}

const contatoStore = new ContatoStore();
export default contatoStore;
export {ContatoStore};