import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class TextInput extends Component {
	constructor(props) {
        super(props);
        this.state = {
            placeholder: true
        }
    }
    onPaste(e) {
        this.changePlaceHolder();
	}
    changePlaceHolder() {
        setTimeout( () => {
		   let textInput = ReactDOM.findDOMNode(this.refs.textInput);
		   if(textInput.innerText != "") {
   				this.setState({placeholder:false})
		   } else {
			   this.setState({placeholder:true})
		   }
	   }, 0);
    }
	componentWillReceiveProps(props) {
		if(props.focus) {
			this.focusInput();
		}
		if(props.clear) {
			this.clear();
		}
	}
	clear() {
		let nodeTextInput = ReactDOM.findDOMNode(this.refs.textInput);
		nodeTextInput.innerText = "";
		this.changePlaceHolder();
	}
    componentDidMount() {
		this.addEventListeners();
        this.focusInput();
	}
    focusInput() {
		setTimeout(() => {
			ReactDOM.findDOMNode(this.refs.textInput).focus();
		}, 0); 
    }
    addEventListeners() {
        let textInput = ReactDOM.findDOMNode(this.refs.textInput);
		textInput.addEventListener('keypress', this.onKeyPress.bind(this));
		textInput.addEventListener('keyup', this.onKeyUp.bind(this));
		textInput.addEventListener('paste', this.onPaste.bind(this));   
    }
    onKeyPress(event) {
        if ( event.which == 13 ) return  event.preventDefault();
        this.changePlaceHolder();
        if(this.props.sendTypingStatus) {
            this.props.sendTypingStatus();
        }
	}
	onKeyUp(event) {
		this.changePlaceHolder();
		let nodeTextInput = ReactDOM.findDOMNode(this.refs.textInput);
		if(nodeTextInput.innerText != "") {
			this.setState({placeholder:false})
			if ( event.which == 13 ) {
			    if(this.props.sendMessage) {
                    this.props.sendMessage();
                }
				this.clear();
				return;
			}
		}
		if(this.props.onChange) {
            this.props.onChange(nodeTextInput.innerText);
        }
	}
	render() {
		return (
			<div className="input-emoji">
                {this.state.placeholder ? <div className="input-placeholder">Digite uma mensagem</div> : null}
				<div dir="auto" contentEditable="true" className="input" ref="textInput"></div>
			</div>

		)
	}
}