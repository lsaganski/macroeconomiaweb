import React, {Component} from 'react';

export default class RecordAudio extends Component {
    constructor(props) {
        super(props);

        this.state = {
            recorder: null,
            stream: null,
            time: 0
        }
    }
    onSuccess(stream) {
        this.setState({stream});
        var context = new AudioContext();
        var mediaStreamSource = context.createMediaStreamSource(stream);
        let recorder = new Recorder(mediaStreamSource);
        let minSecStr = function (n) {
            return (n < 10 ? "0" : "") + n;
        };

        this.countInterval = window.setInterval(() => {
            var sec = context.currentTime | 0;
            let time = "" + (minSecStr(sec / 60 | 0)) + ":" + (minSecStr(sec % 60));
            this.setState({time});
        }, 200);
        recorder.record();
        this.setState({recorder});
    }
    onFail(e) {
        this.clearIntervalTime();
        this.props.onError(e);
    }
    clearIntervalTime() {
        if(this.countInterval) {
            clearInterval(this.countInterval);
        }
    }
    componentDidMount() {
        if (navigator.getUserMedia) {
            navigator.getUserMedia({audio: true}, this.onSuccess.bind(this), this.onFail.bind(this));
        } else {
            console.log('navigator.getUserMedia not present');
        }
    }
    clearStream() {
        if(this.state.stream != null) {
            this.state.stream.getTracks()[0].stop();
        }
    }
    onFinish() {
        this.clearStream();
        this.clearIntervalTime();
        let {recorder} = this.state;
        recorder.stop();
        recorder.exportWAV((blob) => {
            this.props.onComplete(blob);
        });
    }
    onCancel() {
        this.clearStream();
        this.clearIntervalTime();
        let {recorder} = this.state;
        recorder.stop();
        recorder.clear();
        this.props.onCancel();
    }
    render(){
        return(
            <div className="ptt-container active">
    			<span>
    				<div className="ptt-recording">
						<button className="btn-close btn-border ptt-close" onClick={this.onCancel.bind(this)}>
							<span className="icon icon-round-x btn-state-default"></span>
							<span className="icon icon-round-x-inv btn-state-hover"></span>
						</button>
						<div className="ptt-counter">{this.state.time}</div>
						<button className="btn-border ptt-send">
							<span className="icon icon-round-send btn-state-default"></span>
							<span className="icon icon-round-send-inv btn-state-hover" onClick={this.onFinish.bind(this)}></span>
						</button>
					</div>
    			</span>
            </div>
        );
    }
}