import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import Popover from 'material-ui/Popover';
import {white} from 'material-ui/styles/colors';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconButton from 'material-ui/IconButton/IconButton';
import Menu from 'material-ui/Menu';

export default class MenuHeader extends Component {
	render() {
		return (
			<div className="menu-item">
				<IconButton onTouchTap={this.props.onOpen} tooltip="Menu">
				 <MoreVertIcon color={white} />
			   </IconButton>
				<Popover
				  open={this.props.open}
				  anchorEl={this.props.anchorEl}
				  anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
				  targetOrigin={{horizontal: 'left', vertical: 'top'}}
				  onRequestClose={this.props.onClose}
				>
					<Menu>
						{this.props.children}
					</Menu>
				</Popover>
			</div>
		);
	}
}