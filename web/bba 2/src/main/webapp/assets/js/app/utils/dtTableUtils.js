	var oLanguage =  {
       "sProcessing":   "Processando...",
       "sLengthMenu":   "Mostrar _MENU_ registros",
       "sZeroRecords":  "Não foram encontrados resultados",
       "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registros",
       "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registros",
   	   "sInfoFiltered": "",
       "sInfoPostFix":  "",
       "sSearch":       "Buscar:",
       "sUrl":          "",
       "oPaginate": {
    	   "sFirst":    "Primeiro",
       	   "sPrevious": "Anterior",
       	   "sNext":     "Seguinte",
       	   "sLast":     "Último"
       }
   };

var dtTableUtils = (function() {
	return function (jsonDatatable, rowReorder) {
		var dtTable = null;
		
		
		if(jsUtils.isObject(jsonDatatable) && jsonDatatable.ajax) {
			var cols = jsonDatatable.columns;
			var aoColumns = [];
			for(var i in cols) {
				if(jsUtils.isString(cols[i])) {
					aoColumns.push({mData : cols[i]});
				} else {
					aoColumns.push(cols[i]);
				}
			}
			
			if(!jsonDatatable.columnDefs) {
				jsonDatatable.columnDefs = [ { orderable: false, targets: -1} ];
			}
			
			if(!jsonDatatable.order) {
				jsonDatatable.order = [[ 0, "asc" ]];
			}
			
			if(jsUtils.isUndefined(jsonDatatable.searching)){
				jsonDatatable.searching = true;
			}
			
			dtTable = $(jsonDatatable.element).DataTable({
				oLanguage :oLanguage,
				processing: true,
				order: jsonDatatable.order,
				sAjaxSource:  jsonDatatable.ajax.url,
				columnDefs: jsonDatatable.columnDefs,
				responsive: true,
				serverSide: true,
				autoWidth: false,
				searching: jsonDatatable.searching,
				fnDrawCallback: jsonDatatable.fnDrawCallback, 
				aoColumns: aoColumns,
				fnRowCallback: jsonDatatable.fnRowCallback,
				fnServerData:  function ( sSource, aoData, fnCallback, oSettings ) {
					var q = null, page = null, maxRows = null, ws = '';
					if(sSource.lastIndexOf('wsVersion') > -1){
						ws = parseInt(sSource.substring(sSource.lastIndexOf('wsVersion') + 10, sSource.length));
					}
					
					var sSearch = aoData[35];
					if(sSearch && sSearch.name == "sSearch") {
						q = sSearch.value;
					} else if(sSearch && sSearch.name != "sSearch") {
						for(var i in aoData) {
							var sSearch = aoData[i];
							if(sSearch && sSearch.name == "sSearch") {
								q = sSearch.value;
								break;
							} 
						}
					}
					
					var iDisplayStart = aoData[3];
					if(iDisplayStart && iDisplayStart.name == "iDisplayStart") {
						var start = iDisplayStart.value;
						var leng = aoData[4].value;
						page = start / leng;
						if(!page) {
							page = 0;
						}
					} else if(iDisplayStart && iDisplayStart.name != "iDisplayStart") {
						for(var i in aoData) {
							var iDisplayStart = aoData[i];
							if(iDisplayStart && iDisplayStart.name == "iDisplayStart") {
								page = iDisplayStart.value.toString().slice(0, -1);
								if(!page) {
									page = 0;
								}
								break;
							}
						}
					}
					
					
					var iDisplayLength = aoData[4];
					if(iDisplayLength && iDisplayLength.name == "iDisplayLength") {
						maxRows = iDisplayLength.value;
					} else if(iDisplayLength && iDisplayLength.name != "iDisplayLength") {
						for(var i in aoData) {
							var iDisplayLength = aoData[i];
							if(iDisplayLength && iDisplayLength.name == "iDisplayLength") {
								maxRows = iDisplayLength.value;
								break;
							}
						}
					}
					
					var coluna = aoColumns[oSettings.aaSorting[0][0]];
					var ordenacao = oSettings.aaSorting[0][1];
					
					if(ws != ''){
						aoData.push({name: 'wsVersion', value: ws});
					}else{
						aoData.push({name: 'wsVersion', value: wsVersion});
					}
					aoData.push({name: 'wstoken', value: SESSION.USER_INFO.USER_WSTOKEN});
					aoData.push({name: 'page', value: page});
					aoData.push({name: 'maxRows', value: maxRows});
					aoData.push({name: 'max', value: maxRows});
					aoData.push({name: 'q', value: q});
					aoData.push({name: 'coluna', value: coluna.mData});
					aoData.push({name: 'ordenacao', value: ordenacao});
					
					var aoData = aoData.concat(jsonDatatable.filter);
					
					function callback(res) {
						var json = jsonDatatable.filterResponse(res);
						if(json != null) {
							json.draw = oSettings.Idraw;
							fnCallback(json);
						}
			 		}
					
			 		oSettings.jqXHR = jQuery.ajax( {
				        "type": jsonDatatable.ajax.type,
				        "url":  jsonDatatable.ajax.url,
				        "data": aoData,
				       	"success": callback
					});
			 		
			 		
				}
		       
			});
		} else if(jsUtils.isString(jsonDatatable)) {
			dtTable = $(jsonDatatable).DataTable({
			   "pageLength": 10,
		       "oLanguage": oLanguage,
		       "order": [],
		       "columnDefs": [ { orderable: false, targets: -1} ],
		       "responsive": true,
		    });
		} else if(jsUtils.isObject(jsonDatatable) && jsonDatatable.element){
			var pageLength = 10;
			dtTable = $(jsonDatatable.element).DataTable({
				"pageLength": pageLength,
				"lengthMenu": [ 10, 25, 50, 75, 100, 150 ],
			    "oLanguage": oLanguage,
			    "order": [],
			    "columnDefs": [ { orderable: false, targets: -1} ],
			    "responsive": true,
			    "autoWidth": false
			});
		} else {
			var pageLength = 10;
			if(jsUtils.isNumber(jsonDatatable)) {
				pageLength = jsonDatatable;
			}
			
			var desactiveUlr = jQuery(location).attr('href');
			desactiveUlr = desactiveUlr.substr(desactiveUlr.lastIndexOf('/') + 1);
			desactiveUlr = desactiveUlr.substr(0,desactiveUlr.lastIndexOf('?'));
			
			if(desactiveUlr == "detalhesAudienciaReport.htm"){
				var lastColumn = false;
			}else{
				var lastColumn =  [ { orderable: false, targets: -1} ];
			}
			
			if(rowReorder){
					dtTable = $(".table").DataTable({
						"pageLength": pageLength,
						"lengthMenu": [ 10, 25, 50, 75, 100, 150 ],
					    "oLanguage": oLanguage,
					    "processing": true,
					    "order": [],
					    "columnDefs": [ { orderable: false, targets: -1 } ],
					    "responsive": true,
					    "autoWidth": false,
					    "rowReorder": true,
					    "createdRow": function(row, data, dataIndex){
					    	$(row).attr('data-row', 'row-' + dataIndex);
					    }
					});
			}else if(urlAtual == 'grupos.htm'){
				dtTable = $(".table").DataTable({
					"pageLength": pageLength,
					"lengthMenu": [ 10, 25, 50, 75, 100, 150 ],
				    "oLanguage": oLanguage,
				    "processing": true,
				    "order": [],
				    "columnDefs": [ { searchable: false, targets: 1 } ],
				    "responsive": true,
				    "autoWidth": false,
				});
			}else{
				dtTable = $(".table").DataTable({
					"pageLength": pageLength,
					"lengthMenu": [ 10, 25, 50, 75, 100, 150 ],
				    "oLanguage": oLanguage,
				    "processing": true,
				    "order": [],
				    "columnDefs": lastColumn,
				    "responsive": true,
				    "autoWidth": false
				});
			}
		}
		return dtTable;
	}
}());