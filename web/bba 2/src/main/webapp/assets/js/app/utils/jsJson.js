var jsJson = (function() {

    var toString = function(json) {
        try {
            if(jsUtils.isNotEmpty(json) && jsUtils.isObject(json));
                return JSON.stringify(json);
            return null;
        } catch(err) {
            return null;
        }
    }

    var toJson = function(json) {
        try {
            if(jsUtils.isNotEmpty(json) && JSON.parse(json))
                return JSON.parse(json);
            return null;
        } catch(err) {
            return null;
        }
    }

    var jsJson = {
        toString: toString,
        toJson: toJson
    };

    return jsJson;
}());
