var service = (function() {
	var tag = {};
	tag.salvar = function(params) {
		params.id = parseInt(params.id);
		if(params.id) {
			return jsHttp.put(urlContext + "/rest/v1/tag", {
				data: params
			});
		} else {
			return jsHttp.post(urlContext + "/rest/v1/tag", {
				data: params
			});
		}
	}
	
	var categoria = {};
	categoria.salvar = function(params) {
		params.id = parseInt(params.id);
		if(params.id) {
			return jsHttp.put(urlContext + "/rest/v1/categoria", {
				data: params
			});
		} else {
			return jsHttp.post(urlContext + "/rest/v1/categoria",  {
				data: params
			});
		}
		
	}

	return {
		tag: tag,
		categoria: categoria
	}
} ());