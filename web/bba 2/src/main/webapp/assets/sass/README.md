#- Arquitetura SASS Livecom -

sass/  
├── cadastros/  
|   ├── ...  
|   └── _INDEX.scss  
|  
├── components/  
|   ├── ...  
|   ├── metronic/  
|   |   ├── ...  
|   |   └── _INDEX.scss  
|   |  
|   └── _INDEX.scss  
|  
├── misc/  
|   ├── ...  
|   └── _INDEX.scss  
|  
├── mural/  
|   ├── ...  
|   └── _INDEX.scss  
|  
├── relatorios/  
|   ├── ...  
|   └── _INDEX.scss  
|  
├── utils/  
|   ├── _colors.scss  
|   ├── _extends.scss  
|   ├── _mixins.scss  
|   └── _sizes.scss  
|  
├── cadastros.scss  
├── livecom.scss  
├── livecom-global.scss  
├── login.scss  
├── mural.scss  
└── relatorios.scss  


---
#ARQUIVOS

##- cadastros.scss -
1. Nesse arquivo ficam os imports apenas utilizados na sessão de *Cadastros* do Livecom.

##- login.scss -
1. Nesse arquivo ficam os estilos e imports utilizados na tela de *Login* do Livecom.

##- livecom-global.scss -
1. Nesse arquivo ficam todos os estilos utilizados no Livecom; **com** os imports de *Cadastros*, *Mural* e *Relatórios*.

##- livecom.scss -
1. Nesse arquivo ficam todos os estilos comuns utilizados no Livecom; **sem** os imports de *Cadastros*, *Mural* e *Relatórios*.

##- mural.scss -
1. Nesse arquivo ficam os imports apenas utilizados no *Mural* do Livecom.

##- relatorios.scss -
1. Nesse arquivo ficam os imports apenas utilizados na sessão de *Relatórios* do Livecom.



---
#DIRETÓRIOS

##- /cadastros/ -
1) Nesse diretório ficam os estilos utilizados na sessão de *Cadastros* do Livecom.


##- /components/ -
1) Nesse diretório ficam os estilos utilizados nos componentes do Livecom - tudo o que foi criado/modificado pela equipe.

2) As subpastas desse diretório estão organizadas de acordo com o nome de cada componente, contendo 1 (*_componente.scss*) ou mais arquivos (importados dentro do *_INDEX.scss* da pasta).

**/metronic**
2.1) Nesse diretório estão os componentes criados/modificados pelo Metronic, divididos também de acordo com o nome de cada componente.

2.1.1) *_layout.scss* contém estilos referentes as sessões de breadcrumbs, estruturas de páginas, e dados do cadastro de usuários, grupos...

2.1.2) *_metronic-core.scss* contém os estilos e imports dos componentes do Metronic.

2.1.3) *_sobrescreve.scss* contém os estilos que sobrescrevem Bootstrap e Metronic.


##- /misc/ -
1) Nesse diretório ficam os estilos variados utilizados por todo Livecom.

**_reset.scss**
1.2) Deve ser importado antes de qualquer outro import diretamente nos arquivos *livecom-global.scss*, *livecom.scss* e *login.scss*, para resetarem os estilos padrões de browsers.

**_media-queries.scss**
1.3) Deve ser importado diretamente nos arquivos *livecom-global.scss*, *livecom.scss* e *login.scss*, especificamente no final dos arquivos para realizarem o layout responsivo sem nenhum risco de outro estilo interferir.


##- /mural/ -
1) Nesse diretório ficam os estilos utilizados no *Mural* do Livecom.


##- /relatorios/ -
1) Nesse diretório ficam os estilos utilizados na sessão de *Relatórios* do Livecom.


##- /utils/ -
1) Nesse diretório ficam os arquivos úteis .scss, como *_colors*, *_extends*, *_mixins* e *_sizes*.

1.1) **_colors.scss**
Neste arquivo ficam todas as variáveis de cores do projeto

1.2) **_extends.scss**
Neste arquivo ficam todas as classes genéricas para serem extendidas  
**ver doc sass sobre extends - `http://sass-lang.com/guide`**  
**sessão Extend/Inheritance**

1.3) **mixins.scss**
Neste arquivo ficam todos os mixins do projeto  
**ver doc sass sobre mixins - `http://sass-lang.com/guide`**  
**sessão Mixins**

1.4) **_sizes.scss**
Neste arquivo ficam todas os breakpoints do projeto  
(dividido para não confundir com cores)


2) Todo arquivo desse diretório deve começar com _ para que o compilador do sass ignore esse arquivo (para não gerar um .css).


---
##- CSS -
O diretório /css/, dentro de /assets/ jamais deverá ser modificado, por ser gerado a partir da compilação do sass.  
**Toda modificação de estilos deverá ser feita nos arquivos .scss.**


---
##- INICIANDO O SASS -
Para iniciar o compilador do sass, entre na pasta /assets/ através do prompt de comando (ou do Git BASH),
( cd C:/PATH ONDE ESTA O PROJETO/livecom-maven/src/main/webapp/assets )

E inicie o compilador com:

`sass --watch sass:css --style compressed`

Onde:

1. sass indica que será inicializado o compilador do SASS
2. --watch indica que o compilador irá ficar monitorando
3. sass: indica a pasta monitorada
4. css indica a pasta que será gerada a partir da compilação
5. --style compressed indica que o css compilado será minificado (opcional)


#alterado para Gulp.js - rodar `npm test`


##- CAUTELAS -
1. Os caminhos relativos de assets (imagens, fontes e afins) devem ser calculados a partir da pasta gerada pela compilação (no caso, /css/).

2. Sempre importar nos arquivos .htm e .html o arquivo gerado pela compilação (livecom.css) - o browser não interpreta arquivos .scss como estilos válidos.

3. Nomes de variáveis devem começar com letras, nunca com números  
**ver doc sass sobre variáveis - `http://sass-lang.com/guide`**  
**sessão Variables**


---
**login.css   - 57.6 kb (dev) || 13.1 kb (prod)**  
  
**livecom-global.css - 725 kb (dev) || 193 kb (prod)**  
  
**livecom.css - 646 kb (dev) || 172 kb (prod)**  
**mural.css   - 66.5 kb (dev) || 16.1kb (prod)**  
**cadastros.css   - 5.4 kb (dev) || 2.1kb (prod)**  
**relatorios.css   - 6.0 kb (dev) || 2.3kb (prod)**  