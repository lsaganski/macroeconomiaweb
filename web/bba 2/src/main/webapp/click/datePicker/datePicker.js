	function textPick(campo, tecla){
		
		if(validaDigito(campo, tecla)){
			
			//se precionou uma tecla que � um digito (tecla.which != 0) formata.
			if(tecla.which != 0){
				formataData(campo, tecla);
			}
			return true;
		} 
		else {		
			return false;
		}
	}

	function formataData(campo, tecla){
		
		if(tecla.charCode != 0){
			var tammax = 8;
			if (typeof (tecla) == "undefined"){
				var tecla = window.event;
			}

			var codigo = (tecla.which ? tecla.which : tecla.keyCode ? tecla.keyCode : tecla.charCode);

			var vr = campo.value;
			vr = vr.replace("/", "");
			vr = vr.replace("/", "");
			
			var tam = vr.length;

			if (tam < tammax){
				tam = vr.length + 1;
			}

			if (codigo == 8){
				tam = tam - 1;
			}
			
			var separadorAtual = "/";
			
			//se o usu�rio digitou "/", nao poe, senao fica duplicado
			if(codigo == 47){
				separadorAtual = "";
			}

			tam = tam - 1;
			if (tam == 2){
				vr = vr.substr(0, tam - 0) + separadorAtual + vr.substr(tam - 0, 2);
			}
			if (tam == 3){
				vr = vr.substr(0, tam - 1) + separadorAtual + vr.substr(tam - 1, 2);
			}
			if (tam == 4){
				vr = vr.substr(0, tam - 2) + "/" + vr.substr(tam - 2, 2) + separadorAtual + vr.substr(tam - 0, 5);
			}
			if (tam == 5){
				vr = vr.substr(0, tam - 3) + "/" + vr.substr(tam - 3, 2) + separadorAtual + vr.substr(tam - 1, 6);
			}
			if (tam == 6){
				vr = vr.substr(0, tam - 4) + "/" + vr.substr(tam - 4, 2) + separadorAtual + vr.substr(tam - 2, 7);
			}
			if (tam == 7){
				vr = vr.substr(0, tam - 5) + "/" + vr.substr(tam - 5, 2) + separadorAtual + vr.substr(tam - 3, 8);
			}

			campo.value = vr;
		}
	}

	function validaDigito(campo, tecla){

		//se tiver com ctrl ou alt precionado passa.
		//se precionou uma tecla que nao � um digito (tecla.which == 0) passa.
		if(tecla.ctrlKey || tecla.altKey || tecla.which == 0){
			return true;
		}

		var codigo = (tecla.which ? tecla.which : tecla.keyCode ? tecla.keyCode : tecla.charCode);

		//se tiver precionado enter (codigo == 13) passa.
		//se for tap (codigo == 8) passa.
		//se a tecla precionada fot "/" (codigo == 47) passa.
		if( codigo == 13 || codigo == 8 || codigo == 47){
			return true;
		}

		//se for um numero (codigo < 58 && codigo > 47) passa.
		//se o tamando estiver dentro dos limites (campo.value.length < 10) passa.
		if(codigo < 58 && codigo > 47 && campo.value.length < 10){
			return true;
		}
		
		return false;
	}
