package testcase.infra.db;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.ImprovedNamingStrategy;

import br.infra.util.Log;

/**
 * Exportar o Banco
 * 
 * @author ricardo
 * 
 */
public class TesteHibernate {
	protected static Logger log = Log.getLogger(TesteHibernate.class);

	public static void main(String[] args) throws Exception {

		Configuration configuration = new Configuration();
		configuration.setNamingStrategy(new ImprovedNamingStrategy());
		configuration.configure();
		StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
		configuration.getProperties()).build();

		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory(serviceRegistry);
		
		System.out.println(sessionFactory);
		System.out.println(sessionFactory.openSession());
		System.out.println(sessionFactory.openSession().createQuery("from Tag").list());
		
		System.out.println("Hiberante OK");
		
		System.exit(0);
	}
}
