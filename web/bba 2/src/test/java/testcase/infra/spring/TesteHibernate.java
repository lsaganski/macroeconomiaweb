package testcase.infra.spring;

import java.io.IOException;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.spring.SpringUtil;

public class TesteHibernate {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		Session session = SpringUtil.getInstance().openSession();

		System.out.println(session);

		Usuario u = (Usuario) session.createQuery("from Usuario where id=3").setCacheable(true).uniqueResult();
		System.out.println(u.getNome());

		// session.doWork(new Work(){
		// @Override
		// public void execute(Connection conn) throws SQLException {
		// conn.createStatement().executeUpdate("update usuario set nome='X' where id=3");
		// }});
		
//		session.evict(u);

		session.createQuery("update Usuario u set u.nome = 'Y2' where u.id=3").executeUpdate();
		
		
		clearHibernateCache();
		evict2ndLevelCache();
		
		u = (Usuario) session.createQuery("from Usuario where id=3").setCacheable(true).uniqueResult();
		System.out.println(u.getNome());

		session.close();

		System.exit(0);
	}

	public static void clearHibernateCache() {
		Session s = SpringUtil.getInstance().getSession();
		SessionFactory sf = s.getSessionFactory();
		System.out.println(sf.getCache());
		sf.getCache().evictAllRegions();
		sf.getCache().evictCollectionRegions();
		sf.getCache().evictDefaultQueryRegion();
		sf.getCache().evictEntityRegions();
		sf.getCache().evictQueryRegions();
		sf.getCache().evictNaturalIdRegions();
		
		return;
	}
	
	/**
	 * Evicts all second level cache hibernate entites. This is generally only
	 * needed when an external application modifies the game databaase.
	 */
	public static void evict2ndLevelCache() {
	    try {
	    	Session s = SpringUtil.getInstance().getSession();
	    	SessionFactory sf = s.getSessionFactory();
	        Map<String, ClassMetadata> classesMetadata = sf.getAllClassMetadata();
	        for (String entityName : classesMetadata.keySet()) {
	            sf.evictEntity(entityName);
	            sf.evictQueries();
	        }
	       
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}
