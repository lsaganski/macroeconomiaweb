package testcase.infra.spring.scripts;

import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.utils.ChartSetUtils;
import testcase.infra.spring.InfraTestCase;

public class ScriptFixNameUserTestCase extends InfraTestCase {

	public void testScript() throws Exception {
		Usuario u = usuarioService.get(4311L);
		
		System.out.println(u.getNome());
		String nome = new String(u.getNome().getBytes("ISO-8859-1"), "UTF-8");
		u.setNome(nome);
		System.out.println(u.getNome());
		
		System.out.println(u.getNome().getBytes("UTF-8"));
		System.out.println(ChartSetUtils.isUTF8MisInterpreted(u.getNome()));
		System.out.println(ChartSetUtils.isUTF8MisInterpreted(u.getNome(),"ISO-8859-1"));
		System.out.println(ChartSetUtils.isUTF8MisInterpreted(u.getNome(),"UTF-8"));
		
		u = usuarioService.get(2524L);
		
		System.out.println(u.getNome());
		System.out.println(ChartSetUtils.isUTF8MisInterpreted(u.getNome()));
		System.out.println(ChartSetUtils.isUTF8MisInterpreted(u.getNome(),"ISO-8859-1"));
		System.out.println(ChartSetUtils.isUTF8MisInterpreted(u.getNome(),"UTF-8"));
	}
}
