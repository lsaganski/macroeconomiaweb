package testcase.infra.spring;

import org.apache.log4j.Logger;

import br.infra.util.Log;
import br.livetouch.livecom.domain.Usuario;
import br.livetouch.livecom.domain.service.UsuarioService;
import br.livetouch.spring.SpringUtil;

/**
 * Atualiza a base para adicionar o grupo de origem nos grupos do usuario
 * 
 * @author Ricardo Lecheta
 *
 */
public class TesteSelectUsuarioGrupos {
	protected static Logger log = Log.getLogger(TesteSelectUsuarioGrupos.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringUtil spring = SpringUtil.getTestInstance();
		try {

			UsuarioService service = (UsuarioService) spring.getBean(UsuarioService.class);

			Usuario u = service.get(2L);
			System.out.println(u);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			spring.closeSession();
		}
	}

}
