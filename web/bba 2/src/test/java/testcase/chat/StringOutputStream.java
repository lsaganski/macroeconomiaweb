package testcase.chat;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import org.apache.commons.io.IOUtils;

public class StringOutputStream {

	public static final boolean FLAG = true;
	private static final boolean DEBUG_ON = true;
    private final OutputStream out;

    public StringOutputStream(OutputStream out) {
        this.out = out;
    }

    public void writeUTF(String s) throws IOException {
        if (s != null) {
        	
        	log(s);

            ByteArrayOutputStream bytArray = new ByteArrayOutputStream();

            int length = s.length();

            // Create buffer
            byte[] bytesMessageLenght = ByteBuffer.allocate(4).putInt(length).array();
            byte[] bytes = s.getBytes("UTF-8");
            bytArray.write(bytesMessageLenght);
            bytArray.write(bytes);

            // Write
            byte[] byteArray = bytArray.toByteArray();

			this.out.write(byteArray);
            this.out.flush();
        }
    }

    public void writeUTF2(String s) throws IOException {
        if (s != null) {
        	
        	log(s);

            int length = s.length();

            // Create buffer
            byte[] bytesMessageLenght = ByteBuffer.allocate(4).putInt(length).array();
            this.out.write(bytesMessageLenght);
            // Write
            this.out.write(s.getBytes("UTF-8"));
            this.out.flush();
        }
    }
    private void log(String s) {
        if (DEBUG_ON) {
           System.out.println(">> " + s);
        }
    }

    public void close() {
        IOUtils.closeQuietly(out);
    }
}
