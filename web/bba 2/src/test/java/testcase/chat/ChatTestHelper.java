package testcase.chat;

import org.json.JSONException;
import org.json.JSONObject;

public class ChatTestHelper {

	public static boolean validateJsonOK(String sjson) throws JSONException {
		if(sjson == null) {
			return false;
		}
		try {
			JSONObject json = new JSONObject(sjson);
			JSONObject jsonContent = json.optJSONObject("content");
			String jsonStatus = jsonContent.optString("status");
			boolean ok = "ok".equalsIgnoreCase(jsonStatus);
			return ok;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
