package testcase.chat;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import org.apache.commons.io.IOUtils;

public class StringInputStream {

    private final InputStream in;
    boolean LOG_ON = true;

    public StringInputStream(InputStream in) {
        this.in = in;
    }

    public String readUTF() throws IOException {

        try {

            // Le a resposta do socket
            byte[] bytes = new byte[4];

            if (this.in.read(bytes, 0, 4) != 4) {
                return null;
            }


            ByteBuffer wrapped = ByteBuffer.wrap(bytes); // big-endian by default
            int num = wrapped.getInt(); // 1

            byte[] bytesMensagem = new byte[num];

            if (this.in.read(bytesMensagem, 0, num) != num) {
                return null;
            }

            String s = new String(bytesMensagem, "UTF-8");

            System.out.println("readUTF: " + s);

            return s;
        } catch (IOException e) {
            throw e;
        }

    }

    public byte[] readLength(int len) throws IOException {
        byte[] bytes = new byte[len];
        int off = 0;
        int i;
        do {
            i = this.in.read(bytes, off, len - off);
            if (i == -1) {
                try {
                    Thread.sleep(50);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                continue;
            }
            off += i;
        } while (len > off);
        return bytes;
    }
    
    public byte[] readToAvailable(int len) throws IOException {
        byte[] bytes = new byte[len];
        int off = 0;
        int i;
        do {
            i = this.in.read(bytes, off, len - off);
            if (i == -1) {
                try {
                    Thread.sleep(50);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                continue;
            }
            off += i;
        } while (len > off);
        return bytes;
    }

    public void close() {
        IOUtils.closeQuietly(in);
    }
}
