package testcase.utils;

import java.io.IOException;

public class Teste {

	public static void main(String[] args) throws IOException {
		String sql = "update notification set send_push=?, data_push=?, visivel=? where parent_id = ? and usuario_to_id in(%s)";
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < 12000; i++) {
			stringBuffer.append(i);
			stringBuffer.append(",");
		}
		String teste = String.format(sql, stringBuffer.toString());
		System.out.println(teste);
		
	}
	
	public static String replaceLast(String text, String regex, String replacement) {
        return text.replaceFirst("(?s)"+regex+"(?!.*?"+regex+")", replacement);
    }
}
