package testcase.utils;

import br.livetouch.livecom.crypt.AESCrypt;
import junit.framework.TestCase;

public class CryptTest extends TestCase {

	public void testCrypt() throws Exception {
		AESCrypt c = new AESCrypt();
		String key = "rlecheta@livetouch.com.br";
		
		// Criptografa...
		String s = c.encrypt("Ricardo Lecheta", key);
		System.out.println(s);
		
		// Descritpgrafa
		String s2 = c.decrypt(s, key);
		System.out.println(s2);
		
		assertEquals("Ricardo Lecheta", s2);
	}
}
