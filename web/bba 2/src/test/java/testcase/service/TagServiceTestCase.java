package testcase.service;

import java.util.List;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Tag;
import net.livetouch.extras.util.TimeUtil;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class TagServiceTestCase extends SpringTest {

	public void testFindAll() throws DomainException {
		Empresa e = empresaService.get(13L);
		List<Tag> list = tagService.findAll(e);
		for (Tag c : list) {
			System.out.println(c);
		}
	}
	
	public void testDeleteAll() throws DomainException {
		TimeUtil.start();
		List<Tag> list = tagService.findAll(null);
		int total = list.size();
		System.out.println("Total: " + list.size());
		
		for (Tag c : list) {
			tagService.delete(null, c, true);
			System.out.println("Total: " + total--);
		}
		
		System.out.println("Total: " + total);
		System.out.println("Total2: " + tagService.findAll(null));
	}

	public void testDelete() throws DomainException {
		//345: Java
		long id = 374;
		Tag tag = tagService.get(id);
		assertNotNull("Tag not found", tag);

		System.out.println(tag.getNome());
		sleep(500);
		tagService.delete(null, tag, true);
	}

}
