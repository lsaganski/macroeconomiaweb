package testcase.service.fixes;

import java.util.List;

import br.livetouch.livecom.domain.Post;
import testcase.infra.spring.InfraTestCase;

public class FixLogTransacaoTestCase extends InfraTestCase {

	public void testPopular() throws Exception {
		log.debug("Base populada com sucesso");

		List<Post> list = postService.findAll();
		System.out.println("Corrigindo as tags de " + list.size() + " posts.");
		for (Post p : list) {
			if(p.getTags() == null && p.getTagsList().size() > 0) {
				p.setTagsList(p.getTagsList());
				postService.saveOrUpdate(userAdmin,p);
				System.out.println("Post ["+p.toStringDesc()+"] corrigido ");
				break;
			}
		}
	}
}
