package testcase.service;

import java.util.List;

import br.livetouch.livecom.domain.Comentario;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class ComentarioServiceTestCase extends SpringTest {

	public void testDeleteAll() throws DomainException {
		List<Comentario> list = comentarioService.findAll();
		System.out.println("Comentarios: " + list.size());

		List<Long> ids = Comentario.getIds(list);
		System.out.println("Total: " + ids);

		comentarioService.delete(null,ids,false);

		System.out.println("OK");
	}
	
	public void testDelete() throws DomainException {
		long id = 4059;

		Comentario c = comentarioService.get(id);
		assertNotNull("Comentario not found",c);

		comentarioService.delete(null, c, false);
		
		c = comentarioService.get(id);

		assertNull(c);
	}
}
