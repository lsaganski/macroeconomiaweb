package testcase.service;

import java.util.List;
import java.util.Set;

import br.livetouch.livecom.domain.Arquivo;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.extras.util.TimeUtil;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class ArquivoServiceTestCase extends SpringTest {
	
	public void testDeleteAllIds() throws DomainException {
		TimeUtil.start();

		List<Arquivo> arquivos = arquivoService.findAll(null);
		List<Long> ids = Arquivo.getIds(arquivos);
		int total = ids.size();
		System.out.println("Total: " + ids.size());
		
		arquivoService.delete(null,ids, false);
		
//		for (Arquivo a : ids) {
//			System.out.println(a.getNome());
//			try {
//				arquivoService.delete(null,a, false);
//				System.out.println("Total: " + total--);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
		System.out.println("Total: " + total);
	}

	public void testDeleteAll() throws DomainException {
		List<Arquivo> list = arquivoService.findAll(null);
		int total = list.size();
		System.out.println("Total: " + list.size());
		for (Arquivo a : list) {
			System.out.println(a.getNome());
			try {
				arquivoService.delete(null,a, false);
				System.out.println("Total: " + total--);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Total: " + total);
	}
	
	public void testDeleteAllFromUser() throws DomainException {
		long id = 13;
		Usuario u = usuarioService.get(id);
		assertNotNull(u);
		System.out.println(u.toStringIdDesc());

		Set<Arquivo> list = u.getArquivos();
		System.out.println("Arquivos: " + list.size());

		for (Arquivo a : list) {
			System.out.println(a.toStringDesc());
			arquivoService.delete(a);
		}
		
		System.out.println("OK");
	}
	
	public void testDelete() throws DomainException {
		long id = 551;

		Arquivo c = arquivoService.get(id);
		assertNotNull(c);
		System.out.println(c.getArquivoRef());
		
		System.out.println(c.getNome());
		System.out.println(c.getUrl());

		arquivoService.delete(c);
		
		c = arquivoService.get(id);

		assertNull(c);
	}
}
