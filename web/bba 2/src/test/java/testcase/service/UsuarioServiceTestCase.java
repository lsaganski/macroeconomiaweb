package testcase.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import br.livetouch.livecom.domain.Empresa;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class UsuarioServiceTestCase extends SpringTest {
	
	public void testFoundTwoRepresentationsSameCollection() throws DomainException {
		for (int i = 0; i < 10; i++) {
			Thread t = new Thread("Thread:"+i){
				@Override
				public void run() {
					super.run();
					
					while(true) {
						
						Session session = spring.getSessionFactory().openSession();
						spring.bindThread(session);
						
						long id = 2524L;
						Usuario u = usuarioService.get(id);
						
						System.out.println(Thread.currentThread().getName() + ": user: " + u.getLogin());
						
						Date update = usuarioService.updateDataLastChat(u.getId());
						System.out.println("update: " + update);
						
//						try {
//							Thread.sleep(1000);
//						} catch (InterruptedException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
						
						spring.unbindThread(session);
					}
					
				}
			};
			t.setDaemon(true);
			t.start();
		}
		while (true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(".");
			
		}
	}
	
	/**
	 * A: Fim Test: 2.45 minutos
	 * B: Fim Test: 2 segundos
	 * 
	 * @throws DomainException
	 */
	public void testDelete() throws DomainException {
		startTime();
		long id = 111;
		Usuario u = usuarioService.get(id);
		assertNotNull("Este user ja foi excluido",u);

		try {
			System.err.println(u.toString());
			Thread.sleep(1000);
			usuarioService.delete(null, u);
		} catch (Exception e) {
			System.err.println("ERRO: " + e.getMessage());
		}

		System.out.println("delete ok");

		u = usuarioService.get(id);

		assertNull(u);
	}
	
	/**
	 * Time: 
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void testDeleteAll() throws Exception {
		List<Long> ids = session.createQuery("select id from Usuario order by id desc").list();
		int total = ids.size();
		System.out.println("Total users: " + total);
		Thread.sleep(5000);
		for (Long id: ids) {
			
			Usuario u = usuarioService.get(id);
			
			try {
//				if(StringUtils.contains(u.getLogin(), "admin") || StringUtils.contains(u.getLogin(), "root")) {
//					continue;
//				}
				
				System.out.println("User: " + u.toStringIdDesc());

				usuarioService.delete(null, u,true);
				
				System.out.println("OK: " + total--);
			} catch (Exception ex) {
				if(ex.getMessage().contains("Não é possível excluir o admin do sistema.")) {
					System.out.println("ERROR User : " + u.toStringIdDesc() + ": " + ex.getMessage());
				} else {
					Thread.sleep(100);
					System.err.println("ERROR User : " + u.toStringIdDesc() + ": " + ex.getMessage());
				}
				return;
			}
		}
		
		testCount() ;
	}

	public void testCount() throws Exception {
		long count = usuarioService.count();
		System.err.println("Users: " + count);
	}

	private int getCount() {
		System.out.println("--------------");
		List<Empresa> empresas;
		int count = 0;
		empresas = empresaService.findAll();
		for (Empresa e : empresas) {
			List<Usuario> usuarios = usuarioService.findAll(e);
			for (Usuario u : usuarios) {
				System.out.println(u.toStringIdDesc());
			}
			count += usuarios.size();
		}
		System.out.println("--------------");
		return count;
	}

}
