package testcase.service;

import java.util.List;

import br.livetouch.livecom.domain.Mensagem;
import br.livetouch.livecom.domain.MensagemConversa;
import br.livetouch.livecom.domain.Usuario;
import net.livetouch.tiger.ddd.DomainException;
import testcase.infra.spring.SpringTest;

public class MensagemServiceTestCase extends SpringTest {

	public void testDeleteAllMensagensFromUser() throws DomainException {
		long id = 2566;
		Usuario u = usuarioService.get(id);
		assertNotNull(u);

		List<Long> ids = mensagemService.findAllMensagensIdsOwnerByUser(u);
		System.out.println("Mensagem: " + ids.size());

		mensagemService.deleteMensagens(ids);
		
		System.out.println("OK");
	}

	public void testDeleteAllConversasFromUser() throws DomainException {
		long id = 2566;
		Usuario u = usuarioService.get(id);
		assertNotNull(u);
		List<Long> ids = mensagemService.findAllIdsConversasByUser(u);
		System.out.println("MensagemConversas: " + ids.size());

		mensagemService.deleteConversas(ids);

		System.out.println("OK");
	}

	public void testDeleteMensagem() throws DomainException {
		long id = 24170;

		Mensagem u = mensagemService.get(id);
		assertNotNull(u);
		
		System.out.println(u.getMsg());
		System.out.println(u.getId());
		System.out.println("delete");

		mensagemService.delete(u);
		
		System.out.println("delete ok");
	}
	
	public void testDeleteConversa() throws DomainException {
		long id = 24170;

		MensagemConversa u = mensagemService.getMensagemConversa(id);
		assertNotNull(u);
		
		System.out.println(u.getId());
		System.out.println("delete");

		mensagemService.delete(u);
		
		System.out.println("delete ok");
		
		u = mensagemService.getMensagemConversa(id);

		assertNull(u);
	}
}
