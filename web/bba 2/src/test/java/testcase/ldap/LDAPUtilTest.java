package testcase.ldap;

import br.livetouch.livecom.domain.ParametrosMap;
import br.livetouch.livecom.domain.Params;
import br.livetouch.livecom.ldap.LDAPUser;
import br.livetouch.livecom.ldap.LDAPUtil;
import junit.framework.TestCase;
import net.livetouch.tiger.ddd.DomainException;

public class LDAPUtilTest extends TestCase {

	public void testFind() {
		ParametrosMap params = getParams();

		LDAPUtil l = new LDAPUtil(params);
		LDAPUser r = l.findUser("augustoadsis@livetouch.com.br");
		System.out.println(">>"+r);
		assertNotNull("NULO!",r);
		assertEquals("uid=augusto,ou=tecnologia", r.getName());
		String userDN = r.getUserDN();
		assertEquals("uid=augusto,ou=tecnologia,dc=livetouchdev,dc=com,dc=br", userDN);
		
		assertTrue(r != null);
		
		System.err.println(r);
	}
	
	public void testLogin() {
		ParametrosMap params = getParams();

		LDAPUtil l = new LDAPUtil(params);
		LDAPUser r = l.loginByDN("uid=rlecheta,ou=tecnologia,dc=livetouchdev,dc=com,dc=br","rlecheta@livetouch.com.br", "rlecheta");
		System.out.println(r);
		assertNotNull("NULL!",r);
		assertEquals("uid=rlecheta,ou=tecnologia", r.getName());
		
		assertTrue(r != null);
	}
	
	public void testFindLogin() throws DomainException {
		ParametrosMap params = getParams();

		LDAPUtil l = new LDAPUtil(params);
		String login = "juillianlee@livetouch.com.br";
		LDAPUser r = l.findUser(login);
		String userDN = r.getUserDN();
		LDAPUser ldapUser = l.loginByDN(userDN,login, "juillian");
		
		assertNotNull("NULL!",ldapUser);
		assertEquals("uid=juillianlee,ou=tecnologia", r.getName());
		
	}
	

	private ParametrosMap getParams() {
		ParametrosMap params = ParametrosMap.getInstance();
		params.put(Params.LDAP_PROVIDER_URL, "ldap://livetouchdev.com.br:389");
		params.put(Params.LDAP_SECURITY_PRINCIPAL	, "cn=admin,dc=livetouchdev,dc=com,dc=br");
		params.put(Params.LDAP_SECURITY_PASSWD	, "livetouch2013");
		params.put(Params.LDAP_SEARCH_BASE	, "dc=livetouchdev,dc=com,dc=br");
		params.put(Params.LDAP_SEARCH_FILTER	, "(&(mail=%loginOrMail%))");
		return params;
	}
}
