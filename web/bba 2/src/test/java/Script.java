import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class Script {

	public static void main(String[] args) throws NamingException {
		try {
			System.out.println("Début du test Active Directory");

			Hashtable<String, String> ldapEnv = new Hashtable<String, String>(11);
			ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			ldapEnv.put(Context.PROVIDER_URL, "ldap://visanet.corp:389");
			ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
			ldapEnv.put(Context.SECURITY_PRINCIPAL,"CN=Jaime Peres Servidone Nagase,OU=Users,OU=Alphaville,OU=Sites,DC=visanet,DC=corp");
			ldapEnv.put(Context.SECURITY_CREDENTIALS, "Jimoeds8");
			// ldapEnv.put(Context.SECURITY_PROTOCOL, "ssl");
			// ldapEnv.put(Context.SECURITY_PROTOCOL, "simple");
			InitialDirContext ldapContext = new InitialDirContext(ldapEnv);

			 String ldapAccountToLookup = "jnagase";
			ldapAccountToLookup = "jaime.nagase@cielo.com.br";

			// Create the search controls
			SearchControls searchCtls = new SearchControls();

			// Specify the attributes to return
			String returnedAtts[] = { "sn", "givenName", "samAccountName", "MAIL" };
			searchCtls.setReturningAttributes(returnedAtts);

			// Specify the search scope
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			// specify the LDAP search filter
			String searchFilter = "(&(objectClass=user))";

			// Specify the Base for the search
			String searchBase = "dc=visanet,dc=corp";
			

			// 1) lookup the ldap account
			SearchResult srLdapUser = findAccountByAccountName(ldapContext, searchBase, ldapAccountToLookup);
			System.out.println(srLdapUser.getName());
			System.out.println(srLdapUser.getNameInNamespace());

//			print(searchCtls, searchFilter, searchBase);
			ldapContext.close();
		} catch (Exception e) {
			System.err.println(" Search error: " + e.getMessage() + " - " + e);
			System.exit(-1);
		}
	}

	private static void print(InitialDirContext ldapContext, SearchControls searchCtls, String searchFilter, String searchBase)
			throws NamingException {
		// initialize counter to total the results
		int totalResults = 0;
		
		// Search for objects using the filter
		NamingEnumeration<SearchResult> answer = ldapContext.search(searchBase, searchFilter, searchCtls);

		// Loop through the search results
		while (answer.hasMoreElements()) {
			SearchResult sr = (SearchResult) answer.next();

			totalResults++;

			System.out.println(">>>" + sr.getName());
			Attributes attrs = sr.getAttributes();
			System.out.println(">>>>>> " + attrs.get("samAccountName") + " - MAIL: " + attrs.get("MAIL"));
		}

		System.out.println("Total results: " + totalResults);
	}

	public static SearchResult findAccountByAccountName(DirContext ctx, String ldapSearchBase, String accountName)
			throws NamingException {

		String searchFilter = "(&(objectClass=user)(MAIL=" + accountName + "))";

		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		NamingEnumeration<SearchResult> results = ctx.search(ldapSearchBase, searchFilter, searchControls);

		SearchResult searchResult = null;
		if (results.hasMoreElements()) {
			searchResult = (SearchResult) results.nextElement();

			// make sure there is not another item available, there should be
			// only 1 match
			if (results.hasMoreElements()) {
				System.err.println("Matched multiple users for the accountName: " + accountName);
				return null;
			}
		}

		return searchResult;
	}
}